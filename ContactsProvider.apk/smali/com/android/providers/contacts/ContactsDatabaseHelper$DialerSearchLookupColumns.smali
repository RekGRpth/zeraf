.class public interface abstract Lcom/android/providers/contacts/ContactsDatabaseHelper$DialerSearchLookupColumns;
.super Ljava/lang/Object;
.source "ContactsDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DialerSearchLookupColumns"
.end annotation


# static fields
.field public static final CALL_LOG_ID:Ljava/lang/String; = "call_log_id"

.field public static final DATA_ID:Ljava/lang/String; = "data_id"

.field public static final IS_VISIABLE:Ljava/lang/String; = "is_visiable"

.field public static final NAME_TYPE:Ljava/lang/String; = "name_type"

.field public static final NORMALIZED_NAME:Ljava/lang/String; = "normalized_name"

.field public static final NORMALIZED_NAME_ALTERNATIVE:Ljava/lang/String; = "normalized_name_alternative"

.field public static final NUMBER_COUNT:Ljava/lang/String; = "number_count"

.field public static final RAW_CONTACT_ID:Ljava/lang/String; = "raw_contact_id"

.field public static final SEARCH_DATA_OFFSETS:Ljava/lang/String; = "search_data_offsets"

.field public static final SEARCH_DATA_OFFSETS_ALTERNATIVE:Ljava/lang/String; = "search_data_offsets_alternative"

.field public static final _ID:Ljava/lang/String; = "_id"
