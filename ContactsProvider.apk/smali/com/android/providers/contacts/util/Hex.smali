.class public Lcom/android/providers/contacts/util/Hex;
.super Ljava/lang/Object;
.source "Hex.java"


# static fields
.field private static final DIGITS:[B

.field private static final FIRST_CHAR:[C

.field private static final HEX_DIGITS:[C

.field private static final SECOND_CHAR:[C


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x100

    const/16 v1, 0x10

    new-array v1, v1, [C

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/providers/contacts/util/Hex;->HEX_DIGITS:[C

    new-array v1, v4, [C

    sput-object v1, Lcom/android/providers/contacts/util/Hex;->FIRST_CHAR:[C

    new-array v1, v4, [C

    sput-object v1, Lcom/android/providers/contacts/util/Hex;->SECOND_CHAR:[C

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    sget-object v1, Lcom/android/providers/contacts/util/Hex;->FIRST_CHAR:[C

    sget-object v2, Lcom/android/providers/contacts/util/Hex;->HEX_DIGITS:[C

    shr-int/lit8 v3, v0, 0x4

    and-int/lit8 v3, v3, 0xf

    aget-char v2, v2, v3

    aput-char v2, v1, v0

    sget-object v1, Lcom/android/providers/contacts/util/Hex;->SECOND_CHAR:[C

    sget-object v2, Lcom/android/providers/contacts/util/Hex;->HEX_DIGITS:[C

    and-int/lit8 v3, v0, 0xf

    aget-char v2, v2, v3

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/16 v1, 0x67

    new-array v1, v1, [B

    sput-object v1, Lcom/android/providers/contacts/util/Hex;->DIGITS:[B

    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x46

    if-gt v0, v1, :cond_1

    sget-object v1, Lcom/android/providers/contacts/util/Hex;->DIGITS:[B

    const/4 v2, -0x1

    aput-byte v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_2
    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    sget-object v1, Lcom/android/providers/contacts/util/Hex;->DIGITS:[B

    add-int/lit8 v2, v0, 0x30

    aput-byte v0, v1, v2

    add-int/lit8 v1, v0, 0x1

    int-to-byte v0, v1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_3
    const/4 v1, 0x6

    if-ge v0, v1, :cond_3

    sget-object v1, Lcom/android/providers/contacts/util/Hex;->DIGITS:[B

    add-int/lit8 v2, v0, 0x41

    add-int/lit8 v3, v0, 0xa

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    sget-object v1, Lcom/android/providers/contacts/util/Hex;->DIGITS:[B

    add-int/lit8 v2, v0, 0x61

    add-int/lit8 v3, v0, 0xa

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    add-int/lit8 v1, v0, 0x1

    int-to-byte v0, v1

    goto :goto_3

    :cond_3
    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeHex(Ljava/lang/String;)[B
    .locals 13
    .param p0    # Ljava/lang/String;

    const/16 v12, 0x66

    const/4 v11, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    and-int/lit8 v10, v8, 0x1

    if-eqz v10, :cond_0

    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Odd number of characters: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_0
    const/4 v0, 0x0

    shr-int/lit8 v10, v8, 0x1

    new-array v9, v10, [B

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, v8, :cond_6

    add-int/lit8 v6, v7, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-le v1, v12, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid hexadecimal digit: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_1
    sget-object v10, Lcom/android/providers/contacts/util/Hex;->DIGITS:[B

    aget-byte v3, v10, v1

    if-ne v3, v11, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v7, v6, 0x1

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-le v2, v12, :cond_3

    const/4 v0, 0x1

    move v6, v7

    goto :goto_1

    :cond_3
    sget-object v10, Lcom/android/providers/contacts/util/Hex;->DIGITS:[B

    aget-byte v4, v10, v2

    if-ne v4, v11, :cond_4

    const/4 v0, 0x1

    move v6, v7

    goto :goto_1

    :cond_4
    shl-int/lit8 v10, v3, 0x4

    or-int/2addr v10, v4

    int-to-byte v10, v10

    aput-byte v10, v9, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_5
    return-object v9

    :cond_6
    move v6, v7

    goto :goto_1
.end method

.method public static encodeHex([BZ)Ljava/lang/String;
    .locals 7
    .param p0    # [B
    .param p1    # Z

    array-length v5, p0

    mul-int/lit8 v5, v5, 0x2

    new-array v0, v5, [C

    const/4 v3, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v5, p0

    if-ge v1, v5, :cond_0

    aget-byte v5, p0, v1

    and-int/lit16 v2, v5, 0xff

    if-eqz p1, :cond_1

    if-nez v2, :cond_1

    array-length v5, p0

    add-int/lit8 v5, v5, -0x1

    if-ne v1, v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v5

    :cond_1
    add-int/lit8 v4, v3, 0x1

    sget-object v5, Lcom/android/providers/contacts/util/Hex;->FIRST_CHAR:[C

    aget-char v5, v5, v2

    aput-char v5, v0, v3

    add-int/lit8 v3, v4, 0x1

    sget-object v5, Lcom/android/providers/contacts/util/Hex;->SECOND_CHAR:[C

    aget-char v5, v5, v2

    aput-char v5, v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
