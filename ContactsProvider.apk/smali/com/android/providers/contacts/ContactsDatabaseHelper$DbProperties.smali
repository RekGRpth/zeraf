.class public interface abstract Lcom/android/providers/contacts/ContactsDatabaseHelper$DbProperties;
.super Ljava/lang/Object;
.source "ContactsDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DbProperties"
.end annotation


# static fields
.field public static final AGGREGATION_ALGORITHM:Ljava/lang/String; = "aggregation_v2"

.field public static final DIRECTORY_SCAN_COMPLETE:Ljava/lang/String; = "directoryScanComplete"

.field public static final KNOWN_ACCOUNTS:Ljava/lang/String; = "known_accounts"
