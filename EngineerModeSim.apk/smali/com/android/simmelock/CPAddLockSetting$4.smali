.class Lcom/android/simmelock/CPAddLockSetting$4;
.super Ljava/lang/Object;
.source "CPAddLockSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/simmelock/CPAddLockSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/CPAddLockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/CPAddLockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/16 v9, 0xfe

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v1, 0x3

    const/4 v2, 0x2

    const-string v0, "SIMMELOCK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clickFlag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v4}, Lcom/android/simmelock/CPAddLockSetting;->access$000(Lcom/android/simmelock/CPAddLockSetting;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/CPAddLockSetting;->access$000(Lcom/android/simmelock/CPAddLockSetting;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v0, v5}, Lcom/android/simmelock/CPAddLockSetting;->access$002(Lcom/android/simmelock/CPAddLockSetting;Z)Z

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbMCCMNCReadSIM:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbMCCMNCReadSIM1:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbMCCMNCReadSIM2:Z

    if-nez v0, :cond_2

    const/4 v0, 0x5

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-gt v0, v3, :cond_1

    const/4 v0, 0x6

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-virtual {v0, v5}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v0, v5, :cond_3

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v0, v5, :cond_4

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_5

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-gt v0, v9, :cond_5

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget v0, v0, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    if-le v0, v9, :cond_6

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID1ReadSIM:Z

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_7

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-gt v0, v9, :cond_7

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget v0, v0, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    if-le v0, v9, :cond_8

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM:Z

    if-eqz v0, :cond_8

    :cond_7
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v3, 0x4

    if-lt v0, v3, :cond_9

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v3, 0x8

    if-le v0, v3, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/CPAddLockSetting;->access$100(Lcom/android/simmelock/CPAddLockSetting;)Landroid/os/Handler;

    move-result-object v0

    const/16 v3, 0x78

    invoke-static {v0, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    check-cast v8, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget v0, v0, Lcom/android/simmelock/CPAddLockSetting;->intSIMNumber:I

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbMCCMNCReadSIM1:Z

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID1ReadSIM1:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM1:Z

    if-nez v0, :cond_c

    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM1:Z

    if-nez v0, :cond_e

    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_f
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID1ReadSIM1:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM1:Z

    if-nez v0, :cond_10

    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_11
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM1:Z

    if-nez v0, :cond_12

    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_13
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbMCCMNCReadSIM2:Z

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID1ReadSIM2:Z

    if-nez v0, :cond_15

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM2:Z

    if-nez v0, :cond_14

    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_14
    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID2:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_15
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM1:Z

    if-nez v0, :cond_16

    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID2:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_16
    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID2:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID2:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_17
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID1ReadSIM1:Z

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM1:Z

    if-nez v0, :cond_18

    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_18
    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID2:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_19
    iget-object v0, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/CPAddLockSetting;->mbGID2ReadSIM1:Z

    if-nez v0, :cond_1a

    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->etGID2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/CPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/CPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v5, v5, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/CPAddLockSetting$4;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v6, v6, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0
.end method
