.class Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "SpecialCharSequenceMgrProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryHandler"
.end annotation


# instance fields
.field private alert:Landroid/app/AlertDialog;

.field private mShouldDismissSimPickerDialog:Z

.field private sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1
    .param p1    # Landroid/content/ContentResolver;

    invoke-direct {p0, p1}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->mShouldDismissSimPickerDialog:Z

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;)Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;

    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    return-object v0
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 14
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onQueryComplete token = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    check-cast p2, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onQueryComplete sc = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->access$100()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onQueryComplete sc = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "sStopProgress "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->access$100()Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    if-eqz p3, :cond_1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v9

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    invoke-virtual {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->getTextField()Landroid/widget/EditText;

    move-result-object v13

    const/4 v10, 0x0

    const/4 v11, 0x0

    if-eqz p3, :cond_5

    if-eqz v13, :cond_5

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_3
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p1}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->fdnRequest(I)Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fdnRequest for "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    :cond_4
    :goto_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sc.mFoundForSlot["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v5, v5, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mFoundForSlot:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mSimNameForSlot:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mSimNumberForSlot:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "name = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " number = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-boolean v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mIsSingleQuery:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    const/4 v5, 0x0

    iput-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    :cond_6
    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mFoundForSlot:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    invoke-virtual {p0, v9, v3, v10, v11}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->showToast(Landroid/content/Context;Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v3, "index"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget-object v5, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget v5, v5, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->contactNum:I

    if-ne v3, v5, :cond_3

    const-string v3, "name"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v3, "number"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->mFoundForSlot:Landroid/util/SparseBooleanArray;

    const/4 v5, 0x1

    invoke-virtual {v3, p1, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto/16 :goto_1

    :cond_8
    invoke-static {}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->access$200()Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    iget-object v3, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    const/4 v5, 0x0

    iput-object v5, v3, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->progressDialog:Landroid/app/ProgressDialog;

    :cond_9
    iget-boolean v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->mShouldDismissSimPickerDialog:Z

    if-eqz v3, :cond_a

    const-string v3, "SpecialCharSequenceMgrProxy"

    const-string v5, "[onQueryComplete]should dismiss, not show sim select"

    invoke-static {v3, v5}, Lcom/mediatek/contacts/util/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    new-instance v12, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler$1;

    invoke-direct {v12, p0, v9}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler$1;-><init>(Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;Landroid/content/Context;)V

    const v3, 0x7f0c001a

    invoke-virtual {v9, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v9, v3, v5, v12}, Lcom/mediatek/contacts/widget/SimPickerDialog;->create(Landroid/content/Context;Ljava/lang/String;ZLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->alert:Landroid/app/AlertDialog;

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onquerycomplete: show the selector dialog = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    invoke-virtual {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->getQueryHandler()Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;

    move-result-object v1

    invoke-static {}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->access$200()Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/contacts/simcontact/SimCardUtils$SimUri;->getSimUri(I)Landroid/net/Uri;

    move-result-object v4

    iget-object v3, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->sc:Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "number"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "index"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[onQueryComplete]next slot to query is: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public shouldDismissSimPicker(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->mShouldDismissSimPickerDialog:Z

    if-eqz p1, :cond_0

    const-string v0, "SpecialCharSequenceMgrProxy"

    const-string v1, "[shouldDismissSimPicker]should dismiss"

    invoke-static {v0, v1}, Lcom/mediatek/contacts/util/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->alert:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SpecialCharSequenceMgrProxy"

    const-string v1, "[shouldDismissSimPicker]cancel the dialog"

    invoke-static {v0, v1}, Lcom/mediatek/contacts/util/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$QueryHandler;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    :cond_0
    return-void
.end method

.method protected showToast(Landroid/content/Context;Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const v5, 0x7f0c0284

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-static {p2}, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->access$000(Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    iget-object v3, p2, Lcom/mediatek/contacts/SpecialCharSequenceMgrProxy$SimContactQueryCookie;->text:Ljava/lang/String;

    invoke-virtual {v3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    if-le v0, v4, :cond_2

    const/4 v3, 0x5

    if-ge v0, v3, :cond_2

    const-string v3, "#"

    invoke-virtual {p4, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_2
    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSelection(I)V

    const v3, 0x7f0c0107

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p3, v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
