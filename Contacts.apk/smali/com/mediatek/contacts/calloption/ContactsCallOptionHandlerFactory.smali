.class public Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;
.super Lcom/mediatek/calloption/CallOptionHandlerFactory;
.source "ContactsCallOptionHandlerFactory.java"


# instance fields
.field private mFirstCallOptionHandler:Lcom/mediatek/calloption/FirstCallOptionHandler;

.field private mInternationalCallOptionHandler:Lcom/mediatek/calloption/InternationalCallOptionHandler;

.field private mInternetCallOptionHandler:Lcom/mediatek/calloption/InternetCallOptionHandler;

.field private mIpCallOptionHandler:Lcom/mediatek/calloption/IpCallOptionHandler;

.field private mSimSelectionCallOptionHandler:Lcom/mediatek/calloption/SimSelectionCallOptionHandler;

.field private mSimStatusCallOptionHandler:Lcom/mediatek/calloption/SimStatusCallOptionHandler;

.field private mVideoCallOptionHandler:Lcom/mediatek/calloption/VideoCallOptionHandler;

.field private mVoiceMailCallOptionHandler:Lcom/mediatek/calloption/VoiceMailCallOptionHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/calloption/CallOptionHandlerFactory;-><init>()V

    new-instance v0, Lcom/mediatek/contacts/calloption/ContactsFirstCallOptionHandler;

    invoke-direct {v0}, Lcom/mediatek/contacts/calloption/ContactsFirstCallOptionHandler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mFirstCallOptionHandler:Lcom/mediatek/calloption/FirstCallOptionHandler;

    new-instance v0, Lcom/mediatek/contacts/calloption/ContactsInternetCallOptionHandler;

    invoke-direct {v0}, Lcom/mediatek/contacts/calloption/ContactsInternetCallOptionHandler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mInternetCallOptionHandler:Lcom/mediatek/calloption/InternetCallOptionHandler;

    new-instance v0, Lcom/mediatek/contacts/calloption/ContactsVideoCallOptionHandler;

    invoke-direct {v0}, Lcom/mediatek/contacts/calloption/ContactsVideoCallOptionHandler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mVideoCallOptionHandler:Lcom/mediatek/calloption/VideoCallOptionHandler;

    new-instance v0, Lcom/mediatek/contacts/calloption/ContactsInternationalCallOptionHandler;

    invoke-direct {v0}, Lcom/mediatek/contacts/calloption/ContactsInternationalCallOptionHandler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mInternationalCallOptionHandler:Lcom/mediatek/calloption/InternationalCallOptionHandler;

    new-instance v0, Lcom/mediatek/contacts/calloption/ContactsSimSelectionCallOptionHandler;

    invoke-direct {v0}, Lcom/mediatek/contacts/calloption/ContactsSimSelectionCallOptionHandler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mSimSelectionCallOptionHandler:Lcom/mediatek/calloption/SimSelectionCallOptionHandler;

    new-instance v0, Lcom/mediatek/contacts/calloption/ContactsSimStatusCallOptionHandler;

    invoke-direct {v0}, Lcom/mediatek/contacts/calloption/ContactsSimStatusCallOptionHandler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mSimStatusCallOptionHandler:Lcom/mediatek/calloption/SimStatusCallOptionHandler;

    new-instance v0, Lcom/mediatek/contacts/calloption/ContactsIpCallOptionHandler;

    invoke-direct {v0}, Lcom/mediatek/contacts/calloption/ContactsIpCallOptionHandler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mIpCallOptionHandler:Lcom/mediatek/calloption/IpCallOptionHandler;

    new-instance v0, Lcom/mediatek/contacts/calloption/ContactsVoiceMailCallOptionHandler;

    invoke-direct {v0}, Lcom/mediatek/contacts/calloption/ContactsVoiceMailCallOptionHandler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mVoiceMailCallOptionHandler:Lcom/mediatek/calloption/VoiceMailCallOptionHandler;

    return-void
.end method


# virtual methods
.method public getFirstCallOptionHandler()Lcom/mediatek/calloption/FirstCallOptionHandler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mFirstCallOptionHandler:Lcom/mediatek/calloption/FirstCallOptionHandler;

    return-object v0
.end method

.method public getInternationalCallOptionHandler()Lcom/mediatek/calloption/InternationalCallOptionHandler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mInternationalCallOptionHandler:Lcom/mediatek/calloption/InternationalCallOptionHandler;

    return-object v0
.end method

.method public getInternetCallOptionHandler()Lcom/mediatek/calloption/InternetCallOptionHandler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mInternetCallOptionHandler:Lcom/mediatek/calloption/InternetCallOptionHandler;

    return-object v0
.end method

.method public getIpCallOptionHandler()Lcom/mediatek/calloption/IpCallOptionHandler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mIpCallOptionHandler:Lcom/mediatek/calloption/IpCallOptionHandler;

    return-object v0
.end method

.method public getSimSelectionCallOptionHandler()Lcom/mediatek/calloption/SimSelectionCallOptionHandler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mSimSelectionCallOptionHandler:Lcom/mediatek/calloption/SimSelectionCallOptionHandler;

    return-object v0
.end method

.method public getSimStatusCallOptionHandler()Lcom/mediatek/calloption/SimStatusCallOptionHandler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mSimStatusCallOptionHandler:Lcom/mediatek/calloption/SimStatusCallOptionHandler;

    return-object v0
.end method

.method public getVideoCallOptionHandler()Lcom/mediatek/calloption/VideoCallOptionHandler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mVideoCallOptionHandler:Lcom/mediatek/calloption/VideoCallOptionHandler;

    return-object v0
.end method

.method public getVoiceMailCallOptionHandler()Lcom/mediatek/calloption/VoiceMailCallOptionHandler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandlerFactory;->mVoiceMailCallOptionHandler:Lcom/mediatek/calloption/VoiceMailCallOptionHandler;

    return-object v0
.end method
