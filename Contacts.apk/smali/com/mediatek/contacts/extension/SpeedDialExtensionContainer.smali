.class public Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;
.super Lcom/android/contacts/ext/SpeedDialExtension;
.source "SpeedDialExtensionContainer.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SpeedDialExtensionContainer"


# instance fields
.field private mSubExtensionList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/contacts/ext/SpeedDialExtension;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/contacts/ext/SpeedDialExtension;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lcom/android/contacts/ext/SpeedDialExtension;)V
    .locals 1
    .param p1    # Lcom/android/contacts/ext/SpeedDialExtension;

    iget-object v0, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearPrefStateIfNecessary(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    const-string v2, "SpeedDialExtensionContainer"

    const-string v4, "SpeedDialManageActivity: [clearPrefStateIfNecessary]"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v2, :cond_0

    move v1, v3

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/contacts/ext/SpeedDialExtension;

    invoke-virtual {v2, p1}, Lcom/android/contacts/ext/SpeedDialExtension;->clearPrefStateIfNecessary(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_0
.end method

.method public needCheckContacts(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    const-string v2, "SpeedDialExtensionContainer"

    const-string v4, "SpeedDialManageActivity: [needCheckContacts]"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v2, :cond_0

    move v1, v3

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/contacts/ext/SpeedDialExtension;

    invoke-virtual {v2, p1}, Lcom/android/contacts/ext/SpeedDialExtension;->needCheckContacts(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_0
.end method

.method public needClearPreState(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    const-string v2, "SpeedDialExtensionContainer"

    const-string v4, "[needClearPreState()]"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v2, :cond_0

    move v1, v3

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/contacts/ext/SpeedDialExtension;

    invoke-virtual {v2, p1}, Lcom/android/contacts/ext/SpeedDialExtension;->needClearPreState(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_0
.end method

.method public needClearSharedPreferences(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    const-string v2, "SpeedDialExtensionContainer"

    const-string v4, "needClearSharedPreferences()"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v2, :cond_0

    const-string v2, "SpeedDialExtensionContainer"

    const-string v4, "[needClearSharedPreferences()"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v3

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/contacts/ext/SpeedDialExtension;

    invoke-virtual {v2, p1}, Lcom/android/contacts/ext/SpeedDialExtension;->needClearSharedPreferences(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v2, "SpeedDialExtensionContainer"

    const-string v3, "needClearSharedPreferences()]"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v2, "SpeedDialExtensionContainer"

    const-string v4, "[needClearSharedPreferences()]"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v3

    goto :goto_0
.end method

.method public remove(Lcom/android/contacts/ext/SpeedDialExtension;)V
    .locals 1
    .param p1    # Lcom/android/contacts/ext/SpeedDialExtension;

    iget-object v0, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setAddPosition(IZLjava/lang/String;)I
    .locals 4
    .param p1    # I
    .param p2    # Z
    .param p3    # Ljava/lang/String;

    const-string v2, "SpeedDialExtensionContainer"

    const-string v3, "[setAddPosition()]"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return p1

    :cond_1
    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/contacts/ext/SpeedDialExtension;

    invoke-virtual {v2, p1, p2, p3}, Lcom/android/contacts/ext/SpeedDialExtension;->setAddPosition(IZLjava/lang/String;)I

    move-result v1

    if-eq v1, p1, :cond_2

    move p1, v1

    goto :goto_0
.end method

.method public setView(Landroid/view/View;IZILjava/lang/String;)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Z
    .param p4    # I
    .param p5    # Ljava/lang/String;

    const-string v0, "SpeedDialExtensionContainer"

    const-string v1, "[setView()]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/ext/SpeedDialExtension;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/contacts/ext/SpeedDialExtension;->setView(Landroid/view/View;IZILjava/lang/String;)V

    goto :goto_0
.end method

.method public showSpeedInputDialog(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    const-string v2, "SpeedDialExtensionContainer"

    const-string v4, "[showSpeedInputDialog()]"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    if-nez v2, :cond_0

    move v1, v3

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->mSubExtensionList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/contacts/ext/SpeedDialExtension;

    invoke-virtual {v2, p1}, Lcom/android/contacts/ext/SpeedDialExtension;->showSpeedInputDialog(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_0
.end method
