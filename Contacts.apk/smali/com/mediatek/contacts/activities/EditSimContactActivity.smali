.class public Lcom/mediatek/contacts/activities/EditSimContactActivity;
.super Landroid/app/Activity;
.source "EditSimContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;
    }
.end annotation


# static fields
.field private static final ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

.field public static final EDIT_SIM_CONTACT:Ljava/lang/String; = "com.android.contacts.action.EDIT_SIM_CONTACT"

.field private static final LISTEN_PHONE_NONE_STATES:I = 0x2

.field private static final LISTEN_PHONE_STATES:I = 0x1

.field static final MODE_DEFAULT:I = 0x0

.field static final MODE_EDIT:I = 0x2

.field static final MODE_INSERT:I = 0x1

.field private static final SIM_DATA:Ljava/lang/String; = "simData"

.field private static final SIM_NUM_PATTERN:Ljava/lang/String; = "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

.field private static final SIM_OLD_DATA:Ljava/lang/String; = "simOldData"

.field private static final TAG:Ljava/lang/String; = "EditSimContactActivity"

.field private static final USIM_EMAIL_PATTERN:Ljava/lang/String; = "[[0-9][a-z][A-Z][_]][[0-9][a-z][A-Z][-_.]]*@[[0-9][a-z][A-Z][-_.]]+"

.field private static sAfterOtherPhone:Ljava/lang/String;

.field private static sAfterPhone:Ljava/lang/String;

.field private static sEmail:Ljava/lang/String;

.field private static sName:Ljava/lang/String;

.field private static sNickname:Ljava/lang/String;

.field private static sOtherPhone:Ljava/lang/String;

.field private static sPhone:Ljava/lang/String;

.field private static sUpdateNickname:Ljava/lang/String;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAccountName:Ljava/lang/String;

.field private mAccountType:Ljava/lang/String;

.field private mAirPlaneModeOn:Z

.field private mAirPlaneModeOnNotEdit:Z

.field private mAnrsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/contacts/ext/Anr;",
            ">;"
        }
    .end annotation
.end field

.field mContactId:I

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mDoublePhoneNumber:Z

.field private mEmail2GInvalid:Z

.field private mEmailInvalid:Z

.field private mFDNEnabled:Z

.field private mFixNumberInvalid:Z

.field private mFixNumberLong:Z

.field private mGeneralFailure:Z

.field mGroupAddList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mGroupNum:I

.field final mITel:Lcom/android/internal/telephony/ITelephony;

.field private mIndexInSim:J

.field mIndicate:J

.field private mLookupUri:Landroid/net/Uri;

.field mMode:I

.field private mNameLong:Z

.field private mNumberInvalid:Z

.field private mNumberIsNull:Z

.field private mNumberLong:Z

.field private mOldAnrsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/contacts/ext/Anr;",
            ">;"
        }
    .end annotation
.end field

.field private mOldEmail:Ljava/lang/String;

.field mOldGroupAddList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOldName:Ljava/lang/String;

.field private mOldNickname:Ljava/lang/String;

.field private mOldOtherPhone:Ljava/lang/String;

.field private mOldPhone:Ljava/lang/String;

.field private mOnBackGoing:Z

.field mPhbReady:Z

.field private mPhoneTypeSuffix:Ljava/lang/String;

.field private mQuitEdit:Z

.field mRawContactId:J

.field private mSIMInvalid:Z

.field private mSaveContactHandler:Landroid/os/Handler;

.field private mSaveDialog:Landroid/app/ProgressDialog;

.field private mSaveFailToastStrId:I

.field private mSaveMode:I

.field private mSimData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/contacts/model/RawContactDelta;",
            ">;"
        }
    .end annotation
.end field

.field private mSimOldData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/contacts/model/RawContactDelta;",
            ">;"
        }
    .end annotation
.end field

.field private mSimType:Ljava/lang/String;

.field private mSlotId:I

.field private mStorageFull:Z

.field private mUpdateAdditionalNumber:Ljava/lang/String;

.field private mUpdateName:Ljava/lang/String;

.field private mUpdatemail:Ljava/lang/String;

.field private mUpdatephone:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterPhone:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterOtherPhone:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "emails"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "additionalNumber"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "groupIds"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sNickname:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sUpdateNickname:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdatephone:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdatemail:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateAdditionalNumber:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    const-string v0, "SIM"

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimType:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    iput-wide v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndexInSim:J

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOn:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOnNotEdit:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFDNEnabled:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSIMInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberIsNull:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberLong:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNameLong:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberLong:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mStorageFull:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOnBackGoing:Z

    iput-object v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneTypeSuffix:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmailInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail2GInvalid:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mITel:Lcom/android/internal/telephony/ITelephony;

    iput-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndicate:J

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mMode:I

    iput-wide v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mRawContactId:J

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mContactId:I

    iput v3, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    iput-object v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveContactHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAnrsList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldAnrsList:Ljava/util/ArrayList;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldNickname:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAnrsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sNickname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sUpdateNickname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sUpdateNickname:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberIsNull:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1802(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOnBackGoing:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->showSaveFailToast()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdatephone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/contacts/activities/EditSimContactActivity;Landroid/net/Uri;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText2(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdatephone:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Landroid/accounts/Account;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$2200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/mediatek/contacts/activities/EditSimContactActivity;ILjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->showResultToastText(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhoneTypeSuffix:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/mediatek/contacts/activities/EditSimContactActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-wide v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndexInSim:J

    return-wide v0
.end method

.method static synthetic access$2700()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/mediatek/contacts/activities/EditSimContactActivity;I)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->updateFailToastText(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldNickname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldAnrsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOn:Z

    return p1
.end method

.method static synthetic access$3602(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFDNEnabled:Z

    return p1
.end method

.method static synthetic access$3702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSIMInvalid:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/mediatek/contacts/activities/EditSimContactActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdatemail:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdatemail:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateAdditionalNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mUpdateAdditionalNumber:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterOtherPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/contacts/activities/EditSimContactActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/EditSimContactActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    return-object v0
.end method

.method private doSaveAction(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x2

    const/4 v3, 0x1

    const-string v1, "EditSimContactActivity"

    const-string v2, "In doSaveAction "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v3, :cond_1

    const-string v1, "huibin"

    const-string v2, "doSaveAction mode == MODE_INSERT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "EditSimContactActivity"

    const-string v2, "mode == MODE_INSERT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->getsaveContactHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;

    invoke-direct {v1, p0, v3}, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v4, :cond_0

    const-string v1, "huibin"

    const-string v2, "doSaveAction mode == MODE_EDIT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->getsaveContactHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;

    invoke-direct {v1, p0, v4}, Lcom/mediatek/contacts/activities/EditSimContactActivity$InsertSimContactThread;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private getsaveContactHandler()Landroid/os/Handler;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveContactHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "saveContacts"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveContactHandler:Landroid/os/Handler;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveContactHandler:Landroid/os/Handler;

    return-object v1
.end method

.method private setSaveFailToastText()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSaveFailToastText mPhbReady is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    if-nez v2, :cond_1

    const v2, 0x7f0c0040

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    :cond_0
    :goto_0
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSaveFailToastStrId IS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    if-ltz v2, :cond_b

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$1;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_1
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOn:Z

    if-eqz v2, :cond_2

    const v2, 0x7f0c0043

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOn:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFDNEnabled:Z

    if-eqz v2, :cond_3

    const v2, 0x7f0c0044

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFDNEnabled:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSIMInvalid:Z

    if-eqz v2, :cond_4

    const v2, 0x7f0c0041

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSIMInvalid:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberIsNull:Z

    if-eqz v2, :cond_5

    const v2, 0x7f0c0042

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberIsNull:Z

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    if-eqz v2, :cond_6

    const v2, 0x7f0c0045

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    goto :goto_0

    :cond_6
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmailInvalid:Z

    if-eqz v2, :cond_7

    const v2, 0x7f0c0046

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmailInvalid:Z

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail2GInvalid:Z

    if-eqz v2, :cond_8

    const v2, 0x7f0c0047

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mEmail2GInvalid:Z

    goto :goto_0

    :cond_8
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    if-eqz v2, :cond_9

    const v2, 0x7f0c0048

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberInvalid:Z

    goto/16 :goto_0

    :cond_9
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOnNotEdit:Z

    if-eqz v2, :cond_a

    const v2, 0x7f0c0049

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAirPlaneModeOnNotEdit:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto/16 :goto_0

    :cond_a
    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    if-eqz v2, :cond_0

    const v2, 0x7f0c003f

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private setSaveFailToastText2(Landroid/net/Uri;)Z
    .locals 5
    .param p1    # Landroid/net/Uri;

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-eqz p1, :cond_a

    const-string v2, "error"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    const-string v2, "-1"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberLong:Z

    const v2, 0x7f0c004e

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberLong:Z

    :cond_0
    :goto_0
    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSaveFailToastText2 mSaveFailToastStrId IS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    if-ltz v2, :cond_9

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$2;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$2;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    :goto_1
    return v0

    :cond_2
    const-string v2, "-2"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNameLong:Z

    const v2, 0x7f0c004c

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNameLong:Z

    goto :goto_0

    :cond_3
    const-string v2, "-3"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mStorageFull:Z

    const v2, 0x7f0c004d

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mStorageFull:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_4
    const-string v2, "-6"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberLong:Z

    const v2, 0x7f0c004b

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mFixNumberLong:Z

    goto :goto_0

    :cond_5
    const-string v2, "-10"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    const v2, 0x7f0c004f

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto/16 :goto_0

    :cond_6
    const-string v2, "-11"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    const v2, 0x7f0c00df

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto/16 :goto_0

    :cond_7
    const-string v2, "-12"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    const v2, 0x7f0c00ab

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGeneralFailure:Z

    goto/16 :goto_0

    :cond_8
    const-string v2, "-13"

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0c0050

    iput v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1

    :cond_a
    if-eqz p1, :cond_1

    move v0, v1

    goto/16 :goto_1
.end method

.method private showResultToastText(ILjava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v6, -0x1

    const/4 v1, 0x0

    if-ne p1, v6, :cond_0

    const v2, 0x7f0c0140

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "EditSimContactActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[mtk performance result]:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    move-object v0, v1

    if-ne p1, v6, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->compleDate()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-void

    :cond_0
    invoke-static {p1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;->getErrorToastId(I)I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-ne p1, v6, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->compleDate()Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Lcom/mediatek/contacts/activities/EditSimContactActivity$4;

    invoke-direct {v2, p0, v0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$4;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/mediatek/contacts/activities/EditSimContactActivity$5;

    invoke-direct {v2, p0, v0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$5;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private showSaveFailToast()V
    .locals 1

    new-instance v0, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$6;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private trimAnr(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    move-object v0, p1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "EditSimContactActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[run] befor replaceall additional_number : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EditSimContactActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[run] after replaceall additional_number : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method private updateFailToastText(I)Z
    .locals 2
    .param p1    # I

    const/4 v1, -0x1

    const/4 v0, 0x1

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    if-ne p1, v1, :cond_1

    const v1, 0x7f0c004e

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    :cond_0
    :goto_0
    iget v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    if-ltz v1, :cond_8

    new-instance v1, Lcom/mediatek/contacts/activities/EditSimContactActivity$3;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity$3;-><init>(Lcom/mediatek/contacts/activities/EditSimContactActivity;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_1
    return v0

    :cond_1
    const/4 v1, -0x2

    if-ne p1, v1, :cond_2

    const v1, 0x7f0c004c

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto :goto_0

    :cond_2
    const/4 v1, -0x3

    if-ne p1, v1, :cond_3

    const v1, 0x7f0c004d

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_3
    const/4 v1, -0x6

    if-ne p1, v1, :cond_4

    const v1, 0x7f0c004b

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto :goto_0

    :cond_4
    const/16 v1, -0xa

    if-ne p1, v1, :cond_5

    const v1, 0x7f0c004f

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_5
    const/16 v1, -0xb

    if-ne p1, v1, :cond_6

    const v1, 0x7f0c00df

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    goto :goto_0

    :cond_6
    const/16 v1, -0xc

    if-ne p1, v1, :cond_7

    const v1, 0x7f0c00ab

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto :goto_0

    :cond_7
    const/16 v1, -0xd

    if-ne p1, v1, :cond_0

    const v1, 0x7f0c0050

    iput v1, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveFailToastStrId:I

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public backToFragment()V
    .locals 4

    const/4 v3, 0x0

    const-string v1, "EditSimContactActivity"

    const-string v2, "[backToFragment]"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "simData1"

    iget-object v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "mQuitEdit"

    iget-boolean v2, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mQuitEdit:Z

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public compleDate()Z
    .locals 8

    const/4 v2, 0x0

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x1

    :cond_0
    :goto_0
    const/4 v4, 0x0

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v4, 0x1

    :cond_1
    :goto_1
    const/4 v0, 0x0

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x1

    :cond_2
    :goto_2
    const/4 v3, 0x0

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v3, 0x1

    :cond_3
    :goto_3
    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    invoke-virtual {v5, v6}, Ljava/util/AbstractMap;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_4
    const-string v5, "EditSimContactActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[showResultToastText]compleName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | complePhone : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | compleOther : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | compleEmail: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | compleGroup : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "EditSimContactActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[showResultToastText] sName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | mOldName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | sEmail : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | mOldEmail : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_a

    if-eqz v4, :cond_a

    if-eqz v3, :cond_a

    if-eqz v0, :cond_a

    if-eqz v1, :cond_a

    const/4 v5, 0x1

    :goto_5
    return v5

    :cond_5
    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_6
    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v4, 0x1

    goto/16 :goto_1

    :cond_7
    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_8
    sget-object v5, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v3, 0x1

    goto/16 :goto_3

    :cond_9
    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    if-nez v5, :cond_4

    const/4 v1, 0x1

    goto/16 :goto_4

    :cond_a
    const/4 v5, 0x0

    goto :goto_5
.end method

.method public fixIntent()Z
    .locals 28

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "the fixintent resolver = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mLookupUri:Landroid/net/Uri;

    move-object/from16 v23, v0

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "uri is "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v17

    const-string v24, "com.android.contacts"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    const-string v24, "vnd.android.cursor.item/contact"

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    invoke-static/range {v23 .. v23}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    move-object/from16 v0, v21

    invoke-static {v0, v6, v7}, Lcom/mediatek/contacts/SubContactsUtils;->queryForRawContactId(Landroid/content/ContentResolver;J)J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mRawContactId:J

    :cond_0
    :goto_0
    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "mRawContactId IS "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mRawContactId:J

    move-wide/from16 v26, v0

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mRawContactId:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x1

    cmp-long v24, v24, v26

    if-gez v24, :cond_2

    const-string v24, "EditSimContactActivity"

    const-string v25, "the mRawContactId is wrong"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v24, 0x7f0c0040

    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/widget/Toast;->show()V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    const/16 v24, 0x0

    :goto_1
    return v24

    :cond_1
    const-string v24, "vnd.android.cursor.item/raw_contact"

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    invoke-static/range {v23 .. v23}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndicate:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v20

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[fixIntent] oldcount:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v0, v0, [J

    move-object/from16 v19, v0

    const/16 v16, 0x0

    const/4 v14, 0x0

    :goto_2
    move/from16 v0, v20

    if-ge v14, v0, :cond_b

    const-string v25, "EditSimContactActivity"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "mSimOldData.get(0).getContentValues().get(i).getAsString(Data.MIMETYPE)   "

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v27, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v25, "vnd.android.cursor.item/name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v26, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    :cond_3
    :goto_3
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    :cond_4
    const-string v25, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v26, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string v26, "ExtensionForAAS"

    invoke-virtual/range {v24 .. v26}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureAccount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/ContentValues;

    invoke-static {v8}, Lcom/mediatek/contacts/extension/aassne/SimUtils;->isAdditionalNumber(Landroid/content/ContentValues;)Z

    move-result v24

    if-eqz v24, :cond_5

    new-instance v3, Lcom/android/contacts/ext/Anr;

    invoke-direct {v3}, Lcom/android/contacts/ext/Anr;-><init>()V

    const-string v24, "data1"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->trimAnr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    iput-object v0, v3, Lcom/android/contacts/ext/Anr;->mAdditionNumber:Ljava/lang/String;

    const-string v24, "data3"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    iput-object v0, v3, Lcom/android/contacts/ext/Anr;->mAasIndex:Ljava/lang/String;

    const-string v24, "_id"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    iput-wide v0, v3, Lcom/android/contacts/ext/Anr;->mId:J

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldAnrsList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_5
    const-string v24, "data1"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    goto/16 :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data2"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "7"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    goto/16 :goto_3

    :cond_8
    const-string v25, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v26, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    goto/16 :goto_3

    :cond_9
    const-string v25, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v26, "mimetype"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    aput-wide v24, v19, v16

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "mimetype"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/mediatek/contacts/extension/aassne/SneExt;->isNickname(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v24 .. v24}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/content/ContentValues;

    const-string v25, "data1"

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldNickname:Ljava/lang/String;

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "mOldNickname="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldNickname:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_b
    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "the mOldName is : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldName:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "   mOldOtherPhone : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldOtherPhone:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "  mOldPhone:  "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldPhone:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " mOldEmail : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldEmail:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[fixIntent] the mIndicate : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndicate:J

    move-wide/from16 v26, v0

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " | the mSlotId : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, "USIM Account"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v11, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v10, v0, [J

    move-object/from16 v0, v19

    array-length v5, v0

    const-string v24, "EditSimContactActivity"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "bufferGroupNum : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v24, "groupName"

    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const-string v24, "groupId"

    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v10

    const/4 v14, 0x0

    :goto_4
    if-ge v14, v5, :cond_e

    const/4 v9, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_d

    aget-wide v24, v19, v14

    aget-wide v26, v10, v9

    cmp-long v24, v24, v26

    if-nez v24, :cond_c

    aget-object v22, v11, v9

    aget-wide v12, v19, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOldGroupAddList:Ljava/util/HashMap;

    move-object/from16 v24, v0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_c
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_d
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    :cond_e
    const/16 v24, 0x1

    goto/16 :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOnBackGoing:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mOnBackGoing:Z

    const-string v0, "EditSimContactActivity"

    const-string v1, "[onBackPressed]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 30
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v18

    const-string v26, "simData"

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    const-string v26, "simOldData"

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimOldData:Ljava/util/ArrayList;

    const-string v26, "slotId"

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getNonSlotId()I

    move-result v27

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    const-string v26, "indicate_phone_or_sim_contact"

    const-wide/16 v27, -0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    move-wide/from16 v2, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v26

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndicate:J

    const-string v26, "simIndex"

    const/16 v27, -0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v26

    move/from16 v0, v26

    int-to-long v0, v0

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndexInSim:J

    const-string v26, "simSaveMode"

    const/16 v27, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mLookupUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getValues()Lcom/android/contacts/model/RawContactDelta$ValuesDelta;

    move-result-object v26

    const-string v27, "account_type"

    invoke-virtual/range {v26 .. v27}, Lcom/android/contacts/model/RawContactDelta$ValuesDelta;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v26, v0

    const-string v27, "USIM Account"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    const-string v26, "groupNum"

    const/16 v27, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "groupNum : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getValues()Lcom/android/contacts/model/RawContactDelta$ValuesDelta;

    move-result-object v26

    const-string v27, "account_name"

    invoke-virtual/range {v26 .. v27}, Lcom/android/contacts/model/RawContactDelta$ValuesDelta;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountName:Ljava/lang/String;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    new-instance v26, Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountName:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v26 .. v28}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccount:Landroid/accounts/Account;

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "the mSlotId is ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the mIndicate is ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndicate:J

    move-wide/from16 v28, v0

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the mSaveMode = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the accounttype is = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the uri is  = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mLookupUri:Landroid/net/Uri;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " | mIndexInSim : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mIndexInSim:J

    move-wide/from16 v28, v0

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mPhbReady:Z

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v5, v0, [Ljava/lang/String;

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v8, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v26, v0

    move/from16 v0, v26

    new-array v6, v0, [J

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v9

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "onCreate count:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v17, 0x0

    :goto_0
    move/from16 v0, v17

    if-ge v0, v9, :cond_c

    const-string v27, "vnd.android.cursor.item/name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v28, "mimetype"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v27, "data1"

    invoke-virtual/range {v26 .. v27}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    :cond_1
    :goto_1
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    :cond_3
    :goto_2
    return-void

    :cond_4
    const-string v27, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v28, "mimetype"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "ExtensionForAAS"

    invoke-virtual/range {v26 .. v28}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureAccount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/ContentValues;

    invoke-static {v10}, Lcom/mediatek/contacts/extension/aassne/SimUtils;->isAdditionalNumber(Landroid/content/ContentValues;)Z

    move-result v26

    if-eqz v26, :cond_5

    new-instance v4, Lcom/android/contacts/ext/Anr;

    invoke-direct {v4}, Lcom/android/contacts/ext/Anr;-><init>()V

    const-string v26, "data1"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->trimAnr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    iput-object v0, v4, Lcom/android/contacts/ext/Anr;->mAdditionNumber:Ljava/lang/String;

    const-string v26, "data3"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    iput-object v0, v4, Lcom/android/contacts/ext/Anr;->mAasIndex:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAnrsList:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v26, 0x0

    const-string v27, "data1"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v5, v26

    goto/16 :goto_1

    :cond_5
    const-string v26, "data1"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v8, v22

    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_1

    :cond_6
    const-string v27, "7"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v28, "data2"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v27, "data1"

    invoke-virtual/range {v26 .. v27}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v5, v20

    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v27, "data1"

    invoke-virtual/range {v26 .. v27}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v8, v22

    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_1

    :cond_8
    const-string v27, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v28, "mimetype"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v27, "data1"

    invoke-virtual/range {v26 .. v27}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    goto/16 :goto_1

    :cond_9
    const-string v27, "vnd.android.cursor.item/group_membership"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v28, "mimetype"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v27, "data1"

    invoke-virtual/range {v26 .. v27}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    aput-wide v26, v6, v21

    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v27, "mimetype"

    invoke-virtual/range {v26 .. v27}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/mediatek/contacts/extension/aassne/SneExt;->isNickname(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimData:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/android/contacts/model/RawContactDelta;

    invoke-virtual/range {v26 .. v26}, Lcom/android/contacts/model/RawContactDelta;->getContentValues()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/content/ContentValues;

    const-string v27, "data1"

    invoke-virtual/range {v26 .. v27}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sNickname:Ljava/lang/String;

    sget-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sNickname:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_b

    const-string v26, ""

    :goto_3
    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sNickname:Ljava/lang/String;

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "sNickname:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sNickname:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_b
    sget-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sNickname:Ljava/lang/String;

    goto :goto_3

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mAccountType:Ljava/lang/String;

    move-object/from16 v26, v0

    const-string v27, "USIM Account"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v26, v0

    move/from16 v0, v26

    new-array v13, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v26, v0

    move/from16 v0, v26

    new-array v12, v0, [J

    array-length v7, v6

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "bufferGroupNum : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v26, "groupName"

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    const-string v26, "groupId"

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v12

    const/16 v17, 0x0

    :goto_4
    move/from16 v0, v17

    if-ge v0, v7, :cond_f

    const/4 v11, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupNum:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v11, v0, :cond_e

    aget-wide v26, v6, v17

    aget-wide v28, v12, v11

    cmp-long v26, v26, v28

    if-nez v26, :cond_d

    aget-object v24, v13, v11

    aget-wide v14, v6, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mGroupAddList:Ljava/util/HashMap;

    move-object/from16 v26, v0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    :cond_e
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    :cond_f
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/contacts/ExtensionManager;->getContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;

    move-result-object v26

    const-string v27, "ExtensionForAAS"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v5, v8, v1}, Lcom/android/contacts/ext/ContactDetailExtension;->isDoublePhoneNumber([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_13

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    move-result v26

    if-nez v26, :cond_3

    :cond_10
    :goto_6
    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "the sName is = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the sPhone is ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the buffer[] is "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const/16 v28, 0x0

    aget-object v28, v5, v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the sOtherPhone is = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "the email is ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimUsimType(I)Z

    move-result v26

    if-eqz v26, :cond_11

    const-string v26, "USIM"

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSimType:Ljava/lang/String;

    :cond_11
    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "initial phone number "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterPhone:Ljava/lang/String;

    sget-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_16

    sget-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterPhone:Ljava/lang/String;

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "*********** after split phone number "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterPhone:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ",check valid:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterPhone:Ljava/lang/String;

    invoke-static/range {v28 .. v28}, Lcom/mediatek/telephony/PhoneNumberUtilsEx;->extractCLIRPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v26, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    sget-object v27, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterPhone:Ljava/lang/String;

    invoke-static/range {v27 .. v27}, Lcom/mediatek/telephony/PhoneNumberUtilsEx;->extractCLIRPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_12

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    move-result v26

    if-eqz v26, :cond_16

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_2

    :cond_13
    const/16 v26, 0x1

    aget-object v26, v5, v26

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_14

    const/16 v26, 0x1

    aget-object v26, v8, v26

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_15

    :cond_14
    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mDoublePhoneNumber:Z

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    move-result v26

    if-eqz v26, :cond_10

    goto/16 :goto_2

    :cond_15
    const/16 v26, 0x0

    aget-object v26, v5, v26

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    const/16 v26, 0x0

    aget-object v26, v8, v26

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    goto/16 :goto_6

    :cond_16
    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "initial sOtherPhone number "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterOtherPhone:Ljava/lang/String;

    sget-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_18

    sget-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterOtherPhone:Ljava/lang/String;

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "*********** after split sOtherPhone number "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterOtherPhone:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ",check valid:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterOtherPhone:Ljava/lang/String;

    invoke-static/range {v28 .. v28}, Lcom/mediatek/telephony/PhoneNumberUtilsEx;->extractCLIRPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v26, "[+]?[[0-9][*#pw,;]]+[[0-9][*#pw,;]]*"

    sget-object v27, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sAfterOtherPhone:Ljava/lang/String;

    invoke-static/range {v27 .. v27}, Lcom/mediatek/telephony/PhoneNumberUtilsEx;->extractCLIRPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_17

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mNumberInvalid:Z

    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->setSaveFailToastText()Z

    move-result v26

    if-eqz v26, :cond_18

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_2

    :cond_18
    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "initial name is  "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_19

    const/16 v26, 0x2

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mMode:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mLookupUri:Landroid/net/Uri;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->fixIntent()Z

    move-result v19

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "isGoing : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v19, :cond_3

    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->isServiceRunning(I)Z

    move-result v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSlotId:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v23

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "[onCreate] serviceState : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " | hasImported : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v16, :cond_1b

    const v26, 0x7f0c004a

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/widget/Toast;->show()V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_2

    :cond_1a
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_2

    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveMode:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/activities/EditSimContactActivity;->doSaveAction(I)V

    const-string v26, "EditSimContactActivity"

    const-string v27, "StructuredName.CONTENT_ITEM_TYPE = vnd.android.cursor.item/name"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v26, "EditSimContactActivity"

    const-string v27, "Phone.CONTENT_ITEM_TYPE = vnd.android.cursor.item/phone_v2"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v26, "EditSimContactActivity"

    const-string v27, "Email.CONTENT_ITEM_TYPE = vnd.android.cursor.item/email_v2"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v26, "EditSimContactActivity"

    const-string v27, "GroupMembership.CONTENT_ITEM_TYPE = vnd.android.cursor.item/group_membership"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v26, "EditSimContactActivity"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "the sName is = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sName:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the sPhone is ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sPhone:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the buffer[] is "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const/16 v28, 0x0

    aget-object v28, v5, v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " the sOtherPhone is = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sOtherPhone:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "the email is ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    sget-object v28, Lcom/mediatek/contacts/activities/EditSimContactActivity;->sEmail:Ljava/lang/String;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveContactHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/activities/EditSimContactActivity;->mSaveContactHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public startViewActivity(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method
