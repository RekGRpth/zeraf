.class public Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;
.super Landroid/app/Activity;
.source "CallLogMultipleDeleteActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CallLogMultipleDeleteActivity"


# instance fields
.field protected mFragment:Lcom/mediatek/contacts/calllog/CallLogMultipleDeleteFragment;

.field private mIsSelectedAll:Z

.field private mIsSelectedNone:Z

.field private mSelectionMenu:Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

.field public mStatusBarMgr:Landroid/app/StatusBarManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mIsSelectedAll:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mIsSelectedNone:Z

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mSelectionMenu:Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->configureActionBar()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->showDeleteDialog()V

    return-void
.end method

.method private configureActionBar()V
    .locals 10

    const-string v8, "configureActionBar()"

    invoke-direct {p0, v8}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->log(Ljava/lang/String;)V

    const-string v8, "layout_inflater"

    invoke-virtual {p0, v8}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    const v8, 0x7f040016

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v8, 0x7f070099

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    new-instance v8, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$1;

    invoke-direct {v8, p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$1;-><init>(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f07009a

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f02003b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v8, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$2;

    invoke-direct {v8, p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$2;-><init>(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f070097

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "Cancel"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    new-instance v8, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$3;

    invoke-direct {v8, p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$3;-><init>(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V

    invoke-virtual {v2, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f070095

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iget-boolean v8, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mIsSelectedNone:Z

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    const v8, -0x777778

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->getClickListenerOfActionBarOKButton()Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v8, 0x10

    const/16 v9, 0x1a

    invoke-virtual {v0, v8, v9}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    :cond_1
    return-void

    :cond_2
    const/4 v8, 0x1

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    const/4 v8, -0x1

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallLogMultipleDeleteActivity"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private showDeleteDialog()V
    .locals 5

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0c00a6

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1010355

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c00a7

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    new-instance v4, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$4;

    invoke-direct {v4, p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$4;-><init>(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method


# virtual methods
.method protected getClickListenerOfActionBarOKButton()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$8;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$8;-><init>(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "onCreate()"

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->log(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040015

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070073

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/calllog/CallLogMultipleDeleteFragment;

    iput-object v0, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mFragment:Lcom/mediatek/contacts/calllog/CallLogMultipleDeleteFragment;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->configureActionBar()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->updateSelectedItemsView(I)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-static {}, Lcom/mediatek/contacts/util/SetIndicatorUtils;->getInstance()Lcom/mediatek/contacts/util/SetIndicatorUtils;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lcom/mediatek/contacts/util/SetIndicatorUtils;->showIndicator(ZLandroid/app/Activity;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-static {}, Lcom/mediatek/contacts/util/SetIndicatorUtils;->getInstance()Lcom/mediatek/contacts/util/SetIndicatorUtils;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/mediatek/contacts/util/SetIndicatorUtils;->showIndicator(ZLandroid/app/Activity;)V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected onStopForSubClass()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public updateSelectedItemsView(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f07009a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-nez v1, :cond_0

    const-string v2, "Load view resource error!"

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const v2, 0x7f0c000e

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f070095

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-nez p1, :cond_1

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    const v2, -0x777778

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public updateSelectionMenu(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const v5, 0x7f07009a

    new-instance v0, Lcom/mediatek/contacts/list/DropMenu;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/list/DropMenu;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const v4, 0x7f100010

    invoke-virtual {v0, v3, v4}, Lcom/mediatek/contacts/list/DropMenu;->addDropDownMenu(Landroid/widget/Button;I)Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mSelectionMenu:Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02003b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v3, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$5;

    invoke-direct {v3, p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$5;-><init>(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mSelectionMenu:Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    const v4, 0x7f0701ff

    invoke-virtual {v3, v4}, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mFragment:Lcom/mediatek/contacts/calllog/CallLogMultipleDeleteFragment;

    invoke-virtual {v3}, Lcom/mediatek/contacts/calllog/CallLogMultipleDeleteFragment;->isAllSelected()Z

    move-result v3

    iput-boolean v3, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mIsSelectedAll:Z

    iget-boolean v3, p0, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;->mIsSelectedAll:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const v3, 0x7f0c0261

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    new-instance v3, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$6;

    invoke-direct {v3, p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$6;-><init>(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V

    invoke-virtual {v0, v3}, Lcom/mediatek/contacts/list/DropMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const v3, 0x7f0c0260

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    new-instance v3, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$7;

    invoke-direct {v3, p0}, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity$7;-><init>(Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;)V

    invoke-virtual {v0, v3}, Lcom/mediatek/contacts/list/DropMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    goto :goto_0
.end method
