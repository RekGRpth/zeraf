.class public abstract Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
.super Landroid/app/Service;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimerTask;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;,
        Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;
    }
.end annotation


# static fields
.field public static final ACTION_PHB_LOAD_FINISHED:Ljava/lang/String; = "com.android.contacts.ACTION_PHB_LOAD_FINISHED"

.field protected static final ADDITIONAL_NUMBER_COLUMN:I = 0x4

.field private static final COLUMN_NAMES:[Ljava/lang/String;

.field private static final DBG:Z = true

.field protected static final EMAIL_COLUMN:I = 0x3

.field public static final FINISH_IMPORTING:I = 0x1f4

.field protected static final GROUP_COLUMN:I = 0x5

.field public static final IMPORT_NEW:I = 0x190

.field protected static final INDEX_COLUMN:I = 0x0

.field public static final LOAD_SIM_CONTACTS:I = 0xc8

.field private static final MAX_OP_COUNT_IN_ONE_BATCH:I = 0x5a

.field public static final MSG_DELAY_PROCESSING:I = 0x64

.field protected static final NAME_COLUMN:I = 0x1

.field protected static final NUMBER_COLUMN:I = 0x2

.field private static final REFRESH_INTERVAL_MS:J = 0x3e8L

.field private static final REFRESH_INTERVAL_MS_STEP:J = 0x12cL

.field public static final REMOVE_OLD:I = 0x12c

.field public static final SERVICE_DELETE_CONTACTS:I = 0x1

.field public static final SERVICE_IDLE:I = 0x0

.field public static final SERVICE_IMPORT_CONTACTS:I = 0x3

.field public static final SERVICE_QUERY_SIM:I = 0x2

.field public static final SERVICE_SLOT_KEY:Ljava/lang/String; = "which_slot"

.field public static final SERVICE_WORK_IMPORT:I = 0x1

.field public static final SERVICE_WORK_NONE:I = 0x0

.field public static final SERVICE_WORK_REMOVE:I = 0x2

.field public static final SERVICE_WORK_TYPE:Ljava/lang/String; = "work_type"

.field public static final SERVICE_WORK_UNKNOWN:I = 0x3

.field public static final SIM_TYPE_SIM:I = 0x0

.field public static final SIM_TYPE_UIM:I = 0x2

.field public static final SIM_TYPE_UNKNOWN:I = -0x1

.field public static final SIM_TYPE_USIM:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AbstractStartSIMService"

.field private static sServiceStateArray:Landroid/util/SparseIntArray;


# instance fields
.field private mGrpIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

.field private final mRefreshTimerArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;",
            ">;"
        }
    .end annotation
.end field

.field private mSimWorkQueueArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    new-instance v2, Landroid/util/SparseIntArray;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getSlotCount()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceStateArray:Landroid/util/SparseIntArray;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getAllSlotIds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceStateArray:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "index"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "number"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "emails"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "additionalNumber"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "groupIds"

    aput-object v4, v2, v3

    sput-object v2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->COLUMN_NAMES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mGrpIdMap:Ljava/util/HashMap;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getSlotCount()I

    move-result v1

    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueueArray:Landroid/util/SparseArray;

    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v3, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimerArray:Landroid/util/SparseArray;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getAllSlotIds()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueueArray:Landroid/util/SparseArray;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimerArray:Landroid/util/SparseArray;

    new-instance v4, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    invoke-direct {v4, p0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;I)V

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sendLoadSimMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Landroid/util/SparseArray;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueueArray:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->COLUMN_NAMES:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mGrpIdMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Landroid/database/Cursor;Landroid/content/ContentResolver;IJILjava/util/HashSet;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Landroid/database/Cursor;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # I
    .param p4    # J
    .param p6    # I
    .param p7    # Ljava/util/HashSet;
    .param p8    # Z

    invoke-direct/range {p0 .. p8}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->actuallyImportOneSimContact(Landroid/database/Cursor;Landroid/content/ContentResolver;IJILjava/util/HashSet;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iget-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sendRemoveSimContactsMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->processDelayMessage(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method static synthetic access$800(II)V
    .locals 0
    .param p0    # I
    .param p1    # I

    invoke-static {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->setServiceRunningState(II)V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->query(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method private actuallyImportOneSimContact(Landroid/database/Cursor;Landroid/content/ContentResolver;IJILjava/util/HashSet;Z)V
    .locals 48
    .param p1    # Landroid/database/Cursor;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # I
    .param p4    # J
    .param p6    # I
    .param p8    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Landroid/content/ContentResolver;",
            "IJI",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    invoke-static/range {p0 .. p0}, Lcom/android/contacts/model/AccountTypeManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/model/AccountTypeManager;

    move-result-object v18

    const/4 v2, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/android/contacts/model/AccountTypeManager;->getAccounts(Z)Ljava/util/List;

    move-result-object v33

    const/4 v2, 0x1

    move/from16 v0, p6

    if-ne v0, v2, :cond_8

    const/16 v31, 0x1

    :goto_0
    const/4 v15, -0x1

    const/4 v11, 0x0

    invoke-interface/range {v33 .. v33}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_0
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/contacts/model/account/AccountWithDataSet;

    instance-of v2, v12, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-eqz v2, :cond_0

    move-object v13, v12

    check-cast v13, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v13}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v15

    move/from16 v0, p3

    if-ne v15, v0, :cond_0

    iget-object v2, v13, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v5, "USIM Account"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v14, 0x1

    :goto_1
    iget-object v2, v13, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v5, "UIM Account"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v14, 0x2

    :cond_1
    move/from16 v0, p6

    if-ne v14, v0, :cond_2

    move-object v11, v13

    :cond_2
    if-nez v11, :cond_3

    const-string v2, "AbstractStartSIMService"

    const-string v5, "account == null"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v39, Ljava/util/ArrayList;

    invoke-direct/range {v39 .. v39}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    const/16 v16, 0x0

    if-eqz p1, :cond_1c

    const/4 v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static/range {p0 .. p0}, Lcom/android/contacts/ContactsUtils;->getCurrentCountryIso(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v20

    const-string v2, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[actuallyImportOneSimContact] countryCode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v30, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SLOT"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "||indexInSim:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v28

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "||isInserted:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p7, :cond_a

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-eqz p7, :cond_5

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_5
    new-instance v37, Lcom/mediatek/contacts/SubContactsUtils$NamePhoneTypePair;

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/SubContactsUtils$NamePhoneTypePair;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v37

    iget-object v0, v0, Lcom/mediatek/contacts/SubContactsUtils$NamePhoneTypePair;->name:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, v37

    iget v0, v0, Lcom/mediatek/contacts/SubContactsUtils$NamePhoneTypePair;->phoneType:I

    move/from16 v41, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "phoneType is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v41

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    move-object/from16 v0, v37

    iget-object v0, v0, Lcom/mediatek/contacts/SubContactsUtils$NamePhoneTypePair;->phoneTypeSuffix:Ljava/lang/String;

    move-object/from16 v42, v0

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "indexInSim = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v28

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", intIndexInSim = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", name = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", number = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    const/16 v32, 0x0

    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v47, Landroid/content/ContentValues;

    invoke-direct/range {v47 .. v47}, Landroid/content/ContentValues;-><init>()V

    const/4 v3, 0x0

    if-eqz v11, :cond_6

    iget-object v3, v11, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v2, "account_name"

    iget-object v5, v11, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v47

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "account_type"

    iget-object v5, v11, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v0, v47

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string v2, "indicate_phone_or_sim_contact"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v47

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "aggregation_mode"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v47

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "index_in_sim"

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v47

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz p8, :cond_7

    const-string v2, "is_sdn_contact"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v47

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_7
    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v32, v32, 0x1

    invoke-static/range {v40 .. v40}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string v2, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[actuallyImportOneSimContact] phoneNumber before : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v40

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getAsYouTypeFormatter(Ljava/lang/String;)Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    move-result-object v35

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->toCharArray()[C

    move-result-object v19

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v27, v0

    const/16 v38, 0x0

    :goto_4
    move/from16 v0, v38

    move/from16 v1, v27

    if-ge v0, v1, :cond_b

    aget-char v2, v19, v38

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->inputDigit(C)Ljava/lang/String;

    move-result-object v40

    add-int/lit8 v38, v38, 0x1

    goto :goto_4

    :cond_8
    const/16 v31, 0x0

    goto/16 :goto_0

    :cond_9
    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_a
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/16 :goto_3

    :cond_b
    const-string v2, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[actuallyImportOneSimContact] phoneNumber after : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v40

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v2, "raw_contact_id"

    invoke-virtual {v4, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    const/4 v6, 0x2

    const-string v7, "ExtensionForAAS"

    move-object/from16 v5, p1

    invoke-virtual/range {v2 .. v7}, Lcom/android/contacts/ext/ContactAccountExtension;->checkOperationBuilder(Ljava/lang/String;Landroid/content/ContentProviderOperation$Builder;Landroid/database/Cursor;ILjava/lang/String;)Z

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v2

    const-string v5, "ExtensionForOP01"

    move-object/from16 v0, v40

    invoke-virtual {v2, v0, v5}, Lcom/android/contacts/ext/ContactListExtension;->getReplaceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    const-string v2, "data1"

    move-object/from16 v0, v40

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-static/range {v42 .. v42}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    const-string v2, "data15"

    move-object/from16 v0, v42

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_c
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v32, v32, 0x1

    :cond_d
    invoke-static/range {v36 .. v36}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v2, "raw_contact_id"

    invoke-virtual {v4, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v5, "vnd.android.cursor.item/name"

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    move-object/from16 v0, v36

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v32, v32, 0x1

    :cond_e
    if-eqz v31, :cond_19

    const-string v2, "[actuallyImportOneSimContact]import a USIM contact."

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[actuallyImportOneSimContact]emailAddresses:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string v2, ","

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    move-object/from16 v17, v23

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v34, v0

    const/16 v26, 0x0

    :goto_5
    move/from16 v0, v26

    move/from16 v1, v34

    if-ge v0, v1, :cond_10

    aget-object v22, v17, v26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "emailAddress IS "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    const-string v2, "null"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v2, "raw_contact_id"

    invoke-virtual {v4, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data2"

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    move-object/from16 v0, v22

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v32, v32, 0x1

    :cond_f
    add-int/lit8 v26, v26, 0x1

    goto :goto_5

    :cond_10
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[actuallyImportOneSimContact]additionalNumber:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_13

    const-string v2, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[actuallyImportOneSimContact] additionalNumber before : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getAsYouTypeFormatter(Ljava/lang/String;)Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    move-result-object v35

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toCharArray()[C

    move-result-object v19

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v27, v0

    const/16 v38, 0x0

    :goto_6
    move/from16 v0, v38

    move/from16 v1, v27

    if-ge v0, v1, :cond_11

    aget-char v2, v19, v38

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->inputDigit(C)Ljava/lang/String;

    move-result-object v16

    add-int/lit8 v38, v38, 0x1

    goto :goto_6

    :cond_11
    const-string v2, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[actuallyImportOneSimContact] additionalNumber after : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "additionalNumber is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v2, "raw_contact_id"

    invoke-virtual {v4, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "mimetype"

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    const/4 v6, 0x0

    const-string v7, "ExtensionForAAS"

    move-object/from16 v5, p1

    invoke-virtual/range {v2 .. v7}, Lcom/android/contacts/ext/ContactAccountExtension;->checkOperationBuilder(Ljava/lang/String;Landroid/content/ContentProviderOperation$Builder;Landroid/database/Cursor;ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_12

    const-string v2, "data2"

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :cond_12
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v2

    const-string v5, "ExtensionForOP01"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v5}, Lcom/android/contacts/ext/ContactListExtension;->getReplaceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-string v2, "data1"

    move-object/from16 v0, v16

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "is_additional_number"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v32, v32, 0x1

    :cond_13
    invoke-static {v15}, Lcom/mediatek/contacts/extension/aassne/SneExt;->hasSne(I)Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v5

    const-string v10, "ExtensionForSNE"

    move-object v6, v3

    move-object/from16 v7, v39

    move-object/from16 v8, p1

    invoke-virtual/range {v5 .. v10}, Lcom/android/contacts/ext/ContactAccountExtension;->buildOperationFromCursor(Ljava/lang/String;Ljava/util/ArrayList;Landroid/database/Cursor;ILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    add-int/lit8 v32, v32, 0x1

    :cond_14
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v46

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[actuallyImportOneSimContact]sim group id string: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v46

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static/range {v46 .. v46}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_19

    const/16 v44, 0x0

    invoke-static/range {v46 .. v46}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    const-string v2, ","

    move-object/from16 v0, v46

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v44

    :cond_15
    move-object/from16 v17, v44

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v34, v0

    const/16 v26, 0x0

    :goto_7
    move/from16 v0, v26

    move/from16 v1, v34

    if-ge v0, v1, :cond_19

    aget-object v45, v17, v26

    const/16 v43, -0x1

    :try_start_0
    invoke-static/range {v45 .. v45}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_16

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v43

    :cond_16
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[USIM Group] sim group id ugrpId: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-lez v43, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mGrpIdMap:Ljava/util/HashMap;

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Integer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[USIM Group]simgroup mapping group grpId: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-nez v25, :cond_18

    const-string v2, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[USIM Group] Error. Catch unhandled SIM group error. ugrp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v43

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_17
    :goto_8
    add-int/lit8 v26, v26, 0x1

    goto :goto_7

    :catch_0
    move-exception v21

    const-string v2, "[USIM Group] catched exception"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_8

    :cond_18
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v2, "mimetype"

    const-string v5, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "data1"

    move-object/from16 v0, v25

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v2, "raw_contact_id"

    invoke-virtual {v4, v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v39

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v32, v32, 0x1

    goto :goto_8

    :cond_19
    add-int v9, v9, v32

    const/16 v2, 0x5a

    if-le v9, v2, :cond_4

    :try_start_1
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v2

    if-nez v2, :cond_1d

    const-string v2, "check sim State: false"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1a
    :try_start_2
    const-string v2, "Before applyBatch "

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v2

    if-eqz v2, :cond_1b

    const-string v2, "check sim State: true"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1b

    const-string v2, "com.android.contacts"

    move-object/from16 v0, p2

    move-object/from16 v1, v39

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    :cond_1b
    const-string v2, "After applyBatch "

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_4

    :cond_1c
    :goto_9
    return-void

    :cond_1d
    :try_start_3
    const-string v2, "Before applyBatch. "

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    const-string v2, "com.android.contacts"

    move-object/from16 v0, p2

    move-object/from16 v1, v39

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    const-string v2, "After applyBatch "

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_a
    const/4 v9, 0x0

    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_2

    :catch_1
    move-exception v21

    const-string v2, "AbstractStartSIMService"

    const-string v5, "%s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :catch_2
    move-exception v21

    const-string v2, "AbstractStartSIMService"

    const-string v5, "%s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :catch_3
    move-exception v21

    const-string v2, "AbstractStartSIMService"

    const-string v5, "%s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    :catch_4
    move-exception v21

    const-string v2, "AbstractStartSIMService"

    const-string v5, "%s: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9
.end method

.method public static getServiceState(I)I
    .locals 1
    .param p0    # I

    sget-object v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceStateArray:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method public static isServiceRunning(I)Z
    .locals 1
    .param p0    # I

    sget-object v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceStateArray:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "AbstractStartSIMService"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private processDelayMessage(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 8
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iget-object v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimerArray:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    const-wide/16 v2, 0x3e8

    int-to-long v4, v0

    const-wide/16 v6, 0x12c

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->schedule(JLcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method

.method private query(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 6
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v0, 0x1

    iget v3, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    const/4 v5, -0x1

    if-ne v1, v5, :cond_0

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->getSimTypeBySlot(I)I

    move-result v1

    iput v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    :cond_0
    iget v4, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[query]slotId: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " ||isUSIM:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-ne v4, v0, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/SimCardUtils$SimUri;->getSimUri(I)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[query]uri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$2;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Landroid/net/Uri;IILcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendLoadSimMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[sendLoadSimMsg]slotId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    const/16 v3, 0xc8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget v2, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput v2, v0, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method private sendRemoveSimContactsMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[sendRemoveSimContactsMsg]slotId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput v1, v0, Landroid/os/Message;->arg2:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mRefreshTimerArray:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;

    invoke-virtual {v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$RefreshTimer;->clear()V

    return-void
.end method

.method private static setServiceRunningState(II)V
    .locals 1
    .param p0    # I
    .param p1    # I

    sget-object v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sServiceStateArray:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p0, p1}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method


# virtual methods
.method checkPhoneBookState(I)Z
    .locals 1
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v0

    return v0
.end method

.method checkSimState(I)Z
    .locals 9
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimPukRequest(I)Z

    move-result v5

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimPinRequest(I)Z

    move-result v4

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInserted(I)Z

    move-result v3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSetRadioOn(Landroid/content/ContentResolver;I)Z

    move-result v1

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isFdnEnabed(I)Z

    move-result v0

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInfoReady()Z

    move-result v2

    const-string v6, "AbstractStartSIMService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[checkSimState]slotId:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simPUKReq: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simPINReq: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isRadioOn: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isFdnEnabled: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simInserted: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isSimInfoReady:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v5, :cond_0

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    if-nez v4, :cond_0

    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    :cond_0
    const/4 v6, 0x0

    :goto_0
    return v6

    :cond_1
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public deleteSimContact(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 3
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v0, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;ILcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "onCreate()"

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "onDestroy()"

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v3, -0x1

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[onStart]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", startId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v3, "AbstractStartSIMService"

    const-string v4, "[onStart] intent is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :goto_0
    return-void

    :cond_0
    new-instance v4, Landroid/app/Notification;

    invoke-direct {v4}, Landroid/app/Notification;-><init>()V

    invoke-virtual {p0, v8, v4}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    if-nez v4, :cond_1

    new-instance v4, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    invoke-direct {v4, p0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)V

    iput-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    const-string v4, "AbstractStartSIMService"

    const-string v5, "[onStart]a new SimHandler created"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v4, "which_slot"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "work_type"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v4, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mSimWorkQueueArray:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[onStart]slotId: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "|workType:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v2, v8}, Lcom/mediatek/contacts/simcontact/SlotUtils;->updateSimServiceRunningStateForSlot(IZ)V

    new-instance v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v5, 0x0

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;-><init>(IIIILandroid/database/Cursor;I)V

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->sendTaskPoolMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onStartCommand() intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    if-nez p1, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->onStart(Landroid/content/Intent;I)V

    const/4 v0, 0x3

    goto :goto_0
.end method

.method public sendImportSimContactsMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[sendImportSimContactsMsg]slotId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    const/16 v3, 0x190

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput v1, v0, Landroid/os/Message;->arg2:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public sendTaskPoolMsg(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 4
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v1, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iget-object v2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->mHandler:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget v2, p1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    iput v2, v0, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
