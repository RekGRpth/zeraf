.class public Lcom/mediatek/contacts/ExtensionManager;
.super Ljava/lang/Object;
.source "ExtensionManager.java"


# static fields
.field public static final COMMD_FOR_AAS:Ljava/lang/String; = "ExtensionForAAS"

.field public static final COMMD_FOR_RCS:Ljava/lang/String; = "ExtenstionForRCS"

.field public static final COMMD_FOR_SNE:Ljava/lang/String; = "ExtensionForSNE"

.field public static final COMMD_FOR_SNS:Ljava/lang/String; = "ExtensionForSNS"

.field public static final RCS_CONTACT_PRESENCE_CHANGED:Ljava/lang/String; = "android.intent.action.RCS_CONTACT_PRESENCE_CHANGED"

.field private static final TAG:Ljava/lang/String; = "ExtensionManager"

.field private static sInstance:Lcom/mediatek/contacts/ExtensionManager;


# instance fields
.field private mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

.field private mHasPlugin:Z


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z

    new-instance v0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    iput-boolean v1, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z

    invoke-direct {p0}, Lcom/mediatek/contacts/ExtensionManager;->getPlugin()V

    return-void
.end method

.method public static getInstance()Lcom/mediatek/contacts/ExtensionManager;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->sInstance:Lcom/mediatek/contacts/ExtensionManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/ExtensionManager;

    invoke-direct {v0}, Lcom/mediatek/contacts/ExtensionManager;-><init>()V

    sput-object v0, Lcom/mediatek/contacts/ExtensionManager;->sInstance:Lcom/mediatek/contacts/ExtensionManager;

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/ExtensionManager;->sInstance:Lcom/mediatek/contacts/ExtensionManager;

    return-object v0
.end method

.method private getPlugin()V
    .locals 9

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v0

    const-string v6, "ExtensionManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getPlugin applicationContext : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v6, Lcom/android/contacts/ext/IContactPlugin;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [Landroid/content/pm/Signature;

    invoke-static {v0, v6, v7}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v5

    const-string v6, "ExtensionManager"

    const-string v7, "get pluginManager"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v4

    if-nez v4, :cond_1

    const-string v6, "ExtensionManager"

    const-string v7, "no plugin apk"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v6, "ExtensionManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "num : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_0

    :try_start_0
    invoke-virtual {v5, v3}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v7, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v1}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/contacts/ext/IContactPlugin;

    invoke-virtual {v7, v6}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->addExtensions(Lcom/android/contacts/ext/IContactPlugin;)V

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const-string v6, "ExtensionManager"

    const-string v7, "contactPlugin is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v2

    const-string v6, "ExtensionManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getPlugin is error "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public HasPlugin()Z
    .locals 3

    const-string v0, "ExtensionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "return mHasPlugin : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mHasPlugin:Z

    return v0
.end method

.method public getCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;

    move-result-object v0

    return-object v0
.end method

.method public getCallListExtension()Lcom/android/contacts/ext/CallListExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getCallListExtension()Lcom/android/contacts/ext/CallListExtension;

    move-result-object v0

    return-object v0
.end method

.method public getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v0

    return-object v0
.end method

.method public getContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;

    move-result-object v0

    return-object v0
.end method

.method public getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v0

    return-object v0
.end method

.method public getDialPadExtension()Lcom/android/contacts/ext/DialPadExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getDialPadExtension()Lcom/android/contacts/ext/DialPadExtension;

    move-result-object v0

    return-object v0
.end method

.method public getDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;

    move-result-object v0

    return-object v0
.end method

.method public getQuickContactExtension()Lcom/android/contacts/ext/QuickContactExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getQuickContactExtension()Lcom/android/contacts/ext/QuickContactExtension;

    move-result-object v0

    return-object v0
.end method

.method public getSimPickExtension()Lcom/android/contacts/ext/SimPickExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getSimPickExtension()Lcom/android/contacts/ext/SimPickExtension;

    move-result-object v0

    return-object v0
.end method

.method public getSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ExtensionManager;->mContactPluinContainer:Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->getSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;

    move-result-object v0

    return-object v0
.end method
