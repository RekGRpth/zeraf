.class Lcom/mediatek/contacts/ShareContactViaSDCard$CancelListener;
.super Ljava/lang/Object;
.source "ShareContactViaSDCard.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements Landroid/content/DialogInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/ShareContactViaSDCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CancelListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;


# direct methods
.method private constructor <init>(Lcom/mediatek/contacts/ShareContactViaSDCard;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$CancelListener;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/contacts/ShareContactViaSDCard;Lcom/mediatek/contacts/ShareContactViaSDCard$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/ShareContactViaSDCard;
    .param p2    # Lcom/mediatek/contacts/ShareContactViaSDCard$1;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/ShareContactViaSDCard$CancelListener;-><init>(Lcom/mediatek/contacts/ShareContactViaSDCard;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    return-void
.end method

.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$CancelListener;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$CancelListener;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$CancelListener;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/mediatek/contacts/ShareContactViaSDCard$CancelListener;->this$0:Lcom/mediatek/contacts/ShareContactViaSDCard;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x0

    return v0
.end method
