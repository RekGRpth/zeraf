.class Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;
.super Lcom/android/contacts/util/PhoneNumberFormatter$PhoneNumberFormattingTextWatcherEx;
.source "DialerSearchController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/dialpad/DialerSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DsTextWatcher"
.end annotation


# static fields
.field private static final DBG_INT:Z


# instance fields
.field private mChangeInMiddle:Z

.field private mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

.field private mFormatting:Z

.field private mSearchMode:I


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-direct {p0}, Lcom/android/contacts/util/PhoneNumberFormatter$PhoneNumberFormattingTextWatcherEx;-><init>()V

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    return-void
.end method

.method private logd(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method private startQuery(Ljava/lang/String;I)V
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x1

    const/4 v11, 0x0

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startQuery mIsFirstLaunched: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$300(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$300(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$400(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v4, v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    const v5, 0x7f0c027c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v0, v4, v11}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->showLoadingTips(Landroid/view/View;ZLjava/lang/String;Z)V

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startQuery searchContent: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mode: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/mediatek/contacts/dialpad/DialerSearchUtils;->tripHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget v1, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mNoResultDigCnt:I

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v1, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget v3, v3, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mNoResultDigCnt:I

    if-le v1, v3, :cond_2

    move v9, v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mNoResultDigCnt: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget v3, v3, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mNoResultDigCnt:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " || mDigits.getText(): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v3, v3, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    invoke-virtual {v0}, Lcom/android/contacts/preference/ContactsPreferences;->getDisplayOrder()I

    move-result v8

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    invoke-virtual {v0}, Lcom/android/contacts/preference/ContactsPreferences;->getSortOrder()I

    move-result v10

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/16 v1, 0x1e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://com.android.contacts/dialer_search/filter/init#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v9, v11

    goto :goto_0

    :cond_3
    const-string v1, "NULL_INPUT"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/16 v1, 0x28

    const-string v3, "content://com.android.contacts/dialer_search/filter/null_input"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    if-nez p2, :cond_5

    if-nez v9, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/16 v1, 0x32

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://com.android.contacts/dialer_search/filter/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/16 v1, 0x3c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://com.android.contacts/dialer_search_number/filter/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1    # Landroid/text/Editable;

    const/4 v3, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[afterTextChanged]mSelfChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "||text:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "||mFormatting:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mFormatting:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    sget-boolean v1, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$300(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-gtz v1, :cond_0

    :cond_2
    iget-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mFormatting:Z

    if-nez v1, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mFormatting:Z

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_4

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSearchMode:I

    invoke-direct {p0, v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->startQuery(Ljava/lang/String;I)V

    :cond_3
    :goto_1
    iput-boolean v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mFormatting:Z

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-virtual {v1, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->setSearchNumberMode(Z)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$200(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    invoke-direct {p0, v1, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->startQuery(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v1, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$202(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z

    goto :goto_1

    :cond_5
    const-string v1, "NULL_INPUT"

    invoke-direct {p0, v1, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->startQuery(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget-boolean v1, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[beforeTextChanged]mSelfChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[beforeTextChanged]s:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|start:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|after:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mChangeInMiddle:Z

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v5, 0x0

    const/4 v4, 0x1

    sget-boolean v2, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onTextChanged]mSelfChanged:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSelfChanged:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onTextChanged]s:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|start:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|before:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->logd(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mFormatting:Z

    if-nez v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-virtual {v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->isSearchNumberOnly()Z

    move-result v2

    if-nez v2, :cond_2

    if-gt p4, v4, :cond_2

    if-gt p3, v4, :cond_2

    if-ne p4, p3, :cond_1

    add-int/lit8 v2, v1, -0x1

    if-eq p2, v2, :cond_2

    :cond_1
    iget-boolean v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mChangeInMiddle:Z

    if-eqz v2, :cond_4

    :cond_2
    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mController:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-virtual {v2, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->setSearchNumberMode(Z)V

    iput v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSearchMode:I

    :cond_3
    :goto_1
    iput-boolean v5, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mChangeInMiddle:Z

    goto :goto_0

    :cond_4
    iput v5, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;->mSearchMode:I

    goto :goto_1
.end method
