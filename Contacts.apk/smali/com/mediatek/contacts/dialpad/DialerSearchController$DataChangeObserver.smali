.class Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;
.super Landroid/database/ContentObserver;
.source "DialerSearchController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/dialpad/DialerSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataChangeObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DataChangeObserver: mIsForeground:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v2}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$000(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|selfChange:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$000(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-static {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$100(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;->this$0:Lcom/mediatek/contacts/dialpad/DialerSearchController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->access$202(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z

    goto :goto_0
.end method
