.class public Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;
.super Ljava/lang/Object;
.source "AutoScaleTextSizeWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field private static final TAG:Ljava/lang/String; = "AutoScaleTextSizeWatcher"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mCurrentTextSize:I

.field protected mDeltaTextSize:I

.field protected mDigitsWidth:I

.field protected mDisplayMetrics:Landroid/util/DisplayMetrics;

.field protected mMaxTextSize:I

.field protected mMinTextSize:I

.field protected mPaint:Landroid/graphics/Paint;

.field protected mPreviousDigitsLength:I

.field protected mTarget:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;)V
    .locals 3
    .param p1    # Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mTarget:Landroid/widget/EditText;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mTarget:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1    # Landroid/text/Editable;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->autoScaleTextSize(Z)V

    return-void
.end method

.method protected autoScaleTextSize(Z)V
    .locals 13
    .param p1    # Z

    const/4 v12, 0x0

    iget-object v10, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mTarget:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_0

    iget v10, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mMaxTextSize:I

    iput v10, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mCurrentTextSize:I

    iget-object v10, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mTarget:Landroid/widget/EditText;

    iget v11, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mMaxTextSize:I

    int-to-float v11, v11

    invoke-virtual {v10, v12, v11}, Landroid/widget/TextView;->setTextSize(IF)V

    :goto_0
    return-void

    :cond_0
    iget v6, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mMaxTextSize:I

    iget v7, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mMinTextSize:I

    iget v3, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mDigitsWidth:I

    iget v1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mDeltaTextSize:I

    iget-object v8, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mPaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mCurrentTextSize:I

    move v9, v0

    int-to-float v10, v0

    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v5, v10

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "inputWidth = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " current = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " digits = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->log(Ljava/lang/String;)V

    if-nez p1, :cond_4

    :goto_1
    if-le v0, v7, :cond_1

    if-le v5, v3, :cond_1

    sub-int/2addr v0, v1

    if-ge v0, v7, :cond_2

    move v0, v7

    :cond_1
    :goto_2
    iput v0, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mCurrentTextSize:I

    iget-object v10, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mTarget:Landroid/widget/EditText;

    int-to-float v11, v0

    invoke-virtual {v10, v12, v11}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    :cond_2
    int-to-float v10, v0

    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v5, v10

    goto :goto_1

    :cond_3
    int-to-float v10, v0

    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v5, v10

    :cond_4
    if-ge v5, v3, :cond_5

    if-ge v0, v6, :cond_5

    move v9, v0

    add-int/2addr v0, v1

    if-le v0, v6, :cond_3

    move v0, v6

    :cond_5
    int-to-float v10, v0

    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v5, v10

    if-le v5, v3, :cond_1

    move v0, v9

    goto :goto_2
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "AutoScaleTextSizeWatcher"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public setAutoScaleParameters(IIII)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iput p2, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mMaxTextSize:I

    iput p1, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mMinTextSize:I

    iput p3, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mDeltaTextSize:I

    iput p4, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mDigitsWidth:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "digitsWidth = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->log(Ljava/lang/String;)V

    iget v0, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mMaxTextSize:I

    iput v0, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mCurrentTextSize:I

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mTarget:Landroid/widget/EditText;

    const/4 v1, 0x0

    iget v2, p0, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->mCurrentTextSize:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method

.method public trigger(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/AutoScaleTextSizeWatcher;->autoScaleTextSize(Z)V

    return-void
.end method
