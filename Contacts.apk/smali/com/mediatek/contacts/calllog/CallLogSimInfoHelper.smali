.class public Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;
.super Ljava/lang/Object;
.source "CallLogSimInfoHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$1;,
        Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CallLogSimInfoHelper"


# instance fields
.field private mDrawableSimLockedColor:Landroid/graphics/drawable/Drawable;

.field private mDrawableSimSipColor:Landroid/graphics/drawable/Drawable;

.field private mResources:Landroid/content/res/Resources;

.field private mSimColorArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;",
            ">;"
        }
    .end annotation
.end field

.field private mSipCallDisplayName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 5
    .param p1    # Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v2, ""

    iput-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSipCallDisplayName:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    new-instance v2, Landroid/util/SparseArray;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getSlotCount()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSimColorArray:Landroid/util/SparseArray;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getAllSlotIds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSimColorArray:Landroid/util/SparseArray;

    new-instance v3, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;-><init>(Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$1;)V

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static getSimIdBySlotID(I)I
    .locals 4
    .param p0    # I

    const/4 v3, -0x1

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/phone/SIMInfoWrapper;->getInsertedSimInfoList()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/Telephony$SIMInfo;

    iget v2, v2, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne p0, v2, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/Telephony$SIMInfo;

    iget-wide v2, v2, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    long-to-int v2, v2

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallLogSimInfoHelper"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public getSimColorDrawableById(I)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1    # I

    const/4 v6, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSimColorDrawableById() simId == ["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "]"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->log(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f090067

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v2, v5

    const/4 v5, -0x2

    if-ne v5, p1, :cond_2

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimSipColor:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f0200dc

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v1, :cond_1

    instance-of v5, v1, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v5, :cond_1

    iput-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimSipColor:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimSipColor:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    :goto_0
    return-object v5

    :cond_1
    move-object v5, v6

    goto :goto_0

    :cond_2
    if-nez p1, :cond_3

    move-object v5, v6

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/mediatek/phone/SIMInfoWrapper;->getInsertedSimColorById(I)I

    move-result v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSimColorDrawableById() color == ["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "]"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->log(Ljava/lang/String;)V

    const/4 v5, -0x1

    if-eq v5, v0, :cond_7

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/mediatek/phone/SIMInfoWrapper;->getSlotIdBySimId(I)I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSimColorArray:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;

    iget-object v5, v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;->mDrawableSimColor:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSimColorArray:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;

    iget v5, v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;->mInsertSimColor:I

    if-eq v5, v0, :cond_5

    :cond_4
    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimBackgroundDarkResByColorId(I)I

    move-result v3

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSimColorArray:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;

    iput v0, v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;->mInsertSimColor:I

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v1, :cond_6

    instance-of v5, v1, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSimColorArray:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;

    iput-object v1, v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;->mDrawableSimColor:Landroid/graphics/drawable/Drawable;

    :cond_5
    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSimColorArray:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;

    iget-object v5, v5, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper$SimColor;->mDrawableSimColor:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_6
    move-object v5, v6

    goto/16 :goto_0

    :cond_7
    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimLockedColor:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f0200dd

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v1, :cond_9

    instance-of v5, v1, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v5, :cond_9

    iput-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimLockedColor:Landroid/graphics/drawable/Drawable;

    :cond_8
    iget-object v5, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mDrawableSimLockedColor:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto/16 :goto_0

    :cond_9
    move-object v5, v6

    goto/16 :goto_0
.end method

.method public getSimDisplayNameById(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    const/4 v0, -0x2

    if-ne v0, p1, :cond_1

    const-string v0, ""

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSipCallDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSipCallDisplayName:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;->mSipCallDisplayName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    if-nez p1, :cond_2

    const-string v0, ""

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimDisplayNameById(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
