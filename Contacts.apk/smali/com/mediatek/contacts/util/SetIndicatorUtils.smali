.class public Lcom/mediatek/contacts/util/SetIndicatorUtils;
.super Ljava/lang/Object;
.source "SetIndicatorUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/util/SetIndicatorUtils$1;,
        Lcom/mediatek/contacts/util/SetIndicatorUtils$MyBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final INDICATE_TYPE:Ljava/lang/String; = "CONTACTS"

.field private static final PEOPLEACTIVITY:Ljava/lang/String;

.field private static final QUICKCONTACTACTIVITY:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SetIndicatorUtils"

.field private static sInstance:Lcom/mediatek/contacts/util/SetIndicatorUtils;


# instance fields
.field private mQuickContactIsShow:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mShowSimIndicator:Z

.field private mStatusBarMgr:Landroid/app/StatusBarManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/contacts/activities/PeopleActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->PEOPLEACTIVITY:Ljava/lang/String;

    const-class v0, Lcom/android/contacts/quickcontact/QuickContactActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->QUICKCONTACTACTIVITY:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/contacts/util/SetIndicatorUtils$MyBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/contacts/util/SetIndicatorUtils$MyBroadcastReceiver;-><init>(Lcom/mediatek/contacts/util/SetIndicatorUtils;Lcom/mediatek/contacts/util/SetIndicatorUtils$1;)V

    iput-object v0, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mQuickContactIsShow:Z

    iget-object v0, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mStatusBarMgr:Landroid/app/StatusBarManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v0

    const-string v1, "statusbar"

    invoke-virtual {v0, v1}, Lcom/android/contacts/ContactsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    iput-object v0, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mStatusBarMgr:Landroid/app/StatusBarManager;

    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/util/SetIndicatorUtils;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/util/SetIndicatorUtils;

    iget-boolean v0, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mShowSimIndicator:Z

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/util/SetIndicatorUtils;ZLandroid/app/Activity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/util/SetIndicatorUtils;
    .param p1    # Z
    .param p2    # Landroid/app/Activity;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/contacts/util/SetIndicatorUtils;->setSimIndicatorVisibility(ZLandroid/app/Activity;)V

    return-void
.end method

.method public static getInstance()Lcom/mediatek/contacts/util/SetIndicatorUtils;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->sInstance:Lcom/mediatek/contacts/util/SetIndicatorUtils;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/util/SetIndicatorUtils;

    invoke-direct {v0}, Lcom/mediatek/contacts/util/SetIndicatorUtils;-><init>()V

    sput-object v0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->sInstance:Lcom/mediatek/contacts/util/SetIndicatorUtils;

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->sInstance:Lcom/mediatek/contacts/util/SetIndicatorUtils;

    return-object v0
.end method

.method private setSimIndicatorVisibility(ZLandroid/app/Activity;)V
    .locals 5
    .param p1    # Z
    .param p2    # Landroid/app/Activity;

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mStatusBarMgr:Landroid/app/StatusBarManager;

    const-string v3, "voice_call_sim_setting"

    invoke-virtual {v2, v1, v3}, Landroid/app/StatusBarManager;->showSimIndicator(Landroid/content/ComponentName;Ljava/lang/String;)V

    sget-object v2, Lcom/mediatek/contacts/util/SetIndicatorUtils;->QUICKCONTACTACTIVITY:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mQuickContactIsShow:Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v2, "SetIndicatorUtils"

    const-string v3, "set compentantName is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/mediatek/contacts/util/SetIndicatorUtils;->QUICKCONTACTACTIVITY:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mQuickContactIsShow:Z

    :cond_4
    iget-boolean v2, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mQuickContactIsShow:Z

    if-eqz v2, :cond_5

    sget-object v2, Lcom/mediatek/contacts/util/SetIndicatorUtils;->PEOPLEACTIVITY:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "SetIndicatorUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " no hide PEOPLEACTIVITY="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/util/SetIndicatorUtils;->PEOPLEACTIVITY:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mStatusBarMgr:Landroid/app/StatusBarManager;

    invoke-virtual {v2, v1}, Landroid/app/StatusBarManager;->hideSimIndicator(Landroid/content/ComponentName;)V

    goto :goto_1
.end method


# virtual methods
.method public registerReceiver(Landroid/app/Activity;)V
    .locals 4
    .param p1    # Landroid/app/Activity;

    const-string v1, "SetIndicatorUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerReceiver activity : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.VOICE_CALL_DEFAULT_SIM"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public showIndicator(ZLandroid/app/Activity;)V
    .locals 3
    .param p1    # Z
    .param p2    # Landroid/app/Activity;

    const-string v0, "SetIndicatorUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showIndicator visible : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mShowSimIndicator:Z

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v0

    const-string v1, "CONTACTS"

    const-string v2, "ExtensionForAppGuideExt"

    invoke-virtual {v0, p2, v1, v2}, Lcom/android/contacts/ext/ContactAccountExtension;->switchSimGuide(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/mediatek/contacts/util/SetIndicatorUtils;->setSimIndicatorVisibility(ZLandroid/app/Activity;)V

    return-void
.end method

.method public unregisterReceiver(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    const-string v0, "SetIndicatorUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unregisterReceiver activity : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/util/SetIndicatorUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
