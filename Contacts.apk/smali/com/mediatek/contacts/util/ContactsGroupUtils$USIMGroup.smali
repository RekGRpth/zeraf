.class public final Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;
.super Ljava/lang/Object;
.source "ContactsGroupUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/util/ContactsGroupUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "USIMGroup"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;
    }
.end annotation


# static fields
.field public static final SIM_TYPE_USIM:Ljava/lang/String; = "USIM"

.field public static final TAG:Ljava/lang/String; = "ContactsGroupUtils"

.field private static final UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getSlotCount()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;-><init>(I)V

    sput-object v0, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addUSIMGroupMember(III)Z
    .locals 6
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    if-lez p2, :cond_0

    :try_start_0
    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p1, p2}, Lcom/android/internal/telephony/IIccPhoneBook;->addContactToGroup(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v2, 0x1

    :cond_0
    :goto_0
    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[addUSIMGroupMember]succFlag"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :catch_0
    move-exception v0

    const-string v3, "ContactsGroupUtils"

    const-string v4, "catched exception"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static deleteUSIMGroup(ILjava/lang/String;)I
    .locals 6
    .param p0    # I
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v3

    const/4 v1, -0x2

    :try_start_0
    invoke-static {p0, p1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->hasExistGroup(ILjava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/IIccPhoneBook;->removeUsimGroupById(I)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-virtual {v4, p0, v2}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->removeItem(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "ContactsGroupUtils"

    const-string v5, "catched exception"

    invoke-static {v4, v5}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static deleteUSIMGroupMember(III)Z
    .locals 6
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[deleteUSIMGroupMember]slotId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|simIndex:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|grpId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    if-lez p2, :cond_0

    :try_start_0
    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p1, p2}, Lcom/android/internal/telephony/IIccPhoneBook;->removeContactFromGroup(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_0
    :goto_0
    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[deleteUSIMGroupMember]result:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :catch_0
    move-exception v0

    const-string v3, "ContactsGroupUtils"

    const-string v4, "catched exception."

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static deleteUSIMGroupOnPhone(Landroid/content/Context;I)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "account_type=\'USIM Account\' AND account_name=\'USIM"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static getUSIMGrpMaxNameLen(I)I
    .locals 6
    .param p0    # I

    const/4 v2, -0x1

    invoke-static {p0}, Lcom/mediatek/contacts/simcontact/SlotUtils;->isSlotValid(I)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-static {p0}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[getUSIMGrpMaxNameLen]phoneBookReady:false |slot:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-virtual {v3, p0, v2}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->setMaxUsimGroupNameLength(II)V

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-virtual {v3, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->getMaxUsimGroupNameLength(I)I

    move-result v3

    if-gez v3, :cond_2

    :try_start_0
    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-interface {v1}, Lcom/android/internal/telephony/IIccPhoneBook;->getUsimGrpMaxNameLen()I

    move-result v4

    invoke-virtual {v3, p0, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->setMaxUsimGroupNameLength(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const-string v2, "ContactsGroupUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[getUSIMGrpMaxNameLen]end slot:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|maxNameLen:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-virtual {v4, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->getMaxUsimGroupNameLength(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-virtual {v2, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->getMaxUsimGroupNameLength(I)I

    move-result v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "ContactsGroupUtils"

    const-string v4, "catched exception."

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-virtual {v3, p0, v2}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->setMaxUsimGroupNameLength(II)V

    goto :goto_1
.end method

.method public static hasExistGroup(ILjava/lang/String;)I
    .locals 12
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, -0x1

    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v5

    const-string v9, "ContactsGroupUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "grpName:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "|iIccPhb:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    if-nez v5, :cond_1

    :cond_0
    move v3, v2

    :goto_0
    return v3

    :cond_1
    sget-object v9, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-virtual {v9, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->get(I)Ljava/util/ArrayList;

    move-result-object v8

    const-string v9, "ContactsGroupUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[hasExistGroup]ugrpList---size:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v5}, Lcom/android/internal/telephony/IIccPhoneBook;->getUsimGroups()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/common/telephony/UsimGroup;

    invoke-virtual {v7}, Lcom/mediatek/common/telephony/UsimGroup;->getAlphaTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lcom/mediatek/common/telephony/UsimGroup;->getRecordIndex()I

    move-result v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    if-lez v0, :cond_2

    new-instance v9, Lcom/mediatek/common/telephony/UsimGroup;

    invoke-direct {v9, v0, v1}, Lcom/mediatek/common/telephony/UsimGroup;-><init>(ILjava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v9, "ContactsGroupUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[hasExistGroup]gName:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "||gIndex:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v2, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/common/telephony/UsimGroup;

    const-string v9, "ContactsGroupUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[hasExistGroup]ug---index:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/mediatek/common/telephony/UsimGroup;->getRecordIndex()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " || name:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/mediatek/common/telephony/UsimGroup;->getAlphaTag()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/mediatek/common/telephony/UsimGroup;->getAlphaTag()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v7}, Lcom/mediatek/common/telephony/UsimGroup;->getRecordIndex()I

    move-result v2

    :cond_5
    const-string v9, "ContactsGroupUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ugrpList size:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    goto/16 :goto_0
.end method

.method public static moveContactToGroups(II[I)Z
    .locals 6
    .param p0    # I
    .param p1    # I
    .param p2    # [I

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    :try_start_0
    array-length v3, p2

    if-lez v3, :cond_0

    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[moveUSIMGroupMember]slotId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|simIndex:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|grpId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p1, p2}, Lcom/android/internal/telephony/IIccPhoneBook;->updateContactToGroups(I[I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_0
    :goto_0
    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[moveUSIMGroupMember]result:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :catch_0
    move-exception v0

    const-string v3, "ContactsGroupUtils"

    const-string v4, "moveUSIMGroupMember: catched exception."

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized syncUSIMGroupContactsGroup(Landroid/content/Context;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;Ljava/util/HashMap;)V
    .locals 38
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-class v37, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;

    monitor-enter v37

    :try_start_0
    const-string v3, "ContactsGroupUtils"

    const-string v4, "syncUSIMGroupContactsGroup begin"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v3, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    :cond_0
    :goto_0
    monitor-exit v37

    return-void

    :cond_1
    :try_start_1
    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    move/from16 v26, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    move/from16 v36, v0

    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    move/from16 v0, v27

    invoke-virtual {v3, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->get(I)Ljava/util/ArrayList;

    move-result-object v33

    const/4 v3, 0x2

    move/from16 v0, v36

    if-ne v0, v3, :cond_2

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->deleteUSIMGroupOnPhone(Landroid/content/Context;I)V

    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->clear()V

    const-string v3, "ContactsGroupUtils"

    const-string v4, "syncUSIMGroupContactsGroup end. deleteUSIMGroupOnPhone."

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v37

    throw v3

    :cond_2
    :try_start_2
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->clear()V

    invoke-static/range {v27 .. v27}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v23

    if-eqz v23, :cond_0

    :try_start_3
    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/IIccPhoneBook;->getUsimGroups()Ljava/util/List;

    move-result-object v28

    if-eqz v28, :cond_0

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_3
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/mediatek/common/telephony/UsimGroup;

    invoke-virtual/range {v29 .. v29}, Lcom/mediatek/common/telephony/UsimGroup;->getAlphaTag()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {v29 .. v29}, Lcom/mediatek/common/telephony/UsimGroup;->getRecordIndex()I

    move-result v13

    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[syncUSIMGroupContactsGroup]gName:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|gIndex: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    if-lez v13, :cond_3

    new-instance v3, Lcom/mediatek/common/telephony/UsimGroup;

    invoke-direct {v3, v13, v14}, Lcom/mediatek/common/telephony/UsimGroup;-><init>(ILjava/lang/String;)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v12

    :try_start_4
    const-string v3, "ContactsGroupUtils"

    const-string v4, "catched exception"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    :try_start_5
    const-string v3, "ContactsGroupUtils"

    const-string v4, "getUSIMGrpMaxNameLen begin"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/IIccPhoneBook;->getUsimGrpMaxNameLen()I

    move-result v4

    move/from16 v0, v27

    invoke-virtual {v3, v0, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->setMaxUsimGroupNameLength(II)V

    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getUSIMGrpMaxNameLen end. slot:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|NAME_LENGTH:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->getMaxUsimGroupNameLength(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "ContactsGroupUtils"

    const-string v4, "getUSIMGrpMaxCount begin."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-interface/range {v23 .. v23}, Lcom/android/internal/telephony/IIccPhoneBook;->getUsimGrpMaxCount()I

    move-result v4

    move/from16 v0, v27

    invoke-virtual {v3, v0, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->setMaxUsimGroupCount(II)V

    const-string v3, "ContactsGroupUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getUSIMGrpMaxCount end. slot:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|GROUP_COUNT:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->getMaxUsimGroupCount(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_2
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_SUMMARY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deleted=0 AND account_type=\'USIM Account\' AND account_name=\'USIM"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    new-instance v25, Ljava/util/HashMap;

    invoke-direct/range {v25 .. v25}, Ljava/util/HashMap;-><init>()V

    if-eqz v9, :cond_7

    const/4 v3, -0x1

    invoke-interface {v9, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_5
    :goto_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "title"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const-string v3, "_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :catch_1
    move-exception v12

    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    const/4 v4, -0x1

    move/from16 v0, v27

    invoke-virtual {v3, v0, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->setMaxUsimGroupNameLength(II)V

    sget-object v3, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    const/4 v4, -0x1

    move/from16 v0, v27

    invoke-virtual {v3, v0, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->setMaxUsimGroupCount(II)V

    goto :goto_2

    :cond_6
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_7
    if-eqz v33, :cond_10

    const/16 v20, 0x0

    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_8
    :goto_4
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/mediatek/common/telephony/UsimGroup;

    invoke-virtual/range {v32 .. v32}, Lcom/mediatek/common/telephony/UsimGroup;->getAlphaTag()Ljava/lang/String;

    move-result-object v31

    const/16 v20, 0x0

    const-wide/16 v15, -0x1

    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual/range {v32 .. v32}, Lcom/mediatek/common/telephony/UsimGroup;->getRecordIndex()I

    move-result v30

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v15, v3

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v20, 0x1

    :cond_9
    if-nez v20, :cond_a

    new-instance v35, Landroid/content/ContentValues;

    invoke-direct/range {v35 .. v35}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "title"

    move-object/from16 v0, v35

    move-object/from16 v1, v31

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "group_visible"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "system_id"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "account_name"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "USIM"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "account_type"

    const-string v4, "USIM Account"

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v35

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v34

    if-nez v34, :cond_b

    const-wide/16 v15, 0x0

    :cond_a
    :goto_5
    const-wide/16 v3, 0x0

    cmp-long v3, v15, v3

    if-lez v3, :cond_8

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    long-to-int v4, v15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    :cond_b
    invoke-static/range {v34 .. v34}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v15

    goto :goto_5

    :cond_c
    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_f

    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Integer;

    invoke-interface {v3, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [Ljava/lang/Integer;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v8, v17

    array-length v0, v8

    move/from16 v24, v0

    const/16 v22, 0x0

    :goto_6
    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_d

    aget-object v21, v8, v22

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v22, v22, 0x1

    goto :goto_6

    :cond_d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_e

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    :cond_e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_f

    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id IN ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_f
    const-string v3, "ContactsGroupUtils"

    const-string v4, "syncUSIMGroupContactsGroup end"

    invoke-static {v3, v4}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->deleteUSIMGroupOnPhone(Landroid/content/Context;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0
.end method

.method public static syncUSIMGroupNewIfMissing(ILjava/lang/String;)I
    .locals 8
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v3, 0x0

    const-string v4, "ContactsGroupUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[syncUSIMGroupNewIfMissing]name:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, -0x1

    :cond_0
    return v1

    :cond_1
    :try_start_0
    const-string v4, "GBK"

    invoke-virtual {p1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    array-length v3, v4
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v4, "ContactsGroupUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[syncUSIMGroupNewIfMissing]nameLen:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ||getUSIMGrpMaxNameLen(slotId):"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->getUSIMGrpMaxNameLen(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->getUSIMGrpMaxNameLen(I)I

    move-result v4

    if-le v3, v4, :cond_2

    new-instance v4, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;

    const-string v5, "Group name out of bound"

    invoke-direct {v4, v5, v7, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;-><init>(Ljava/lang/String;II)V

    throw v4

    :catch_0
    move-exception v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v2

    const/4 v1, -0x1

    invoke-static {p0, p1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->hasExistGroup(ILjava/lang/String;)I

    move-result v1

    if-ge v1, v7, :cond_3

    if-eqz v2, :cond_3

    invoke-interface {v2, p1}, Lcom/android/internal/telephony/IIccPhoneBook;->insertUsimGroup(Ljava/lang/String;)I

    move-result v1

    const-string v4, "ContactsGroupUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[syncUSIMGroupNewIfMissing]inserted grpId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v1, :cond_3

    sget-object v4, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    new-instance v5, Lcom/mediatek/common/telephony/UsimGroup;

    invoke-direct {v5, v1, p1}, Lcom/mediatek/common/telephony/UsimGroup;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, p0, v5}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->addItem(ILcom/mediatek/common/telephony/UsimGroup;)Z

    :cond_3
    const-string v4, "ContactsGroupUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[syncUSIMGroupNewIfMissing]grpId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-ge v1, v7, :cond_0

    sparse-switch v1, :sswitch_data_0

    new-instance v4, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;

    const-string v5, "Group generic error"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;-><init>(Ljava/lang/String;II)V

    throw v4

    :sswitch_0
    new-instance v4, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;

    const-string v5, "Group count out of bound"

    const/4 v6, 0x2

    invoke-direct {v4, v5, v6, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;-><init>(Ljava/lang/String;II)V

    throw v4

    :sswitch_1
    new-instance v4, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;

    const-string v5, "Group name out of bound"

    invoke-direct {v4, v5, v7, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;-><init>(Ljava/lang/String;II)V

    throw v4

    nop

    :sswitch_data_0
    .sparse-switch
        -0x14 -> :sswitch_0
        -0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public static syncUSIMGroupUpdate(ILjava/lang/String;Ljava/lang/String;)I
    .locals 10
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;
        }
    .end annotation

    const/4 v6, 0x1

    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->getIIccPhoneBook(I)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->hasExistGroup(ILjava/lang/String;)I

    move-result v1

    const-string v7, "ContactsGroupUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "grpId:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|slotId:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|oldName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|newName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-lez v1, :cond_2

    const/4 v3, 0x0

    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "GBK"

    invoke-virtual {p2, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    array-length v3, v7
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->getUSIMGrpMaxNameLen(I)I

    move-result v7

    if-ge v7, v3, :cond_0

    new-instance v7, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;

    const-string v8, "Group name out of bound"

    invoke-direct {v7, v8, v6, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;-><init>(Ljava/lang/String;II)V

    throw v7

    :catch_0
    move-exception v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    goto :goto_0

    :cond_0
    invoke-interface {v2, v1, p2}, Lcom/android/internal/telephony/IIccPhoneBook;->updateUsimGroup(ILjava/lang/String;)I

    move-result v4

    const/16 v7, -0xa

    if-ne v4, v7, :cond_1

    new-instance v6, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;

    const-string v7, "Group count out of bound"

    const/4 v8, 0x2

    invoke-direct {v6, v7, v8, p0}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroupException;-><init>(Ljava/lang/String;II)V

    throw v6

    :cond_1
    sget-object v7, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup;->UGRP_LISTARRAY:Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;

    invoke-virtual {v7, p0, v1}, Lcom/mediatek/contacts/util/ContactsGroupUtils$USIMGroup$USimGroupArray;->getItem(II)Lcom/mediatek/common/telephony/UsimGroup;

    move-result-object v5

    const-string v7, "ContactsGroupUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[syncUSIMGroupUpdate]: usimGrp is null = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-nez v5, :cond_3

    :goto_1
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/mediatek/contacts/util/ContactsGroupUtils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v5, :cond_2

    invoke-virtual {v5, p2}, Lcom/mediatek/common/telephony/UsimGroup;->setAlphaTag(Ljava/lang/String;)V

    :cond_2
    return v1

    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method
