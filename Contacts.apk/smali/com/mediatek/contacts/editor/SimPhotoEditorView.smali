.class public Lcom/mediatek/contacts/editor/SimPhotoEditorView;
.super Lcom/android/contacts/editor/PhotoEditorView;
.source "SimPhotoEditorView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/contacts/editor/PhotoEditorView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/contacts/editor/PhotoEditorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected getPhotoImageResource()I
    .locals 1

    const v0, 0x7f020070

    return v0
.end method

.method protected getTriangleAffordance()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onInflatePhotoImageId()I
    .locals 1

    const v0, 0x7f070163

    return v0
.end method
