.class public Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;
.super Ljava/lang/Object;
.source "DropMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/list/DropMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DropDownMenu"
.end annotation


# instance fields
.field private mButton:Landroid/widget/Button;

.field private mMenu:Landroid/view/Menu;

.field private mPopupMenu:Landroid/widget/PopupMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Button;ILandroid/widget/PopupMenu$OnMenuItemClickListener;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/Button;
    .param p3    # I
    .param p4    # Landroid/widget/PopupMenu$OnMenuItemClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mButton:Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mButton:Landroid/widget/Button;

    invoke-direct {v0, p1, v1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    iget-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mMenu:Landroid/view/Menu;

    iget-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, p3, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0, p4}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu$1;-><init>(Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;)Landroid/widget/PopupMenu;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    iget-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    return-object v0
.end method


# virtual methods
.method public findItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mMenu:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public show()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method
