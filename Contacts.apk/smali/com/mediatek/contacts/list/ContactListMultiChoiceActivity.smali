.class public Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;
.super Lcom/android/contacts/ContactsActivity;
.source "ContactListMultiChoiceActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/SearchView$OnCloseListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$3;,
        Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;
    }
.end annotation


# static fields
.field public static final CONTACTGROUPLISTACTIVITY_RESULT_CODE:I = 0x1

.field private static final DEFAULT_DIRECTORY_RESULT_LIMIT:I = 0x14

.field private static final KEY_ACTION_CODE:Ljava/lang/String; = "actionCode"

.field public static final RESTRICT_LIST:Ljava/lang/String; = "restrictlist"

.field private static final SUBACTIVITY_ADD_TO_EXISTING_CONTACT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ContactsMultiChoiceActivity"


# instance fields
.field private mActionCode:I

.field private mIntentResolverEx:Lcom/mediatek/contacts/list/ContactsIntentResolverEx;

.field private mIsSearchMode:Z

.field private mIsSelectedAll:Z

.field private mIsSelectedNone:Z

.field protected mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

.field private mRequest:Lcom/android/contacts/list/ContactsRequest;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSelectionMenu:Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/android/contacts/ContactsActivity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    iput-boolean v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSelectedAll:Z

    iput-boolean v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSelectedNone:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSearchMode:Z

    new-instance v0, Lcom/mediatek/contacts/list/ContactsIntentResolverEx;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/list/ContactsIntentResolverEx;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIntentResolverEx:Lcom/mediatek/contacts/list/ContactsIntentResolverEx;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;
    .param p1    # Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    return-void
.end method

.method private showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V
    .locals 21
    .param p1    # Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    sget-object v18, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$3;->$SwitchMap$com$mediatek$contacts$list$ContactListMultiChoiceActivity$SelectionMode:[I

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSearchMode:Z

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    invoke-virtual {v2}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v18

    const v19, 0x7f040096

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v15

    const v18, 0x7f070169

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const v18, 0x7f07009a

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/Button;

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    const v18, 0x7f0700ec

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/SearchView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    const v19, 0x7f0c0283

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/SearchView;->setIconified(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/SearchView;->onActionViewExpanded()V

    new-instance v18, Landroid/app/ActionBar$LayoutParams;

    const/16 v19, -0x1

    const/16 v20, -0x2

    invoke-direct/range {v18 .. v20}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    move-object/from16 v0, v18

    invoke-virtual {v2, v15, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    const v18, 0x7f070097

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v18, "Cancel"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v18, 0x41400000

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_0
    sget-object v18, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v18, 0x7f07016b

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    sget-object v18, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSelectedNone:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    const v18, -0x333334

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_1
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    const/16 v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :pswitch_2
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSearchMode:Z

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    const-string v18, "layout_inflater"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/contacts/ContactsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/LayoutInflater;

    const v18, 0x7f040096

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    const v18, 0x102002c

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const v18, 0x7f0700ec

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/SearchView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    const v18, 0x7f070169

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v18, 0x7f07009a

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    invoke-virtual/range {p0 .. p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f02003b

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v18, 0x7f070097

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v18, "Cancel"

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v18, 0x41400000

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_2
    sget-object v18, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v18, 0x7f07016b

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    sget-object v18, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {v12}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v18, 0x10

    const/16 v19, 0x1a

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v2, v8}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private showInputMethod(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/android/contacts/ContactsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ContactsMultiChoiceActivity"

    const-string v2, "Failed to show soft input method."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public configureListFragment()V
    .locals 4

    iget v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v2}, Lcom/android/contacts/list/ContactsRequest;->getActionCode()I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v1}, Lcom/android/contacts/list/ContactsRequest;->getActionCode()I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    const-string v1, "ContactsMultiChoiceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "configureListFragment action code is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    sparse-switch v1, :sswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid action code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    new-instance v1, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    :goto_1
    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v2}, Lcom/android/contacts/list/ContactsRequest;->isLegacyCompatibilityMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/contacts/list/ContactEntryListFragment;->setLegacyCompatibilityMode(Z)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v2}, Lcom/android/contacts/list/ContactsRequest;->getQueryString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/contacts/list/ContactEntryListFragment;->setQueryString(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/android/contacts/list/ContactEntryListFragment;->setDirectoryResultLimit(I)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/contacts/list/ContactEntryListFragment;->setVisibleScrollbarEnabled(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0700ed

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_0

    :sswitch_1
    new-instance v1, Lcom/mediatek/contacts/list/ContactsVCardPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/ContactsVCardPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    goto :goto_1

    :sswitch_2
    new-instance v1, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    const-string v1, "intent"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_1

    :sswitch_3
    new-instance v1, Lcom/mediatek/contacts/list/MultiEmailsPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiEmailsPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    goto :goto_1

    :sswitch_4
    new-instance v1, Lcom/mediatek/contacts/list/MultiPhoneNumbersPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiPhoneNumbersPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    goto :goto_1

    :sswitch_5
    new-instance v1, Lcom/mediatek/contacts/list/MultiDataItemsPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiDataItemsPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    const-string v1, "intent"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_1

    :sswitch_6
    new-instance v1, Lcom/mediatek/contacts/list/ContactsMultiDeletionFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/ContactsMultiDeletionFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    goto/16 :goto_1

    :sswitch_7
    new-instance v1, Lcom/mediatek/contacts/list/ContactsGroupMultiPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/ContactsGroupMultiPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    const-string v1, "intent"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :sswitch_8
    new-instance v1, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    goto/16 :goto_1

    :sswitch_9
    new-instance v1, Lcom/mediatek/contacts/list/MultiContactsShareFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/MultiContactsShareFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    goto/16 :goto_1

    :sswitch_a
    new-instance v1, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsFragment;

    invoke-direct {v1}, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    const-string v1, "intent"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x3d -> :sswitch_0
        0x3e -> :sswitch_6
        0x3f -> :sswitch_7
        0x40 -> :sswitch_9
        0x41 -> :sswitch_a
        0x5b -> :sswitch_4
        0x5c -> :sswitch_8
        0x5d -> :sswitch_5
        0x6a -> :sswitch_3
        0x100003d -> :sswitch_1
        0x200003d -> :sswitch_2
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p1, :cond_1

    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    if-eqz p3, :cond_0

    invoke-virtual {p0, p3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    const v2, 0x1b208

    if-ne p2, v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_2
    const/4 v2, 0x1

    if-ne p2, v2, :cond_3

    const-string v2, "checkedids"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    instance-of v2, v2, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    check-cast v0, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->markItemsAsSelectedForCheckedGroups([J)V

    :cond_3
    return-void
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/app/Fragment;

    instance-of v0, p1, Lcom/mediatek/contacts/list/AbstractPickerFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/mediatek/contacts/list/AbstractPickerFragment;

    iput-object p1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_0
    sget-object v0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->ListMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v0}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v2}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView()V

    sget-object v2, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->SearchMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v2}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    invoke-virtual {p0}, Landroid/app/Activity;->closeOptionsMenu()V

    goto :goto_0

    :sswitch_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    instance-of v2, v2, Lcom/mediatek/contacts/list/MultiContactsDuplicationFragment;

    if-eqz v2, :cond_0

    const-string v2, "ContactsMultiChoiceActivity"

    const-string v3, "Send result for copy action"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x1b208

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setResult(I)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v2}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->onOptionAction()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->updateSelectionMenu(Landroid/view/View;)V

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSelectionMenu:Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    invoke-virtual {v2}, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->show()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f070097 -> :sswitch_1
        0x7f07009a -> :sswitch_3
        0x7f070169 -> :sswitch_1
        0x7f07016b -> :sswitch_2
        0x7f0701f1 -> :sswitch_0
    .end sparse-switch
.end method

.method public onClose()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_1
    sget-object v1, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->ListMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v1}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "ContactsMultiChoiceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onConfigurationChanged]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/16 v5, 0x8

    invoke-super {p0, p1}, Lcom/android/contacts/ContactsActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v3, "actionCode"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    :cond_0
    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIntentResolverEx:Lcom/mediatek/contacts/list/ContactsIntentResolverEx;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/contacts/list/ContactsIntentResolverEx;->resolveIntent(Landroid/content/Intent;)Lcom/android/contacts/list/ContactsRequest;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v3}, Lcom/android/contacts/list/ContactsRequest;->isValid()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "ContactsMultiChoiceActivity"

    const-string v4, "Request is invalid!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mRequest:Lcom/android/contacts/list/ContactsRequest;

    invoke-virtual {v3}, Lcom/android/contacts/list/ContactsRequest;->getRedirectIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const v3, 0x7f04003a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->configureListFragment()V

    const v3, 0x7f0700ec

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SearchView;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f0700ee

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    sget-object v3, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->ListMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v3}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v2, 0x7f10000d

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v2, 0x7f0701f1

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0c0104

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    const/4 v2, 0x1

    return v2
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/android/contacts/ContactsActivity;->onDestroy()V

    const-string v0, "ContactsMultiChoiceActivity"

    const-string v1, "[onDestroy]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0700ec

    if-ne v0, v1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->findFocus()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showInputMethod(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/MenuItem;

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0701f1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v1}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView()V

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v1}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->isSelectedNone()Z

    move-result v1

    iput-boolean v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSelectedNone:Z

    sget-object v1, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;->SearchMode:Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->showActionBar(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$SelectionMode;)V

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->onBackPressed()V

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0701f2

    if-ne v1, v2, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/contacts/list/ContactGroupListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v4, 0x1

    const/4 v2, 0x0

    const v3, 0x7f0701f1

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-boolean v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSearchMode:Z

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    return v2

    :cond_0
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    instance-of v2, v2, Lcom/mediatek/contacts/list/MultiPhoneAndEmailsPickerFragment;

    if-eqz v2, :cond_1

    const v2, 0x7f0701f2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    goto :goto_0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v0, p1}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->startSearch(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/contacts/activities/TransactionSafeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "actionCode"

    iget v1, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mActionCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public returnPickerResult(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public returnPickerResult(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->returnPickerResult(Landroid/content/Intent;)V

    return-void
.end method

.method public startActivityAndForwardResult(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const/high16 v1, 0x2000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public updateSelectionMenu(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const v5, 0x7f07009a

    new-instance v0, Lcom/mediatek/contacts/list/DropMenu;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/list/DropMenu;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const v4, 0x7f100010

    invoke-virtual {v0, v3, v4}, Lcom/mediatek/contacts/list/DropMenu;->addDropDownMenu(Landroid/widget/Button;I)Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSelectionMenu:Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02003b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mSelectionMenu:Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;

    const v4, 0x7f0701ff

    invoke-virtual {v3, v4}, Lcom/mediatek/contacts/list/DropMenu$DropDownMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v3}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView()V

    iget-object v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mListFragment:Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v3}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->isSelectedAll()Z

    move-result v3

    iput-boolean v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSelectedAll:Z

    iget-boolean v3, p0, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;->mIsSelectedAll:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0c0261

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    new-instance v3, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$1;

    invoke-direct {v3, p0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$1;-><init>(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;)V

    invoke-virtual {v0, v3}, Lcom/mediatek/contacts/list/DropMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    :goto_0
    return-void

    :cond_0
    const v3, 0x7f0c0260

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    new-instance v3, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$2;

    invoke-direct {v3, p0}, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity$2;-><init>(Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;)V

    invoke-virtual {v0, v3}, Lcom/mediatek/contacts/list/DropMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    goto :goto_0
.end method
