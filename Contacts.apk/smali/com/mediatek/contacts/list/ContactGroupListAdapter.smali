.class public Lcom/mediatek/contacts/list/ContactGroupListAdapter;
.super Lcom/android/contacts/group/GroupBrowseListAdapter;
.source "ContactGroupListAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ContactGroupListAdapter"


# instance fields
.field private mSparseBooleanArray:Landroid/util/SparseBooleanArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/contacts/group/GroupBrowseListAdapter;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected getGroupListItemLayout()I
    .locals 1

    const v0, 0x7f04006d

    return v0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/contacts/group/GroupBrowseListAdapter;->getItem(I)Lcom/android/contacts/group/GroupListItem;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v1, -0x1

    :goto_0
    return-wide v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/contacts/group/GroupListItem;->getGroupId()J

    move-result-wide v1

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setSparseBooleanArray(Landroid/util/SparseBooleanArray;)V
    .locals 0
    .param p1    # Landroid/util/SparseBooleanArray;

    iput-object p1, p0, Lcom/mediatek/contacts/list/ContactGroupListAdapter;->mSparseBooleanArray:Landroid/util/SparseBooleanArray;

    return-void
.end method

.method protected setViewWithCheckBox(Landroid/view/View;I)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactGroupListAdapter;->mSparseBooleanArray:Landroid/util/SparseBooleanArray;

    if-nez v1, :cond_0

    const-string v1, "ContactGroupListAdapter"

    const-string v2, "mSparseBooleanArray is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const v1, 0x1020001

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/mediatek/contacts/list/ContactGroupListAdapter;->mSparseBooleanArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0
.end method
