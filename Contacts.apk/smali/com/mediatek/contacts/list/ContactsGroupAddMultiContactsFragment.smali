.class public Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsFragment;
.super Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;
.source "ContactsGroupAddMultiContactsFragment.java"

# interfaces
.implements Lcom/android/contacts/group/GroupEditorFragment$ScrubListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/contacts/list/MultiContactsPickerBaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createListAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsFragment;->createListAdapter()Lcom/android/contacts/list/ContactListAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected createListAdapter()Lcom/android/contacts/list/ContactListAdapter;
    .locals 7

    const/4 v6, 0x1

    new-instance v2, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsAdapter;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsAdapter;-><init>(Landroid/content/Context;Landroid/widget/ListView;)V

    const/4 v4, -0x2

    invoke-static {v4}, Lcom/android/contacts/list/ContactListFilter;->createFilterWithType(I)Lcom/android/contacts/list/ContactListFilter;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->setFilter(Lcom/android/contacts/list/ContactListFilter;)V

    invoke-virtual {v2, v6}, Lcom/android/contacts/widget/IndexerListAdapter;->setSectionHeaderDisplayEnabled(Z)V

    invoke-virtual {v2, v6}, Lcom/android/contacts/list/ContactEntryListAdapter;->setDisplayPhotos(Z)V

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQuickContactEnabled(Z)V

    invoke-virtual {v2, v6}, Lcom/android/contacts/list/ContactEntryListAdapter;->setEmptyListEnabled(Z)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "intent"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    const-string v4, "account_name"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "account_type"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsAdapter;->setGroupAccount(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "member_ids"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsAdapter;->setExistMemberList([J)V

    return-object v2
.end method

.method public isAccountFilterEnable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    sget-object v0, Lcom/mediatek/contacts/list/ContactsGroupAddMultiContactsFragment;->TAG:Ljava/lang/String;

    const-string v1, "onCreate setScrubListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/android/contacts/list/ContactEntryListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/android/contacts/group/GroupEditorFragment;->setScrubListener(Lcom/android/contacts/group/GroupEditorFragment$ScrubListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->onDestroy()V

    invoke-static {p0}, Lcom/android/contacts/group/GroupEditorFragment;->removeScrubListener(Lcom/android/contacts/group/GroupEditorFragment$ScrubListener;)V

    return-void
.end method

.method public scrubAffinity()V
    .locals 1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method
