.class public Lcom/mediatek/contacts/NfcShareContactsHandler;
.super Ljava/lang/Object;
.source "NfcShareContactsHandler.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# static fields
.field private static final TAG:Ljava/lang/String; = "ContactNfcHandler"


# instance fields
.field private final mMultiContactsShareFragment:Lcom/mediatek/contacts/list/MultiContactsShareFragment;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/list/MultiContactsShareFragment;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/list/MultiContactsShareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/contacts/NfcShareContactsHandler;->mMultiContactsShareFragment:Lcom/mediatek/contacts/list/MultiContactsShareFragment;

    return-void
.end method

.method public static register(Landroid/app/Activity;Lcom/mediatek/contacts/list/MultiContactsShareFragment;)V
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/mediatek/contacts/list/MultiContactsShareFragment;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/mediatek/contacts/NfcShareContactsHandler;

    invoke-direct {v1, p1}, Lcom/mediatek/contacts/NfcShareContactsHandler;-><init>(Lcom/mediatek/contacts/list/MultiContactsShareFragment;)V

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 14
    .param p1    # Landroid/nfc/NfcEvent;

    const/4 v11, 0x0

    iget-object v10, p0, Lcom/mediatek/contacts/NfcShareContactsHandler;->mMultiContactsShareFragment:Lcom/mediatek/contacts/list/MultiContactsShareFragment;

    invoke-virtual {v10}, Lcom/mediatek/contacts/list/MultiContactsShareFragment;->getUri()Landroid/net/Uri;

    move-result-object v1

    iget-object v10, p0, Lcom/mediatek/contacts/NfcShareContactsHandler;->mMultiContactsShareFragment:Lcom/mediatek/contacts/list/MultiContactsShareFragment;

    invoke-virtual {v10}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v10

    const/4 v12, 0x2

    invoke-interface {v10, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v10, Landroid/provider/ContactsContract$Contacts;->CONTENT_MULTI_VCARD_URI:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    const-string v12, "nophoto"

    const-string v13, "true"

    invoke-virtual {v10, v12, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v10, 0x400

    new-array v0, v10, [B

    :try_start_0
    invoke-virtual {v7, v8}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v9

    :goto_0
    invoke-virtual {v9, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-lez v5, :cond_0

    const/4 v10, 0x0

    invoke-virtual {v4, v0, v10, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v10, "ContactNfcHandler"

    const-string v12, "IOException creating vcard."

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v10, v11

    :goto_1
    return-object v10

    :cond_0
    :try_start_1
    const-string v10, "text/x-vcard"

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v12

    invoke-static {v10, v12}, Landroid/nfc/NdefRecord;->createMime(Ljava/lang/String;[B)Landroid/nfc/NdefRecord;

    move-result-object v6

    new-instance v10, Landroid/nfc/NdefMessage;

    const/4 v12, 0x0

    new-array v12, v12, [Landroid/nfc/NdefRecord;

    invoke-direct {v10, v6, v12}, Landroid/nfc/NdefMessage;-><init>(Landroid/nfc/NdefRecord;[Landroid/nfc/NdefRecord;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_1
    const-string v10, "ContactNfcHandler"

    const-string v12, "No contact URI to share."

    invoke-static {v10, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v10, v11

    goto :goto_1
.end method
