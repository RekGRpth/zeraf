.class public final Lcom/mediatek/phone/provider/CallHistory$Calls$CallInfo;
.super Ljava/lang/Object;
.source "CallHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/provider/CallHistory$Calls;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallInfo"
.end annotation


# instance fields
.field public final mAreaCode:Ljava/lang/String;

.field public final mConfirm:J

.field public final mCountryISO:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/phone/provider/CallHistory$Calls$CallInfo;->mCountryISO:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/phone/provider/CallHistory$Calls$CallInfo;->mAreaCode:Ljava/lang/String;

    iput-wide p3, p0, Lcom/mediatek/phone/provider/CallHistory$Calls$CallInfo;->mConfirm:J

    return-void
.end method
