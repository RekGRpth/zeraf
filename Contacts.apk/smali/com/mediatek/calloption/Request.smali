.class public Lcom/mediatek/calloption/Request;
.super Ljava/lang/Object;
.source "Request.java"


# instance fields
.field private mActivityContext:Landroid/content/Context;

.field private mApplicationContext:Landroid/content/Context;

.field private mCallOptionHandlerFactory:Lcom/mediatek/calloption/CallOptionHandlerFactory;

.field private mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

.field private mIntent:Landroid/content/Intent;

.field private mIs3GSwitchSupport:Z

.field private mIsMultipleSim:Z

.field private mResultHandler:Lcom/mediatek/calloption/CallOptionBaseHandler$ICallOptionResultHandle;

.field private mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Context;Landroid/content/Intent;Lcom/mediatek/calloption/CallOptionBaseHandler$ICallOptionResultHandle;Lcom/mediatek/CellConnService/CellConnMgr;Lcom/android/internal/telephony/ITelephony;ZZLcom/mediatek/calloption/CallOptionHandlerFactory;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/mediatek/calloption/CallOptionBaseHandler$ICallOptionResultHandle;
    .param p5    # Lcom/mediatek/CellConnService/CellConnMgr;
    .param p6    # Lcom/android/internal/telephony/ITelephony;
    .param p7    # Z
    .param p8    # Z
    .param p9    # Lcom/mediatek/calloption/CallOptionHandlerFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/calloption/Request;->mActivityContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/calloption/Request;->mApplicationContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/mediatek/calloption/Request;->mIntent:Landroid/content/Intent;

    iput-object p4, p0, Lcom/mediatek/calloption/Request;->mResultHandler:Lcom/mediatek/calloption/CallOptionBaseHandler$ICallOptionResultHandle;

    iput-object p5, p0, Lcom/mediatek/calloption/Request;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    iput-object p6, p0, Lcom/mediatek/calloption/Request;->mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

    iput-boolean p7, p0, Lcom/mediatek/calloption/Request;->mIsMultipleSim:Z

    iput-boolean p8, p0, Lcom/mediatek/calloption/Request;->mIs3GSwitchSupport:Z

    iput-object p9, p0, Lcom/mediatek/calloption/Request;->mCallOptionHandlerFactory:Lcom/mediatek/calloption/CallOptionHandlerFactory;

    return-void
.end method


# virtual methods
.method public getActivityContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calloption/Request;->mActivityContext:Landroid/content/Context;

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calloption/Request;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCallOptionHandlerFactory()Lcom/mediatek/calloption/CallOptionHandlerFactory;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calloption/Request;->mCallOptionHandlerFactory:Lcom/mediatek/calloption/CallOptionHandlerFactory;

    return-object v0
.end method

.method public getCellConnMgr()Lcom/mediatek/CellConnService/CellConnMgr;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calloption/Request;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calloption/Request;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getResultHandler()Lcom/mediatek/calloption/CallOptionBaseHandler$ICallOptionResultHandle;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calloption/Request;->mResultHandler:Lcom/mediatek/calloption/CallOptionBaseHandler$ICallOptionResultHandle;

    return-object v0
.end method

.method public getTelephonyInterface()Lcom/android/internal/telephony/ITelephony;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/calloption/Request;->mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

    return-object v0
.end method

.method public is3GSwitchSupport()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/calloption/Request;->mIs3GSwitchSupport:Z

    return v0
.end method

.method public isMultipleSim()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/calloption/Request;->mIsMultipleSim:Z

    return v0
.end method
