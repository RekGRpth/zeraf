.class public Lcom/android/contacts/calllog/CallDetailHistoryAdapter;
.super Landroid/widget/BaseAdapter;
.source "CallDetailHistoryAdapter.java"


# static fields
.field private static final VIEW_TYPE_HEADER:I = 0x0

.field private static final VIEW_TYPE_HISTORY_ITEM:I = 0x1


# instance fields
.field private final mCallTypeHelper:Lcom/android/contacts/calllog/CallTypeHelper;

.field private final mContext:Landroid/content/Context;

.field private final mControls:Landroid/view/View;

.field private mHeaderFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private mNumber:Ljava/lang/String;

.field private final mPhoneCallDetails:[Lcom/android/contacts/PhoneCallDetails;

.field private final mShowCallAndSms:Z

.field private final mShowVoicemail:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/android/contacts/calllog/CallTypeHelper;[Lcom/android/contacts/PhoneCallDetails;ZZLandroid/view/View;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/android/contacts/calllog/CallTypeHelper;
    .param p4    # [Lcom/android/contacts/PhoneCallDetails;
    .param p5    # Z
    .param p6    # Z
    .param p7    # Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallDetailHistoryAdapter$1;-><init>(Lcom/android/contacts/calllog/CallDetailHistoryAdapter;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mHeaderFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mNumber:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iput-object p3, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mCallTypeHelper:Lcom/android/contacts/calllog/CallTypeHelper;

    iput-object p4, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mPhoneCallDetails:[Lcom/android/contacts/PhoneCallDetails;

    iput-boolean p5, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowVoicemail:Z

    iput-boolean p6, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowCallAndSms:Z

    iput-object p7, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mControls:Landroid/view/View;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mPhoneCallDetails:[Lcom/android/contacts/PhoneCallDetails;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mPhoneCallDetails:[Lcom/android/contacts/PhoneCallDetails;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mPhoneCallDetails:[Lcom/android/contacts/PhoneCallDetails;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mNumber:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/contacts/calllog/CallDetailHistoryAdapter;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallDetailHistoryAdapter;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mControls:Landroid/view/View;

    return-object v0
.end method

.method private formatDuration(J)Ljava/lang/String;
    .locals 9
    .param p1    # J

    const-wide/16 v5, 0x3c

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    cmp-long v4, p1, v5

    if-ltz v4, :cond_0

    div-long v0, p1, v5

    mul-long v4, v0, v5

    sub-long/2addr p1, v4

    :cond_0
    move-wide v2, p1

    iget-object v4, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mContext:Landroid/content/Context;

    const v5, 0x7f0c0181

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mPhoneCallDetails:[Lcom/android/contacts/PhoneCallDetails;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mPhoneCallDetails:[Lcom/android/contacts/PhoneCallDetails;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    add-int/lit8 v0, p1, -0x1

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 31
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p1, :cond_d

    const/4 v3, 0x0

    invoke-static {}, Lcom/mediatek/contacts/util/VvmUtils;->isVvmEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f04000c

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    :goto_0
    const v2, 0x7f070053

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowVoicemail:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/phone/SIMInfoWrapper;->getInsertedSimCount()I

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x0

    :goto_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    const v2, 0x7f070054

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowCallAndSms:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v14, v2}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f07004a

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    if-eqz v28, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowCallAndSms:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const v2, 0x7f07005a

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v29

    const v2, 0x7f07005c

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowVoicemail:Z

    if-eqz v2, :cond_8

    const/16 v2, 0x8

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_6
    const v2, 0x7f07005b

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowCallAndSms:Z

    if-eqz v2, :cond_a

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f070059

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mNumber:Ljava/lang/String;

    const-string v5, "ExtenstionForRCS"

    const v6, 0x7f07005e

    const v7, 0x7f07005e

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v2 .. v12}, Lcom/android/contacts/ext/CallDetailExtension;->setViewVisible(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;IIIIIII)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowCallAndSms:Z

    if-eqz v2, :cond_b

    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowCallAndSms:Z

    if-eqz v2, :cond_c

    const/4 v2, 0x0

    :goto_9
    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/view/View;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mHeaderFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    :goto_a
    return-object v3

    :cond_1
    move-object/from16 v3, p2

    goto/16 :goto_0

    :cond_2
    const/16 v2, 0x8

    goto/16 :goto_1

    :cond_3
    if-nez p2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f04000d

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    :goto_b
    goto/16 :goto_2

    :cond_4
    move-object/from16 v3, p2

    goto :goto_b

    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_3

    :cond_6
    const/16 v2, 0x8

    goto/16 :goto_4

    :cond_7
    const/16 v2, 0x8

    goto/16 :goto_5

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mShowCallAndSms:Z

    if-eqz v2, :cond_9

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x0

    :goto_c
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    :cond_9
    const/16 v2, 0x8

    goto :goto_c

    :cond_a
    const/16 v2, 0x8

    goto/16 :goto_7

    :cond_b
    const/16 v2, 0x8

    goto :goto_8

    :cond_c
    const/16 v2, 0x8

    goto :goto_9

    :cond_d
    if-nez p2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f04000b

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v25

    :goto_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mPhoneCallDetails:[Lcom/android/contacts/PhoneCallDetails;

    add-int/lit8 v4, p1, -0x1

    aget-object v20, v2, v4

    const v2, 0x7f070055

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/android/contacts/calllog/CallTypeIconsView;

    const v2, 0x7f070056

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    const v2, 0x7f070057

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    const v2, 0x7f070058

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v20

    iget v15, v0, Lcom/android/contacts/PhoneCallDetails;->callType:I

    invoke-virtual/range {v16 .. v16}, Lcom/android/contacts/calllog/CallTypeIconsView;->clear()V

    move-object/from16 v0, v20

    iget v13, v0, Lcom/android/contacts/PhoneCallDetails;->vtCall:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v15, v13}, Lcom/android/contacts/calllog/CallTypeIconsView;->set(II)V

    const-string v2, "CallDetailHistoryAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IP prefix:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/android/contacts/PhoneCallDetails;->ipPrefix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " position: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/android/contacts/PhoneCallDetails;->ipPrefix:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    const/4 v2, 0x2

    if-ne v15, v2, :cond_f

    const-string v2, "CallDetailHistoryAdapter "

    const-string v4, " ipPrefix"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0c00ad

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/android/contacts/PhoneCallDetails;->ipPrefix:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v20

    iget-wide v5, v0, Lcom/android/contacts/PhoneCallDetails;->date:J

    move-object/from16 v0, v20

    iget-wide v7, v0, Lcom/android/contacts/PhoneCallDetails;->date:J

    const/16 v9, 0x17

    invoke-static/range {v4 .. v9}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;

    move-result-object v2

    move-object/from16 v0, v20

    iget-wide v4, v0, Lcom/android/contacts/PhoneCallDetails;->duration:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->formatDuration(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "ExtensionForOP01"

    move-object/from16 v0, v21

    invoke-virtual {v2, v15, v0, v4, v5}, Lcom/android/contacts/ext/CallDetailExtension;->setTextView(ILandroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v3, v25

    goto/16 :goto_a

    :cond_e
    move-object/from16 v25, p2

    goto/16 :goto_d

    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallDetailHistoryAdapter;->mCallTypeHelper:Lcom/android/contacts/calllog/CallTypeHelper;

    invoke-virtual {v2, v15}, Lcom/android/contacts/calllog/CallTypeHelper;->getCallTypeText(I)Ljava/lang/CharSequence;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_e
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
