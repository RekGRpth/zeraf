.class public Lcom/android/contacts/calllog/CallLogFragment;
.super Landroid/app/ListFragment;
.source "CallLogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;
.implements Lcom/android/contacts/calllog/CallLogQueryHandler$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/contacts/calllog/CallLogFragment$PhoneNumberActionModeCallback;,
        Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;,
        Lcom/android/contacts/calllog/CallLogFragment$CustomContentObserver;
    }
.end annotation


# static fields
.field private static final EMPTY_LOADER_ID:I = 0x0

.field public static final EXTRA_CALL_LOG_IDS:Ljava/lang/String; = "EXTRA_CALL_LOG_IDS"

.field private static final FLAG_FILTER_MODE_AUTO_REJECTED:I = 0x1

.field private static final FLAG_FILTER_MODE_NORMAL:I = 0x0

.field public static ISTABLET_LAND:Z = false

.field private static final PROVIDER_STATUS_CHANGING_LOCALE:I = 0x4e2

.field private static final PROVIDER_STATUS_UPGRATING:I = 0x4e3

.field private static final SETFIRSTTAG:I = 0x65

.field private static final SIM_INFO_UPDATE_MESSAGE:I = 0x64

.field private static final TAB_INDEX_CALL_LOG:I = 0x1

.field private static final TAG:Ljava/lang/String; = "CallLogFragment"

.field private static final WAITING_DESCRIPTION_PADDING:I = 0xa

.field private static final WAIT_CURSOR_DELAY_TIME:J = 0x1f4L

.field private static final WAIT_CURSOR_START:I = 0x4ce

.field private static mPosition:I


# instance fields
.field private convertView:Landroid/view/View;

.field private divider:Landroid/view/View;

.field public handle:Landroid/os/Handler;

.field private historyList:Landroid/widget/ListView;

.field private icon:Landroid/widget/ImageView;

.field private mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

.field private mAsyncTaskExecutor:Lcom/android/contacts/util/AsyncTaskExecutor;

.field private mCallDetail:Landroid/view/View;

.field private mCallLogFetched:Z

.field private final mCallLogObserver:Landroid/database/ContentObserver;

.field private mCallLogQueryHandler:Lcom/android/contacts/calllog/CallLogQueryHandler;

.field private mCallTypeHelper:Lcom/android/contacts/calllog/CallTypeHelper;

.field private mContactBackgroundView:Landroid/widget/ImageView;

.field private mContactInfoHelper:Lcom/android/contacts/calllog/ContactInfoHelper;

.field private mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

.field private final mContactsObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mControls:Landroid/view/View;

.field private mConvertView1:Landroid/view/View;

.field private mConvertView2:Landroid/view/View;

.field private mEmptyLoaderRunning:Z

.field private mEmptyTitle:Landroid/widget/TextView;

.field private final mFourthActionListener:Landroid/view/View$OnClickListener;

.field private mHandler:Landroid/os/Handler;

.field private mHasEditNumberBeforeCallOption:Z

.field private mHasSms:Z

.field private mHeader:Landroid/view/View;

.field private mHeaderOverlayView:Landroid/view/View;

.field private mHeaderTextView:Landroid/widget/TextView;

.field mInflater:Landroid/view/LayoutInflater;

.field private mIsFinished:Z

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mLayoutAutorejected:Landroid/widget/LinearLayout;

.field private mLayoutSearchbutton:Landroid/widget/LinearLayout;

.field private mLoadingContact:Landroid/widget/TextView;

.field private mLoadingContainer:Landroid/view/View;

.field private mMainActionPushLayerView:Landroid/widget/ImageButton;

.field private mMainActionView:Landroid/widget/ImageView;

.field private mMenuVisible:Z

.field private mModeFlag:I

.field private mNumber:Ljava/lang/String;

.field private mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

.field private mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

.field private mPhoneNumberActionMode:Landroid/view/ActionMode;

.field private mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

.field private mPhoneNumberLabelToCopy:Ljava/lang/CharSequence;

.field private mPhoneNumberToCopy:Ljava/lang/CharSequence;

.field private mPhoto:Landroid/view/View;

.field private final mPrimaryActionListener:Landroid/view/View$OnClickListener;

.field private final mPrimaryLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mProgress:Landroid/widget/ProgressBar;

.field private mRefreshDataRequired:Z

.field mResources:Landroid/content/res/Resources;

.field private mScrollToTop:Z

.field private final mSecondaryActionListener:Landroid/view/View$OnClickListener;

.field public mSelectResDialog:Landroid/app/AlertDialog;

.field private mSeparator:Landroid/view/View;

.field private mSeparator01:Landroid/view/View;

.field private mSeparator02:Landroid/view/View;

.field private mSimName:Landroid/widget/TextView;

.field private mStatusMessageAction:Landroid/widget/TextView;

.field private mStatusMessageText:Landroid/widget/TextView;

.field private mStatusMessageView:Landroid/view/View;

.field private final mThirdActionListener:Landroid/view/View$OnClickListener;

.field private mTypeFilterAll:Landroid/widget/Button;

.field private mTypeFilterIncoming:Landroid/widget/Button;

.field private mTypeFilterMissed:Landroid/widget/Button;

.field private mTypeFilterOutgoing:Landroid/widget/Button;

.field private mViewRestored:Landroid/view/View;

.field private mVoicemailSourcesAvailable:Z

.field private mVoicemailStatusFetched:Z

.field private mVoicemailStatusHelper:Lcom/android/contacts/voicemail/VoicemailStatusHelper;

.field private mainAction:Landroid/view/View;

.field private text:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/contacts/calllog/CallLogFragment;->mPosition:I

    sput-boolean v0, Lcom/android/contacts/calllog/CallLogFragment;->ISTABLET_LAND:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    iput-boolean v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mVoicemailSourcesAvailable:Z

    iput-boolean v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHasSms:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mNumber:Ljava/lang/String;

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$CustomContentObserver;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$CustomContentObserver;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$CustomContentObserver;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$CustomContentObserver;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactsObserver:Landroid/database/ContentObserver;

    iput-boolean v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mRefreshDataRequired:Z

    iput-boolean v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mMenuVisible:Z

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$1;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$1;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->handle:Landroid/os/Handler;

    iput v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mModeFlag:I

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$2;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$2;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$4;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$4;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPrimaryActionListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$5;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$5;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPrimaryLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$6;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$6;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSecondaryActionListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$7;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$7;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mThirdActionListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/contacts/calllog/CallLogFragment$8;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogFragment$8;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mFourthActionListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$002(Lcom/android/contacts/calllog/CallLogFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mRefreshDataRequired:Z

    return p1
.end method

.method static synthetic access$1000(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/contacts/calllog/CallLogFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mScrollToTop:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/contacts/calllog/CallLogFragment;)V
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->refreshData()V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/contacts/calllog/CallLogFragment;Landroid/net/Uri;)Lcom/android/contacts/PhoneCallDetails;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->getPhoneCallDetailsForUri(Landroid/net/Uri;)Lcom/android/contacts/PhoneCallDetails;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/contacts/calllog/CallLogFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/contacts/calllog/CallLogFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mNumber:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHeaderTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/contacts/calllog/CallLogFragment;)Lcom/android/contacts/PhoneCallDetailsHelper;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/contacts/calllog/CallLogFragment;)Lcom/android/contacts/calllog/PhoneNumberHelper;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mMainActionView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mMainActionPushLayerView:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/contacts/calllog/CallLogFragment;)Lcom/mediatek/contacts/calllog/CallLogListItemView;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHeaderOverlayView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/contacts/calllog/CallLogFragment;Lcom/mediatek/contacts/calllog/CallLogListItemView;)Lcom/mediatek/contacts/calllog/CallLogListItemView;
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Lcom/mediatek/contacts/calllog/CallLogListItemView;

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/contacts/calllog/CallLogFragment;)Z
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHasSms:Z

    return v0
.end method

.method static synthetic access$2200(Lcom/android/contacts/calllog/CallLogFragment;I)V
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->setSimInfo(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/contacts/calllog/CallLogFragment;Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;)V
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->configureCallButton(Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/contacts/calllog/CallLogFragment;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberToCopy:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/android/contacts/calllog/CallLogFragment;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberToCopy:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/android/contacts/calllog/CallLogFragment;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberLabelToCopy:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/android/contacts/calllog/CallLogFragment;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberLabelToCopy:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$2602(Lcom/android/contacts/calllog/CallLogFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHasEditNumberBeforeCallOption:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/android/contacts/calllog/CallLogFragment;)Lcom/android/contacts/calllog/CallTypeHelper;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallTypeHelper:Lcom/android/contacts/calllog/CallTypeHelper;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mControls:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->historyList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/contacts/calllog/CallLogFragment;)Lcom/mediatek/contacts/calllog/CallLogListAdapter;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHeader:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoto:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/contacts/calllog/CallLogFragment;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->loadContactPhotos(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$3400(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallDetail:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator02:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView2:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/android/contacts/calllog/CallLogFragment;)Z
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->finishPhoneNumerSelectedActionModeIfShown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/android/contacts/calllog/CallLogFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->startPhoneNumberSelectedActionMode(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/contacts/calllog/CallLogFragment;Landroid/content/Intent;)[Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->getCallLogEntryUris(Landroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4002(Lcom/android/contacts/calllog/CallLogFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/contacts/calllog/CallLogFragment;[Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # [Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->updateData([Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/contacts/calllog/CallLogFragment;)Z
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mIsFinished:Z

    return v0
.end method

.method static synthetic access$700(Lcom/android/contacts/calllog/CallLogFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mEmptyTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/contacts/calllog/CallLogFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogFragment;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContact:Landroid/widget/TextView;

    return-object v0
.end method

.method private changeButton(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x7f020018

    const v3, 0x7f020017

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeButton(), view = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    if-nez p1, :cond_1

    iget v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mModeFlag:I

    if-eq v6, v1, :cond_0

    iput v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mModeFlag:I

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "calllog_type_filter"

    const/16 v2, 0x4e35

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0, v5}, Lcom/android/contacts/calllog/CallLogFragment;->setAutoRejectedModeVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mModeFlag:I

    if-eqz v1, :cond_2

    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/android/contacts/calllog/CallLogFragment;->setAutoRejectedModeVisibility(I)V

    iput v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mModeFlag:I

    :cond_2
    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mViewRestored:Landroid/view/View;

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterAll:Landroid/widget/Button;

    if-eq p1, v1, :cond_3

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterAll:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_1
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterOutgoing:Landroid/widget/Button;

    if-eq p1, v1, :cond_4

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterOutgoing:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_2
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterIncoming:Landroid/widget/Button;

    if-eq p1, v1, :cond_5

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterIncoming:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_3
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterMissed:Landroid/widget/Button;

    if-eq p1, v1, :cond_6

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterMissed:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterAll:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterOutgoing:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterIncoming:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    :cond_6
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterMissed:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private configureCallButton(Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;)V
    .locals 14
    .param p1    # Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;

    const v13, 0x7f09004c

    const/16 v12, 0x8

    const/4 v11, 0x0

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mainAction:Landroid/view/View;

    iget-object v10, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPrimaryActionListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mainAction:Landroid/view/View;

    invoke-virtual {v9, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mainAction:Landroid/view/View;

    iget-object v10, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->primaryDescription:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mainAction:Landroid/view/View;

    iget-object v10, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPrimaryLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v9, v10}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->secondaryIntent:Landroid/content/Intent;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->icon:Landroid/widget/ImageView;

    iget-object v10, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSecondaryActionListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->icon:Landroid/widget/ImageView;

    iget v10, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->secondaryIcon:I

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->icon:Landroid/widget/ImageView;

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->icon:Landroid/widget/ImageView;

    invoke-virtual {v9, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->icon:Landroid/widget/ImageView;

    iget-object v10, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->secondaryDescription:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->divider:Landroid/view/View;

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->text:Landroid/widget/TextView;

    iget-object v10, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->text:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->text:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v9, v10, v11, v11, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    const v10, 0x7f07004f

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->label:Ljava/lang/CharSequence;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    const v10, 0x7f070063

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    const v10, 0x7f070062

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-virtual {v5, v9, v11, v11, v11}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->label:Ljava/lang/CharSequence;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v5, v12}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator01:Landroid/view/View;

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator02:Landroid/view/View;

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView1:Landroid/view/View;

    const v10, 0x7f070061

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->thirdIntent:Landroid/content/Intent;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mThirdActionListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v6, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->thirdDescription:Ljava/lang/String;

    invoke-virtual {v6, v9}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView1:Landroid/view/View;

    const v10, 0x7f070067

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->videoText:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView1:Landroid/view/View;

    const v10, 0x7f070068

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->label:Ljava/lang/CharSequence;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v7, v12}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator01:Landroid/view/View;

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView1:Landroid/view/View;

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->fourthIntent:Landroid/content/Intent;

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView2:Landroid/view/View;

    const v10, 0x7f070066

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mFourthActionListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->fourthDescription:Ljava/lang/String;

    invoke-virtual {v1, v9}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView2:Landroid/view/View;

    const v10, 0x7f07006b

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->ipText:Ljava/lang/String;

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView2:Landroid/view/View;

    const v10, 0x7f07006c

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->label:Ljava/lang/CharSequence;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v2, v12}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    return-void

    :cond_0
    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->icon:Landroid/widget/ImageView;

    invoke-virtual {v9, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->divider:Landroid/view/View;

    invoke-virtual {v9, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_1
    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->label:Ljava/lang/CharSequence;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f09004a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-virtual {v4, v11, v11, v9, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    :cond_2
    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_3
    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->label:Ljava/lang/CharSequence;

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_4
    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator01:Landroid/view/View;

    invoke-virtual {v9, v12}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView1:Landroid/view/View;

    invoke-virtual {v9, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_5
    iget-object v9, p1, Lcom/android/contacts/calllog/CallLogFragment$ViewEntry;->label:Ljava/lang/CharSequence;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    :cond_6
    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator02:Landroid/view/View;

    invoke-virtual {v9, v12}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView2:Landroid/view/View;

    invoke-virtual {v9, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5
.end method

.method private destroyEmptyLoaderIfAllDataFetched()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogFetched:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mVoicemailStatusFetched:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mEmptyLoaderRunning:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mEmptyLoaderRunning:Z

    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/LoaderManager;->destroyLoader(I)V

    :cond_0
    return-void
.end method

.method private finishPhoneNumerSelectedActionModeIfShown()Z
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getCallLogEntryUris(Landroid/content/Intent;)[Landroid/net/Uri;
    .locals 9
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_1

    const-string v7, "content://call_log/callsjoindataview"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v5}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v7, 0x1

    new-array v6, v7, [Landroid/net/Uri;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    :cond_0
    return-object v6

    :cond_1
    const-string v7, "EXTRA_CALL_LOG_IDS"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v2

    array-length v7, v2

    new-array v6, v7, [Landroid/net/Uri;

    const/4 v3, 0x0

    :goto_0
    array-length v7, v2

    if-ge v3, v7, :cond_0

    const-string v7, "content://call_log/callsjoindataview"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    aget-wide v7, v2, v3

    invoke-static {v4, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    aput-object v7, v6, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private getPhoneCallDetailsForUri(Landroid/net/Uri;)Lcom/android/contacts/PhoneCallDetails;
    .locals 24
    .param p1    # Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallLogFragment;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Cannot find mContext"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallLogFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/android/contacts/calllog/CallLogQuery;->PROJECTION_CALLS_JOIN_DATAVIEW:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    if-eqz v21, :cond_1

    :try_start_0
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot find content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    if-eqz v21, :cond_2

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v2

    :cond_3
    :try_start_1
    invoke-static/range {v21 .. v21}, Lcom/android/contacts/calllog/ContactInfo;->fromCursor(Landroid/database/Cursor;)Lcom/android/contacts/calllog/ContactInfo;

    move-result-object v22

    const/16 v2, 0x14

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v16, 0x0

    if-eqz v23, :cond_7

    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "number = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/contacts/calllog/PhoneNumberHelper;->canPlaceCallsTo(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    move-object/from16 v0, v22

    iget v4, v0, Lcom/android/contacts/calllog/ContactInfo;->simId:I

    invoke-virtual {v2, v3, v4}, Lcom/android/contacts/calllog/PhoneNumberHelper;->isVoiceMailNumberForMtk(Ljava/lang/CharSequence;I)Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/contacts/calllog/PhoneNumberHelper;->isEmergencyNumber(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/android/contacts/calllog/PhoneNumberHelper;->getDisplayNumber(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    iput-object v2, v0, Lcom/android/contacts/calllog/ContactInfo;->formattedNumber:Ljava/lang/String;

    const-string v2, ""

    move-object/from16 v0, v22

    iput-object v2, v0, Lcom/android/contacts/calllog/ContactInfo;->name:Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, v22

    iput v2, v0, Lcom/android/contacts/calllog/ContactInfo;->nNumberTypeId:I

    const-string v2, ""

    move-object/from16 v0, v22

    iput-object v2, v0, Lcom/android/contacts/calllog/ContactInfo;->label:Ljava/lang/String;

    const/16 v16, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, v22

    iput-object v2, v0, Lcom/android/contacts/calllog/ContactInfo;->lookupUri:Landroid/net/Uri;

    :cond_5
    new-instance v2, Lcom/android/contacts/PhoneCallDetails;

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/android/contacts/calllog/ContactInfo;->formattedNumber:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/android/contacts/calllog/ContactInfo;->countryIso:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v6, v0, Lcom/android/contacts/calllog/ContactInfo;->geocode:Ljava/lang/String;

    move-object/from16 v0, v22

    iget v7, v0, Lcom/android/contacts/calllog/ContactInfo;->type:I

    move-object/from16 v0, v22

    iget-wide v8, v0, Lcom/android/contacts/calllog/ContactInfo;->date:J

    move-object/from16 v0, v22

    iget-wide v10, v0, Lcom/android/contacts/calllog/ContactInfo;->duration:J

    move-object/from16 v0, v22

    iget-object v12, v0, Lcom/android/contacts/calllog/ContactInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v22

    iget v13, v0, Lcom/android/contacts/calllog/ContactInfo;->nNumberTypeId:I

    move-object/from16 v0, v22

    iget-object v14, v0, Lcom/android/contacts/calllog/ContactInfo;->label:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v15, v0, Lcom/android/contacts/calllog/ContactInfo;->lookupUri:Landroid/net/Uri;

    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/contacts/calllog/ContactInfo;->simId:I

    move/from16 v17, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/contacts/calllog/ContactInfo;->vtCall:I

    move/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/contacts/calllog/ContactInfo;->ipPrefix:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v2 .. v20}, Lcom/android/contacts/PhoneCallDetails;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/net/Uri;Landroid/net/Uri;IIILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v21, :cond_6

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_6
    return-object v2

    :cond_7
    const/16 v16, 0x0

    goto/16 :goto_0
.end method

.method private loadContactPhotos(Landroid/net/Uri;)V
    .locals 4
    .param p1    # Landroid/net/Uri;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactBackgroundView:Landroid/widget/ImageView;

    const v1, 0x7f020062

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactBackgroundView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactBackgroundView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/android/contacts/ContactPhotoManager;->loadPhoto(Landroid/widget/ImageView;Landroid/net/Uri;IZ)V

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallLogFragment"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private refreshData()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mRefreshDataRequired:Z

    if-eqz v0, :cond_0

    const-string v0, "refreshData()"

    invoke-direct {p0, v0}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/contacts/calllog/CallLogFragment;->startCallsQuery()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mRefreshDataRequired:Z

    :cond_0
    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->updateOnEntry()V

    return-void
.end method

.method private removeMissedCallNotifications()V
    .locals 4

    :try_start_0
    const-string v2, "phone"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->cancelMissedCallsNotification()V

    :goto_0
    return-void

    :cond_0
    const-string v2, "CallLogFragment"

    const-string v3, "Telephony service is null, can\'t call cancelMissedCallsNotification"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "CallLogFragment"

    const-string v3, "Failed to clear missed calls notification due to remote exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setAutoRejectedModeVisibility(I)V
    .locals 3
    .param p1    # I

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLayoutSearchbutton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLayoutAutorejected:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLayoutAutorejected:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLayoutSearchbutton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setSimInfo(I)V
    .locals 9
    .param p1    # I

    const/4 v8, 0x4

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090065

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090066

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v5, v2, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    const/4 v6, -0x2

    if-ne p1, v6, :cond_0

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    const v7, 0x7f0200dc

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    const v7, 0x7f0c0019

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v5, v2, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimDisplayNameById(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v5, v2, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_1
    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/mediatek/phone/SIMInfoWrapper;->getInsertedSimColorById(I)I

    move-result v0

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimBackgroundDarkResByColorId(I)I

    move-result v3

    const/4 v6, -0x1

    if-eq v6, v0, :cond_3

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v5, v2, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    const v7, 0x7f0200dd

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v5, v2, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method private startPhoneNumberSelectedActionMode(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/contacts/calllog/CallLogFragment$PhoneNumberActionModeCallback;

    invoke-direct {v1, p0, p1}, Lcom/android/contacts/calllog/CallLogFragment$PhoneNumberActionModeCallback;-><init>(Lcom/android/contacts/calllog/CallLogFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberActionMode:Landroid/view/ActionMode;

    return-void
.end method

.method private varargs updateData([Landroid/net/Uri;)V
    .locals 4
    .param p1    # [Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAsyncTaskExecutor:Lcom/android/contacts/util/AsyncTaskExecutor;

    sget-object v1, Lcom/android/contacts/CallDetailActivity$Tasks;->UPDATE_PHONE_CALL_DETAILS:Lcom/android/contacts/CallDetailActivity$Tasks;

    new-instance v2, Lcom/android/contacts/calllog/CallLogFragment$1UpdateContactDetailsTask;

    invoke-direct {v2, p0, p1}, Lcom/android/contacts/calllog/CallLogFragment$1UpdateContactDetailsTask;-><init>(Lcom/android/contacts/calllog/CallLogFragment;[Landroid/net/Uri;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-interface {v0, v1, v2, v3}, Lcom/android/contacts/util/AsyncTaskExecutor;->submit(Ljava/lang/Object;Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private updateOnEntry()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/contacts/calllog/CallLogFragment;->updateOnTransition(Z)V

    return-void
.end method

.method private updateOnExit()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/contacts/calllog/CallLogFragment;->updateOnTransition(Z)V

    return-void
.end method

.method private updateOnTransition(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-nez v1, :cond_2

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogQueryHandler:Lcom/android/contacts/calllog/CallLogQueryHandler;

    invoke-virtual {v1}, Lcom/android/contacts/calllog/CallLogQueryHandler;->markMissedCallsAsRead()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/contacts/activities/DialtactsActivity;

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/android/contacts/activities/DialtactsActivity;

    invoke-virtual {v1}, Lcom/android/contacts/activities/DialtactsActivity;->getCurrentFragmentId()I

    move-result v1

    if-ne v2, v1, :cond_2

    invoke-static {}, Lcom/mediatek/contacts/util/VvmUtils;->isVvmEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->updateVoicemailNotifications()V

    :cond_1
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogQueryHandler:Lcom/android/contacts/calllog/CallLogQueryHandler;

    invoke-virtual {v1}, Lcom/android/contacts/calllog/CallLogQueryHandler;->markNewCallsAsOld()V

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->removeMissedCallNotifications()V

    :cond_2
    return-void
.end method

.method private updateVoicemailNotifications()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/contacts/calllog/CallLogNotificationsService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.android.contacts.calllog.UPDATE_NOTIFICATIONS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public configureScreenFromIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "vnd.android.cursor.dir/calls"

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mScrollToTop:Z

    return-void
.end method

.method public fetchCalls()V
    .locals 8

    const/16 v7, 0x4e2b

    const/16 v6, 0x4e21

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v4, "CallLogFragment"

    const-string v5, " fetchCalls(), but this.getActivity() is null, use default value"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogQueryHandler:Lcom/android/contacts/calllog/CallLogQueryHandler;

    invoke-virtual {v4, v6, v7}, Lcom/android/contacts/calllog/CallLogQueryHandler;->fetchCallsJionDataView(II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "calllog_sim_filter"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "calllog_type_filter"

    invoke-interface {v1, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    iget-object v4, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogQueryHandler:Lcom/android/contacts/calllog/CallLogQueryHandler;

    invoke-virtual {v4, v2, v3}, Lcom/android/contacts/calllog/CallLogQueryHandler;->fetchCallsJionDataView(II)V

    goto :goto_0
.end method

.method getAdapter()Lcom/mediatek/contacts/calllog/CallLogListAdapter;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    return-object v0
.end method

.method public isAutoRejectedFilterMode()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mModeFlag:I

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackHandled()V
    .locals 3

    const-string v0, "CallLogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBackHandled() Mode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mModeFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " View:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mViewRestored:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/contacts/calllog/CallLogFragment;->isAutoRejectedFilterMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mViewRestored:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterAll:Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mViewRestored:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mViewRestored:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/contacts/calllog/CallLogFragment;->onClick(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public onCallsDeleted()V
    .locals 1

    const-string v0, "onCallsDeleted(), do nothing"

    invoke-direct {p0, v0}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onCallsFetched(Landroid/database/Cursor;)V
    .locals 7
    .param p1    # Landroid/database/Cursor;

    const/4 v4, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCallsFetched(), cursor = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {v2, v5}, Lcom/android/contacts/calllog/CallLogAdapter;->setLoading(Z)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {v2, p1}, Lcom/android/contacts/calllog/CallLogAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v2, v0, Lcom/android/contacts/activities/DialtactsActivity;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/contacts/activities/DialtactsActivity;

    invoke-virtual {v2}, Lcom/android/contacts/activities/DialtactsActivity;->getCurrentFragmentId()I

    move-result v2

    if-ne v4, v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_2
    iget-boolean v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mScrollToTop:Z

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setSelection(I)V

    iput-boolean v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mScrollToTop:Z

    :cond_3
    iput-boolean v4, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogFetched:Z

    const-string v2, "CallLogFragment"

    const-string v3, "onCallsFetched is call"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v4, p0, Lcom/android/contacts/calllog/CallLogFragment;->mIsFinished:Z

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContainer:Landroid/view/View;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x10a0001

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContainer:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContact:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallDetail:Landroid/view/View;

    if-eqz v2, :cond_5

    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallDetail:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    :goto_1
    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mEmptyTitle:Landroid/widget/TextView;

    const v3, 0x7f0c015c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->destroyEmptyLoaderIfAllDataFetched()V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->handle:Landroid/os/Handler;

    const/16 v3, 0x65

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallDetail:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick(), view id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const v2, 0x7f070079

    if-eq v1, v2, :cond_0

    const v2, 0x7f07007f

    if-eq v1, v2, :cond_0

    const v2, 0x7f07007c

    if-eq v1, v2, :cond_0

    const v2, 0x7f070082

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

    invoke-virtual {v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getSelectImageView()Landroid/widget/ImageView;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/mediatek/contacts/calllog/CallLogListAdapter;->setSelectedPosition(I)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

    :cond_1
    sparse-switch v1, :sswitch_data_0

    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mRefreshDataRequired:Z

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->refreshData()V

    return-void

    :sswitch_0
    const-string v2, "calllog_type_filter"

    const/16 v3, 0x4e2b

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->changeButton(Landroid/view/View;)V

    goto :goto_0

    :sswitch_1
    const-string v2, "calllog_type_filter"

    const/16 v3, 0x4e2e

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->changeButton(Landroid/view/View;)V

    goto :goto_0

    :sswitch_2
    const-string v2, "calllog_type_filter"

    const/16 v3, 0x4e2c

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->changeButton(Landroid/view/View;)V

    goto :goto_0

    :sswitch_3
    const-string v2, "calllog_type_filter"

    const/16 v3, 0x4e2d

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, p1}, Lcom/android/contacts/calllog/CallLogFragment;->changeButton(Landroid/view/View;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f070079 -> :sswitch_0
        0x7f07007c -> :sswitch_2
        0x7f07007f -> :sswitch_1
        0x7f070082 -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "CallLogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Performance test][Contacts] loading data start time: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/contacts/calllog/CallLogQueryHandler;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/contacts/calllog/CallLogQueryHandler;-><init>(Landroid/content/ContentResolver;Lcom/android/contacts/calllog/CallLogQueryHandler$Listener;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogQueryHandler:Lcom/android/contacts/calllog/CallLogQueryHandler;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CallLog;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0, v5}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v4}, Lcom/mediatek/phone/SIMInfoWrapper;->registerForSimInfoUpdate(Landroid/os/Handler;ILjava/lang/Object;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/android/contacts/ContactPhotoManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/ContactPhotoManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    invoke-static {}, Lcom/android/contacts/util/AsyncTaskExecutors;->createThreadPoolExecutor()Lcom/android/contacts/util/AsyncTaskExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAsyncTaskExecutor:Lcom/android/contacts/util/AsyncTaskExecutor;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mResources:Landroid/content/res/Resources;

    new-instance v0, Lcom/android/contacts/calllog/CallTypeHelper;

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/contacts/calllog/CallTypeHelper;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallTypeHelper:Lcom/android/contacts/calllog/CallTypeHelper;

    new-instance v0, Lcom/android/contacts/calllog/PhoneNumberHelper;

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mResources:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, Lcom/android/contacts/calllog/PhoneNumberHelper;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    new-instance v0, Lcom/android/contacts/PhoneCallDetailsHelper;

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallTypeHelper:Lcom/android/contacts/calllog/CallTypeHelper;

    iget-object v3, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/android/contacts/PhoneCallDetailsHelper;-><init>(Landroid/content/res/Resources;Lcom/android/contacts/calllog/CallTypeHelper;Lcom/android/contacts/calllog/PhoneNumberHelper;Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const v0, 0x7f100004

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v9, 0x7f070047

    const/16 v8, 0xa

    const/4 v7, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    const v2, 0x7f040012

    invoke-virtual {p1, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070079

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterAll:Landroid/widget/Button;

    const v2, 0x7f07007f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterOutgoing:Landroid/widget/Button;

    const v2, 0x7f07007c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterIncoming:Landroid/widget/Button;

    const v2, 0x7f070082

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterMissed:Landroid/widget/Button;

    const v2, 0x7f070076

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLayoutAutorejected:Landroid/widget/LinearLayout;

    const v2, 0x7f070078

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLayoutSearchbutton:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterAll:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterOutgoing:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterIncoming:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterMissed:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0701cc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContainer:Landroid/view/View;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContainer:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mEmptyTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mEmptyTitle:Landroid/widget/TextView;

    const v3, 0x7f0c015c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const v2, 0x7f0701ce

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContact:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContact:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContact:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x1030042

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContact:Landroid/widget/TextView;

    invoke-virtual {v2, v8, v5, v8, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContact:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mLoadingContact:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f0701cd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mProgress:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "calllog_type_filter"

    const/16 v3, 0x4e2b

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v2, "calllog_sim_filter"

    const/16 v3, 0x4e25

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mTypeFilterAll:Landroid/widget/Button;

    invoke-direct {p0, v2}, Lcom/android/contacts/calllog/CallLogFragment;->changeButton(Landroid/view/View;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    sput-boolean v7, Lcom/android/contacts/calllog/CallLogFragment;->ISTABLET_LAND:Z

    :goto_0
    sget-boolean v2, Lcom/android/contacts/calllog/CallLogFragment;->ISTABLET_LAND:Z

    if-eqz v2, :cond_0

    const v2, 0x7f070049

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHeaderTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHeaderOverlayView:Landroid/view/View;

    const v2, 0x7f070048

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mMainActionView:Landroid/widget/ImageView;

    const v2, 0x7f07004a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mMainActionPushLayerView:Landroid/widget/ImageButton;

    const v2, 0x7f070045

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactBackgroundView:Landroid/widget/ImageView;

    const v2, 0x7f07005f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSimName:Landroid/widget/TextView;

    const v2, 0x7f07004c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    const v3, 0x7f070051

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->icon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    const v3, 0x7f070050

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->divider:Landroid/view/View;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    const v3, 0x7f07004e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->text:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->convertView:Landroid/view/View;

    const v3, 0x7f07004d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mainAction:Landroid/view/View;

    const v2, 0x7f070041

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->historyList:Landroid/widget/ListView;

    const v2, 0x7f070042

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mControls:Landroid/view/View;

    const v2, 0x7f070044

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mPhoto:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHeader:Landroid/view/View;

    const v2, 0x7f070046

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator:Landroid/view/View;

    const v2, 0x7f070040

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallDetail:Landroid/view/View;

    const v2, 0x7f070059

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator01:Landroid/view/View;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator01:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f07005b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator02:Landroid/view/View;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSeparator02:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f070065

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView1:Landroid/view/View;

    const v2, 0x7f07006a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mConvertView2:Landroid/view/View;

    :cond_0
    return-object v1

    :cond_1
    sput-boolean v5, Lcom/android/contacts/calllog/CallLogFragment;->ISTABLET_LAND:Z

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/contacts/calllog/CallLogAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mContactsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/SIMInfoWrapper;->unregisterForSimInfoUpdate(Landroid/os/Handler;)V

    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 9
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    const/4 v8, 0x1

    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-interface {v5}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-eqz p2, :cond_0

    instance-of v5, p2, Lcom/mediatek/contacts/calllog/CallLogListItemView;

    if-nez v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/Exception;

    const-string v6, "CallLogFragment exception"

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void

    :cond_1
    move-object v4, p2

    check-cast v4, Lcom/mediatek/contacts/calllog/CallLogListItemView;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v5, "CallLogFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "context is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/contacts/calllog/IntentProvider;

    if-eqz v3, :cond_4

    invoke-virtual {v3, v0}, Lcom/android/contacts/calllog/IntentProvider;->getIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    const-string v5, "follow_sim_management"

    invoke-virtual {v2, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-boolean v5, Lcom/android/contacts/calllog/CallLogFragment;->ISTABLET_LAND:Z

    if-eqz v5, :cond_5

    if-eqz v4, :cond_3

    iget-object v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {v5}, Lcom/mediatek/contacts/calllog/CallLogListAdapter;->getSelectedPosition()I

    move-result v5

    invoke-virtual {v4}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getTagId()I

    move-result v6

    if-eq v5, v6, :cond_2

    iget-object v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

    invoke-virtual {v5, v4, v6}, Lcom/mediatek/contacts/calllog/CallLogListAdapter;->itemSetSelect(Lcom/mediatek/contacts/calllog/CallLogListItemView;Lcom/mediatek/contacts/calllog/CallLogListItemView;)V

    :cond_2
    iput-object v4, p0, Lcom/android/contacts/calllog/CallLogFragment;->mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

    :cond_3
    invoke-direct {p0, v2}, Lcom/android/contacts/calllog/CallLogFragment;->getCallLogEntryUris(Landroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/contacts/calllog/CallLogFragment;->updateData([Landroid/net/Uri;)V

    :cond_4
    :goto_1
    iget-object v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {v4}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getTagId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/mediatek/contacts/calllog/CallLogListAdapter;->setSelectedPosition(I)V

    goto :goto_0

    :cond_5
    invoke-virtual {v3, v0}, Lcom/android/contacts/calllog/IntentProvider;->getIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "follow_sim_management"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onOptionsItemSelected(), item id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/mediatek/contacts/activities/CallLogMultipleDeleteActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "calllog_sim_filter"

    const/16 v5, 0x4e21

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iget-object v4, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogQueryHandler:Lcom/android/contacts/calllog/CallLogQueryHandler;

    const/16 v5, 0x4e35

    invoke-virtual {v4, v2, v5}, Lcom/android/contacts/calllog/CallLogQueryHandler;->fetchCallsJionDataView(II)V

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/android/contacts/calllog/CallLogFragment;->changeButton(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0701d9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const v2, 0x7f0701d9

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {v2}, Lcom/android/contacts/calllog/CallLogAdapter;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-boolean v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mVoicemailSourcesAvailable:Z

    if-nez v2, :cond_0

    const v2, 0x7f0701d7

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    const v2, 0x7f0701d8

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/contacts/calllog/CallLogFragment;->isAutoRejectedFilterMode()Z

    move-result v5

    if-nez v5, :cond_3

    :goto_1
    const-string v4, "ExtensionForOP01"

    invoke-virtual {v2, v3, v4}, Lcom/android/contacts/ext/CallDetailExtension;->isNeedAutoRejectedMenu(ZLjava/lang/String;)Z

    move-result v0

    const v2, 0x7f0701da

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    return-void

    :cond_2
    move v2, v4

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-static {}, Lcom/android/contacts/calllog/PhoneNumberHelper;->getVoiceMailNumber()V

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->refreshData()V

    const-string v0, "CallLogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Performance test][Contacts] loading data end time: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStart()V
    .locals 5

    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Lcom/android/contacts/util/EmptyLoader$Callback;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/contacts/util/EmptyLoader$Callback;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mEmptyLoaderRunning:Z

    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {v0}, Lcom/android/common/widget/GroupingListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->updateOnExit()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2}, Landroid/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/contacts/ContactsUtils;->getCurrentCountryIso(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Lcom/android/contacts/calllog/ContactInfoHelper;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Lcom/android/contacts/calllog/ContactInfoHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {v2, v3, p0, v4, p0}, Lcom/mediatek/contacts/calllog/CallLogListAdapter;-><init>(Landroid/content/Context;Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;Lcom/android/contacts/calllog/ContactInfoHelper;Lcom/android/contacts/calllog/CallLogFragment;)V

    iput-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {p0, v2}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    :cond_0
    return-void
.end method

.method public onVoicemailStatusFetched(Landroid/database/Cursor;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;

    return-void
.end method

.method public setMenuVisibility(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    iget-boolean v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mMenuVisible:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mMenuVisible:Z

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->updateOnExit()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/contacts/calllog/CallLogFragment;->refreshData()V

    goto :goto_0
.end method

.method public setOldItemView(Lcom/mediatek/contacts/calllog/CallLogListItemView;)V
    .locals 2
    .param p1    # Lcom/mediatek/contacts/calllog/CallLogListItemView;

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mOldItemView:Lcom/mediatek/contacts/calllog/CallLogListItemView;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getTagId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/calllog/CallLogListAdapter;->setSelectedPosition(I)V

    return-void
.end method

.method public showChoiceResourceDialog()V
    .locals 7

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0c0015

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v1, Lcom/android/contacts/calllog/CallLogFragment$3;

    invoke-direct {v1, p0}, Lcom/android/contacts/calllog/CallLogFragment$3;-><init>(Lcom/android/contacts/calllog/CallLogFragment;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v5, "calllog_sim_filter"

    const/16 v6, 0x4e25

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showChoiceResourceDialog() choiceItem "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/contacts/calllog/CallLogFragment;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5, v4, v0, v1}, Lcom/mediatek/contacts/widget/SimPickerDialog;->createSingleChoice(Landroid/content/Context;Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSelectResDialog:Landroid/app/AlertDialog;

    iget-object v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mSelectResDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public startCallsQuery()V
    .locals 9

    iget-object v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mAdapter:Lcom/mediatek/contacts/calllog/CallLogListAdapter;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/android/contacts/calllog/CallLogAdapter;->setLoading(Z)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v5, "calllog_sim_filter"

    const/16 v6, 0x4e21

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v5, "calllog_type_filter"

    const/16 v6, 0x4e2b

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iget-object v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mCallLogQueryHandler:Lcom/android/contacts/calllog/CallLogQueryHandler;

    invoke-virtual {v5, v2, v4}, Lcom/android/contacts/calllog/CallLogQueryHandler;->fetchCallsJionDataView(II)V

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getCount()I

    move-result v0

    const-string v5, "CallLogFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "***********************count : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mIsFinished:Z

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/android/contacts/activities/DialtactsActivity;

    invoke-virtual {v5}, Lcom/android/contacts/activities/DialtactsActivity;->getProviderStatus()Lcom/android/contacts/list/ProviderStatusWatcher$Status;

    move-result-object v3

    if-nez v0, :cond_0

    const-string v5, "CallLogFragment"

    const-string v6, "call sendmessage"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x4ce

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    const-wide/16 v7, 0x1f4

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-virtual {p0, v3}, Lcom/android/contacts/calllog/CallLogFragment;->updateProviderStauts(Lcom/android/contacts/list/ProviderStatusWatcher$Status;)V

    :cond_0
    return-void
.end method

.method public updateProviderStauts(Lcom/android/contacts/list/ProviderStatusWatcher$Status;)V
    .locals 5
    .param p1    # Lcom/android/contacts/list/ProviderStatusWatcher$Status;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_2

    :cond_0
    const-string v1, "CallLogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateProviderStauts Error! providerStatus:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, -0x1

    iget v1, p1, Lcom/android/contacts/list/ProviderStatusWatcher$Status;->status:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const-string v1, "CallLogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateProviderStauts needn\'t handle msg:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/android/contacts/list/ProviderStatusWatcher$Status;->status:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const-string v1, "CallLogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateProviderStauts status:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/android/contacts/list/ProviderStatusWatcher$Status;->status:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " msgWhat:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    if-eq v1, v0, :cond_1

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x4e2

    goto :goto_1

    :pswitch_2
    const/16 v0, 0x4e3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
