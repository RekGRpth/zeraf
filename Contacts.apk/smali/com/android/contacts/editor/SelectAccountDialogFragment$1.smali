.class Lcom/android/contacts/editor/SelectAccountDialogFragment$1;
.super Ljava/lang/Object;
.source "SelectAccountDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/contacts/editor/SelectAccountDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

.field final synthetic val$accountAdapter:Lcom/android/contacts/util/AccountsListAdapter;


# direct methods
.method constructor <init>(Lcom/android/contacts/editor/SelectAccountDialogFragment;Lcom/android/contacts/util/AccountsListAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    iput-object p2, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->val$accountAdapter:Lcom/android/contacts/util/AccountsListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    iget-object v2, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->val$accountAdapter:Lcom/android/contacts/util/AccountsListAdapter;

    invoke-virtual {v2, p2}, Lcom/android/contacts/util/AccountsListAdapter;->getItem(I)Lcom/android/contacts/model/account/AccountWithDataSet;

    move-result-object v2

    # setter for: Lcom/android/contacts/editor/SelectAccountDialogFragment;->account:Lcom/android/contacts/model/account/AccountWithDataSet;
    invoke-static {v1, v2}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$002(Lcom/android/contacts/editor/SelectAccountDialogFragment;Lcom/android/contacts/model/account/AccountWithDataSet;)Lcom/android/contacts/model/account/AccountWithDataSet;

    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    # getter for: Lcom/android/contacts/editor/SelectAccountDialogFragment;->account:Lcom/android/contacts/model/account/AccountWithDataSet;
    invoke-static {v1}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$000(Lcom/android/contacts/editor/SelectAccountDialogFragment;)Lcom/android/contacts/model/account/AccountWithDataSet;

    move-result-object v1

    instance-of v1, v1, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->val$accountAdapter:Lcom/android/contacts/util/AccountsListAdapter;

    invoke-virtual {v1, p2}, Lcom/android/contacts/util/AccountsListAdapter;->getItem(I)Lcom/android/contacts/model/account/AccountWithDataSet;

    move-result-object v1

    check-cast v1, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    # setter for: Lcom/android/contacts/editor/SelectAccountDialogFragment;->account:Lcom/android/contacts/model/account/AccountWithDataSet;
    invoke-static {v2, v1}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$002(Lcom/android/contacts/editor/SelectAccountDialogFragment;Lcom/android/contacts/model/account/AccountWithDataSet;)Lcom/android/contacts/model/account/AccountWithDataSet;

    iget-object v2, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    # getter for: Lcom/android/contacts/editor/SelectAccountDialogFragment;->account:Lcom/android/contacts/model/account/AccountWithDataSet;
    invoke-static {v1}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$000(Lcom/android/contacts/editor/SelectAccountDialogFragment;)Lcom/android/contacts/model/account/AccountWithDataSet;

    move-result-object v1

    check-cast v1, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v1}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v1

    # setter for: Lcom/android/contacts/editor/SelectAccountDialogFragment;->mSlotId:I
    invoke-static {v2, v1}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$102(Lcom/android/contacts/editor/SelectAccountDialogFragment;I)I

    :cond_0
    iget-object v1, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    # getter for: Lcom/android/contacts/editor/SelectAccountDialogFragment;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {v1}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$200(Lcom/android/contacts/editor/SelectAccountDialogFragment;)Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v1

    iget-object v2, p0, Lcom/android/contacts/editor/SelectAccountDialogFragment$1;->this$0:Lcom/android/contacts/editor/SelectAccountDialogFragment;

    # getter for: Lcom/android/contacts/editor/SelectAccountDialogFragment;->mSlotId:I
    invoke-static {v2}, Lcom/android/contacts/editor/SelectAccountDialogFragment;->access$100(Lcom/android/contacts/editor/SelectAccountDialogFragment;)I

    move-result v2

    const/16 v3, 0x130

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(II)I

    move-result v0

    return-void
.end method
