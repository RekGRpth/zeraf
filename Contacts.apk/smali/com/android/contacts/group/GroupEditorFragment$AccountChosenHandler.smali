.class Lcom/android/contacts/group/GroupEditorFragment$AccountChosenHandler;
.super Landroid/os/Handler;
.source "GroupEditorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/group/GroupEditorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AccountChosenHandler"
.end annotation


# static fields
.field public static final ACCOUNT_CHOOSE_CANCEL:I = 0x1

.field public static final ACCOUNT_CHOSEN:I


# instance fields
.field private mFragment:Lcom/android/contacts/group/GroupEditorFragment;


# direct methods
.method public constructor <init>(Lcom/android/contacts/group/GroupEditorFragment;)V
    .locals 0
    .param p1    # Lcom/android/contacts/group/GroupEditorFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/android/contacts/group/GroupEditorFragment$AccountChosenHandler;->mFragment:Lcom/android/contacts/group/GroupEditorFragment;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "GroupEditorFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[handleMessage] invalid msg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/contacts/group/GroupEditorFragment$AccountChosenHandler;->mFragment:Lcom/android/contacts/group/GroupEditorFragment;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/contacts/model/account/AccountWithDataSet;

    invoke-static {v1, v0}, Lcom/android/contacts/group/GroupEditorFragment;->access$3300(Lcom/android/contacts/group/GroupEditorFragment;Lcom/android/contacts/model/account/AccountWithDataSet;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/contacts/group/GroupEditorFragment$AccountChosenHandler;->mFragment:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-static {v0}, Lcom/android/contacts/group/GroupEditorFragment;->access$3400(Lcom/android/contacts/group/GroupEditorFragment;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
