.class Lcom/android/contacts/group/GroupDetailFragment$GroupDeleteHandler;
.super Landroid/os/Handler;
.source "GroupDetailFragment.java"

# interfaces
.implements Lcom/android/contacts/ContactSaveService$DeleteEndListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/group/GroupDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GroupDeleteHandler"
.end annotation


# static fields
.field private static final DELETE_END:I = 0x1

.field private static final DELETE_START:I


# instance fields
.field final synthetic this$0:Lcom/android/contacts/group/GroupDetailFragment;


# direct methods
.method private constructor <init>(Lcom/android/contacts/group/GroupDetailFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/group/GroupDetailFragment$GroupDeleteHandler;->this$0:Lcom/android/contacts/group/GroupDetailFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/contacts/group/GroupDetailFragment;Lcom/android/contacts/group/GroupDetailFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/contacts/group/GroupDetailFragment;
    .param p2    # Lcom/android/contacts/group/GroupDetailFragment$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/group/GroupDetailFragment$GroupDeleteHandler;-><init>(Lcom/android/contacts/group/GroupDetailFragment;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "GroupDetailFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[handleMessage] unexpected message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/contacts/group/GroupDetailFragment$GroupDeleteHandler;->this$0:Lcom/android/contacts/group/GroupDetailFragment;

    invoke-virtual {v0}, Lcom/android/contacts/group/GroupDetailFragment;->showDialog()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/contacts/group/GroupDetailFragment$GroupDeleteHandler;->this$0:Lcom/android/contacts/group/GroupDetailFragment;

    invoke-virtual {v0}, Lcom/android/contacts/group/GroupDetailFragment;->dismissDialog()V

    iget-object v0, p0, Lcom/android/contacts/group/GroupDetailFragment$GroupDeleteHandler;->this$0:Lcom/android/contacts/group/GroupDetailFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/group/GroupDetailFragment$GroupDeleteHandler;->this$0:Lcom/android/contacts/group/GroupDetailFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDeleteEnd()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onDeleteStart()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
