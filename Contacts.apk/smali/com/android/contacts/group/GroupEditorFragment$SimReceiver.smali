.class Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GroupEditorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/group/GroupEditorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SimReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/group/GroupEditorFragment;


# direct methods
.method constructor <init>(Lcom/android/contacts/group/GroupEditorFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v2, "GroupEditorFragment"

    const-string v3, "In onReceive "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "GroupEditorFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-virtual {v2, p2}, Lcom/android/contacts/group/GroupEditorFragment;->processAirplaneModeChanged(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "android.intent.action.PHB_STATE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-virtual {v2, p2}, Lcom/android/contacts/group/GroupEditorFragment;->processPhbStateChange(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiPhone;->EVENT_PRE_3G_SWITCH:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-static {v2}, Lcom/android/contacts/group/GroupEditorFragment;->access$1900(Lcom/android/contacts/group/GroupEditorFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/contacts/model/account/AccountType;->isAccountTypeIccCard(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-static {v2}, Lcom/android/contacts/group/GroupEditorFragment;->access$2000(Lcom/android/contacts/group/GroupEditorFragment;)V

    :cond_3
    const-string v2, "GroupEditorFragment"

    const-string v3, "Modem switch ....."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-static {v2}, Lcom/android/contacts/group/GroupEditorFragment;->access$2100(Lcom/android/contacts/group/GroupEditorFragment;)V

    goto :goto_0

    :cond_4
    const-string v2, "android.intent.action.DUAL_SIM_MODE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "mode"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "GroupEditorFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DUAL_SIM_MODE_CHANGED, new mode is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-static {v2}, Lcom/android/contacts/group/GroupEditorFragment;->access$1100(Lcom/android/contacts/group/GroupEditorFragment;)I

    move-result v2

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-static {v2}, Lcom/android/contacts/group/GroupEditorFragment;->access$1100(Lcom/android/contacts/group/GroupEditorFragment;)I

    move-result v2

    invoke-static {v2, v1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isDualSimModeOn(II)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "GroupEditorFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current slot was turned off. slot id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-static {v4}, Lcom/android/contacts/group/GroupEditorFragment;->access$1100(Lcom/android/contacts/group/GroupEditorFragment;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/contacts/group/GroupEditorFragment$SimReceiver;->this$0:Lcom/android/contacts/group/GroupEditorFragment;

    invoke-static {v2}, Lcom/android/contacts/group/GroupEditorFragment;->access$2100(Lcom/android/contacts/group/GroupEditorFragment;)V

    goto/16 :goto_0
.end method
