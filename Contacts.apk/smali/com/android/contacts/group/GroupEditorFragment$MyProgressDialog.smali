.class public Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;
.super Landroid/app/DialogFragment;
.source "GroupEditorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/group/GroupEditorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyProgressDialog"
.end annotation


# instance fields
.field private mIsDismiss:Z

.field private mShouldDismiss:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    iput-boolean v0, p0, Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;->mIsDismiss:Z

    iput-boolean v0, p0, Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;->mShouldDismiss:Z

    return-void
.end method

.method static synthetic access$2202(Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;Z)Z
    .locals 0
    .param p0    # Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;->mIsDismiss:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;)Z
    .locals 1
    .param p0    # Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;

    iget-boolean v0, p0, Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;->mShouldDismiss:Z

    return v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0c005b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/android/contacts/group/GroupEditorFragment;->access$3102(Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;)Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object v0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;->mShouldDismiss:Z

    return-void
.end method

.method public onResume()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iput-boolean v1, p0, Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;->mShouldDismiss:Z

    iget-boolean v0, p0, Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;->mIsDismiss:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/contacts/group/GroupEditorFragment;->access$3100()Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/contacts/group/GroupEditorFragment;->access$3100()Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/contacts/group/GroupEditorFragment;->access$3100()Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/contacts/group/GroupEditorFragment;->access$3100()Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    iput-boolean v1, p0, Lcom/android/contacts/group/GroupEditorFragment$MyProgressDialog;->mIsDismiss:Z

    :cond_0
    return-void
.end method
