.class Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;
.super Ljava/lang/Object;
.source "PhoneFavoriteFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/list/PhoneFavoriteFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AllContactsLoaderListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;


# direct methods
.method private constructor <init>(Lcom/android/contacts/list/PhoneFavoriteFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/contacts/list/PhoneFavoriteFragment;Lcom/android/contacts/list/PhoneFavoriteFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/contacts/list/PhoneFavoriteFragment;
    .param p2    # Lcom/android/contacts/list/PhoneFavoriteFragment$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;-><init>(Lcom/android/contacts/list/PhoneFavoriteFragment;)V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v1}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$400(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/PhoneNumberListAdapter;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/contacts/list/PhoneNumberListAdapter;->configureLoader(Landroid/content/CursorLoader;J)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$400(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/PhoneNumberListAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, p2}, Lcom/android/contacts/list/ContactEntryListAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/contacts/util/PhoneCapabilityTester;->isUsingTwoPanes(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1100(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1200(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1300(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$800(Lcom/android/contacts/list/PhoneFavoriteFragment;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1200(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1300(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1400(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v1}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$400(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/PhoneNumberListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1100(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1500(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$800(Lcom/android/contacts/list/PhoneFavoriteFragment;)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1500(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1600(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$800(Lcom/android/contacts/list/PhoneFavoriteFragment;)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1500(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1600(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/contacts/list/PhoneFavoriteFragment$AllContactsLoaderListener;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
