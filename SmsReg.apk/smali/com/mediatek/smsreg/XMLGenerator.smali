.class Lcom/mediatek/smsreg/XMLGenerator;
.super Ljava/lang/Object;
.source "XMLgenerator.java"

# interfaces
.implements Lcom/mediatek/smsreg/ConfigInfoGenerator;


# static fields
.field private static final TAG:Ljava/lang/String; = "SmsReg/XMLGenerator"

.field private static sXmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;


# instance fields
.field private mDom:Lorg/w3c/dom/Document;

.field private mManufacturerName:Ljava/lang/String;

.field private mNetworkNumber:[Ljava/lang/String;

.field private mOperatorName:Ljava/lang/String;

.field private mSmsInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/smsreg/SmsInfoUnit;",
            ">;"
        }
    .end annotation
.end field

.field private mSmsNumber:Ljava/lang/String;

.field private mSmsPort:Ljava/lang/String;

.field private mSrcPort:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/smsreg/XMLGenerator;->sXmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsInfoList:Ljava/util/List;

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mOperatorName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mManufacturerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsPort:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSrcPort:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mNetworkNumber:[Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/mediatek/smsreg/XMLGenerator;->parse(Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/mediatek/smsreg/XMLGenerator;->getOperatorName()Ljava/lang/String;

    return-void
.end method

.method static getInstance(Ljava/lang/String;)Lcom/mediatek/smsreg/ConfigInfoGenerator;
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-object v1, Lcom/mediatek/smsreg/XMLGenerator;->sXmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    if-nez v1, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "SmsReg/XMLGenerator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "create XMLGenerator failed! config file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not exist!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    sget-object v1, Lcom/mediatek/smsreg/XMLGenerator;->sXmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    return-object v1

    :cond_1
    new-instance v1, Lcom/mediatek/smsreg/XMLGenerator;

    invoke-direct {v1, p0}, Lcom/mediatek/smsreg/XMLGenerator;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/mediatek/smsreg/XMLGenerator;->sXmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    goto :goto_0
.end method

.method private getValArrayFromDom(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 11
    .param p1    # Lorg/w3c/dom/Node;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Node;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v10, 0x3

    if-nez p1, :cond_2

    iget-object v7, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    const-string v8, "operator"

    invoke-interface {v7, v8}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v7, "SmsReg/XMLGenerator"

    const-string v8, "Node <operator> is not exist in the xml file."

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-ge v1, v7, :cond_2

    invoke-interface {v2, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v7

    const-string v8, "xml:id"

    invoke-interface {v7, v8}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/smsreg/XMLGenerator;->mOperatorName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object p1

    :cond_2
    if-nez p1, :cond_4

    const-string v7, "SmsReg/XMLGenerator"

    const-string v8, "Node to read is null"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v7

    if-ne v7, v10, :cond_5

    const-string v7, "SmsReg/XMLGenerator"

    const-string v8, "Node to read is a leaf."

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    const-string v7, "SmsReg/XMLGenerator"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Node to read: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    :cond_6
    :goto_2
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v5

    :cond_7
    :goto_3
    if-eqz v5, :cond_b

    const-string v7, "SmsReg/XMLGenerator"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sibling node: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v7

    if-eq v7, v10, :cond_8

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v7

    if-nez v7, :cond_9

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v7, "SmsReg/XMLGenerator"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "found NodeValue: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    :goto_4
    :try_start_0
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v5

    if-eqz v5, :cond_7

    const-string v7, "SmsReg/XMLGenerator"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "siblingNode NodeName: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    const/4 v5, 0x0

    goto :goto_3

    :cond_9
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    invoke-virtual {p3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    const-string v7, "SmsReg/XMLGenerator"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "found NodeValue: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_b
    :try_start_1
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object p1

    if-eqz p1, :cond_6

    const-string v7, "SmsReg/XMLGenerator"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "node NodeName:-- "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v0

    const/4 p1, 0x0

    goto/16 :goto_2

    :cond_c
    invoke-interface {p1}, Lorg/w3c/dom/Node;->hasChildNodes()Z

    move-result v7

    if-eqz v7, :cond_d

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-direct {p0, v7, p2, p3}, Lcom/mediatek/smsreg/XMLGenerator;->getValArrayFromDom(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_d
    const/4 v4, 0x0

    :try_start_2
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v4

    :goto_5
    if-eqz v4, :cond_0

    invoke-direct {p0, v4, p2, p3}, Lcom/mediatek/smsreg/XMLGenerator;->getValArrayFromDom(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const/4 v4, 0x0

    goto :goto_5
.end method

.method private getValByTagName(Lorg/w3c/dom/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Lorg/w3c/dom/Document;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-interface {p1, p2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method public getCustomizedStatus()Z
    .locals 2

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    const-string v1, "customized"

    invoke-direct {p0, v0, v1}, Lcom/mediatek/smsreg/XMLGenerator;->getValByTagName(Lorg/w3c/dom/Document;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getManufacturerName()Ljava/lang/String;
    .locals 4

    const-string v0, "dm"

    const-string v1, "Manufacturer"

    iget-object v2, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    const-string v3, "manufacturer"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/smsreg/XMLGenerator;->getValByTagName(Lorg/w3c/dom/Document;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/mediatek/custom/CustomProperties;->getString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mManufacturerName:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mManufacturerName:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkNumber()[Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/smsreg/XMLGenerator;->mNetworkNumber:[Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const-string v2, "networkNumber"

    invoke-direct {p0, v1, v2, v0}, Lcom/mediatek/smsreg/XMLGenerator;->getValArrayFromDom(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/smsreg/XMLGenerator;->mNetworkNumber:[Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/smsreg/XMLGenerator;->mNetworkNumber:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/smsreg/XMLGenerator;->mNetworkNumber:[Ljava/lang/String;

    return-object v1
.end method

.method public getOemName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mOperatorName:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    const-string v1, "oemname"

    invoke-direct {p0, v0, v1}, Lcom/mediatek/smsreg/XMLGenerator;->getValByTagName(Lorg/w3c/dom/Document;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mOperatorName:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mOperatorName:Ljava/lang/String;

    return-object v0
.end method

.method public getOperatorName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mOperatorName:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    const-string v1, "operatorcustomized"

    invoke-direct {p0, v0, v1}, Lcom/mediatek/smsreg/XMLGenerator;->getValByTagName(Lorg/w3c/dom/Document;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mOperatorName:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mOperatorName:Ljava/lang/String;

    return-object v0
.end method

.method public getSmsInfoList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/smsreg/SmsInfoUnit;",
            ">;"
        }
    .end annotation

    iget-object v4, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsInfoList:Ljava/util/List;

    if-nez v4, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    const-string v5, "segment"

    invoke-direct {p0, v4, v5, v3}, Lcom/mediatek/smsreg/XMLGenerator;->getValArrayFromDom(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v1, v4, [Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsInfoList:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v4, v0, 0x3

    add-int/lit8 v4, v4, 0x1

    array-length v5, v1

    if-ge v4, v5, :cond_3

    mul-int/lit8 v4, v0, 0x3

    add-int/lit8 v4, v4, 0x1

    aget-object v4, v1, v4

    if-eqz v4, :cond_3

    new-instance v2, Lcom/mediatek/smsreg/SmsInfoUnit;

    invoke-direct {v2}, Lcom/mediatek/smsreg/SmsInfoUnit;-><init>()V

    mul-int/lit8 v4, v0, 0x3

    array-length v5, v1

    if-ge v4, v5, :cond_0

    mul-int/lit8 v4, v0, 0x3

    aget-object v4, v1, v4

    invoke-virtual {v2, v4}, Lcom/mediatek/smsreg/SmsInfoUnit;->setPrefix(Ljava/lang/String;)V

    :cond_0
    mul-int/lit8 v4, v0, 0x3

    add-int/lit8 v4, v4, 0x1

    array-length v5, v1

    if-ge v4, v5, :cond_1

    mul-int/lit8 v4, v0, 0x3

    add-int/lit8 v4, v4, 0x1

    aget-object v4, v1, v4

    invoke-virtual {v2, v4}, Lcom/mediatek/smsreg/SmsInfoUnit;->setContent(Ljava/lang/String;)V

    :cond_1
    mul-int/lit8 v4, v0, 0x3

    add-int/lit8 v4, v4, 0x2

    array-length v5, v1

    if-ge v4, v5, :cond_2

    mul-int/lit8 v4, v0, 0x3

    add-int/lit8 v4, v4, 0x2

    aget-object v4, v1, v4

    invoke-virtual {v2, v4}, Lcom/mediatek/smsreg/SmsInfoUnit;->setPostfix(Ljava/lang/String;)V

    :cond_2
    iget-object v4, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsInfoList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsInfoList:Ljava/util/List;

    return-object v4
.end method

.method public getSmsNumber()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsNumber:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    const-string v1, "smsnumber"

    invoke-direct {p0, v0, v1}, Lcom/mediatek/smsreg/XMLGenerator;->getValByTagName(Lorg/w3c/dom/Document;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsNumber:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSmsPort()Ljava/lang/Short;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsPort:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    const-string v1, "smsport"

    invoke-direct {p0, v0, v1}, Lcom/mediatek/smsreg/XMLGenerator;->getValByTagName(Lorg/w3c/dom/Document;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsPort:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSmsPort:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getSrcPort()Ljava/lang/Short;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSrcPort:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;

    const-string v1, "srcport"

    invoke-direct {p0, v0, v1}, Lcom/mediatek/smsreg/XMLGenerator;->getValByTagName(Lorg/w3c/dom/Document;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSrcPort:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/smsreg/XMLGenerator;->mSrcPort:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method parse(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v3

    :try_start_0
    invoke-virtual {v3}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SmsReg/XMLGenerator"

    const-string v6, "config file is not exist!"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/smsreg/XMLGenerator;->mDom:Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :catch_0
    move-exception v2

    const-string v5, "SmsReg/XMLGenerator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Create document builder failed!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v5, "SmsReg/XMLGenerator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Create mDom failed! SAXException:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lorg/xml/sax/SAXException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v2

    const-string v5, "SmsReg/XMLGenerator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Create mDom failed! IOException:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
