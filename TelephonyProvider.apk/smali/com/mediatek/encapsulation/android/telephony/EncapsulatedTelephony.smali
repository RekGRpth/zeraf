.class public Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;
.super Ljava/lang/Object;
.source "EncapsulatedTelephony.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$GprsInfo;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$Carriers;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$WapPush;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$ThreadSettings;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$Threads;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$ThreadsColumns;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$Sms;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$TextBasedSmsColumns;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SimInfo;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$MmsSms;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$Mms;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$BaseMmsColumns;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SmsCb;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$TextBasedSmsCbColumns;
    }
.end annotation


# static fields
.field public static final SIMBackgroundRes:[I

.field private static final TAG:Ljava/lang/String; = "EncapsulatedTelephony"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;->SIMBackgroundRes:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x20200e1
        0x20200e4
        0x20200e2
        0x20200e5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
