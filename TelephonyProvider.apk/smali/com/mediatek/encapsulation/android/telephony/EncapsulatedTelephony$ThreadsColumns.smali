.class public interface abstract Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$ThreadsColumns;
.super Ljava/lang/Object;
.source "EncapsulatedTelephony.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ThreadsColumns"
.end annotation


# static fields
.field public static final LATEST_IMPORTANT_DATE:Ljava/lang/String; = "li_date"

.field public static final LATEST_IMPORTANT_SNIPPET:Ljava/lang/String; = "li_snippet"

.field public static final LATEST_IMPORTANT_SNIPPET_CHARSET:Ljava/lang/String; = "li_snippet_cs"

.field public static final READCOUNT:Ljava/lang/String; = "readcount"
