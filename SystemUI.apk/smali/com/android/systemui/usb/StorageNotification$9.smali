.class Lcom/android/systemui/usb/StorageNotification$9;
.super Ljava/lang/Object;
.source "StorageNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/usb/StorageNotification;->setUsbStorageNotification(IIIZZLandroid/app/PendingIntent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/usb/StorageNotification;

.field final synthetic val$visible:Z


# direct methods
.method constructor <init>(Lcom/android/systemui/usb/StorageNotification;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/usb/StorageNotification$9;->this$0:Lcom/android/systemui/usb/StorageNotification;

    iput-boolean p2, p0, Lcom/android/systemui/usb/StorageNotification$9;->val$visible:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/usb/StorageNotification$9;->this$0:Lcom/android/systemui/usb/StorageNotification;

    iget-boolean v0, p0, Lcom/android/systemui/usb/StorageNotification$9;->val$visible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v0, p0, Lcom/android/systemui/usb/StorageNotification$9;->this$0:Lcom/android/systemui/usb/StorageNotification;

    invoke-static {v0}, Lcom/android/systemui/usb/StorageNotification;->access$100(Lcom/android/systemui/usb/StorageNotification;)Landroid/os/storage/StorageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->enableUsbMassStorage()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/usb/StorageNotification$9;->this$0:Lcom/android/systemui/usb/StorageNotification;

    invoke-static {v0}, Lcom/android/systemui/usb/StorageNotification;->access$100(Lcom/android/systemui/usb/StorageNotification;)Landroid/os/storage/StorageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->disableUsbMassStorage()V

    goto :goto_0
.end method
