.class public Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;
.super Landroid/widget/LinearLayout;
.source "ToolBarIndicator.java"

# interfaces
.implements Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;


# static fields
.field private static final VIEW_PADDING_HORIZONTAL:I = 0x4


# instance fields
.field private mCount:I

.field private mDensity:F

.field private mFocusedDrawable:Landroid/graphics/drawable/Drawable;

.field private mNormalDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->init()V

    return-void
.end method

.method private init()V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mDensity:F

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020223

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020224

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mFocusedDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public generateIndicators()V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mCount:I

    if-ge v0, v3, :cond_0

    new-instance v1, Landroid/widget/ImageView;

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mDensity:F

    const/high16 v4, 0x40800000

    mul-float/2addr v3, v4

    float-to-int v2, v3

    invoke-virtual {v1, v2, v5, v2, v5}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onScrollFinish(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->updateIndicator(I)V

    return-void
.end method

.method public setCount(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mCount:I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->generateIndicators()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->updateIndicator(I)V

    return-void
.end method

.method public updateIndicator(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mCount:I

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mFocusedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->mNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_1
    return-void
.end method
