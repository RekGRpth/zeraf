.class public Lcom/mediatek/activity/ServiceActivityHelper;
.super Ljava/lang/Object;
.source "ServiceActivityHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ServiceInterface:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final DIALOG_BUSY:I = -0xd776


# instance fields
.field protected busyDialog:Landroid/app/ProgressDialog;

.field protected isServiceAvailable:Ljava/lang/Boolean;

.field public service:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServiceInterface;"
        }
    .end annotation
.end field

.field protected serviceActivity:Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity",
            "<TServiceInterface;>;"
        }
    .end annotation
.end field

.field private serviceConn:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity",
            "<TServiceInterface;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->isServiceAvailable:Ljava/lang/Boolean;

    new-instance v0, Lcom/mediatek/activity/ServiceActivityHelper$1;

    invoke-direct {v0, p0}, Lcom/mediatek/activity/ServiceActivityHelper$1;-><init>(Lcom/mediatek/activity/ServiceActivityHelper;)V

    iput-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->serviceConn:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mediatek/activity/ServiceActivityHelper;->serviceActivity:Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;

    return-void
.end method


# virtual methods
.method public acquireServiceLock()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/activity/ServiceActivityHelper;->isServiceAvailable:Ljava/lang/Boolean;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->isServiceAvailable:Ljava/lang/Boolean;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bindService(Landroid/content/Context;)Z
    .locals 4
    .param p1    # Landroid/content/Context;

    const-string v1, "bindService()[+]"

    invoke-static {v1}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/activity/ServiceActivityHelper;->isServiceAvailable:Ljava/lang/Boolean;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/activity/ServiceActivityHelper;->serviceActivity:Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;

    invoke-interface {v2}, Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;->getServiceAction()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/activity/ServiceActivityHelper;->serviceConn:Landroid/content/ServiceConnection;

    const/4 v3, 0x5

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bind service failed: action["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/activity/ServiceActivityHelper;->serviceActivity:Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;

    invoke-interface {v2}, Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;->getServiceAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/util/BtLog;->e(Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method public createBusyDialog(ILandroid/content/Context;)Landroid/app/Dialog;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/content/Context;

    const-string v0, "createBusyDialog()[+]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    const v0, -0xd776

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->busyDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->busyDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f050079

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->busyDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->busyDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->busyDialog:Landroid/app/ProgressDialog;

    goto :goto_0
.end method

.method public refreshUi(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    iget-object v1, p0, Lcom/mediatek/activity/ServiceActivityHelper;->isServiceAvailable:Ljava/lang/Boolean;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->isServiceAvailable:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, -0xd776

    invoke-virtual {p1, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->serviceActivity:Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;

    invoke-interface {v0}, Lcom/mediatek/activity/ServiceActivityHelper$ServiceActivity;->refreshActivityUi()V

    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->busyDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->busyDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, -0xd776

    invoke-virtual {p1, v0}, Landroid/app/Activity;->dismissDialog(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public releaseServiceLock()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/activity/ServiceActivityHelper;->isServiceAvailable:Ljava/lang/Boolean;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->isServiceAvailable:Ljava/lang/Boolean;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unbindService(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    const-string v0, "unbindService()[+]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/activity/ServiceActivityHelper;->serviceConn:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v1, p0, Lcom/mediatek/activity/ServiceActivityHelper;->service:Ljava/lang/Object;

    iput-object v1, p0, Lcom/mediatek/activity/ServiceActivityHelper;->busyDialog:Landroid/app/ProgressDialog;

    return-void
.end method
