.class public Lcom/mediatek/bluetooth/ilm/MessageService;
.super Landroid/app/Service;
.source "MessageService.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final MSG_ID_SYS_SHUTDOWN_SERVICE_REQ:I = -0x1

.field private static SHUTDOWN_SERVICE_REQUEST:Lcom/mediatek/bluetooth/ilm/Message;


# instance fields
.field protected clientSocketFd:I

.field protected isListening:Z

.field protected listenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/bluetooth/ilm/MessageListener;",
            ">;"
        }
    .end annotation
.end field

.field protected serverSocketFd:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/mediatek/bluetooth/ilm/Message;

    sget-object v1, Lcom/mediatek/bluetooth/ilm/Message;->ILM:[I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/ilm/Message;-><init>([I)V

    sput-object v0, Lcom/mediatek/bluetooth/ilm/MessageService;->SHUTDOWN_SERVICE_REQUEST:Lcom/mediatek/bluetooth/ilm/Message;

    sget-object v0, Lcom/mediatek/bluetooth/ilm/MessageService;->SHUTDOWN_SERVICE_REQUEST:Lcom/mediatek/bluetooth/ilm/Message;

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/bluetooth/ilm/Message;->setInt(II)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->clientSocketFd:I

    iput v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->serverSocketFd:I

    return-void
.end method


# virtual methods
.method protected closeSocket(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MessageService.closeSocket()[+]:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    if-ltz p1, :cond_0

    invoke-static {p1}, Lcom/mediatek/bluetooth/ilm/ilm_native;->close_socket(I)V

    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const-string v0, "MessageService.onBind(): unimplemented function."

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, -0x1

    const/4 v3, 0x0

    const-string v0, "MessageService.onCreate()[+]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->i(Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->isListening:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->listenerList:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iget v1, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->serverSocketFd:I

    const-string v2, "bt.ext.adp.prx"

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/mediatek/bluetooth/ilm/MessageService;->openSocket(ZILjava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->serverSocketFd:I

    iget v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->clientSocketFd:I

    const-string v1, "/dev/socket/bt.int.adp"

    invoke-virtual {p0, v3, v0, v1, v5}, Lcom/mediatek/bluetooth/ilm/MessageService;->openSocket(ZILjava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->clientSocketFd:I

    iget v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->serverSocketFd:I

    if-le v0, v4, :cond_0

    iget v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->clientSocketFd:I

    if-le v0, v4, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/ilm/MessageService;->startListen()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "MessageService.onCreate() error: can\'t create server & client sockets."

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    const-string v0, "MessageService.onDestroy()[+]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->i(Ljava/lang/String;)V

    iget v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->clientSocketFd:I

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/ilm/MessageService;->closeSocket(I)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/ilm/MessageService;->stopListen()V

    return-void
.end method

.method protected openSocket(ZILjava/lang/String;I)I
    .locals 2
    .param p1    # Z
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # I

    if-ltz p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/mediatek/bluetooth/ilm/MessageService;->closeSocket(I)V

    :cond_0
    if-eqz p1, :cond_2

    invoke-static {p3, p4}, Lcom/mediatek/bluetooth/ilm/ilm_native;->create_server_socket(Ljava/lang/String;I)I

    move-result p2

    :goto_0
    if-gez p2, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MessageService.openSocket() error: isServer["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], fd["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], name["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], namespace["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->e(Ljava/lang/String;)V

    :cond_1
    return p2

    :cond_2
    invoke-static {p3, p4}, Lcom/mediatek/bluetooth/ilm/ilm_native;->create_client_socket(Ljava/lang/String;I)I

    move-result p2

    goto :goto_0
.end method

.method public declared-synchronized registerMessageListener(Lcom/mediatek/bluetooth/ilm/MessageListener;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/ilm/MessageListener;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->listenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 11

    const/4 v10, 0x1

    const/4 v5, -0x1

    iput-boolean v10, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->isListening:Z

    :cond_0
    :goto_0
    iget-boolean v8, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->isListening:Z

    if-eqz v8, :cond_1

    const-string v8, "MessageService.run() - listening message..."

    invoke-static {v8}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    :try_start_0
    new-instance v2, Lcom/mediatek/bluetooth/ilm/Message;

    sget-object v8, Lcom/mediatek/bluetooth/ilm/Message;->ILM:[I

    invoke-direct {v2, v8}, Lcom/mediatek/bluetooth/ilm/Message;-><init>([I)V

    iget v8, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->serverSocketFd:I

    invoke-static {v8, v2}, Lcom/mediatek/bluetooth/ilm/ilm_native;->recv_message(ILcom/mediatek/bluetooth/ilm/Message;)I

    move-result v5

    if-ne v5, v10, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MessageService.run(): recv message["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/ilm/Message;->toPrintString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/ilm/Message;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    const/4 v8, -0x1

    if-ne v4, v8, :cond_2

    :cond_1
    iget v8, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->serverSocketFd:I

    invoke-virtual {p0, v8}, Lcom/mediatek/bluetooth/ilm/MessageService;->closeSocket(I)V

    const/4 v8, 0x0

    iput-object v8, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->listenerList:Ljava/util/ArrayList;

    return-void

    :cond_2
    const/16 v8, 0x1c

    const/4 v9, 0x4

    :try_start_1
    invoke-virtual {v2, v8, v9}, Lcom/mediatek/bluetooth/ilm/Message;->getBuffer(II)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v6

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Ljava/nio/ShortBuffer;->get(I)S

    move-result v7

    iget-object v8, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->listenerList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/bluetooth/ilm/MessageListener;

    const/16 v8, 0x1c

    invoke-virtual {v2, v8, v7}, Lcom/mediatek/bluetooth/ilm/Message;->getBuffer(II)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-interface {v3, v4, v8}, Lcom/mediatek/bluetooth/ilm/MessageListener;->onMessageReceived(ILjava/nio/ByteBuffer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v8, "MessageService.run() error: "

    invoke-static {v8, v0}, Lcom/mediatek/bluetooth/util/BtLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    :try_start_2
    const-string v8, "MessageService.run(): revc_message() failed"

    invoke-static {v8}, Lcom/mediatek/bluetooth/util/BtLog;->w(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method

.method public send(Lcom/mediatek/bluetooth/ilm/Message;)V
    .locals 6
    .param p1    # Lcom/mediatek/bluetooth/ilm/Message;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "send()[+]: msgId["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/ilm/Message;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/bluetooth/ilm/Message;

    sget-object v4, Lcom/mediatek/bluetooth/ilm/Message;->ILM:[I

    invoke-direct {v2, v4}, Lcom/mediatek/bluetooth/ilm/Message;-><init>([I)V

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/ilm/Message;->getId()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/mediatek/bluetooth/ilm/Message;->setInt(II)V

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v5}, Lcom/mediatek/bluetooth/ilm/Message;->setInt(II)V

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/mediatek/bluetooth/ilm/Message;->setInt(II)V

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/ilm/Message;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/ilm/Message;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    const/16 v4, 0x1c

    invoke-virtual {v0, v4}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->flip()Ljava/nio/Buffer;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/ilm/Message;->size()I

    move-result v4

    add-int/lit16 v4, v4, -0x800

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/ilm/Message;->size()I

    move-result v5

    add-int v3, v4, v5

    iget v4, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->clientSocketFd:I

    invoke-static {v4, v2, v3}, Lcom/mediatek/bluetooth/ilm/ilm_native;->send_message(ILcom/mediatek/bluetooth/ilm/Message;I)I

    return-void
.end method

.method protected startListen()V
    .locals 2

    const-string v0, "MessageService.startListen()[+]"

    invoke-static {v0}, Lcom/mediatek/bluetooth/util/BtLog;->d(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "MessageServiceThread"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected declared-synchronized stopListen()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->isListening:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->isListening:Z

    const-string v1, "bt.ext.adp.prx"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/ilm/ilm_native;->create_client_socket(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, Lcom/mediatek/bluetooth/ilm/MessageService;->SHUTDOWN_SERVICE_REQUEST:Lcom/mediatek/bluetooth/ilm/Message;

    sget-object v2, Lcom/mediatek/bluetooth/ilm/MessageService;->SHUTDOWN_SERVICE_REQUEST:Lcom/mediatek/bluetooth/ilm/Message;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/ilm/Message;->size()I

    move-result v2

    add-int/lit16 v2, v2, -0x800

    invoke-static {v0, v1, v2}, Lcom/mediatek/bluetooth/ilm/ilm_native;->send_message(ILcom/mediatek/bluetooth/ilm/Message;I)I

    invoke-static {v0}, Lcom/mediatek/bluetooth/ilm/ilm_native;->close_socket(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized unregisterMessageListener(Lcom/mediatek/bluetooth/ilm/MessageListener;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/ilm/MessageListener;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/ilm/MessageService;->listenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
