.class public Lcom/mediatek/bluetooth/util/ConvertUtils;
.super Ljava/lang/Object;
.source "ConvertUtils.java"


# static fields
.field private static final HEX:Ljava/lang/String; = "0123456789ABCDEF"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertBdAddr([B)Ljava/lang/String;
    .locals 9
    .param p0    # [B

    const/16 v8, 0x9

    if-eqz p0, :cond_0

    array-length v6, p0

    const/4 v7, 0x6

    if-eq v6, v7, :cond_1

    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "convertBdAddr() error: invalid bdaddr["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1
    const/16 v6, 0x11

    new-array v3, v6, [C

    const/4 v1, 0x0

    const/4 v0, 0x5

    move v2, v1

    :goto_0
    if-ltz v0, :cond_4

    aget-byte v5, p0, v0

    ushr-int/lit8 v6, v5, 0x4

    and-int/lit8 v4, v6, 0xf

    if-le v4, v8, :cond_2

    add-int/lit8 v6, v4, -0xa

    add-int/lit8 v4, v6, 0x41

    :goto_1
    add-int/lit8 v1, v2, 0x1

    int-to-char v6, v4

    aput-char v6, v3, v2

    and-int/lit8 v4, v5, 0xf

    if-le v4, v8, :cond_3

    add-int/lit8 v6, v4, -0xa

    add-int/lit8 v4, v6, 0x41

    :goto_2
    add-int/lit8 v2, v1, 0x1

    int-to-char v6, v4

    aput-char v6, v3, v1

    if-lez v0, :cond_5

    add-int/lit8 v1, v2, 0x1

    const/16 v6, 0x3a

    aput-char v6, v3, v2

    :goto_3
    add-int/lit8 v0, v0, -0x1

    move v2, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v4, v4, 0x30

    goto :goto_1

    :cond_3
    add-int/lit8 v4, v4, 0x30

    goto :goto_2

    :cond_4
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([C)V

    return-object v6

    :cond_5
    move v1, v2

    goto :goto_3
.end method

.method public static convertBdAddr(Ljava/lang/String;)[B
    .locals 11
    .param p0    # Ljava/lang/String;

    const/16 v10, 0x46

    const/16 v9, 0x41

    const/16 v8, 0x39

    const/16 v7, 0x30

    const/16 v6, 0x11

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v4, v6, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "convertBdAddr() error: invalid bdaddr["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    const/4 v1, 0x0

    const/4 v4, 0x6

    new-array v3, v4, [B

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v6, :cond_6

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-gt v7, v0, :cond_2

    if-gt v0, v8, :cond_2

    add-int/lit8 v4, v0, -0x30

    int-to-short v1, v4

    :goto_1
    shl-int/lit8 v4, v1, 0x4

    int-to-short v1, v4

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-gt v7, v0, :cond_4

    if-gt v0, v8, :cond_4

    add-int/lit8 v4, v0, -0x30

    int-to-short v4, v4

    add-int/2addr v4, v1

    int-to-short v1, v4

    :goto_2
    div-int/lit8 v4, v2, 0x3

    rsub-int/lit8 v4, v4, 0x5

    int-to-byte v5, v1

    aput-byte v5, v3, v4

    add-int/lit8 v2, v2, 0x3

    goto :goto_0

    :cond_2
    if-gt v9, v0, :cond_3

    if-gt v0, v10, :cond_3

    add-int/lit8 v4, v0, -0x41

    add-int/lit8 v4, v4, 0xa

    int-to-short v1, v4

    goto :goto_1

    :cond_3
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "convertBdAddr() error: invalid char["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] in ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    if-gt v9, v0, :cond_5

    if-gt v0, v10, :cond_5

    add-int/lit8 v4, v0, -0x41

    add-int/lit8 v4, v4, 0xa

    int-to-short v4, v4

    add-int/2addr v4, v1

    int-to-short v1, v4

    goto :goto_2

    :cond_5
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "convertBdAddr() error: invalid char["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] in ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    return-object v3
.end method

.method public static toHexString(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/nio/ByteBuffer;

    if-nez p0, :cond_0

    const-string v4, ""

    :goto_0
    return-object v4

    :cond_0
    invoke-virtual {p0}, Ljava/nio/Buffer;->capacity()I

    move-result v3

    new-instance v2, Ljava/lang/StringBuilder;

    mul-int/lit8 v4, v3, 0x2

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const-string v4, "0123456789ABCDEF"

    and-int/lit16 v5, v0, 0xf0

    shr-int/lit8 v5, v5, 0x4

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "0123456789ABCDEF"

    and-int/lit8 v6, v0, 0xf

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
