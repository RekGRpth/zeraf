.class Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;
.super Ljava/lang/Thread;
.source "BluetoothHidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/hid/BluetoothHidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionTimeoutThread"
.end annotation


# instance fields
.field public mBTAddr:Ljava/lang/String;

.field public mState:Ljava/lang/String;

.field private mStoped:Z

.field final synthetic this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;


# direct methods
.method private constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->mStoped:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p2    # Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    return-void
.end method

.method private actionTimeout(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x5

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v4, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ERROR: stateMap not contain "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x64

    :cond_2
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-boolean v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->mStoped:Z

    if-nez v4, :cond_3

    const v4, 0xea60

    if-lt v0, v4, :cond_1

    const/4 v3, 0x1

    :cond_3
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v5, "Waiting action time-out. Force return."

    invoke-static {v4, v5, v6}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;I)V

    const-string v4, "connected"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v4, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$400(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-static {v4, v6, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;ILjava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v2

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const-string v5, "Waiting for action was interrupted."

    invoke-static {v4, v5, v7}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;I)V

    goto :goto_1

    :cond_4
    const-string v4, "disconnect"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->this$0:Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    const/4 v5, 0x7

    invoke-static {v4, v5, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->access$500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->mBTAddr:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->mState:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->actionTimeout(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public shutdown()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$ActionTimeoutThread;->mStoped:Z

    return-void
.end method
