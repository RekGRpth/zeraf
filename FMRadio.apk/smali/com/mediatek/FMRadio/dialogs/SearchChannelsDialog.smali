.class public Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;
.super Landroid/app/DialogFragment;
.source "SearchChannelsDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;)Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;

    iget-object v0, p0, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;

    return-object v0
.end method

.method public static newInstance()Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;
    .locals 1

    new-instance v0, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;

    invoke-direct {v0}, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    :try_start_0
    check-cast p1, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;

    iput-object p1, p0, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;

    invoke-interface {v0}, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$CancelSearchListener;->cancelSearch()V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f040011

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const v2, 0x7f040010

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(I)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    const v2, 0x7f04000f

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$1;

    invoke-direct {v3, p0}, Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog$1;-><init>(Lcom/mediatek/FMRadio/dialogs/SearchChannelsDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object v1
.end method
