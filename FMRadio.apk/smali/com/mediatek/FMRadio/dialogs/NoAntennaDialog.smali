.class public Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;
.super Landroid/app/DialogFragment;
.source "NoAntennaDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$NoAntennaListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$NoAntennaListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$NoAntennaListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;)Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$NoAntennaListener;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;

    iget-object v0, p0, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$NoAntennaListener;

    return-object v0
.end method

.method public static newInstance()Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;
    .locals 1

    new-instance v0, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;

    invoke-direct {v0}, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    :try_start_0
    check-cast p1, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$NoAntennaListener;

    iput-object p1, p0, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$NoAntennaListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f04000a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f04000b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f04000c

    new-instance v3, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$2;

    invoke-direct {v3, p0}, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$2;-><init>(Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f04000d

    new-instance v3, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$1;

    invoke-direct {v3, p0}, Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog$1;-><init>(Lcom/mediatek/FMRadio/dialogs/NoAntennaDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
