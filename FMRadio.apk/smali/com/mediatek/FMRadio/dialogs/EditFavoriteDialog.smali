.class public Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;
.super Landroid/app/DialogFragment;
.source "EditFavoriteDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$EditFavoriteListener;
    }
.end annotation


# static fields
.field private static final STATION_FREQ:Ljava/lang/String; = "station_freq"

.field private static final STATION_NAME:Ljava/lang/String; = "station_name"


# instance fields
.field private mEditTextFrequency:Landroid/widget/EditText;

.field mFilter:Landroid/text/InputFilter;

.field private mListener:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$EditFavoriteListener;

.field private mWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    iput-object v0, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$EditFavoriteListener;

    iput-object v0, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mEditTextFrequency:Landroid/widget/EditText;

    new-instance v0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$2;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$2;-><init>(Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mFilter:Landroid/text/InputFilter;

    new-instance v0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$3;

    invoke-direct {v0, p0}, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$3;-><init>(Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;)V

    iput-object v0, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;)Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$EditFavoriteListener;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;

    iget-object v0, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$EditFavoriteListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;

    iget-object v0, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mEditTextFrequency:Landroid/widget/EditText;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;I)Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # I

    new-instance v1, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;

    invoke-direct {v1}, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "station_name"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "station_freq"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    :try_start_0
    check-cast p1, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$EditFavoriteListener;

    iput-object p1, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mListener:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$EditFavoriteListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v13, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "station_name"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "station_freq"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f030001

    invoke-static {v8, v9, v13}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f060007

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v8, 0x7f060009

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    iput-object v8, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mEditTextFrequency:Landroid/widget/EditText;

    const/4 v2, 0x6

    const/4 v1, 0x5

    const/4 v3, 0x5

    iget-object v8, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mEditTextFrequency:Landroid/widget/EditText;

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/text/InputFilter;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mFilter:Landroid/text/InputFilter;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    new-instance v11, Landroid/text/InputFilter$LengthFilter;

    const/4 v12, 0x5

    invoke-direct {v11, v12}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    iget-object v8, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->mEditTextFrequency:Landroid/widget/EditText;

    invoke-static {v4}, Lcom/mediatek/FMRadio/FMRadioUtils;->formatStation(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v5, :cond_0

    const-string v8, ""

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    const v8, 0x7f040015

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setHint(I)V

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    invoke-virtual {v0}, Landroid/view/View;->requestFocusFromTouch()Z

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v8

    invoke-static {v6, v8}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f040014

    invoke-virtual {p0, v9}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f04000e

    new-instance v10, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$1;

    invoke-direct {v10, p0}, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$1;-><init>(Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f04000f

    invoke-virtual {v8, v9, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8

    :cond_1
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
