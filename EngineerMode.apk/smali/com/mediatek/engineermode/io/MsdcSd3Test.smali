.class public Lcom/mediatek/engineermode/io/MsdcSd3Test;
.super Lcom/mediatek/engineermode/io/MsdcTest;
.source "MsdcSd3Test.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "EM/MSDC_SD30_TEST"


# instance fields
.field private mBtnGet:Landroid/widget/Button;

.field private mBtnSet:Landroid/widget/Button;

.field private mIndexDrive:I

.field private mIndexHost:I

.field private mIndexMaxCurrent:I

.field private mIndexMode:I

.field private mIndexPowerControl:I

.field private mSpinnerDrive:Landroid/widget/Spinner;

.field private mSpinnerHost:Landroid/widget/Spinner;

.field private final mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mSpinnerMaxCurrent:Landroid/widget/Spinner;

.field private mSpinnerMode:Landroid/widget/Spinner;

.field private mSpinnerPowerControl:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mediatek/engineermode/io/MsdcTest;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerHost:Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMode:Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMaxCurrent:Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerDrive:Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerPowerControl:Landroid/widget/Spinner;

    iput v1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexHost:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexMode:I

    iput v1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexMaxCurrent:I

    iput v2, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexDrive:I

    iput v2, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexPowerControl:I

    new-instance v0, Lcom/mediatek/engineermode/io/MsdcSd3Test$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/io/MsdcSd3Test$1;-><init>(Lcom/mediatek/engineermode/io/MsdcSd3Test;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/io/MsdcSd3Test;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/io/MsdcSd3Test;

    iget v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexHost:I

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/io/MsdcSd3Test;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcSd3Test;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexHost:I

    return p1
.end method

.method static synthetic access$008(Lcom/mediatek/engineermode/io/MsdcSd3Test;)I
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/io/MsdcSd3Test;

    iget v0, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexHost:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexHost:I

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/io/MsdcSd3Test;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcSd3Test;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexMode:I

    return p1
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/io/MsdcSd3Test;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcSd3Test;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexMaxCurrent:I

    return p1
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/io/MsdcSd3Test;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcSd3Test;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexDrive:I

    return p1
.end method

.method static synthetic access$402(Lcom/mediatek/engineermode/io/MsdcSd3Test;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcSd3Test;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexPowerControl:I

    return p1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_1

    const/16 v1, 0x6e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexHost:I

    iget v2, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexMode:I

    iget v3, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexMaxCurrent:I

    iget v4, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexDrive:I

    iget v5, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexPowerControl:I

    invoke-static {v1, v2, v3, v4, v5}, Lcom/mediatek/engineermode/io/EmGpio;->setSd30Mode(IIIII)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v1, 0x64

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x65

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const v11, 0x1090009

    const v10, 0x1090008

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v8, 0x7f030050

    invoke-virtual {p0, v8}, Landroid/app/Activity;->setContentView(I)V

    const v8, 0x7f0b0297

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mBtnGet:Landroid/widget/Button;

    const v8, 0x7f0b0298

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mBtnSet:Landroid/widget/Button;

    const v8, 0x7f0b0292

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerHost:Landroid/widget/Spinner;

    const v8, 0x7f0b0293

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMode:Landroid/widget/Spinner;

    const v8, 0x7f0b0294

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMaxCurrent:Landroid/widget/Spinner;

    const v8, 0x7f0b0295

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerDrive:Landroid/widget/Spinner;

    const v8, 0x7f0b0296

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerPowerControl:Landroid/widget/Spinner;

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mBtnGet:Landroid/widget/Button;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f06003b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    aget-object v8, v2, v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x1

    aget-object v8, v2, v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v8, 0x20

    invoke-static {v8}, Lcom/mediatek/engineermode/ChipSupport;->isCurrentChipEquals(I)Z

    move-result v8

    if-nez v8, :cond_0

    const/4 v8, 0x2

    aget-object v8, v2, v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x4

    aget-object v8, v2, v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-direct {v1, p0, v10, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerHost:Landroid/widget/Spinner;

    invoke-virtual {v8, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerHost:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v5, Landroid/widget/ArrayAdapter;

    const v8, 0x7f060041

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, p0, v10, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v5, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v4, Landroid/widget/ArrayAdapter;

    const v8, 0x7f060042

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, p0, v10, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v4, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v8, 0x7f060043

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, p0, v10, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v6, Landroid/widget/ArrayAdapter;

    const v8, 0x7f060044

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, p0, v10, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v6, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMode:Landroid/widget/Spinner;

    invoke-virtual {v8, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMode:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMaxCurrent:Landroid/widget/Spinner;

    invoke-virtual {v8, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMaxCurrent:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerDrive:Landroid/widget/Spinner;

    invoke-virtual {v8, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerDrive:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerPowerControl:Landroid/widget/Spinner;

    invoke-virtual {v8, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerPowerControl:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMode:Landroid/widget/Spinner;

    iget v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexMode:I

    invoke-virtual {v8, v9}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerMaxCurrent:Landroid/widget/Spinner;

    iget v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexMaxCurrent:I

    invoke-virtual {v8, v9}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerDrive:Landroid/widget/Spinner;

    iget v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexDrive:I

    invoke-virtual {v8, v9}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mSpinnerPowerControl:Landroid/widget/Spinner;

    iget v9, p0, Lcom/mediatek/engineermode/io/MsdcSd3Test;->mIndexPowerControl:I

    invoke-virtual {v8, v9}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void
.end method
