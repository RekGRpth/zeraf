.class public Lcom/mediatek/engineermode/dcm/ApdcmActivity;
.super Landroid/app/Activity;
.source "ApdcmActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final APDCM_NUM:I = 0x7

.field private static final CAT:Ljava/lang/String; = "cat "

.field private static final CMD_SET_DCM:Ljava/lang/String; = "echo 1 0 %1$d %2$s > /proc/dcm/dbg"

.field private static final FS_DBG:Ljava/lang/String; = "/proc/dcm/dbg"

.field private static final FS_DUMPREGS:Ljava/lang/String; = "/proc/dcm/dumpregs"

.field private static final FS_HELP:Ljava/lang/String; = "/proc/dcm/help"

.field private static final TAG:Ljava/lang/String; = "APDCM"


# instance fields
.field private mApdcmEdits:[Landroid/widget/EditText;

.field private mApdcmTags:[Ljava/lang/String;

.field private mBtnArmRead:Landroid/widget/Button;

.field private mBtnArmSet:Landroid/widget/Button;

.field private mBtnDumpRegs:Landroid/widget/Button;

.field private mBtnEmiRead:Landroid/widget/Button;

.field private mBtnEmiSet:Landroid/widget/Button;

.field private mBtnHelp:Landroid/widget/Button;

.field private mBtnInfraRead:Landroid/widget/Button;

.field private mBtnInfraSet:Landroid/widget/Button;

.field private mBtnMfgRead:Landroid/widget/Button;

.field private mBtnMfgSet:Landroid/widget/Button;

.field private mBtnMiscRead:Landroid/widget/Button;

.field private mBtnMiscSet:Landroid/widget/Button;

.field private mBtnMmRead:Landroid/widget/Button;

.field private mBtnMmSet:Landroid/widget/Button;

.field private mBtnPeriRead:Landroid/widget/Button;

.field private mBtnPeriSet:Landroid/widget/Button;

.field private mDcmStr:Ljava/lang/String;

.field private mEditArmDcm:Landroid/widget/EditText;

.field private mEditEmiDcm:Landroid/widget/EditText;

.field private mEditInfraDcm:Landroid/widget/EditText;

.field private mEditMfgDcm:Landroid/widget/EditText;

.field private mEditMiscDcm:Landroid/widget/EditText;

.field private mEditMmDcm:Landroid/widget/EditText;

.field private mEditPeriDcm:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x7

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-array v0, v1, [Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    return-void
.end method

.method private execCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, -0x1

    const-string v3, "APDCM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[cmd]:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {p1}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    if-nez v2, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v1

    const-string v3, "APDCM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[output]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    const-string v3, "APDCM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private handleClickReadBtn(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->setUiByData(IZ)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0805dd

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0805ea

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private handleClickSetBtn(I)V
    .locals 11
    .param p1    # I

    const v10, 0x7f0805eb

    const/16 v7, 0x10

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    aget-object v5, v5, p1

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    aget-object v5, v5, p1

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v7}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->parseLongStr(Ljava/lang/String;I)J

    move-result-wide v2

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-ltz v5, :cond_1

    const-string v5, "ffffffff"

    invoke-direct {p0, v5, v7}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->parseLongStr(Ljava/lang/String;I)J

    move-result-wide v5

    cmp-long v5, v2, v5

    if-lez v5, :cond_2

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " DCM:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const-string v5, "echo 1 0 %1$d %2$s > /proc/dcm/dbg"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    aput-object v1, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f0805de

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f0805ea

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method private parseLongStr(Ljava/lang/String;I)J
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-wide/16 v1, -0x1

    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    move-wide v3, v1

    :goto_0
    return-wide v3

    :catch_0
    move-exception v0

    const-wide/16 v3, -0x1

    goto :goto_0
.end method

.method private resolveFillData(Ljava/lang/String;I)Z
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x7

    const-string v6, " *, *\n *"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v6, v0

    if-eq v6, v8, :cond_0

    const-string v5, "APDCM"

    const-string v6, "resolveFillData() Resolve outStr fail, Invalid DCM Number"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v4

    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v8, :cond_6

    if-eq p2, v8, :cond_1

    if-ne v1, p2, :cond_5

    :cond_1
    aget-object v6, v0, v1

    const-string v7, " *= *"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v3, v2, v5

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    :cond_2
    const-string v6, "0x"

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "0X"

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_4

    :cond_3
    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :cond_4
    iget-object v6, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    aget-object v6, v6, v1

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    move v4, v5

    goto :goto_0
.end method

.method private setUiByData(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    const-string v0, "cat /proc/dcm/dbg"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v2, "Feature Fail or Don\'t Support!"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mDcmStr:Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, v1, p1}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->resolveFillData(Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method private showDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const-string v4, "APDCM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onClick() Unknown view id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_1
    const-string v0, "cat /proc/dcm/dumpregs"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f0805df

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "cat /proc/dcm/help"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f0805e0

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickReadBtn(I)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickReadBtn(I)V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickReadBtn(I)V

    goto :goto_0

    :pswitch_6
    invoke-direct {p0, v7}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickReadBtn(I)V

    goto :goto_0

    :pswitch_7
    invoke-direct {p0, v8}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickReadBtn(I)V

    goto :goto_0

    :pswitch_8
    const/4 v4, 0x5

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickReadBtn(I)V

    goto :goto_0

    :pswitch_9
    const/4 v4, 0x6

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickReadBtn(I)V

    goto :goto_0

    :pswitch_a
    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickSetBtn(I)V

    goto :goto_0

    :pswitch_b
    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickSetBtn(I)V

    goto :goto_0

    :pswitch_c
    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickSetBtn(I)V

    goto :goto_0

    :pswitch_d
    invoke-direct {p0, v7}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickSetBtn(I)V

    goto :goto_0

    :pswitch_e
    invoke-direct {p0, v8}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickSetBtn(I)V

    goto :goto_0

    :pswitch_f
    const/4 v4, 0x5

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickSetBtn(I)V

    goto :goto_0

    :pswitch_10
    const/4 v4, 0x6

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->handleClickSetBtn(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00ee
        :pswitch_3
        :pswitch_a
        :pswitch_0
        :pswitch_4
        :pswitch_b
        :pswitch_0
        :pswitch_5
        :pswitch_c
        :pswitch_0
        :pswitch_6
        :pswitch_d
        :pswitch_0
        :pswitch_7
        :pswitch_e
        :pswitch_0
        :pswitch_8
        :pswitch_f
        :pswitch_0
        :pswitch_9
        :pswitch_10
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030021

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f0b00ed

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditArmDcm:Landroid/widget/EditText;

    const v1, 0x7f0b00f0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditEmiDcm:Landroid/widget/EditText;

    const v1, 0x7f0b00f3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditInfraDcm:Landroid/widget/EditText;

    const v1, 0x7f0b00f6

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditPeriDcm:Landroid/widget/EditText;

    const v1, 0x7f0b00f9

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditMiscDcm:Landroid/widget/EditText;

    const v1, 0x7f0b00fc

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditMmDcm:Landroid/widget/EditText;

    const v1, 0x7f0b00ff

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditMfgDcm:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditArmDcm:Landroid/widget/EditText;

    aput-object v2, v1, v5

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditEmiDcm:Landroid/widget/EditText;

    aput-object v2, v1, v4

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditInfraDcm:Landroid/widget/EditText;

    aput-object v2, v1, v6

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditPeriDcm:Landroid/widget/EditText;

    aput-object v2, v1, v7

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditMiscDcm:Landroid/widget/EditText;

    aput-object v2, v1, v8

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditMmDcm:Landroid/widget/EditText;

    aput-object v3, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mEditMfgDcm:Landroid/widget/EditText;

    aput-object v3, v1, v2

    const v1, 0x7f0b00ee

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnArmRead:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnArmRead:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00f1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnEmiRead:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnEmiRead:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00f4

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnInfraRead:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnInfraRead:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00f7

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnPeriRead:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnPeriRead:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00fa

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMiscRead:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMiscRead:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00fd

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMmRead:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMmRead:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0100

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMfgRead:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMfgRead:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00ef

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnArmSet:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnArmSet:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00f2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnEmiSet:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnEmiSet:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00f5

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnInfraSet:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnInfraSet:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00f8

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnPeriSet:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnPeriSet:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00fb

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMiscSet:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMiscSet:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b00fe

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMmSet:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMmSet:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0101

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMfgSet:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnMfgSet:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0102

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnDumpRegs:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnDumpRegs:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0103

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnHelp:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mBtnHelp:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    const v2, 0x7f0805d5

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    const v2, 0x7f0805d6

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    const v2, 0x7f0805d7

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    const v2, 0x7f0805d8

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    const v2, 0x7f0805d9

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    const/4 v2, 0x5

    const v3, 0x7f0805da

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    const/4 v2, 0x6

    const v3, 0x7f0805db

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const v1, 0x7f0805d2

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mDcmStr:Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmEdits:[Landroid/widget/EditText;

    aget-object v1, v1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mApdcmTags:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->mDcmStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x7

    invoke-direct {p0, v1, v4}, Lcom/mediatek/engineermode/dcm/ApdcmActivity;->setUiByData(IZ)V

    return-void
.end method
