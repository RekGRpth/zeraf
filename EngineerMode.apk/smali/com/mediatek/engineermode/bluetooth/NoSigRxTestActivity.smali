.class public Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;
.super Landroid/app/Activity;
.source "NoSigRxTestActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$WorkRunnable;
    }
.end annotation


# static fields
.field private static final CHECK_BT_STATE:I = 0x14

.field private static final DIALOG_RX_FAIL:I = 0x15

.field private static final DIALOG_RX_TEST:I = 0x16

.field public static final OP_ADDR_DEFAULT:I = 0xb

.field public static final OP_FINISH:I = 0x9

.field public static final OP_IN_PROCESS:I = 0x8

.field public static final OP_RX_FAIL:I = 0xa

.field public static final OP_TEST_OK_STEP1:I = 0xc

.field public static final OP_TEST_OK_STEP2:I = 0xd

.field private static final TAG:Ljava/lang/String; = "NoSigRx"

.field private static final TEST_STATUS_BEGIN:I = 0x64

.field private static final TEST_STATUS_RESULT:I = 0x65


# instance fields
.field private mBitErrRate:Landroid/widget/TextView;

.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

.field private mBtnStartTest:Landroid/widget/Button;

.field private mDoneFinished:Z

.field private mEdAddr:Landroid/widget/EditText;

.field private mEdFreq:Landroid/widget/EditText;

.field private mPackCnt:Landroid/widget/TextView;

.field private mPackErrRate:Landroid/widget/TextView;

.field private mPattern:Landroid/widget/Spinner;

.field private mPocketType:Landroid/widget/Spinner;

.field private mResult:[I

.field private mRxByteCnt:Landroid/widget/TextView;

.field private mStateBt:I

.field private mTestStatus:I

.field private mUiHandler:Landroid/os/Handler;

.field private mWorkHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mResult:[I

    const/16 v0, 0x64

    iput v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mTestStatus:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mDoneFinished:Z

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mWorkHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    new-instance v0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$1;-><init>(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mEdAddr:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->doSendCommandAction()Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mTestStatus:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtnStartTest:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)[I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mResult:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPackCnt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPackErrRate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mRxByteCnt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBitErrRate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$902(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mDoneFinished:Z

    return p1
.end method

.method private doSendCommandAction()Z
    .locals 2

    iget v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mTestStatus:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->getBtState()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->enableBluetooth(Z)V

    invoke-virtual {p0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->getValuesAndSend()V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    iget v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mTestStatus:I

    const/16 v1, 0x65

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->getResult()V

    goto :goto_0
.end method

.method private enableBluetooth(Z)V
    .locals 3
    .param p1    # Z

    const-string v1, "NoSigRx"

    const-string v2, "Enter EnableBluetooth()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "NoSigRx"

    const-string v2, "we can not find a bluetooth adapter."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string v1, "NoSigRx"

    const-string v2, "Bluetooth is enabled"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    :goto_1
    const-string v1, "NoSigRx"

    const-string v2, "Leave EnableBluetooth()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "NoSigRx"

    const-string v2, "Bluetooth is disabled"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto :goto_1
.end method

.method private getBtState()V
    .locals 3

    const-string v1, "NoSigRx"

    const-string v2, "Enter GetBtState()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "NoSigRx"

    const-string v2, "we can not find a bluetooth adapter."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mStateBt:I

    const-string v1, "NoSigRx"

    const-string v2, "Leave GetBtState()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getResult()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/bluetooth/BtTest;->noSigRxTestResult()[I

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mResult:[I

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mResult:[I

    if-nez v0, :cond_3

    const-string v0, "NoSigRx"

    const-string v1, "no signal rx test failed."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0xb

    iget v1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mStateBt:I

    if-eq v0, v1, :cond_1

    const/16 v0, 0xc

    iget v1, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mStateBt:I

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->enableBluetooth(Z)V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    const-string v0, "NoSigRx"

    const-string v1, "Leave getresult()."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method


# virtual methods
.method public getValuesAndSend()V
    .locals 13

    const/16 v12, 0xc

    const/16 v11, 0xb

    const/16 v10, 0xa

    const-string v8, "NoSigRx"

    const-string v9, "Enter GetValuesAndSend()."

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-direct {v8}, Lcom/mediatek/engineermode/bluetooth/BtTest;-><init>()V

    iput-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPattern:Landroid/widget/Spinner;

    invoke-virtual {v8}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v5

    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPocketType:Landroid/widget/Spinner;

    invoke-virtual {v8}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    const/4 v4, 0x0

    const/4 v3, 0x0

    :try_start_0
    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mEdFreq:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mEdAddr:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v3, v1

    if-ltz v4, :cond_0

    const/16 v8, 0x4e

    if-le v4, v8, :cond_1

    :cond_0
    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    const/16 v9, 0xa

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_1
    if-nez v3, :cond_2

    const v3, 0xa5f0c3

    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    const/16 v9, 0xb

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtTest:Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-virtual {v8, v5, v6, v4, v3}, Lcom/mediatek/engineermode/bluetooth/BtTest;->noSigRxTestStart(IIII)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v8, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    const-string v8, "NoSigRx"

    const-string v9, "Leave GetValuesAndSend()."

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v8, "NoSigRx"

    const-string v9, "input number error!"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v8, "NoSigRx"

    const-string v9, "no signal rx test failed."

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mStateBt:I

    if-eq v11, v8, :cond_4

    iget v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mStateBt:I

    if-ne v12, v8, :cond_5

    :cond_4
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->enableBluetooth(Z)V

    :cond_5
    iget-object v8, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v8, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    const-string v0, "NoSigRx"

    const-string v1, "-->onCancel"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mDoneFinished:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mWorkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$WorkRunnable;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$WorkRunnable;-><init>(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "NoSigRx"

    const-string v1, "last click is not finished yet."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030075

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->setValuesSpinner()V

    const v2, 0x7f0b03cf

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mEdFreq:Landroid/widget/EditText;

    const v2, 0x7f0b03d0

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mEdAddr:Landroid/widget/EditText;

    const v2, 0x7f0b03d1

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtnStartTest:Landroid/widget/Button;

    const v2, 0x7f0b03d2

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPackCnt:Landroid/widget/TextView;

    const v2, 0x7f0b03d3

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPackErrRate:Landroid/widget/TextView;

    const v2, 0x7f0b03d4

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mRxByteCnt:Landroid/widget/TextView;

    const v2, 0x7f0b03d5

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBitErrRate:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtnStartTest:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "NoSigRx"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mWorkHandler:Landroid/os/Handler;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f080285

    const v3, 0x7f080284

    const/4 v2, 0x0

    const/16 v1, 0x14

    if-ne p1, v1, :cond_0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080287

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$2;-><init>(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/16 v1, 0x15

    if-ne p1, v1, :cond_1

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080290

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$3;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity$3;-><init>(Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v1, 0x16

    if-ne p1, v1, :cond_2

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f08028f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const v1, 0x7f0802ac

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_1
    return-void
.end method

.method protected setValuesSpinner()V
    .locals 5

    const v4, 0x1090009

    const v3, 0x1090008

    const v2, 0x7f0b03cd

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPattern:Landroid/widget/Spinner;

    const v2, 0x7f060018

    invoke-static {p0, v2, v3}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPattern:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v2, 0x7f0b03ce

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPocketType:Landroid/widget/Spinner;

    const v2, 0x7f060019

    invoke-static {p0, v2, v3}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/NoSigRxTestActivity;->mPocketType:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method
