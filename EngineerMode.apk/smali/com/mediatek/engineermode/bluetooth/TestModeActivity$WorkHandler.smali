.class final Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;
.super Landroid/os/Handler;
.source "TestModeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/bluetooth/TestModeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;Landroid/os/Looper;Lcom/mediatek/engineermode/bluetooth/TestModeActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/engineermode/bluetooth/TestModeActivity;
    .param p2    # Landroid/os/Looper;
    .param p3    # Lcom/mediatek/engineermode/bluetooth/TestModeActivity$1;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;-><init>(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1    # Landroid/os/Message;

    const/4 v8, 0x5

    const/4 v7, -0x1

    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xb

    if-ne v4, v5, :cond_2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    :try_start_0
    const-string v4, "su"

    invoke-virtual {v1, v4}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    const-string v4, "TestMode"

    const-string v5, "excute su command."

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    new-instance v5, Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-direct {v5}, Lcom/mediatek/engineermode/bluetooth/BtTest;-><init>()V

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$402(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;Lcom/mediatek/engineermode/bluetooth/BtTest;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-static {v4}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$200(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    :goto_1
    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-static {v4}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$400(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;->setPower(I)V

    const-string v4, "TestMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "power set "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-static {v4}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$400(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/mediatek/engineermode/bluetooth/BtTest;->doBtTest(I)I

    move-result v4

    if-ne v7, v4, :cond_1

    const-string v4, "TestMode"

    const-string v5, "transmit data failed."

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-static {v4}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$500(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_2
    return-void

    :catch_0
    move-exception v0

    const-string v4, "TestMode"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v4, "TestMode"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-static {v4}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$500(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    :cond_2
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    :try_start_2
    const-string v4, "su"

    invoke-virtual {v1, v4}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    const-string v4, "TestMode"

    const-string v5, "excute su command."

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-static {v4}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$400(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-static {v4}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$400(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/mediatek/engineermode/bluetooth/BtTest;->doBtTest(I)I

    move-result v4

    if-ne v7, v4, :cond_3

    const-string v4, "TestMode"

    const-string v5, "transmit data failed 1."

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$402(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;Lcom/mediatek/engineermode/bluetooth/BtTest;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    :cond_4
    iget-object v4, p0, Lcom/mediatek/engineermode/bluetooth/TestModeActivity$WorkHandler;->this$0:Lcom/mediatek/engineermode/bluetooth/TestModeActivity;

    invoke-static {v4}, Lcom/mediatek/engineermode/bluetooth/TestModeActivity;->access$500(Lcom/mediatek/engineermode/bluetooth/TestModeActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    :catch_2
    move-exception v0

    const-string v4, "TestMode"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
