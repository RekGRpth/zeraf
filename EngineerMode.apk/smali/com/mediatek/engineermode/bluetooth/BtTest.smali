.class public Lcom/mediatek/engineermode/bluetooth/BtTest;
.super Ljava/lang/Object;
.source "BtTest.java"


# instance fields
.field private mChannels:I

.field private mFreq:I

.field private mPatter:I

.field private mPocketType:I

.field private mPocketTypeLen:I

.field private mPower:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "em_bt_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPatter:I

    iput v0, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mChannels:I

    iput v0, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPocketType:I

    iput v1, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPocketTypeLen:I

    iput v1, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mFreq:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPower:I

    return-void
.end method


# virtual methods
.method public native doBtTest(I)I
.end method

.method public native getChipEcoNum()I
.end method

.method public native getChipId()I
.end method

.method public native getChipIdInt()I
.end method

.method public getFreq()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mFreq:I

    return v0
.end method

.method public native getPatchId()[C
.end method

.method public native getPatchLen()J
.end method

.method public getPocketType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPocketType:I

    return v0
.end method

.method public native hciCommandRun([CI)[C
.end method

.method public native hciReadEvent()[C
.end method

.method public native init()I
.end method

.method public native isBLESupport()I
.end method

.method public native isComboSupport()I
.end method

.method public native noSigRxTestResult()[I
.end method

.method public native noSigRxTestStart(IIII)Z
.end method

.method public native relayerExit()I
.end method

.method public native relayerStart(II)I
.end method

.method public setChannels(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mChannels:I

    return-void
.end method

.method public native setFWDump(J)I
.end method

.method public setFreq(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mFreq:I

    return-void
.end method

.method public setPatter(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPatter:I

    return-void
.end method

.method public setPocketType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPocketType:I

    return-void
.end method

.method public setPocketTypeLen(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPocketTypeLen:I

    return-void
.end method

.method public setPower(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/bluetooth/BtTest;->mPower:I

    return-void
.end method

.method public native unInit()I
.end method
