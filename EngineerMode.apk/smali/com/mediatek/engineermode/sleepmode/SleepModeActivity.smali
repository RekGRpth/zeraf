.class public Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;
.super Landroid/app/Activity;
.source "SleepModeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final CAT:Ljava/lang/String; = "cat "

.field private static final CMD_CPU_PDN:Ljava/lang/String; = "echo \"%1$d cpu_pdn\" > /proc/spm_fs/suspend"

.field private static final CMD_FGAUGE:Ljava/lang/String; = "echo \"%1$d fgauge\"  > /proc/spm_fs/suspend"

.field private static final CMD_INFRA_PDN:Ljava/lang/String; = "echo \"%1$d infra_pdn\" > /proc/spm_fs/suspend"

.field private static final CMD_TIMER_VAL:Ljava/lang/String; = "echo \"%1$s timer_val_ms\"  > /proc/spm_fs/suspend"

.field private static final CMD_WAKE_LOCK:Ljava/lang/String; = "echo nosleep > /sys/power/wake_lock"

.field private static final CMD_WAKE_UNLOCK:Ljava/lang/String; = "echo nosleep > /sys/power/wake_unlock"

.field private static final FS_SUSPEND:Ljava/lang/String; = "/proc/spm_fs/suspend"

.field private static final FS_SUSPEND_MODE:Ljava/lang/String; = "/proc/spm_fs/suspend_mode"

.field private static final FS_SUSPEND_TIMER:Ljava/lang/String; = "/proc/spm_fs/suspend_timer"

.field private static final FS_WAKE_LOCK:Ljava/lang/String; = "/sys/power/wake_lock"

.field private static final SLEEP_MODE_DISABLE:I = 0x0

.field private static final SLEEP_MODE_LEGACY_SLEEP:I = 0x1

.field private static final SLEEP_MODE_SHUT_DOWN:I = 0x2

.field private static final TAG:Ljava/lang/String; = "EM/SleepMode"

.field private static final TAG_NOSLEEP:Ljava/lang/String; = "nosleep"


# instance fields
.field private mBtnGetSetting:Landroid/widget/Button;

.field private mBtnStartTimer:Landroid/widget/Button;

.field private mEditTimerVal:Landroid/widget/EditText;

.field private mLVTimerControler:Landroid/widget/LinearLayout;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRBFuelGauge:Landroid/widget/RadioButton;

.field private mRBLagacySleep:Landroid/widget/RadioButton;

.field private mRBModeDisable:Landroid/widget/RadioButton;

.field private mRBShutDown:Landroid/widget/RadioButton;

.field private mRBSleepModes:[Landroid/widget/RadioButton;

.field private mRBTimerDisable:Landroid/widget/RadioButton;

.field private mRBTimerValSet:Landroid/widget/RadioButton;

.field private mRBWakeupTimers:[Landroid/widget/RadioButton;

.field private mSleepMode:I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    return-void
.end method

.method private acquireWakeLock()Z
    .locals 2

    const-string v0, "echo nosleep > /sys/power/wake_lock"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V
    .locals 3
    .param p1    # [Landroid/widget/RadioButton;
    .param p2    # Landroid/widget/RadioButton;
    .param p3    # Z

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    aget-object v1, p1, v0

    if-ne p2, v1, :cond_0

    aget-object v1, p1, v0

    invoke-virtual {v1, p3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    aget-object v2, p1, v0

    if-nez p3, :cond_1

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    return-void
.end method

.method private enableInputTimerUI(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mBtnStartTimer:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method private execCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, -0x1

    const-string v3, "EM/SleepMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[cmd]:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {p1}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    if-nez v2, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v1

    const-string v3, "EM/SleepMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[output]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    const-string v3, "EM/SleepMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private initUIByData()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-string v0, "cat /sys/power/wake_lock"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v3, "Feature Fail or Don\'t Support!"

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v3, "nosleep"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBModeDisable:Landroid/widget/RadioButton;

    invoke-virtual {v3, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iput v7, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    :goto_1
    const-string v0, "cat /proc/spm_fs/suspend_timer"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    const-string v3, "Feature Fail or Don\'t Support!"

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    const-string v0, "cat /proc/spm_fs/suspend_mode"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v3, "Feature Fail or Don\'t Support!"

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v3, "0"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBLagacySleep:Landroid/widget/RadioButton;

    invoke-virtual {v3, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iput v6, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    goto :goto_1

    :cond_3
    const-string v3, "1"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBShutDown:Landroid/widget/RadioButton;

    invoke-virtual {v3, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const/4 v3, 0x2

    iput v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    goto :goto_1

    :cond_4
    const/4 v3, -0x1

    iput v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    const-string v3, "EM/SleepMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cat suspend_mode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v3, " +"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    aget-object v4, v2, v7

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v4, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBFuelGauge:Landroid/widget/RadioButton;

    invoke-direct {p0, v3, v4, v6}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    goto/16 :goto_0

    :cond_6
    const-string v3, "0"

    aget-object v4, v2, v6

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v4, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerDisable:Landroid/widget/RadioButton;

    invoke-direct {p0, v3, v4, v6}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v4, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerValSet:Landroid/widget/RadioButton;

    invoke-direct {p0, v3, v4, v6}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mEditTimerVal:Landroid/widget/EditText;

    aget-object v4, v2, v6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private releaseWakeLock()Z
    .locals 2

    const-string v0, "echo nosleep > /sys/power/wake_unlock"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setCpuPdn(I)Z
    .locals 6
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "echo \"%1$d cpu_pdn\" > /proc/spm_fs/suspend"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private setFgauge(I)Z
    .locals 6
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "echo \"%1$d fgauge\"  > /proc/spm_fs/suspend"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private setInfraPdn(I)Z
    .locals 6
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "echo \"%1$d infra_pdn\" > /proc/spm_fs/suspend"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private setTimerVal(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "echo \"%1$s timer_val_ms\"  > /proc/spm_fs/suspend"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private showDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private validateInput()Z
    .locals 8

    const v7, 0x7f0805f8

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :goto_0
    return v4

    :cond_0
    iget-object v5, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/16 v5, 0xf

    if-lt v2, v5, :cond_1

    const v5, 0xf4240

    if-le v2, v5, :cond_2

    :cond_1
    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, "EM/SleepMode"

    const-string v6, "NumberFormatException: parse timerVal fail"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v1, "EM/SleepMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown view id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    if-eqz p2, :cond_0

    iput v4, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBSleepModes:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBModeDisable:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mLVTimerControler:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->acquireWakeLock()Z

    goto :goto_0

    :pswitch_2
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBSleepModes:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBLagacySleep:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mLVTimerControler:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->releaseWakeLock()Z

    :cond_1
    iput v3, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setCpuPdn(I)Z

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setInfraPdn(I)Z

    goto :goto_0

    :pswitch_3
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBSleepModes:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBShutDown:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mLVTimerControler:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->releaseWakeLock()Z

    :cond_2
    const/4 v1, 0x2

    iput v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mSleepMode:I

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setCpuPdn(I)Z

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setInfraPdn(I)Z

    goto :goto_0

    :pswitch_4
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerDisable:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setFgauge(I)Z

    const-string v1, "0"

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setTimerVal(Ljava/lang/String;)Z

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->enableInputTimerUI(Z)V

    goto :goto_0

    :pswitch_5
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBFuelGauge:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setFgauge(I)Z

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->enableInputTimerUI(Z)V

    goto :goto_0

    :pswitch_6
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerValSet:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->enableInputTimerUI(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b03ec
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v4, "EM/SleepMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unknown view id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->validateInput()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setFgauge(I)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->setTimerVal(Ljava/lang/String;)Z

    const v4, 0x7f0805f9

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_1
    const-string v0, "cat /proc/spm_fs/suspend"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0805ef

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v3}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b03f4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03007b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b03ec

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBModeDisable:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBModeDisable:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b03ed

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBLagacySleep:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBLagacySleep:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b03ee

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBShutDown:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBShutDown:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-array v0, v5, [Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBSleepModes:[Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBSleepModes:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBModeDisable:Landroid/widget/RadioButton;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBSleepModes:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBLagacySleep:Landroid/widget/RadioButton;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBSleepModes:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBShutDown:Landroid/widget/RadioButton;

    aput-object v1, v0, v4

    const v0, 0x7f0b03f0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerDisable:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerDisable:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b03f1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBFuelGauge:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBFuelGauge:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b03f2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerValSet:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerValSet:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-array v0, v5, [Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerDisable:Landroid/widget/RadioButton;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBFuelGauge:Landroid/widget/RadioButton;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBWakeupTimers:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mRBTimerValSet:Landroid/widget/RadioButton;

    aput-object v1, v0, v4

    const v0, 0x7f0b03f4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mBtnStartTimer:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mBtnStartTimer:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b03f5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mBtnGetSetting:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mBtnGetSetting:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b03f3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mEditTimerVal:Landroid/widget/EditText;

    const v0, 0x7f0b03ef

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mLVTimerControler:Landroid/widget/LinearLayout;

    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mPowerManager:Landroid/os/PowerManager;

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mPowerManager:Landroid/os/PowerManager;

    const/16 v1, 0x1a

    const-string v2, "EM/SleepMode"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-direct {p0}, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->initUIByData()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/sleepmode/SleepModeActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
