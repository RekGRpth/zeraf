.class public Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;
.super Landroid/view/View;
.source "TsMultiTouch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field public mInputIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Vector",
            "<",
            "Ljava/util/Vector",
            "<",
            "Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mMinPtrId:I

.field public mPtsStatus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;


# direct methods
.method public constructor <init>(Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mMinPtrId:I

    return-void
.end method

.method private calcMinId(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mMinPtrId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mMinPtrId:I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mMinPtrId:I

    if-ge v0, p1, :cond_1

    iget p1, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mMinPtrId:I

    :cond_1
    iput p1, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mMinPtrId:I

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 5

    iget-object v4, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iget-object v4, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method getPaint(I)Landroid/graphics/Paint;
    .locals 7
    .param p1    # I

    const/4 v3, 0x0

    const/16 v6, 0xff

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v2, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->RGB:[[I

    array-length v2, v2

    if-ge p1, v2, :cond_0

    sget-object v2, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->RGB:[[I

    aget-object v2, v2, p1

    aget v2, v2, v3

    sget-object v3, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->RGB:[[I

    aget-object v3, v3, p1

    const/4 v4, 0x1

    aget v3, v3, v4

    sget-object v4, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->RGB:[[I

    aget-object v4, v4, p1

    const/4 v5, 0x2

    aget v4, v4, v5

    invoke-virtual {v0, v6, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    :goto_0
    iget-object v2, p0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    iget v2, v2, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->mPointSize:I

    int-to-double v2, v2

    const-wide v4, 0x400d0a3d70a3d70aL

    mul-double/2addr v2, v4

    const-wide v4, 0x401d7ae147ae147bL

    add-double/2addr v2, v4

    double-to-int v1, v2

    int-to-float v2, v1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    return-object v0

    :cond_0
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 24
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v5, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Vector;

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v18

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v9

    const-string v21, "EM/TouchScreen/MT"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "idx: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " input size: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v9, :cond_7

    invoke-virtual {v8, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Vector;

    invoke-virtual {v14}, Ljava/util/Vector;->size()I

    move-result v15

    const-string v21, "EM/TouchScreen/MT"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Line"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " size "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v15, :cond_4

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateX()I

    move-result v12

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateY()I

    move-result v13

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v15, :cond_1

    invoke-virtual {v14, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateX()I

    move-result v19

    invoke-virtual {v14, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateY()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->mDisplayHistory:Z

    move/from16 v21, v0

    if-eqz v21, :cond_0

    int-to-float v0, v12

    move/from16 v21, v0

    int-to-float v0, v13

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->mPointSize:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_0
    move/from16 v12, v19

    move/from16 v13, v20

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v21, v15, -0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;

    add-int/lit8 v21, v9, -0x1

    move/from16 v0, v21

    if-ne v10, v0, :cond_4

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "pid "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmPid()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " x="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateX()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", y="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateY()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    const/16 v21, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v22

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    move-object/from16 v4, v16

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    invoke-virtual {v11}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateX()I

    move-result v21

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v22

    div-int/lit8 v22, v22, 0x2

    sub-int v19, v21, v22

    invoke-virtual {v11}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateY()I

    move-result v21

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v22

    mul-int/lit8 v22, v22, 0x3

    sub-int v20, v21, v22

    if-gez v19, :cond_5

    const/16 v19, 0x0

    :cond_2
    :goto_3
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_6

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v20

    :cond_3
    :goto_4
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-virtual {v11}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateX()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    invoke-virtual {v11}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->getmCoordinateY()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->mPointSize:I

    move/from16 v23, v0

    mul-int/lit8 v23, v23, 0x3

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move-object/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->mMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v21, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v22

    sub-int v21, v21, v22

    move/from16 v0, v19

    move/from16 v1, v21

    if-le v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->mMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v21, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v22

    sub-int v19, v21, v22

    goto/16 :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->mMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->this$0:Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch;->mMetrics:Landroid/util/DisplayMetrics;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v20, v0

    goto/16 :goto_4

    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_8
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v3, v2, 0xff

    shr-int/lit8 v14, v2, 0x8

    const-string v17, "EM/TouchScreen/MT"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "onTouchEvent: ptIdx: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " mPtsStatus.size(): "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0x5

    move/from16 v0, v17

    if-eq v3, v0, :cond_0

    if-nez v3, :cond_5

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v14, v0, :cond_4

    new-instance v13, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;

    invoke-direct {v13}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;-><init>()V

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->setmDown(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    const/4 v7, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v7, v0, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;

    const-string v17, "EM/TouchScreen/MT"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mPtsStatus.size(): "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " st.ismDown(): "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->ismDown()Z

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " st.ismNewLine(): "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->ismNewLine()Z

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v15}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->ismDown()Z

    move-result v17

    if-eqz v17, :cond_8

    invoke-virtual {v15}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->ismNewLine()Z

    move-result v17

    if-nez v17, :cond_3

    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v7, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    new-instance v18, Ljava/util/Vector;

    invoke-direct/range {v18 .. v18}, Ljava/util/Vector;-><init>()V

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/Vector;

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->setmNewLine(Z)V

    :cond_3
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->setmDown(Z)V

    goto/16 :goto_0

    :cond_5
    const/16 v17, 0x6

    move/from16 v0, v17

    if-eq v3, v0, :cond_6

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v3, v0, :cond_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v14, v0, :cond_7

    new-instance v13, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;

    invoke-direct {v13}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;-><init>()V

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->setmDown(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->setmDown(Z)V

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mPtsStatus:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/engineermode/touchscreen/TsPointStatusStruct;->setmNewLine(Z)V

    goto :goto_2

    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v12

    const-string v17, "EM/TouchScreen/MT"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Pointer counts = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    :goto_3
    if-ge v6, v12, :cond_c

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v10

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->calcMinId(I)V

    const-string v17, "EM/TouchScreen/MT"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " i ="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " notZeroBasedPid = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " mMinPtrId = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mMinPtrId:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mMinPtrId:I

    move/from16 v17, v0

    sub-int v11, v10, v17

    :try_start_0
    new-instance v8, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;

    invoke-direct {v8}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;-><init>()V

    invoke-virtual {v8, v3}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->setmAction(I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->setmCoordinateX(I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->setmCoordinateY(I)V

    invoke-virtual {v8, v11}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->setmPid(I)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->setmPressure(F)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v16

    const v17, 0x3c23d70a

    cmpg-float v17, v16, v17

    if-gez v17, :cond_a

    const v16, 0x3c23d70a

    :cond_a
    move/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/mediatek/engineermode/touchscreen/TsPointDataStruct;->setmFatSize(F)V

    const-string v17, "EM/TouchScreen/MT"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " pid = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " mInputIds.size() = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_b

    const-string v18, "EM/TouchScreen/MT"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " mInputIds.get(i).size() = "

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/Vector;

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->size()I

    move-result v17

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/Vector;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/engineermode/touchscreen/TsMultiTouch$MyView;->mInputIds:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/Vector;

    invoke-virtual/range {v18 .. v18}, Ljava/util/Vector;->size()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    invoke-virtual/range {v17 .. v18}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Vector;

    invoke-virtual {v4, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_b
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_3

    :catch_0
    move-exception v5

    const-string v17, "EM/TouchScreen/MT"

    const-string v18, "get point data fail!!"

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v17, "EM/TouchScreen/MT"

    invoke-virtual {v5}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_c
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    const/16 v17, 0x1

    return v17
.end method
