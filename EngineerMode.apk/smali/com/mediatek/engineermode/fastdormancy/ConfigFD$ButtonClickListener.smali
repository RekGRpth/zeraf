.class public Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;
.super Ljava/lang/Object;
.source "ConfigFD.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/fastdormancy/ConfigFD;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ButtonClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;


# direct methods
.method public constructor <init>(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const/4 v5, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f0b0163

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$000(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)Landroid/widget/RadioGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    const v3, 0x7f0b0161

    if-ne v0, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$200(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)I

    move-result v3

    if-eq v3, v5, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$200(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)I

    move-result v3

    const v4, 0x7fffff

    and-int v2, v3, v4

    const-string v3, "EM_FD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "To Modem :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AT+EPCT="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v4}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v6

    const-string v3, ""

    aput-object v3, v1, v7

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$400(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v4}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$300(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const-string v3, "EM_FD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invoke cmdStr :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    const-string v4, "Get FD data fail!"

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    const-string v3, "EM_FD"

    const-string v4, "returnData is null"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const v3, 0x7f0b0162

    if-ne v0, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$200(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)I

    move-result v3

    if-eq v3, v5, :cond_3

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$200(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)I

    move-result v3

    const/high16 v4, 0x800000

    or-int v2, v3, v4

    const-string v3, "EM_FD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "To Modem :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AT+EPCT="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v4}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v6

    const-string v3, ""

    aput-object v3, v1, v7

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$400(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v4}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$300(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    const-string v3, "EM_FD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invoke cmdStr :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    const-string v4, "Get FD data fail!"

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    const-string v3, "EM_FD"

    const-string v4, "returnData is null"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$ButtonClickListener;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0
.end method
