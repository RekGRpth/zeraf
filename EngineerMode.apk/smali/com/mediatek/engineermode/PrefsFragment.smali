.class public Lcom/mediatek/engineermode/PrefsFragment;
.super Landroid/preference/PreferenceFragment;
.source "PrefsFragment.java"


# static fields
.field private static final FRAGMENT_RES:[I

.field private static final INNER_LOAD_INDICATOR_FILE:Ljava/lang/String; = "/system/etc/system_update/address.xml"

.field private static final MTK_NFC_CHIP_TYPE_MSR3110:I = 0x1

.field private static final MTK_NFC_CHIP_TYPE_MT6605:I = 0x2

.field private static final TAG:Ljava/lang/String; = "EM/PrefsFragment"


# instance fields
.field private mXmlResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/engineermode/PrefsFragment;->FRAGMENT_RES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f04000c
        0x7f040001
        0x7f040003
        0x7f040005
        0x7f040006
        0x7f040009
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private isActivityAvailable(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isVoiceCapable()Z
    .locals 5

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isVoiceCapable()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "EM/PrefsFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sIsVoiceCapable : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWifiOnly()Z
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p2}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x4

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget-object v3, Lcom/mediatek/engineermode/PrefsFragment;->FRAGMENT_RES:[I

    iget v4, p0, Lcom/mediatek/engineermode/PrefsFragment;->mXmlResId:I

    aget v3, v3, v4

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const-string v3, "dualtalk_network_info"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "dualtalk_bandmode"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "digital_standard"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/engineermode/ModemCategory;->getModemType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    const-string v3, "rat_mode"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_0
    const-string v3, "dualtalk_network_select"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/nfc/dynamicload/NativeDynamicLoad;->queryVersion()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    const-string v3, "EM/PrefsFragment"

    const-string v4, "no nfc chip support"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "hqanfc"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "nfc"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :goto_0
    const-string v3, "ext_md_logger"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "cmmb"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v3

    if-eqz v3, :cond_e

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "fm_transmitter"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_1
    :goto_1
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v5}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    const-string v3, "location_basedservice"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_3
    invoke-static {v5}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "ygps"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_4
    const/4 v3, 0x5

    invoke-static {v3}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "matv"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_5
    const/4 v3, 0x6

    invoke-static {v3}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "bluetooth"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_6
    const/4 v3, 0x7

    invoke-static {v3}, Lcom/mediatek/engineermode/ChipSupport;->isFeatureSupported(I)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "wifi"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_7
    const-string v3, "tv_out"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/PrefsFragment;->isVoiceCapable()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-direct {p0}, Lcom/mediatek/engineermode/PrefsFragment;->isWifiOnly()Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_8
    const-string v3, "auto_answer"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "repeat_call_test"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "video_telephony"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_9
    invoke-direct {p0}, Lcom/mediatek/engineermode/PrefsFragment;->isWifiOnly()Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "GPRS"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "Modem"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "NetworkInfo"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "Baseband"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "SIMMeLock"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "BandMode"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "RAT Mode"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "SWLA"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "ModemTest"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_a
    const-string v3, "simme_lock1"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "EM/PrefsFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ChipSupport.getChip(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/engineermode/ChipSupport;->getChip()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x10

    invoke-static {}, Lcom/mediatek/engineermode/ChipSupport;->getChip()I

    move-result v4

    if-le v3, v4, :cond_f

    const-string v3, "de_sense"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "camera89"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :goto_2
    new-instance v0, Ljava/io/File;

    const-string v3, "/system/etc/system_update/address.xml"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "system_update"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_b
    const/16 v3, 0x20

    invoke-static {v3}, Lcom/mediatek/engineermode/ChipSupport;->isCurrentChipEquals(I)Z

    move-result v3

    if-eqz v3, :cond_10

    :goto_3
    const-string v3, "OP01"

    const-string v4, "ro.operator.optr"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "ConfigureCheck"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_c
    const-string v3, "cmas"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/PrefsFragment;->isActivityAvailable(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string v3, "cmas"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :cond_d
    return-void

    :pswitch_0
    const-string v3, "EM/PrefsFragment"

    const-string v4, "MSR3110 nfc chip, call nfc"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "hqanfc"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    const-string v3, "EM/PrefsFragment"

    const-string v4, "MT6606 nfc chip, call hqanfc"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "nfc"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    const-string v3, "fm_receiver"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "fm_transmitter"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_f
    const-string v3, "camera"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto :goto_2

    :cond_10
    const-string v3, "deep_idle"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "sleep_mode"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "dcm"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "pll_cg"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "cpu_dvfs"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string v3, "mcdi_setting"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/PrefsFragment;->removePreference(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onStart()V
    .locals 6

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStart()V

    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/PrefsFragment;->isActivityAvailable(Landroid/content/Intent;)Z

    move-result v5

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setResource(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/PrefsFragment;->mXmlResId:I

    return-void
.end method
