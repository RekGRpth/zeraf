.class public Lcom/mediatek/engineermode/tvout/TVOutEntry;
.super Landroid/preference/PreferenceActivity;
.source "TVOutEntry.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final CHINESE_TEST_PATT:Ljava/lang/String; = "chinese_test_patter"

.field private static final KEY_TV_SYSTEM:Ljava/lang/String; = "tv_system"

.field private static final TAG:Ljava/lang/String; = "EM/TVOut"

.field private static final TVOUT_ENABLE:Ljava/lang/String; = "tvout_en_disable"


# instance fields
.field private mChineseTestPatter:Landroid/preference/Preference;

.field private mHaveTvoutCheck:Landroid/preference/CheckBoxPreference;

.field private mTVSystem:Landroid/preference/ListPreference;

.field private mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EM/TVOut"

    const-string v1, "TVOutEntry->onCreate()."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f04000d

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    const-string v0, "tvout_en_disable"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mHaveTvoutCheck:Landroid/preference/CheckBoxPreference;

    const-string v0, "chinese_test_patter"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mChineseTestPatter:Landroid/preference/Preference;

    const-string v0, "tv_system"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTVSystem:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    if-nez v0, :cond_0

    const-class v0, Lcom/mediatek/common/tvout/ITVOUTNative;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/mediatek/common/MediatekClassFactory;->createInstance(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/common/tvout/ITVOUTNative;

    iput-object v0, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mHaveTvoutCheck:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mChineseTestPatter:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTVSystem:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "EM/TVOut"

    const-string v2, "TVOutEntry->onPreferenceChange()."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "tvout_en_disable"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "EM/TVOut"

    const-string v2, "TVOutEntry->checkbox disable."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/mediatek/common/tvout/ITVOUTNative;->enableTvOutManual(Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "tv_system"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "NTSC"

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :try_start_1
    iget-object v1, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/mediatek/common/tvout/ITVOUTNative;->setTvSystem(I)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    iget-object v1, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTVSystem:Landroid/preference/ListPreference;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_2
    const-string v1, "EM/TVOut"

    const-string v2, "TVOutEntry->checkbox enable."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    iget-object v1, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/mediatek/common/tvout/ITVOUTNative;->enableTvOutManual(Z)Z
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :cond_3
    :try_start_3
    iget-object v1, p0, Lcom/mediatek/engineermode/tvout/TVOutEntry;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/mediatek/common/tvout/ITVOUTNative;->setTvSystem(I)Z
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chinese_test_patter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EM/TVOut"

    const-string v1, "TVOutEntry->start TVOutActivity."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/tvout/TVOutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
