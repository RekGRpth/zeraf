.class public Lcom/mediatek/engineermode/baseband/Baseband;
.super Landroid/app/Activity;
.source "Baseband.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final MAX_VALUE:I = 0x400

.field private static final PARA_NUM:I = 0x4

.field private static final RADIX_LENGTH_SIXTEEN:I = 0x10

.field private static final RADIX_LENGTH_TEN:I = 0xa

.field private static final READ:I = 0x0

.field private static final TAG:Ljava/lang/String; = "EM-Baseband"

.field private static final WRITE:I = 0x1


# instance fields
.field private mBtnRead:Landroid/widget/Button;

.field private mBtnWrite:Landroid/widget/Button;

.field private mEditAddr:Landroid/widget/EditText;

.field private mEditLen:Landroid/widget/EditText;

.field private mEditVal:Landroid/widget/EditText;

.field private mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

.field private mInfo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private updateUI()V
    .locals 3

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    move-result-object v0

    const-string v1, ""

    iget-object v2, v0, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_0
    iget v1, v0, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mInfo:Landroid/widget/TextView;

    const v2, 0x7f08046c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mInfo:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, v0, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public checkValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const-wide/16 v1, 0x0

    const/16 v4, 0x10

    :try_start_0
    invoke-static {p1, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    const/16 v4, 0xa

    invoke-static {p2, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v1

    const/16 v4, 0x10

    invoke-static {p3, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-lez v4, :cond_0

    const-wide/16 v4, 0x400

    cmp-long v4, v1, v4

    if-gtz v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public functionCall(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    new-instance v1, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-direct {v1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const/16 v2, 0x2711

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->startCallFunctionStringReturn(I)Z

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamNo(I)Z

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const-string v2, "r"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v1, p2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v1, p3}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    :goto_1
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    const-string v2, "w"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mFmFunctionCallEx:Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-virtual {v1, p4}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamString(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const v8, 0x7f08046b

    const/4 v7, 0x1

    iget-object v5, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditAddr:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditLen:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditVal:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnRead:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v6

    if-ne v5, v6, :cond_3

    invoke-virtual {p0, v0, v1, v4}, Lcom/mediatek/engineermode/baseband/Baseband;->checkValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const v5, 0x7f080468

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v5, 0x0

    invoke-virtual {p0, v5, v0, v1, v4}, Lcom/mediatek/engineermode/baseband/Baseband;->functionCall(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p0, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/baseband/Baseband;->updateUI()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnWrite:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v6

    if-ne v5, v6, :cond_0

    invoke-virtual {p0, v0, v1, v4}, Lcom/mediatek/engineermode/baseband/Baseband;->checkValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const v5, 0x7f080469

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v7, v0, v1, v4}, Lcom/mediatek/engineermode/baseband/Baseband;->functionCall(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {p0, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/mediatek/engineermode/baseband/Baseband;->updateUI()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03000d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b005a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnRead:Landroid/widget/Button;

    const v0, 0x7f0b005b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnWrite:Landroid/widget/Button;

    const v0, 0x7f0b0056

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditAddr:Landroid/widget/EditText;

    const v0, 0x7f0b0059

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditLen:Landroid/widget/EditText;

    const v0, 0x7f0b0057

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mEditVal:Landroid/widget/EditText;

    const v0, 0x7f0b005c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mInfo:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnRead:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/baseband/Baseband;->mBtnWrite:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
