.class public Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;
.super Landroid/app/Activity;
.source "NfcSoftwareStackScan.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan$1;,
        Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan$OnClickListenerSpecial;
    }
.end annotation


# static fields
.field private static final CARDTYPE_MC1K:Ljava/lang/String; = "MIFARE_UL"

.field public static final CARDTYPE_MC1K_VAL:I = 0x1

.field private static final CARDTYPE_MC4K:Ljava/lang/String; = "MIFARE_STD"

.field public static final CARDTYPE_MC4K_VAL:I = 0x2

.field private static final CARDTYPE_NDEF:Ljava/lang/String; = "NDEF"

.field public static final CARDTYPE_NDEF_VAL:I = 0x9

.field public static final CARDTYPE_TAG_TYPE_DEFAULT_VAL:I = 0x0

.field private static final CARDTYPE_TAG_TYPE_FELICA:Ljava/lang/String; = "FELICA"

.field public static final CARDTYPE_TAG_TYPE_FELICA_VAL:I = 0x7

.field private static final CARDTYPE_TAG_TYPE_ISO1443_4A:Ljava/lang/String; = "ISO14443_4A"

.field public static final CARDTYPE_TAG_TYPE_ISO1443_4A_VAL:I = 0x3

.field private static final CARDTYPE_TAG_TYPE_ISO1443_4B:Ljava/lang/String; = "ISO14443_4B"

.field public static final CARDTYPE_TAG_TYPE_ISO1443_4B_VAL:I = 0x4

.field private static final CARDTYPE_TAG_TYPE_ISO15693:Ljava/lang/String; = "ISO15693"

.field public static final CARDTYPE_TAG_TYPE_ISO15693_VAL:I = 0x8

.field private static final CARDTYPE_TAG_TYPE_JEWWL:Ljava/lang/String; = "JEWEL"

.field public static final CARDTYPE_TAG_TYPE_JEWWL_VAL:I = 0x5

.field private static final CARDTYPE_TAG_TYPE_NFC:Ljava/lang/String; = "NFC"

.field public static final CARDTYPE_TAG_TYPE_NFC_VAL:I = 0x6

.field private static final FUNCTION_SUPPORT_NONE:I = 0x3

.field private static final FUNCTION_SUPPORT_RAW:I = 0x2

.field private static final FUNCTION_SUPPORT_RW:I = 0x1

.field public static final TAG:Ljava/lang/String; = "EM/nfc"

.field private static final TAGTYPE_NDEF:I = 0x2

.field private static final TAGTYPE_NORMAL:I = 0x1


# instance fields
.field private mBtnDisconnect:Landroid/widget/Button;

.field private mBtnFormatNDEF:Landroid/widget/Button;

.field private mBtnRawCmd:Landroid/widget/Button;

.field private mBtnRead:Landroid/widget/Button;

.field private mBtnWrite:Landroid/widget/Button;

.field private mChkNDEFTag:Landroid/widget/CheckBox;

.field private mChkNormalTag:Landroid/widget/CheckBox;

.field private mTextAppData:Landroid/widget/TextView;

.field private mTextAtqA:Landroid/widget/TextView;

.field private mTextCardType:Landroid/widget/TextView;

.field private mTextMaxDataRate:Landroid/widget/TextView;

.field private mTextSak:Landroid/widget/TextView;

.field private mTextUid:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private disconnectCard()V
    .locals 0

    return-void
.end method

.method private formatNdef()V
    .locals 0

    return-void
.end method

.method private initUI()V
    .locals 2

    const v1, 0x7f0b0341

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const v1, 0x7f0b0342

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextUid:Landroid/widget/TextView;

    const v1, 0x7f0b0343

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextSak:Landroid/widget/TextView;

    const v1, 0x7f0b0344

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextAtqA:Landroid/widget/TextView;

    const v1, 0x7f0b0345

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextAppData:Landroid/widget/TextView;

    const v1, 0x7f0b0346

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextMaxDataRate:Landroid/widget/TextView;

    const v1, 0x7f0b0347

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRead:Landroid/widget/Button;

    const v1, 0x7f0b0348

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnWrite:Landroid/widget/Button;

    const v1, 0x7f0b034a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRawCmd:Landroid/widget/Button;

    const v1, 0x7f0b0349

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnDisconnect:Landroid/widget/Button;

    const v1, 0x7f0b034b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnFormatNDEF:Landroid/widget/Button;

    const v1, 0x7f0b033f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mChkNormalTag:Landroid/widget/CheckBox;

    const v1, 0x7f0b0340

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mChkNDEFTag:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRead:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnWrite:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRawCmd:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnDisconnect:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnFormatNDEF:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan$OnClickListenerSpecial;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan$OnClickListenerSpecial;-><init>(Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan$1;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mChkNormalTag:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mChkNDEFTag:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private readTagInfo()V
    .locals 7

    const/4 v3, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    invoke-static {}, Lcom/mediatek/engineermode/nfc/NfcRespMap;->getInst()Lcom/mediatek/engineermode/nfc/NfcRespMap;

    move-result-object v1

    const-string v2, "nfc.software_stack.normaltag_dect"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/nfc/NfcRespMap;->take(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;

    if-nez v0, :cond_0

    const-string v1, "EM/nfc"

    const-string v2, "Take NfcRespMap.KEY_SS_TAG_DECT is null"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->printMember()V

    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->tag_type:I

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mChkNormalTag:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mChkNDEFTag:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_1
    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->card_type:I

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Garbarge: card_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->card_type:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    :goto_2
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextUid:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->uid:[B

    invoke-static {v2}, Lcom/mediatek/engineermode/nfc/NfcUtils;->printArray(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextSak:Landroid/widget/TextView;

    iget v2, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->sak:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextAtqA:Landroid/widget/TextView;

    iget v2, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->atag:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextAppData:Landroid/widget/TextView;

    iget v2, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->appdata:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextMaxDataRate:Landroid/widget/TextView;

    iget v2, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->maxdatarate:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->tag_type:I

    if-ne v1, v5, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mChkNormalTag:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mChkNDEFTag:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_2
    const-string v1, "EM/nfc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Garbarge: tag_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->tag_type:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "MIFARE_UL"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "MIFARE_STD"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "NDEF"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnFormatNDEF:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_2

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "ISO14443_4A"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    goto/16 :goto_2

    :pswitch_4
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "ISO14443_4B"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    goto/16 :goto_2

    :pswitch_5
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "JEWEL"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    goto/16 :goto_2

    :pswitch_6
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "FELICA"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    goto/16 :goto_2

    :pswitch_7
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "NFC"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    goto/16 :goto_2

    :pswitch_8
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mTextCardType:Landroid/widget/TextView;

    const-string v2, "ISO15693"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->setBtnSupportState(I)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_2
    .end packed-switch
.end method

.method private setBtnSupportState(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 p1, 0x3

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRead:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnWrite:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRawCmd:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnDisconnect:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnFormatNDEF:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRead:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnWrite:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRawCmd:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnDisconnect:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnFormatNDEF:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRead:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnWrite:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRawCmd:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnDisconnect:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnFormatNDEF:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const-string v1, "EM/nfc"

    const-string v2, "NfcSoftwareStackScan onClick"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRead:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.mediatek.engineermode.nfc.NfcSoftwareStackScanReadTag"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnWrite:Landroid/widget/Button;

    if-ne p1, v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.mediatek.engineermode.nfc.NfcSoftwareStackScanWriteTag"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnRawCmd:Landroid/widget/Button;

    if-ne p1, v1, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.mediatek.engineermode.nfc.NfcSoftwareStackRawCommand"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnDisconnect:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_4

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->disconnectCard()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->mBtnFormatNDEF:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_5

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->formatNdef()V

    goto :goto_0

    :cond_5
    const-string v1, "EM/nfc"

    const-string v2, "ghost button?"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v0, 0x7f030062

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    const-string v0, "EM/nfc"

    const-string v1, "NfcSoftwareStackScan onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->initUI()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->disconnectCard()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScan;->readTagInfo()V

    return-void
.end method
