.class public Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;
.super Landroid/app/Activity;
.source "NfcSoftwareStackP2PInitDect.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "EM/nfc"


# instance fields
.field private mBtnRecv:Landroid/widget/Button;

.field private mBtnSend:Landroid/widget/Button;

.field private mEditRecv:Landroid/widget/EditText;

.field private mEditSend:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mBtnSend:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mBtnRecv:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mEditSend:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mEditRecv:Landroid/widget/EditText;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mBtnSend:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    const-string v0, "EM/nfc"

    const-string v1, "NfcSoftwareStackP2PInitDect mBtnSend"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mBtnRecv:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    const-string v0, "EM/nfc"

    const-string v1, "NfcSoftwareStackP2PInitDect mBtnRecv"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030060

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b033a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mBtnSend:Landroid/widget/Button;

    const v0, 0x7f0b033b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mBtnRecv:Landroid/widget/Button;

    const v0, 0x7f0b0339

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mEditSend:Landroid/widget/EditText;

    const v0, 0x7f0b0338

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mEditRecv:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mBtnSend:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mBtnRecv:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mEditRecv:Landroid/widget/EditText;

    const-string v1, "ddd\nddd\nddd\nddd\nddd\nddd\nddd\nddd\nddd\n\nddd\nddd\nddd\nddd\n"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackP2PInitDect;->mEditSend:Landroid/widget/EditText;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
