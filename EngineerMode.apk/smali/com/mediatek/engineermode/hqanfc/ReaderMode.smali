.class public Lcom/mediatek/engineermode/hqanfc/ReaderMode;
.super Landroid/app/Activity;
.source "ReaderMode.java"


# static fields
.field private static final CHECKBOXS_NUMBER:I = 0x12

.field private static final CHECKBOX_READER_KOVIO:I = 0x11

.field private static final CHECKBOX_READER_TYPEA:I = 0x0

.field private static final CHECKBOX_READER_TYPEA_106:I = 0x1

.field private static final CHECKBOX_READER_TYPEA_212:I = 0x2

.field private static final CHECKBOX_READER_TYPEA_424:I = 0x3

.field private static final CHECKBOX_READER_TYPEA_848:I = 0x4

.field private static final CHECKBOX_READER_TYPEB:I = 0x5

.field private static final CHECKBOX_READER_TYPEB2:I = 0x10

.field private static final CHECKBOX_READER_TYPEB_106:I = 0x6

.field private static final CHECKBOX_READER_TYPEB_212:I = 0x7

.field private static final CHECKBOX_READER_TYPEB_424:I = 0x8

.field private static final CHECKBOX_READER_TYPEB_848:I = 0x9

.field private static final CHECKBOX_READER_TYPEF:I = 0xa

.field private static final CHECKBOX_READER_TYPEF_212:I = 0xb

.field private static final CHECKBOX_READER_TYPEF_424:I = 0xc

.field private static final CHECKBOX_READER_TYPEV:I = 0xd

.field private static final CHECKBOX_READER_TYPEV_166:I = 0xe

.field private static final CHECKBOX_READER_TYPEV_2648:I = 0xf

.field private static final DIALOG_ID_WAIT:I = 0x0

.field private static final HANDLER_MSG_GET_NTF:I = 0x64

.field private static final HANDLER_MSG_GET_RSP:I = 0xc8

.field protected static final KEY_READER_MODE_RSP_ARRAY:Ljava/lang/String; = "reader_mode_rsp_array"

.field protected static final KEY_READER_MODE_RSP_NDEF:Ljava/lang/String; = "reader_mode_rsp_ndef"


# instance fields
.field private mBtnClearAll:Landroid/widget/Button;

.field private mBtnReturn:Landroid/widget/Button;

.field private mBtnRunInBack:Landroid/widget/Button;

.field private mBtnSelectAll:Landroid/widget/Button;

.field private mBtnStart:Landroid/widget/Button;

.field private final mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mEnableBackKey:Z

.field private final mHandler:Landroid/os/Handler;

.field private mRbTypeVDualSubcarrier:Landroid/widget/RadioButton;

.field private mRbTypeVSubcarrier:Landroid/widget/RadioButton;

.field private mReadermNtf:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mResponse:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermRsp;

.field private mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

.field private mRspArray:[B

.field private mSettingsCkBoxs:[Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x12

    new-array v0, v0, [Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mEnableBackKey:Z

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/ReaderMode$1;-><init>(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;-><init>(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$3;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/ReaderMode$3;-><init>(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$4;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/ReaderMode$4;-><init>(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)[B
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRspArray:[B

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/hqanfc/ReaderMode;[B)[B
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;
    .param p1    # [B

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRspArray:[B

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermRsp;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mResponse:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermRsp;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnClearAll:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/hqanfc/ReaderMode;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermRsp;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermRsp;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;
    .param p1    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermRsp;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mResponse:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermRsp;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnReturn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnRunInBack:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mReadermNtf:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/hqanfc/ReaderMode;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;
    .param p1    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mReadermNtf:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    return-object p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/hqanfc/ReaderMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->setButtonsStatus(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)[Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/hqanfc/ReaderMode;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->doTestAction(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnSelectAll:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/hqanfc/ReaderMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/ReaderMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->changeAllSelect(Z)V

    return-void
.end method

.method private changeAllSelect(Z)V
    .locals 4
    .param p1    # Z

    const-string v1, "EM/HQA/NFC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[ReaderMode]changeDisplay status is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

    const v2, 0x7f0b024c

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

    const v2, 0x7f0b024b

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->check(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRbTypeVDualSubcarrier:Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRbTypeVSubcarrier:Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1
.end method

.method private doTestAction(Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->sendCommand(Ljava/lang/Boolean;)V

    return-void
.end method

.method private fillRequest(Ljava/lang/Boolean;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;)V
    .locals 13
    .param p1    # Ljava/lang/Boolean;
    .param p2    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_0

    iput v10, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mAction:I

    :goto_0
    const/4 v7, 0x6

    new-array v2, v7, [Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v7, v7, v5

    aput-object v7, v2, v5

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    aput-object v7, v2, v6

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    aput-object v7, v2, v10

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0xd

    aget-object v7, v7, v8

    aput-object v7, v2, v11

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0x10

    aget-object v7, v7, v8

    aput-object v7, v2, v12

    const/4 v7, 0x5

    iget-object v8, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v9, 0x11

    aget-object v8, v8, v9

    aput-object v8, v2, v7

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeValue([Landroid/widget/CheckBox;)I

    move-result v7

    iput v7, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mSupportType:I

    new-array v0, v12, [Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v7, v7, v6

    aput-object v7, v0, v5

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v7, v7, v10

    aput-object v7, v0, v6

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v7, v7, v11

    aput-object v7, v0, v10

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v7, v7, v12

    aput-object v7, v0, v11

    invoke-static {v0}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeAbDataRateValue([Landroid/widget/CheckBox;)I

    move-result v7

    iput v7, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeADataRate:I

    new-array v1, v12, [Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    aput-object v7, v1, v5

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v8, 0x7

    aget-object v7, v7, v8

    aput-object v7, v1, v6

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0x8

    aget-object v7, v7, v8

    aput-object v7, v1, v10

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    aput-object v7, v1, v11

    invoke-static {v1}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeAbDataRateValue([Landroid/widget/CheckBox;)I

    move-result v7

    iput v7, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeBDataRate:I

    new-array v3, v10, [Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0xb

    aget-object v7, v7, v8

    aput-object v7, v3, v5

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0xc

    aget-object v7, v7, v8

    aput-object v7, v3, v6

    invoke-static {v3}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeFDataRateValue([Landroid/widget/CheckBox;)I

    move-result v7

    iput v7, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeFDataRate:I

    new-array v4, v10, [Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0xe

    aget-object v7, v7, v8

    aput-object v7, v4, v5

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v8, 0xf

    aget-object v7, v7, v8

    aput-object v7, v4, v6

    invoke-static {v4}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeVDataRateValue([Landroid/widget/CheckBox;)I

    move-result v7

    iput v7, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeVDataRate:I

    iget-object v7, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRbTypeVSubcarrier:Landroid/widget/RadioButton;

    invoke-virtual {v7}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_2

    :goto_1
    iput v5, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeVSubcarrier:I

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_1

    iput v5, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mAction:I

    goto/16 :goto_0

    :cond_1
    iput v6, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mAction:I

    goto/16 :goto_0

    :cond_2
    move v5, v6

    goto :goto_1
.end method

.method private initComponents()V
    .locals 8

    const v7, 0x7f0b024b

    const/16 v6, 0xd

    const/16 v5, 0xa

    const/4 v4, 0x5

    const/4 v3, 0x0

    const-string v0, "EM/HQA/NFC"

    const-string v1, "[ReaderMode]initComponents"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b023c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v3

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v3

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x1

    const v0, 0x7f0b023d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x2

    const v0, 0x7f0b023e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x3

    const v0, 0x7f0b023f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x4

    const v0, 0x7f0b0240

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b0241

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v4

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x6

    const v0, 0x7f0b0242

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x7

    const v0, 0x7f0b0243

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x8

    const v0, 0x7f0b0244

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x9

    const v0, 0x7f0b0245

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b0246

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v5

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v5

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xb

    const v0, 0x7f0b0247

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xc

    const v0, 0x7f0b0248

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b0249

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v6

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xe

    const v0, 0x7f0b024d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xf

    const v0, 0x7f0b024e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    const v0, 0x7f0b024a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRbTypeVSubcarrier:Landroid/widget/RadioButton;

    const v0, 0x7f0b024c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRbTypeVDualSubcarrier:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x10

    const v0, 0x7f0b024f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x11

    const v0, 0x7f0b0250

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    const v0, 0x7f0b0251

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnSelectAll:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnSelectAll:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0252

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnClearAll:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnClearAll:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0253

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnStart:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnStart:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0254

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnReturn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnReturn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0255

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnRunInBack:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnRunInBack:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v7}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnRunInBack:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method private sendCommand(Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    invoke-direct {v0}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->fillRequest(Ljava/lang/Boolean;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;)V

    invoke-static {}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->getInstance()Lcom/mediatek/engineermode/hqanfc/NfcClient;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v2, v0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sendCommand(ILcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;)I

    return-void
.end method

.method private setButtonsStatus(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08048e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnRunInBack:Landroid/widget/Button;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iput-boolean p1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mEnableBackKey:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnReturn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnSelectAll:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnClearAll:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08048f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mEnableBackKey:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030042

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->initComponents()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->changeAllSelect(Z)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0804ba

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.mediatek.hqanfc.102"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mediatek.hqanfc.118"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
