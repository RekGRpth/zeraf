.class Lcom/mediatek/engineermode/hqanfc/RwFunction$1;
.super Landroid/content/BroadcastReceiver;
.source "RwFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/hqanfc/RwFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/hqanfc/RwFunction;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v2, "EM/HQA/NFC"

    const-string v3, "[RwFunction]mReceiver onReceive"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "content"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$002(Lcom/mediatek/engineermode/hqanfc/RwFunction;[B)[B

    const-string v2, "com.mediatek.hqanfc.104"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$000(Lcom/mediatek/engineermode/hqanfc/RwFunction;)[B

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$000(Lcom/mediatek/engineermode/hqanfc/RwFunction;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    new-instance v3, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;

    invoke-direct {v3}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;-><init>()V

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$102(Lcom/mediatek/engineermode/hqanfc/RwFunction;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$100(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$200(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "com.mediatek.hqanfc.118"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$000(Lcom/mediatek/engineermode/hqanfc/RwFunction;)[B

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$000(Lcom/mediatek/engineermode/hqanfc/RwFunction;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    new-instance v3, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    invoke-direct {v3}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;-><init>()V

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$302(Lcom/mediatek/engineermode/hqanfc/RwFunction;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$300(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->readRaw(Ljava/nio/ByteBuffer;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$1;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$200(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    const-string v2, "EM/HQA/NFC"

    const-string v3, "[RwFunction]Other response"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
