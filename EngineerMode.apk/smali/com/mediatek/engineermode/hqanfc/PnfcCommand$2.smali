.class Lcom/mediatek/engineermode/hqanfc/PnfcCommand$2;
.super Landroid/os/Handler;
.source "PnfcCommand.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/hqanfc/PnfcCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/hqanfc/PnfcCommand;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/hqanfc/PnfcCommand;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/PnfcCommand$2;->this$0:Lcom/mediatek/engineermode/hqanfc/PnfcCommand;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const/16 v1, 0xc8

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PnfcCommand$2;->this$0:Lcom/mediatek/engineermode/hqanfc/PnfcCommand;

    invoke-virtual {v1, v3}, Landroid/app/Activity;->dismissDialog(I)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PnfcCommand$2;->this$0:Lcom/mediatek/engineermode/hqanfc/PnfcCommand;

    invoke-static {v1}, Lcom/mediatek/engineermode/hqanfc/PnfcCommand;->access$100(Lcom/mediatek/engineermode/hqanfc/PnfcCommand;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPnfcRsp;

    move-result-object v1

    iget v1, v1, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmRsp;->mResult:I

    packed-switch v1, :pswitch_data_0

    const-string v0, "PnfcCommand Rsp Result: ERROR"

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PnfcCommand$2;->this$0:Lcom/mediatek/engineermode/hqanfc/PnfcCommand;

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void

    :pswitch_0
    const-string v0, "PnfcCommand Rsp Result: SUCCESS"

    goto :goto_0

    :pswitch_1
    const-string v0, "PnfcCommand Rsp Result: FAIL"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
