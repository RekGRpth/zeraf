.class Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;
.super Landroid/os/Handler;
.source "ReaderMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/hqanfc/ReaderMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const/4 v1, 0x0

    const/16 v2, 0xc8

    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->dismissDialog(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$100(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermRsp;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmRsp;->mResult:I

    sparse-switch v2, :sswitch_data_0

    const-string v1, "ReaderMode Rsp Result: ERROR"

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    return-void

    :sswitch_0
    const-string v1, "ReaderMode Rsp Result: SUCCESS"

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$400(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    const v4, 0x7f08048e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2, v5}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$500(Lcom/mediatek/engineermode/hqanfc/ReaderMode;Z)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2, v6}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$500(Lcom/mediatek/engineermode/hqanfc/ReaderMode;Z)V

    goto :goto_0

    :sswitch_1
    const-string v1, "ReaderMode Rsp Result: FAIL"

    goto :goto_0

    :sswitch_2
    const-string v1, "ReaderMode Rsp Result: INVALID_FORMAT"

    goto :goto_0

    :sswitch_3
    const-string v1, "ReaderMode Rsp Result: INVALID_NDEF_FORMAT"

    goto :goto_0

    :sswitch_4
    const-string v1, "ReaderMode Rsp Result: NDEF_EOF_REACHED"

    goto :goto_0

    :sswitch_5
    const-string v1, "ReaderMode Rsp Result: NOT_SUPPORT"

    goto :goto_0

    :cond_2
    const/16 v2, 0x64

    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$300(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mResult:I

    packed-switch v2, :pswitch_data_0

    const-string v1, "ReaderMode Ntf Result: ERROR"

    goto :goto_0

    :pswitch_0
    const-string v1, "ReaderMode Ntf Result: CONNECT"

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$300(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mIsNdef:I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$300(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mIsNdef:I

    if-eq v2, v6, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$300(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mIsNdef:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "reader_mode_rsp_array"

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v3}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$000(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v2, "reader_mode_rsp_ndef"

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-static {v3}, Lcom/mediatek/engineermode/hqanfc/ReaderMode;->access$300(Lcom/mediatek/engineermode/hqanfc/ReaderMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v3

    iget v3, v3, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mIsNdef:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    const-class v3, Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/ReaderMode$2;->this$0:Lcom/mediatek/engineermode/hqanfc/ReaderMode;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_1
    const-string v1, "ReaderMode Ntf Result: DISCONNECT"

    goto/16 :goto_0

    :pswitch_2
    const-string v1, "ReaderMode Ntf Result: FAIL"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0xa -> :sswitch_5
        0x20 -> :sswitch_3
        0x21 -> :sswitch_2
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
