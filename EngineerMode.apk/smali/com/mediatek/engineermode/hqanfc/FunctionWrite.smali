.class public Lcom/mediatek/engineermode/hqanfc/FunctionWrite;
.super Landroid/app/Activity;
.source "FunctionWrite.java"


# static fields
.field protected static final HANDLER_MSG_GET_RSP:I = 0xc8


# instance fields
.field private mBtnCancel:Landroid/widget/Button;

.field private mBtnWrite:Landroid/widget/Button;

.field private final mCheckedChangeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mEtCompany:Landroid/widget/EditText;

.field private mEtText:Landroid/widget/EditText;

.field private mEtUrl:Landroid/widget/EditText;

.field private final mHandler:Landroid/os/Handler;

.field private mOptRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;

.field private mRbTypeOther:Landroid/widget/RadioButton;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRgTagType:Landroid/widget/RadioGroup;

.field private mRspArray:[B

.field private mSpLang:Landroid/widget/Spinner;

.field private mTvCompany:Landroid/widget/TextView;

.field private mTvText:Landroid/widget/TextView;

.field private mTvUrl:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/FunctionWrite$1;-><init>(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite$2;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/FunctionWrite$2;-><init>(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite$3;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/FunctionWrite$3;-><init>(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite$4;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/FunctionWrite$4;-><init>(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mCheckedChangeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)[B
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/FunctionWrite;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRspArray:[B

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;[B)[B
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/FunctionWrite;
    .param p1    # [B

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRspArray:[B

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/FunctionWrite;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mOptRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/FunctionWrite;
    .param p1    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mOptRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/FunctionWrite;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/FunctionWrite;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mBtnWrite:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/FunctionWrite;

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->doWrite()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/hqanfc/FunctionWrite;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/FunctionWrite;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mBtnCancel:Landroid/widget/Button;

    return-object v0
.end method

.method private checkInput()Z
    .locals 2

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRgTagType:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private doWrite()V
    .locals 15

    const/4 v14, 0x0

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->checkInput()Z

    move-result v12

    if-eqz v12, :cond_0

    new-instance v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;

    invoke-direct {v5}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;-><init>()V

    const/4 v12, 0x1

    iput v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mAction:I

    const/4 v7, -0x1

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRgTagType:Landroid/widget/RadioGroup;

    invoke-virtual {v12}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v12

    packed-switch v12, :pswitch_data_0

    const/4 v7, 0x3

    :goto_0
    iget-object v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mTagWriteNdef:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;

    iget-object v12, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;->mNdefType:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcNdefType;

    iput v7, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcNdefType;->mEnumValue:I

    iget-object v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mTagWriteNdef:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;

    iget-object v12, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;->mNdefLangType:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcNdefLangType;

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mSpLang:Landroid/widget/Spinner;

    invoke-virtual {v13}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v13

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcNdefLangType;->mEnumValue:I

    packed-switch v7, :pswitch_data_1

    :goto_1
    :pswitch_0
    invoke-static {}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->getInstance()Lcom/mediatek/engineermode/hqanfc/NfcClient;

    move-result-object v12

    const/16 v13, 0x67

    invoke-virtual {v12, v13, v5}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sendCommand(ILcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;)I

    :goto_2
    return-void

    :pswitch_1
    const/4 v7, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v7, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v7, 0x2

    goto :goto_0

    :pswitch_4
    new-instance v10, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$UrlT;

    invoke-direct {v10}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$UrlT;-><init>()V

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    iget-object v12, v10, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$UrlT;->mUrlData:[B

    array-length v13, v11

    invoke-static {v11, v14, v12, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v12, v10, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$UrlT;->mUrlData:[B

    array-length v12, v12

    int-to-short v12, v12

    iput-short v12, v10, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$UrlT;->mUrlDataLength:S

    invoke-virtual {v10}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$UrlT;->getByteArray()[B

    move-result-object v2

    iget-object v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mTagWriteNdef:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;

    iget-object v12, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;->mNdefData:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdefData;

    iget-object v12, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdefData;->mData:[B

    array-length v13, v2

    invoke-static {v2, v14, v12, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mTagWriteNdef:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;

    array-length v13, v2

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;->mLength:I

    goto :goto_1

    :pswitch_5
    new-instance v8, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$TextT;

    invoke-direct {v8}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$TextT;-><init>()V

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtText:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    iget-object v12, v8, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$TextT;->mData:[B

    array-length v13, v9

    invoke-static {v9, v14, v12, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v12, v9

    int-to-short v12, v12

    iput-short v12, v8, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$TextT;->mDataLength:S

    invoke-virtual {v8}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$TextT;->getByteArray()[B

    move-result-object v1

    iget-object v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mTagWriteNdef:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;

    iget-object v12, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;->mNdefData:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdefData;

    iget-object v12, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdefData;->mData:[B

    array-length v13, v1

    invoke-static {v1, v14, v12, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mTagWriteNdef:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;

    array-length v13, v1

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;->mLength:I

    goto :goto_1

    :pswitch_6
    new-instance v6, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$SmartPosterT;

    invoke-direct {v6}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$SmartPosterT;-><init>()V

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtCompany:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    iget-object v12, v6, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$SmartPosterT;->mCompany:[B

    array-length v13, v3

    invoke-static {v3, v14, v12, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v12, v3

    int-to-short v12, v12

    iput-short v12, v6, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$SmartPosterT;->mCompanyLength:S

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    iget-object v12, v6, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$SmartPosterT;->mCompanyUrl:[B

    array-length v13, v4

    invoke-static {v4, v14, v12, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v12, v4

    int-to-short v12, v12

    iput-short v12, v6, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$SmartPosterT;->mCompanyUrlLength:S

    invoke-virtual {v6}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$SmartPosterT;->getByteArray()[B

    move-result-object v0

    iget-object v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mTagWriteNdef:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;

    iget-object v12, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;->mNdefData:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdefData;

    iget-object v12, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdefData;->mData:[B

    array-length v13, v0

    invoke-static {v0, v14, v12, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v12, v5, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptReq;->mTagWriteNdef:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;

    array-length v13, v0

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcTagWriteNdef;->mLength:I

    goto/16 :goto_1

    :cond_0
    const-string v12, "Input error"

    invoke-static {p0, v12, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x7f0b01e4
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method

.method private initComponents()V
    .locals 2

    const v0, 0x7f0b01e3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRgTagType:Landroid/widget/RadioGroup;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRgTagType:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mCheckedChangeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    const v0, 0x7f0b01e7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRbTypeOther:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRbTypeOther:Landroid/widget/RadioButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0b01e8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mSpLang:Landroid/widget/Spinner;

    const v0, 0x7f0b01ea

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtCompany:Landroid/widget/EditText;

    const v0, 0x7f0b01ec

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    const v0, 0x7f0b01ee

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtText:Landroid/widget/EditText;

    const v0, 0x7f0b01ef

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mBtnWrite:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mBtnWrite:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b01f0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mBtnCancel:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mBtnCancel:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b01e9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvCompany:Landroid/widget/TextView;

    const v0, 0x7f0b01eb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvUrl:Landroid/widget/TextView;

    const v0, 0x7f0b01ed

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mRgTagType:Landroid/widget/RadioGroup;

    const v1, 0x7f0b01e4

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    return-void
.end method


# virtual methods
.method protected checkedChange(I)V
    .locals 4
    .param p1    # I

    const/16 v3, 0x8

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mSpLang:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtCompany:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvCompany:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvUrl:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mSpLang:Landroid/widget/Spinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtCompany:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvCompany:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvUrl:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mSpLang:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtCompany:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvCompany:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mTvUrl:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtUrl:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtCompany:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtCompany:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mEtCompany:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b01e4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03003e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->initComponents()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.mediatek.hqanfc.104"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/FunctionWrite;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
