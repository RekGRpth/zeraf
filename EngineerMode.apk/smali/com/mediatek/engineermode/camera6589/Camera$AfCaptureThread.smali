.class Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;
.super Ljava/lang/Thread;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera6589/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AfCaptureThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/Camera;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/16 v5, 0x66

    const/4 v4, 0x0

    const/16 v3, 0x6a

    const-string v1, "test/camera"

    const-string v2, "AfCaptureThread start."

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v4}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3302(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3400(Lcom/mediatek/engineermode/camera6589/Camera;)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3300(Lcom/mediatek/engineermode/camera6589/Camera;)Z

    move-result v1

    if-nez v1, :cond_0

    const-wide/16 v1, 0x64

    :try_start_0
    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "test/camera"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3500(Lcom/mediatek/engineermode/camera6589/Camera;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    const-string v1, "test/camera"

    const-string v2, "AfCaptureThread finish."

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1700(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3600(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1800(Lcom/mediatek/engineermode/camera6589/Camera;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v4}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v4}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1702(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
