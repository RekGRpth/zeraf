.class Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;
.super Ljava/lang/Object;
.source "AutoCalibration.java"

# interfaces
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/engineermode/camera6589/AutoCalibration;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;IZ)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Z

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    aput-boolean p3, v1, p2

    if-nez p2, :cond_1

    if-eqz p3, :cond_3

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    aput-boolean v3, v1, v0

    move-object v1, p1

    check-cast v1, Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    aput-boolean v2, v1, v0

    move-object v1, p1

    check-cast v1, Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_1

    :cond_1
    if-ne p2, v3, :cond_2

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    aput-boolean v2, v1, v2

    move-object v1, p1

    check-cast v1, Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    const/4 v0, 0x2

    :goto_2
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    aput-boolean v3, v1, v0

    move-object v1, p1

    check-cast v1, Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    if-eqz p3, :cond_4

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    aput-boolean v2, v1, v2

    check-cast p1, Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    :cond_3
    :goto_3
    return-void

    :cond_4
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z

    move-result-object v1

    aput-boolean v2, v1, v3

    check-cast p1, Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v3, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_3
.end method
