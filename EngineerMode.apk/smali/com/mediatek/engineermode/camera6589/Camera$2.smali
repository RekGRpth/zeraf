.class Lcom/mediatek/engineermode/camera6589/Camera$2;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera6589/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/Camera;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera$2;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x1

    :try_start_0
    const-string v1, "test/camera"

    const-string v2, "mCameraOpenThread start"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "client.appmode"

    const-string v2, "MtkEng"

    invoke-static {v1, v2}, Landroid/hardware/Camera;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$2;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$2;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$2;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$4700(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/camera6589/Util;->openCamera(Landroid/app/Activity;I)Landroid/hardware/Camera;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;
    :try_end_0
    .catch Lcom/mediatek/engineermode/camera6589/CameraHardwareException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mediatek/engineermode/camera6589/CameraDisabledException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$2;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v4}, Lcom/mediatek/engineermode/camera6589/Camera;->access$4802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$2;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v4}, Lcom/mediatek/engineermode/camera6589/Camera;->access$4902(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    goto :goto_0
.end method
