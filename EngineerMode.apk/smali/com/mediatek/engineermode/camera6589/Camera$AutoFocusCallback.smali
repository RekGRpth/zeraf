.class final Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera6589/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AutoFocusCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/Camera;


# direct methods
.method private constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p2    # Lcom/mediatek/engineermode/camera6589/Camera$1;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;)V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 6
    .param p1    # Z
    .param p2    # Landroid/hardware/Camera;

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1000(Lcom/mediatek/engineermode/camera6589/Camera;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$4500(Lcom/mediatek/engineermode/camera6589/Camera;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$4402(Lcom/mediatek/engineermode/camera6589/Camera;J)J

    const-string v0, "test/camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAutoFocusTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$4400(Lcom/mediatek/engineermode/camera6589/Camera;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", camera State = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1100(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1100(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v0, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1200(Lcom/mediatek/engineermode/camera6589/Camera;I)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v0, v0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->doSnap()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v0, v0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v0, p1}, Lcom/mediatek/engineermode/camera6589/FocusManager;->onAutoFocus(Z)V

    invoke-static {v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$4602(Z)Z

    goto :goto_0
.end method
