.class Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;
.super Landroid/os/Handler;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera6589/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MainHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/Camera;


# direct methods
.method private constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p2    # Lcom/mediatek/engineermode/camera6589/Camera$1;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1    # Landroid/os/Message;

    const/4 v10, 0x3

    const-wide/16 v8, 0x64

    const/16 v7, 0x66

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$500(Lcom/mediatek/engineermode/camera6589/Camera;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$600(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$700(Lcom/mediatek/engineermode/camera6589/Camera;)V

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$800(Lcom/mediatek/engineermode/camera6589/Camera;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x1388

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$900(Lcom/mediatek/engineermode/camera6589/Camera;)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1000(Lcom/mediatek/engineermode/camera6589/Camera;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1100(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v6}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1200(Lcom/mediatek/engineermode/camera6589/Camera;I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$900(Lcom/mediatek/engineermode/camera6589/Camera;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/camera6589/FocusManager;->capture()V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-virtual {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->onShutterButtonFocus(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1300(Lcom/mediatek/engineermode/camera6589/Camera;)V

    goto :goto_0

    :sswitch_5
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1400(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1602(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1702(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v6}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1300(Lcom/mediatek/engineermode/camera6589/Camera;)V

    const-string v1, "test/camera"

    const-string v2, "Enabled mCaptureBtn"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1400(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    const-string v1, "test/camera"

    const-string v2, "Disabled mCaptureBtn"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "test/camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EVENT_CAPTURE_ACTION af mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1100(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-eq v1, v10, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1000(Lcom/mediatek/engineermode/camera6589/Camera;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const-string v1, "test/camera"

    const-string v2, "EVENT_CAPTURE_ACTION return1"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2000(Lcom/mediatek/engineermode/camera6589/Camera;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    const-string v2, "storage space is not enough or unavailable"

    invoke-static {v1, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, v2, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2102(Lcom/mediatek/engineermode/camera6589/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-ne v1, v6, :cond_9

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-ne v1, v6, :cond_5

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1702(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v6}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getBestFocusStep()I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2202(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMaxFocusStep()I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2302(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMinFocusStep()I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2402(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    const-string v1, "test/camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bracket max "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bracket min "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bracket best "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2200(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1708(Lcom/mediatek/engineermode/camera6589/Camera;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1800(Lcom/mediatek/engineermode/camera6589/Camera;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2200(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2600(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v4}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1700(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v4

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-le v1, v2, :cond_7

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1702(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1508(Lcom/mediatek/engineermode/camera6589/Camera;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6a

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2200(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2600(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v4}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1700(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v4

    mul-int/2addr v3, v4

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-ge v1, v2, :cond_7

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_7
    const-string v1, "test/camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bracket position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setFocusEngStep(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    const-wide/16 v1, 0x64

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    :goto_1
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1508(Lcom/mediatek/engineermode/camera6589/Camera;)I

    :cond_9
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-ne v1, v10, :cond_b

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1702(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMaxFocusStep()I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2302(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMinFocusStep()I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2402(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    const-string v1, "test/camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "through max "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " through min "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2700(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    :cond_a
    :goto_2
    const-string v1, "test/camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "through position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setFocusEngStep(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    const-wide/16 v1, 0x64

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1508(Lcom/mediatek/engineermode/camera6589/Camera;)I

    :cond_b
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3200(Lcom/mediatek/engineermode/camera6589/Camera;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_1

    :cond_c
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2700(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-ne v1, v6, :cond_d

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v6}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    goto :goto_2

    :cond_d
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2800(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-ge v1, v2, :cond_10

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v6}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2800(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-lt v1, v2, :cond_e

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2800(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    :goto_4
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-gt v1, v2, :cond_f

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2302(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    goto/16 :goto_2

    :cond_e
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    const v2, 0x7f0804de

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3000(Lcom/mediatek/engineermode/camera6589/Camera;II)V

    goto :goto_4

    :cond_f
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    const v2, 0x7f0804df

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3000(Lcom/mediatek/engineermode/camera6589/Camera;II)V

    goto/16 :goto_2

    :cond_10
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2800(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-le v1, v2, :cond_a

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v5}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2800(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-gt v1, v2, :cond_11

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2800(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    :goto_5
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-lt v1, v2, :cond_12

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2900(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2402(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    goto/16 :goto_2

    :cond_11
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    const v2, 0x7f0804de

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3000(Lcom/mediatek/engineermode/camera6589/Camera;II)V

    goto :goto_5

    :cond_12
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    const v2, 0x7f0804df

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3000(Lcom/mediatek/engineermode/camera6589/Camera;II)V

    goto/16 :goto_2

    :cond_13
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1708(Lcom/mediatek/engineermode/camera6589/Camera;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1800(Lcom/mediatek/engineermode/camera6589/Camera;)Z

    move-result v1

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3100(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-le v1, v2, :cond_a

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_14
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3100(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    if-ge v1, v2, :cond_a

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, v1, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_3

    :sswitch_8
    const-string v1, "test/camera"

    const-string v2, "EVENT_PREVIEW_RAW_DUMP"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-virtual {v1, v6}, Lcom/mediatek/engineermode/camera6589/Camera;->onShutterButtonFocus(Z)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0x5 -> :sswitch_2
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0x66 -> :sswitch_5
        0x67 -> :sswitch_6
        0x6a -> :sswitch_7
        0x6b -> :sswitch_8
    .end sparse-switch
.end method
