.class Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;
.super Ljava/lang/Object;
.source "AutoCalibration.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera6589/AutoCalibration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 9
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const-string v2, "EM/AutoCalibration"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Button is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "isChecked "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$000(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    if-ne p1, v2, :cond_2

    new-array v1, v8, [I

    fill-array-data v1, :array_0

    new-array v0, v6, [Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$200(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v7}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$302(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v1, v0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v5}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$500(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1000(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)I

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1200(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v5}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1300(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v5}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1202(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$200(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    if-ne p1, v2, :cond_3

    new-array v1, v8, [I

    fill-array-data v1, :array_1

    new-array v0, v6, [Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$000(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v5}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$302(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v1, v0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v5}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$500(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    if-ne p1, v2, :cond_4

    new-array v1, v8, [I

    fill-array-data v1, :array_2

    new-array v0, v6, [Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$000(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$200(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v6}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$302(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v1, v0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$600(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v7}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$500(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)V

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$600(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    if-ne p1, v2, :cond_5

    new-array v1, v6, [I

    fill-array-data v1, :array_3

    new-array v0, v8, [Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$700(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$800(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$900(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v6

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v7}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1002(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v1, v0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$700(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    if-ne p1, v2, :cond_6

    new-array v1, v6, [I

    fill-array-data v1, :array_4

    new-array v0, v8, [Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$600(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$800(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$900(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v6

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v5}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1002(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v1, v0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V

    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$800(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    if-ne p1, v2, :cond_7

    new-array v1, v6, [I

    fill-array-data v1, :array_5

    new-array v0, v8, [Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$700(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$600(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$900(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v6

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v6}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1002(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v1, v0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V

    goto/16 :goto_0

    :cond_7
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$900(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    if-ne p1, v2, :cond_0

    new-array v1, v6, [I

    fill-array-data v1, :array_6

    new-array v0, v8, [Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$700(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$800(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$600(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;

    move-result-object v2

    aput-object v2, v0, v6

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v8}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1002(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v1, v0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V

    goto/16 :goto_0

    :cond_8
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1200(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v7}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1300(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;->this$0:Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    invoke-static {v2, v7}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->access$1202(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)Z

    goto/16 :goto_1

    nop

    :array_0
    .array-data 4
        0x0
        0x8
        0x8
    .end array-data

    :array_1
    .array-data 4
        0x8
        0x0
        0x8
    .end array-data

    :array_2
    .array-data 4
        0x8
        0x8
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x8
        0x8
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x8
    .end array-data

    :array_5
    .array-data 4
        0x8
        0x8
    .end array-data

    :array_6
    .array-data 4
        0x8
        0x0
    .end array-data
.end method
