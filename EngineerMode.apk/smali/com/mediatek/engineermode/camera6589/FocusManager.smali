.class public Lcom/mediatek/engineermode/camera6589/FocusManager;
.super Ljava/lang/Object;
.source "FocusManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/camera6589/FocusManager$1;,
        Lcom/mediatek/engineermode/camera6589/FocusManager$MainHandler;,
        Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;
    }
.end annotation


# static fields
.field private static final CONTINUOUS_FOCUSING:I = 0x0

.field private static final CONTINUOUS_FOCUS_FAIL:I = 0x2

.field private static final CONTINUOUS_FOCUS_SUCCESS:I = 0x1

.field private static final FOCUS_BEEP_VOLUME:I = 0x64

.field private static final FOCUS_FRAME_DELAY:I = 0x3e8

.field private static final RESET_FOCUS_FRAME:I = 0x1

.field private static final RESET_FOCUS_FRAME_DELAY:I = 0x320

.field private static final RESET_TOUCH_FOCUS:I = 0x0

.field private static final RESET_TOUCH_FOCUS_DELAY:I = 0xbb8

.field private static final STATE_FAIL:I = 0x4

.field private static final STATE_FOCUSING:I = 0x1

.field private static final STATE_FOCUSING_SNAP_ON_FINISH:I = 0x2

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_SUCCESS:I = 0x3

.field private static final TAG:Ljava/lang/String; = "test/FocusManager"


# instance fields
.field private mAeAwbLock:Z

.field private mDefaultFocusModes:[Ljava/lang/String;

.field private mEnableFaceBeauty:Z

.field private mFocusArea:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation
.end field

.field private mFocusAreaSupported:Z

.field private mFocusIndicator:Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;

.field private mFocusIndicatorRotateLayout:Landroid/view/View;

.field private mFocusMode:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mInitialized:Z

.field mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

.field private mLockAeAwbNeeded:Z

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMeteringArea:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation
.end field

.field private mOverrideFocusMode:Ljava/lang/String;

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private mPreviewFrame:Landroid/view/View;

.field private mRealFocusArea:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation
.end field

.field private mState:I


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 2
    .param p1    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mDefaultFocusModes:[Ljava/lang/String;

    new-instance v0, Lcom/mediatek/engineermode/camera6589/FocusManager$MainHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/engineermode/camera6589/FocusManager$MainHandler;-><init>(Lcom/mediatek/engineermode/camera6589/FocusManager;Lcom/mediatek/engineermode/camera6589/FocusManager$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mMatrix:Landroid/graphics/Matrix;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/camera6589/FocusManager;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->cancelAutoFocus()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/camera6589/FocusManager;)Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/FocusManager;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicator:Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;

    return-object v0
.end method

.method private autoFocus()V
    .locals 2

    const-string v0, "test/FocusManager"

    const-string v1, "Start autofocus."

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;->autoFocus()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->updateFocusUI()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method private cancelAutoFocus()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "test/FocusManager"

    const-string v1, "Cancel autofocus."

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->resetTouchFocus()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;->cancelAutoFocus()V

    iput v2, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->updateFocusUI()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method private initRealFocusArea()V
    .locals 10

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mRealFocusArea:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mRealFocusArea:Ljava/util/List;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mRealFocusArea:Ljava/util/List;

    new-instance v3, Landroid/hardware/Camera$Area;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    const/4 v9, 0x1

    invoke-direct {v3, v8, v9}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicatorRotateLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicatorRotateLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    if-eqz v1, :cond_1

    if-nez v2, :cond_2

    :cond_1
    const-string v0, "test/FocusManager"

    const-string v3, "UI Component not initialized, cancel this touch"

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mPreviewFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mPreviewFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v7

    div-int/lit8 v4, v6, 0x2

    div-int/lit8 v5, v7, 0x2

    const-string v0, "test/FocusManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "real area.x = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " real area.y = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/high16 v3, 0x3f800000

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mRealFocusArea:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Area;

    iget-object v8, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/mediatek/engineermode/camera6589/FocusManager;->calculateTapArea(IIFIIIILandroid/graphics/Rect;)V

    goto :goto_0
.end method

.method private static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private needAutoFocusCall()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->getFocusMode()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v2, "infinity"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "fixed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "edof"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "manual"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public calculateTapArea(IIFIIIILandroid/graphics/Rect;)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # F
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # Landroid/graphics/Rect;

    int-to-float v6, p1

    mul-float/2addr v6, p3

    float-to-int v2, v6

    int-to-float v6, p2

    mul-float/2addr v6, p3

    float-to-int v1, v6

    div-int/lit8 v6, v2, 0x2

    sub-int v6, p4, v6

    const/4 v7, 0x0

    sub-int v8, p6, v2

    invoke-static {v6, v7, v8}, Lcom/mediatek/engineermode/camera6589/Util;->clamp(III)I

    move-result v3

    div-int/lit8 v6, v1, 0x2

    sub-int v6, p5, v6

    const/4 v7, 0x0

    sub-int v8, p7, v1

    invoke-static {v6, v7, v8}, Lcom/mediatek/engineermode/camera6589/Util;->clamp(III)I

    move-result v5

    new-instance v4, Landroid/graphics/RectF;

    int-to-float v6, v3

    int-to-float v7, v5

    add-int v8, v3, v2

    int-to-float v8, v8

    add-int v9, v5, v1

    int-to-float v9, v9

    invoke-direct {v4, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    move-object/from16 v0, p8

    invoke-static {v4, v0}, Lcom/mediatek/engineermode/camera6589/Util;->rectFToRect(Landroid/graphics/RectF;Landroid/graphics/Rect;)V

    return-void
.end method

.method public capture()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "test/FocusManager"

    const-string v1, "capture()"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput v2, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public doSnap()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mInitialized:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    goto :goto_0
.end method

.method public getAeAwbLock()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mAeAwbLock:Z

    return v0
.end method

.method public getFocusAreas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    return-object v0
.end method

.method public getFocusMode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mOverrideFocusMode:Ljava/lang/String;

    return-object v0
.end method

.method public getMeteringAreas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mMeteringArea:Ljava/util/List;

    return-object v0
.end method

.method public getRealFocusAreas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mRealFocusArea:Ljava/util/List;

    return-object v0
.end method

.method public initialize(Landroid/view/View;Landroid/view/View;Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;ZI)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;
    .param p4    # Z
    .param p5    # I

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicatorRotateLayout:Landroid/view/View;

    const v1, 0x7f0b02ba

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicator:Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;

    iput-object p2, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mPreviewFrame:Landroid/view/View;

    iput-object p3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {v0, p4, p5, v1, v2}, Lcom/mediatek/engineermode/camera6589/Util;->prepareMatrix(Landroid/graphics/Matrix;ZIII)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mParameters:Landroid/hardware/Camera$Parameters;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mInitialized:Z

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->initRealFocusArea()V

    return-void

    :cond_0
    const-string v1, "test/FocusManager"

    const-string v2, "mParameters is not initialized."

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initializeParameters(Landroid/hardware/Camera$Parameters;)V
    .locals 4
    .param p1    # Landroid/hardware/Camera$Parameters;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, "auto"

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/camera6589/FocusManager;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusAreaSupported:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoExposureLockSupported()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoWhiteBalanceLockSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mLockAeAwbNeeded:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public isFocusCompleted()Z
    .locals 2

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFocusingSnapOnFinish()Z
    .locals 2

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAutoFocus(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x4

    const/4 v3, 0x3

    const-string v0, "test/FocusManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAutoFocus, mState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_1

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    :goto_0
    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->updateFocusUI()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;->onAutoFocusDone()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iput v4, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    if-eqz p1, :cond_3

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    :goto_2
    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->updateFocusUI()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_3
    iput v4, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;->onAutoFocusDone()V

    goto :goto_1
.end method

.method public onCameraReleased()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->onPreviewStopped()V

    return-void
.end method

.method public onPreviewStarted()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    return-void
.end method

.method public onPreviewStopped()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->resetTouchFocus()V

    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->updateFocusUI()V

    return-void
.end method

.method public onShutterDown()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mInitialized:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mLockAeAwbNeeded:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mAeAwbLock:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mAeAwbLock:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;->setFocusParameters()V

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->needAutoFocusCall()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->autoFocus()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;->onAutoFocusDone()V

    goto :goto_0
.end method

.method public onShutterUp()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mInitialized:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->needAutoFocusCall()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->cancelAutoFocus()V

    :cond_3
    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mLockAeAwbNeeded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mAeAwbLock:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mAeAwbLock:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;->setFocusParameters()V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/MotionEvent;)Z
    .locals 15
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mInitialized:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-string v0, "test/FocusManager"

    const-string v3, "onTouch "

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v3, 0x1

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->cancelAutoFocus()V

    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicatorRotateLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicatorRotateLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    if-eqz v1, :cond_4

    if-nez v2, :cond_5

    :cond_4
    const-string v0, "test/FocusManager"

    const-string v3, "UI Component not initialized, cancel this touch"

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const-string v0, "test/FocusManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TouchFocus: touch.x = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " touch.y = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mPreviewFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mPreviewFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v7

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    new-instance v3, Landroid/hardware/Camera$Area;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    const/4 v13, 0x1

    invoke-direct {v3, v8, v13}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mMeteringArea:Ljava/util/List;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mMeteringArea:Ljava/util/List;

    new-instance v3, Landroid/hardware/Camera$Area;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    const/4 v13, 0x1

    invoke-direct {v3, v8, v13}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    const/high16 v3, 0x3f800000

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Area;

    iget-object v8, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/mediatek/engineermode/camera6589/FocusManager;->calculateTapArea(IIFIIIILandroid/graphics/Rect;)V

    const/high16 v3, 0x3fc00000

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mMeteringArea:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Area;

    iget-object v8, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/mediatek/engineermode/camera6589/FocusManager;->calculateTapArea(IIFIIIILandroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicatorRotateLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    div-int/lit8 v0, v1, 0x2

    sub-int v0, v4, v0

    const/4 v3, 0x0

    sub-int v8, v6, v1

    invoke-static {v0, v3, v8}, Lcom/mediatek/engineermode/camera6589/Util;->clamp(III)I

    move-result v9

    div-int/lit8 v0, v2, 0x2

    sub-int v0, v5, v0

    const/4 v3, 0x0

    sub-int v8, v7, v2

    invoke-static {v0, v3, v8}, Lcom/mediatek/engineermode/camera6589/Util;->clamp(III)I

    move-result v12

    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-virtual {v10, v9, v12, v0, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {v10}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v11

    const/16 v0, 0xd

    const/4 v3, 0x0

    aput v3, v11, v0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicatorRotateLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mListener:Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;->setFocusParameters()V

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusAreaSupported:Z

    if-eqz v0, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_7

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->autoFocus()V

    :goto_1
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->updateFocusUI()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const-wide/16 v13, 0xbb8

    invoke-virtual {v0, v3, v13, v14}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method public overrideFocusMode(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mOverrideFocusMode:Ljava/lang/String;

    return-void
.end method

.method public removeMessages()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public resetTouchFocus()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-boolean v2, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mInitialized:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicatorRotateLayout:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v1

    const/16 v2, 0xd

    const/4 v3, -0x1

    aput v3, v1, v2

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iput-object v5, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    iput-object v5, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mMeteringArea:Ljava/util/List;

    goto :goto_0
.end method

.method public setAeAwbLock(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mAeAwbLock:Z

    return-void
.end method

.method public updateFocusUI()V
    .locals 5

    const-string v3, "test/FocusManager"

    const-string v4, "updateFocusUI()"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mInitialized:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mPreviewFrame:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mPreviewFrame:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    div-int/lit8 v2, v3, 0x4

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicator:Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusIndicator:Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mFocusArea:Ljava/util/List;

    if-nez v3, :cond_2

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusIndicator;->clear()V

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusIndicator;->showStart()V

    goto :goto_0

    :cond_3
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    :cond_4
    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusIndicator;->showStart()V

    goto :goto_0

    :cond_5
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusIndicator;->showSuccess()V

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/FocusManager;->mState:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    invoke-interface {v0}, Lcom/mediatek/engineermode/camera6589/FocusIndicator;->showFail()V

    goto :goto_0
.end method
