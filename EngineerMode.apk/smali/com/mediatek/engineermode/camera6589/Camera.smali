.class public Lcom/mediatek/engineermode/camera6589/Camera;
.super Lcom/mediatek/engineermode/camera6589/ActivityBase;
.source "Camera.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;,
        Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;,
        Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;,
        Lcom/mediatek/engineermode/camera6589/Camera$JpegPictureCallback;,
        Lcom/mediatek/engineermode/camera6589/Camera$RawPictureCallback;,
        Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;,
        Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;,
        Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;
    }
.end annotation


# static fields
.field private static final APP_MODE_NAME_MTK_ENG:Ljava/lang/String; = "MtkEng"

.field private static final CHECK_DISPLAY_ROTATION:I = 0x5

.field private static final CLEAR_SCREEN_DELAY:I = 0x3

.field private static final EVENT_CAPTURE_ACTION:I = 0x6a

.field private static final EVENT_COMPLETE_CAPTURE:I = 0x66

.field private static final EVENT_PREVIEW_RAW_DUMP:I = 0x6b

.field private static final EVENT_START_CAPTURE:I = 0x67

.field private static final FIRST_TIME_INIT:I = 0x2

.field private static final FOCUSING:I = 0x2

.field private static final FULL_SDCARD:J = -0x4L

.field private static final IDLE:I = 0x1

.field private static final JPEG_MAX_SIZE:J = 0x100000L

.field private static final KEY_FLASH_MODE:Ljava/lang/String; = "flash-mode"

.field private static final KEY_ISP_MODE:Ljava/lang/String; = "isp-mode"

.field private static final KEY_RAW_PATH:Ljava/lang/String; = "rawfname"

.field private static final KEY_RAW_SAVE_MODE:Ljava/lang/String; = "rawsave-mode"

.field private static final LOW_STORAGE_THRESHOLD:J = 0x2faf080L

.field private static final PICTURES_SAVING_DONE:I = 0x9

.field private static final PICTURE_SIZE:J = 0x16e360L

.field private static final PREPARING:J = -0x2L

.field private static final PREVIEW_STOPPED:I = 0x0

.field private static final RAW_JPEG_MAX_SIZE:J = 0x1100000L

.field private static final RAW_SAVE_JPEG:I = 0x3

.field private static final RAW_SAVE_VIDEO:I = 0x4

.field private static final ROPERTY_KEY_CLIENT_APPMODE:Ljava/lang/String; = "client.appmode"

.field private static final SAVING_PICTURES:I = 0x5

.field private static final SCREEN_DELAY:I = 0x1d4c0

.field private static final SELFTIMER_COUNTING:I = 0x4

.field private static final SNAPSHOT_IN_PROGRESS:I = 0x3

.field private static final STROBE_MODE:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "test/camera"

.field private static final UNAVAILABLE:J = -0x1L

.field private static final UNKNOWN_SIZE:J = -0x3L

.field private static final UPDATE_PARAM_ALL:I = -0x1

.field private static final UPDATE_PARAM_INITIALIZE:I = 0x1

.field private static final UPDATE_PARAM_PREFERENCE:I = 0x4

.field private static final UPDATE_STORAGE:I = 0x8

.field private static sIsAutoFocusCallback:Z


# instance fields
.field private mAeLockSupported:Z

.field private mAfBracketIntervel:I

.field private mAfBracketRange:I

.field private mAfCaptureTimes:I

.field private mAfMode:I

.field private mAfThroughDirect:I

.field private mAfThroughIntervel:I

.field private final mAutoFocusCallback:Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;

.field private mAutoFocusTime:J

.field private mAwbLockSupported:Z

.field private mBracketBestPos:I

.field private mBracketMaxPos:I

.field private mBracketMinPos:I

.field private mBracketTimes:I

.field private mCameraDisabled:Z

.field private mCameraId:I

.field private mCameraImageName:Ljava/lang/String;

.field mCameraOpenThread:Ljava/lang/Thread;

.field mCameraPreviewThread:Ljava/lang/Thread;

.field private mCameraState:I

.field private mCaptureMode:I

.field private mCaptureNumber:I

.field private mCaptureSize:I

.field private mCaptureStartTime:J

.field private mCaptureType:I

.field private mDidRegister:Z

.field private mDisplayOrientation:I

.field private mDisplayRotation:I

.field private final mErrorCallback:Lcom/mediatek/engineermode/camera6589/CameraErrorCallback;

.field private mFirstTimeInitialized:Z

.field private mFlickerString:Ljava/lang/String;

.field private mFocusAreaIndicator:Lcom/mediatek/engineermode/camera6589/RotateLayout;

.field private mFocusAreaSupported:Z

.field mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

.field private mFocusStartTime:J

.field final mHandler:Landroid/os/Handler;

.field private mInitialParams:Landroid/hardware/Camera$Parameters;

.field private mIsBracketAddPos:Z

.field private mIsoValue:Ljava/lang/String;

.field private mIsoValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mJpegImageData:[B

.field private mMainFlashLevel:I

.field private mMeteringAreaSupported:Z

.field private mOnResumeTime:J

.field private mOnScreenIndicators:Lcom/mediatek/engineermode/camera6589/Rotatable;

.field private mOpenCameraFail:Z

.field private mOrientation:I

.field private mOrientationCompensation:I

.field private mOrientationListener:Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private mPausing:Z

.field private mPicturesRemaining:J

.field private mPopupGestureDetector:Landroid/view/GestureDetector;

.field private mPosValue:I

.field private mPreFlashLevel:I

.field private mPreviewFrame:Landroid/view/View;

.field private mPreviewFrameLayout:Lcom/mediatek/engineermode/camera6589/PreviewFrameLayout;

.field private mPreviewPanel:Landroid/view/View;

.field private final mPreviewRawDumpCallback:Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;

.field private mRawCaptureFileName:Ljava/lang/String;

.field private final mRawPictureCallback:Lcom/mediatek/engineermode/camera6589/Camera$RawPictureCallback;

.field private mRawPictureCallbackTime:J

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSettingRetrieved:Z

.field private mShutterButton:Landroid/widget/Button;

.field private final mShutterCallback:Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;

.field private mShutterCallbackTime:J

.field private mShutterLag:J

.field private mSnapshotOnIdle:Z

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mStrobeMode:Ljava/lang/String;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mTakePicDone:Z

.field private mThroughFocusEndPos:I

.field private mThroughFocusStartPos:I

.field private mTripleCount:I

.field private mVideoResolution:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    sput-boolean v2, Lcom/mediatek/engineermode/camera6589/Camera;->sIsAutoFocusCallback:Z

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "auto"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "on"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "off"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/engineermode/camera6589/Camera;->STROBE_MODE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/ActivityBase;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientation:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientationCompensation:I

    iput-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOpenCameraFail:Z

    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraDisabled:Z

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSnapshotOnIdle:Z

    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDidRegister:Z

    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterCallback:Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;

    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$RawPictureCallback;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera$RawPictureCallback;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawPictureCallback:Lcom/mediatek/engineermode/camera6589/Camera$RawPictureCallback;

    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAutoFocusCallback:Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;

    new-instance v0, Lcom/mediatek/engineermode/camera6589/CameraErrorCallback;

    invoke-direct {v0}, Lcom/mediatek/engineermode/camera6589/CameraErrorCallback;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mErrorCallback:Lcom/mediatek/engineermode/camera6589/CameraErrorCallback;

    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewRawDumpCallback:Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;

    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera$MainHandler;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSettingRetrieved:Z

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureSize:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureType:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureNumber:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mVideoResolution:I

    const-string v0, "50hz"

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFlickerString:Ljava/lang/String;

    const-string v0, "auto"

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mStrobeMode:Ljava/lang/String;

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreFlashLevel:I

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mMainFlashLevel:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfBracketRange:I

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfBracketIntervel:I

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfThroughIntervel:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfThroughDirect:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mThroughFocusStartPos:I

    const/16 v0, 0x3ff

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mThroughFocusEndPos:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValue:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTakePicDone:Z

    iput-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mStorageManager:Landroid/os/storage/StorageManager;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraImageName:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsBracketAddPos:Z

    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/camera6589/Camera$1;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/engineermode/camera6589/Camera$2;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/camera6589/Camera$2;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraOpenThread:Ljava/lang/Thread;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/engineermode/camera6589/Camera$3;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/camera6589/Camera$3;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/camera6589/Camera;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    return v0
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/camera6589/Camera;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraState(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->startPreview()V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    return v0
.end method

.method static synthetic access$1502(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    return p1
.end method

.method static synthetic access$1508(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    return v0
.end method

.method static synthetic access$1602(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    return p1
.end method

.method static synthetic access$1700(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketTimes:I

    return v0
.end method

.method static synthetic access$1702(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketTimes:I

    return p1
.end method

.method static synthetic access$1708(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketTimes:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketTimes:I

    return v0
.end method

.method static synthetic access$1800(Lcom/mediatek/engineermode/camera6589/Camera;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsBracketAddPos:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsBracketAddPos:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    return v0
.end method

.method static synthetic access$2000(Lcom/mediatek/engineermode/camera6589/Camera;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->isStorageEnough4Capture()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/mediatek/engineermode/camera6589/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # Landroid/hardware/Camera$Parameters;

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketBestPos:I

    return v0
.end method

.method static synthetic access$2202(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketBestPos:I

    return p1
.end method

.method static synthetic access$2300(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketMaxPos:I

    return v0
.end method

.method static synthetic access$2302(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketMaxPos:I

    return p1
.end method

.method static synthetic access$2400(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketMinPos:I

    return v0
.end method

.method static synthetic access$2402(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketMinPos:I

    return p1
.end method

.method static synthetic access$2500(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPosValue:I

    return v0
.end method

.method static synthetic access$2502(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPosValue:I

    return p1
.end method

.method static synthetic access$2600(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfBracketIntervel:I

    return v0
.end method

.method static synthetic access$2700(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfThroughDirect:I

    return v0
.end method

.method static synthetic access$2800(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mThroughFocusStartPos:I

    return v0
.end method

.method static synthetic access$2900(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mThroughFocusEndPos:I

    return v0
.end method

.method static synthetic access$3000(Lcom/mediatek/engineermode/camera6589/Camera;II)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/camera6589/Camera;->setThroughFocusManualPos(II)V

    return-void
.end method

.method static synthetic access$3100(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfThroughIntervel:I

    return v0
.end method

.method static synthetic access$3200(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->capture()V

    return-void
.end method

.method static synthetic access$3300(Lcom/mediatek/engineermode/camera6589/Camera;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTakePicDone:Z

    return v0
.end method

.method static synthetic access$3302(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTakePicDone:Z

    return p1
.end method

.method static synthetic access$3400(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->takePicture()V

    return-void
.end method

.method static synthetic access$3500(Lcom/mediatek/engineermode/camera6589/Camera;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfBracketRange:I

    return v0
.end method

.method static synthetic access$3800(Lcom/mediatek/engineermode/camera6589/Camera;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$3802(Lcom/mediatek/engineermode/camera6589/Camera;J)J
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterCallbackTime:J

    return-wide p1
.end method

.method static synthetic access$3900(Lcom/mediatek/engineermode/camera6589/Camera;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterLag:J

    return-wide v0
.end method

.method static synthetic access$3902(Lcom/mediatek/engineermode/camera6589/Camera;J)J
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterLag:J

    return-wide p1
.end method

.method static synthetic access$4000(Lcom/mediatek/engineermode/camera6589/Camera;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureStartTime:J

    return-wide v0
.end method

.method static synthetic access$4100(Lcom/mediatek/engineermode/camera6589/Camera;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawPictureCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$4102(Lcom/mediatek/engineermode/camera6589/Camera;J)J
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawPictureCallbackTime:J

    return-wide p1
.end method

.method static synthetic access$4202(Lcom/mediatek/engineermode/camera6589/Camera;[B)[B
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # [B

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mJpegImageData:[B

    return-object p1
.end method

.method static synthetic access$4300(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->doAttach()V

    return-void
.end method

.method static synthetic access$4400(Lcom/mediatek/engineermode/camera6589/Camera;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAutoFocusTime:J

    return-wide v0
.end method

.method static synthetic access$4402(Lcom/mediatek/engineermode/camera6589/Camera;J)J
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAutoFocusTime:J

    return-wide p1
.end method

.method static synthetic access$4500(Lcom/mediatek/engineermode/camera6589/Camera;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusStartTime:J

    return-wide v0
.end method

.method static synthetic access$4602(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/engineermode/camera6589/Camera;->sIsAutoFocusCallback:Z

    return p0
.end method

.method static synthetic access$4700(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    return v0
.end method

.method static synthetic access$4802(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOpenCameraFail:Z

    return p1
.end method

.method static synthetic access$4902(Lcom/mediatek/engineermode/camera6589/Camera;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraDisabled:Z

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->initializeFirstTime()V

    return-void
.end method

.method static synthetic access$5000(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->initializeCapabilities()V

    return-void
.end method

.method static synthetic access$5100(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientation:I

    return v0
.end method

.method static synthetic access$5102(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientation:I

    return p1
.end method

.method static synthetic access$5200(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientationCompensation:I

    return v0
.end method

.method static synthetic access$5202(Lcom/mediatek/engineermode/camera6589/Camera;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientationCompensation:I

    return p1
.end method

.method static synthetic access$5300(Lcom/mediatek/engineermode/camera6589/Camera;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera;->setOrientationIndicator(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/camera6589/Camera;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDisplayRotation:I

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->setDisplayOrientation()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/camera6589/Camera;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOnResumeTime:J

    return-wide v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->checkStorage()V

    return-void
.end method

.method private capture()V
    .locals 2

    const-string v0, "test/camera"

    const-string v1, "capture()"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "test/camera"

    const-string v1, "capture() return1"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/engineermode/camera6589/Camera;->onShutterButtonFocus(Z)V

    goto :goto_0
.end method

.method private checkStorage()V
    .locals 10

    const-wide/32 v8, 0x2faf080

    const-wide/16 v6, 0x0

    const-string v2, "test/camera"

    const-string v3, "checkStorage()"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->getAvailableSpace()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    iget-wide v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    const-wide/16 v4, -0x2

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    const v2, 0x7f0804fe

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->toastOnUiThread(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    const v2, 0x7f0804e0

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->toastOnUiThread(I)V

    goto :goto_0

    :cond_2
    iget-wide v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_4

    const-wide/32 v0, 0x16e360

    iget-wide v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    sub-long/2addr v2, v8

    div-long/2addr v2, v0

    iput-wide v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    :cond_3
    :goto_1
    iget v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    cmp-long v2, v2, v6

    if-gez v2, :cond_5

    const v2, 0x7f0804fa

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->toastOnUiThread(I)V

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_3

    iput-wide v6, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    goto :goto_1

    :cond_5
    const-string v2, "test/camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can take "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " photos."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private closeCamera()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/camera6589/CameraHolder;->instance()Lcom/mediatek/engineermode/camera6589/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/CameraHolder;->release()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewRawDumpCallback(Landroid/hardware/Camera$PreviewRawDumpCallback;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->onCameraReleased()V

    :cond_0
    return-void
.end method

.method private createName(J)Ljava/lang/String;
    .locals 1
    .param p1    # J

    const-string v0, "ddkkmmss"

    invoke-static {v0, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createNameJpeg(J)Ljava/lang/String;
    .locals 1
    .param p0    # J

    const-string v0, "yyyy-MM-dd kk.mm.ss"

    invoke-static {v0, p0, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private doAttach()V
    .locals 7

    const-string v4, "test/camera"

    const-string v5, "doAttach()"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x5

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraState(I)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "test/camera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Jpeg name is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mJpegImageData:[B

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private getAvailableSpace()J
    .locals 7

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v3, :cond_0

    const-string v3, "storage"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageManager;

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mStorageManager:Landroid/os/storage/StorageManager;

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/storage/StorageManagerEx;->getDefaultPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/DCIM/CameraEM/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraImageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-static {}, Lcom/mediatek/storage/StorageManagerEx;->getDefaultPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "test/camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "External storage state="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mount point = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/storage/StorageManagerEx;->getDefaultPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "checking"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-wide/16 v3, -0x2

    :goto_0
    return-wide v3

    :cond_1
    const-string v3, "mounted"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-wide/16 v3, -0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraImageName:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    const-wide/16 v3, -0x4

    goto :goto_0

    :cond_4
    new-instance v1, Landroid/os/StatFs;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraImageName:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v5

    int-to-long v5, v5

    mul-long/2addr v3, v5

    goto :goto_0
.end method

.method private getIsoValue(ILjava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const-string v2, "test/camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getIsoValue iso "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " af mode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iput-object p2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValue:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v2, ","

    invoke-virtual {p2, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    :cond_1
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_2

    const-string v2, "test/camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_3
    const-string v2, "test/camera"

    const-string v3, "mIsoValues == null"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getSettingsFromPref()V
    .locals 6

    const v5, 0x7f0804d2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "test/camera"

    const-string v2, "getSettingsFromPref"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "camera_inter_settings"

    invoke-virtual {p0, v1, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const v1, 0x7f0804cf

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    if-nez v1, :cond_1

    const v1, 0x7f0804d0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureSize:I

    const v1, 0x7f0804d1

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureType:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureNumber:I

    :goto_0
    const v1, 0x7f0804d5

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "50hz"

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFlickerString:Ljava/lang/String;

    :goto_1
    sget-object v1, Lcom/mediatek/engineermode/camera6589/Camera;->STROBE_MODE:[Ljava/lang/String;

    const v2, 0x7f0804d6

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mStrobeMode:Ljava/lang/String;

    const v1, 0x7f0804d7

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreFlashLevel:I

    const v1, 0x7f0804d8

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mMainFlashLevel:I

    const v1, 0x7f0804d9

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    if-ne v1, v4, :cond_4

    const v1, 0x7f0804db

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfBracketIntervel:I

    const v1, 0x7f0804da

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfBracketRange:I

    :cond_0
    :goto_2
    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    const v2, 0x7f0804d4

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->getIsoValue(ILjava/lang/String;)V

    return-void

    :cond_1
    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    if-ne v1, v4, :cond_2

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureNumber:I

    goto/16 :goto_0

    :cond_2
    const v1, 0x7f0804d3

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mVideoResolution:I

    goto/16 :goto_0

    :cond_3
    const-string v1, "60hz"

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFlickerString:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const v1, 0x7f0804dc

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfThroughDirect:I

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfThroughDirect:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    const v1, 0x7f0804de

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mThroughFocusStartPos:I

    const v1, 0x7f0804df

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3ff

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mThroughFocusEndPos:I

    :cond_5
    const v1, 0x7f0804dd

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfThroughIntervel:I

    goto :goto_2
.end method

.method private initializeCapabilities()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "test/camera"

    const-string v3, "initializeCapabilities()"

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v3}, Lcom/mediatek/engineermode/camera6589/FocusManager;->initializeParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "auto"

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/engineermode/camera6589/Camera;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusAreaSupported:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v0

    if-lez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mMeteringAreaSupported:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoExposureLockSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAeLockSupported:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoWhiteBalanceLockSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAwbLockSupported:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private initializeFirstTime()V
    .locals 10

    const/4 v9, 0x1

    const-string v0, "test/camera"

    const-string v1, "initializeFirstTime()"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFirstTimeInitialized:Z

    if-eqz v0, :cond_0

    const-string v0, "test/camera"

    const-string v1, "initializeFirstTime() return 1"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;

    invoke-direct {v0, p0, p0}, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientationListener:Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientationListener:Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->checkStorage()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0b00c1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewFrame:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewFrame:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0b02b9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mediatek/engineermode/camera6589/RotateLayout;

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusAreaIndicator:Lcom/mediatek/engineermode/camera6589/RotateLayout;

    invoke-static {}, Lcom/mediatek/engineermode/camera6589/CameraHolder;->instance()Lcom/mediatek/engineermode/camera6589/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    aget-object v6, v0, v1

    iget v0, v6, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v0, v9, :cond_1

    move v4, v9

    :goto_1
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusAreaIndicator:Lcom/mediatek/engineermode/camera6589/RotateLayout;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewFrame:Landroid/view/View;

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDisplayOrientation:I

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/engineermode/camera6589/FocusManager;->initialize(Landroid/view/View;Landroid/view/View;Lcom/mediatek/engineermode/camera6589/FocusManager$Listener;ZI)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/camera6589/Util;->initializeScreenBrightness(Landroid/view/Window;Landroid/content/ContentResolver;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->installIntentFilter()V

    iput-boolean v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFirstTimeInitialized:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const-string v0, "test/camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Performance test][Camera][Camera] camera switch Main To Sub end ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private initializeSecondTime()V
    .locals 2

    const-string v0, "test/camera"

    const-string v1, "initializeSecondTime()"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientationListener:Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->installIntentFilter()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->checkStorage()V

    return-void
.end method

.method private installIntentFilter()V
    .locals 3

    const-string v1, "test/camera"

    const-string v2, "installIntentFilter()"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDidRegister:Z

    return-void
.end method

.method private isCameraIdle()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/camera6589/FocusManager;->isFocusCompleted()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isStorageEnough4Capture()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "test/camera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isStorageEnough() captureType:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->getAvailableSpace()J

    move-result-wide v0

    iget v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    const-wide/32 v4, 0x100000

    cmp-long v4, v0, v4

    if-lez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    const-wide/32 v4, 0x1100000

    cmp-long v4, v0, v4

    if-gtz v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method private isSupportCameraPictureSize(Landroid/hardware/Camera$Size;)Z
    .locals 11
    .param p1    # Landroid/hardware/Camera$Size;

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f06005b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v5, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v1, v0, v3

    const/16 v8, 0x78

    invoke-virtual {v1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/4 v8, -0x1

    if-ne v4, v8, :cond_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget v8, p1, Landroid/hardware/Camera$Size;->width:I

    if-ne v8, v6, :cond_0

    iget v8, p1, Landroid/hardware/Camera$Size;->height:I

    if-ne v8, v2, :cond_0

    const/4 v7, 0x1

    :goto_1
    return v7

    :cond_2
    const-string v8, "test/camera"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "No supported picture size found "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p1, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private keepScreenOnAwhile()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.method private resetScreenOn()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method private setCameraParameters(I)V
    .locals 3
    .param p1    # I

    const-string v0, "test/camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCameraParameters() updateSet "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->updateCameraParametersInitialize()V

    :cond_0
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->checkStorage()V

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera;->updateCameraParametersPreference(I)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/engineermode/camera6589/Camera;->sIsAutoFocusCallback:Z

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    return-void
.end method

.method private setCameraState(I)V
    .locals 3
    .param p1    # I

    const-string v0, "test/camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCameraState() state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    return-void
.end method

.method private setDisplayOrientation()V
    .locals 2

    invoke-static {p0}, Lcom/mediatek/engineermode/camera6589/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDisplayRotation:I

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDisplayRotation:I

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/camera6589/Util;->getDisplayOrientation(II)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDisplayOrientation:I

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    return-void
.end method

.method private setEmCameraParam()V
    .locals 9

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    const-string v3, "test/camera"

    const-string v4, "setEmCameraParam() set EM parameters."

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3, v8}, Landroid/hardware/Camera$Parameters;->setCameraMode(I)V

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureType:I

    if-ne v3, v7, :cond_2

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "rawsave-mode"

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    const-string v3, "JpegOnly"

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    :cond_0
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraImageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/camera6589/Camera;->createName(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    const-string v2, ""

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    if-nez v3, :cond_7

    const-string v2, "auto"

    :goto_1
    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3, v2}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v3, v2}, Lcom/mediatek/engineermode/camera6589/FocusManager;->overrideFocusMode(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v4}, Lcom/mediatek/engineermode/camera6589/FocusManager;->getRealFocusAreas()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFlickerString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/hardware/Camera$Parameters;->setAntibanding(Ljava/lang/String;)V

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    if-nez v3, :cond_11

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    if-ne v3, v7, :cond_c

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Landroid/hardware/Camera$Parameters;->setISOSpeed(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ISO"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Auto"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    :goto_2
    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "rawfname"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".raw"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_3
    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "flash-mode"

    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mStrobeMode:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "test/camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setEmCameraParam mRawCaptureFileName "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureSize:I

    if-ne v3, v7, :cond_3

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "rawsave-mode"

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    const-string v3, "Video"

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    :goto_4
    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "isp-mode"

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureType:I

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_3
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureSize:I

    if-nez v3, :cond_4

    const-string v3, "Preview"

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    :goto_5
    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "rawsave-mode"

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureSize:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    goto :goto_4

    :cond_4
    const-string v3, "Capture"

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    goto :goto_5

    :cond_5
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    if-ne v3, v7, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3, v6}, Landroid/hardware/Camera$Parameters;->setCameraMode(I)V

    const-string v3, "VideoClip"

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "rawsave-mode"

    invoke-virtual {v3, v4, v6}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mVideoResolution:I

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3, v8}, Landroid/hardware/Camera$Parameters;->setPreviewRawDumpResolution(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3, v6}, Landroid/hardware/Camera$Parameters;->setPreviewRawDumpResolution(I)V

    goto/16 :goto_0

    :cond_7
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    if-ne v3, v6, :cond_9

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    if-nez v3, :cond_8

    const-string v2, "auto"

    goto/16 :goto_1

    :cond_8
    const-string v2, "manual"

    goto/16 :goto_1

    :cond_9
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfMode:I

    if-ne v3, v7, :cond_a

    const-string v2, "fullscan"

    goto/16 :goto_1

    :cond_a
    const-string v2, "manual"

    goto/16 :goto_1

    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ISO"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    goto/16 :goto_2

    :cond_c
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_10

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Landroid/hardware/Camera$Parameters;->setISOSpeed(Ljava/lang/String;)V

    const-string v4, "test/camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EVENT_CAPTURE_ACTION iso speed "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    iget v6, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    iget v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ISO"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Auto"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    :goto_6
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureType:I

    if-eq v3, v7, :cond_d

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "rawfname"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".raw"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    const-string v3, "test/camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mTripleCount "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAfCaptureTimes "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    iget v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureNumber:I

    if-ne v3, v4, :cond_f

    iput v8, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    :goto_7
    const-string v3, "test/camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mTripleCount "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAfCaptureTimes "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ISO"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValues:Ljava/util/ArrayList;

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    goto/16 :goto_6

    :cond_f
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    goto :goto_7

    :cond_10
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    goto/16 :goto_3

    :cond_11
    const-string v3, "test/camera"

    const-string v4, "EVENT_CAPTURE_ACTION mAfMode != 0 "

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/hardware/Camera$Parameters;->setISOSpeed(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValue:Ljava/lang/String;

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ISO"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Auto"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    :goto_8
    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureType:I

    if-eq v3, v7, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v4, "rawfname"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".raw"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_12
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ISO"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsoValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawCaptureFileName:Ljava/lang/String;

    goto :goto_8
.end method

.method private setOrientationIndicator(I)V
    .locals 8
    .param p1    # I

    const-string v5, "test/camera"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setOrientationIndicator() orientation "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v3, v5, [Lcom/mediatek/engineermode/camera6589/Rotatable;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusAreaIndicator:Lcom/mediatek/engineermode/camera6589/RotateLayout;

    aput-object v6, v3, v5

    move-object v0, v3

    array-length v4, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v2, v0, v1

    if-eqz v2, :cond_0

    invoke-interface {v2, p1}, Lcom/mediatek/engineermode/camera6589/Rotatable;->setOrientation(I)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOnScreenIndicators:Lcom/mediatek/engineermode/camera6589/Rotatable;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOnScreenIndicators:Lcom/mediatek/engineermode/camera6589/Rotatable;

    invoke-interface {v5, p1}, Lcom/mediatek/engineermode/camera6589/Rotatable;->setOrientation(I)V

    :cond_2
    return-void
.end method

.method private setPictureSize(I)V
    .locals 13
    .param p1    # I

    const-string v10, "test/camera"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "setPictureSize camOri "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v10}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v3

    const/4 v10, 0x0

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    if-ge v0, v10, :cond_2

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->height:I

    iget v11, v1, Landroid/hardware/Camera$Size;->height:I

    if-gt v10, v11, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->width:I

    iget v11, v1, Landroid/hardware/Camera$Size;->width:I

    if-le v10, v11, :cond_1

    :cond_0
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    invoke-direct {p0, v10}, Lcom/mediatek/engineermode/camera6589/Camera;->isSupportCameraPictureSize(Landroid/hardware/Camera$Size;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string v10, "test/camera"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Max picture size is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v1, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    if-nez v10, :cond_4

    iget v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureSize:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    if-eqz p1, :cond_3

    const/16 v10, 0xb4

    if-ne p1, v10, :cond_5

    :cond_3
    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v11, v1, Landroid/hardware/Camera$Size;->height:I

    iget v12, v1, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v10, v11, v12}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v11, v1, Landroid/hardware/Camera$Size;->width:I

    iget v12, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v10, v11, v12}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto :goto_1

    :cond_6
    iget v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureSize:I

    if-nez v10, :cond_c

    const/4 v10, 0x0

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/Camera$Size;

    iget v10, v1, Landroid/hardware/Camera$Size;->width:I

    div-int/lit8 v6, v10, 0x2

    iget v10, v1, Landroid/hardware/Camera$Size;->height:I

    div-int/lit8 v8, v10, 0x2

    const/4 v10, 0x0

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->width:I

    sub-int v10, v6, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v7

    const/4 v10, 0x0

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->height:I

    sub-int v10, v8, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v9

    const/4 v0, 0x1

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    if-ge v0, v10, :cond_9

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->width:I

    sub-int v10, v6, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    if-gt v7, v10, :cond_7

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->height:I

    sub-int v10, v8, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    if-le v9, v10, :cond_8

    :cond_7
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/Camera$Size;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->width:I

    sub-int v10, v6, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v7

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->height:I

    sub-int v10, v8, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v9

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    const-string v10, "test/camera"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "preview size is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_a

    const/16 v10, 0xb4

    if-ne p1, v10, :cond_b

    :cond_a
    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v11, v4, Landroid/hardware/Camera$Size;->height:I

    iget v12, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v10, v11, v12}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto/16 :goto_1

    :cond_b
    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v11, v4, Landroid/hardware/Camera$Size;->width:I

    iget v12, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v10, v11, v12}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto/16 :goto_1

    :cond_c
    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v10}, Landroid/hardware/Camera$Parameters;->getSupportedVideoSizes()Ljava/util/List;

    move-result-object v5

    const/4 v0, 0x0

    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-ge v0, v10, :cond_e

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    invoke-virtual {p0, v3, v10}, Lcom/mediatek/engineermode/camera6589/Camera;->isSupportSize(Ljava/util/List;Landroid/hardware/Camera$Size;)Z

    move-result v10

    if-nez v10, :cond_d

    invoke-interface {v5, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_e
    const/4 v10, 0x0

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    const/4 v0, 0x1

    :goto_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-ge v0, v10, :cond_11

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->height:I

    iget v11, v2, Landroid/hardware/Camera$Size;->height:I

    if-gt v10, v11, :cond_f

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v10, v10, Landroid/hardware/Camera$Size;->width:I

    iget v11, v2, Landroid/hardware/Camera$Size;->width:I

    if-le v10, v11, :cond_10

    :cond_f
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_11
    const-string v10, "test/camera"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "video size is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v2, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_12

    const/16 v10, 0xb4

    if-ne p1, v10, :cond_13

    :cond_12
    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v11, v2, Landroid/hardware/Camera$Size;->height:I

    iget v12, v2, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v10, v11, v12}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto/16 :goto_1

    :cond_13
    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v11, v2, Landroid/hardware/Camera$Size;->width:I

    iget v12, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v10, v11, v12}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto/16 :goto_1
.end method

.method private setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->closeCamera()V

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "setPreviewDisplay failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private setThroughFocusManualPos(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v2, "camera_inter_settings"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private startPreview()V
    .locals 4

    const/4 v3, 0x1

    const-string v1, "test/camera"

    const-string v2, "startPreview()"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const-string v1, "test/camera"

    const-string v2, "startPreview() return1"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/camera6589/FocusManager;->resetTouchFocus()V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mErrorCallback:Lcom/mediatek/engineermode/camera6589/CameraErrorCallback;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewRawDumpCallback:Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setPreviewRawDumpCallback(Landroid/hardware/Camera$PreviewRawDumpCallback;)V

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->stopPreview()V

    :cond_3
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v1, :cond_4

    const-string v1, "test/camera"

    const-string v2, "startPreview() mSurfaceHolder == null"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/camera6589/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->setDisplayOrientation()V

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSnapshotOnIdle:Z

    if-nez v1, :cond_6

    const-string v1, "continuous-picture"

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v2}, Lcom/mediatek/engineermode/camera6589/FocusManager;->getFocusMode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->cancelAutoFocus()V

    :cond_5
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/camera6589/FocusManager;->setAeAwbLock(Z)V

    :cond_6
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraParameters(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    if-eqz v1, :cond_7

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSettingRetrieved:Z

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_7
    :try_start_1
    const-string v1, "test/camera"

    const-string v2, "startPreview"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraState(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/camera6589/FocusManager;->onPreviewStarted()V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    if-eqz v1, :cond_8

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    monitor-enter v2

    :try_start_2
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_8
    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTakePicDone:Z

    if-nez v1, :cond_1

    iput-boolean v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTakePicDone:Z

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->closeCamera()V

    const-string v1, "test/camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startPreview exception."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "startPreview failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method private stopPreview()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    if-eqz v0, :cond_0

    const-string v0, "test/camera"

    const-string v1, "stopPreview"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->onPreviewStopped()V

    return-void
.end method

.method private takePicture()V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "test/camera"

    const-string v1, "takePicture() return1"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "test/camera"

    const-string v1, "takePicture() start"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureStartTime:J

    iput-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mJpegImageData:[B

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    iget v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientation:I

    invoke-static {v0, v1, v2}, Lcom/mediatek/engineermode/camera6589/Util;->setRotationParameter(Landroid/hardware/Camera$Parameters;II)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterCallback:Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mRawPictureCallback:Lcom/mediatek/engineermode/camera6589/Camera$RawPictureCallback;

    new-instance v3, Lcom/mediatek/engineermode/camera6589/Camera$JpegPictureCallback;

    invoke-direct {v3, p0, v4}, Lcom/mediatek/engineermode/camera6589/Camera$JpegPictureCallback;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    goto :goto_0
.end method

.method private toastOnUiThread(I)V
    .locals 1
    .param p1    # I

    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$4;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera$4;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;I)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private updateCameraParametersInitialize()V
    .locals 4

    const-string v2, "test/camera"

    const-string v3, "updateCameraParametersInitialize()"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFrameRates()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setRecordingHint(Z)V

    return-void
.end method

.method private updateCameraParametersPreference(I)V
    .locals 13
    .param p1    # I

    const-string v9, "test/camera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "updateCameraParametersPreference() updataSet "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const v9, 0x7f0804ff

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/mediatek/engineermode/camera6589/Camera;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSceneMode()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v6}, Landroid/hardware/Camera$Parameters;->setSceneMode(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v10}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v9}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v9

    iput-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    :cond_0
    :goto_0
    iget-boolean v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAeLockSupported:Z

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v10}, Lcom/mediatek/engineermode/camera6589/FocusManager;->getAeAwbLock()Z

    move-result v10

    invoke-virtual {v9, v10}, Landroid/hardware/Camera$Parameters;->setAutoExposureLock(Z)V

    :cond_1
    iget-boolean v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAwbLockSupported:Z

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v10}, Lcom/mediatek/engineermode/camera6589/FocusManager;->getAeAwbLock()Z

    move-result v10

    invoke-virtual {v9, v10}, Landroid/hardware/Camera$Parameters;->setAutoWhiteBalanceLock(Z)V

    :cond_2
    iget-boolean v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mMeteringAreaSupported:Z

    if-eqz v9, :cond_3

    sget-boolean v9, Lcom/mediatek/engineermode/camera6589/Camera;->sIsAutoFocusCallback:Z

    if-nez v9, :cond_3

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v10}, Lcom/mediatek/engineermode/camera6589/FocusManager;->getMeteringAreas()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    :cond_3
    invoke-static {}, Lcom/mediatek/engineermode/camera6589/CameraHolder;->instance()Lcom/mediatek/engineermode/camera6589/CameraHolder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/engineermode/camera6589/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v9

    iget v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    aget-object v9, v9, v10

    iget v2, v9, Landroid/hardware/Camera$CameraInfo;->orientation:I

    const-string v9, "test/camera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " Sensor["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]\'s orientation is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->setPictureSize(I)V

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v7

    const-string v9, "test/camera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getPictureSize is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "x"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    iget v9, v7, Landroid/hardware/Camera$Size;->height:I

    iget v10, v7, Landroid/hardware/Camera$Size;->width:I

    if-le v9, v10, :cond_8

    iget v9, v7, Landroid/hardware/Camera$Size;->height:I

    int-to-double v9, v9

    iget v11, v7, Landroid/hardware/Camera$Size;->width:I

    int-to-double v11, v11

    div-double v0, v9, v11

    :goto_1
    const v9, 0x7f0b00bf

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewPanel:Landroid/view/View;

    const v9, 0x7f0b00c0

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/mediatek/engineermode/camera6589/PreviewFrameLayout;

    iput-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewFrameLayout:Lcom/mediatek/engineermode/camera6589/PreviewFrameLayout;

    const-string v9, "test/camera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "setAspectRatio "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewFrameLayout:Lcom/mediatek/engineermode/camera6589/PreviewFrameLayout;

    invoke-virtual {v9, v0, v1}, Lcom/mediatek/engineermode/camera6589/PreviewFrameLayout;->setAspectRatio(D)V

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v8

    invoke-static {p0, v8, v0, v1}, Lcom/mediatek/engineermode/camera6589/Util;->getOptimalPreviewSize(Landroid/app/Activity;Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v4

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/hardware/Camera$Size;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    if-eqz v2, :cond_4

    const/16 v9, 0xb4

    if-ne v2, v9, :cond_9

    :cond_4
    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v10, v4, Landroid/hardware/Camera$Size;->height:I

    iget v11, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v9, v10, v11}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    :goto_2
    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v10, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v10}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v9}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v9

    iput-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    :cond_5
    const-string v9, "test/camera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Preview size is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "x"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/media/CameraProfile;->getJpegEncodingQualityParameter(II)I

    move-result v3

    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v3}, Landroid/hardware/Camera$Parameters;->setJpegQuality(I)V

    const/4 v9, -0x1

    if-ne p1, v9, :cond_6

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->setEmCameraParam()V

    :cond_6
    return-void

    :cond_7
    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSceneMode()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    const-string v6, "auto"

    goto/16 :goto_0

    :cond_8
    iget v9, v7, Landroid/hardware/Camera$Size;->width:I

    int-to-double v9, v9

    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    int-to-double v11, v11

    div-double v0, v9, v11

    goto/16 :goto_1

    :cond_9
    iget-object v9, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v10, v4, Landroid/hardware/Camera$Size;->width:I

    iget v11, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v9, v10, v11}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto :goto_2
.end method


# virtual methods
.method public autoFocus()V
    .locals 2

    const-string v0, "test/camera"

    const-string v1, "autoFocus"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusStartTime:J

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAutoFocusCallback:Lcom/mediatek/engineermode/camera6589/Camera$AutoFocusCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraState(I)V

    return-void
.end method

.method protected canTakePicture()Z
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->isCameraIdle()Z

    move-result v0

    const-string v1, "test/camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "canTakePicture() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method public cancelAutoFocus()V
    .locals 3

    const/4 v2, 0x4

    const-string v0, "test/camera"

    const-string v1, "cancelAutoFocus"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraState(I)V

    :cond_0
    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraParameters(I)V

    return-void
.end method

.method public checkCameraState()Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "test/camera"

    const-string v2, "Check camera state in ModeActor"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/camera6589/FocusManager;->isFocusingSnapOnFinish()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSnapshotOnIdle:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPopupGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPopupGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected doOnResume()V
    .locals 5

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOpenCameraFail:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraDisabled:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    const-string v1, "test/camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doOnResume() Camera State = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    if-nez v1, :cond_2

    :try_start_0
    const-string v1, "client.appmode"

    const-string v2, "MtkEng"

    invoke-static {v1, v2}, Landroid/hardware/Camera;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    invoke-static {p0, v1}, Lcom/mediatek/engineermode/camera6589/Util;->openCamera(Landroid/app/Activity;I)Landroid/hardware/Camera;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->initializeCapabilities()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->startPreview()V
    :try_end_0
    .catch Lcom/mediatek/engineermode/camera6589/CameraHardwareException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mediatek/engineermode/camera6589/CameraDisabledException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFirstTimeInitialized:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->keepScreenOnAwhile()V

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOnResumeTime:J

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_4
    const-string v1, "test/camera"

    const-string v2, "doOnresume end"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const v1, 0x7f0804f7

    invoke-static {p0, v1}, Lcom/mediatek/engineermode/camera6589/Util;->showErrorAndFinish(Landroid/app/Activity;I)V

    goto :goto_0

    :catch_1
    move-exception v0

    const v1, 0x7f080502

    invoke-static {p0, v1}, Lcom/mediatek/engineermode/camera6589/Util;->showErrorAndFinish(Landroid/app/Activity;I)V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->initializeSecondTime()V

    goto :goto_1
.end method

.method public getCameraId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    return v0
.end method

.method protected getCameraParameters()Landroid/hardware/Camera$Parameters;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientation:I

    return v0
.end method

.method public getPictureRemaining()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    return-wide v0
.end method

.method public getPreviewFrameLayout()Lcom/mediatek/engineermode/camera6589/PreviewFrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPreviewFrameLayout:Lcom/mediatek/engineermode/camera6589/PreviewFrameLayout;

    return-object v0
.end method

.method public getRemainPictures()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPicturesRemaining:J

    return-wide v0
.end method

.method isSupportSize(Ljava/util/List;Landroid/hardware/Camera$Size;)Z
    .locals 4
    .param p2    # Landroid/hardware/Camera$Size;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "Landroid/hardware/Camera$Size;",
            ")Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    iget v2, v1, Landroid/hardware/Camera$Size;->width:I

    iget v3, p2, Landroid/hardware/Camera$Size;->width:I

    if-ne v2, v3, :cond_0

    iget v2, v1, Landroid/hardware/Camera$Size;->height:I

    iget v3, p2, Landroid/hardware/Camera$Size;->height:I

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onAutoFocusDone()V
    .locals 3

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    if-eqz v1, :cond_2

    :cond_0
    const-string v1, "test/camera"

    const-string v2, "onAutoFocusDone() return1"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "test/camera"

    const-string v2, "onAutoFocusDone()"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    if-nez v1, :cond_3

    new-instance v0, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/camera6589/Camera$AfCaptureThread;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setRawDumpFlag(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->isCameraIdle()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x0

    invoke-super {p0, p1}, Lcom/mediatek/engineermode/camera6589/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    const-string v4, "test/camera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate Bundle = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    iput v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraId:I

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f06005c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-direct {v4, v0}, Lcom/mediatek/engineermode/camera6589/FocusManager;-><init>([Ljava/lang/String;)V

    iput-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraOpenThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    const v4, 0x7f030053

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setContentView(I)V

    const v4, 0x7f0b00c1

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    const/4 v4, 0x3

    invoke-interface {v2, v4}, Landroid/view/SurfaceHolder;->setType(I)V

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraOpenThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->join()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraOpenThread:Ljava/lang/Thread;

    iget-boolean v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOpenCameraFail:Z

    if-eqz v4, :cond_0

    const v4, 0x7f0804f7

    invoke-static {p0, v4}, Lcom/mediatek/engineermode/camera6589/Util;->showErrorAndFinish(Landroid/app/Activity;I)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraDisabled:Z

    if-eqz v4, :cond_1

    const v4, 0x7f080502

    invoke-static {p0, v4}, Lcom/mediatek/engineermode/camera6589/Util;->showErrorAndFinish(Landroid/app/Activity;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "test/camera"

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->getSettingsFromPref()V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    const v4, 0x7f0b00c3

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    monitor-enter v5

    :try_start_1
    iget-boolean v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSettingRetrieved:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->join()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_2
    iput-object v7, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraPreviewThread:Ljava/lang/Thread;

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_4
    const-string v4, "test/camera"

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    :catch_2
    move-exception v1

    const-string v4, "test/camera"

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/mediatek/engineermode/camera6589/ActivityBase;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "test/camera"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSnapshotOnIdle:Z

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->stopPreview()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->closeCamera()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->resetScreenOn()V

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFirstTimeInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mOrientationListener:Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDidRegister:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDidRegister:Z

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mJpegImageData:[B

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->removeMessages()V

    sput-boolean v2, Lcom/mediatek/engineermode/camera6589/Camera;->sIsAutoFocusCallback:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iput v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mAfCaptureTimes:I

    iput v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mTripleCount:I

    iput v2, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mBracketTimes:I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mIsBracketAddPos:Z

    :cond_2
    invoke-super {p0}, Lcom/mediatek/engineermode/camera6589/ActivityBase;->onPause()V

    return-void
.end method

.method public onShutterButtonFocus(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->canTakePicture()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    const-string v0, "test/camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onShutterButtonFocus pressed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->onShutterDown()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->onShutterUp()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFirstTimeInitialized:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    :cond_0
    const-string v0, "test/camera"

    const-string v1, "onTouch return1"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mShutterButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    const-string v1, "test/camera"

    const-string v2, "mCaptureBtn key up!"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x67

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6a

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCaptureMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->keepScreenOnAwhile()V

    return-void
.end method

.method public resumePreview()V
    .locals 2

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->startPreview()V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->checkStorage()V

    return-void
.end method

.method public setFocusParameters()V
    .locals 2

    const-string v0, "test/camera"

    const-string v1, "setFocusParameters()"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/camera6589/Camera;->setCameraParameters(I)V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "test/camera"

    const-string v1, "holder.getSurface() == null"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "test/camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceChanged. w="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". h="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_2

    const-string v0, "test/camera"

    const-string v1, "surfaceChanged. holder == null"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mPausing:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mCameraState:I

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->startPreview()V

    :cond_3
    :goto_1
    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mFirstTimeInitialized:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lcom/mediatek/engineermode/camera6589/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v0

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mDisplayRotation:I

    if-eq v0, v1, :cond_5

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->setDisplayOrientation()V

    :cond_5
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->isCreating()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->initializeSecondTime()V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "test/camera"

    const-string v1, "surfaceCreated."

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/Camera;->stopPreview()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-void
.end method
