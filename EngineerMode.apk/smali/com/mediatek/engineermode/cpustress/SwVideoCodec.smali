.class public Lcom/mediatek/engineermode/cpustress/SwVideoCodec;
.super Lcom/mediatek/engineermode/cpustress/CpuStressCommon;
.source "SwVideoCodec.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final PERCENT:D = 100.0

.field public static final SWCODEC_MASK_CORE_0:I = 0x0

.field public static final SWCODEC_MASK_CORE_1:I = 0x1

.field public static final SWCODEC_MASK_CORE_2:I = 0x2

.field public static final SWCODEC_MASK_CORE_3:I = 0x3

.field private static final TAG:Ljava/lang/String; = "EM/CpuStress_SwVideoCodec"


# instance fields
.field private mBtnStart:Landroid/widget/Button;

.field private mEtIteration:Landroid/widget/EditText;

.field private mEtLoopCount:Landroid/widget/EditText;

.field private mTvArray:[Landroid/widget/TextView;

.field private mTvResult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvResult:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/cpustress/SwVideoCodec;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->updateTestResult()V

    return-void
.end method

.method private showTestResult(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-gt v0, v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    aget-object v3, v2, v0

    const/4 v2, 0x1

    shl-int/2addr v2, v0

    and-int/2addr v2, p2

    if-nez v2, :cond_0

    const v2, 0x7f080124

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x3

    goto :goto_0

    :cond_0
    const v2, 0x7f080123

    goto :goto_2

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateStartUI()V
    .locals 4

    const v3, 0x7f080122

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    const v2, 0x7f080158

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvResult:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateTestCount(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const-string v1, "EM/CpuStress_SwVideoCodec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter updateTestResult: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz p1, :cond_0

    const v1, 0x7f080159

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    int-to-double v4, p2

    const-wide/high16 v6, 0x4059000000000000L

    mul-double/2addr v4, v6

    int-to-double v6, p1

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvResult:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTestResult()V
    .locals 8

    const-string v0, "EM/CpuStress_SwVideoCodec"

    const-string v1, "Enter updateTestResult"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->getData()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_0

    const-string v0, "result_video_codec"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v0, "run"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v0, "loopcount"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v0, "iteration"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v0, "result"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->updateTestUi(ZJIII)V

    const-string v0, "result_pass_video_codec"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v6, v0}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->updateTestCount(II)V

    :cond_0
    return-void
.end method

.method private updateTestUi(ZJIII)V
    .locals 8
    .param p1    # Z
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v2, 0x1

    const v4, 0x7f080124

    const v5, 0x7f080123

    const/4 v3, 0x0

    const-string v1, "EM/CpuStress_SwVideoCodec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateTestUI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " 0x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    if-nez p1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    if-nez p1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setSelection(I)V

    if-lez p6, :cond_0

    const/4 v1, 0x2

    sget v6, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v1, v6, :cond_6

    sget v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_2
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    if-eqz p1, :cond_9

    const v1, 0x7f080158

    :goto_3
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    if-nez p1, :cond_a

    const/4 v0, 0x0

    :goto_4
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const v2, 0x7f080122

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    aget-object v2, v1, v3

    and-int/lit8 v1, p5, 0x1

    if-nez v1, :cond_3

    move v1, v4

    :goto_5
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_3
    move v1, v5

    goto :goto_5

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    aget-object v3, v1, v3

    and-int/lit8 v1, p5, 0x1

    if-nez v1, :cond_4

    move v1, v4

    :goto_6
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    aget-object v1, v1, v2

    and-int/lit8 v2, p5, 0x2

    if-nez v2, :cond_5

    :goto_7
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_4
    move v1, v5

    goto :goto_6

    :cond_5
    move v4, v5

    goto :goto_7

    :cond_6
    const/4 v1, 0x4

    sget v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v1, v2, :cond_7

    sget v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-direct {p0, v1, p5}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->showTestResult(II)V

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    aget-object v1, v1, v3

    and-int/lit8 v2, p5, 0x1

    if-nez v2, :cond_8

    :goto_8
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_8
    move v4, v5

    goto :goto_8

    :cond_9
    const v1, 0x7f080157

    goto :goto_3

    :cond_a
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-boolean v1, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopSwCodec:Z

    if-nez v1, :cond_b

    const/16 v1, 0x3e9

    invoke-virtual {p0, v1}, Landroid/app/Activity;->removeDialog(I)V

    :cond_b
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    if-ne v5, v6, :cond_1

    const-string v5, "EM/CpuStress_SwVideoCodec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is clicked"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080157

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-wide/16 v0, 0x0

    const/4 v3, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v5, "loopcount"

    invoke-virtual {v2, v5, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v5, "iteration"

    invoke-virtual {v2, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v5, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->startTest(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->updateStartUI()V

    :goto_0
    return-void

    :catch_0
    move-exception v4

    const-string v5, "EM/CpuStress_SwVideoCodec"

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v5, 0x7f08011e

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_0
    const/16 v5, 0x3e9

    invoke-virtual {p0, v5}, Landroid/app/Activity;->showDialog(I)V

    iget-object v5, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v5}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->stopTest()V

    goto :goto_0

    :cond_1
    const-string v5, "EM/CpuStress_SwVideoCodec"

    const-string v6, "Unknown event"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b01c6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    const v0, 0x7f0b01c0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtLoopCount:Landroid/widget/EditText;

    const v0, 0x7f0b01c1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mEtIteration:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    const/4 v2, 0x0

    const v0, 0x7f0b01c2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    const/4 v2, 0x1

    const v0, 0x7f0b01c3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    const/4 v2, 0x2

    const v0, 0x7f0b01c4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvArray:[Landroid/widget/TextView;

    const/4 v2, 0x3

    const v0, 0x7f0b01c5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    const v0, 0x7f0b01c7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mTvResult:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/cpustress/SwVideoCodec$1;-><init>(Lcom/mediatek/engineermode/cpustress/SwVideoCodec;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mHandler:Landroid/os/Handler;

    return-void
.end method
