.class public Lcom/mediatek/engineermode/UartUsbSwitch;
.super Landroid/app/Activity;
.source "UartUsbSwitch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/UartUsbSwitch$WorkerHandler;
    }
.end annotation


# static fields
.field private static final FAIL:Ljava/lang/String; = " fail"

.field private static final FILE_PORT_MODE:Ljava/lang/String; = "/sys/devices/platform/mt_usb/portmode"

.field private static final KEY_USB_PORT:Ljava/lang/String; = "mediatek.usb.port.mode"

.field private static final MODE_UART:Ljava/lang/String; = "1"

.field private static final MODE_USB:Ljava/lang/String; = "0"

.field private static final MSG_CHECK_RESULT:I = 0xb

.field private static final SUCCESS:Ljava/lang/String; = " success"

.field private static final TAG:Ljava/lang/String; = "EM/UartUsbSwitch"

.field private static final VAL_UART:Ljava/lang/String; = "uart"

.field private static final VAL_USB:Ljava/lang/String; = "usb"


# instance fields
.field private final mCheckListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private mModeVal:Ljava/lang/String;

.field private mRgMode:Landroid/widget/RadioGroup;

.field private mTvCurrent:Landroid/widget/TextView;

.field private mWorkerHandler:Lcom/mediatek/engineermode/UartUsbSwitch$WorkerHandler;

.field private mWorkerThread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerHandler:Lcom/mediatek/engineermode/UartUsbSwitch$WorkerHandler;

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerThread:Landroid/os/HandlerThread;

    new-instance v0, Lcom/mediatek/engineermode/UartUsbSwitch$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/UartUsbSwitch$1;-><init>(Lcom/mediatek/engineermode/UartUsbSwitch;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mCheckListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/UartUsbSwitch;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/UartUsbSwitch;

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mModeVal:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/UartUsbSwitch;Ljava/lang/String;I)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/UartUsbSwitch;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/UartUsbSwitch;->waitForState(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/UartUsbSwitch;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/UartUsbSwitch;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/UartUsbSwitch;->updateStatus(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/UartUsbSwitch;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/UartUsbSwitch;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/UartUsbSwitch;->doSwitch(Ljava/lang/Boolean;)V

    return-void
.end method

.method private doSwitch(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "usb"

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mModeVal:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mModeVal:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/UartUsbSwitch;->setUsbMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerHandler:Lcom/mediatek/engineermode/UartUsbSwitch$WorkerHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void

    :cond_1
    const-string v0, "uart"

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mModeVal:Ljava/lang/String;

    goto :goto_0
.end method

.method private getUsbMode()Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cat "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/sys/devices/platform/mt_usb/portmode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "EM/UartUsbSwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get current dramc cmd: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v3, "EM/UartUsbSwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get current dramc IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setUsbMode(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "EM/UartUsbSwitch"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setUsbMode(), value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "mediatek.usb.port.mode"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private updateStatus(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mTvCurrent:Landroid/widget/TextView;

    const v1, 0x7f080613

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mRgMode:Landroid/widget/RadioGroup;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mTvCurrent:Landroid/widget/TextView;

    const v1, 0x7f080611

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mRgMode:Landroid/widget/RadioGroup;

    const v1, 0x7f0b041b

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mTvCurrent:Landroid/widget/TextView;

    const v1, 0x7f080612

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mRgMode:Landroid/widget/RadioGroup;

    const v1, 0x7f0b041c

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0
.end method

.method private waitForState(Ljava/lang/String;I)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    div-int/lit8 v0, p2, 0x32

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    const-string v2, "mediatek.usb.port.mode"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_0
    const-wide/16 v2, 0x32

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/platform/mt_usb/portmode"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f080614

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "EM/UartUsbSwitch"

    const-string v1, "Port mode file not exist"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f030085

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b0419

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mTvCurrent:Landroid/widget/TextView;

    const v0, 0x7f0b041a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mRgMode:Landroid/widget/RadioGroup;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "EM/UartUsbSwitch"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Lcom/mediatek/engineermode/UartUsbSwitch$WorkerHandler;

    iget-object v1, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mediatek/engineermode/UartUsbSwitch$WorkerHandler;-><init>(Lcom/mediatek/engineermode/UartUsbSwitch;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerHandler:Lcom/mediatek/engineermode/UartUsbSwitch$WorkerHandler;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/UartUsbSwitch;->getUsbMode()Ljava/lang/String;

    move-result-object v0

    const-string v2, "EM/UartUsbSwitch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    const v2, 0x7f080615

    invoke-static {p0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/UartUsbSwitch;->updateStatus(Ljava/lang/Boolean;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mRgMode:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/mediatek/engineermode/UartUsbSwitch;->mCheckListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto :goto_0

    :cond_2
    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1
.end method
