.class public Lcom/mediatek/engineermode/bandselect/BandSelect;
.super Landroid/app/Activity;
.source "BandSelect.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;
    }
.end annotation


# static fields
.field private static final GSM_BASEBAND:Ljava/lang/String; = "gsm.baseband.capability"

.field private static final LOG_TAG:Ljava/lang/String; = "BandModeSim1"

.field private static final TDSCDMA:I = 0x8

.field private static final WCDMA:I = 0x4


# instance fields
.field private mBtnSet:Landroid/widget/Button;

.field private mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

.field private final mGsmModeArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;",
            ">;"
        }
    .end annotation
.end field

.field private mIsThisAlive:Z

.field private mPhoneProxey:Lcom/android/internal/telephony/Phone;

.field private final mResponseHander:Landroid/os/Handler;

.field private mSimType:I

.field private final mUmtsModeArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iput-object v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mPhoneProxey:Lcom/android/internal/telephony/Phone;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mIsThisAlive:Z

    new-instance v0, Lcom/mediatek/engineermode/bandselect/BandSelect$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/bandselect/BandSelect$1;-><init>(Lcom/mediatek/engineermode/bandselect/BandSelect;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mResponseHander:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/bandselect/BandSelect;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/bandselect/BandSelect;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mIsThisAlive:Z

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/bandselect/BandSelect;Landroid/os/AsyncResult;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bandselect/BandSelect;
    .param p1    # Landroid/os/AsyncResult;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/bandselect/BandSelect;->setMode(Landroid/os/AsyncResult;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/bandselect/BandSelect;II)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bandselect/BandSelect;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/bandselect/BandSelect;->setSupportedMode(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/bandselect/BandSelect;II)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/bandselect/BandSelect;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/bandselect/BandSelect;->setCurrentMode(II)V

    return-void
.end method

.method public static getModemType()Z
    .locals 5

    const-string v4, "gsm.baseband.capability"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    and-int/lit8 v4, v1, 0x8

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getValFromGsmBox()I
    .locals 5

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    iget-object v3, v2, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    iget v4, v2, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mBit:I

    shl-int/2addr v3, v4

    or-int/2addr v0, v3

    goto :goto_0

    :cond_1
    return v0
.end method

.method private getValFromUmtsBox()I
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    iget-object v3, v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    iget v4, v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mBit:I

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    goto :goto_0

    :cond_1
    return v2
.end method

.method private queryCurrentMode()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "AT+EPBSE?"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "+EPBSE:"

    aput-object v2, v0, v1

    const-string v1, "BandModeSim1"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AT String:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x65

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/bandselect/BandSelect;->sendATCommand([Ljava/lang/String;I)V

    return-void
.end method

.method private querySupportMode()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "AT+EPBSE=?"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "+EPBSE:"

    aput-object v2, v0, v1

    const-string v1, "BandModeSim1"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AT String:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x64

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/bandselect/BandSelect;->sendATCommand([Ljava/lang/String;I)V

    return-void
.end method

.method private sendATCommand([Ljava/lang/String;I)V
    .locals 3
    .param p1    # [Ljava/lang/String;
    .param p2    # I

    const/4 v2, 0x1

    iget v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mSimType:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v1, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mResponseHander:Landroid/os/Handler;

    invoke-virtual {v1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->invokeOemRilRequestStringsGemini([Ljava/lang/String;Landroid/os/Message;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v1, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mResponseHander:Landroid/os/Handler;

    invoke-virtual {v1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->invokeOemRilRequestStringsGemini([Ljava/lang/String;Landroid/os/Message;I)V

    goto :goto_0
.end method

.method private setBandMode(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x0

    move v0, p1

    move v2, p2

    const/16 v3, 0xff

    if-gt v0, v3, :cond_0

    const v3, 0xffff

    if-le v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_2

    const/16 v0, 0xff

    :cond_2
    if-nez v2, :cond_3

    const v2, 0xffff

    :cond_3
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AT+EPBSE="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v6

    const/4 v3, 0x1

    const-string v4, ""

    aput-object v4, v1, v3

    const-string v3, "BandModeSim1"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AT String:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x6e

    invoke-direct {p0, v1, v3}, Lcom/mediatek/engineermode/bandselect/BandSelect;->sendATCommand([Ljava/lang/String;I)V

    invoke-direct {p0, v0, v2}, Lcom/mediatek/engineermode/bandselect/BandSelect;->setCurrentMode(II)V

    goto :goto_0
.end method

.method private setCurrentMode(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    iget v3, v0, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mBit:I

    shl-int v3, v4, v3

    and-int/2addr v3, p1

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    iget v3, v2, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mBit:I

    shl-int v3, v4, v3

    and-int/2addr v3, p2

    if-nez v3, :cond_4

    iget-object v3, v2, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_4
    iget-object v3, v2, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v2, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_5
    return-void
.end method

.method private setMode(Landroid/os/AsyncResult;I)V
    .locals 12
    .param p1    # Landroid/os/AsyncResult;
    .param p2    # I

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v7, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v7, [Ljava/lang/String;

    move-object v4, v7

    check-cast v4, [Ljava/lang/String;

    move-object v0, v4

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v6, v0, v2

    const-string v7, "BandModeSim1"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "--.>"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "+EPBSE:"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, ","

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v7, v1, v10

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    if-eqz v7, :cond_0

    aget-object v7, v1, v11

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    if-eqz v7, :cond_0

    const/16 v7, 0x64

    if-ne p2, v7, :cond_1

    aget-object v7, v1, v10

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aget-object v8, v1, v11

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-direct {p0, v7, v8}, Lcom/mediatek/engineermode/bandselect/BandSelect;->setSupportedMode(II)V

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget-object v7, v1, v10

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aget-object v8, v1, v11

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-direct {p0, v7, v8}, Lcom/mediatek/engineermode/bandselect/BandSelect;->setCurrentMode(II)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private setSupportedMode(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    iget v2, v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mBit:I

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    iget-object v2, v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    iget v2, v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mBit:I

    shl-int v2, v3, v2

    and-int/2addr v2, p2

    if-nez v2, :cond_2

    iget-object v2, v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_2
    iget-object v2, v1, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/bandselect/BandSelect;->getValFromGsmBox()I

    move-result v0

    invoke-direct {p0}, Lcom/mediatek/engineermode/bandselect/BandSelect;->getValFromUmtsBox()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/bandselect/BandSelect;->setBandMode(II)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v9, 0x7

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "mSimType"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mSimType:I

    invoke-static {}, Lcom/mediatek/engineermode/bandselect/BandSelect;->getModemType()Z

    move-result v1

    iget v2, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mSimType:I

    if-ne v2, v6, :cond_1

    if-eqz v1, :cond_0

    const v2, 0x7f03007e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f0b0405

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mBtnSet:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b03fb

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v6}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b03fc

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v7}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b03fd

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v8}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b03fe

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v9}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b03ff

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0400

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v6}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0401

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const/4 v5, 0x2

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0402

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v7}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0403

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v8}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0404

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const/4 v5, 0x5

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iput-boolean v6, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mIsThisAlive:Z

    iget-object v2, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const v2, 0x7f03000c

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f0b0053

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mBtnSet:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0045

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v6}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0046

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v7}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0047

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v8}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0048

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v9}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0049

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b004a

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v6}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b004b

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const/4 v5, 0x2

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b004c

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v7}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b004d

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v8}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b004e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const/4 v5, 0x5

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b004f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const/4 v5, 0x6

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0050

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v9}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0051

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const/16 v5, 0x8

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mUmtsModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0052

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const/16 v5, 0x9

    invoke-direct {v4, v2, v5}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_1
    const v2, 0x7f03000a

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f0b0043

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mBtnSet:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b003f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v6}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0040

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v7}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0041

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v8}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGsmModeArray:Ljava/util/ArrayList;

    new-instance v4, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;

    const v2, 0x7f0b0042

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-direct {v4, v2, v9}, Lcom/mediatek/engineermode/bandselect/BandSelect$BandModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f08044d

    const v3, 0x7f08044a

    const/4 v2, 0x0

    const/16 v1, 0x64

    if-ne v1, p1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f08044b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    :cond_0
    const/16 v1, 0x65

    if-ne v1, p1, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f08044c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080192

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f08044e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f08044f

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080450

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080451

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080452

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mIsThisAlive:Z

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "BandModeSim1"

    const-string v1, "mGeminiPhone"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iput-object v0, p0, Lcom/mediatek/engineermode/bandselect/BandSelect;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-direct {p0}, Lcom/mediatek/engineermode/bandselect/BandSelect;->querySupportMode()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/bandselect/BandSelect;->queryCurrentMode()V

    return-void
.end method
