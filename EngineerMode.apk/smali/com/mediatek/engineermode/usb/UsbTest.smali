.class public Lcom/mediatek/engineermode/usb/UsbTest;
.super Landroid/app/Activity;
.source "UsbTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/usb/UsbTest$ResultCollectHandler;,
        Lcom/mediatek/engineermode/usb/UsbTest$TestHandler;
    }
.end annotation


# static fields
.field private static final A_UUT:I = 0x5

.field private static final BUTTONS_IDS_EX:[I

.field private static final BUTTONS_IDS_IF:[I

.field private static final B_UUT:I = 0x6

.field private static final DETECT_SRP:I = 0x3

.field private static final DETECT_VBUS:I = 0x4

.field private static final DLG_ERROR_MSG:I = 0x4

.field private static final DLG_MSG:I = 0x2

.field private static final DLG_STOP:I = 0x1

.field private static final DLG_UNKNOW_MSG:I = 0x3

.field private static final ENABLE_SRP:I = 0x2

.field private static final ENABLE_VBUS:I = 0x1

.field private static final ERROR_MSG:I = 0xd

.field private static final EX_COMMAND:[I

.field private static final GET_DESCRIPTOR:I = 0xc

.field private static final GET_MSG:I = 0x14

.field private static final IF_COMMOND:[I

.field private static final OP_FINISH:I = 0xb

.field private static final OP_IN_PROCESS:I = 0xa

.field private static final SET_FEATURE:I = 0xd

.field private static final START_TEST:I = 0x15

.field private static final SUSPEND_RESUME:I = 0xb

.field private static final TAG:Ljava/lang/String; = "USBTest"

.field private static final TD_5_9:I = 0xe

.field private static final TEST_J:I = 0x8

.field private static final TEST_K:I = 0x9

.field private static final TEST_PACKET:I = 0xa

.field private static final TEST_SE0_NAK:I = 0x7

.field private static final UPDATAT_MSG:I = 0xc


# instance fields
.field private mBtnAUutStart:Landroid/widget/Button;

.field private mBtnAUutStop:Landroid/widget/Button;

.field private mBtnBUutStart:Landroid/widget/Button;

.field private mBtnBUutStop:Landroid/widget/Button;

.field private mBtnBUutTD59:Landroid/widget/Button;

.field private mBtnDeSrpStart:Landroid/widget/Button;

.field private mBtnDeSrpStop:Landroid/widget/Button;

.field private mBtnDeVbusStart:Landroid/widget/Button;

.field private mBtnDeVbusStop:Landroid/widget/Button;

.field private mBtnEnSrpStart:Landroid/widget/Button;

.field private mBtnEnSrpStop:Landroid/widget/Button;

.field private mBtnEnVbusStart:Landroid/widget/Button;

.field private mBtnEnVbusStop:Landroid/widget/Button;

.field private mBtnIds:[I

.field private mBtnList:[Landroid/widget/Button;

.field private mCommand:I

.field private mMsg:I

.field private mResultCollectHandler:Lcom/mediatek/engineermode/usb/UsbTest$ResultCollectHandler;

.field private mResultCollectThread:Landroid/os/HandlerThread;

.field private mRun:Z

.field private mTestHandler:Lcom/mediatek/engineermode/usb/UsbTest$TestHandler;

.field private mTestIf:Z

.field private mTestThread:Landroid/os/HandlerThread;

.field private mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/engineermode/usb/UsbTest;->BUTTONS_IDS_IF:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mediatek/engineermode/usb/UsbTest;->BUTTONS_IDS_EX:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/mediatek/engineermode/usb/UsbTest;->IF_COMMOND:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/mediatek/engineermode/usb/UsbTest;->EX_COMMAND:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0b041e
        0x7f0b041f
        0x7f0b0420
        0x7f0b0421
        0x7f0b0422
        0x7f0b0423
        0x7f0b0424
        0x7f0b0425
        0x7f0b0426
        0x7f0b0427
        0x7f0b0428
        0x7f0b0429
        0x7f0b042a
    .end array-data

    :array_1
    .array-data 4
        0x7f0b042b
        0x7f0b042c
        0x7f0b042d
        0x7f0b042e
        0x7f0b042f
        0x7f0b0430
        0x7f0b0431
        0x7f0b0432
        0x7f0b0433
        0x7f0b0434
        0x7f0b0435
        0x7f0b0436
        0x7f0b0437
        0x7f0b0438
    .end array-data

    :array_2
    .array-data 4
        0x1
        0x4
        0x2
        0x3
        0x5
        0x6
    .end array-data

    :array_3
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mResultCollectThread:Landroid/os/HandlerThread;

    iput-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mResultCollectHandler:Lcom/mediatek/engineermode/usb/UsbTest$ResultCollectHandler;

    iput-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestThread:Landroid/os/HandlerThread;

    iput-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestHandler:Lcom/mediatek/engineermode/usb/UsbTest$TestHandler;

    iput v1, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mCommand:I

    iput v1, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mMsg:I

    iput-boolean v1, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mRun:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestIf:Z

    new-instance v0, Lcom/mediatek/engineermode/usb/UsbTest$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/usb/UsbTest$1;-><init>(Lcom/mediatek/engineermode/usb/UsbTest;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/usb/UsbTest;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/usb/UsbTest;

    iget v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mMsg:I

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/usb/UsbTest;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/usb/UsbTest;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mMsg:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/usb/UsbTest;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/usb/UsbTest;

    iget v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mCommand:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/usb/UsbTest;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/usb/UsbTest;

    iget-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/usb/UsbTest;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/usb/UsbTest;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mRun:Z

    return v0
.end method

.method private findBtnIndex(I)I
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnIds:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnIds:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_0

    const-string v1, "USBTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "find btn index: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "USBTest"

    const-string v2, "find btn index error"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    goto :goto_1
.end method

.method private makeOneBtnEnable(Landroid/widget/Button;)V
    .locals 5
    .param p1    # Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    if-ne v1, p1, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private updateAllBtn(Z)V
    .locals 4
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sget-object v4, Lcom/mediatek/engineermode/usb/UsbTest;->BUTTONS_IDS_IF:[I

    sget-object v5, Lcom/mediatek/engineermode/usb/UsbTest;->BUTTONS_IDS_IF:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    if-ne v3, v4, :cond_2

    const/16 v3, 0xe

    iput v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mCommand:I

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x3

    aget-object v2, v3, v4

    :cond_0
    :goto_0
    const-string v3, "USBTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSTART--"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "USBTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "command--"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mCommand:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_4

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/usb/UsbTest;->makeOneBtnEnable(Landroid/widget/Button;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestHandler:Lcom/mediatek/engineermode/usb/UsbTest$TestHandler;

    const/16 v4, 0x15

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iput-boolean v7, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mRun:Z

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mResultCollectHandler:Lcom/mediatek/engineermode/usb/UsbTest$ResultCollectHandler;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/usb/UsbTest;->findBtnIndex(I)I

    move-result v0

    iget-boolean v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestIf:Z

    if-eqz v3, :cond_3

    sget-object v3, Lcom/mediatek/engineermode/usb/UsbTest;->IF_COMMOND:[I

    div-int/lit8 v4, v0, 0x2

    aget v3, v3, v4

    :goto_2
    iput v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mCommand:I

    const/4 v1, 0x0

    rem-int/lit8 v3, v0, 0x2

    if-nez v3, :cond_0

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    add-int/lit8 v4, v0, 0x1

    aget-object v2, v3, v4

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/mediatek/engineermode/usb/UsbTest;->EX_COMMAND:[I

    div-int/lit8 v4, v0, 0x2

    aget v3, v3, v4

    goto :goto_2

    :cond_4
    iget-boolean v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mRun:Z

    if-eqz v3, :cond_1

    iput-boolean v6, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mRun:Z

    iget v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mCommand:I

    invoke-static {v3}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeStopTest(I)Z

    move-result v3

    if-nez v3, :cond_5

    const v3, 0x7f08037e

    invoke-static {p0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-static {}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeCleanMsg()Z

    :cond_5
    invoke-direct {p0, v7}, Lcom/mediatek/engineermode/usb/UsbTest;->updateAllBtn(Z)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "if_test"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestIf:Z

    :cond_0
    const-string v2, "USBTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "is test IF ? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestIf:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestIf:Z

    if-eqz v2, :cond_1

    const v2, 0x7f030087

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    sget-object v2, Lcom/mediatek/engineermode/usb/UsbTest;->BUTTONS_IDS_IF:[I

    array-length v2, v2

    new-array v2, v2, [Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    sget-object v2, Lcom/mediatek/engineermode/usb/UsbTest;->BUTTONS_IDS_IF:[I

    iput-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnIds:[I

    :goto_0
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnIds:[I

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnIds:[I

    aget v2, v2, v1

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    aput-object v2, v3, v1

    iget-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    aget-object v2, v2, v1

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const v2, 0x7f080378

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setTitle(I)V

    const v2, 0x7f030088

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    sget-object v2, Lcom/mediatek/engineermode/usb/UsbTest;->BUTTONS_IDS_EX:[I

    array-length v2, v2

    new-array v2, v2, [Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnList:[Landroid/widget/Button;

    sget-object v2, Lcom/mediatek/engineermode/usb/UsbTest;->BUTTONS_IDS_EX:[I

    iput-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mBtnIds:[I

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeInit()Z

    move-result v2

    if-nez v2, :cond_3

    const v2, 0x7f08037d

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_3
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "USBTest/ResultCollect"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mResultCollectThread:Landroid/os/HandlerThread;

    iget-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mResultCollectThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    new-instance v2, Lcom/mediatek/engineermode/usb/UsbTest$ResultCollectHandler;

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mResultCollectThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/usb/UsbTest$ResultCollectHandler;-><init>(Lcom/mediatek/engineermode/usb/UsbTest;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mResultCollectHandler:Lcom/mediatek/engineermode/usb/UsbTest$ResultCollectHandler;

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "USBTest/Test"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestThread:Landroid/os/HandlerThread;

    iget-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    new-instance v2, Lcom/mediatek/engineermode/usb/UsbTest$TestHandler;

    iget-object v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/usb/UsbTest$TestHandler;-><init>(Lcom/mediatek/engineermode/usb/UsbTest;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestHandler:Lcom/mediatek/engineermode/usb/UsbTest$TestHandler;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    const v5, 0x7f08037c

    const v4, 0x7f080284

    const/4 v3, 0x0

    const-string v1, "USBTest"

    const-string v2, "-->onCreateDialog"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v6, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080377

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f08038a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/engineermode/usb/UsbDriver;->MSG:[Ljava/lang/String;

    iget v3, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mMsg:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/usb/UsbTest$2;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/usb/UsbTest$2;-><init>(Lcom/mediatek/engineermode/usb/UsbTest;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mMsg:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/usb/UsbTest$3;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/usb/UsbTest$3;-><init>(Lcom/mediatek/engineermode/usb/UsbTest;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080379

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/engineermode/usb/UsbTest$4;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/usb/UsbTest$4;-><init>(Lcom/mediatek/engineermode/usb/UsbTest;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "USBTest"

    const-string v1, "-->onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mRun:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mRun:Z

    iget v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mCommand:I

    invoke-static {v0}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeStopTest(I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "USBTest"

    const-string v1, "onDestroy() nativeStopTest fail"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeCleanMsg()Z

    :cond_0
    invoke-static {}, Lcom/mediatek/engineermode/usb/UsbDriver;->nativeDeInit()V

    iget-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mResultCollectThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/mediatek/engineermode/usb/UsbTest;->mTestThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
