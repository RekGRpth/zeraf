.class public Lcom/mediatek/engineermode/mcdi/McdiSetting;
.super Landroid/app/Activity;
.source "McdiSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final CAT:Ljava/lang/String; = "cat"

.field private static final ECHO:Ljava/lang/String; = "echo"

.field private static final MCDI_MODE_TAG:Ljava/lang/String; = "mcdi_mode"

.field private static final MODE_FS:Ljava/lang/String; = "/proc/spm_fs/mcdi_mode"

.field private static final SETTING_FS:Ljava/lang/String; = "/proc/spm_fs/mcdi"

.field private static final TAG:Ljava/lang/String; = "McdiSetting"

.field private static final TIMER_FS:Ljava/lang/String; = "/proc/spm_fs/mcdi_timer"

.field private static final TIMER_VAL_TAG:Ljava/lang/String; = "timer_val_ms"


# instance fields
.field private mBtnGetMcdiSetting:Landroid/widget/Button;

.field private mBtnStartTimer:Landroid/widget/Button;

.field private mEditTimerVal:Landroid/widget/EditText;

.field private mMcdiMode:I

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRBDisableMcdi:Landroid/widget/RadioButton;

.field private mRBDisableTimer:Landroid/widget/RadioButton;

.field private mRBMcdiOnly:Landroid/widget/RadioButton;

.field private mRBMcdiSodi:Landroid/widget/RadioButton;

.field private mRBModeArray:[Landroid/widget/RadioButton;

.field private mRBSetTimer:Landroid/widget/RadioButton;

.field private mRBTimerArray:[Landroid/widget/RadioButton;

.field private mSetTimerControler:Landroid/widget/LinearLayout;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V
    .locals 3
    .param p1    # [Landroid/widget/RadioButton;
    .param p2    # Landroid/widget/RadioButton;
    .param p3    # Z

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    aget-object v1, p1, v0

    if-ne p2, v1, :cond_0

    aget-object v1, p1, v0

    invoke-virtual {v1, p3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    aget-object v2, p1, v0

    if-nez p3, :cond_1

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    return-void
.end method

.method private execCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, -0x1

    const-string v3, "McdiSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[cmd]:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {p1}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    if-nez v2, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v1

    const-string v3, "McdiSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[output]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    const-string v3, "McdiSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private handleSetTimer(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBTimerArray:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableTimer:Landroid/widget/RadioButton;

    invoke-direct {p0, v0, v1, v2}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mBtnStartTimer:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    if-ne v2, p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBTimerArray:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBSetTimer:Landroid/widget/RadioButton;

    invoke-direct {p0, v0, v1, v2}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mBtnStartTimer:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const-string v0, "McdiSetting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initUIByData()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v3, "McdiSetting"

    const-string v4, "initUIByData()"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "cat /proc/spm_fs/mcdi_mode"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v3, "Feature Fail or Don\'t Support!"

    invoke-static {p0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    iput v3, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mMcdiMode:I

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mMcdiMode:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    :try_start_1
    iget-object v3, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBModeArray:[Landroid/widget/RadioButton;

    iget v4, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mMcdiMode:I

    aget-object v3, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    const-string v0, "cat /proc/spm_fs/mcdi_timer"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v3, "Feature Fail or Don\'t Support!"

    invoke-static {p0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "McdiSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NumberFormatException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v3, "McdiSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mMcdiMode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mMcdiMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IndexOutOfBoundsException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_1
    const-string v3, "0"

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->handleSetTimer(I)V

    goto/16 :goto_0

    :cond_2
    invoke-direct {p0, v7}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->handleSetTimer(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private setMcdiMode(I)V
    .locals 3
    .param p1    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "echo \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mcdi_mode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/proc/spm_fs/mcdi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method private setTimerVal(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "echo \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timer_val_ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/proc/spm_fs/mcdi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method private showDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private validateSetting()Z
    .locals 6

    const v5, 0x7f0805be

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableMcdi:Landroid/widget/RadioButton;

    invoke-virtual {v4}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f0805b5

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/16 v4, 0xf

    if-lt v2, v4, :cond_1

    const v4, 0xf4240

    if-gt v2, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private visibleSetTimerUI(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mSetTimerControler:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mBtnStartTimer:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v1, "McdiSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown view id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBModeArray:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableMcdi:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->visibleSetTimerUI(Z)V

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->setMcdiMode(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :sswitch_1
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBModeArray:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBMcdiOnly:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->visibleSetTimerUI(Z)V

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->setMcdiMode(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0

    :sswitch_2
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBModeArray:[Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBMcdiSodi:Landroid/widget/RadioButton;

    invoke-direct {p0, v1, v2, v3}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->checkOneRadio([Landroid/widget/RadioButton;Landroid/widget/RadioButton;Z)V

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->visibleSetTimerUI(Z)V

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->setMcdiMode(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0

    :sswitch_3
    if-eqz p2, :cond_0

    const-string v1, "0"

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->setTimerVal(Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->handleSetTimer(I)V

    goto :goto_0

    :sswitch_4
    if-eqz p2, :cond_0

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->handleSetTimer(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0119 -> :sswitch_4
        0x7f0b026d -> :sswitch_0
        0x7f0b026e -> :sswitch_1
        0x7f0b026f -> :sswitch_2
        0x7f0b0271 -> :sswitch_3
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v4, "McdiSetting"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unknown view id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->validateSetting()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mEditTimerVal:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->setTimerVal(Ljava/lang/String;)V

    const v4, 0x7f0805bd

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_1
    const-string v0, "cat /proc/spm_fs/mcdi"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "MCDI Setting"

    invoke-direct {p0, v4, v3}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0273
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030049

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b026d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableMcdi:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableMcdi:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b026e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBMcdiOnly:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBMcdiOnly:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b026f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBMcdiSodi:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBMcdiSodi:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b0271

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableTimer:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableTimer:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b0119

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBSetTimer:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBSetTimer:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0b0273

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mBtnStartTimer:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mBtnStartTimer:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0274

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mBtnGetMcdiSetting:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mBtnGetMcdiSetting:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0272

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mEditTimerVal:Landroid/widget/EditText;

    const v0, 0x7f0b0270

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mSetTimerControler:Landroid/widget/LinearLayout;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBModeArray:[Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBModeArray:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableMcdi:Landroid/widget/RadioButton;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBModeArray:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBMcdiOnly:Landroid/widget/RadioButton;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBModeArray:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBMcdiSodi:Landroid/widget/RadioButton;

    aput-object v1, v0, v4

    new-array v0, v4, [Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBTimerArray:[Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBTimerArray:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBDisableTimer:Landroid/widget/RadioButton;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBTimerArray:[Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mRBSetTimer:Landroid/widget/RadioButton;

    aput-object v1, v0, v3

    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mPowerManager:Landroid/os/PowerManager;

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mPowerManager:Landroid/os/PowerManager;

    const/16 v1, 0x1a

    const-string v2, "McdiSetting"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-direct {p0}, Lcom/mediatek/engineermode/mcdi/McdiSetting;->initUIByData()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/mcdi/McdiSetting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
