.class public abstract Lcom/leff/midi/event/MidiEvent;
.super Ljava/lang/Object;
.source "MidiEvent.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/leff/midi/event/MidiEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static sChannel:I

.field private static sId:I

.field private static sType:I


# instance fields
.field protected mDelta:Lcom/leff/midi/util/VariableLengthInt;

.field protected mTick:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/leff/midi/event/MidiEvent;->sId:I

    sput v0, Lcom/leff/midi/event/MidiEvent;->sType:I

    sput v0, Lcom/leff/midi/event/MidiEvent;->sChannel:I

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 2
    .param p1    # J
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/leff/midi/event/MidiEvent;->mTick:J

    new-instance v0, Lcom/leff/midi/util/VariableLengthInt;

    long-to-int v1, p3

    invoke-direct {v0, v1}, Lcom/leff/midi/util/VariableLengthInt;-><init>(I)V

    iput-object v0, p0, Lcom/leff/midi/event/MidiEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    return-void
.end method


# virtual methods
.method public getDelta()J
    .locals 2

    iget-object v0, p0, Lcom/leff/midi/event/MidiEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v0}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method protected abstract getEventSize()I
.end method

.method public getSize()I
    .locals 2

    invoke-virtual {p0}, Lcom/leff/midi/event/MidiEvent;->getEventSize()I

    move-result v0

    iget-object v1, p0, Lcom/leff/midi/event/MidiEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v1}, Lcom/leff/midi/util/VariableLengthInt;->getByteCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getTick()J
    .locals 2

    iget-wide v0, p0, Lcom/leff/midi/event/MidiEvent;->mTick:J

    return-wide v0
.end method

.method public requiresStatusByte(Lcom/leff/midi/event/MidiEvent;)Z
    .locals 3
    .param p1    # Lcom/leff/midi/event/MidiEvent;

    const/4 v0, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p0, Lcom/leff/midi/event/meta/MetaEvent;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDelta(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/leff/midi/event/MidiEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Lcom/leff/midi/util/VariableLengthInt;->setValue(I)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/leff/midi/event/MidiEvent;->mTick:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/leff/midi/event/MidiEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v1}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToFile(Ljava/io/OutputStream;Z)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/leff/midi/event/MidiEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v0}, Lcom/leff/midi/util/VariableLengthInt;->getBytes()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method
