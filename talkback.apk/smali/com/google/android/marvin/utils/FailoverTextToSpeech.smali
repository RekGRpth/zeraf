.class public Lcom/google/android/marvin/utils/FailoverTextToSpeech;
.super Ljava/lang/Object;
.source "FailoverTextToSpeech.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;,
        Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;,
        Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentPitch:F

.field private mCurrentRate:F

.field private mDefaultPitch:F

.field private mDefaultRate:F

.field private mDefaultTtsEngine:Ljava/lang/String;

.field private final mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

.field private final mInstalledTtsEngines:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

.field private final mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

.field private final mPitchObserver:Landroid/database/ContentObserver;

.field private final mRateObserver:Landroid/database/ContentObserver;

.field private final mResolver:Landroid/content/ContentResolver;

.field private final mSynthObserver:Landroid/database/ContentObserver;

.field private mSystemTtsEngine:Ljava/lang/String;

.field private mTempTts:Landroid/speech/tts/TextToSpeech;

.field private mTempTtsEngine:Ljava/lang/String;

.field private mTts:Landroid/speech/tts/TextToSpeech;

.field private final mTtsChangeListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

.field private mTtsEngine:Ljava/lang/String;

.field private mTtsFailures:I

.field private final mTtsListener:Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/high16 v4, 0x3f800000

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    iput v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentRate:F

    iput v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentPitch:F

    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$1;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$1;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSynthObserver:Landroid/database/ContentObserver;

    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$2;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$2;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mPitchObserver:Landroid/database/ContentObserver;

    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$3;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$3;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mRateObserver:Landroid/database/ContentObserver;

    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$4;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$4;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsListener:Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;

    new-instance v3, Lcom/google/android/marvin/utils/FailoverTextToSpeech$5;

    invoke-direct {v3, p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$5;-><init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsChangeListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    invoke-virtual {v5}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->getFilter()Landroid/content/IntentFilter;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v3, "tts_default_synth"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "tts_default_pitch"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v3, "tts_default_rate"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSynthObserver:Landroid/database/ContentObserver;

    invoke-virtual {v3, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mPitchObserver:Landroid/database/ContentObserver;

    invoke-virtual {v3, v0, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mRateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v3, v1, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultPitch()V

    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultRate()V

    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultEngine()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultEngine()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultPitch()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-direct {p0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->updateDefaultRate()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;
    .locals 1
    .param p0    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/utils/FailoverTextToSpeech;I)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleTtsInitialized(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleUtteranceCompleted(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/marvin/utils/FailoverTextToSpeech;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/utils/FailoverTextToSpeech;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->handleMediaStateChanged(Ljava/lang/String;)V

    return-void
.end method

.method private attemptTtsFailover(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-class v1, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x6

    const-string v3, "Attempting TTS failover from %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-static {v1, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v5, :cond_0

    iget v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    :cond_0
    invoke-direct {p0, p1, v6}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, v5}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private handleMediaStateChanged(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemTtsEngine:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Saw media unmount"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v4, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemTtsEngine:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    :cond_0
    const-string v0, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultTtsEngine:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Saw media mount"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v4, v0, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultTtsEngine:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method private handleTtsInitialized(I)V
    .locals 8
    .param p1    # I

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    if-nez v5, :cond_1

    const/4 v3, 0x6

    const-string v5, "Attempted to initialize TTS more than once!"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v3, v5, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    iput-object v6, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    if-eqz p1, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->attemptTtsFailover(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v5, :cond_4

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v5}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->attemptTtsShutdown(Landroid/speech/tts/TextToSpeech;)V

    :cond_3
    iput-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v6, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsListener:Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;

    invoke-virtual {v5, v6}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    iput-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    const-class v5, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v6, 0x4

    const-string v7, "Switched to TTS engine: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-static {v5, v6, v7, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    invoke-interface {v3, v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;->onTtsInitialized(Z)V

    goto :goto_0

    :cond_4
    move v0, v4

    goto :goto_1
.end method

.method private handleUtteranceCompleted(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;->onUtteranceCompleted(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method private setEngineByPackageName_GB_HC(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iput-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/compat/speech/tts/TextToSpeechCompatUtils;->setEngineByPackageName(Landroid/speech/tts/TextToSpeech;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsChangeListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    invoke-interface {v1, v0}, Landroid/speech/tts/TextToSpeech$OnInitListener;->onInit(I)V

    return-void
.end method

.method private setTtsEngine(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    iput v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsFailures:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v0}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->attemptTtsShutdown(Landroid/speech/tts/TextToSpeech;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    const-class v0, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v1, 0x6

    const-string v2, "Can\'t start TTS engine %s while still loading previous engine"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    const-class v0, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v1, 0x4

    const-string v2, "Switching to TTS engine: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-le v0, v1, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setEngineByPackageName_GB_HC(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTtsEngine:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsChangeListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    invoke-static {v0, v1, p1}, Lcom/googlecode/eyesfree/compat/speech/tts/TextToSpeechCompatUtils;->newTextToSpeech(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    goto :goto_0
.end method

.method private static splitUtteranceIntoSpeakableStrings(Ljava/lang/CharSequence;Ljava/util/LinkedList;)V
    .locals 5
    .param p0    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    add-int/lit16 v1, v3, 0xf9f

    const/16 v4, 0x20

    invoke-static {p0, v4, v3, v1}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;CII)I

    move-result v2

    if-gez v2, :cond_0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_0
    invoke-static {p0, v3, v2}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private trySpeak(Ljava/lang/String;FFLjava/util/HashMap;)I
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # F
    .param p3    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "FF",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    const/4 v10, 0x3

    const/4 v7, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-nez v5, :cond_1

    const/4 v1, -0x1

    :cond_0
    return v1

    :cond_1
    iget v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentPitch:F

    cmpl-float v5, v5, p2

    if-nez v5, :cond_2

    iget v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentRate:F

    cmpl-float v5, v5, p3

    if-eqz v5, :cond_3

    :cond_2
    iput p2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentPitch:F

    iput p3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mCurrentRate:F

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v5}, Landroid/speech/tts/TextToSpeech;->stop()I

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget v6, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultPitch:F

    mul-float/2addr v6, p2

    invoke-virtual {v5, v6}, Landroid/speech/tts/TextToSpeech;->setPitch(F)I

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget v6, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultRate:F

    mul-float/2addr v6, p3

    invoke-virtual {v5, v6}, Landroid/speech/tts/TextToSpeech;->setSpeechRate(F)I

    :cond_3
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0xf9f

    if-le v5, v6, :cond_4

    invoke-static {p1, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->splitUtteranceIntoSpeakableStrings(Ljava/lang/CharSequence;Ljava/util/LinkedList;)V

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    :cond_4
    const-string v5, "utteranceId"

    invoke-virtual {p4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v5, p1, v7, p4}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    move-result v1

    const-string v5, "Speak call for \"%s\" returned %d"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {p0, v10, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    if-nez v1, :cond_0

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v5, "Queue overflow speech: \"%s\""

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v2, v6, v9

    invoke-static {p0, v10, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v5, v2, v8, p4}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_0
.end method

.method private updateDefaultEngine()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mInstalledTtsEngines:Ljava/util/LinkedList;

    invoke-static {v1, v2}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->reloadInstalledTtsEngines(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSystemTtsEngine:Ljava/lang/String;

    const-string v1, "tts_default_synth"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultTtsEngine:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultTtsEngine:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setTtsEngine(Ljava/lang/String;Z)V

    return-void
.end method

.method private updateDefaultPitch()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "tts_default_pitch"

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultPitch:F

    return-void
.end method

.method private updateDefaultRate()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "tts_default_rate"

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mDefaultRate:F

    return-void
.end method


# virtual methods
.method public getEngineLabel()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->getLabelForEngine(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    return-void
.end method

.method public shutdown()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mMediaStateMonitor:Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mSynthObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mPitchObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mRateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v0}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->attemptTtsShutdown(Landroid/speech/tts/TextToSpeech;)V

    iput-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {v0}, Lcom/google/android/marvin/utils/TextToSpeechUtils;->attemptTtsShutdown(Landroid/speech/tts/TextToSpeech;)V

    iput-object v2, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTempTts:Landroid/speech/tts/TextToSpeech;

    return-void
.end method

.method public speak(Ljava/lang/String;FFLjava/util/HashMap;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # F
    .param p3    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "FF",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x5

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    const-string v3, "utteranceId"

    invoke-virtual {p4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->onUtteranceCompleted(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->trySpeak(Ljava/lang/String;FFLjava/util/HashMap;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    iget-object v3, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTtsEngine:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->attemptTtsFailover(Ljava/lang/String;)V

    :cond_2
    if-eqz v2, :cond_0

    const-string v3, "utteranceId"

    invoke-virtual {p4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_3

    const-string v3, "Failed to speak \"%s\" due to an exception"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {p0, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_2
    iget-object v4, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    const-string v3, "utteranceId"

    invoke-virtual {p4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->onUtteranceCompleted(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    const/4 v2, -0x1

    goto :goto_1

    :cond_3
    const-string v3, "Failed to speak \"%s\""

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {p0, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public stopAll()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mTts:Landroid/speech/tts/TextToSpeech;

    const-string v1, ""

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
