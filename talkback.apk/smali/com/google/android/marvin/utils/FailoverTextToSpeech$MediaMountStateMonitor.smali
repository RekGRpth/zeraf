.class Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;
.super Landroid/content/BroadcastReceiver;
.source "FailoverTextToSpeech.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/utils/FailoverTextToSpeech;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaMountStateMonitor"
.end annotation


# instance fields
.field private final mMediaIntentFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/google/android/marvin/utils/FailoverTextToSpeech;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->this$0:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->mMediaIntentFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->mMediaIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->mMediaIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->mMediaIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->mMediaIntentFilter:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/utils/FailoverTextToSpeech$MediaMountStateMonitor;->this$0:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    # getter for: Lcom/google/android/marvin/utils/FailoverTextToSpeech;->mHandler:Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;
    invoke-static {v1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->access$300(Lcom/google/android/marvin/utils/FailoverTextToSpeech;)Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech$SpeechHandler;->onMediaStateChanged(Ljava/lang/String;)V

    return-void
.end method
