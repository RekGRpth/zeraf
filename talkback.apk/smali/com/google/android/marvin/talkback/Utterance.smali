.class public Lcom/google/android/marvin/talkback/Utterance;
.super Ljava/lang/Object;
.source "Utterance.java"


# static fields
.field private static sPool:Lcom/google/android/marvin/talkback/Utterance;

.field private static final sPoolLock:Ljava/lang/Object;

.field private static sPoolSize:I


# instance fields
.field private final mCustomEarcons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mCustomVibrations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mEarcons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsInPool:Z

.field private final mMetadata:Landroid/os/Bundle;

.field private mNext:Lcom/google/android/marvin/talkback/Utterance;

.field private mReferenceCount:I

.field private final mReferenceLock:Ljava/lang/Object;

.field private final mText:Ljava/lang/StringBuilder;

.field private final mVibrationPatterns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/Utterance;->sPoolLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mReferenceLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mText:Ljava/lang/StringBuilder;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mMetadata:Landroid/os/Bundle;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mEarcons:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mVibrationPatterns:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mCustomEarcons:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mCustomVibrations:Ljava/util/List;

    return-void
.end method

.method private clear()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mText:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mMetadata:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mEarcons:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mVibrationPatterns:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mCustomEarcons:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mCustomVibrations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public static obtain()Lcom/google/android/marvin/talkback/Utterance;
    .locals 1

    const-string v0, ""

    invoke-static {v0}, Lcom/google/android/marvin/talkback/Utterance;->obtain(Ljava/lang/String;)Lcom/google/android/marvin/talkback/Utterance;

    move-result-object v0

    return-object v0
.end method

.method public static obtain(Ljava/lang/String;)Lcom/google/android/marvin/talkback/Utterance;
    .locals 3
    .param p0    # Ljava/lang/String;

    sget-object v2, Lcom/google/android/marvin/talkback/Utterance;->sPoolLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/marvin/talkback/Utterance;->sPool:Lcom/google/android/marvin/talkback/Utterance;

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/marvin/talkback/Utterance;->sPool:Lcom/google/android/marvin/talkback/Utterance;

    sget-object v1, Lcom/google/android/marvin/talkback/Utterance;->sPool:Lcom/google/android/marvin/talkback/Utterance;

    iget-object v1, v1, Lcom/google/android/marvin/talkback/Utterance;->mNext:Lcom/google/android/marvin/talkback/Utterance;

    sput-object v1, Lcom/google/android/marvin/talkback/Utterance;->sPool:Lcom/google/android/marvin/talkback/Utterance;

    sget v1, Lcom/google/android/marvin/talkback/Utterance;->sPoolSize:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/google/android/marvin/talkback/Utterance;->sPoolSize:I

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/marvin/talkback/Utterance;->mNext:Lcom/google/android/marvin/talkback/Utterance;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/marvin/talkback/Utterance;->mIsInPool:Z

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/marvin/talkback/Utterance;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/Utterance;-><init>()V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public getCustomEarcons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mCustomEarcons:Ljava/util/List;

    return-object v0
.end method

.method public getCustomVibrations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mCustomVibrations:Ljava/util/List;

    return-object v0
.end method

.method public getMetadata()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mMetadata:Landroid/os/Bundle;

    return-object v0
.end method

.method public getText()Ljava/lang/StringBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mText:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public recycle()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/marvin/talkback/Utterance;->mReferenceLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mReferenceCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mReferenceCount:I

    iget v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mReferenceCount:I

    if-lez v0, :cond_1

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mIsInPool:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/Utterance;->clear()V

    sget-object v1, Lcom/google/android/marvin/talkback/Utterance;->sPoolLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    sget v0, Lcom/google/android/marvin/talkback/Utterance;->sPoolSize:I

    const/4 v2, 0x3

    if-gt v0, v2, :cond_2

    sget-object v0, Lcom/google/android/marvin/talkback/Utterance;->sPool:Lcom/google/android/marvin/talkback/Utterance;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mNext:Lcom/google/android/marvin/talkback/Utterance;

    sput-object p0, Lcom/google/android/marvin/talkback/Utterance;->sPool:Lcom/google/android/marvin/talkback/Utterance;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/Utterance;->mIsInPool:Z

    sget v0, Lcom/google/android/marvin/talkback/Utterance;->sPoolSize:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/marvin/talkback/Utterance;->sPoolSize:I

    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Text:{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/Utterance;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, "}, Metadata:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/Utterance;->mMetadata:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
