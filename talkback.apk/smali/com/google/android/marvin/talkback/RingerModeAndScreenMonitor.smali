.class Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;
.super Landroid/content/BroadcastReceiver;
.source "RingerModeAndScreenMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;
    }
.end annotation


# static fields
.field private static final STATE_CHANGE_FILTER:Landroid/content/IntentFilter;


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;

.field private final mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

.field private mRingerMode:I

.field private mScreenIsOff:Z

.field private final mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;-><init>(Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mHandler:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    iput-object p1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getOrientationMonitor()Lcom/google/android/marvin/talkback/OrientationMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getShakeDetector()Lcom/google/android/marvin/talkback/ShakeDetector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mAudioManager:Landroid/media/AudioManager;

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mScreenIsOff:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->internalOnReceive(Landroid/content/Intent;)V

    return-void
.end method

.method private appendCurrentTimeAnnouncement(Ljava/lang/StringBuilder;)V
    .locals 5
    .param p1    # Ljava/lang/StringBuilder;

    const/16 v1, 0x1401

    iget-object v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    or-int/lit16 v1, v1, 0x80

    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v2, v3, v4, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {p1, v2}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private appendRingerStateAnouncement(Ljava/lang/StringBuilder;)V
    .locals 10
    .param p1    # Ljava/lang/StringBuilder;

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget v4, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    packed-switch v4, :pswitch_data_0

    const-class v4, Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v5, 0x6

    const-string v6, "Unknown ringer mode: %d"

    new-array v7, v8, [Ljava/lang/Object;

    iget v8, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v4, v5, v6, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v4, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v5, 0x7f0a007b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v9

    invoke-static {p1, v4}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v5, 0x7f0a007a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    iget-object v4, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/lit8 v4, v1, 0x14

    div-int/2addr v4, v2

    int-to-double v4, v4

    const-wide/high16 v6, 0x3fe0000000000000L

    add-double/2addr v4, v6

    double-to-int v4, v4

    mul-int/lit8 v3, v4, 0x5

    iget-object v4, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v5, 0x7f0a004d

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleDeviceUnlocked()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v3, 0x7f0a007d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v5, v1}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v4, v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    return-void
.end method

.method private handleRingerModeChanged(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iput p1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mRingerMode:I

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->appendRingerStateAnouncement(Ljava/lang/StringBuilder;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v3, v2}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private handleScreenOff()V
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mScreenIsOff:Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v1, v4}, Lcom/google/android/marvin/talkback/SpeechController;->setScreenIsOn(Z)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mContext:Landroid/content/Context;

    const v3, 0x7f0a007c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v5, v1}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->appendRingerStateAnouncement(Ljava/lang/StringBuilder;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/ShakeDetector;->pausePolling()V

    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-virtual {v1, v4}, Lcom/google/android/marvin/talkback/OrientationMonitor;->setScreenState(Z)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v1, v0, v4, v4, v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private handleScreenOn()V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mScreenIsOff:Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/SpeechController;->setScreenIsOn(Z)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->appendCurrentTimeAnnouncement(Ljava/lang/StringBuilder;)V

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->appendRingerStateAnouncement(Ljava/lang/StringBuilder;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/ShakeDetector;->resumePolling()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/OrientationMonitor;->setScreenState(Z)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v3, v2}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private internalOnReceive(Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Intent;

    const/4 v6, 0x5

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v2

    if-nez v2, :cond_0

    const-class v2, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    const-string v3, "Service not initialized during broadcast."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "android.media.EXTRA_RINGER_MODE"

    const/4 v3, 0x2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->handleRingerModeChanged(I)V

    goto :goto_0

    :cond_1
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->handleScreenOn()V

    goto :goto_0

    :cond_2
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->handleScreenOff()V

    goto :goto_0

    :cond_3
    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->handleDeviceUnlocked()V

    goto :goto_0

    :cond_4
    const-class v2, Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v3, "Registered for but not handling action %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v6, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    sget-object v0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->STATE_CHANGE_FILTER:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public isScreenOff()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mScreenIsOff:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->mHandler:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;

    invoke-virtual {v0, p2}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor$RingerModeHandler;->onReceive(Landroid/content/Intent;)V

    return-void
.end method
