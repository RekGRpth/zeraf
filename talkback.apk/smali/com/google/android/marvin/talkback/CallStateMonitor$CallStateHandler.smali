.class Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;
.super Lcom/google/android/marvin/talkback/BroadcastHandler;
.source "CallStateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/CallStateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CallStateHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/marvin/talkback/BroadcastHandler",
        "<",
        "Lcom/google/android/marvin/talkback/CallStateMonitor;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/CallStateMonitor;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/BroadcastHandler;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleOnReceive(Landroid/content/Intent;Lcom/google/android/marvin/talkback/CallStateMonitor;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/marvin/talkback/CallStateMonitor;

    # invokes: Lcom/google/android/marvin/talkback/CallStateMonitor;->internalOnReceive(Landroid/content/Intent;)V
    invoke-static {p2, p1}, Lcom/google/android/marvin/talkback/CallStateMonitor;->access$000(Lcom/google/android/marvin/talkback/CallStateMonitor;Landroid/content/Intent;)V

    return-void
.end method

.method public bridge synthetic handleOnReceive(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;->handleOnReceive(Landroid/content/Intent;Lcom/google/android/marvin/talkback/CallStateMonitor;)V

    return-void
.end method
