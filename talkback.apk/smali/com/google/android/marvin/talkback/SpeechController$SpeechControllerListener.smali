.class public interface abstract Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;
.super Ljava/lang/Object;
.source "SpeechController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/SpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SpeechControllerListener"
.end annotation


# virtual methods
.method public abstract onUtteranceCompleted(II)V
.end method

.method public abstract onUtteranceStarted(ILjava/lang/String;FFLjava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "FF",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
