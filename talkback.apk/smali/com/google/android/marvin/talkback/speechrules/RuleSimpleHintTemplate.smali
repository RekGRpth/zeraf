.class public Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;
.super Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;
.source "RuleSimpleHintTemplate.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;


# instance fields
.field private final mHintResId:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;II)V
    .locals 0
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;-><init>(Ljava/lang/Class;I)V

    iput p3, p0, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;->mHintResId:I

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-super {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic format(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;->format(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getHintText(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget v0, p0, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;->mHintResId:I

    invoke-static {p1, v0}, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->getHintString(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
