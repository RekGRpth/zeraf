.class Lcom/google/android/marvin/talkback/speechrules/RuleEditText;
.super Ljava/lang/Object;
.source "RuleEditText.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;
.implements Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;
.implements Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getText(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {p2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/provider/SettingsCompatUtils$SecureCompatUtils;->shouldSpeakPasswords(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isPassword()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isPassword()Z

    move-result v3

    if-eqz v3, :cond_3

    if-nez v1, :cond_3

    const v3, 0x7f0a0063

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    const-string v2, ""

    goto :goto_0
.end method


# virtual methods
.method public accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const-class v0, Landroid/widget/EditText;

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public format(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleEditText;->getText(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f0a0049

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getHintText(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {p2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0a006d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a0073

    invoke-static {p1, v0}, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->getHintString(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 13
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-static {p2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v12

    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v12}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-array v1, v6, [I

    const/16 v4, 0x200

    aput v4, v1, v2

    invoke-static {v12, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const v3, 0x7f08003f

    const v1, 0x7f0a00c0

    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-array v1, v6, [I

    const/16 v4, 0x100

    aput v4, v1, v2

    invoke-static {v12, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v3, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const v6, 0x7f080040

    const v1, 0x7f0a00c1

    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v4, p1

    move v5, v2

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;

    invoke-direct {v1, p1, v12}, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    invoke-interface {v10, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_2
    return-object v11
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0a00bf

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
