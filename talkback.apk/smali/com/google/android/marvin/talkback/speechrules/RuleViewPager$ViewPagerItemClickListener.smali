.class Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;
.super Ljava/lang/Object;
.source "RuleViewPager.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/speechrules/RuleViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewPagerItemClickListener"
.end annotation


# instance fields
.field private final mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 0
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :goto_0
    return v1

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x7f08003d

    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v3, 0x2000

    invoke-virtual {v2, v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    :goto_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_1
    const v2, 0x7f08003e

    if-ne v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v3, 0x1000

    invoke-virtual {v2, v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
