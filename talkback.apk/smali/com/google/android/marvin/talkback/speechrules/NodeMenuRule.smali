.class interface abstract Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;
.super Ljava/lang/Object;
.source "NodeMenuRule.java"


# virtual methods
.method public abstract accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
.end method

.method public abstract getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
.end method
