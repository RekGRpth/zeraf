.class public Lcom/google/android/marvin/talkback/speechrules/RuleViewGroup;
.super Ljava/lang/Object;
.source "RuleViewGroup.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const-class v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public format(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getNodeText(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    const-string v2, "Using node text: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
