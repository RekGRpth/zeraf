.class public Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;
.super Landroid/app/Activity;
.source "GestureChangeNotificationActivity.java"


# instance fields
.field private final mActionNameRes:[I

.field private mAlert:Landroid/app/AlertDialog;

.field private final mGestureNameRes:[I

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v1, 0x8

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mGestureNameRes:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mActionNameRes:[I

    return-void

    :array_0
    .array-data 4
        0x7f0a00c6
        0x7f0a00c8
        0x7f0a00c7
        0x7f0a00c9
        0x7f0a00ca
        0x7f0a00cc
        0x7f0a00cb
        0x7f0a00cd
    .end array-data

    :array_1
    .array-data 4
        0x7f0a00ae
        0x7f0a00af
        0x7f0a00b2
        0x7f0a00b3
        0x7f0a00b1
        0x7f0a00ad
        0x7f0a00ad
        0x7f0a00b0
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->clearPreviouslyConfiguredMappings()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->dismissNotification()V

    return-void
.end method

.method private clearPreviouslyConfiguredMappings()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const v1, 0x7f0a001b

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const v1, 0x7f0a001c

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const v1, 0x7f0a0019

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const v1, 0x7f0a001a

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const v1, 0x7f0a001d

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const v1, 0x7f0a001e

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const v1, 0x7f0a001f

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const v1, 0x7f0a0020

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private dismissNotification()V
    .locals 4

    const/4 v2, 0x2

    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v3, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const v3, 0x7f0a0021

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private getMappingDescription([I[I)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # [I
    .param p2    # [I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p2, v0

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v10, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mPrefs:Landroid/content/SharedPreferences;

    const v6, 0x7f0a00d7

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0a00de

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mGestureNameRes:[I

    iget-object v9, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mActionNameRes:[I

    invoke-direct {p0, v8, v9}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getMappingDescription([I[I)Ljava/lang/CharSequence;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {p0, v6, v7}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f0a00df

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$1;-><init>(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V

    const v6, 0x7f0a00e0

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v2, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;-><init>(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V

    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v3, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mAlert:Landroid/app/AlertDialog;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->mAlert:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
