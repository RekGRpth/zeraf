.class Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "TutorialSpeechController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SpeechHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;",
        ">;"
    }
.end annotation


# instance fields
.field private mShouldRepeatIdAfterDone:I

.field private mShouldRepeatTextAfterDone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->mShouldRepeatTextAfterDone:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->mShouldRepeatIdAfterDone:I

    return-void
.end method

.method private doneInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;I)V
    .locals 8
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;
    .param p2    # I

    iget-object v3, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->mShouldRepeatTextAfterDone:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->mShouldRepeatIdAfterDone:I

    if-lez v2, :cond_0

    if-ne p2, v2, :cond_0

    const/4 v5, 0x5

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p0, v5, v2, v6, v7}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    const-wide/16 v5, 0xbb8

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mListeners:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$900(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v6

    monitor-enter v6

    :try_start_0
    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mListeners:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$900(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechListener;

    invoke-interface {v1, p2}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechListener;->onDone(I)V

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_1
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private doneSpeakingInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)V
    .locals 4
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mListeners:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$900(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mListeners:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$900(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechListener;

    invoke-interface {v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechListener;->onDoneSpeaking()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private speakInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;Ljava/lang/String;I)V
    .locals 3
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mQueuedText:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$500(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mTtsReady:Z
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$600(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Z

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mQueuedText:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$500(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mQueuedIds:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$700(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    # invokes: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->speakImmediately(Ljava/lang/String;I)Z
    invoke-static {p1, p2, p3}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$800(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;Ljava/lang/String;I)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startSpeakingInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)V
    .locals 4
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mListeners:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$900(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    # getter for: Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->mListeners:Ljava/util/LinkedList;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->access$900(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechListener;

    invoke-interface {v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechListener;->onStartSpeaking()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)V
    .locals 2
    .param p1    # Landroid/os/Message;
    .param p2    # Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->speakInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->startSpeakingInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p2}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->doneSpeakingInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)V

    goto :goto_0

    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, p2, v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->doneInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->speakInternal(Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;)V

    return-void
.end method

.method public interrupt()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->removeMessages(I)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->removeMessages(I)V

    return-void
.end method

.method public postDone(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public postDoneSpeaking()V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public postStartSpeaking()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public speak(Ljava/lang/String;IZ)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->removeMessages(I)V

    if-eqz p3, :cond_0

    move v0, p2

    :goto_0
    iput v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->mShouldRepeatIdAfterDone:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1, p1}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController$SpeechHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
