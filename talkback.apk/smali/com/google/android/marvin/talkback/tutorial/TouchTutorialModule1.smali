.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;
.super Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.source "TouchTutorialModule1.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final mAllApps:Landroid/widget/GridView;

.field private final mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 6
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v0, 0x7f030002

    const v1, 0x7f0a0110

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V

    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030005

    const v3, 0x7f080049

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    const v0, 0x7f080046

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->setSkipVisible(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->setBackVisible(Z)V

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->setNextVisible(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->setFinishVisible(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)Landroid/widget/GridView;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger1()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger2()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger3()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger4()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger5()V

    return-void
.end method

.method private onTrigger1()V
    .locals 3

    const v0, 0x7f0a0112

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$2;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger2()V
    .locals 3

    const v0, 0x7f0a0113

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$3;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$3;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger3()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->getLabel(I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f0a0114

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v4, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    new-instance v2, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$4;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$4;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger4()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->getLabel(I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f0a0115

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v4, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    new-instance v2, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$5;

    invoke-direct {v2, p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$5;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    new-instance v2, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$6;

    invoke-direct {v2, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$6;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private onTrigger5()V
    .locals 6

    const/4 v5, 0x1

    const v0, 0x7f0a0117

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a010b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onShown()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->onTrigger0()V

    return-void
.end method

.method public onTrigger0()V
    .locals 3

    const v0, 0x7f0a0111

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;->mAllApps:Landroid/widget/GridView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule1;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method
