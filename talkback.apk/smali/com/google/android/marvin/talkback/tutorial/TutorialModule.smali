.class abstract Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.super Landroid/widget/FrameLayout;
.source "TutorialModule.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final mBack:Landroid/widget/Button;

.field private final mFinish:Landroid/widget/Button;

.field private final mHandler:Landroid/os/Handler;

.field private final mInstructions:Landroid/widget/TextView;

.field private mIsVisible:Z

.field private final mNext:Landroid/widget/Button;

.field private final mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

.field private final mSkip:Landroid/widget/Button;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

.field private final mTitleResId:I


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V
    .locals 6
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;
    .param p2    # I
    .param p3    # I

    const/4 v5, 0x1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getSpeechController()Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mSpeechController:Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

    iput p3, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mTitleResId:I

    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f030006

    invoke-virtual {v2, v4, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f08004d

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mInstructions:Landroid/widget/TextView;

    const v4, 0x7f080041

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mSkip:Landroid/widget/Button;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mSkip:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f080042

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mBack:Landroid/widget/Button;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mBack:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f080043

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mNext:Landroid/widget/Button;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mNext:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f080044

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mFinish:Landroid/widget/Button;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mFinish:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f08004a

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v3, :cond_0

    invoke-virtual {v3, p3}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    const v4, 0x7f08004c

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v2, p2, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final activate()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mIsVisible:Z

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mInstructions:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    iget v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mTitleResId:I

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->onShown()V

    return-void
.end method

.method protected varargs addInstruction(IZ[Ljava/lang/Object;)V
    .locals 3
    .param p1    # I
    .param p2    # Z
    .param p3    # [Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mIsVisible:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v1, p1, p3}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mInstructions:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mInstructions:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mSpeechController:Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/android/marvin/talkback/tutorial/TutorialSpeechController;->speak(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public deactivate()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mIsVisible:Z

    return-void
.end method

.method protected installTriggerDelayed(Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x5dc

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f080041

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f080042

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->previous()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f080043

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->next()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f080044

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mParentTutorial:Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->finish()V

    goto :goto_0
.end method

.method public abstract onShown()V
.end method

.method protected setBackVisible(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mBack:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setFinishVisible(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mFinish:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setNextVisible(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mNext:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setSkipVisible(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;->mSkip:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
