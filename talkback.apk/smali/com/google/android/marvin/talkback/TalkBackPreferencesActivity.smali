.class public Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "TalkBackPreferencesActivity.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private final mTouchExplorationChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private final mTouchExploreObserver:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$2;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$2;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExploreObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$3;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExplorationChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$4;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->setTouchExplorationRequested(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;I)Landroid/preference/Preference;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->updateTouchExplorationState()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->confirmDisableExploreByTouch()V

    return-void
.end method

.method private assignTutorialIntent()V
    .locals 5

    const v3, 0x7f0a0016

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    const v3, 0x7f0a0015

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v3, v4, :cond_2

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private checkAccelerometerSupport()V
    .locals 6

    const-string v4, "sensor"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v4, 0x7f0a0002

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceGroup;

    const v4, 0x7f0a0014

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private checkInstalledBacks()V
    .locals 10

    const/4 v6, 0x1

    const/4 v8, 0x0

    const v9, 0x7f0a0006

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    const v9, 0x7f0a0007

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    const-string v9, "com.google.android.marvin.kickback"

    invoke-static {p0, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v1

    const/4 v9, 0x5

    if-lt v1, v9, :cond_4

    move v5, v6

    :goto_0
    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    const v9, 0x7f0a0008

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    const v9, 0x7f0a000c

    invoke-direct {p0, v9}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v3

    const-string v9, "com.google.android.marvin.soundback"

    invoke-static {p0, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v7

    const/4 v9, 0x7

    if-lt v7, v9, :cond_5

    :goto_1
    if-eqz v6, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    return-void

    :cond_4
    move v5, v8

    goto :goto_0

    :cond_5
    move v6, v8

    goto :goto_1
.end method

.method private checkProximitySupport()V
    .locals 5

    const-string v4, "sensor"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v4, 0x7f0a0002

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    const v4, 0x7f0a0004

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private checkTelephonySupport()V
    .locals 5

    const-string v4, "phone"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v4, 0x7f0a0002

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    const v4, 0x7f0a0010

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private checkTouchExplorationSupport()V
    .locals 3

    const v1, 0x7f0a0017

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkTouchExplorationSupportInner(Landroid/preference/PreferenceGroup;)V

    goto :goto_0
.end method

.method private checkTouchExplorationSupportInner(Landroid/preference/PreferenceGroup;)V
    .locals 6
    .param p1    # Landroid/preference/PreferenceGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const v4, 0x7f0a0012

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const v4, 0x7f0a0013

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-ge v4, v5, :cond_1

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->updateTouchExplorationState()V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExplorationChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const v4, 0x7f0a0018

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v3

    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private checkVibrationSupport()V
    .locals 4

    const-string v3, "vibrator"

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    if-eqz v2, :cond_1

    invoke-static {v2}, Lcom/googlecode/eyesfree/compat/os/VibratorCompatUtils;->hasVibrator(Landroid/os/Vibrator;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v3, 0x7f0a0006

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    const v3, 0x7f0a0007

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private confirmDisableExploreByTouch()V
    .locals 4

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$1;-><init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a00e1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00e2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private findPreferenceByResId(I)Landroid/preference/Preference;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method private fixListSummaries(Landroid/preference/PreferenceGroup;)V
    .locals 5
    .param p1    # Landroid/preference/PreferenceGroup;

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    instance-of v3, v2, Landroid/preference/PreferenceGroup;

    if-eqz v3, :cond_3

    check-cast v2, Landroid/preference/PreferenceGroup;

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->fixListSummaries(Landroid/preference/PreferenceGroup;)V

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    instance-of v3, v2, Landroid/preference/ListPreference;

    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    move-object v3, v2

    check-cast v3, Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v2, v3}, Landroid/preference/Preference$OnPreferenceChangeListener;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mPreferenceChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_1
.end method

.method private isTouchExplorationEnabled(Landroid/content/ContentResolver;)Z
    .locals 3
    .param p1    # Landroid/content/ContentResolver;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "touch_exploration_enabled"

    invoke-static {p1, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private registerTouchSettingObserver()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const-string v1, "touch_exploration_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExploreObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method private setTouchExplorationRequested(Z)Z
    .locals 5
    .param p1    # Z

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0011

    invoke-static {v0, v2, v3, p1}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V

    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v2, 0x3

    const-string v3, "TalkBack active, waiting for EBT request to take effect"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private updateTouchExplorationState()V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const v12, 0x7f0a0011

    const v7, 0x7f0a0012

    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->findPreferenceByResId(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const v7, 0x7f0c0008

    invoke-static {v2, v5, v12, v7}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v4

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-direct {p0, v6}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->isTouchExplorationEnabled(Landroid/content/ContentResolver;)Z

    move-result v0

    :goto_1
    if-eq v4, v0, :cond_2

    const/4 v7, 0x3

    const-string v8, "Set touch exploration preference to reflect actual state %b"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {p0, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2, v5, v12, v0}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V

    :cond_2
    if-eq v3, v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    :cond_3
    move v0, v4

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040001

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->fixListSummaries(Landroid/preference/PreferenceGroup;)V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->assignTutorialIntent()V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkTouchExplorationSupport()V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkTelephonySupport()V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkVibrationSupport()V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkProximitySupport()V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkAccelerometerSupport()V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->checkInstalledBacks()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    sget-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->mTouchExploreObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    sget-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->registerTouchSettingObserver()V

    :cond_0
    return-void
.end method
