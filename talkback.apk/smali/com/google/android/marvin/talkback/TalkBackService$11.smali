.class Lcom/google/android/marvin/talkback/TalkBackService$11;
.super Landroid/content/BroadcastReceiver;
.source "TalkBackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.marvin.talkback.RESUME_FEEDBACK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->resumeTalkBack()V
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1100(Lcom/google/android/marvin/talkback/TalkBackService;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$11;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->resumeTalkBack()V
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1100(Lcom/google/android/marvin/talkback/TalkBackService;)V

    goto :goto_0
.end method
