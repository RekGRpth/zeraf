.class public Lcom/google/android/marvin/talkback/CustomResourceMapper;
.super Ljava/lang/Object;
.source "CustomResourceMapper.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mCustomResourceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mDefaultResourceMap:Landroid/util/SparseIntArray;

.field private final mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final mResolvedValue:Landroid/util/TypedValue;

.field private final mResources:Landroid/content/res/Resources;

.field private final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mDefaultResourceMap:Landroid/util/SparseIntArray;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mCustomResourceMap:Ljava/util/Map;

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mResolvedValue:Landroid/util/TypedValue;

    new-instance v0, Lcom/google/android/marvin/talkback/CustomResourceMapper$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/CustomResourceMapper$1;-><init>(Lcom/google/android/marvin/talkback/CustomResourceMapper;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mResources:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefaults()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/CustomResourceMapper;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/CustomResourceMapper;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mCustomResourceMap:Ljava/util/Map;

    return-object v0
.end method

.method private loadDefault(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mResources:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mResolvedValue:Landroid/util/TypedValue;

    const/4 v2, 0x1

    invoke-virtual {v0, p2, v1, v2}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mDefaultResourceMap:Landroid/util/SparseIntArray;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mResolvedValue:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method

.method private loadDefaults()V
    .locals 2

    const v0, 0x7f080022

    const v1, 0x7f05000c

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080023

    const v1, 0x7f05000d

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080024

    const v1, 0x7f05000e

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080025

    const v1, 0x7f05000f

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080026

    const v1, 0x7f050010

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080027

    const v1, 0x7f050011

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080028

    const v1, 0x7f050012

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080029

    const v1, 0x7f050013

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f08002a

    const v1, 0x7f050014

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f08002b

    const v1, 0x7f050015

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f08002c

    const v1, 0x7f050016

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f08002d

    const v1, 0x7f050017

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f08002e

    const v1, 0x7f050018

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f08002f

    const v1, 0x7f050019

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080030

    const v1, 0x7f05001a

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080031

    const v1, 0x7f05001b

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080032

    const v1, 0x7f05001c

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080033

    const v1, 0x7f0b0010

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080034

    const v1, 0x7f0b0011

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080035

    const v1, 0x7f0b0012

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080036

    const v1, 0x7f0b0013

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080037

    const v1, 0x7f0b0014

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080038

    const v1, 0x7f0b0015

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f080039

    const v1, 0x7f0b0016

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    const v0, 0x7f08003a

    const v1, 0x7f0b0017

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->loadDefault(II)V

    return-void
.end method


# virtual methods
.method public getResourceIdForPreference(I)I
    .locals 8
    .param p1    # I

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mContext:Landroid/content/Context;

    invoke-virtual {v5, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mCustomResourceMap:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v5, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mResources:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v7, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mCustomResourceMap:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/marvin/talkback/CustomResourceMapper;->mDefaultResourceMap:Landroid/util/SparseIntArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
