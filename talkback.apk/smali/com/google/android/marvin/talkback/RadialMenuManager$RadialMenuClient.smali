.class public interface abstract Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
.super Ljava/lang/Object;
.source "RadialMenuManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/RadialMenuManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RadialMenuClient"
.end annotation


# virtual methods
.method public abstract onCreateRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)V
.end method

.method public abstract onMenuItemClicked(Landroid/view/MenuItem;)Z
.end method

.method public abstract onMenuItemHovered(Landroid/view/MenuItem;)Z
.end method

.method public abstract onPrepareRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)Z
.end method
