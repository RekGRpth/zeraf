.class public Lcom/google/android/marvin/talkback/ProcessorWebContent;
.super Ljava/lang/Object;
.source "ProcessorWebContent.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$EventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

.field private mLastNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    return-void
.end method


# virtual methods
.method public process(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 8
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v7, 0x0

    const v4, 0x8080

    invoke-static {p1, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v2, p1}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :cond_3
    iput-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mLastNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasWebContent(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->isScriptInjectionEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/FullScreenReadController;->getReadingState()Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    move-result-object v4

    sget-object v5, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->ENTERING_WEB_CONTENT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-eq v4, v5, :cond_4

    const/4 v4, -0x2

    invoke-static {v3, v4}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performSpecialAction(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    goto :goto_0

    :cond_5
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-gt v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v5, "com.android.settings"

    const-string v6, "accessibility_toggle_script_injection_preference_title"

    invoke-static {v4, v5, v6}, Lcom/google/android/marvin/utils/AutomationUtils;->getPackageString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v5, 0x7f0a00e5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorWebContent;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v7, v7, v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0
.end method
