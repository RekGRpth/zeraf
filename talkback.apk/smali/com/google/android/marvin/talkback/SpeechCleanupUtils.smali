.class public Lcom/google/android/marvin/talkback/SpeechCleanupUtils;
.super Ljava/lang/Object;
.source "SpeechCleanupUtils.java"


# static fields
.field private static final UNICODE_MAP:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x26

    const v2, 0x7f0a0126

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3c

    const v2, 0x7f0a0127

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3e

    const v2, 0x7f0a0128

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x27

    const v2, 0x7f0a0125

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2a

    const v2, 0x7f0a0129

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x40

    const v2, 0x7f0a012a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5c

    const v2, 0x7f0a012b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2022

    const v2, 0x7f0a012c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5e

    const v2, 0x7f0a012d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa2

    const v2, 0x7f0a012e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3a

    const v2, 0x7f0a012f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2c

    const v2, 0x7f0a0130

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa9

    const v2, 0x7f0a0131

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x7b

    const v2, 0x7f0a0132

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x7d

    const v2, 0x7f0a0133

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xb0

    const v2, 0x7f0a0134

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf7

    const v2, 0x7f0a0135

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x24

    const v2, 0x7f0a0136

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2026

    const v2, 0x7f0a0137

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2014

    const v2, 0x7f0a0138

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2013

    const v2, 0x7f0a0139

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x20ac

    const v2, 0x7f0a013a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x21

    const v2, 0x7f0a013b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x60

    const v2, 0x7f0a013c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2d

    const v2, 0x7f0a013d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x201e

    const v2, 0x7f0a013e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xd7

    const v2, 0x7f0a013f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    const v2, 0x7f0a0140

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xb6

    const v2, 0x7f0a0141

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x28

    const v2, 0x7f0a0142

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x29

    const v2, 0x7f0a0143

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x25

    const v2, 0x7f0a0144

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2e

    const v2, 0x7f0a0145

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3c0

    const v2, 0x7f0a0146

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x23

    const v2, 0x7f0a0147

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa3

    const v2, 0x7f0a0148

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3f

    const v2, 0x7f0a0149

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x22

    const v2, 0x7f0a014a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xae

    const v2, 0x7f0a014b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x3b

    const v2, 0x7f0a014c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2f

    const v2, 0x7f0a014d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x20

    const v2, 0x7f0a014f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5b

    const v2, 0x7f0a0150

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5d

    const v2, 0x7f0a0151

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x221a

    const v2, 0x7f0a0152

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2122

    const v2, 0x7f0a0153

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x5f

    const v2, 0x7f0a0154

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x7c

    const v2, 0x7f0a0155

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa5

    const v2, 0x7f0a0156

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xac

    const v2, 0x7f0a0157

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa6

    const v2, 0x7f0a0158

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xb5

    const v2, 0x7f0a0159

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2248

    const v2, 0x7f0a015a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2260

    const v2, 0x7f0a015b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa4

    const v2, 0x7f0a015c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xa7

    const v2, 0x7f0a015d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2191

    const v2, 0x7f0a015e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2190

    const v2, 0x7f0a015f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method

.method public static cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/CharSequence;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->getCleanValueFor(Landroid/content/Context;C)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static getCleanValueFor(Landroid/content/Context;C)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # C

    sget-object v1, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->UNICODE_MAP:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0a005f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
