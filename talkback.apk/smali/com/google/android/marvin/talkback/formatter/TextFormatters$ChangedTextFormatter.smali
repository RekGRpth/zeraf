.class public final Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;
.super Ljava/lang/Object;
.source "TextFormatters.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/TextFormatters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChangedTextFormatter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private appendLastWordIfNeeded(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Ljava/lang/StringBuilder;)Z
    .locals 8
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/StringBuilder;

    const/4 v5, 0x1

    const/4 v4, 0x0

    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->getEventText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$300(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v1

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-le v1, v6, :cond_1

    const/4 v6, 0x5

    const-string v7, "Received event with invalid fromIndex: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v4

    invoke-static {p0, v6, v7, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-interface {v2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {v2, v1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->getPrecedingWhitespace(Ljava/lang/CharSequence;I)I

    move-result v0

    invoke-interface {v2, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v6

    if-eqz v6, :cond_0

    new-array v6, v5, [Ljava/lang/Object;

    aput-object v3, v6, v4

    invoke-static {p3, v6}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v4, v5

    goto :goto_0
.end method

.method private formatInternal(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 13
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v10, "pitch"

    const v11, 0x3f99999a

    invoke-virtual {v4, v10, v11}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    const-string v10, "rate"

    const/high16 v11, 0x3f800000

    invoke-virtual {v4, v10, v11}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "speech_params"

    invoke-virtual {v10, v11, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p2}, Lcom/googlecode/eyesfree/compat/provider/SettingsCompatUtils$SecureCompatUtils;->shouldSpeakPasswords(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isPassword()Z

    move-result v10

    if-eqz v10, :cond_0

    if-nez v7, :cond_0

    invoke-direct {p0, p1, p2, v8}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->formatPassword(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Ljava/lang/StringBuilder;)Z

    move-result v10

    :goto_0
    return v10

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->passesSanityCheck(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v10

    if-nez v10, :cond_1

    const/4 v10, 0x6

    const-string v11, "Inconsistent text change event detected"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v10, v11, v12}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v10, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_2

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v11

    if-ne v10, v11, :cond_2

    const/4 v9, 0x1

    :goto_1
    if-eqz v9, :cond_3

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const v12, 0x7f0a0067

    invoke-virtual {p2, v12}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v10}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v10, 0x1

    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    goto :goto_1

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->getRemovedText(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->getAddedText(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v10, 0x3

    const-string v11, "Drop event, nothing changed"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v10, v11, v12}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v10, 0x0

    goto :goto_0

    :cond_4
    if-eqz v6, :cond_5

    if-nez v1, :cond_6

    :cond_5
    const/4 v10, 0x3

    const-string v11, "Drop event, either added or removed was null"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v10, v11, v12}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v10, 0x0

    goto :goto_0

    :cond_6
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v5, v0, :cond_9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v6, v10, v1, v11, v0}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-static {v6, v0, v5}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v6

    const-string v1, ""

    :cond_7
    :goto_2
    invoke-static {p2, v6}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {p2, v1}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a

    invoke-direct {p0, p1, p2, v8}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->appendLastWordIfNeeded(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Ljava/lang/StringBuilder;)Z

    move-result v10

    if-nez v10, :cond_8

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v2, v10, v11

    invoke-static {v8, v10}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v10, 0x1

    goto/16 :goto_0

    :cond_9
    if-le v0, v5, :cond_7

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v6, v10, v1, v11, v5}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    move-result v10

    if-eqz v10, :cond_7

    const-string v6, ""

    invoke-static {v1, v5, v0}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_a
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_b

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v3, v10, v11

    const/4 v11, 0x1

    const v12, 0x7f0a0068

    invoke-virtual {p2, v12}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v10}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v10, 0x1

    goto/16 :goto_0

    :cond_b
    const/4 v10, 0x3

    const-string v11, "Drop event, cleaned up text was empty"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v10, v11, v12}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method private formatPassword(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Ljava/lang/StringBuilder;)Z
    .locals 8
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/StringBuilder;

    const v7, 0x7f0a012c

    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v0

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    if-ne v0, v4, :cond_1

    if-nez v2, :cond_1

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {p2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {p3, v5}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_1
    move v3, v4

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    if-ne v2, v4, :cond_2

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {p2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const v3, 0x7f0a0068

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v4

    invoke-static {p3, v5}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const v5, 0x7f0a004c

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {p2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v5, v4, [Ljava/lang/Object;

    aput-object v1, v5, v3

    invoke-static {p3, v5}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private getAddedText(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 11
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/content/Context;

    const/4 v10, 0x1

    const/4 v4, 0x0

    const/4 v9, 0x5

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v10, :cond_1

    :cond_0
    const-string v5, "getAddedText: Text list was null or bad size"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p0, v9, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v4

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_2

    const-string v4, ""

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    if-nez v2, :cond_3

    const-string v5, "getAddedText: First text entry was null"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p0, v9, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v5

    add-int v1, v0, v5

    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->areValidIndices(Ljava/lang/CharSequence;II)Z
    invoke-static {v2, v0, v1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$400(Ljava/lang/CharSequence;II)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "getAddedText: Invalid indices (%d,%d) for \"%s\""

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    const/4 v7, 0x2

    aput-object v2, v6, v7

    invoke-static {p0, v9, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    invoke-interface {v2, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_0
.end method

.method private static getPrecedingWhitespace(Ljava/lang/CharSequence;I)I
    .locals 2
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I

    add-int/lit8 v0, p1, -0x1

    :goto_0
    if-lez v0, :cond_1

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getRemovedText(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v4

    add-int v1, v0, v4

    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->areValidIndices(Ljava/lang/CharSequence;II)Z
    invoke-static {v2, v0, v1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$400(Ljava/lang/CharSequence;II)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0
.end method

.method private passesSanityCheck(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 7
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v3, 0x1

    const/4 v4, 0x0

    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->getEventText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$300(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v5

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-ne v5, v6, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v6

    sub-int v2, v5, v6

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/2addr v5, v2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-eq v5, v6, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method private shouldEchoKeyboard(Landroid/content/Context;)Z
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0a000e

    const v7, 0x7f0a0026

    invoke-static {v2, v3, v6, v7}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const/4 v6, 0x6

    const-string v7, "Invalid keyboard echo preference value: %d"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v4

    invoke-static {p0, v6, v7, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    :pswitch_0
    return v4

    :pswitch_1
    move v4, v5

    goto :goto_0

    :pswitch_2
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v6, v0, Landroid/content/res/Configuration;->keyboard:I

    if-eq v6, v5, :cond_1

    iget v6, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    :cond_1
    move v4, v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 8
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v0

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelection:Z
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$100()J

    move-result-wide v4

    sub-long v4, v0, v4

    const-wide/16 v6, 0x64

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->formatInternal(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v4

    if-eqz v4, :cond_0

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelection:Z
    invoke-static {v3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$002(Z)Z

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {v0, v1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$102(J)J

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v4

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$202(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    invoke-direct {p0, p2}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;->shouldEchoKeyboard(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_0
.end method
