.class Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;
.super Ljava/lang/Object;
.source "EventSpeechRule.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultFormatter"
.end annotation


# instance fields
.field private final mSelectors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mTemplate:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Lorg/w3c/dom/Node;)V
    .locals 9
    .param p2    # Lorg/w3c/dom/Node;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mSelectors:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {p2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    :goto_0
    if-ge v3, v2, :cond_3

    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_0

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$000(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "template"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v6

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getLocalizedTextContent(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v6, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$700(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_1
    const-string v6, "property"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mSelectors:Ljava/util/List;

    new-instance v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v8

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getLocalizedTextContent(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v8, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$700(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v5, v8}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mSelectors:Ljava/util/List;

    new-instance v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v5, v8}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplate:Ljava/lang/String;

    return-void
.end method

.method private formatTemplateOrAppendSpaceSeparatedValueIfNoTemplate(Lcom/google/android/marvin/talkback/Utterance;[Ljava/lang/Object;)V
    .locals 13
    .param p1    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2    # [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplate:Ljava/lang/String;

    if-eqz v7, :cond_1

    :try_start_0
    iget-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplate:Ljava/lang/String;

    invoke-static {v7, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/util/MissingFormatArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v5

    const-class v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;

    const/4 v8, 0x6

    const-string v9, "Speech rule: \'%d\' has inconsistency between template: \'%s\' and arguments: \'%s\'. Possibliy #template arguments does not match #parameters. %s"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mRuleIndex:I
    invoke-static {v12}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$800(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mTemplate:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x2

    aput-object p2, v10, v11

    const/4 v11, 0x3

    invoke-virtual {v5}, Ljava/util/MissingFormatArgumentException;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v7, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move-object v1, p2

    array-length v4, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v0, v1, v3

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-static {v6, v7}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 16
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->mSelectors:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v12

    new-array v2, v12, [Ljava/lang/Object;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v4, 0x0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v3

    :goto_0
    if-ge v4, v3, :cond_2

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;

    iget-object v7, v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    iget-object v8, v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    const-string v12, "property"

    invoke-virtual {v12, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyValue(Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;
    invoke-static {v12, v0, v8, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$500(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    :goto_1
    aput-object v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const-string v5, ""

    goto :goto_1

    :cond_1
    new-instance v13, Ljava/lang/IllegalArgumentException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unknown selector type: ["

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v12, v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;->first:Ljava/lang/Object;

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, ", "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v12, v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;->second:Ljava/lang/Object;

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, "]"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v13, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->formatTemplateOrAppendSpaceSeparatedValueIfNoTemplate(Lcom/google/android/marvin/talkback/Utterance;[Ljava/lang/Object;)V

    const v12, 0x808c

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v10

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityEvent;->isEnabled()Z

    move-result v12

    if-nez v12, :cond_3

    if-eqz v10, :cond_3

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v14}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v14

    const v15, 0x7f0a006d

    invoke-virtual {v14, v15}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v12, 0x1

    return v12
.end method
