.class public Lcom/google/android/marvin/talkback/formatter/ScrollFormatter;
.super Ljava/lang/Object;
.source "ScrollFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# static fields
.field private static mLastScrollEvent:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/google/android/marvin/talkback/formatter/ScrollFormatter;->mLastScrollEvent:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getScrollPercent(Landroid/view/accessibility/AccessibilityEvent;)F
    .locals 4
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/ScrollFormatter;->getScrollPosition(Landroid/view/accessibility/AccessibilityEvent;)F

    move-result v0

    const/high16 v1, 0x42c80000

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    mul-float/2addr v1, v2

    return v1
.end method

.method private getScrollPosition(Landroid/view/accessibility/AccessibilityEvent;)F
    .locals 7
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    new-instance v3, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v3, p1}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v0

    if-ltz v0, :cond_0

    if-lez v1, :cond_0

    int-to-float v5, v0

    int-to-float v6, v1

    div-float/2addr v5, v6

    :goto_0
    return v5

    :cond_0
    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getScrollY()I

    move-result v4

    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getMaxScrollY()I

    move-result v2

    if-ltz v4, :cond_1

    if-lez v2, :cond_1

    int-to-float v5, v4

    int-to-float v6, v2

    div-float/2addr v5, v6

    goto :goto_0

    :cond_1
    if-ltz v4, :cond_2

    if-lez v1, :cond_2

    if-gt v4, v1, :cond_2

    int-to-float v5, v4

    int-to-float v6, v1

    div-float/2addr v5, v6

    goto :goto_0

    :cond_2
    const/high16 v5, 0x3f000000

    goto :goto_0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 10
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sget-wide v4, Lcom/google/android/marvin/talkback/formatter/ScrollFormatter;->mLastScrollEvent:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0xfa

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    return v4

    :cond_0
    sput-wide v0, Lcom/google/android/marvin/talkback/formatter/ScrollFormatter;->mLastScrollEvent:J

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/ScrollFormatter;->getScrollPercent(Landroid/view/accessibility/AccessibilityEvent;)F

    move-result v2

    const-wide/high16 v4, 0x4000000000000000L

    float-to-double v6, v2

    const-wide/high16 v8, 0x4049000000000000L

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x3ff0000000000000L

    sub-double/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v4

    const v5, 0x7f080031

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "earcon_rate"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    const/4 v4, 0x1

    goto :goto_0
.end method
