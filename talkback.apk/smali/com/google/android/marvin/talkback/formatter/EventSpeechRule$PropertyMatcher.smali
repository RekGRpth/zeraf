.class Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;
.super Ljava/lang/Object;
.source "EventSpeechRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PropertyMatcher"
.end annotation


# static fields
.field private static final PATTERN_GREATER_THAN:Ljava/util/regex/Pattern;

.field private static final PATTERN_GREATER_THAN_OR_EQUAL:Ljava/util/regex/Pattern;

.field private static final PATTERN_LESS_THAN:Ljava/util/regex/Pattern;

.field private static final PATTERN_LESS_THAN_OR_EQUAL:Ljava/util/regex/Pattern;

.field private static final PATTERN_OR:Ljava/util/regex/Pattern;

.field private static final PATTERN_SPLIT_OR:Ljava/util/regex/Pattern;


# instance fields
.field private final mAcceptedValues:[Ljava/lang/Object;

.field private final mContext:Landroid/content/Context;

.field private final mPropertyName:Ljava/lang/String;

.field private final mPropertyType:I

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "(\\s)*<=(\\s)*([+-])?((\\d)+(\\.(\\d)+)?|\\.(\\d)+)(\\s)*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_LESS_THAN_OR_EQUAL:Ljava/util/regex/Pattern;

    const-string v0, "(\\s)*>=(\\s)*([+-])?((\\d)+(\\.(\\d)+)?|\\.(\\d)+)(\\s)*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_GREATER_THAN_OR_EQUAL:Ljava/util/regex/Pattern;

    const-string v0, "(\\s)*<(\\s)*([+-])?((\\d)+(\\.(\\d)+)?|\\.(\\d)+)(\\s)*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_LESS_THAN:Ljava/util/regex/Pattern;

    const-string v0, "(\\s)*>(\\s)*([+-])?((\\d)+(\\.(\\d)+)?|\\.(\\d)+)(\\s)*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_GREATER_THAN:Ljava/util/regex/Pattern;

    const-string v0, "(.)+\\|\\|(.)+(\\|\\|(.)+)*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_OR:Ljava/util/regex/Pattern;

    const-string v0, "(\\s)*\\|\\|(\\s)*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_SPLIT_OR:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyName:Ljava/lang/String;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyType(Ljava/lang/String;)I
    invoke-static {p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$900(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyType:I

    if-nez p3, :cond_1

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    iput v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v8, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyType:I

    if-eq v8, v9, :cond_2

    iget v8, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyType:I

    if-ne v8, v10, :cond_3

    :cond_2
    move v4, v7

    :goto_1
    if-eqz v4, :cond_4

    sget-object v8, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_LESS_THAN_OR_EQUAL:Ljava/util/regex/Pattern;

    invoke-virtual {v8, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_4

    iput v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    const-string v8, "<="

    invoke-virtual {p3, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v8, v2, 0x2

    invoke-virtual {p3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    invoke-static {p2, v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$1000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v8

    aput-object v8, v7, v6

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move v4, v6

    goto :goto_1

    :cond_4
    if-eqz v4, :cond_5

    sget-object v8, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_GREATER_THAN_OR_EQUAL:Ljava/util/regex/Pattern;

    invoke-virtual {v8, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_5

    iput v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    const-string v8, ">="

    invoke-virtual {p3, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v8, v2, 0x2

    invoke-virtual {p3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    invoke-static {p2, v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$1000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v8

    aput-object v8, v7, v6

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    goto :goto_0

    :cond_5
    if-eqz v4, :cond_6

    sget-object v8, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_LESS_THAN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_6

    iput v10, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    const-string v8, "<"

    invoke-virtual {p3, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {p3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    invoke-static {p2, v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$1000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v8

    aput-object v8, v7, v6

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    goto/16 :goto_0

    :cond_6
    if-eqz v4, :cond_7

    sget-object v8, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_GREATER_THAN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_7

    const/4 v8, 0x4

    iput v8, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    const-string v8, ">"

    invoke-virtual {p3, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {p3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    invoke-static {p2, v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$1000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v8

    aput-object v8, v7, v6

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    sget-object v8, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_OR:Ljava/util/regex/Pattern;

    invoke-virtual {v8, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_8

    const/4 v6, 0x5

    iput v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    sget-object v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->PATTERN_SPLIT_OR:Ljava/util/regex/Pattern;

    invoke-virtual {v6, p3}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    array-length v6, v0

    new-array v6, v6, [Ljava/lang/Object;

    iput-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    const/4 v3, 0x0

    array-length v1, v0

    :goto_2
    if-ge v3, v1, :cond_0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    aget-object v7, v0, v3

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    invoke-static {p2, v7}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$1000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v7

    aput-object v7, v6, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_8
    iput v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    invoke-static {p2, p3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$1000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v8

    aput-object v8, v7, v6

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private acceptClassNameProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const/4 v7, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    array-length v6, v1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_0

    aget-object v0, v1, v5

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    if-nez p4, :cond_0

    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v9, v4, p3}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->loadOrGetCachedClass(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Class;

    move-result-object v3

    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10, p2}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->loadOrGetCachedClass(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Class;

    move-result-object v2

    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {v3, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v7

    goto :goto_0

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private acceptComparableProperty(Ljava/lang/Comparable;Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Comparable",
            "<TT;>;TT;)Z"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    iget v3, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    packed-switch v3, :pswitch_data_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :pswitch_0
    if-lez v0, :cond_0

    move v1, v2

    goto :goto_0

    :pswitch_1
    if-gez v0, :cond_0

    move v1, v2

    goto :goto_0

    :pswitch_2
    if-ltz v0, :cond_0

    move v1, v2

    goto :goto_0

    :pswitch_3
    if-gtz v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private acceptProperty(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;

    const/4 v4, 0x0

    instance-of v5, p1, Ljava/lang/CharSequence;

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mType:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_4

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v0, v1, v2

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v4, 0x1

    :cond_2
    :goto_1
    return v4

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    iget v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyType:I

    packed-switch v5, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    check-cast p1, Ljava/lang/Float;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    aget-object v4, v5, v4

    check-cast v4, Ljava/lang/Float;

    invoke-direct {p0, p1, v4}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->acceptComparableProperty(Ljava/lang/Comparable;Ljava/lang/Object;)Z

    move-result v4

    goto :goto_1

    :pswitch_1
    check-cast p1, Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    aget-object v4, v5, v4

    check-cast v4, Ljava/lang/Integer;

    invoke-direct {p0, p1, v4}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->acceptComparableProperty(Ljava/lang/Comparable;Ljava/lang/Object;)Z

    move-result v4

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public varargs accept(Ljava/lang/Object;[Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;
    .param p2    # [Ljava/lang/Object;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    if-nez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    if-nez p1, :cond_1

    iget v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyType:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_3

    const-string p1, ""

    :cond_1
    const-string v5, "className"

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "classNameStrict"

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_2
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    aget-object v1, p2, v4

    check-cast v1, Ljava/lang/String;

    aget-object v2, p2, v3

    check-cast v2, Ljava/lang/String;

    const-string v3, "classNameStrict"

    iget-object v4, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->acceptClassNameProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->acceptProperty(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0
.end method

.method public getAcceptedValues()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mAcceptedValues:[Ljava/lang/Object;

    return-object v0
.end method
