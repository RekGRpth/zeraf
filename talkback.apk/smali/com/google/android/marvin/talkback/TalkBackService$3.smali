.class Lcom/google/android/marvin/talkback/TalkBackService$3;
.super Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;
.source "TalkBackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/TalkBackService;->createSummaryMenu(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;-><init>()V

    return-void
.end method


# virtual methods
.method public onFirstTouch()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$300(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorController;->refocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$300(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/CursorController;->next(Z)Z

    :cond_0
    return-void
.end method

.method public onNext()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$300(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/CursorController;->next(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$400(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v0

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    :cond_0
    return-void
.end method

.method public onPrevious()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$300(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/CursorController;->previous(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$400(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v0

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    :cond_0
    return-void
.end method
