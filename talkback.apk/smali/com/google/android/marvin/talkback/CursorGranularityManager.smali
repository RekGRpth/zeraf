.class public Lcom/google/android/marvin/talkback/CursorGranularityManager;
.super Ljava/lang/Object;
.source "CursorGranularityManager.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;
    }
.end annotation


# static fields
.field private static final GRANULARITIES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;


# instance fields
.field private final mArguments:Landroid/os/Bundle;

.field private mCurrentNodeIndex:I

.field private mLockedNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestedGranularityIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->values()[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->GRANULARITIES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNodes:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mArguments:Landroid/os/Bundle;

    return-void
.end method

.method private clearCurrentNode()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mArguments:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iput v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    iput v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNodes:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    new-array v0, v0, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-void
.end method

.method private static extractNavigableChildren(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/util/ArrayList;)I
    .locals 5
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            "Ljava/util/ArrayList",
            "<",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;)I"
        }
    .end annotation

    if-nez p0, :cond_1

    const/4 v3, 0x0

    :cond_0
    return v3

    :cond_1
    invoke-static {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getMovementGranularities()I

    move-result v3

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    invoke-virtual {p0, v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isActionableForAccessibility(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->extractNavigableChildren(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/util/ArrayList;)I

    move-result v4

    or-int/2addr v3, v4

    :cond_2
    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private manageLockedNode(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0, p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->clearCurrentNode()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v0, :cond_1

    invoke-static {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mLockedNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNodes:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->extractNavigableChildren(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/util/ArrayList;)I

    :cond_1
    return-void
.end method


# virtual methods
.method public adjustWithin(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    .locals 4
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->manageLockedNode(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    iget v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    sget-object v3, Lcom/google/android/marvin/talkback/CursorGranularityManager;->GRANULARITIES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    array-length v0, v3

    iget v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    add-int/2addr v3, p2

    iput v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    iget v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    if-gez v3, :cond_2

    add-int/lit8 v3, v0, -0x1

    iput v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    :cond_0
    :goto_0
    iget v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    if-eq v3, v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2

    :cond_2
    iget v3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    if-lt v3, v0, :cond_0

    iput v2, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    goto :goto_0
.end method

.method public getRequestedGranularity()Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;
    .locals 2

    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->GRANULARITIES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    iget v1, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public navigateWithin(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)I
    .locals 9
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # I

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->manageLockedNode(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNodes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget-object v5, Lcom/google/android/marvin/talkback/CursorGranularityManager;->GRANULARITIES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    iget v6, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    aget-object v2, v5, v6

    sget-object v5, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    if-ne v2, v5, :cond_1

    const/4 v3, -0x1

    :cond_0
    :goto_0
    return v3

    :cond_1
    sparse-switch p2, :sswitch_data_0

    :cond_2
    :goto_1
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    if-ltz v5, :cond_3

    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    if-ge v5, v0, :cond_3

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mNodes:Ljava/util/ArrayList;

    iget v6, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mArguments:Landroid/os/Bundle;

    const-string v6, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    iget v7, v2, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->id:I

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mArguments:Landroid/os/Bundle;

    invoke-virtual {v1, p2, v5}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v5

    if-nez v5, :cond_0

    sparse-switch p2, :sswitch_data_1

    :goto_2
    const/4 v5, 0x2

    const-string v6, "Failed to move with granularity %s, trying next node"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->name()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {p0, v5, v6, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_0
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    if-gez v5, :cond_2

    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    goto :goto_1

    :sswitch_1
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    if-lt v5, v0, :cond_2

    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    goto :goto_1

    :sswitch_2
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    goto :goto_2

    :sswitch_3
    iget v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mCurrentNodeIndex:I

    goto :goto_2

    :cond_3
    move v3, v4

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x100 -> :sswitch_2
        0x200 -> :sswitch_3
    .end sparse-switch
.end method

.method public requestGranularity(Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;)Z
    .locals 2
    .param p1    # Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularityManager;->GRANULARITIES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularityManager;->GRANULARITIES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_0

    iput v0, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager;->mRequestedGranularityIndex:I

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public shutdown()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->clearCurrentNode()V

    return-void
.end method
