.class public Lcom/googlecode/eyesfree/utils/FeedbackController;
.super Ljava/lang/Object;
.source "FeedbackController.java"


# instance fields
.field private mAuditoryEnabled:Z

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mHapticEnabled:Z

.field private final mPostLoadPlayables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mResourceIdToSoundMap:Landroid/util/SparseIntArray;

.field private final mResourceIdToVibrationPatternMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[J>;"
        }
    .end annotation
.end field

.field private final mResources:Landroid/content/res/Resources;

.field private final mSoundPool:Landroid/media/SoundPool;

.field private final mVibrator:Landroid/os/Vibrator;

.field private mVolume:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToVibrationPatternMap:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToSoundMap:Landroid/util/SparseIntArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mPostLoadPlayables:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mHapticEnabled:Z

    iput-boolean v3, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mAuditoryEnabled:Z

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVolume:F

    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResources:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    new-instance v0, Landroid/media/SoundPool;

    const/16 v1, 0xa

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v3}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mSoundPool:Landroid/media/SoundPool;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mSoundPool:Landroid/media/SoundPool;

    new-instance v1, Lcom/googlecode/eyesfree/utils/FeedbackController$1;

    invoke-direct {v1, p0}, Lcom/googlecode/eyesfree/utils/FeedbackController$1;-><init>(Lcom/googlecode/eyesfree/utils/FeedbackController;)V

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToSoundMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToVibrationPatternMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/MidiUtils;->purgeMidiTempFiles(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/utils/FeedbackController;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/googlecode/eyesfree/utils/FeedbackController;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mPostLoadPlayables:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getVibrationPattern(I)[J
    .locals 6
    .param p1    # I

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToVibrationPatternMap:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v4, 0x0

    new-array v0, v4, [J

    goto :goto_0

    :cond_1
    array-length v4, v2

    new-array v3, v4, [J

    const/4 v1, 0x0

    :goto_1
    array-length v4, v3

    if-ge v1, v4, :cond_2

    aget v4, v2, v1

    int-to-long v4, v4

    aput-wide v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToVibrationPatternMap:Landroid/util/SparseArray;

    invoke-virtual {v4, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v0, v3

    goto :goto_0
.end method


# virtual methods
.method public cancelVibration()V
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    :cond_0
    return-void
.end method

.method protected final getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public interrupt()V
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    return-void
.end method

.method public loadMidiSoundFromArray([IZ)I
    .locals 5
    .param p1    # [I
    .param p2    # Z

    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/googlecode/eyesfree/utils/MidiUtils;->generateMidiFileFromArray(Landroid/content/Context;[I)Ljava/io/File;

    move-result-object v0

    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v1

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mPostLoadPlayables:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return v1
.end method

.method public playMidiScale(IIIIII)Z
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mAuditoryEnabled:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-static/range {p1 .. p6}, Lcom/googlecode/eyesfree/utils/MidiUtils;->generateMidiScale(IIIIII)[I

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mContext:Landroid/content/Context;

    invoke-static {v5, v2}, Lcom/googlecode/eyesfree/utils/MidiUtils;->generateMidiFileFromArray(Landroid/content/Context;[I)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    new-instance v5, Lcom/googlecode/eyesfree/utils/FeedbackController$2;

    invoke-direct {v5, p0, v3}, Lcom/googlecode/eyesfree/utils/FeedbackController$2;-><init>(Lcom/googlecode/eyesfree/utils/FeedbackController;Landroid/media/MediaPlayer;)V

    invoke-virtual {v3, v5}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    new-instance v5, Lcom/googlecode/eyesfree/utils/FeedbackController$3;

    invoke-direct {v5, p0, v3, v1}, Lcom/googlecode/eyesfree/utils/FeedbackController$3;-><init>(Lcom/googlecode/eyesfree/utils/FeedbackController;Landroid/media/MediaPlayer;Ljava/io/File;)V

    invoke-virtual {v3, v5}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    const/4 v5, 0x3

    :try_start_0
    invoke-virtual {v3, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public playRepeatedVibration(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-boolean v1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mHapticEnabled:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/FeedbackController;->getVibrationPattern(I)[J

    move-result-object v0

    if-ltz p2, :cond_2

    array-length v1, v0

    if-lt p2, v1, :cond_3

    :cond_2
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v1, p2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v1

    :cond_3
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1, v0, p2}, Landroid/os/Vibrator;->vibrate([JI)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public playSound(IFF)Z
    .locals 9
    .param p1    # I
    .param p2    # F
    .param p3    # F

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mAuditoryEnabled:Z

    if-nez v0, :cond_1

    move v4, v5

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToSoundMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/FeedbackController;->preloadSound(I)I

    move-result v7

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mPostLoadPlayables:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToSoundMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    iget v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVolume:F

    mul-float v2, v0, p3

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mSoundPool:Landroid/media/SoundPool;

    move v3, v2

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v8

    if-nez v8, :cond_0

    move v4, v5

    goto :goto_0
.end method

.method public playVibration(I)Z
    .locals 3
    .param p1    # I

    iget-boolean v1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mHapticEnabled:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/FeedbackController;->getVibrationPattern(I)[J

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public preloadSound(I)I
    .locals 9
    .param p1    # I

    const/4 v8, 0x6

    const/4 v3, -0x1

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToSoundMap:Landroid/util/SparseIntArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v4

    if-ltz v4, :cond_0

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToSoundMap:Landroid/util/SparseIntArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "raw"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mSoundPool:Landroid/media/SoundPool;

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, p1, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    :goto_1
    if-gez v2, :cond_3

    const-string v4, "Failed to load sound: Invalid sound pool ID"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {p0, v8, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    goto :goto_0

    :cond_1
    const-string v4, "array"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    invoke-virtual {p0, v0, v7}, Lcom/googlecode/eyesfree/utils/FeedbackController;->loadMidiSoundFromArray([IZ)I

    move-result v2

    goto :goto_1

    :cond_2
    const-string v4, "Failed to load sound: Unknown resource type"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {p0, v8, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mResourceIdToSoundMap:Landroid/util/SparseIntArray;

    invoke-virtual {v3, p1, v2}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method public setAuditoryEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mAuditoryEnabled:Z

    return-void
.end method

.method public setHapticEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mHapticEnabled:Z

    return-void
.end method

.method public setVolume(I)V
    .locals 2
    .param p1    # I

    const/16 v0, 0x64

    const/4 v1, 0x0

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000

    div-float/2addr v0, v1

    iput v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVolume:F

    return-void
.end method

.method public shutdown()V
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/MidiUtils;->purgeMidiTempFiles(Landroid/content/Context;)V

    return-void
.end method
