.class public Lcom/googlecode/eyesfree/utils/MidiUtils;
.super Ljava/lang/Object;
.source "MidiUtils.java"


# static fields
.field private static final DEFAULT_TEMPO_TRACK:Lcom/leff/midi/MidiTrack;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x4

    new-instance v2, Lcom/leff/midi/MidiTrack;

    invoke-direct {v2}, Lcom/leff/midi/MidiTrack;-><init>()V

    sput-object v2, Lcom/googlecode/eyesfree/utils/MidiUtils;->DEFAULT_TEMPO_TRACK:Lcom/leff/midi/MidiTrack;

    new-instance v1, Lcom/leff/midi/event/meta/TimeSignature;

    invoke-direct {v1}, Lcom/leff/midi/event/meta/TimeSignature;-><init>()V

    new-instance v0, Lcom/leff/midi/event/meta/Tempo;

    invoke-direct {v0}, Lcom/leff/midi/event/meta/Tempo;-><init>()V

    const/16 v2, 0x18

    const/16 v3, 0x8

    invoke-virtual {v1, v4, v4, v2, v3}, Lcom/leff/midi/event/meta/TimeSignature;->setTimeSignature(IIII)V

    const/high16 v2, 0x42be0000

    invoke-virtual {v0, v2}, Lcom/leff/midi/event/meta/Tempo;->setBpm(F)V

    sget-object v2, Lcom/googlecode/eyesfree/utils/MidiUtils;->DEFAULT_TEMPO_TRACK:Lcom/leff/midi/MidiTrack;

    invoke-virtual {v2, v1}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    sget-object v2, Lcom/googlecode/eyesfree/utils/MidiUtils;->DEFAULT_TEMPO_TRACK:Lcom/leff/midi/MidiTrack;

    invoke-virtual {v2, v0}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    return-void
.end method

.method public static generateMidiFileFromArray(Landroid/content/Context;[I)Ljava/io/File;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # [I

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/MidiUtils;->readMidiTrackFromArray([I)Lcom/leff/midi/MidiTrack;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/MidiUtils;->writeMidiTrackToTempFile(Landroid/content/Context;Lcom/leff/midi/MidiTrack;)Ljava/io/File;

    move-result-object v1

    goto :goto_0
.end method

.method public static generateMidiScale(IIIIII)[I
    .locals 10
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    if-lez p4, :cond_0

    if-gtz p2, :cond_2

    :cond_0
    const/4 v3, 0x0

    :cond_1
    return-object v3

    :cond_2
    const/4 v8, 0x5

    if-ne p5, v8, :cond_3

    div-int/lit8 v0, p4, 0x5

    rem-int/lit8 v7, p4, 0x5

    mul-int/lit8 v9, v0, 0x2

    const/4 v8, 0x3

    if-le v7, v8, :cond_4

    const/4 v8, 0x1

    :goto_0
    add-int/2addr v8, v9

    add-int/2addr p4, v8

    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v4, p3

    const/4 v1, 0x1

    :goto_1
    if-gt v1, p4, :cond_5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    rem-int/lit8 v5, v1, 0x7

    packed-switch v5, :pswitch_data_0

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v8, 0x0

    goto :goto_0

    :pswitch_0
    add-int/lit8 v4, v4, 0x2

    goto :goto_2

    :pswitch_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    const/4 v8, 0x2

    if-eq p5, v8, :cond_6

    const/4 v8, 0x3

    if-eq p5, v8, :cond_6

    const/4 v8, 0x4

    if-ne p5, v8, :cond_9

    :cond_6
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v1, v8, :cond_b

    add-int/lit8 v8, v1, 0x1

    rem-int/lit8 v5, v8, 0x7

    sparse-switch v5, :sswitch_data_0

    :cond_7
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :sswitch_0
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v1, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_4

    :sswitch_1
    const/4 v8, 0x2

    if-eq p5, v8, :cond_8

    const/4 v8, 0x3

    if-ne p5, v8, :cond_7

    :cond_8
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v1, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_4

    :sswitch_2
    const/4 v8, 0x2

    if-ne p5, v8, :cond_7

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v1, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_4

    :cond_9
    const/4 v8, 0x5

    if-ne p5, v8, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v1, v8, :cond_a

    add-int/lit8 v8, v1, 0x1

    rem-int/lit8 v5, v8, 0x7

    sparse-switch v5, :sswitch_data_1

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :sswitch_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_7
    if-lez v1, :cond_b

    add-int/lit8 v8, v1, -0x1

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, -0x1

    goto :goto_7

    :cond_b
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    mul-int/lit8 v8, v8, 0x3

    add-int/lit8 v8, v8, 0x1

    new-array v3, v8, [I

    const/4 v8, 0x0

    aput p0, v3, v8

    const/4 v1, 0x1

    :goto_8
    array-length v8, v3

    if-ge v1, v8, :cond_1

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v3, v1

    add-int/lit8 v8, v1, 0x1

    aput p1, v3, v8

    add-int/lit8 v8, v1, 0x2

    aput p2, v3, v8

    add-int/lit8 v1, v1, 0x3

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x3 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0x4 -> :sswitch_3
    .end sparse-switch
.end method

.method public static purgeMidiTempFiles(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    new-instance v4, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    const-string v7, "midi"

    invoke-direct {v4, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    move-object v0, v5

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    return-void
.end method

.method private static readMidiTrackFromArray([I)Lcom/leff/midi/MidiTrack;
    .locals 13
    .param p0    # [I

    new-instance v10, Lcom/leff/midi/MidiTrack;

    invoke-direct {v10}, Lcom/leff/midi/MidiTrack;-><init>()V

    const/4 v12, 0x0

    const/4 v0, 0x0

    aget v11, p0, v0

    if-ltz v11, :cond_0

    const/16 v0, 0x7f

    if-le v11, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MIDI track program must be in the range [0,127]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/leff/midi/event/Controller;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/16 v7, 0x7f

    invoke-direct/range {v0 .. v7}, Lcom/leff/midi/event/Controller;-><init>(JJIII)V

    invoke-virtual {v10, v0}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    new-instance v0, Lcom/leff/midi/event/Controller;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x5b

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/leff/midi/event/Controller;-><init>(JJIII)V

    invoke-virtual {v10, v0}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    new-instance v0, Lcom/leff/midi/event/Controller;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x5d

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/leff/midi/event/Controller;-><init>(JJIII)V

    invoke-virtual {v10, v0}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    new-instance v0, Lcom/leff/midi/event/ProgramChange;

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    invoke-direct {v0, v4, v5, v1, v11}, Lcom/leff/midi/event/ProgramChange;-><init>(JII)V

    invoke-virtual {v10, v0}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    array-length v0, p0

    rem-int/lit8 v0, v0, 0x3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MIDI note array must contain a single integer followed by triplets"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v9, 0x1

    :goto_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x2

    if-ge v9, v0, :cond_7

    aget v2, p0, v9

    const/16 v0, 0x15

    if-lt v2, v0, :cond_3

    const/16 v0, 0x6c

    if-le v2, v0, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MIDI note pitch must be in the range [21,108]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    add-int/lit8 v0, v9, 0x1

    aget v3, p0, v0

    if-ltz v3, :cond_5

    const/16 v0, 0x7f

    if-le v3, v0, :cond_6

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MIDI note velocity must be in the range [0,127]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    add-int/lit8 v0, v9, 0x2

    aget v8, p0, v0

    const/4 v1, 0x0

    int-to-long v4, v12

    int-to-long v6, v8

    move-object v0, v10

    invoke-virtual/range {v0 .. v7}, Lcom/leff/midi/MidiTrack;->insertNote(IIIJJ)V

    add-int/2addr v12, v8

    add-int/lit8 v9, v9, 0x3

    goto :goto_0

    :cond_7
    return-object v10
.end method

.method private static writeMidiTrackToTempFile(Landroid/content/Context;Lcom/leff/midi/MidiTrack;)Ljava/io/File;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/leff/midi/MidiTrack;

    const/4 v5, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/googlecode/eyesfree/utils/MidiUtils;->DEFAULT_TEMPO_TRACK:Lcom/leff/midi/MidiTrack;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    const-string v7, "midi"

    invoke-direct {v2, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v6

    if-nez v6, :cond_0

    move-object v3, v5

    :goto_0
    return-object v3

    :cond_0
    new-instance v1, Lcom/leff/midi/MidiFile;

    const/16 v6, 0x1e0

    invoke-direct {v1, v6, v4}, Lcom/leff/midi/MidiFile;-><init>(ILjava/util/ArrayList;)V

    const-string v6, "talkback"

    const-string v7, ".mid"

    invoke-static {v6, v7, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/leff/midi/MidiFile;->writeToFile(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v5

    goto :goto_0
.end method
