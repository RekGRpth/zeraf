.class Lcom/googlecode/eyesfree/utils/FeedbackController$1;
.super Ljava/lang/Object;
.source "FeedbackController.java"

# interfaces
.implements Landroid/media/SoundPool$OnLoadCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/utils/FeedbackController;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/utils/FeedbackController;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/utils/FeedbackController;)V
    .locals 0

    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$1;->this$0:Lcom/googlecode/eyesfree/utils/FeedbackController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .locals 8
    .param p1    # Landroid/media/SoundPool;
    .param p2    # I
    .param p3    # I

    if-nez p3, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$1;->this$0:Lcom/googlecode/eyesfree/utils/FeedbackController;

    # getter for: Lcom/googlecode/eyesfree/utils/FeedbackController;->mPostLoadPlayables:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/FeedbackController;->access$000(Lcom/googlecode/eyesfree/utils/FeedbackController;)Ljava/util/ArrayList;

    move-result-object v7

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$1;->this$0:Lcom/googlecode/eyesfree/utils/FeedbackController;

    # getter for: Lcom/googlecode/eyesfree/utils/FeedbackController;->mPostLoadPlayables:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/FeedbackController;->access$000(Lcom/googlecode/eyesfree/utils/FeedbackController;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v2, 0x3f800000

    const/high16 v3, 0x3f800000

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    move-object v0, p1

    move v1, p2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$1;->this$0:Lcom/googlecode/eyesfree/utils/FeedbackController;

    # getter for: Lcom/googlecode/eyesfree/utils/FeedbackController;->mPostLoadPlayables:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/FeedbackController;->access$000(Lcom/googlecode/eyesfree/utils/FeedbackController;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v7

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
