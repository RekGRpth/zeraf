.class public Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoRef.java"


# instance fields
.field private mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mOwned:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V
    .locals 0
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iput-boolean p2, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mOwned:Z

    return-void
.end method

.method private getChildNumber(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)I
    .locals 5
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, -0x1

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    if-gez v3, :cond_2

    invoke-virtual {p1, v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return v3
.end method

.method public static obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;
    .locals 3
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-static {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;-><init>(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V

    return-object v0
.end method

.method public static unOwned(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;
    .locals 2
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz p0, :cond_0

    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;-><init>(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public firstChild()Z
    .locals 5

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_0

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public get()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-object v0
.end method

.method public lastChild()Z
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v4

    if-ge v4, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    :goto_1
    if-ltz v0, :cond_0

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public lastDescendant()Z
    .locals 1

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->lastChild()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->lastChild()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public nextInOrder()Z
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->firstChild()Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->nextSibling()Z

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->unOwned(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    move-result-object v0

    :cond_4
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->parent()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->nextSibling()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;)V

    move v1, v2

    goto :goto_0
.end method

.method public nextSibling()Z
    .locals 7

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v6, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v6}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    if-eqz v4, :cond_0

    :try_start_0
    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v0

    invoke-direct {p0, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->getChildNumber(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-gez v1, :cond_2

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v1, 0x1

    :goto_1
    if-ge v2, v0, :cond_5

    :try_start_1
    invoke-virtual {v4, v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    if-nez v3, :cond_3

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {p0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v5, 0x1

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_4
    :try_start_3
    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    throw v5
.end method

.method public parent()Z
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    move-object v0, v1

    goto :goto_1
.end method

.method public previousInOrder()Z
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->previousSibling()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->lastDescendant()Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->parent()Z

    move-result v0

    goto :goto_0
.end method

.method public previousSibling()Z
    .locals 6

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v5}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    if-eqz v3, :cond_0

    :try_start_0
    invoke-direct {p0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->getChildNumber(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-ltz v1, :cond_4

    invoke-virtual {v3, v1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-nez v2, :cond_2

    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, 0x1

    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-virtual {v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    throw v4
.end method

.method public release()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mOwned:Z

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-object v0
.end method

.method public reset(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mOwned:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :cond_0
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mOwned:Z

    return-void
.end method

.method public reset(Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;)V
    .locals 1
    .param p1    # Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->get()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->reset(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    iget-boolean v0, p1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mOwned:Z

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mOwned:Z

    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->mOwned:Z

    return-void
.end method
