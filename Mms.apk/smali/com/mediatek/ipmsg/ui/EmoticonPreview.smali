.class public Lcom/mediatek/ipmsg/ui/EmoticonPreview;
.super Ljava/lang/Object;
.source "EmoticonPreview.java"


# instance fields
.field private mContentView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mImage:Lcom/mediatek/ipmsg/ui/GifView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsDismissed:Z

.field private mParent:Landroid/view/View;

.field private mPopWindow:Landroid/widget/PopupWindow;

.field private mResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mIsDismissed:Z

    iput-object p1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mParent:Landroid/view/View;

    invoke-direct {p0}, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->constructRecordWinsow()V

    return-void
.end method

.method private constructRecordWinsow()V
    .locals 5

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040026

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContentView:Landroid/view/View;

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContentView:Landroid/view/View;

    const v3, 0x7f0f00aa

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/mediatek/ipmsg/ui/GifView;

    iput-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mImage:Lcom/mediatek/ipmsg/ui/GifView;

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContentView:Landroid/view/View;

    invoke-direct {v2, v3, v1, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mPopWindow:Landroid/widget/PopupWindow;

    return-void
.end method


# virtual methods
.method public dissWindow()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mIsDismissed:Z

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mPopWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method

.method public isShow()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mIsDismissed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEmoticon(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mResId:I

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mImage:Lcom/mediatek/ipmsg/ui/GifView;

    invoke-virtual {v0, p1}, Lcom/mediatek/ipmsg/ui/GifView;->setSource(I)V

    return-void
.end method

.method public setEmoticon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public showWindow()V
    .locals 5

    const/16 v4, 0x31

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mIsDismissed:Z

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mPopWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mParent:Landroid/view/View;

    invoke-virtual {v1, v2, v4, v3, v0}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mPopWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mPopWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mPopWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/EmoticonPreview;->mParent:Landroid/view/View;

    invoke-virtual {v1, v2, v4, v3, v0}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method
