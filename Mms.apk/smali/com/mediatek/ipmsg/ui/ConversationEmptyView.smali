.class public Lcom/mediatek/ipmsg/ui/ConversationEmptyView;
.super Landroid/widget/LinearLayout;
.source "ConversationEmptyView.java"


# instance fields
.field private mActivate:Landroid/widget/LinearLayout;

.field private mBackground:Landroid/widget/LinearLayout;

.field private mBtnActivate:Landroid/widget/Button;

.field private mContent:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mConvertView:Landroid/view/View;

.field private mGroupChat:Landroid/widget/RelativeLayout;

.field private mImportant:Landroid/widget/LinearLayout;

.field private mSpam:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040010

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mConvertView:Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mConvertView:Landroid/view/View;

    const v2, 0x7f0f0053

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mBackground:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mConvertView:Landroid/view/View;

    const v2, 0x7f0f0054

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mConvertView:Landroid/view/View;

    const v2, 0x7f0f005d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mImportant:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mConvertView:Landroid/view/View;

    const v2, 0x7f0f0055

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mSpam:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mConvertView:Landroid/view/View;

    const v2, 0x7f0f0059

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mGroupChat:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mConvertView:Landroid/view/View;

    const v2, 0x7f0f0060

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mActivate:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mConvertView:Landroid/view/View;

    const v2, 0x7f0f0062

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mBtnActivate:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mBtnActivate:Landroid/widget/Button;

    new-instance v2, Lcom/mediatek/ipmsg/ui/ConversationEmptyView$1;

    invoke-direct {v2, p0, p1}, Lcom/mediatek/ipmsg/ui/ConversationEmptyView$1;-><init>(Lcom/mediatek/ipmsg/ui/ConversationEmptyView;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setActivate(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mActivate:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mActivate:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public setAllChatEmpty()V
    .locals 4

    const/16 v3, 0x8

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mBackground:Landroid/widget/LinearLayout;

    const v1, 0x7f07001c

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0xae

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mSpam:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mImportant:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mGroupChat:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setGroupChatEmpty(Z)V
    .locals 3
    .param p1    # Z

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mSpam:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mImportant:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mGroupChat:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mBackground:Landroid/widget/LinearLayout;

    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0xac

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->setActivate(Z)V

    return-void
.end method

.method public setImportantEmpty(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mSpam:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mImportant:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mGroupChat:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mBackground:Landroid/widget/LinearLayout;

    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0xad

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->setActivate(Z)V

    return-void
.end method

.method public setSpamEmpty(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mSpam:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mImportant:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mGroupChat:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mBackground:Landroid/widget/LinearLayout;

    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0xab

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/mediatek/ipmsg/ui/ConversationEmptyView;->setActivate(Z)V

    return-void
.end method
