.class Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;
.super Landroid/widget/BaseAdapter;
.source "SharePanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/ipmsg/ui/SharePanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShareAdapter"
.end annotation


# instance fields
.field private mIconArray:[I

.field private mStringArray:[Ljava/lang/String;

.field final synthetic this$0:Lcom/mediatek/ipmsg/ui/SharePanel;


# direct methods
.method public constructor <init>(Lcom/mediatek/ipmsg/ui/SharePanel;[Ljava/lang/String;[I)V
    .locals 0
    .param p2    # [Ljava/lang/String;
    .param p3    # [I

    iput-object p1, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->this$0:Lcom/mediatek/ipmsg/ui/SharePanel;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->mStringArray:[Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->mIconArray:[I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->this$0:Lcom/mediatek/ipmsg/ui/SharePanel;

    invoke-static {v1}, Lcom/mediatek/ipmsg/ui/SharePanel;->access$1000(Lcom/mediatek/ipmsg/ui/SharePanel;)I

    move-result v1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->this$0:Lcom/mediatek/ipmsg/ui/SharePanel;

    invoke-static {v1}, Lcom/mediatek/ipmsg/ui/SharePanel;->access$1100(Lcom/mediatek/ipmsg/ui/SharePanel;)[I

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    mul-int/lit8 v0, v1, 0x2

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->this$0:Lcom/mediatek/ipmsg/ui/SharePanel;

    invoke-static {v1}, Lcom/mediatek/ipmsg/ui/SharePanel;->access$1100(Lcom/mediatek/ipmsg/ui/SharePanel;)[I

    move-result-object v1

    aget v0, v1, v2

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    if-nez p2, :cond_2

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->this$0:Lcom/mediatek/ipmsg/ui/SharePanel;

    invoke-static {v2}, Lcom/mediatek/ipmsg/ui/SharePanel;->access$400(Lcom/mediatek/ipmsg/ui/SharePanel;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->this$0:Lcom/mediatek/ipmsg/ui/SharePanel;

    invoke-static {v2}, Lcom/mediatek/ipmsg/ui/SharePanel;->access$400(Lcom/mediatek/ipmsg/ui/SharePanel;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04005d

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :goto_0
    invoke-virtual {p2, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    const v2, 0x7f0f014f

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0f014e

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->mStringArray:[Ljava/lang/String;

    array-length v2, v2

    if-ge p1, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->mStringArray:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->mIconArray:[I

    aget v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-object p2

    :cond_1
    iget-object v2, p0, Lcom/mediatek/ipmsg/ui/SharePanel$ShareAdapter;->this$0:Lcom/mediatek/ipmsg/ui/SharePanel;

    invoke-static {v2}, Lcom/mediatek/ipmsg/ui/SharePanel;->access$400(Lcom/mediatek/ipmsg/ui/SharePanel;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04005e

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    goto :goto_1
.end method
