.class final Lcom/mediatek/ipmsg/util/IpMessageUtils$3;
.super Ljava/lang/Object;
.source "IpMessageUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/ipmsg/util/IpMessageUtils;->showSimSelectionDialog(Landroid/app/Activity;Ljava/util/List;ILandroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activityContext:Landroid/app/Activity;

.field final synthetic val$currentSimInfoList:Ljava/util/List;

.field final synthetic val$ipMsgHandler:Landroid/os/Handler;

.field final synthetic val$mode:I


# direct methods
.method constructor <init>(ILandroid/app/Activity;Ljava/util/List;Landroid/os/Handler;)V
    .locals 0

    iput p1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$mode:I

    iput-object p2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$activityContext:Landroid/app/Activity;

    iput-object p3, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$currentSimInfoList:Ljava/util/List;

    iput-object p4, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$ipMsgHandler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v5, 0x1

    iget v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$mode:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$activityContext:Landroid/app/Activity;

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$currentSimInfoList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v1}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v3

    long-to-int v1, v3

    invoke-static {v2, v5, v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->access$200(Landroid/app/Activity;II)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$activityContext:Landroid/app/Activity;

    invoke-static {v1}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v2

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$currentSimInfoList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v1}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v3

    long-to-int v1, v3

    invoke-virtual {v2, v1}, Lcom/mediatek/mms/ipmessage/ServiceManager;->enableIpService(I)V

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$activityContext:Landroid/app/Activity;

    iget-object v2, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$activityContext:Landroid/app/Activity;

    invoke-static {v2}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v2

    const/16 v3, 0x94

    invoke-virtual {v2, v3}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$ipMsgHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/mediatek/ipmsg/util/IpMessageUtils$3;->val$ipMsgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
