.class public Lcom/mediatek/mms/ext/MmsTransactionImpl;
.super Landroid/content/ContextWrapper;
.source "MmsTransactionImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsTransaction;


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MmsTransactionImpl"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ext/MmsTransactionImpl;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/mms/ext/MmsTransactionImpl;->mContext:Landroid/content/Context;

    return-void
.end method

.method private static isTransientFailure(I)Z
    .locals 3
    .param p0    # I

    const-string v0, "Mms/MmsTransactionImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isTransientFailure, type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xa

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getHttpRequestRetryHandler()Lorg/apache/http/impl/client/DefaultHttpRequestRetryHandler;
    .locals 3

    const/4 v2, 0x1

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "getHttpRequestRetryHandler"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpRequestRetryHandler;

    invoke-direct {v0, v2, v2}, Lorg/apache/http/impl/client/DefaultHttpRequestRetryHandler;-><init>(IZ)V

    return-object v0
.end method

.method public getMmsServerFailCount()I
    .locals 2

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "getMmsServerFailCount"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isPendingMmsNeedRestart(Landroid/net/Uri;I)Z
    .locals 12
    .param p1    # Landroid/net/Uri;
    .param p2    # I

    const/4 v5, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    const-string v1, "Mms/MmsTransactionImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPendingMmsNeedRestart, uri="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x2

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "msg_box"

    aput-object v1, v2, v11

    const-string v1, "m_id"

    aput-object v1, v2, v10

    const-string v1, "st"

    aput-object v1, v2, v5

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/mediatek/mms/ext/MmsTransactionImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v10, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    const-string v1, "Mms/MmsTransactionImpl"

    const-string v3, "Bad uri"

    invoke-static {v1, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    move v1, v10

    :cond_2
    :goto_0
    return v1

    :cond_3
    const/4 v1, 0x2

    :try_start_1
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const-string v1, "Mms/MmsTransactionImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v9, :cond_5

    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    move v1, v10

    goto :goto_0

    :cond_5
    and-int/lit8 v1, v9, 0x4

    if-nez v1, :cond_6

    :try_start_2
    invoke-static {p2}, Lcom/mediatek/mms/ext/MmsTransactionImpl;->isTransientFailure(I)Z
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_6
    if-eqz v7, :cond_7

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_7
    move v1, v11

    goto :goto_0

    :catch_0
    move-exception v8

    :try_start_3
    const-string v1, "Mms/MmsTransactionImpl"

    const-string v3, "Catch a SQLiteException when query: "

    invoke-static {v1, v3, v8}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p2}, Lcom/mediatek/mms/ext/MmsTransactionImpl;->isTransientFailure(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v1

    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v7, :cond_8

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v1
.end method

.method public isRestartPendingsEnabled()Z
    .locals 2

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "isRestartPendingsEnabled"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isSyncStartPdpEnabled()Z
    .locals 2

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "isSyncStartPdpEnabled"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public setMmsServerStatusCode(I)V
    .locals 2
    .param p1    # I

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "setMmsServerStatusCode"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSoSendTimeoutProperty()V
    .locals 2

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "setSoSendTimeoutProperty"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public startServiceForeground(Landroid/app/Service;)V
    .locals 2
    .param p1    # Landroid/app/Service;

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "startServiceForeground"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public stopServiceForeground(Landroid/app/Service;)V
    .locals 2
    .param p1    # Landroid/app/Service;

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "stopServiceForeground"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public updateConnection()Z
    .locals 2

    const-string v0, "Mms/MmsTransactionImpl"

    const-string v1, "updateConnection"

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method
