.class public Lcom/mediatek/mms/ext/MmsAttachmentEnhanceImpl;
.super Landroid/content/ContextWrapper;
.source "MmsAttachmentEnhanceImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/Op01MmsAttachmentEnhanceExt"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getSaveAttachMode(Landroid/content/Intent;)I
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, -0x1

    return v0
.end method

.method public isSupportAttachmentEnhance()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setAttachmentName(Landroid/widget/TextView;I)V
    .locals 0
    .param p1    # Landroid/widget/TextView;
    .param p2    # I

    return-void
.end method

.method public setSaveAttachIntent(Landroid/content/Intent;I)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    return-void
.end method
