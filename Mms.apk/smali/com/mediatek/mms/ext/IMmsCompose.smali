.class public interface abstract Lcom/mediatek/mms/ext/IMmsCompose;
.super Ljava/lang/Object;
.source "IMmsCompose.java"


# static fields
.field public static final CAPTURE_PIC_AT_TEMP_FILE:I = 0x1

.field public static final CAPTURE_PIC_NORMAL:I


# virtual methods
.method public abstract addContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;Ljava/lang/CharSequence;)V
.end method

.method public abstract configSubjectEditor(Landroid/widget/EditText;)V
.end method

.method public abstract getCapturePicMode()I
.end method

.method public abstract init(Lcom/mediatek/mms/ext/IMmsComposeHost;)V
.end method

.method public abstract isAddMmsUrlToBookMark()Z
.end method

.method public abstract isAppendSender()Z
.end method
