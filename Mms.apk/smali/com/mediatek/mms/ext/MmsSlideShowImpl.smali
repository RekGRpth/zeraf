.class public Lcom/mediatek/mms/ext/MmsSlideShowImpl;
.super Landroid/content/ContextWrapper;
.source "MmsSlideShowImpl.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsSlideShow;


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/MmsSlideShowImpl"


# instance fields
.field private mHost:Lcom/mediatek/mms/ext/IMmsSlideShowHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ext/MmsSlideShowImpl;->mHost:Lcom/mediatek/mms/ext/IMmsSlideShowHost;

    return-void
.end method


# virtual methods
.method public addContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p4    # Ljava/lang/CharSequence;

    return-void
.end method

.method public configTextView(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/TextView;

    return-void
.end method

.method public configVideoView(Landroid/widget/VideoView;)V
    .locals 0
    .param p1    # Landroid/widget/VideoView;

    return-void
.end method

.method protected getHost()Lcom/mediatek/mms/ext/IMmsSlideShowHost;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ext/MmsSlideShowImpl;->mHost:Lcom/mediatek/mms/ext/IMmsSlideShowHost;

    return-object v0
.end method

.method public getInitialPlayState()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public init(Lcom/mediatek/mms/ext/IMmsSlideShowHost;)V
    .locals 0
    .param p1    # Lcom/mediatek/mms/ext/IMmsSlideShowHost;

    iput-object p1, p0, Lcom/mediatek/mms/ext/MmsSlideShowImpl;->mHost:Lcom/mediatek/mms/ext/IMmsSlideShowHost;

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    return v0
.end method
