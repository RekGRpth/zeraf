.class public final Lcom/mediatek/mms/ipmessage/IpMessageConsts$string;
.super Ljava/lang/Object;
.source "IpMessageConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mms/ipmessage/IpMessageConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ipmsg_activate:I = 0xb8

.field public static final ipmsg_activate_chat_frequently:I = 0xb9

.field public static final ipmsg_activate_message:I = 0x96

.field public static final ipmsg_activate_note:I = 0xba

.field public static final ipmsg_activate_title:I = 0x93

.field public static final ipmsg_active:I = 0x8d

.field public static final ipmsg_agree_and_continue:I = 0x92

.field public static final ipmsg_allchat_empty:I = 0xae

.field public static final ipmsg_cancel:I = 0x66

.field public static final ipmsg_cant_save:I = 0xcb

.field public static final ipmsg_cant_share:I = 0xc5

.field public static final ipmsg_caption_hint:I = 0xb1

.field public static final ipmsg_chat_setting_saving:I = 0x7b

.field public static final ipmsg_chat_setting_sending:I = 0x7c

.field public static final ipmsg_chat_setting_updating:I = 0x76

.field public static final ipmsg_choose_audio:I = 0x72

.field public static final ipmsg_choose_photo:I = 0x6f

.field public static final ipmsg_choose_video:I = 0x70

.field public static final ipmsg_continue:I = 0xea

.field public static final ipmsg_conversation_list_all:I = 0x81

.field public static final ipmsg_conversation_list_group_chats:I = 0x83

.field public static final ipmsg_conversation_list_important:I = 0x82

.field public static final ipmsg_conversation_list_spam:I = 0x84

.field public static final ipmsg_convert_to_ipmsg:I = 0xdb

.field public static final ipmsg_convert_to_mms:I = 0xd9

.field public static final ipmsg_convert_to_mms_for_recipients:I = 0xde

.field public static final ipmsg_convert_to_mms_for_service:I = 0xdc

.field public static final ipmsg_convert_to_sms:I = 0xda

.field public static final ipmsg_convert_to_sms_for_recipients:I = 0xdf

.field public static final ipmsg_convert_to_sms_for_service:I = 0xdd

.field public static final ipmsg_current_sim_enabled:I = 0x94

.field public static final ipmsg_dailog_multi_forward:I = 0x9e

.field public static final ipmsg_delete:I = 0xd4

.field public static final ipmsg_delete_important:I = 0x75

.field public static final ipmsg_dialog_clear_description:I = 0x7a

.field public static final ipmsg_dialog_clear_title:I = 0x79

.field public static final ipmsg_dialog_email_description:I = 0x78

.field public static final ipmsg_dialog_email_title:I = 0x7e

.field public static final ipmsg_dialog_save_description:I = 0x77

.field public static final ipmsg_dialog_save_title:I = 0x7d

.field public static final ipmsg_dialog_send_result:I = 0x9f

.field public static final ipmsg_dismiss:I = 0xb5

.field public static final ipmsg_dismiss_content:I = 0xbb

.field public static final ipmsg_divider_never_online:I = 0xbe

.field public static final ipmsg_divider_offline:I = 0xbd

.field public static final ipmsg_divider_online:I = 0xbc

.field public static final ipmsg_dlg_send_all:I = 0xa0

.field public static final ipmsg_dlg_send_failed:I = 0xa2

.field public static final ipmsg_dlg_send_sucess:I = 0xa1

.field public static final ipmsg_download_history_dlg:I = 0xec

.field public static final ipmsg_draw_sketch:I = 0x6d

.field public static final ipmsg_enable:I = 0xb6

.field public static final ipmsg_enable_chat_frequently:I = 0xb3

.field public static final ipmsg_enable_message:I = 0x98

.field public static final ipmsg_enable_notice:I = 0xb7

.field public static final ipmsg_enable_title:I = 0x97

.field public static final ipmsg_err_file:I = 0xe7

.field public static final ipmsg_export_to_sdcard:I = 0xd0

.field public static final ipmsg_failed_title:I = 0xc0

.field public static final ipmsg_file_limit:I = 0xc6

.field public static final ipmsg_forward_failed:I = 0xa7

.field public static final ipmsg_forward_norecipients:I = 0xa8

.field public static final ipmsg_getsim_failed:I = 0xa5

.field public static final ipmsg_groupchat_empty:I = 0xac

.field public static final ipmsg_hint:I = 0xb0

.field public static final ipmsg_important_empty:I = 0xad

.field public static final ipmsg_introduction:I = 0x87

.field public static final ipmsg_invalid_file_type:I = 0xcc

.field public static final ipmsg_invite:I = 0xb4

.field public static final ipmsg_invite_chat_frequently:I = 0xb2

.field public static final ipmsg_invite_friends_content:I = 0xe9

.field public static final ipmsg_invite_friends_to_chat:I = 0xcd

.field public static final ipmsg_invite_friends_to_ipmsg:I = 0xed

.field public static final ipmsg_invite_friends_to_ipmsg_dialog_msg:I = 0x8c

.field public static final ipmsg_ip_message:I = 0x65

.field public static final ipmsg_ipmsg:I = 0x8a

.field public static final ipmsg_keep_mms:I = 0xe2

.field public static final ipmsg_keep_sms:I = 0xe3

.field public static final ipmsg_load_all_message:I = 0x6a

.field public static final ipmsg_logo:I = 0xca

.field public static final ipmsg_mark_as_important:I = 0xd6

.field public static final ipmsg_mark_as_spam_tips:I = 0x85

.field public static final ipmsg_mms_convert_to_ipmsg:I = 0xe1

.field public static final ipmsg_multi_forward_failed_part:I = 0xef

.field public static final ipmsg_multi_forward_no_sim:I = 0xa4

.field public static final ipmsg_multi_forward_sim_info:I = 0xa3

.field public static final ipmsg_multi_forward_tips_content:I = 0xee

.field public static final ipmsg_multiforward_no_message:I = 0xa9

.field public static final ipmsg_need_input_recipients:I = 0xaa

.field public static final ipmsg_no_app:I = 0xc7

.field public static final ipmsg_no_internet:I = 0x69

.field public static final ipmsg_no_sdcard:I = 0x8b

.field public static final ipmsg_no_sim_card:I = 0xeb

.field public static final ipmsg_no_such_file:I = 0x67

.field public static final ipmsg_not_delivered_title:I = 0xbf

.field public static final ipmsg_over_file_limit:I = 0x68

.field public static final ipmsg_record_audio:I = 0x71

.field public static final ipmsg_record_video:I = 0x6c

.field public static final ipmsg_remove_from_important:I = 0xd7

.field public static final ipmsg_replace_attach:I = 0xe5

.field public static final ipmsg_replace_attach_msg:I = 0xe6

.field public static final ipmsg_resend_discard_message:I = 0xe8

.field public static final ipmsg_resend_via_mms:I = 0xc4

.field public static final ipmsg_resend_via_sms:I = 0xc3

.field public static final ipmsg_retry:I = 0xd3

.field public static final ipmsg_save_chat_history_failed:I = 0x7f

.field public static final ipmsg_save_file:I = 0xd8

.field public static final ipmsg_send_chat_history_failed:I = 0x80

.field public static final ipmsg_send_via_mms:I = 0xd2

.field public static final ipmsg_send_via_text_msg:I = 0xd1

.field public static final ipmsg_service_title:I = 0x88

.field public static final ipmsg_share:I = 0xd5

.field public static final ipmsg_share_calendar:I = 0x74

.field public static final ipmsg_share_contact:I = 0x6e

.field public static final ipmsg_share_location:I = 0x73

.field public static final ipmsg_share_title:I = 0xc9

.field public static final ipmsg_sim_selected_dialog_title_for_activate:I = 0x95

.field public static final ipmsg_sim_selected_dialog_title_for_enable:I = 0x99

.field public static final ipmsg_sim_status_error:I = 0xa6

.field public static final ipmsg_sms_convert_to_ipmsg:I = 0xe0

.field public static final ipmsg_spam_empty:I = 0xab

.field public static final ipmsg_switch:I = 0xe4

.field public static final ipmsg_switch_sim_button:I = 0x9c

.field public static final ipmsg_switch_sim_message:I = 0x9b

.field public static final ipmsg_switch_sim_successfully:I = 0x9d

.field public static final ipmsg_switch_sim_title:I = 0x9a

.field public static final ipmsg_take_photo:I = 0x6b

.field public static final ipmsg_term_key:I = 0x91

.field public static final ipmsg_term_warn_activate:I = 0x90

.field public static final ipmsg_term_warn_welcome:I = 0x8f

.field public static final ipmsg_try_again:I = 0xc1

.field public static final ipmsg_try_all_again:I = 0xc2

.field public static final ipmsg_type_to_compose_text:I = 0x89

.field public static final ipmsg_typing:I = 0x86

.field public static final ipmsg_typing_text:I = 0xc8

.field public static final ipmsg_vcard_file_name:I = 0xaf

.field public static final ipmsg_view_all_location:I = 0xcf

.field public static final ipmsg_view_all_media:I = 0xce

.field public static final ipmsg_welcome_active:I = 0x8e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
