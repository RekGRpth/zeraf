.class public Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;
.super Lcom/mediatek/mms/ipmessage/message/IpMessage;
.source "IpAttachMessage.java"


# instance fields
.field private mPath:Ljava/lang/String;

.field private mSize:I

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/mms/ipmessage/message/IpMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->mSize:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isInboxMsgDownloalable()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->mPath:Ljava/lang/String;

    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->mSize:I

    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->mUrl:Ljava/lang/String;

    return-void
.end method
