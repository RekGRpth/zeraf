.class public Lcom/mediatek/mms/ipmessage/message/IpVoiceMessage;
.super Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;
.source "IpVoiceMessage.java"


# instance fields
.field private mCaption:Ljava/lang/String;

.field private mDurationTime:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getCaption()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpVoiceMessage;->mCaption:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpVoiceMessage;->mDurationTime:I

    return v0
.end method

.method public setCaption(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpVoiceMessage;->mCaption:Ljava/lang/String;

    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpVoiceMessage;->mDurationTime:I

    return-void
.end method
