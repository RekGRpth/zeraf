.class public Lcom/mediatek/mms/ipmessage/message/IpMessage;
.super Ljava/lang/Object;
.source "IpMessage.java"


# instance fields
.field private mFrom:Ljava/lang/String;

.field private mId:I

.field private mIpDbId:I

.field private mSimId:I

.field private mStatus:I

.field private mTo:Ljava/lang/String;

.field private mType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFrom()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mFrom:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mId:I

    return v0
.end method

.method public getIpDbId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mIpDbId:I

    return v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mSimId:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mStatus:I

    return v0
.end method

.method public getTo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mTo:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mType:I

    return v0
.end method

.method public setFrom(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mFrom:Ljava/lang/String;

    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mId:I

    return-void
.end method

.method public setIpDbId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mIpDbId:I

    return-void
.end method

.method public setSimId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mSimId:I

    return-void
.end method

.method public setStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mStatus:I

    return-void
.end method

.method public setTo(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mTo:Ljava/lang/String;

    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mms/ipmessage/message/IpMessage;->mType:I

    return-void
.end method
