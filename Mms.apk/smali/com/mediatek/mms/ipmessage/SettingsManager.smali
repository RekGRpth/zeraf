.class public Lcom/mediatek/mms/ipmessage/SettingsManager;
.super Landroid/content/ContextWrapper;
.source "SettingsManager.java"


# instance fields
.field public mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ipmessage/SettingsManager;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/SettingsManager;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getMessageWallpaper()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public isAudioCaptionOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isAutoDownloadOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCaptionOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isPhotoCaptionOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSendAsSMSOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isVideoCaptionOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
