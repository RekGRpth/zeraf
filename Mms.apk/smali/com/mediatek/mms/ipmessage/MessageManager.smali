.class public Lcom/mediatek/mms/ipmessage/MessageManager;
.super Landroid/content/ContextWrapper;
.source "MessageManager.java"


# instance fields
.field public mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mms/ipmessage/MessageManager;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/mms/ipmessage/MessageManager;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public addMessageToImportantList([J)Z
    .locals 1
    .param p1    # [J

    const/4 v0, 0x0

    return v0
.end method

.method public cancelDownloading(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public deleteIpMsg([JZZ)V
    .locals 0
    .param p1    # [J
    .param p2    # Z
    .param p3    # Z

    return-void
.end method

.method public deleteMessageFromImportantList([J)Z
    .locals 1
    .param p1    # [J

    const/4 v0, 0x0

    return v0
.end method

.method public downloadAttach(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public getDownloadProcess(J)I
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public getFrom(J)Ljava/lang/String;
    .locals 1
    .param p1    # J

    const-string v0, ""

    return-object v0
.end method

.method public getIpDatabaseId(J)I
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public getIpMessageFromIntent(Landroid/content/Intent;)Lcom/mediatek/mms/ipmessage/message/IpMessage;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getIpMessageStatusString(J)Ljava/lang/String;
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return-object v0
.end method

.method public getIpMsgInfo(J)Lcom/mediatek/mms/ipmessage/message/IpMessage;
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSimId(J)I
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public getStatus(J)I
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public getTime(J)I
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public getTo(J)Ljava/lang/String;
    .locals 1
    .param p1    # J

    const-string v0, ""

    return-object v0
.end method

.method public getType(J)I
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public handleIpMessage(JI)V
    .locals 0
    .param p1    # J
    .param p3    # I

    return-void
.end method

.method public isDownloading(J)Z
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public isReaded(J)Z
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public resendMessage(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public resendMessage(JI)V
    .locals 0
    .param p1    # J
    .param p3    # I

    return-void
.end method

.method public saveIpMsg(Lcom/mediatek/mms/ipmessage/message/IpMessage;I)I
    .locals 1
    .param p1    # Lcom/mediatek/mms/ipmessage/message/IpMessage;
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public setIpMessageStatus(JI)V
    .locals 0
    .param p1    # J
    .param p3    # I

    return-void
.end method

.method public setIpMsgAsViewed(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public setThreadAsViewed(J)V
    .locals 0
    .param p1    # J

    return-void
.end method
