.class public final Lcom/mediatek/mms/ipmessage/IpMessageConsts$drawable;
.super Ljava/lang/Object;
.source "IpMessageConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mms/ipmessage/IpMessageConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ad01:I = 0x191

.field public static final ad01_png:I = 0x1f5

.field public static final ad02:I = 0x192

.field public static final ad02_png:I = 0x1f6

.field public static final ad03:I = 0x193

.field public static final ad03_png:I = 0x1f7

.field public static final ad04:I = 0x194

.field public static final ad04_png:I = 0x1f8

.field public static final ad05:I = 0x195

.field public static final ad05_png:I = 0x1f9

.field public static final ad06:I = 0x196

.field public static final ad06_png:I = 0x1fa

.field public static final ad07:I = 0x197

.field public static final ad07_png:I = 0x1fb

.field public static final ad08:I = 0x198

.field public static final ad08_png:I = 0x1fc

.field public static final ad09:I = 0x199

.field public static final ad09_png:I = 0x1fd

.field public static final ad10:I = 0x19a

.field public static final ad10_png:I = 0x1fe

.field public static final ad11:I = 0x19b

.field public static final ad11_png:I = 0x1ff

.field public static final ad12:I = 0x19c

.field public static final ad12_png:I = 0x200

.field public static final ad13:I = 0x19d

.field public static final ad13_png:I = 0x201

.field public static final ad14:I = 0x19e

.field public static final ad14_png:I = 0x202

.field public static final ad15:I = 0x19f

.field public static final ad15_png:I = 0x203

.field public static final ad16:I = 0x1a0

.field public static final ad16_png:I = 0x204

.field public static final ad17:I = 0x1a1

.field public static final ad17_png:I = 0x205

.field public static final ad18:I = 0x1a2

.field public static final ad18_png:I = 0x206

.field public static final ad19:I = 0x1a3

.field public static final ad19_png:I = 0x207

.field public static final ad20:I = 0x1a4

.field public static final ad20_png:I = 0x208

.field public static final ad21:I = 0x1a5

.field public static final ad21_png:I = 0x209

.field public static final ad22:I = 0x1a6

.field public static final ad22_png:I = 0x20a

.field public static final ad23:I = 0x1a7

.field public static final ad23_png:I = 0x20b

.field public static final ad24:I = 0x1a8

.field public static final ad24_png:I = 0x20c

.field public static final emo_angry:I = 0x6d

.field public static final emo_approve:I = 0x6e

.field public static final emo_boring:I = 0x6f

.field public static final emo_cry:I = 0x70

.field public static final emo_driving:I = 0x71

.field public static final emo_dynamic_01:I = 0xc9

.field public static final emo_dynamic_01_png:I = 0x12d

.field public static final emo_dynamic_02:I = 0xca

.field public static final emo_dynamic_02_png:I = 0x12e

.field public static final emo_dynamic_03:I = 0xcb

.field public static final emo_dynamic_03_png:I = 0x12f

.field public static final emo_dynamic_04:I = 0xcc

.field public static final emo_dynamic_04_png:I = 0x130

.field public static final emo_dynamic_05:I = 0xcd

.field public static final emo_dynamic_05_png:I = 0x131

.field public static final emo_dynamic_06:I = 0xce

.field public static final emo_dynamic_06_png:I = 0x132

.field public static final emo_dynamic_07:I = 0xcf

.field public static final emo_dynamic_07_png:I = 0x133

.field public static final emo_dynamic_08:I = 0xd0

.field public static final emo_dynamic_08_png:I = 0x134

.field public static final emo_dynamic_09:I = 0xd1

.field public static final emo_dynamic_09_png:I = 0x135

.field public static final emo_dynamic_10:I = 0xd2

.field public static final emo_dynamic_10_png:I = 0x136

.field public static final emo_dynamic_11:I = 0xd3

.field public static final emo_dynamic_11_png:I = 0x137

.field public static final emo_dynamic_12:I = 0xd4

.field public static final emo_dynamic_12_png:I = 0x138

.field public static final emo_dynamic_13:I = 0xd5

.field public static final emo_dynamic_13_png:I = 0x139

.field public static final emo_dynamic_14:I = 0xd6

.field public static final emo_dynamic_14_png:I = 0x13a

.field public static final emo_dynamic_15:I = 0xd7

.field public static final emo_dynamic_15_png:I = 0x13b

.field public static final emo_dynamic_16:I = 0xd8

.field public static final emo_dynamic_16_png:I = 0x13c

.field public static final emo_dynamic_17:I = 0xd9

.field public static final emo_dynamic_17_png:I = 0x13d

.field public static final emo_dynamic_18:I = 0xda

.field public static final emo_dynamic_18_png:I = 0x13e

.field public static final emo_dynamic_19:I = 0xdb

.field public static final emo_dynamic_19_png:I = 0x13f

.field public static final emo_dynamic_20:I = 0xdc

.field public static final emo_dynamic_20_png:I = 0x140

.field public static final emo_dynamic_21:I = 0xdd

.field public static final emo_dynamic_21_png:I = 0x141

.field public static final emo_dynamic_22:I = 0xde

.field public static final emo_dynamic_22_png:I = 0x142

.field public static final emo_dynamic_23:I = 0xdf

.field public static final emo_dynamic_23_png:I = 0x143

.field public static final emo_dynamic_24:I = 0xe0

.field public static final emo_dynamic_24_png:I = 0x144

.field public static final emo_eating:I = 0x72

.field public static final emo_fly:I = 0x6c

.field public static final emo_gift:I = 0x66

.field public static final emo_happy:I = 0x73

.field public static final emo_hold:I = 0x74

.field public static final emo_holiday:I = 0x75

.field public static final emo_ill:I = 0x6a

.field public static final emo_kongfu:I = 0x67

.field public static final emo_love:I = 0x76

.field public static final emo_praise:I = 0x65

.field public static final emo_pray:I = 0x77

.field public static final emo_pressure:I = 0x78

.field public static final emo_rich:I = 0x6b

.field public static final emo_scare:I = 0x69

.field public static final emo_shower:I = 0x68

.field public static final emo_sing:I = 0x79

.field public static final emo_sleep:I = 0x7a

.field public static final emo_sports:I = 0x7b

.field public static final emo_swimming:I = 0x7c

.field public static final ipmsg_service:I = 0x2329

.field public static final ipmsg_sim_indicator:I = 0x232a

.field public static final xm01:I = 0x259

.field public static final xm01_png:I = 0x2bd

.field public static final xm02:I = 0x25a

.field public static final xm02_png:I = 0x2be

.field public static final xm03:I = 0x25b

.field public static final xm03_png:I = 0x2bf

.field public static final xm04:I = 0x25c

.field public static final xm04_png:I = 0x2c0

.field public static final xm05:I = 0x25d

.field public static final xm05_png:I = 0x2c1

.field public static final xm06:I = 0x25e

.field public static final xm06_png:I = 0x2c2

.field public static final xm07:I = 0x25f

.field public static final xm07_png:I = 0x2c3

.field public static final xm08:I = 0x260

.field public static final xm08_png:I = 0x2c4

.field public static final xm09:I = 0x261

.field public static final xm09_png:I = 0x2c5

.field public static final xm10:I = 0x262

.field public static final xm10_png:I = 0x2c6

.field public static final xm11:I = 0x263

.field public static final xm11_png:I = 0x2c7

.field public static final xm12:I = 0x264

.field public static final xm12_png:I = 0x2c8

.field public static final xm13:I = 0x265

.field public static final xm13_png:I = 0x2c9

.field public static final xm14:I = 0x266

.field public static final xm14_png:I = 0x2ca

.field public static final xm15:I = 0x267

.field public static final xm15_png:I = 0x2cb

.field public static final xm16:I = 0x268

.field public static final xm16_png:I = 0x2cc

.field public static final xm17:I = 0x269

.field public static final xm17_png:I = 0x2cd

.field public static final xm18:I = 0x26a

.field public static final xm18_png:I = 0x2ce

.field public static final xm19:I = 0x26b

.field public static final xm19_png:I = 0x2cf

.field public static final xm20:I = 0x26c

.field public static final xm20_png:I = 0x2d0

.field public static final xm21:I = 0x26d

.field public static final xm21_png:I = 0x2d1

.field public static final xm22:I = 0x26e

.field public static final xm22_png:I = 0x2d2

.field public static final xm23:I = 0x26f

.field public static final xm23_png:I = 0x2d3

.field public static final xm24:I = 0x270

.field public static final xm24_png:I = 0x2d4


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
