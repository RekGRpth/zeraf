.class public interface abstract Lcom/mediatek/mms/ipmessage/IIpMessagePlugin;
.super Ljava/lang/Object;
.source "IIpMessagePlugin.java"


# virtual methods
.method public abstract getActivitiesManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ActivitiesManager;
.end method

.method public abstract getChatManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ChatManager;
.end method

.method public abstract getContactManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ContactManager;
.end method

.method public abstract getGroupManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/GroupManager;
.end method

.method public abstract getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;
.end method

.method public abstract getNotificationsManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/NotificationsManager;
.end method

.method public abstract getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;
.end method

.method public abstract getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;
.end method

.method public abstract getSettingsManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/SettingsManager;
.end method

.method public abstract isActualPlugin()Z
.end method
