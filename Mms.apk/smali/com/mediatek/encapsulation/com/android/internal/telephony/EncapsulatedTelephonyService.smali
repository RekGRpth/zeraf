.class public Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;
.super Ljava/lang/Object;
.source "EncapsulatedTelephonyService.java"


# static fields
.field private static sTelephony:Lcom/android/internal/telephony/ITelephony;

.field private static sTelephonyService:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;

    invoke-direct {v0}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;-><init>()V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephonyService:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;
    .locals 2

    const-class v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephonyService:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephonyService:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public adjustModemRadioPower(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->adjustModemRadioPower(II)Z

    move-result v0

    return v0
.end method

.method public adjustModemRadioPowerByBand(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/ITelephony;->adjustModemRadioPowerByBand(III)Z

    move-result v0

    return v0
.end method

.method public answerRingingCall()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->answerRingingCall()V

    return-void
.end method

.method public answerRingingCallGemini(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->answerRingingCallGemini(I)V

    return-void
.end method

.method public aquire3GSwitchLock()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->aquire3GSwitchLock()I

    move-result v0

    return v0
.end method

.method public btSimapApduRequest(ILjava/lang/String;Landroid/telephony/BtSimapOperResponse;)I
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/telephony/BtSimapOperResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/ITelephony;->btSimapApduRequest(ILjava/lang/String;Landroid/telephony/BtSimapOperResponse;)I

    move-result v0

    return v0
.end method

.method public btSimapConnectSIM(ILandroid/telephony/BtSimapOperResponse;)I
    .locals 1
    .param p1    # I
    .param p2    # Landroid/telephony/BtSimapOperResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->btSimapConnectSIM(ILandroid/telephony/BtSimapOperResponse;)I

    move-result v0

    return v0
.end method

.method public btSimapDisconnectSIM()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->btSimapDisconnectSIM()I

    move-result v0

    return v0
.end method

.method public btSimapPowerOffSIM()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->btSimapPowerOffSIM()I

    move-result v0

    return v0
.end method

.method public btSimapPowerOnSIM(ILandroid/telephony/BtSimapOperResponse;)I
    .locals 1
    .param p1    # I
    .param p2    # Landroid/telephony/BtSimapOperResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->btSimapPowerOnSIM(ILandroid/telephony/BtSimapOperResponse;)I

    move-result v0

    return v0
.end method

.method public btSimapResetSIM(ILandroid/telephony/BtSimapOperResponse;)I
    .locals 1
    .param p1    # I
    .param p2    # Landroid/telephony/BtSimapOperResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->btSimapResetSIM(ILandroid/telephony/BtSimapOperResponse;)I

    move-result v0

    return v0
.end method

.method public call(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->call(Ljava/lang/String;)V

    return-void
.end method

.method public callGemini(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->callGemini(Ljava/lang/String;I)V

    return-void
.end method

.method public cancelMissedCallsNotification()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->cancelMissedCallsNotification()V

    return-void
.end method

.method public cancelMissedCallsNotificationGemini(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->cancelMissedCallsNotificationGemini(I)V

    return-void
.end method

.method public cleanupApnTypeGemini(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->cleanupApnTypeGemini(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public closeIccLogicalChannel(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->closeIccLogicalChannel(I)Z

    move-result v0

    return v0
.end method

.method public closeIccLogicalChannelGemini(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->closeIccLogicalChannelGemini(II)Z

    move-result v0

    return v0
.end method

.method public dial(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->dial(Ljava/lang/String;)V

    return-void
.end method

.method public dialGemini(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->dialGemini(Ljava/lang/String;I)V

    return-void
.end method

.method public disableApnType(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->disableApnType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public disableApnTypeGemini(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->disableApnTypeGemini(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public disableDataConnectivity()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->disableDataConnectivity()Z

    move-result v0

    return v0
.end method

.method public disableDataConnectivityGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->disableDataConnectivityGemini(I)I

    move-result v0

    return v0
.end method

.method public disableLocationUpdates()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->disableLocationUpdates()V

    return-void
.end method

.method public disableLocationUpdatesGemini(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->disableLocationUpdatesGemini(I)V

    return-void
.end method

.method public enableApnType(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->enableApnType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public enableApnTypeGemini(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->enableApnTypeGemini(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public enableDataConnectivity()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->enableDataConnectivity()Z

    move-result v0

    return v0
.end method

.method public enableDataConnectivityGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->enableDataConnectivityGemini(I)I

    move-result v0

    return v0
.end method

.method public enableLocationUpdates()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->enableLocationUpdates()V

    return-void
.end method

.method public enableLocationUpdatesGemini(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->enableLocationUpdatesGemini(I)V

    return-void
.end method

.method public endCall()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->endCall()Z

    move-result v0

    return v0
.end method

.method public endCallGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->endCallGemini(I)Z

    move-result v0

    return v0
.end method

.method public get3GCapabilitySIM()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->get3GCapabilitySIM()I

    move-result v0

    return v0
.end method

.method public getActivePhoneType()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getActivePhoneType()I

    move-result v0

    return v0
.end method

.method public getActivePhoneTypeGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getActivePhoneTypeGemini(I)I

    move-result v0

    return v0
.end method

.method public getAllCellInfo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getAllCellInfo()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCallState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getCallState()I

    move-result v0

    return v0
.end method

.method public getCallStateGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getCallStateGemini(I)I

    move-result v0

    return v0
.end method

.method public getCdmaEriIconIndex()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getCdmaEriIconIndex()I

    move-result v0

    return v0
.end method

.method public getCdmaEriIconMode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getCdmaEriIconMode()I

    move-result v0

    return v0
.end method

.method public getCdmaEriText()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getCdmaEriText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCellLocation()Landroid/os/Bundle;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getCellLocation()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getCellLocationGemini(I)Landroid/os/Bundle;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getCellLocationGemini(I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getDataActivity()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getDataActivity()I

    move-result v0

    return v0
.end method

.method public getDataActivityGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getDataActivityGemini(I)I

    move-result v0

    return v0
.end method

.method public getDataState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getDataState()I

    move-result v0

    return v0
.end method

.method public getDataStateGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getDataStateGemini(I)I

    move-result v0

    return v0
.end method

.method public getIccATR()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getIccATR()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIccATRGemini(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getIccATRGemini(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIccCardType()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getIccCardType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIccCardTypeGemini(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getIccCardTypeGemini(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastError()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getLastError()I

    move-result v0

    return v0
.end method

.method public getLastErrorGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getLastErrorGemini(I)I

    move-result v0

    return v0
.end method

.method public getLteOnCdmaMode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getLteOnCdmaMode()I

    move-result v0

    return v0
.end method

.method public getMissedCallCount()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getMissedCallCount()I

    move-result v0

    return v0
.end method

.method public getMobileRevisionAndIMEI(ILandroid/os/Message;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->getMobileRevisionAndIMEI(ILandroid/os/Message;)V

    return-void
.end method

.method public getNeighboringCellInfo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/NeighboringCellInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getNeighboringCellInfo()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNeighboringCellInfoGemini(I)Ljava/util/List;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/NeighboringCellInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getNeighboringCellInfoGemini(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkType()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getNetworkType()I

    move-result v0

    return v0
.end method

.method public getNetworkTypeGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getNetworkTypeGemini(I)I

    move-result v0

    return v0
.end method

.method public getPendingMmiCodesGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getPendingMmiCodesGemini(I)I

    move-result v0

    return v0
.end method

.method public getPreciseCallState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getPreciseCallState()I

    move-result v0

    return v0
.end method

.method public getSN()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getSN()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScAddressGemini(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getScAddressGemini(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServiceState()Landroid/os/Bundle;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getServiceState()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getServiceStateGemini(I)Landroid/os/Bundle;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getServiceStateGemini(I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getSimIndicatorState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getSimIndicatorState()I

    move-result v0

    return v0
.end method

.method public getSimIndicatorStateGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getSimIndicatorStateGemini(I)I

    move-result v0

    return v0
.end method

.method public getSmsDefaultSim()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getSmsDefaultSim()I

    move-result v0

    return v0
.end method

.method public getSpNameInEfSpn()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getSpNameInEfSpn()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpNameInEfSpnGemini(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getSpNameInEfSpnGemini(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVoiceMessageCount()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->getVoiceMessageCount()I

    move-result v0

    return v0
.end method

.method public getVoiceMessageCountGemini(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->getVoiceMessageCountGemini(I)I

    move-result v0

    return v0
.end method

.method public handlePinMmi(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->handlePinMmi(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public handlePinMmiGemini(Ljava/lang/String;I)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->handlePinMmiGemini(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public hasIccCard()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->hasIccCard()Z

    move-result v0

    return v0
.end method

.method public hasIccCardGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->hasIccCardGemini(I)Z

    move-result v0

    return v0
.end method

.method public is3GSwitchLocked()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->is3GSwitchLocked()Z

    move-result v0

    return v0
.end method

.method public isDataConnectivityPossible()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isDataConnectivityPossible()Z

    move-result v0

    return v0
.end method

.method public isDataConnectivityPossibleGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isDataConnectivityPossibleGemini(I)Z

    move-result v0

    return v0
.end method

.method public isFDNEnabled()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabled()Z

    move-result v0

    return v0
.end method

.method public isFDNEnabledGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isFDNEnabledGemini(I)Z

    move-result v0

    return v0
.end method

.method public isIccCardProviderAsMvno()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isIccCardProviderAsMvno()Z

    move-result v0

    return v0
.end method

.method public isIccCardProviderAsMvnoGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isIccCardProviderAsMvnoGemini(I)Z

    move-result v0

    return v0
.end method

.method public isIdle()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isIdle()Z

    move-result v0

    return v0
.end method

.method public isIdleGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isIdleGemini(I)Z

    move-result v0

    return v0
.end method

.method public isOffhook()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z

    move-result v0

    return v0
.end method

.method public isOffhookGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isOffhookGemini(I)Z

    move-result v0

    return v0
.end method

.method public isOperatorMvnoForImsi()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isOperatorMvnoForImsi()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isOperatorMvnoForImsiGemini(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isOperatorMvnoForImsiGemini(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isRadioOn()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isRadioOn()Z

    move-result v0

    return v0
.end method

.method public isRadioOnGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isRadioOnGemini(I)Z

    move-result v0

    return v0
.end method

.method public isRinging()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    move-result v0

    return v0
.end method

.method public isRingingGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isRingingGemini(I)Z

    move-result v0

    return v0
.end method

.method public isSimInsert(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v0

    return v0
.end method

.method public isSimPinEnabled()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isSimPinEnabled()Z

    move-result v0

    return v0
.end method

.method public isTestIccCard()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isTestIccCard()Z

    move-result v0

    return v0
.end method

.method public isTestIccCardGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->isTestIccCardGemini(I)Z

    move-result v0

    return v0
.end method

.method public isVTIdle()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isVTIdle()Z

    move-result v0

    return v0
.end method

.method public isVoiceIdle()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->isVoiceIdle()Z

    move-result v0

    return v0
.end method

.method public needsOtaServiceProvisioning()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->needsOtaServiceProvisioning()Z

    move-result v0

    return v0
.end method

.method public openIccLogicalChannel(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->openIccLogicalChannel(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public openIccLogicalChannelGemini(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->openIccLogicalChannelGemini(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public registerForSimModeChange(Landroid/os/IBinder;I)V
    .locals 1
    .param p1    # Landroid/os/IBinder;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->registerForSimModeChange(Landroid/os/IBinder;I)V

    return-void
.end method

.method public release3GSwitchLock(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->release3GSwitchLock(I)Z

    move-result v0

    return v0
.end method

.method public set3GCapabilitySIM(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->set3GCapabilitySIM(I)Z

    move-result v0

    return v0
.end method

.method public setDataRoamingEnabledGemini(ZI)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->setDataRoamingEnabledGemini(ZI)V

    return-void
.end method

.method public setDefaultPhone(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->setDefaultPhone(I)V

    return-void
.end method

.method public setGprsConnType(II)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->setGprsConnType(II)V

    return-void
.end method

.method public setGprsTransferType(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->setGprsTransferType(I)V

    return-void
.end method

.method public setGprsTransferTypeGemini(II)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->setGprsTransferTypeGemini(II)V

    return-void
.end method

.method public setRadio(Z)Z
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->setRadio(Z)Z

    move-result v0

    return v0
.end method

.method public setRadioOff()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->setRadioOff()Z

    move-result v0

    return v0
.end method

.method public setRoamingIndicatorNeddedProperty(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->setRoamingIndicatorNeddedProperty(ZZ)V

    return-void
.end method

.method public setScAddressGemini(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->setScAddressGemini(Ljava/lang/String;I)V

    return-void
.end method

.method public showCallScreen()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->showCallScreen()Z

    move-result v0

    return v0
.end method

.method public showCallScreenGemini(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->showCallScreenGemini(I)Z

    move-result v0

    return v0
.end method

.method public showCallScreenWithDialpad(Z)Z
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->showCallScreenWithDialpad(Z)Z

    move-result v0

    return v0
.end method

.method public showCallScreenWithDialpadGemini(ZI)Z
    .locals 1
    .param p1    # Z
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->showCallScreenWithDialpadGemini(ZI)Z

    move-result v0

    return v0
.end method

.method public silenceRinger()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->silenceRinger()V

    return-void
.end method

.method public silenceRingerGemini(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->silenceRingerGemini(I)V

    return-void
.end method

.method public simAuth(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->simAuth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public simAuthGemini(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->simAuthGemini(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public supplyPin(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->supplyPin(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public supplyPinGemini(Ljava/lang/String;I)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->supplyPinGemini(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public supplyPuk(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->supplyPuk(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public supplyPukGemini(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/ITelephony;->supplyPukGemini(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public toggleRadioOnOff()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->toggleRadioOnOff()V

    return-void
.end method

.method public transmitIccBasicChannel(IIIIILjava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/ITelephony;->transmitIccBasicChannel(IIIIILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transmitIccBasicChannelGemini(IIIIILjava/lang/String;I)Ljava/lang/String;
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/ITelephony;->transmitIccBasicChannelGemini(IIIIILjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transmitIccLogicalChannel(IIIIIILjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/ITelephony;->transmitIccLogicalChannel(IIIIIILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transmitIccLogicalChannelGemini(IIIIIILjava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Ljava/lang/String;
    .param p8    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Lcom/android/internal/telephony/ITelephony;->transmitIccLogicalChannelGemini(IIIIIILjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transmitIccSimIO(IIIIILjava/lang/String;)[B
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/ITelephony;->transmitIccSimIO(IIIIILjava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public transmitIccSimIOGemini(IIIIILjava/lang/String;I)[B
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/ITelephony;->transmitIccSimIOGemini(IIIIILjava/lang/String;I)[B

    move-result-object v0

    return-object v0
.end method

.method public uSimAuth(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/ITelephony;->uSimAuth(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public uSimAuthGemini(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/ITelephony;->uSimAuthGemini(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregisterForSimModeChange(Landroid/os/IBinder;)V
    .locals 1
    .param p1    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->unregisterForSimModeChange(Landroid/os/IBinder;)V

    return-void
.end method

.method public updateServiceLocation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->updateServiceLocation()V

    return-void
.end method

.method public updateServiceLocationGemini(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->sTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/ITelephony;->updateServiceLocationGemini(I)V

    return-void
.end method
