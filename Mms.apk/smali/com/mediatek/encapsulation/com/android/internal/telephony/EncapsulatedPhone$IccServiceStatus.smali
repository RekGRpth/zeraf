.class public final enum Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;
.super Ljava/lang/Enum;
.source "EncapsulatedPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IccServiceStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

.field public static final enum ACTIVATED:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

.field public static final enum INACTIVATED:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

.field public static final enum NOT_EXIST_IN_SIM:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

.field public static final enum NOT_EXIST_IN_USIM:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

.field public static final enum UNKNOWN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    const-string v1, "NOT_EXIST_IN_SIM"

    invoke-direct {v0, v1, v2}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->NOT_EXIST_IN_SIM:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    const-string v1, "NOT_EXIST_IN_USIM"

    invoke-direct {v0, v1, v3}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->NOT_EXIST_IN_USIM:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    const-string v1, "ACTIVATED"

    invoke-direct {v0, v1, v4}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->ACTIVATED:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    const-string v1, "INACTIVATED"

    invoke-direct {v0, v1, v5}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->INACTIVATED:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->UNKNOWN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->NOT_EXIST_IN_SIM:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->NOT_EXIST_IN_USIM:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->ACTIVATED:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->INACTIVATED:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->UNKNOWN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->$VALUES:[Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    return-object v0
.end method

.method public static values()[Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;
    .locals 1

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;->$VALUES:[Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccServiceStatus;

    return-object v0
.end method
