.class public final enum Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;
.super Ljava/lang/Enum;
.source "EncapsulatedPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IccService"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum CFIS:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum CHV1_DISABLE_FUNCTION:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum EPLMN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum MWIS:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum OPL:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum PNN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum SPDI:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum SPN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

.field public static final enum UNSUPPORTED_SERVICE:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "CHV1_DISABLE_FUNCTION"

    invoke-direct {v0, v1, v3}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->CHV1_DISABLE_FUNCTION:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "SPN"

    invoke-direct {v0, v1, v4}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->SPN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "PNN"

    invoke-direct {v0, v1, v5}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->PNN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "OPL"

    invoke-direct {v0, v1, v6}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->OPL:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "MWIS"

    invoke-direct {v0, v1, v7}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->MWIS:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "CFIS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->CFIS:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "SPDI"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->SPDI:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "EPLMN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->EPLMN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    new-instance v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const-string v1, "UNSUPPORTED_SERVICE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->UNSUPPORTED_SERVICE:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->CHV1_DISABLE_FUNCTION:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->SPN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->PNN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->OPL:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->MWIS:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->CFIS:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->SPDI:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->EPLMN:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->UNSUPPORTED_SERVICE:Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->$VALUES:[Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    return-object v0
.end method

.method public static values()[Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;
    .locals 1

    sget-object v0, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;->$VALUES:[Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$IccService;

    return-object v0
.end method


# virtual methods
.method public getIndex()I
    .locals 3

    const/4 v0, -0x1

    sget-object v1, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedPhone$1;->$SwitchMap$com$mediatek$encapsulation$com$android$internal$telephony$EncapsulatedPhone$IccService:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x8

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
