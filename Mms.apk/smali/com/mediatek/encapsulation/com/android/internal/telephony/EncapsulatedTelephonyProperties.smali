.class public interface abstract Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyProperties;
.super Ljava/lang/Object;
.source "EncapsulatedTelephonyProperties.java"

# interfaces
.implements Lcom/android/internal/telephony/TelephonyProperties;


# static fields
.field public static final PROPERTY_BASEBAND_VERSION_2:Ljava/lang/String; = "gsm.version.baseband.2"

.field public static final PROPERTY_CS_NETWORK_TYPE:Ljava/lang/String; = "gsm.cs.network.type"

.field public static final PROPERTY_CS_NETWORK_TYPE_2:Ljava/lang/String; = "gsm.cs.network.type.2"

.field public static final PROPERTY_DATA_NETWORK_TYPE_2:Ljava/lang/String; = "gsm.network.type.2"

.field public static final PROPERTY_GSM_SIM_INSERTED:Ljava/lang/String; = "gsm.sim.inserted"

.field public static final PROPERTY_ICC_OPERATOR_ALPHA_2:Ljava/lang/String; = "gsm.sim.operator.alpha.2"

.field public static final PROPERTY_ICC_OPERATOR_DEFAULT_NAME:Ljava/lang/String; = "gsm.sim.operator.default-name"

.field public static final PROPERTY_ICC_OPERATOR_DEFAULT_NAME_2:Ljava/lang/String; = "gsm.sim.operator.default-name.2"

.field public static final PROPERTY_ICC_OPERATOR_ISO_COUNTRY_2:Ljava/lang/String; = "gsm.sim.operator.iso-country.2"

.field public static final PROPERTY_ICC_OPERATOR_NUMERIC_2:Ljava/lang/String; = "gsm.sim.operator.numeric.2"

.field public static final PROPERTY_OPERATOR_ALPHA_2:Ljava/lang/String; = "gsm.operator.alpha.2"

.field public static final PROPERTY_OPERATOR_ISMANUAL_2:Ljava/lang/String; = "operator.ismanual.2"

.field public static final PROPERTY_OPERATOR_ISO_COUNTRY_2:Ljava/lang/String; = "gsm.operator.iso-country.2"

.field public static final PROPERTY_OPERATOR_ISROAMING_2:Ljava/lang/String; = "gsm.operator.isroaming.2"

.field public static final PROPERTY_OPERATOR_NUMERIC_2:Ljava/lang/String; = "gsm.operator.numeric.2"

.field public static final PROPERTY_ROAMING_INDICATOR_NEEDED:Ljava/lang/String; = "gsm.roaming.indicator.needed"

.field public static final PROPERTY_ROAMING_INDICATOR_NEEDED_2:Ljava/lang/String; = "gsm.roaming.indicator.needed.2"

.field public static final PROPERTY_SIM_INFO_READY:Ljava/lang/String; = "gsm.siminfo.ready"

.field public static final PROPERTY_SIM_LOCALE_SETTINGS:Ljava/lang/String; = "gsm.sim.locale.waiting"

.field public static final PROPERTY_SIM_STATE_2:Ljava/lang/String; = "gsm.sim.state.2"
