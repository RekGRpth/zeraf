.class public Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;
.super Landroid/text/style/ReplacementSpan;
.source "EncapsulatedBackgroundImageSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mImageId:I

.field private mWidth:I


# direct methods
.method public constructor <init>(ILandroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mWidth:I

    new-instance v0, Lcom/mediatek/text/style/BackgroundImageSpan;

    invoke-direct {v0, p1, p2}, Lcom/mediatek/text/style/BackgroundImageSpan;-><init>(ILandroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    iput p1, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mImageId:I

    iput-object p2, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mWidth:I

    new-instance v0, Lcom/mediatek/text/style/BackgroundImageSpan;

    invoke-direct {v0, p1}, Lcom/mediatek/text/style/BackgroundImageSpan;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mImageId:I

    return-void
.end method

.method public static convert(Ljava/lang/CharSequence;Landroid/content/Context;)V
    .locals 0
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # Landroid/content/Context;

    invoke-static {p0, p1}, Lcom/mediatek/text/style/BackgroundImageSpan;->convert(Ljava/lang/CharSequence;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public convertToDrawable(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    invoke-virtual {v0, p1}, Lcom/mediatek/text/style/BackgroundImageSpan;->convertToDrawable(Landroid/content/Context;)V

    return-void
.end method

.method public describeContents()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    invoke-virtual {v0}, Lcom/mediatek/text/style/BackgroundImageSpan;->describeContents()I

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;IFIIILandroid/graphics/Paint;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # F
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/mediatek/text/style/BackgroundImageSpan;->draw(Landroid/graphics/Canvas;IFIIILandroid/graphics/Paint;)V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # I
    .param p5    # F
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/mediatek/text/style/BackgroundImageSpan;->draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V

    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 6
    .param p1    # Landroid/graphics/Paint;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/graphics/Paint$FontMetricsInt;

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/text/style/BackgroundImageSpan;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    move-result v0

    return v0
.end method

.method public getSpanTypeId()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    invoke-virtual {v0}, Lcom/mediatek/text/style/BackgroundImageSpan;->getSpanTypeId()I

    move-result v0

    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 0
    .param p1    # Landroid/text/TextPaint;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/text/style/EncapsulatedBackgroundImageSpan;->mBackgroundImageSpan:Lcom/mediatek/text/style/BackgroundImageSpan;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/text/style/BackgroundImageSpan;->writeToParcel(Landroid/os/Parcel;I)V

    return-void
.end method
