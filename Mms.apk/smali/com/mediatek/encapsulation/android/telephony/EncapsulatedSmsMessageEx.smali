.class public Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsMessageEx;
.super Ljava/lang/Object;
.source "EncapsulatedSmsMessageEx.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSmsc(Landroid/telephony/SmsMessage;I)[B
    .locals 1
    .param p0    # Landroid/telephony/SmsMessage;
    .param p1    # I

    invoke-static {}, Lcom/mediatek/telephony/SmsMessageEx;->getDefault()Lcom/mediatek/telephony/SmsMessageEx;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/mediatek/telephony/SmsMessageEx;->getSmsc(Landroid/telephony/SmsMessage;I)[B

    move-result-object v0

    return-object v0
.end method

.method public static getTpdu(Landroid/telephony/SmsMessage;I)[B
    .locals 1
    .param p0    # Landroid/telephony/SmsMessage;
    .param p1    # I

    invoke-static {}, Lcom/mediatek/telephony/SmsMessageEx;->getDefault()Lcom/mediatek/telephony/SmsMessageEx;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/mediatek/telephony/SmsMessageEx;->getTpdu(Landroid/telephony/SmsMessage;I)[B

    move-result-object v0

    return-object v0
.end method
