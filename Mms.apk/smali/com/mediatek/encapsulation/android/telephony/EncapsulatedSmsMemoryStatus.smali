.class public Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsMemoryStatus;
.super Ljava/lang/Object;
.source "EncapsulatedSmsMemoryStatus.java"


# instance fields
.field private mSmsMemoryStatus:Lcom/mediatek/common/telephony/IccSmsStorageStatus;

.field public mTotal:I

.field public mUsed:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsMemoryStatus;->mUsed:I

    iput p2, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsMemoryStatus;->mTotal:I

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/common/telephony/IccSmsStorageStatus;)V
    .locals 0
    .param p1    # Lcom/mediatek/common/telephony/IccSmsStorageStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsMemoryStatus;->mSmsMemoryStatus:Lcom/mediatek/common/telephony/IccSmsStorageStatus;

    :cond_0
    return-void
.end method


# virtual methods
.method public getSmsMemoryStatus()Lcom/mediatek/common/telephony/IccSmsStorageStatus;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsMemoryStatus;->mSmsMemoryStatus:Lcom/mediatek/common/telephony/IccSmsStorageStatus;

    return-object v0
.end method

.method public getTotal()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsMemoryStatus;->mSmsMemoryStatus:Lcom/mediatek/common/telephony/IccSmsStorageStatus;

    invoke-virtual {v0}, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->getTotal()I

    move-result v0

    return v0
.end method

.method public getUsed()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsMemoryStatus;->mSmsMemoryStatus:Lcom/mediatek/common/telephony/IccSmsStorageStatus;

    invoke-virtual {v0}, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->getUsed()I

    move-result v0

    return v0
.end method
