.class public Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;
.super Ljava/lang/Object;
.source "EncapsulatedTelephony.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$GprsInfo;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$Carriers;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$WapPush;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$ThreadSettings;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$Threads;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$ThreadsColumns;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$Sms;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$TextBasedSmsColumns;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SimInfo;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$MmsSms;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$Mms;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$BaseMmsColumns;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SmsCb;,
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$TextBasedSmsCbColumns;
    }
.end annotation


# static fields
.field public static final SIMBackgroundDarkRes:[I

.field public static final SIMBackgroundLightRes:[I

.field public static final SIMBackgroundRes:[I

.field private static final TAG:Ljava/lang/String; = "EncapsulatedTelephony"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x4

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;->SIMBackgroundRes:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;->SIMBackgroundDarkRes:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;->SIMBackgroundLightRes:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x20200e1
        0x20200e4
        0x20200e2
        0x20200e5
    .end array-data

    :array_1
    .array-data 4
        0x20200f0
        0x20200f2
        0x20200f1
        0x20200f3
    .end array-data

    :array_2
    .array-data 4
        0x20200f9
        0x20200fb
        0x20200fa
        0x20200fc
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
