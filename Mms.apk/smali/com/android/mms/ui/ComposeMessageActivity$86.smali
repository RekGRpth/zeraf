.class Lcom/android/mms/ui/ComposeMessageActivity$86;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity;->asyncAttachVCardByContactsId(Landroid/content/Intent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;

.field final synthetic val$data:Landroid/content/Intent;

.field final synthetic val$isAddingIpMsgVCardFile:Z


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;Landroid/content/Intent;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iput-object p2, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->val$data:Landroid/content/Intent;

    iput-boolean p3, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->val$isAddingIpMsgVCardFile:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v8, 0x6

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->val$data:Landroid/content/Intent;

    const-string v5, "com.mediatek.contacts.list.pickcontactsresult"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    new-instance v3, Lcom/android/mms/ui/VCardAttachment;

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {v3, v4}, Lcom/android/mms/ui/VCardAttachment;-><init>(Landroid/content/Context;)V

    iget-boolean v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->val$isAddingIpMsgVCardFile:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-virtual {v3, v0, v7}, Lcom/android/mms/ui/VCardAttachment;->getVCardFileNameByContactsId([JZ)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16602(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v6}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getCachePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16600(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16702(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;)Ljava/lang/String;

    const-string v4, "Mms/ipmsg/compose"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "asyncAttachVCardByContactsId(): mIpMessageVcardName = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16600(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mDstPath = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16700(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity;->mIpMsgHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16800(Lcom/android/mms/ui/ComposeMessageActivity;)Ljava/lang/Runnable;

    move-result-object v5

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/android/mms/data/WorkingMessage;->setIsDeleteVcardFile(Z)V

    invoke-virtual {v3, v0, v6}, Lcom/android/mms/ui/VCardAttachment;->getVCardFileNameByContactsId([JZ)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x5

    invoke-static {v4}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$16900(Lcom/android/mms/ui/ComposeMessageActivity;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v4

    if-ne v4, v7, :cond_1

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v1, v8, v7}, Lcom/android/mms/ui/ComposeMessageActivity;->access$17000(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;IZ)V

    :goto_1
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/android/mms/data/WorkingMessage;->saveDraft(Z)V

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/android/mms/data/WorkingMessage;->setIsDeleteVcardFile(Z)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$86;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v1, v8, v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$17000(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;IZ)V

    goto :goto_1
.end method
