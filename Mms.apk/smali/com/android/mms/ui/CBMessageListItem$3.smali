.class Lcom/android/mms/ui/CBMessageListItem$3;
.super Ljava/lang/Object;
.source "CBMessageListItem.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/CBMessageListItem;->onMessageListItemClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/CBMessageListItem;

.field final synthetic val$mUriTemp:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/CBMessageListItem;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/CBMessageListItem$3;->this$0:Lcom/android/mms/ui/CBMessageListItem;

    iput-object p2, p0, Lcom/android/mms/ui/CBMessageListItem$3;->val$mUriTemp:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v2, p0, Lcom/android/mms/ui/CBMessageListItem$3;->val$mUriTemp:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "com.android.browser.application_id"

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem$3;->this$0:Lcom/android/mms/ui/CBMessageListItem;

    invoke-static {v3}, Lcom/android/mms/ui/CBMessageListItem;->access$000(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x80000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/mms/ui/CBMessageListItem$3;->this$0:Lcom/android/mms/ui/CBMessageListItem;

    invoke-static {v2}, Lcom/android/mms/ui/CBMessageListItem;->access$100(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
