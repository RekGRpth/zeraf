.class public Lcom/android/mms/ui/DeliveryReportAdapter;
.super Landroid/widget/ArrayAdapter;
.source "DeliveryReportAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/mms/ui/DeliveryReportItem;",
        ">;"
    }
.end annotation


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "DeliveryReportAdapter"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/android/mms/ui/DeliveryReportItem;",
            ">;)V"
        }
    .end annotation

    const v0, 0x7f04001e

    const v1, 0x7f0f0082

    invoke-direct {p0, p1, v0, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mms/ui/DeliveryReportItem;

    if-nez p2, :cond_1

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04001e

    const/4 v4, 0x0

    invoke-virtual {v0, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mms/ui/DeliveryReportListItem;

    :goto_0
    iget-object v3, v1, Lcom/android/mms/ui/DeliveryReportItem;->recipient:Ljava/lang/String;

    iget-object v4, v1, Lcom/android/mms/ui/DeliveryReportItem;->status:Ljava/lang/String;

    iget-object v5, v1, Lcom/android/mms/ui/DeliveryReportItem;->deliveryDate:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/mms/ui/DeliveryReportListItem;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, v2

    :cond_0
    return-object p2

    :cond_1
    instance-of v3, p2, Lcom/android/mms/ui/DeliveryReportListItem;

    if-eqz v3, :cond_0

    move-object v2, p2

    check-cast v2, Lcom/android/mms/ui/DeliveryReportListItem;

    goto :goto_0
.end method
