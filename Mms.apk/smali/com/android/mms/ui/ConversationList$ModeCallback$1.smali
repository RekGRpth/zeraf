.class Lcom/android/mms/ui/ConversationList$ModeCallback$1;
.super Ljava/lang/Object;
.source "ConversationList.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ConversationList$ModeCallback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ConversationList$ModeCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$1;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$1;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iget-object v0, v0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList;->access$800(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/ConversationListAdapter;->isAllSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$1;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$1;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iget-object v1, v1, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1}, Lcom/android/mms/ui/ConversationList;->access$2500(Lcom/android/mms/ui/ConversationList;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/android/mms/ui/ConversationList$ModeCallback;->access$3500(Lcom/android/mms/ui/ConversationList$ModeCallback;Landroid/view/ActionMode;Z)V

    :goto_0
    return v3

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$1;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback$1;->this$1:Lcom/android/mms/ui/ConversationList$ModeCallback;

    iget-object v1, v1, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1}, Lcom/android/mms/ui/ConversationList;->access$2500(Lcom/android/mms/ui/ConversationList;)Landroid/view/ActionMode;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/mms/ui/ConversationList$ModeCallback;->access$3500(Lcom/android/mms/ui/ConversationList$ModeCallback;Landroid/view/ActionMode;Z)V

    goto :goto_0
.end method
