.class public Lcom/android/mms/ui/RecipientListActivity;
.super Landroid/app/ListActivity;
.source "RecipientListActivity.java"

# interfaces
.implements Lcom/android/mms/data/Contact$UpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/RecipientListActivity$RecipientListAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RecipientListActivity"


# instance fields
.field private mContacts:Lcom/android/mms/data/ContactList;

.field private mConv:Lcom/android/mms/data/Conversation;

.field private mFirstEnterActivity:Z

.field private mHandler:Landroid/os/Handler;

.field private mThreadId:J


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/RecipientListActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/RecipientListActivity;)Lcom/android/mms/data/ContactList;
    .locals 1
    .param p0    # Lcom/android/mms/ui/RecipientListActivity;

    iget-object v0, p0, Lcom/android/mms/ui/RecipientListActivity;->mContacts:Lcom/android/mms/data/ContactList;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/mms/ui/RecipientListActivity;Lcom/android/mms/data/ContactList;)Lcom/android/mms/data/ContactList;
    .locals 0
    .param p0    # Lcom/android/mms/ui/RecipientListActivity;
    .param p1    # Lcom/android/mms/data/ContactList;

    iput-object p1, p0, Lcom/android/mms/ui/RecipientListActivity;->mContacts:Lcom/android/mms/data/ContactList;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/mms/ui/RecipientListActivity;)Lcom/android/mms/data/Conversation;
    .locals 1
    .param p0    # Lcom/android/mms/ui/RecipientListActivity;

    iget-object v0, p0, Lcom/android/mms/ui/RecipientListActivity;->mConv:Lcom/android/mms/data/Conversation;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v2, "thread_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mThreadId:J

    :goto_0
    iget-wide v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mThreadId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    const-string v2, "RecipientListActivity"

    const-string v3, "No thread_id specified in extras or icicle. Finishing..."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "thread_id"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mThreadId:J

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mThreadId:J

    invoke-static {p0, v2, v3, v7}, Lcom/android/mms/data/Conversation;->get(Landroid/content/Context;JZ)Lcom/android/mms/data/Conversation;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mConv:Lcom/android/mms/data/Conversation;

    iget-object v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mConv:Lcom/android/mms/data/Conversation;

    if-nez v2, :cond_2

    const-string v2, "RecipientListActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No conversation found for threadId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/mms/ui/RecipientListActivity;->mThreadId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Finishing..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mConv:Lcom/android/mms/data/Conversation;

    invoke-virtual {v2}, Lcom/android/mms/data/Conversation;->getRecipients()Lcom/android/mms/data/ContactList;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mContacts:Lcom/android/mms/data/ContactList;

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/android/mms/ui/RecipientListActivity$RecipientListAdapter;

    const v4, 0x7f040052

    iget-object v5, p0, Lcom/android/mms/ui/RecipientListActivity;->mContacts:Lcom/android/mms/data/ContactList;

    invoke-direct {v3, p0, v4, v5}, Lcom/android/mms/ui/RecipientListActivity$RecipientListAdapter;-><init>(Landroid/content/Context;ILcom/android/mms/data/ContactList;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    iget-object v2, p0, Lcom/android/mms/ui/RecipientListActivity;->mContacts:Lcom/android/mms/data/ContactList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0008

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/android/mms/data/Contact;->addListener(Lcom/android/mms/data/Contact$UpdateListener;)V

    iput-boolean v7, p0, Lcom/android/mms/ui/RecipientListActivity;->mFirstEnterActivity:Z

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-static {p0}, Lcom/android/mms/data/Contact;->removeListener(Lcom/android/mms/data/Contact$UpdateListener;)V

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :sswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/mms/ui/SettingListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityIfNeeded(Landroid/content/Intent;I)Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f019b -> :sswitch_1
    .end sparse-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "thread_id"

    iget-wide v1, p0, Lcom/android/mms/ui/RecipientListActivity;->mThreadId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-boolean v0, p0, Lcom/android/mms/ui/RecipientListActivity;->mFirstEnterActivity:Z

    if-nez v0, :cond_0

    const-string v0, "RecipientListActivity"

    const-string v1, "onStart: Contact reload thread start"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mms/ui/RecipientListActivity$2;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/RecipientListActivity$2;-><init>(Lcom/android/mms/ui/RecipientListActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/RecipientListActivity;->mFirstEnterActivity:Z

    return-void
.end method

.method public onUpdate(Lcom/android/mms/data/Contact;)V
    .locals 2
    .param p1    # Lcom/android/mms/data/Contact;

    const-string v0, "RecipientListActivity"

    const-string v1, "Contact onUpdate"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/mms/ui/RecipientListActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/mms/ui/RecipientListActivity$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/RecipientListActivity$1;-><init>(Lcom/android/mms/ui/RecipientListActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
