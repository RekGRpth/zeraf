.class Lcom/android/mms/ui/ComposeMessageActivity$19;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity;->lockMessage(Lcom/android/mms/ui/MessageItem;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;

.field final synthetic val$lockUri:Landroid/net/Uri;

.field final synthetic val$locked:Z

.field final synthetic val$msgItem:Lcom/android/mms/ui/MessageItem;

.field final synthetic val$values:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;ZLcom/android/mms/ui/MessageItem;Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iput-boolean p2, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$locked:Z

    iput-object p3, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iput-object p4, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$lockUri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$values:Landroid/content/ContentValues;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$locked:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v0

    new-array v1, v1, [J

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-wide v2, v2, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    aput-wide v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/mediatek/mms/ipmessage/MessageManager;->addMessageToImportantList([J)Z

    :goto_0
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$lockUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$values:Landroid/content/ContentValues;

    invoke-virtual {v0, v1, v2, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v0

    new-array v1, v1, [J

    iget-object v2, p0, Lcom/android/mms/ui/ComposeMessageActivity$19;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-wide v2, v2, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    aput-wide v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/mediatek/mms/ipmessage/MessageManager;->deleteMessageFromImportantList([J)Z

    goto :goto_0
.end method
