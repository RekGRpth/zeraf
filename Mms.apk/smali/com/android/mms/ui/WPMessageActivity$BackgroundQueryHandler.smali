.class final Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;
.super Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;
.source "WPMessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/WPMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BackgroundQueryHandler"
.end annotation


# instance fields
.field private dialog:Lcom/android/mms/ui/NewProgressDialog;

.field final synthetic this$0:Lcom/android/mms/ui/WPMessageActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/WPMessageActivity;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-direct {p0, p2}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected dismissProgressDialog()V
    .locals 3

    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->dialog:Lcom/android/mms/ui/NewProgressDialog;

    if-nez v1, :cond_0

    const-string v1, "Mms/WapPush"

    const-string v2, "mDialog is null!"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->dialog:Lcom/android/mms/ui/NewProgressDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/mms/ui/NewProgressDialog;->setDismiss(Z)V

    :try_start_0
    iget-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->dialog:Lcom/android/mms/ui/NewProgressDialog;

    invoke-virtual {v1}, Lcom/android/mms/ui/NewProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->dialog:Lcom/android/mms/ui/NewProgressDialog;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Mms/WapPush"

    const-string v2, "ignore IllegalArgumentException"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/android/mms/ui/WPMessageActivity;->access$200()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-static {}, Lcom/android/mms/ui/WPMessageActivity;->access$210()I

    const-string v0, "Mms/WapPush"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "igonre a onDeleteComplete,mDeleteCounter:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/mms/ui/WPMessageActivity;->access$200()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/mms/ui/WPMessageActivity;->access$202(I)I

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    const-wide/16 v1, -0x2

    invoke-static {v0, v1, v2}, Lcom/android/mms/transaction/WapPushMessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;J)V

    invoke-virtual {p0}, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->dismissProgressDialog()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x25e4
        :pswitch_0
    .end packed-switch
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v3, v3, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v3, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/WPMessageActivity;->access$300(Lcom/android/mms/ui/WPMessageActivity;)Landroid/view/ActionMode;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/WPMessageActivity;->access$400(Lcom/android/mms/ui/WPMessageActivity;)Lcom/android/mms/ui/WPMessageActivity$ModeCallback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mms/ui/WPMessageActivity$ModeCallback;->confirmSyncCheckedPositons()V

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->invalidateOptionsMenu()V

    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/WPMessageActivity;->access$900(Lcom/android/mms/ui/WPMessageActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/WPMessageActivity;->access$1000(Lcom/android/mms/ui/WPMessageActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3, v5}, Lcom/android/mms/ui/WPMessageActivity;->access$1002(Lcom/android/mms/ui/WPMessageActivity;Z)Z

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v3}, Lcom/android/mms/ui/WPMessageActivity;->markAllMessageAsSeen()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v3, v3, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    iget-object v3, v3, Lcom/android/mms/ui/WPMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/WPMessageListAdapter;

    invoke-virtual {v3}, Lcom/android/mms/ui/WPMessageListAdapter;->uncheckAll()V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/WPMessageActivity;->access$900(Lcom/android/mms/ui/WPMessageActivity;)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :pswitch_1
    const/4 v0, 0x0

    if-eqz p3, :cond_4

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_4
    const/16 v3, 0x63

    if-le v0, v3, :cond_5

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/WPMessageActivity;->access$1100(Lcom/android/mms/ui/WPMessageActivity;)Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "99+"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    new-instance v1, Lcom/mediatek/encapsulation/android/content/res/EncapsulatedResources;

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-virtual {v3}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/mediatek/encapsulation/android/content/res/EncapsulatedResources;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v1}, Lcom/mediatek/encapsulation/android/content/res/EncapsulatedResources;->getThemeMainColor()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/WPMessageActivity;->access$1100(Lcom/android/mms/ui/WPMessageActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->this$0:Lcom/android/mms/ui/WPMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/WPMessageActivity;->access$1100(Lcom/android/mms/ui/WPMessageActivity;)Landroid/widget/TextView;

    move-result-object v4

    if-lez v0, :cond_6

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    :goto_4
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x2537
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setProgressDialog(Lcom/android/mms/ui/NewProgressDialog;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/NewProgressDialog;

    iput-object p1, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->dialog:Lcom/android/mms/ui/NewProgressDialog;

    return-void
.end method

.method public showProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->dialog:Lcom/android/mms/ui/NewProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/WPMessageActivity$BackgroundQueryHandler;->dialog:Lcom/android/mms/ui/NewProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method
