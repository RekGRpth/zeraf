.class Lcom/android/mms/ui/ConversationList$ModeCallback;
.super Ljava/lang/Object;
.source "ConversationList.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ConversationList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModeCallback"
.end annotation


# instance fields
.field private mCheckedNum:I

.field private mCheckedPosition:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDeleteItem:Landroid/view/MenuItem;

.field private mIsSelectAll:Z

.field private mMultiSelectActionBarView:Landroid/view/View;

.field private mSelectedThreadIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectionTitle:Landroid/widget/Button;

.field final synthetic this$0:Lcom/android/mms/ui/ConversationList;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/ConversationList;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    iput-boolean v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mIsSelectAll:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/ConversationList;Lcom/android/mms/ui/ConversationList$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/ConversationList;
    .param p2    # Lcom/android/mms/ui/ConversationList$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/ConversationList$ModeCallback;-><init>(Lcom/android/mms/ui/ConversationList;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/mms/ui/ConversationList$ModeCallback;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList$ModeCallback;

    invoke-direct {p0}, Lcom/android/mms/ui/ConversationList$ModeCallback;->updateActionMode()V

    return-void
.end method

.method static synthetic access$3500(Lcom/android/mms/ui/ConversationList$ModeCallback;Landroid/view/ActionMode;Z)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList$ModeCallback;
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/ConversationList$ModeCallback;->setAllItemChecked(Landroid/view/ActionMode;Z)V

    return-void
.end method

.method static synthetic access$3802(Lcom/android/mms/ui/ConversationList$ModeCallback;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList$ModeCallback;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mIsSelectAll:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/android/mms/ui/ConversationList$ModeCallback;)Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList$ModeCallback;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mDeleteItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method private setAllItemChecked(Landroid/view/ActionMode;Z)V
    .locals 4
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mIsSelectAll:Z

    new-instance v0, Lcom/android/mms/ui/AsyncDialog;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-direct {v0, v1}, Lcom/android/mms/ui/AsyncDialog;-><init>(Landroid/app/Activity;)V

    new-instance v1, Lcom/android/mms/ui/ConversationList$ModeCallback$5;

    invoke-direct {v1, p0, p2}, Lcom/android/mms/ui/ConversationList$ModeCallback$5;-><init>(Lcom/android/mms/ui/ConversationList$ModeCallback;Z)V

    new-instance v2, Lcom/android/mms/ui/ConversationList$ModeCallback$6;

    invoke-direct {v2, p0, p2}, Lcom/android/mms/ui/ConversationList$ModeCallback$6;-><init>(Lcom/android/mms/ui/ConversationList$ModeCallback;Z)V

    const v3, 0x7f0b010e

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/mms/ui/AsyncDialog;->runAsync(Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    return-void
.end method

.method private updateActionMode()V
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mDeleteItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mDeleteItem:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    :goto_0
    iget v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList;->access$2500(Lcom/android/mms/ui/ConversationList;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList;->access$2500(Lcom/android/mms/ui/ConversationList;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectionTitle:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0001

    iget v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v0}, Lcom/android/mms/ui/ConversationList;->access$3700(Lcom/android/mms/ui/ConversationList;)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mDeleteItem:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public confirmSyncCheckedPositons()V
    .locals 13

    iget-boolean v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mIsSelectAll:Z

    if-eqz v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->clear()V

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->clear()V

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-virtual {v6}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/ConversationListAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v4

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_2

    invoke-virtual {v3, v5}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v6, v2}, Lcom/android/mms/data/Conversation;->getFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/mms/data/Conversation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    iput v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectionTitle:Landroid/widget/Button;

    iget-object v7, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-virtual {v7}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0001

    iget v9, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v8, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v6}, Lcom/android/mms/ui/ConversationList;->access$3700(Lcom/android/mms/ui/ConversationList;)V

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    const/4 v7, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    :cond_0
    :goto_0
    return v7

    :sswitch_0
    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    if-lez v4, :cond_1

    const-string v4, "ConversationList"

    const-string v5, "ConversationList->ModeCallback: delete"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v5}, Lcom/android/mms/ui/ConversationList;->access$1200(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationList$ThreadListQueryHandler;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/mms/ui/ConversationList;->confirmDeleteThreads(Ljava/util/Collection;Landroid/content/AsyncQueryHandler;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    invoke-interface {p2, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :sswitch_1
    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    if-lez v4, :cond_2

    const-string v4, "ConversationList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "threadIds:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/AbstractCollection;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-static {v4, v5, p1}, Lcom/android/mms/ui/ConversationList;->access$3600(Lcom/android/mms/ui/ConversationList;Ljava/util/HashSet;Landroid/view/ActionMode;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v4}, Lcom/android/mms/ui/ConversationList;->access$400(Lcom/android/mms/ui/ConversationList;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/android/mms/ui/ConversationList$ModeCallback$2;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/ConversationList$ModeCallback$2;-><init>(Lcom/android/mms/ui/ConversationList$ModeCallback;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :sswitch_2
    const-string v4, "ConversationList"

    const-string v5, "click shortcut!"

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    if-lez v4, :cond_3

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-static {v4, v5}, Lcom/android/mms/ui/MessageUtils;->addShortcutToLauncher(Landroid/content/Context;Ljava/util/HashSet;)V

    :cond_3
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto :goto_0

    :sswitch_3
    const-string v4, "ConversationList"

    const-string v5, "click mark as spam!"

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    new-instance v1, Lcom/android/mms/ui/ConversationList$ModeCallback$3;

    invoke-direct {v1, p0, v3, p1}, Lcom/android/mms/ui/ConversationList$ModeCallback$3;-><init>(Lcom/android/mms/ui/ConversationList$ModeCallback;Ljava/util/HashSet;Landroid/view/ActionMode;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b015b

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v5}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v5

    const/16 v6, 0xea

    invoke-virtual {v5, v6}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0b0007

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v5}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v5

    const/16 v6, 0x85

    invoke-virtual {v5, v6}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :sswitch_4
    const-string v4, "ConversationList"

    const-string v5, "click mark as nonspam!"

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/android/mms/ui/ConversationList$ModeCallback$4;

    invoke-direct {v5, p0, v2}, Lcom/android/mms/ui/ConversationList$ModeCallback$4;-><init>(Lcom/android/mms/ui/ConversationList$ModeCallback;Ljava/util/HashSet;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0049 -> :sswitch_0
        0x7f0f01a0 -> :sswitch_1
        0x7f0f01a1 -> :sswitch_3
        0x7f0f01a2 -> :sswitch_2
        0x7f0f01a3 -> :sswitch_4
    .end sparse-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const v5, 0x7f0f0072

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_1

    const v1, 0x7f0e0003

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :goto_0
    const v1, 0x7f0f0049

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mDeleteItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040015

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectionTitle:Landroid/widget/Button;

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f0b0315

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1, v4}, Lcom/android/mms/ui/ConversationList;->access$3202(Lcom/android/mms/ui/ConversationList;Z)Z

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-virtual {v1}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setLongClickable(Z)V

    return v4

    :cond_1
    const v1, 0x7f0e0002

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 5
    .param p1    # Landroid/view/ActionMode;

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-virtual {v1}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/mms/ui/ConversationListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/ConversationListAdapter;->uncheckSelect(Ljava/util/HashSet;)V

    iput-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    iput-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1, v4}, Lcom/android/mms/ui/ConversationList;->access$3202(Lcom/android/mms/ui/ConversationList;Z)Z

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-virtual {v1}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setLongClickable(Z)V

    iput v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1, v3}, Lcom/android/mms/ui/ConversationList;->access$2502(Lcom/android/mms/ui/ConversationList;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1}, Lcom/android/mms/ui/ConversationList;->access$800(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationListAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v1}, Lcom/android/mms/ui/ConversationList;->access$800(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/ConversationListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/ui/ConversationListAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 11
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const v10, 0x7f0f01a3

    const v9, 0x7f0f01a2

    const v8, 0x7f0f01a1

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040015

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {p1, v2}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v4, 0x7f0f0072

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectionTitle:Landroid/widget/Button;

    :cond_0
    new-instance v0, Lcom/android/mms/ui/CustomMenu;

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-direct {v0, v3}, Lcom/android/mms/ui/CustomMenu;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectionTitle:Landroid/widget/Button;

    const v5, 0x7f0e0008

    invoke-virtual {v0, v4, v5}, Lcom/android/mms/ui/CustomMenu;->addDropDownMenu(Landroid/widget/Button;I)Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/mms/ui/ConversationList;->access$3302(Lcom/android/mms/ui/ConversationList;Lcom/android/mms/ui/CustomMenu$DropDownMenu;)Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v4}, Lcom/android/mms/ui/ConversationList;->access$3300(Lcom/android/mms/ui/ConversationList;)Lcom/android/mms/ui/CustomMenu$DropDownMenu;

    move-result-object v4

    const v5, 0x7f0f01ab

    invoke-virtual {v4, v5}, Lcom/android/mms/ui/CustomMenu$DropDownMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/mms/ui/ConversationList;->access$3402(Lcom/android/mms/ui/ConversationList;Landroid/view/MenuItem;)Landroid/view/MenuItem;

    new-instance v3, Lcom/android/mms/ui/ConversationList$ModeCallback$1;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/ConversationList$ModeCallback$1;-><init>(Lcom/android/mms/ui/ConversationList$ModeCallback;)V

    invoke-virtual {v0, v3}, Lcom/android/mms/ui/CustomMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    sget v3, Lcom/android/mms/ui/ConversationList;->sConversationListOption:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_5

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    invoke-interface {p2, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    :goto_0
    iget-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v3}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v3

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Lcom/mediatek/mms/ipmessage/ServiceManager;->isFeatureSupported(I)Z

    move-result v3

    if-nez v3, :cond_4

    const v3, 0x7f0f01a0

    invoke-interface {p2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_4
    return v7

    :cond_5
    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v3, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v3}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_6
    :goto_1
    invoke-interface {p2, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_7
    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_8
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public setItemChecked(IZLandroid/database/Cursor;)V
    .locals 6
    .param p1    # I
    .param p2    # Z
    .param p3    # Landroid/database/Cursor;

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-virtual {v4}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    if-nez p3, :cond_1

    invoke-virtual {v1, p1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/database/Cursor;

    :goto_0
    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->this$0:Lcom/android/mms/ui/ConversationList;

    invoke-static {v4, p3}, Lcom/android/mms/data/Conversation;->getFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/mms/data/Conversation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/data/Conversation;->isChecked()Z

    move-result v4

    if-ne p2, v4, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-interface {p3, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p2}, Lcom/android/mms/data/Conversation;->setIsChecked(Z)V

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v2

    if-eqz p2, :cond_3

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mSelectedThreadIds:Ljava/util/HashSet;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedPosition:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/mms/ui/ConversationList$ModeCallback;->mCheckedNum:I

    goto :goto_1
.end method
