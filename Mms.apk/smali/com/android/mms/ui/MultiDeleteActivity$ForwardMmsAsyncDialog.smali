.class Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;
.super Lcom/android/mms/ui/AsyncDialog;
.source "MultiDeleteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MultiDeleteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ForwardMmsAsyncDialog"
.end annotation


# instance fields
.field private mTempMmsUri:Landroid/net/Uri;

.field private mTempThreadId:J

.field final synthetic this$0:Lcom/android/mms/ui/MultiDeleteActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/app/Activity;)V
    .locals 0
    .param p2    # Landroid/app/Activity;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0, p2}, Lcom/android/mms/ui/AsyncDialog;-><init>(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$4400(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->mTempMmsUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->mTempMmsUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;)J
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    iget-wide v0, p0, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->mTempThreadId:J

    return-wide v0
.end method

.method static synthetic access$4502(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;J)J
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->mTempThreadId:J

    return-wide p1
.end method
