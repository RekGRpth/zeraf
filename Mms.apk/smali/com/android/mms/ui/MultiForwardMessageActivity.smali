.class public Lcom/android/mms/ui/MultiForwardMessageActivity;
.super Landroid/app/Activity;
.source "MultiForwardMessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;,
        Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;
    }
.end annotation


# static fields
.field private static final ALL_COUNT:Ljava/lang/String; = "AllCount"

.field private static final ERROR_DIALOG_MSG:Ljava/lang/String; = "ErrorDialogMsg"

.field private static final ERROR_DIALOG_TITLE:Ljava/lang/String; = "ErrorDialogTitle"

.field private static final EXIT_ECM_RESULT:Ljava/lang/String; = "exit_ecm_result"

.field private static final FAILED_COUNT:Ljava/lang/String; = "FailedCount"

.field private static final INCREASE_PROCESS:I = 0x1

.field private static final MMS:Ljava/lang/String; = "mms"

.field private static final MMS_PROJECTION:[Ljava/lang/String;

.field private static final MSG_SAVE_MESSAGE_TO_SIM_AFTER_SELECT_SIM:I = 0x68

.field public static final MULTI_FORWARD_ACTION:Ljava/lang/String; = "com.android.mms.ui.MultiForwardMessageActivity"

.field public static final MULTI_FORWARD_PARAM_MESSAGEIDS:Ljava/lang/String; = "MultiForwardMessageParamMessageIds"

.field public static final MULTI_FORWARD_PARAM_NUMBERS:Ljava/lang/String; = "MultiForwardMessageParamNumbers"

.field public static final MULTI_FORWARD_PARAM_THREADID:Ljava/lang/String; = "MultiForwardMessageParamThreadId"

.field private static final PROCESSING_FORWARD:Ljava/lang/String; = "forwardingMsg"

.field private static final REQUEST_CODE_ECM_EXIT_DIALOG:I = 0x6b

.field private static final SELECT_TYPE:Ljava/lang/String; = "Select_type"

.field private static final SENT_COUNT:Ljava/lang/String; = "SentCount"

.field private static final SHOW_ERROR:I = 0x3

.field private static final SHOW_RESULT:I = 0x2

.field private static final SIM_SELECT_FOR_SAVE_MSG_TO_SIM:I = 0x2

.field private static final SIM_SELECT_FOR_SEND_MSG:I = 0x1

.field private static final SMS:Ljava/lang/String; = "sms"

.field private static final SMS_PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "Mms/MultiForwardMessage"

.field private static mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;


# instance fields
.field private mAllCount:I

.field private mAssociatedSimId:I

.field private mFailedCount:I

.field private mForwardingMsg:Z

.field private mHandler:Landroid/os/Handler;

.field private mMessageSimId:J

.field private mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

.field private mMsgIds:Ljava/lang/String;

.field private mNumbers:Ljava/lang/String;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressBarText:Landroid/widget/TextView;

.field private mSelectedSimId:I

.field private mSentCount:I

.field private mSimCount:I

.field private mSimInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;

.field private mUnDownloadedIpMessageCounter:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    sput-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "body"

    aput-object v1, v0, v3

    const-string v1, "ipmsg_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->SMS_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "thread_id"

    aput-object v1, v0, v3

    const-string v1, "sub"

    aput-object v1, v0, v4

    const-string v1, "sub_cs"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "date_sent"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "read"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "m_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "msg_box"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "d_rpt"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "rr"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "locked"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "sim_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "service_center"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "st"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->MMS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    iput v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMessageSimId:J

    iput-boolean v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mForwardingMsg:Z

    iput v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAllCount:I

    iput v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSentCount:I

    iput v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mFailedCount:I

    iput v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mNumbers:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMsgIds:Ljava/lang/String;

    new-instance v0, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    invoke-direct {v0}, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    new-instance v0, Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;

    invoke-direct {v0}, Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;

    iput v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mUnDownloadedIpMessageCounter:I

    new-instance v0, Lcom/android/mms/ui/MultiForwardMessageActivity$2;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/MultiForwardMessageActivity$2;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$002(Lcom/android/mms/ui/MultiForwardMessageActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mForwardingMsg:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/mms/ui/MultiForwardMessageActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->getSimInfoList()V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/mms/ui/MultiForwardMessageActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    return v0
.end method

.method static synthetic access$1002(Lcom/android/mms/ui/MultiForwardMessageActivity;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    return p1
.end method

.method static synthetic access$1100(Lcom/android/mms/ui/MultiForwardMessageActivity;Z)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->forwardDirectly(Z)V

    return-void
.end method

.method static synthetic access$1202(Lcom/android/mms/ui/MultiForwardMessageActivity;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAllCount:I

    return p1
.end method

.method static synthetic access$1300()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->MMS_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/mms/ui/MultiForwardMessageActivity;Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;)Lcom/android/mms/ui/MessageItem;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->getMessageItemFromCursor(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;)Lcom/android/mms/ui/MessageItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1508(Lcom/android/mms/ui/MultiForwardMessageActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mUnDownloadedIpMessageCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mUnDownloadedIpMessageCounter:I

    return v0
.end method

.method static synthetic access$1600(Lcom/android/mms/ui/MultiForwardMessageActivity;Lcom/android/mms/ui/MessageItem;[Ljava/lang/String;IJ)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Lcom/android/mms/ui/MessageItem;
    .param p2    # [Ljava/lang/String;
    .param p3    # I
    .param p4    # J

    invoke-direct/range {p0 .. p5}, Lcom/android/mms/ui/MultiForwardMessageActivity;->forwardMms(Lcom/android/mms/ui/MessageItem;[Ljava/lang/String;IJ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->SMS_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/mms/ui/MultiForwardMessageActivity;)Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/mms/ui/MultiForwardMessageActivity;[Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->isRecipientIpMessageUser([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/android/mms/ui/MultiForwardMessageActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mNumbers:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/mms/ui/MultiForwardMessageActivity;Lcom/android/mms/data/WorkingMessage;JI)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Lcom/android/mms/data/WorkingMessage;
    .param p2    # J
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMms(Lcom/android/mms/data/WorkingMessage;JI)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/android/mms/ui/MultiForwardMessageActivity;Ljava/lang/String;[Ljava/lang/String;IJ)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # I
    .param p4    # J

    invoke-direct/range {p0 .. p5}, Lcom/android/mms/ui/MultiForwardMessageActivity;->forwardSms(Ljava/lang/String;[Ljava/lang/String;IJ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/android/mms/ui/MultiForwardMessageActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/mms/ui/MultiForwardMessageActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/mms/ui/MultiForwardMessageActivity;Z)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->checkEcmMode(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/mms/ui/MultiForwardMessageActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMsgIds:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/mms/ui/MultiForwardMessageActivity;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/MultiForwardMessageActivity;->simSelection(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/android/mms/ui/MultiForwardMessageActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mms/ui/MultiForwardMessageActivity;I)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->finishSelf(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/mms/ui/MultiForwardMessageActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/MultiForwardMessageActivity;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800()Lcom/mediatek/CellConnService/CellConnMgr;
    .locals 1

    sget-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/mms/ui/MultiForwardMessageActivity;III)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiForwardMessageActivity;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V

    return-void
.end method

.method private checkEcmMode(Z)V
    .locals 7
    .param p1    # Z

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mForwardingMsg:Z

    const/16 v1, 0x12e

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0x132

    iget v4, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    int-to-long v4, v4

    invoke-static {p0, v4, v5}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v2

    const-string v4, "Mms/Txn"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "check pin and...: simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\t slotId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, p1

    sget-object v4, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    new-instance v5, Lcom/android/mms/ui/MultiForwardMessageActivity$3;

    invoke-direct {v5, p0, v2, v0}, Lcom/android/mms/ui/MultiForwardMessageActivity$3;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;IZ)V

    invoke-virtual {v4, v2, v1, v5}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(IILjava/lang/Runnable;)I

    return-void
.end method

.method private checkSimInfoAndSendAsync()V
    .locals 4

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v2

    const/16 v3, 0xa3

    invoke-virtual {v2, v3}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v2

    const/16 v3, 0x9e

    invoke-virtual {v2, v3}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/android/mms/ui/MultiForwardMessageActivity$1;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/MultiForwardMessageActivity$1;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private doForward(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/16 v9, 0xa9

    const/16 v8, 0xa8

    const/4 v7, 0x1

    const/16 v6, 0xa7

    const/4 v5, 0x3

    const-string v3, "Mms/MultiForwardMessage"

    const-string v4, "doForward begin"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const-string v3, "Mms/MultiForwardMessage"

    const-string v4, "multiForard failed for empty numbers"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v5, v6, v8}, Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V

    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const-string v3, "Mms/MultiForwardMessage"

    const-string v4, "multiForard failed for empty message ids"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v5, v6, v9}, Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V

    goto :goto_0

    :cond_3
    const-string v3, ","

    const-string v4, ";"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v3, ";"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    array-length v3, v2

    if-ge v3, v7, :cond_5

    :cond_4
    const-string v3, "Mms/MultiForwardMessage"

    const-string v4, "multiForard failed for empty numbers"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v5, v6, v8}, Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V

    goto :goto_0

    :cond_5
    const-string v3, ";"

    const-string v4, ","

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    array-length v3, v1

    if-ge v3, v7, :cond_7

    :cond_6
    const-string v3, "Mms/MultiForwardMessage"

    const-string v4, "multiForard failed for empty message ids"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v5, v6, v9}, Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V

    goto :goto_0

    :cond_7
    const-string v3, "Mms/MultiForwardMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Forward Begin; simId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " msgIds:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/android/mms/ui/MultiForwardMessageActivity$4;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/android/mms/ui/MultiForwardMessageActivity$4;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    const-string v5, "ForwardThread"

    invoke-direct {v3, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0
.end method

.method private finishSelf(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->setResult(I)V

    iget v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mUnDownloadedIpMessageCounter:I

    if-lez v0, :cond_0

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v0

    const/16 v1, 0xef

    invoke-virtual {v0, v1}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->finish()V

    return-void
.end method

.method private forwardDirectly(Z)V
    .locals 5
    .param p1    # Z

    if-eqz p1, :cond_1

    const-string v2, "ril.cdma.inecmmode"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/16 v3, 0x6b

    invoke-virtual {p0, v2, v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Mms/MultiForwardMessage"

    const-string v3, "Cannot find EmergencyCallbackModeExitDialog"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/mms/ui/MultiForwardMessageActivity;->finishSelf(I)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mNumbers:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMsgIds:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->doForward(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mNumbers:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMsgIds:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->doForward(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private forwardMms(Lcom/android/mms/ui/MessageItem;[Ljava/lang/String;IJ)Z
    .locals 10
    .param p1    # Lcom/android/mms/ui/MessageItem;
    .param p2    # [Ljava/lang/String;
    .param p3    # I
    .param p4    # J

    new-instance v3, Lcom/google/android/mms/pdu/SendReq;

    invoke-direct {v3}, Lcom/google/android/mms/pdu/SendReq;-><init>()V

    const v7, 0x7f0b0259

    invoke-virtual {p0, v7}, Lcom/android/mms/ui/MultiForwardMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p1, Lcom/android/mms/ui/MessageItem;->mSubject:Ljava/lang/String;

    if-eqz v7, :cond_0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/android/mms/ui/MessageItem;->mSubject:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_0
    new-instance v7, Lcom/google/android/mms/pdu/EncodedStringValue;

    invoke-direct {v7, v5}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Lcom/google/android/mms/pdu/SendReq;->setSubject(Lcom/google/android/mms/pdu/EncodedStringValue;)V

    iget-object v7, p1, Lcom/android/mms/ui/MessageItem;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v7}, Lcom/android/mms/model/SlideshowModel;->makeCopy()Lcom/google/android/mms/pdu/PduBody;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/google/android/mms/pdu/SendReq;->setBody(Lcom/google/android/mms/pdu/PduBody;)V

    invoke-static {p2}, Lcom/google/android/mms/pdu/EncodedStringValue;->encodeStrings([Ljava/lang/String;)[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v3, v1}, Lcom/google/android/mms/pdu/SendReq;->setTo([Lcom/google/android/mms/pdu/EncodedStringValue;)V

    :cond_1
    const/4 v6, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v2

    sget-object v7, Landroid/provider/Telephony$Mms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v7}, Lcom/google/android/mms/pdu/PduPersister;->persist(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    new-instance v4, Lcom/android/mms/transaction/MmsMessageSender;

    iget-object v7, p1, Lcom/android/mms/ui/MessageItem;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v7}, Lcom/android/mms/model/SlideshowModel;->getCurrentSlideshowSize()I

    move-result v7

    int-to-long v7, v7

    invoke-direct {v4, p0, v6, v7, v8}, Lcom/android/mms/transaction/MmsMessageSender;-><init>(Landroid/content/Context;Landroid/net/Uri;J)V

    :try_start_1
    invoke-interface {v4, p3}, Lcom/android/mms/transaction/MessageSender;->setSimId(I)V

    invoke-interface {v4, p4, p5}, Lcom/android/mms/transaction/MessageSender;->sendMessage(J)Z
    :try_end_1
    .catch Lcom/google/android/mms/MmsException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v7, 0x1

    :goto_0
    return v7

    :catch_0
    move-exception v0

    const-string v7, "Mms/MultiForwardMessage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "forwardMms Failed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/android/mms/ui/MessageItem;->mMessageUri:Landroid/net/Uri;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v7, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v7, "Mms/MultiForwardMessage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "forwardMms Failed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/android/mms/ui/MessageItem;->mMessageUri:Landroid/net/Uri;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v7, 0x0

    goto :goto_0
.end method

.method private forwardSms(Ljava/lang/String;[Ljava/lang/String;IJ)Z
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # I
    .param p4    # J

    new-instance v0, Lcom/android/mms/transaction/SmsMessageSender;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/transaction/SmsMessageSender;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v0, p3}, Lcom/android/mms/transaction/SmsMessageSender;->setSimId(I)V

    :try_start_0
    invoke-virtual {v0, p4, p5}, Lcom/android/mms/transaction/SmsMessageSender;->sendMessage(J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v6

    const-string v1, "Mms/MultiForwardMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to send SMS message, threadId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getMessageItemFromCursor(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;)Lcom/android/mms/ui/MessageItem;
    .locals 27
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Ljava/lang/String;

    if-eqz p2, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->isCursorValid(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v20, 0x0

    const-string v3, "mms"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsMessageBox:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsMessageType:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsSimId:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsLocked:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsSubjectCharset:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMsgId:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    move-object/from16 v12, p3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsSubject:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsServiceCenter:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsDeliveryReport:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsReadReport:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsStatus:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMmsColumnsMap:Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$MmsColumnsMap;->mColumnMmsDate:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    :try_start_0
    new-instance v2, Lcom/android/mms/ui/MessageItem;

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v24}, Lcom/android/mms/ui/MessageItem;-><init>(Landroid/content/Context;IIIIIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;ZZIZJI)V
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v25

    const-string v3, "Mms/MultiForwardMessage"

    const-string v26, "getMessageItemFromCursor failed: "

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-static {v3, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getSimInfoList()V
    .locals 5

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-static {p0, v2}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    move-result-object v1

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_0
    iput v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimCount:I

    const-string v2, "Mms/MultiForwardMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MultiforwardMessage: getSimInfoList; mSimCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_0
.end method

.method private initProgressDialog()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x9e

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f012f

    invoke-virtual {p0, v1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const v1, 0x7f0f0130

    invoke-virtual {p0, v1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBarText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBarText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBarText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    :cond_0
    return-void
.end method

.method private isCursorValid(Landroid/database/Cursor;)Z
    .locals 1
    .param p1    # Landroid/database/Cursor;

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isRecipientIpMessageUser([Ljava/lang/String;)Z
    .locals 7
    .param p1    # [Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    array-length v6, p1

    if-ge v6, v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    move-object v0, p1

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getContactManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ContactManager;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/mediatek/mms/ipmessage/ContactManager;->isIpMessageNumber(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v4, v5

    goto :goto_0
.end method

.method private sendMms(Lcom/android/mms/data/WorkingMessage;JI)Z
    .locals 7
    .param p1    # Lcom/android/mms/data/WorkingMessage;
    .param p2    # J
    .param p4    # I

    const/4 v3, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1, v3}, Lcom/android/mms/data/WorkingMessage;->saveAsMms(Z)Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/android/mms/data/WorkingMessage;->getMessageUri()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v1, Lcom/android/mms/transaction/MmsMessageSender;

    invoke-virtual {p1}, Lcom/android/mms/data/WorkingMessage;->getSlideshow()Lcom/android/mms/model/SlideshowModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mms/model/SlideshowModel;->getCurrentSlideshowSize()I

    move-result v4

    int-to-long v4, v4

    invoke-direct {v1, p0, v2, v4, v5}, Lcom/android/mms/transaction/MmsMessageSender;-><init>(Landroid/content/Context;Landroid/net/Uri;J)V

    :try_start_0
    invoke-interface {v1, p4}, Lcom/android/mms/transaction/MessageSender;->setSimId(I)V

    invoke-interface {v1, p2, p3}, Lcom/android/mms/transaction/MessageSender;->sendMessage(J)Z
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "Mms/MultiForwardMessage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "forwardMms Failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private sendMsgToHandler(III)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    iput p1, v2, Landroid/os/Message;->what:I

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "ErrorDialogTitle"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ErrorDialogMsg"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    const v3, 0x7f0d0004

    invoke-direct {v2, p0, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b027c

    new-instance v4, Lcom/android/mms/ui/MultiForwardMessageActivity$7;

    invoke-direct {v4, p0}, Lcom/android/mms/ui/MultiForwardMessageActivity$7;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    new-instance v2, Lcom/android/mms/ui/MultiForwardMessageActivity$8;

    invoke-direct {v2, p0}, Lcom/android/mms/ui/MultiForwardMessageActivity$8;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private showResultDialog(III)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-string v5, "Mms/MultiForwardMessage"

    const-string v6, "showResultDialog begin"

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    new-instance v5, Landroid/view/ContextThemeWrapper;

    const v6, 0x7f0d0004

    invoke-direct {v5, p0, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v2, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v5

    const/16 v6, 0xa0

    invoke-virtual {v5, v6}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v5

    const/16 v6, 0xa1

    invoke-virtual {v5, v6}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v5

    const/16 v6, 0xa2

    invoke-virtual {v5, v6}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v5

    const/16 v6, 0x9f

    invoke-virtual {v5, v6}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0b027c

    new-instance v7, Lcom/android/mms/ui/MultiForwardMessageActivity$5;

    invoke-direct {v7, p0}, Lcom/android/mms/ui/MultiForwardMessageActivity$5;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    new-instance v5, Lcom/android/mms/ui/MultiForwardMessageActivity$6;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/MultiForwardMessageActivity$6;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const-string v5, "Mms/MultiForwardMessage"

    const-string v6, "showResultDialog end"

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showSimSelectedDialog(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v9, "Mms/MultiForwardMessage"

    const-string v10, "showSimSelectedDialog begin"

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, p1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_0
    iget v9, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimCount:I

    if-ge v4, v9, :cond_7

    iget-object v9, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v9, "simIcon"

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimBackgroundRes()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-static {}, Lcom/mediatek/encapsulation/com/mediatek/telephony/EncapsulatedTelephonyManagerEx;->getDefault()Lcom/mediatek/encapsulation/com/mediatek/telephony/EncapsulatedTelephonyManagerEx;

    move-result-object v10

    invoke-static {v4, v9, v10}, Lcom/android/mms/ui/MessageUtils;->getSimStatus(ILjava/util/List;Lcom/mediatek/encapsulation/com/mediatek/telephony/EncapsulatedTelephonyManagerEx;)I

    move-result v8

    const-string v9, "simStatus"

    invoke-static {v8}, Lcom/android/mms/ui/MessageUtils;->getSimStatusResource(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v9

    long-to-int v9, v9

    invoke-static {p0, v9}, Lcom/android/mms/MmsConfig;->isServiceEnabled(Landroid/content/Context;I)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "Mms/MultiForwardMessage"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "show ipmessage icon, simId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "ipmsg_indicator"

    const/16 v10, 0x232a

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    const-string v7, ""

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getDispalyNumberFormat()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :cond_0
    :goto_2
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "simNumberShort"

    const-string v10, ""

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    const-string v9, "simName"

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    const-string v9, "simNumber"

    const-string v10, ""

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_4
    iget v9, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v10

    long-to-int v10, v10

    if-ne v9, v10, :cond_6

    const-string v9, "Select_type"

    const/4 v10, -0x1

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    const/4 v10, 0x2

    if-eq v9, v10, :cond_6

    const-string v9, "suggested"

    const v10, 0x7f0b0084

    invoke-virtual {p0, v10}, Lcom/android/mms/ui/MultiForwardMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_5
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_1
    const-string v9, "Mms/MultiForwardMessage"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "hide ipmessage icon, simId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "ipmsg_indicator"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_0
    const-string v7, ""

    goto :goto_2

    :pswitch_1
    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x4

    if-gt v9, v10, :cond_2

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2

    :cond_2
    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x4

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2

    :pswitch_2
    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x4

    if-gt v9, v10, :cond_3

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2

    :cond_3
    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x4

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2

    :cond_4
    const-string v9, "simNumberShort"

    invoke-virtual {v3, v9, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    :cond_5
    const-string v9, "simNumber"

    invoke-virtual {v6}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getNumber()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    :cond_6
    const-string v9, "suggested"

    const-string v10, ""

    invoke-virtual {v3, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    :cond_7
    invoke-static {v2, p0}, Lcom/android/mms/ui/MessageUtils;->createSimpleAdapter(Ljava/util/List;Landroid/content/Context;)Landroid/widget/SimpleAdapter;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0b0083

    invoke-virtual {p0, v9}, Lcom/android/mms/ui/MultiForwardMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 v9, 0x1

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    new-instance v9, Lcom/android/mms/ui/MultiForwardMessageActivity$9;

    invoke-direct {v9, p0, v5}, Lcom/android/mms/ui/MultiForwardMessageActivity$9;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;Landroid/content/Intent;)V

    invoke-virtual {v1, v0, v9}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v9, Lcom/android/mms/ui/MultiForwardMessageActivity$10;

    invoke-direct {v9, p0, v1}, Lcom/android/mms/ui/MultiForwardMessageActivity$10;-><init>(Lcom/android/mms/ui/MultiForwardMessageActivity;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p0, v9}, Lcom/android/mms/ui/MultiForwardMessageActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    const-string v9, "Mms/MultiForwardMessage"

    const-string v10, "showSimSelectedDialog end"

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private simSelection(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/16 v10, 0xa7

    const/4 v8, 0x3

    const/4 v9, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, "Mms/MultiForwardMessage"

    const-string v6, "simSelection begin"

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v5, "Mms/MultiForwardMessage"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "simSelection begin. mSelectedSimId:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v5, v4, :cond_1

    :cond_0
    const/16 v4, 0xa4

    invoke-direct {p0, v8, v10, v4}, Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V

    :goto_0
    return v3

    :cond_1
    iget-object v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v4, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v3}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v5

    long-to-int v3, v5

    iput v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    invoke-direct {p0, v4}, Lcom/android/mms/ui/MultiForwardMessageActivity;->checkEcmMode(Z)V

    move v3, v4

    goto :goto_0

    :cond_2
    iget v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    if-lt v5, v4, :cond_3

    invoke-direct {p0, v4}, Lcom/android/mms/ui/MultiForwardMessageActivity;->checkEcmMode(Z)V

    move v3, v4

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v4, :cond_a

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v5, "Select_type"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiForwardMessageActivity;->splitNumbers(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v5, v1

    if-ne v5, v4, :cond_4

    aget-object v3, v1, v3

    invoke-static {p0, v3}, Lcom/android/mms/ui/MultiForwardUtils;->getContactSIM(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    :goto_1
    const-string v3, "Mms/MultiForwardMessage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mAssociatedSimId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "sms_sim_setting"

    const-wide/16 v6, -0x5

    invoke-static {v3, v5, v6, v7}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMessageSimId:J

    iget-wide v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMessageSimId:J

    const-wide/16 v7, -0x1

    cmp-long v3, v5, v7

    if-nez v3, :cond_5

    invoke-direct {p0, v0, p1, p2}, Lcom/android/mms/ui/MultiForwardMessageActivity;->showSimSelectedDialog(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const-string v3, "Mms/MultiForwardMessage"

    const-string v5, "simSelection end"

    invoke-static {v3, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    goto/16 :goto_0

    :cond_4
    iput v9, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    goto :goto_1

    :cond_5
    iget-wide v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMessageSimId:J

    const-wide/16 v7, -0x5

    cmp-long v3, v5, v7

    if-nez v3, :cond_7

    iget v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    if-ne v3, v9, :cond_6

    invoke-direct {p0, v0, p1, p2}, Lcom/android/mms/ui/MultiForwardMessageActivity;->showSimSelectedDialog(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    iget v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    iput v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    invoke-direct {p0, v4}, Lcom/android/mms/ui/MultiForwardMessageActivity;->checkEcmMode(Z)V

    goto :goto_2

    :cond_7
    iget v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    if-eq v3, v9, :cond_8

    iget-wide v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMessageSimId:J

    iget v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mAssociatedSimId:I

    int-to-long v7, v3

    cmp-long v3, v5, v7

    if-nez v3, :cond_9

    :cond_8
    iget-wide v5, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMessageSimId:J

    long-to-int v3, v5

    iput v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I

    invoke-direct {p0, v4}, Lcom/android/mms/ui/MultiForwardMessageActivity;->checkEcmMode(Z)V

    goto :goto_2

    :cond_9
    invoke-direct {p0, v0, p1, p2}, Lcom/android/mms/ui/MultiForwardMessageActivity;->showSimSelectedDialog(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_a
    const/16 v4, 0xa5

    invoke-direct {p0, v8, v10, v4}, Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V

    goto/16 :goto_0
.end method

.method private splitNumbers(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const-string v0, ","

    const-string v1, ";"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v0, 0x6b

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->finishSelf(I)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->requestWindowFeature(I)Z

    const v0, 0x7f040045

    invoke-virtual {p0, v0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->initProgressDialog()V

    if-eqz p1, :cond_0

    const-string v0, "forwardingMsg"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mForwardingMsg:Z

    :cond_0
    sget-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    if-nez v0, :cond_1

    new-instance v0, Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-direct {v0}, Lcom/mediatek/CellConnService/CellConnMgr;-><init>()V

    sput-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    :cond_1
    sget-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {p0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/CellConnService/CellConnMgr;->register(Landroid/content/Context;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    sget-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->unregister()V

    const/4 v0, 0x0

    sput-object v0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x54 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-boolean v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mForwardingMsg:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mForwardingMsg:Z

    invoke-virtual {p0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "MultiForwardMessageParamMessageIds"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMsgIds:Ljava/lang/String;

    const-string v1, "MultiForwardMessageParamNumbers"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mNumbers:Ljava/lang/String;

    const-string v1, "Mms/MultiForwardMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MessageIds:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mMsgIds:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Numbers:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mNumbers:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiForwardMessageActivity;->checkSimInfoAndSendAsync()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiForwardMessageActivity;->mForwardingMsg:Z

    if-eqz v0, :cond_0

    const-string v0, "forwardingMsg"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method
