.class Lcom/android/mms/ui/ComposeMessageActivity$108;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity;->showIpMessageConvertToMmsOrSmsDialog(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4200(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0, v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18700(Lcom/android/mms/ui/ComposeMessageActivity;Z)Z

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/mms/ipmessage/message/IpMessage;->getType()I

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "Mms/ipmsg/compose"

    const-string v1, "convertIpMessageToMmsOrSms(): convert emoticon IP message to SMS."

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mms/ipmessage/message/IpTextMessage;

    invoke-virtual {v0}, Lcom/mediatek/mms/ipmessage/message/IpTextMessage;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$17100(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/mms/ipmessage/message/IpMessage;->getSimId()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mms/ipmessage/message/IpMessage;->getSimId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$15102(Lcom/android/mms/ui/ComposeMessageActivity;I)I

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0, v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18302(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/mediatek/mms/ipmessage/message/IpMessage;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0, v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4300(Lcom/android/mms/ui/ComposeMessageActivity;Z)V

    :cond_2
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4202(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/mediatek/mms/ipmessage/message/IpMessage;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0, v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18302(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/mediatek/mms/ipmessage/message/IpMessage;)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$108;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0, v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$18700(Lcom/android/mms/ui/ComposeMessageActivity;Z)Z

    goto :goto_0
.end method
