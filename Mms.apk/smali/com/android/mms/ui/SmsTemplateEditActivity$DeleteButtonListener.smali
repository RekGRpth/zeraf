.class Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;
.super Ljava/lang/Object;
.source "SmsTemplateEditActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/SmsTemplateEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteButtonListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/SmsTemplateEditActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/SmsTemplateEditActivity;Lcom/android/mms/ui/SmsTemplateEditActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/SmsTemplateEditActivity;
    .param p2    # Lcom/android/mms/ui/SmsTemplateEditActivity$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;-><init>(Lcom/android/mms/ui/SmsTemplateEditActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$100(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1600(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)I

    move-result v0

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1200(Lcom/android/mms/ui/SmsTemplateEditActivity;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$102(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const v2, 0x7f0b0204

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1300(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$DeleteButtonListener;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const v2, 0x7f0b0205

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$1300(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)V

    goto :goto_0
.end method
