.class public Lcom/android/mms/ui/MmsPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "MmsPreferenceActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final AUTO_RETRIEVAL:Ljava/lang/String; = "pref_key_mms_auto_retrieval"

.field public static final CREATION_MODE:Ljava/lang/String; = "pref_key_mms_creation_mode"

.field private static final CREATION_MODE_FREE:Ljava/lang/String; = "FREE"

.field private static final CREATION_MODE_RESTRICTED:Ljava/lang/String; = "RESTRICTED"

.field private static final CREATION_MODE_WARNING:Ljava/lang/String; = "WARNING"

.field private static final DEBUG:Z = false

.field public static final EXPIRY_TIME:Ljava/lang/String; = "pref_key_mms_expiry"

.field public static final GROUP_MMS_MODE:Ljava/lang/String; = "pref_key_mms_group_mms"

.field private static final LOCATION_PHONE:Ljava/lang/String; = "Phone"

.field private static final LOCATION_SIM:Ljava/lang/String; = "Sim"

.field private static final MENU_RESTORE_DEFAULTS:I = 0x1

.field public static final MMS_DELIVERY_REPORT_MODE:Ljava/lang/String; = "pref_key_mms_delivery_reports"

.field public static final MMS_ENABLE_TO_SEND_DELIVERY_REPORT:Ljava/lang/String; = "pref_key_mms_enable_to_send_delivery_reports"

.field public static final MMS_SETTINGS:Ljava/lang/String; = "pref_key_mms_settings"

.field public static final MMS_SIZE_LIMIT:Ljava/lang/String; = "pref_key_mms_size_limit"

.field public static final PRIORITY:Ljava/lang/String; = "pref_key_mms_priority"

.field private static final PRIORITY_HIGH:Ljava/lang/String; = "High"

.field private static final PRIORITY_LOW:Ljava/lang/String; = "Low"

.field private static final PRIORITY_NORMAL:Ljava/lang/String; = "Normal"

.field public static final READ_REPORT_AUTO_REPLY:Ljava/lang/String; = "pref_key_mms_auto_reply_read_reports"

.field public static final READ_REPORT_MODE:Ljava/lang/String; = "pref_key_mms_read_reports"

.field public static final RETRIEVAL_DURING_ROAMING:Ljava/lang/String; = "pref_key_mms_retrieval_during_roaming"

.field private static final SIZE_LIMIT_100:Ljava/lang/String; = "100"

.field private static final SIZE_LIMIT_200:Ljava/lang/String; = "200"

.field private static final SIZE_LIMIT_300:Ljava/lang/String; = "300"

.field private static final TAG:Ljava/lang/String; = "MmsPreferenceActivity"


# instance fields
.field private mCurrentSimCount:I

.field private mListSimInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMMSHandler:Landroid/os/Handler;

.field private mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

.field private mMmsAutoReplyReadReportMultiSim:Landroid/preference/Preference;

.field private mMmsAutoRetrieval:Landroid/preference/CheckBoxPreference;

.field private mMmsAutoRetrievalMultiSim:Landroid/preference/Preference;

.field private mMmsCreationMode:Landroid/preference/ListPreference;

.field private mMmsDeliveryReport:Landroid/preference/CheckBoxPreference;

.field private mMmsDeliveryReportMultiSim:Landroid/preference/Preference;

.field private mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

.field private mMmsEnableToSendDeliveryReportMultiSim:Landroid/preference/Preference;

.field private mMmsGroupMms:Landroid/preference/CheckBoxPreference;

.field private mMmsPriority:Landroid/preference/ListPreference;

.field private mMmsReadReport:Landroid/preference/CheckBoxPreference;

.field private mMmsReadReportMultiSim:Landroid/preference/Preference;

.field private mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

.field private mMmsRetrievalDuringRoamingMultiSim:Landroid/preference/Preference;

.field private mMmsSizeLimit:Landroid/preference/ListPreference;

.field private mNumberText:Landroid/widget/EditText;

.field private mNumberTextDialog:Landroid/app/AlertDialog;

.field private mSMSHandler:Landroid/os/Handler;

.field private mSimReceiver:Landroid/content/BroadcastReceiver;

.field private mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mSMSHandler:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMMSHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mCurrentSimCount:I

    new-instance v0, Lcom/android/mms/ui/MmsPreferenceActivity$1;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/MmsPreferenceActivity$1;-><init>(Lcom/android/mms/ui/MmsPreferenceActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mSimReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/MmsPreferenceActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MmsPreferenceActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->setMessagePreferences()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/mms/ui/MmsPreferenceActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MmsPreferenceActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->setListPrefSummary()V

    return-void
.end method

.method private changeSingleCardKeyToSimRelated()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-static {p0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mListSimInfo:Ljava/util/List;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    :cond_0
    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v4}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v4, "MmsPreferenceActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changeSingleCardKeyToSimRelated Got simId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "pref_key_mms_delivery_reports"

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReport:Landroid/preference/CheckBoxPreference;

    const-string v4, "pref_key_mms_read_reports"

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReport:Landroid/preference/CheckBoxPreference;

    const-string v4, "pref_key_mms_auto_reply_read_reports"

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

    const-string v4, "pref_key_mms_auto_retrieval"

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrieval:Landroid/preference/CheckBoxPreference;

    const-string v4, "pref_key_mms_retrieval_during_roaming"

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReport:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_key_mms_delivery_reports"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReport:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_key_mms_read_reports"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_key_mms_auto_reply_read_reports"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    :cond_3
    const-string v4, "pref_key_mms_group_mms"

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    const-string v5, "pref_key_mms_group_mms"

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    :cond_4
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrieval:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_key_mms_auto_retrieval"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_key_mms_auto_retrieval"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_key_mms_retrieval_during_roaming"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const-string v4, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/mms/MmsConfig;->getDeliveryReportAllowed()Z

    move-result v4

    if-nez v4, :cond_c

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const-string v4, "pref_key_mms_settings"

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    :goto_1
    const-string v4, "com.android.mms_preferences"

    invoke-virtual {p0, v4, v9}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReport:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReport:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_6
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_7
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReport:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReport:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_8
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_9
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrieval:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrieval:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrieval:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_a
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_b
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    goto/16 :goto_0

    :cond_c
    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static getIsGroupMmsEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v3, "pref_key_mms_group_mms"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {}, Lcom/android/mms/MmsConfig;->getGroupMmsEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private restoreDefaultPreferences()V
    .locals 8

    const/4 v7, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mListSimInfo:Ljava/util/List;

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mListSimInfo:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mListSimInfo:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v4}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pref_key_mms_delivery_reports"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pref_key_mms_read_reports"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pref_key_mms_auto_reply_read_reports"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pref_key_mms_auto_retrieval"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pref_key_mms_retrieval_during_roaming"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_0
    const-string v4, "pref_key_mms_creation_mode"

    const-string v5, "FREE"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v4, "pref_key_mms_size_limit"

    const-string v5, "300"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v4, "pref_key_mms_priority"

    const-string v5, "Normal"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v4, "pref_key_mms_group_mms"

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->setMessagePreferences()V

    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->setListPrefSummary()V

    return-void
.end method

.method private setListPrefSummary()V
    .locals 5

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "pref_key_mms_priority"

    const v3, 0x7f0b02b7

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsPriority:Landroid/preference/ListPreference;

    const v3, 0x7f060008

    const v4, 0x7f060009

    invoke-static {p0, v1, v3, v4}, Lcom/android/mms/ui/MessageUtils;->getVisualTextName(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v2, "pref_key_mms_creation_mode"

    const-string v3, "FREE"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsCreationMode:Landroid/preference/ListPreference;

    const v3, 0x7f060004

    const v4, 0x7f060005

    invoke-static {p0, v1, v3, v4}, Lcom/android/mms/ui/MessageUtils;->getVisualTextName(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v2, "pref_key_mms_size_limit"

    const-string v3, "300"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsSizeLimit:Landroid/preference/ListPreference;

    const v3, 0x7f060006

    const v4, 0x7f060007

    invoke-static {p0, v1, v3, v4}, Lcom/android/mms/ui/MessageUtils;->getVisualTextName(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setMessagePreferences()V
    .locals 9

    const v8, 0x7f050006

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v3, "MmsPreferenceActivity"

    const-string v4, "MTK_GEMINI_SUPPORT is true"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMCount(Landroid/content/Context;)I

    move-result v3

    iput v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mCurrentSimCount:I

    const-string v3, "MmsPreferenceActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mCurrentSimCount is :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mCurrentSimCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mCurrentSimCount:I

    if-nez v3, :cond_4

    invoke-virtual {p0, v8}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-static {}, Lcom/android/mms/MmsConfig;->getDeliveryReportAllowed()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    :goto_0
    const-string v3, "pref_key_mms_delivery_reports"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReport:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string v3, "pref_key_mms_read_reports"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReport:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string v3, "pref_key_mms_auto_reply_read_reports"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string v3, "pref_key_mms_auto_retrieval"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrieval:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrieval:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string v3, "pref_key_mms_retrieval_during_roaming"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoaming:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    :goto_1
    invoke-static {}, Lcom/android/mms/MmsConfig;->getGroupMmsEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Mms/Txn"

    const-string v4, "remove the group mms entry, it should be hidden."

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "pref_key_mms_settings"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string v3, "pref_key_mms_group_mms"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    const-string v3, "pref_key_mms_priority"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsPriority:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsPriority:Landroid/preference/ListPreference;

    invoke-virtual {v3, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "pref_key_mms_creation_mode"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsCreationMode:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsCreationMode:Landroid/preference/ListPreference;

    invoke-virtual {v3, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "pref_key_mms_size_limit"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsSizeLimit:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsSizeLimit:Landroid/preference/ListPreference;

    invoke-virtual {v3, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-static {}, Lcom/android/mms/MmsConfig;->getMmsEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "pref_key_mms_settings"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    const-string v3, "MmsPreferenceActivity"

    const-string v4, "MTK_GEMINI_SUPPORT is true"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mCurrentSimCount:I

    if-ne v3, v7, :cond_7

    const-string v3, "MmsPreferenceActivity"

    const-string v4, "single sim"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->changeSingleCardKeyToSimRelated()V

    :cond_2
    :goto_2
    return-void

    :cond_3
    const-string v3, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    const-string v3, "pref_key_mms_settings"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_4
    iget v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mCurrentSimCount:I

    if-ne v3, v7, :cond_6

    invoke-virtual {p0, v8}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-static {}, Lcom/android/mms/MmsConfig;->getDeliveryReportAllowed()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    goto/16 :goto_1

    :cond_5
    const-string v3, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    const-string v3, "pref_key_mms_settings"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReport:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    :cond_6
    const v3, 0x7f050005

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    goto/16 :goto_1

    :cond_7
    iget v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mCurrentSimCount:I

    if-le v3, v7, :cond_2

    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->setMultiCardPreference()V

    goto :goto_2
.end method

.method private setMultiCardPreference()V
    .locals 5

    const-string v2, "pref_key_mms_delivery_reports"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReportMultiSim:Landroid/preference/Preference;

    invoke-static {}, Lcom/android/mms/MmsConfig;->getDeliveryReportAllowed()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReportMultiSim:Landroid/preference/Preference;

    :goto_0
    const-string v2, "pref_key_mms_read_reports"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReportMultiSim:Landroid/preference/Preference;

    const-string v2, "pref_key_mms_auto_reply_read_reports"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReportMultiSim:Landroid/preference/Preference;

    const-string v2, "pref_key_mms_auto_retrieval"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrievalMultiSim:Landroid/preference/Preference;

    const-string v2, "pref_key_mms_retrieval_during_roaming"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoamingMultiSim:Landroid/preference/Preference;

    const-string v2, "com.android.mms_preferences"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "pref_key_mms_group_mms"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    iput-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    const-string v3, "pref_key_mms_group_mms"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsGroupMms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_1
    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReportMultiSim:Landroid/preference/Preference;

    const-string v3, "pref_key_mms_delivery_reports"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReportMultiSim:Landroid/preference/Preference;

    const-string v3, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReportMultiSim:Landroid/preference/Preference;

    const-string v3, "pref_key_mms_read_reports"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReportMultiSim:Landroid/preference/Preference;

    const-string v3, "pref_key_mms_auto_reply_read_reports"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrievalMultiSim:Landroid/preference/Preference;

    const-string v3, "pref_key_mms_auto_retrieval"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoamingMultiSim:Landroid/preference/Preference;

    const-string v3, "pref_key_mms_retrieval_during_roaming"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v2, "pref_key_mms_enable_to_send_delivery_reports"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReportMultiSim:Landroid/preference/Preference;

    const-string v2, "pref_key_mms_settings"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReportMultiSim:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0
.end method

.method private showToast(I)V
    .locals 4
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "MmsPreferenceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged: newConfig = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",this = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->clearScrapViewsIfNeeded()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "MmsPreferenceActivity"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01ae

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->setMessagePreferences()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    const v0, 0x7f0b0283

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    return v2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->restoreDefaultPreferences()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v7, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/mms/MmsConfig;->getSmsMultiSaveLocationEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {p0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMCount(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v7, :cond_0

    invoke-static {p0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v4}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSlot()I

    move-result v2

    :cond_0
    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    const-string v4, "pref_key_mms_priority"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsPriority:Landroid/preference/ListPreference;

    const v5, 0x7f060008

    const v6, 0x7f060009

    invoke-static {p0, v3, v5, v6}, Lcom/android/mms/ui/MessageUtils;->getVisualTextName(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return v7

    :cond_2
    const-string v4, "pref_key_mms_size_limit"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsSizeLimit:Landroid/preference/ListPreference;

    const v5, 0x7f060006

    const v6, 0x7f060007

    invoke-static {p0, v3, v5, v6}, Lcom/android/mms/ui/MessageUtils;->getVisualTextName(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/android/mms/MmsConfig;->setUserSetMmsSizeLimit(I)V

    goto :goto_0

    :cond_3
    const-string v4, "pref_key_mms_creation_mode"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsCreationMode:Landroid/preference/ListPreference;

    const v5, 0x7f060004

    const v6, 0x7f060005

    invoke-static {p0, v3, v5, v6}, Lcom/android/mms/ui/MessageUtils;->getVisualTextName(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsCreationMode:Landroid/preference/ListPreference;

    invoke-virtual {v4, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/mms/data/WorkingMessage;->updateCreationMode(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReportMultiSim:Landroid/preference/Preference;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReportMultiSim:Landroid/preference/Preference;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReportMultiSim:Landroid/preference/Preference;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReportMultiSim:Landroid/preference/Preference;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrievalMultiSim:Landroid/preference/Preference;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoamingMultiSim:Landroid/preference/Preference;

    if-ne p2, v1, :cond_2

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/android/mms/ui/MultiSimPreferenceActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "preference"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsDeliveryReportMultiSim:Landroid/preference/Preference;

    if-ne p2, v1, :cond_3

    const-string v1, "preferenceTitleId"

    const v2, 0x7f0b028f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v1

    return v1

    :cond_3
    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsEnableToSendDeliveryReportMultiSim:Landroid/preference/Preference;

    if-ne p2, v1, :cond_4

    const-string v1, "preferenceTitleId"

    const v2, 0x7f0b0002

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsReadReportMultiSim:Landroid/preference/Preference;

    if-ne p2, v1, :cond_5

    const-string v1, "preferenceTitleId"

    const v2, 0x7f0b0290

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoReplyReadReportMultiSim:Landroid/preference/Preference;

    if-ne p2, v1, :cond_6

    const-string v1, "preferenceTitleId"

    const v2, 0x7f0b0094

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsAutoRetrievalMultiSim:Landroid/preference/Preference;

    if-ne p2, v1, :cond_7

    const-string v1, "preferenceTitleId"

    const v2, 0x7f0b0299

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mMmsRetrievalDuringRoamingMultiSim:Landroid/preference/Preference;

    if-ne p2, v1, :cond_1

    const-string v1, "preferenceTitleId"

    const v2, 0x7f0b029b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->setListPrefSummary()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mSimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    iget-object v0, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mSimReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MmsPreferenceActivity;->mSimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
