.class public Lcom/android/mms/ui/CBMessageListItem;
.super Landroid/widget/LinearLayout;
.source "CBMessageListItem.java"


# static fields
.field public static final EXTRA_URLS:Ljava/lang/String; = "com.android.mms.ExtraUrls"

.field private static final STYLE_BOLD:Landroid/text/style/StyleSpan;

.field private static final TAG:Ljava/lang/String; = "CBMessageListItem"

.field public static final UPDATE_CHANNEL:I = 0xf

.field private static sDefaultContactImage:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mBodyTextView:Landroid/widget/TextView;

.field mColorSpan:Landroid/text/style/ForegroundColorSpan;

.field private mDateView:Landroid/widget/TextView;

.field private mDefaultCountryIso:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mIsLastItemInList:Z

.field private mIsTel:Z

.field private mItemContainer:Landroid/view/View;

.field private mMessageItem:Lcom/android/mms/ui/CBMessageItem;

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private mSelectedBox:Landroid/widget/CheckBox;

.field private mSimStatus:Landroid/widget/TextView;

.field private mSpan:Landroid/text/style/LineHeightSpan;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcom/android/mms/ui/CBMessageListItem;->STYLE_BOLD:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mPath:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mPaint:Landroid/graphics/Paint;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mIsTel:Z

    new-instance v0, Lcom/android/mms/ui/CBMessageListItem$7;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/CBMessageListItem$7;-><init>(Lcom/android/mms/ui/CBMessageListItem;)V

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    sget-object v0, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02009e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    :cond_0
    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/MmsApp;->getCurrentCountryIso()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mDefaultCountryIso:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mPath:Landroid/graphics/Path;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mIsTel:Z

    new-instance v1, Lcom/android/mms/ui/CBMessageListItem$7;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/CBMessageListItem$7;-><init>(Lcom/android/mms/ui/CBMessageListItem;)V

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    sget-object v1, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02009e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/android/mms/ui/CBMessageListItem;->sDefaultContactImage:Landroid/graphics/drawable/Drawable;

    :cond_0
    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/MmsApp;->getCurrentCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mDefaultCountryIso:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/mms/ui/CBMessageListItem;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mDefaultCountryIso:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/mms/ui/CBMessageListItem;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/CBMessageListItem;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private bindCommonMessage(Lcom/android/mms/ui/CBMessageItem;)V
    .locals 6
    .param p1    # Lcom/android/mms/ui/CBMessageItem;

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->getCachedFormattedMessage()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->getDate()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/android/mms/ui/CBMessageListItem;->formatMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/mms/ui/CBMessageItem;->setCachedFormattedMessage(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/android/mms/ui/CBMessageListItem;->formatTimestamp(Lcom/android/mms/ui/CBMessageItem;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mDateView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/CBMessageListItem;->formatSimStatus(Lcom/android/mms/ui/CBMessageItem;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mSimStatus:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mSimStatus:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mSimStatus:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private formatMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/CharSequence;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/util/regex/Pattern;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/android/mms/util/SmileyParser2;->getInstance()Lcom/android/mms/util/SmileyParser2;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/mms/util/SmileyParser2;->addSmileySpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private formatSimStatus(Lcom/android/mms/ui/CBMessageItem;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Lcom/android/mms/ui/CBMessageItem;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    iget v4, p1, Lcom/android/mms/ui/CBMessageItem;->mSimId:I

    invoke-static {v3, v4}, Lcom/android/mms/ui/MessageUtils;->getSimInfo(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0082

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    iget-object v3, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    const/4 v4, 0x0

    const/16 v5, 0x21

    invoke-virtual {v0, v3, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method private formatTimestamp(Lcom/android/mms/ui/CBMessageItem;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Lcom/android/mms/ui/CBMessageItem;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p2, " "

    :cond_0
    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mSpan:Landroid/text/style/LineHeightSpan;

    const/4 v2, 0x1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Lcom/android/mms/ui/CBMessageListItem;->mColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/android/mms/ui/CBMessageItem;ZZ)V
    .locals 3
    .param p1    # Lcom/android/mms/ui/CBMessageItem;
    .param p2    # Z
    .param p3    # Z

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageItem:Lcom/android/mms/ui/CBMessageItem;

    iput-boolean p2, p0, Lcom/android/mms/ui/CBMessageListItem;->mIsLastItemInList:Z

    invoke-virtual {p0, v2}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/android/mms/ui/CBMessageItem;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mItemContainer:Landroid/view/View;

    new-instance v1, Lcom/android/mms/ui/CBMessageListItem$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/CBMessageListItem$1;-><init>(Lcom/android/mms/ui/CBMessageListItem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/CBMessageListItem;->bindCommonMessage(Lcom/android/mms/ui/CBMessageItem;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public getMessageItem()Lcom/android/mms/ui/CBMessageItem;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageItem:Lcom/android/mms/ui/CBMessageItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    const-string v0, "MmsLog"

    const-string v1, "CBMessageListItem.onFinishInflate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0f001e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    const v0, 0x7f0f001f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mDateView:Landroid/widget/TextView;

    const v0, 0x7f0f001d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mItemContainer:Landroid/view/View;

    const v0, 0x7f0f0023

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSimStatus:Landroid/widget/TextView;

    const v0, 0x7f0f001c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    return-void
.end method

.method public onMessageListItemClick()V
    .locals 14

    iget-object v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    if-eqz v12, :cond_2

    iget-object v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v12

    if-nez v12, :cond_2

    iget-object v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-nez v12, :cond_1

    const/4 v12, 0x1

    invoke-virtual {p0, v12}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    :goto_0
    iget-object v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mHandler:Landroid/os/Handler;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mHandler:Landroid/os/Handler;

    const/4 v13, 0x5

    invoke-static {v12, v13}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v6

    iget-object v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mMessageItem:Lcom/android/mms/ui/CBMessageItem;

    invoke-virtual {v12}, Lcom/android/mms/ui/CBMessageItem;->getMessageId()J

    move-result-wide v12

    long-to-int v12, v12

    iput v12, v6, Landroid/os/Message;->arg1:I

    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v12, 0x0

    invoke-virtual {p0, v12}, Lcom/android/mms/ui/CBMessageListItem;->setSelectedBackGroud(Z)V

    goto :goto_0

    :cond_2
    iget-object v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mBodyTextView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getUrls()[Landroid/text/style/URLSpan;

    move-result-object v7

    invoke-static {v7}, Lcom/android/mms/ui/MessageUtils;->extractUris([Landroid/text/style/URLSpan;)Ljava/util/ArrayList;

    move-result-object v11

    const-string v8, "tel:"

    const-string v10, ""

    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v3, v12, :cond_4

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v12, "tel:"

    invoke-virtual {v10, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mIsTel:Z

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "smsto:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "tel:"

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v10, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    array-length v12, v7

    if-eqz v12, :cond_0

    array-length v12, v7

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    iget-boolean v12, p0, Lcom/android/mms/ui/CBMessageListItem;->mIsTel:Z

    if-nez v12, :cond_6

    const/4 v12, 0x0

    aget-object v12, v7, v12

    invoke-virtual {v12}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/android/mms/MmsConfig;->isShowUrlDialog()Z

    move-result v12

    if-eqz v12, :cond_5

    const-string v12, "mailto:"

    invoke-virtual {v5, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v12, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-direct {v1, v12}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v12, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$string;->url_dialog_choice_title:I

    invoke-virtual {v1, v12}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v12, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$string;->url_dialog_choice_message:I

    invoke-virtual {v1, v12}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const/high16 v12, 0x1040000

    new-instance v13, Lcom/android/mms/ui/CBMessageListItem$2;

    invoke-direct {v13, p0}, Lcom/android/mms/ui/CBMessageListItem$2;-><init>(Lcom/android/mms/ui/CBMessageListItem;)V

    invoke-virtual {v1, v12, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v12, 0x104000a

    new-instance v13, Lcom/android/mms/ui/CBMessageListItem$3;

    invoke-direct {v13, p0, v5}, Lcom/android/mms/ui/CBMessageListItem$3;-><init>(Lcom/android/mms/ui/CBMessageListItem;Ljava/lang/String;)V

    invoke-virtual {v1, v12, v13}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    :cond_5
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    new-instance v4, Landroid/content/Intent;

    const-string v12, "android.intent.action.VIEW"

    invoke-direct {v4, v12, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v12, "com.android.browser.application_id"

    iget-object v13, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v12, 0x80000

    invoke-virtual {v4, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v12, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_6
    new-instance v0, Lcom/android/mms/ui/CBMessageListItem$4;

    iget-object v12, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const v13, 0x1090011

    invoke-direct {v0, p0, v12, v13, v11}, Lcom/android/mms/ui/CBMessageListItem$4;-><init>(Lcom/android/mms/ui/CBMessageListItem;Landroid/content/Context;ILjava/util/List;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v12, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-direct {v1, v12}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/android/mms/ui/CBMessageListItem$5;

    invoke-direct {v2, p0, v11}, Lcom/android/mms/ui/CBMessageListItem$5;-><init>(Lcom/android/mms/ui/CBMessageListItem;Ljava/util/ArrayList;)V

    const v12, 0x7f0b02f9

    invoke-virtual {v1, v12}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v12, 0x1040000

    new-instance v13, Lcom/android/mms/ui/CBMessageListItem$6;

    invoke-direct {v13, p0}, Lcom/android/mms/ui/CBMessageListItem$6;-><init>(Lcom/android/mms/ui/CBMessageListItem;)V

    invoke-virtual {v1, v12, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1
.end method

.method public setMsgListItemHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/mms/ui/CBMessageListItem;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public setSelectedBackGroud(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f02012a

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/CBMessageListItem;->mSelectedBox:Landroid/widget/CheckBox;

    const v1, 0x7f02012d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public unbind()V
    .locals 0

    return-void
.end method
