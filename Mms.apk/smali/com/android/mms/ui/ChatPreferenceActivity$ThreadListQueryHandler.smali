.class final Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;
.super Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;
.source "ChatPreferenceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ChatPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ThreadListQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ChatPreferenceActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/ChatPreferenceActivity;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-direct {p0, p2}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 8
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const-wide/16 v6, -0x2

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-static {v3}, Lcom/android/mms/data/Contact;->init(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-static {v3}, Lcom/android/mms/data/Conversation;->init(Landroid/content/Context;)V

    :try_start_0
    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->isTestIccCard()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "ChatPreferenceActivity"

    const-string v4, "All threads has been deleted, send notification.."

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedSmsManager;->setSmsMemoryStatus(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    const/4 v4, 0x0

    invoke-static {v3, v6, v7, v4}, Lcom/android/mms/transaction/MessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;JZ)V

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    iget-object v4, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ChatPreferenceActivity;->access$300(Lcom/android/mms/ui/ChatPreferenceActivity;)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/android/mms/transaction/MessagingNotification;->updateSendFailedNotificationForThread(Landroid/content/Context;J)V

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-static {v3}, Lcom/android/mms/transaction/MessagingNotification;->updateDownloadFailedNotification(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-static {v3, v6, v7}, Lcom/android/mms/transaction/WapPushMessagingNotification;->nonBlockingUpdateNewMessageIndicator(Landroid/content/Context;J)V

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-static {v3}, Lcom/android/mms/transaction/CBMessagingNotification;->updateNewMessageIndicator(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/mms/ui/ConversationList$BaseProgressQueryHandler;->dismissProgressDialog()V

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    const-class v4, Lcom/android/mms/ui/ConversationList;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x4000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-virtual {v3, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v3, "ChatPreferenceActivity"

    const-string v4, "Telephony service is not available!"

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v3, "ChatPreferenceActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x709
        :pswitch_0
    .end packed-switch
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    const-string v2, "ChatPreferenceActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onQueryComplete called with unknown token "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-static {v2, p3}, Lcom/android/mms/ui/ChatPreferenceActivity;->access$1400(Lcom/android/mms/ui/ChatPreferenceActivity;Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_1
    const-string v2, "ChatPreferenceActivity"

    const-string v3, "onQueryComplete HAVE_LOCKED_MESSAGES_TOKEN"

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p2

    check-cast v1, Ljava/util/Collection;

    new-instance v0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget-object v2, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-static {v2}, Lcom/android/mms/ui/ChatPreferenceActivity;->access$1500(Lcom/android/mms/ui/ChatPreferenceActivity;)Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;-><init>(Ljava/util/Collection;Landroid/content/AsyncQueryHandler;Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    if-eqz p3, :cond_2

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    iget-object v4, p0, Lcom/android/mms/ui/ChatPreferenceActivity$ThreadListQueryHandler;->this$0:Lcom/android/mms/ui/ChatPreferenceActivity;

    invoke-virtual {v3, v0, v1, v2, v4}, Lcom/android/mms/ui/ChatPreferenceActivity;->confirmDeleteThreadDialog(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;Ljava/util/Collection;ZLandroid/content/Context;)V

    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
