.class public Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;
.super Landroid/widget/BaseAdapter;
.source "EmoticonPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/EmoticonPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "EmoticonAdapter"
.end annotation


# instance fields
.field protected mIconArr:[I

.field final synthetic this$0:Lcom/android/mms/ui/EmoticonPanel;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/EmoticonPanel;[I)V
    .locals 0
    .param p2    # [I

    iput-object p1, p0, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;->this$0:Lcom/android/mms/ui/EmoticonPanel;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;->mIconArr:[I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;->mIconArr:[I

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;->this$0:Lcom/android/mms/ui/EmoticonPanel;

    invoke-static {v1}, Lcom/android/mms/ui/EmoticonPanel;->access$500(Lcom/android/mms/ui/EmoticonPanel;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040023

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    const v1, 0x7f0f007b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/mms/ui/EmoticonPanel$EmoticonAdapter;->mIconArr:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    goto :goto_0
.end method
