.class Lcom/android/mms/ui/ComposeMessageActivity$13$1;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ComposeMessageActivity$13;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity$13;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$13;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4500(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/RecipientsEditor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mms/ui/RecipientsEditor;->getNumbers()Ljava/util/List;

    move-result-object v2

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$13;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/android/mms/data/WorkingMessage;->setWorkingRecipients(Ljava/util/List;)V

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v3, :cond_0

    move v1, v3

    :goto_0
    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$13;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MmsPreferenceActivity;->getIsGroupMmsEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v1, :cond_1

    move v0, v3

    :goto_1
    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$13;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4, v0}, Lcom/android/mms/ui/MessageListAdapter;->setIsGroupConversation(Z)V

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$13;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    invoke-virtual {v4, v1, v3}, Lcom/android/mms/data/WorkingMessage;->setHasMultipleRecipients(ZZ)V

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    iget-object v4, v4, Lcom/android/mms/ui/ComposeMessageActivity$13;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4}, Lcom/android/mms/ui/ComposeMessageActivity;->access$1300(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v4

    iget-object v5, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    iget-object v5, v5, Lcom/android/mms/ui/ComposeMessageActivity$13;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4500(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/RecipientsEditor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mms/ui/RecipientsEditor;->containsEmail()Z

    move-result v5

    invoke-virtual {v4, v5, v3}, Lcom/android/mms/data/WorkingMessage;->setHasEmail(ZZ)V

    iget-object v3, p0, Lcom/android/mms/ui/ComposeMessageActivity$13$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$13;

    iget-object v3, v3, Lcom/android/mms/ui/ComposeMessageActivity$13;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/ComposeMessageActivity;->access$5100(Lcom/android/mms/ui/ComposeMessageActivity;)V

    return-void

    :cond_0
    move v1, v4

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1
.end method
