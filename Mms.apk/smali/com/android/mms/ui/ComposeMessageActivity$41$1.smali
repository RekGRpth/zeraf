.class Lcom/android/mms/ui/ComposeMessageActivity$41$1;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/ComposeMessageActivity$41;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

.field final synthetic val$allList:Lcom/android/mms/data/ContactList;

.field final synthetic val$list:Lcom/android/mms/data/ContactList;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity$41;Lcom/android/mms/data/ContactList;Lcom/android/mms/data/ContactList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

    iput-object p2, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->val$allList:Lcom/android/mms/data/ContactList;

    iput-object p3, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->val$list:Lcom/android/mms/data/ContactList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$41;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$2600(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/data/Conversation;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->val$allList:Lcom/android/mms/data/ContactList;

    invoke-virtual {v0, v1}, Lcom/android/mms/data/Conversation;->setRecipients(Lcom/android/mms/data/ContactList;)V

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->val$list:Lcom/android/mms/data/ContactList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$41;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$9800(Lcom/android/mms/ui/ComposeMessageActivity;)V

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$41;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4500(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/RecipientsEditor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->val$list:Lcom/android/mms/data/ContactList;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/RecipientsEditor;->populate(Lcom/android/mms/data/ContactList;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$41;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v1, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->val$allList:Lcom/android/mms/data/ContactList;

    invoke-static {v0, v1}, Lcom/android/mms/ui/ComposeMessageActivity;->access$5300(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/android/mms/data/ContactList;)V

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$41;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4500(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/RecipientsEditor;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$41;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4400(Lcom/android/mms/ui/ComposeMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/ComposeMessageActivity$41$1;->this$1:Lcom/android/mms/ui/ComposeMessageActivity$41;

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$41;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$4500(Lcom/android/mms/ui/ComposeMessageActivity;)Lcom/android/mms/ui/RecipientsEditor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void
.end method
