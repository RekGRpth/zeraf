.class public Lcom/android/mms/ui/ConversationList$DeleteThreadListener;
.super Ljava/lang/Object;
.source "ConversationList.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ConversationList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteThreadListener"
.end annotation


# static fields
.field public static sDeleteNumber:I


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDeleteLockedMessages:Z

.field private final mHandler:Landroid/content/AsyncQueryHandler;

.field private mMaxMmsId:I

.field private mMaxSmsId:I

.field private mMode:Landroid/view/ActionMode;

.field private final mThreadIds:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;Landroid/content/AsyncQueryHandler;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/AsyncQueryHandler;
    .param p3    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/content/AsyncQueryHandler;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mThreadIds:Ljava/util/Collection;

    iput-object p2, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mHandler:Landroid/content/AsyncQueryHandler;

    iput-object p3, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mContext:Landroid/content/Context;

    const/4 v0, 0x0

    sput v0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->sDeleteNumber:I

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;Landroid/content/AsyncQueryHandler;Landroid/content/Context;Landroid/view/ActionMode;)V
    .locals 1
    .param p2    # Landroid/content/AsyncQueryHandler;
    .param p3    # Landroid/content/Context;
    .param p4    # Landroid/view/ActionMode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/content/AsyncQueryHandler;",
            "Landroid/content/Context;",
            "Landroid/view/ActionMode;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mThreadIds:Ljava/util/Collection;

    iput-object p2, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mHandler:Landroid/content/AsyncQueryHandler;

    iput-object p3, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mMode:Landroid/view/ActionMode;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    sput v0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->sDeleteNumber:I

    :cond_0
    return-void
.end method

.method static synthetic access$1300(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Ljava/util/Collection;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mThreadIds:Ljava/util/Collection;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget-boolean v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mDeleteLockedMessages:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mMaxSmsId:I

    return v0
.end method

.method static synthetic access$1800(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)Landroid/content/AsyncQueryHandler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mHandler:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/ConversationList$DeleteThreadListener;

    iget v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mMaxMmsId:I

    return v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mThreadIds:Ljava/util/Collection;

    const/16 v2, 0x81

    new-instance v3, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;

    invoke-direct {v3, p0}, Lcom/android/mms/ui/ConversationList$DeleteThreadListener$1;-><init>(Lcom/android/mms/ui/ConversationList$DeleteThreadListener;)V

    invoke-static {v0, v1, v2, v3}, Lcom/android/mms/ui/MessageUtils;->handleReadReport(Landroid/content/Context;Ljava/util/Collection;ILjava/lang/Runnable;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public setDeleteLockedMessage(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mDeleteLockedMessages:Z

    return-void
.end method

.method public setMaxMsgId(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mMaxMmsId:I

    iput p2, p0, Lcom/android/mms/ui/ConversationList$DeleteThreadListener;->mMaxSmsId:I

    return-void
.end method
