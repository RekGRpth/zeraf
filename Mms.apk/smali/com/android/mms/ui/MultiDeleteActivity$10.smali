.class Lcom/android/mms/ui/MultiDeleteActivity$10;
.super Ljava/lang/Object;
.source "MultiDeleteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MultiDeleteActivity;->forwardOneMms(Lcom/android/mms/ui/MessageItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MultiDeleteActivity;

.field final synthetic val$msgItem:Lcom/android/mms/ui/MessageItem;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/ui/MessageItem;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iput-object p2, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v2, v3}, Lcom/android/mms/ui/MultiDeleteActivity;->access$4600(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/android/mms/MmsConfig;->isNeedExitComposerAfterForward()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "exit_on_sent"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const-string v2, "forwarded_message"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v2}, Lcom/android/mms/ui/MultiDeleteActivity;->access$4300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->access$4500(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    const-string v2, "thread_id"

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiDeleteActivity;->access$4300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->access$4500(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;)J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_1
    const-string v2, "msg_uri"

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiDeleteActivity;->access$4300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;->access$4400(Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    const v3, 0x7f0b0259

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-object v2, v2, Lcom/android/mms/ui/MessageItem;->mSubject:Ljava/lang/String;

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->val$msgItem:Lcom/android/mms/ui/MessageItem;

    iget-object v3, v3, Lcom/android/mms/ui/MessageItem;->mSubject:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    const-string v2, "subject"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    const-string v3, "com.android.mms.ui.ForwardMessageActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity$10;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
