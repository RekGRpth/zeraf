.class Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;
.super Ljava/lang/Object;
.source "MmsPlayerActivityAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MmsPlayerActivityAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "mediaClick"
.end annotation


# instance fields
.field private mTitle:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/android/mms/ui/MmsPlayerActivityAdapter;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/MmsPlayerActivityAdapter;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->this$0:Lcom/android/mms/ui/MmsPlayerActivityAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->mUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->mType:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->mTitle:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v3, "SingleItemOnly"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "CanShare"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "title"

    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->mType:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    iget-object v3, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->this$0:Lcom/android/mms/ui/MmsPlayerActivityAdapter;

    invoke-static {v3}, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->access$000(Lcom/android/mms/ui/MmsPlayerActivityAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/MmsPlayerActivityAdapter$mediaClick;->this$0:Lcom/android/mms/ui/MmsPlayerActivityAdapter;

    invoke-static {v3}, Lcom/android/mms/ui/MmsPlayerActivityAdapter;->access$000(Lcom/android/mms/ui/MmsPlayerActivityAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
