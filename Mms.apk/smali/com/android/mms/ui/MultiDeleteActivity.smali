.class public Lcom/android/mms/ui/MultiDeleteActivity;
.super Landroid/app/ListActivity;
.source "MultiDeleteActivity.java"

# interfaces
.implements Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;
.implements Lcom/mediatek/mms/ipmessage/INotificationsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;,
        Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;,
        Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;,
        Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;
    }
.end annotation


# static fields
.field private static final DELETE_MESSAGE_TOKEN:I = 0x25e4

.field public static final FORWARD_MESSAGE:Ljava/lang/String; = "forwarded_message"

.field private static final FOR_MULTIDELETE:Ljava/lang/String; = "ForMultiDelete"

.field public static final HIDE_DOWNLOAD_PROGRESS_BAR:I = 0x3e9

.field public static final IPMSG_IDS:Ljava/lang/String; = "forward_ipmsg_ids"

.field public static final IPMSG_TAG:Ljava/lang/String; = "Mms/ipmsg/MultiDeleteActivity"

.field private static final MESSAGE_LIST_QUERY_TOKEN:I = 0x2537

.field public static final MESSAGE_STATUS_IMPORTANT:I = 0x1

.field public static final MESSAGE_STATUS_NOT_IMPORTANT:I = 0x0

.field private static final REQUEST_CODE_FORWARD:I = 0x3e8

.field public static final SHOW_DOWNLOAD_PROGRESS_BAR:I = 0x3e8

.field public static final TAG:Ljava/lang/String; = "Mms/MultiDeleteActivity"

.field public static final UPDATE_SELECTED_COUNT:I = 0x3ea


# instance fields
.field private mAsyncDialog:Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

.field private mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

.field private mCancelSelect:Landroid/view/MenuItem;

.field private mChatSelect:Landroid/widget/Button;

.field private mConversation:Lcom/android/mms/data/Conversation;

.field private final mDataSetChangedListener:Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;

.field private mDelete:Landroid/view/MenuItem;

.field private mDeleteRunningCount:I

.field private mDownloadDialog:Landroid/app/ProgressDialog;

.field private mDownloadedIpMessageStepCounter:I

.field private mDownloadedIpMessageStepCounterFail:I

.field private mDownloadedIpMessageStepCounterSuccess:I

.field private mForwardMsgIds:Ljava/lang/String;

.field private mImportantCount:I

.field private mIsLockOrUnlockFinish:Z

.field private mIsSelectedAll:Z

.field private final mMessageListItemHandler:Landroid/os/Handler;

.field private mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

.field private mMmsNotificationCount:I

.field private mMmsNotificationHasRun:Z

.field public mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

.field private mMsgListView:Landroid/widget/ListView;

.field private mOP01Plugin:Z

.field private mPossiblePendingNotification:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSelectActionMode:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

.field private mSelectAll:Landroid/view/MenuItem;

.field private mSelectMode:Landroid/view/ActionMode;

.field private mSelectedIpAttachMessageItem:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/mms/ui/MessageItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedIpMessageIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedLockedMsgIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mTempActionMode:Landroid/view/ActionMode;

.field private mTempActionModeMenu:Landroid/view/MenuItem;

.field private mThreadCountManager:Lcom/android/mms/util/ThreadCountManager;

.field private mThreadId:J

.field private mUnDownloadedIpMessageCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    invoke-static {}, Lcom/android/mms/util/ThreadCountManager;->getInstance()Lcom/android/mms/util/ThreadCountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadCountManager:Lcom/android/mms/util/ThreadCountManager;

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedIpMessageIds:Ljava/util/HashSet;

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    iput-boolean v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationHasRun:Z

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedIpAttachMessageItem:Ljava/util/HashSet;

    iput-boolean v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mOP01Plugin:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsLockOrUnlockFinish:Z

    new-instance v0, Lcom/android/mms/ui/MultiDeleteActivity$4;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/MultiDeleteActivity$4;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMessageListItemHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/mms/ui/MultiDeleteActivity$5;

    invoke-direct {v0, p0}, Lcom/android/mms/ui/MultiDeleteActivity$5;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDataSetChangedListener:Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    return-void
.end method

.method static synthetic access$100(Lcom/android/mms/ui/MultiDeleteActivity;)J
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-wide v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadId:J

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    return v0
.end method

.method static synthetic access$1008(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    return v0
.end method

.method static synthetic access$1010(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    return v0
.end method

.method static synthetic access$1100(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    return v0
.end method

.method static synthetic access$1108(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    return v0
.end method

.method static synthetic access$1110(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    return v0
.end method

.method static synthetic access$1200(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    return v0
.end method

.method static synthetic access$1202(Lcom/android/mms/ui/MultiDeleteActivity;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    return p1
.end method

.method static synthetic access$1208(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    return v0
.end method

.method static synthetic access$1210(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    return v0
.end method

.method static synthetic access$1300(Lcom/android/mms/ui/MultiDeleteActivity;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedIpAttachMessageItem:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/mms/ui/MultiDeleteActivity;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->updateSelectCount()V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/android/mms/ui/MultiDeleteActivity;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounter:I

    return p1
.end method

.method static synthetic access$1902(Lcom/android/mms/ui/MultiDeleteActivity;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounterSuccess:I

    return p1
.end method

.method static synthetic access$200(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/data/Conversation;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounterFail:I

    return v0
.end method

.method static synthetic access$2002(Lcom/android/mms/ui/MultiDeleteActivity;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounterFail:I

    return p1
.end method

.method static synthetic access$202(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/data/Conversation;)Lcom/android/mms/data/Conversation;
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Lcom/android/mms/data/Conversation;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->getSelectedCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mTempActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mTempActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mTempActionModeMenu:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Landroid/view/MenuItem;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mTempActionModeMenu:Landroid/view/MenuItem;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectActionMode:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/mms/ui/MultiDeleteActivity;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsLockOrUnlockFinish:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->startMsgListQuery()V

    return-void
.end method

.method static synthetic access$2700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->updateSendFailedNotification()V

    return-void
.end method

.method static synthetic access$2900(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/util/ThreadCountManager;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadCountManager:Lcom/android/mms/util/ThreadCountManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mPossiblePendingNotification:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mChatSelect:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mChatSelect:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->setSelectAll()V

    return-void
.end method

.method static synthetic access$3300(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->setDeselectAll()V

    return-void
.end method

.method static synthetic access$3400(Lcom/android/mms/ui/MultiDeleteActivity;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationHasRun:Z

    return v0
.end method

.method static synthetic access$3402(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationHasRun:Z

    return p1
.end method

.method static synthetic access$3500(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMessageListItemHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/mms/ui/MultiDeleteActivity;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->selectedMsgHasLocked()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3700(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;ZZLjava/lang/Long;Landroid/content/Context;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Ljava/lang/Long;
    .param p5    # Landroid/content/Context;

    invoke-direct/range {p0 .. p5}, Lcom/android/mms/ui/MultiDeleteActivity;->confirmMultiDeleteMsgDialog(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;ZZLjava/lang/Long;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/android/mms/ui/MultiDeleteActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mForwardMsgIds:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mForwardMsgIds:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/android/mms/ui/MultiDeleteActivity;[J[JZ)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # [J
    .param p2    # [J
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mms/ui/MultiDeleteActivity;->markAsImportant([J[JZ)V

    return-void
.end method

.method static synthetic access$4100(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->dismissProgressIndication()V

    return-void
.end method

.method static synthetic access$4200(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/util/ArrayList;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity;->forwardMessage(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mAsyncDialog:Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4700(Lcom/android/mms/ui/MultiDeleteActivity;Z)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity;->markCheckedState(Z)V

    return-void
.end method

.method static synthetic access$4800(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity;->showReachLimitDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/util/Map$Entry;)Z
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # Ljava/util/Map$Entry;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity;->isMsgLocked(Ljava/util/Map$Entry;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/android/mms/ui/MultiDeleteActivity;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedIpMessageIds:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 1
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    return v0
.end method

.method static synthetic access$902(Lcom/android/mms/ui/MultiDeleteActivity;I)I
    .locals 0
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    return p1
.end method

.method static synthetic access$908(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    return v0
.end method

.method static synthetic access$910(Lcom/android/mms/ui/MultiDeleteActivity;)I
    .locals 2
    .param p0    # Lcom/android/mms/ui/MultiDeleteActivity;

    iget v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDeleteRunningCount:I

    return v0
.end method

.method private checkPendingNotification()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mPossiblePendingNotification:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v0}, Lcom/android/mms/data/Conversation;->markAsRead()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mPossiblePendingNotification:Z

    :cond_0
    return-void
.end method

.method private confirmMultiDeleteMsgDialog(Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;ZZLjava/lang/Long;Landroid/content/Context;)V
    .locals 14
    .param p1    # Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Ljava/lang/Long;
    .param p5    # Landroid/content/Context;

    const v1, 0x7f04001b

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    const v1, 0x7f0f007c

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    if-nez p3, :cond_5

    const v1, 0x7f0b0073

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v1, 0x7f0f007d

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    if-eqz p2, :cond_7

    invoke-static/range {p5 .. p5}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v1

    if-lez v1, :cond_0

    invoke-static/range {p5 .. p5}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v1

    const/16 v2, 0x75

    invoke-virtual {v1, v2}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {v8}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->setDeleteLockedMessage(Z)V

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$2;

    invoke-direct {v1, p0, p1, v8}, Lcom/android/mms/ui/MultiDeleteActivity$2;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;Landroid/widget/CheckBox;)V

    invoke-virtual {v8, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    const/4 v10, 0x0

    const/4 v13, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "max(_id)"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_2

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "confirmMultiDeleteMsgDialog max SMS id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    :cond_2
    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "max(_id)"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_4

    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "confirmMultiDeleteMsgDialog max MMS id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    :cond_4
    invoke-virtual {p1, v11, v13}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->setMaxMsgId(II)V

    new-instance v7, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p5

    invoke-direct {v7, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b029d

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b02a6

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b027d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_5
    move/from16 v0, p3

    invoke-virtual {p1, v0}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->setDeleteThread(Z)V

    invoke-virtual/range {p1 .. p2}, Lcom/android/mms/ui/MultiDeleteActivity$MultiDeleteMsgListener;->setHasLockedMsg(Z)V

    if-nez p4, :cond_6

    const v1, 0x7f0b02a0

    invoke-virtual {v12, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0005

    const/4 v3, 0x1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    const/16 v1, 0x8

    invoke-virtual {v8, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    throw v1

    :catchall_1
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    throw v1
.end method

.method private createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private dismissProgressIndication()V
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private forwardMessage(Ljava/util/ArrayList;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v16

    if-gtz v16, :cond_0

    new-instance v16, Lcom/android/mms/ui/MultiDeleteActivity$11;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/mms/ui/MultiDeleteActivity$11;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p1 .. p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {}, Lcom/android/mms/MmsConfig;->getMaxTextLimit()I

    move-result v8

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v15, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x0

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v10

    const-string v16, "pref_key_forward_with_sender"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v10, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    const-string v16, "Mms/MultiDeleteActivity"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "forwardMessage(): SMS Forward With Sender ?= "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v16

    move/from16 v0, v16

    if-ge v4, v0, :cond_3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Long;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v16, 0x0

    cmp-long v16, v6, v16

    if-lez v16, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v6, v7}, Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;->getBody(J)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mOP01Plugin:Z

    move/from16 v16, v0

    if-eqz v16, :cond_1

    if-eqz v13, :cond_1

    const-string v16, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const v16, 0x7f0b00af

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v6, v7}, Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;->getAddress(J)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-static/range {v16 .. v17}, Lcom/android/mms/data/Contact;->get(Ljava/lang/String;Z)Lcom/android/mms/data/Contact;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mms/data/Contact;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v2}, Lcom/android/mms/data/Contact;->getNumber()Ljava/lang/String;

    move-result-object v17

    const-string v18, ""

    invoke-static/range {v16 .. v18}, Lcom/android/mms/data/Contact;->formatNameAndNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v16, "Mms/MultiDeleteActivity"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "forwardMessage(): Contact\'s name and number="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    if-ge v4, v0, :cond_2

    const-string v16, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v16

    move/from16 v0, v16

    if-le v0, v8, :cond_4

    const/4 v11, 0x1

    if-eqz v15, :cond_3

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_3

    const/16 v16, 0x0

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    :cond_3
    if-eqz v11, :cond_5

    move-object v3, v15

    new-instance v16, Lcom/android/mms/ui/MultiDeleteActivity$12;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/android/mms/ui/MultiDeleteActivity$12;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, "Mms/MultiDeleteActivity"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "forwardMessage  strbuf.length()="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "  tempbuf.length() = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/mms/ui/MultiDeleteActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    invoke-static {}, Lcom/android/mms/MmsConfig;->isNeedExitComposerAfterForward()Z

    move-result v16

    if-eqz v16, :cond_6

    const-string v16, "exit_on_sent"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_6
    const-string v16, "forwarded_message"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v16, "sms_body"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v16, "com.android.mms.ui.ForwardMessageActivity"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private forwardOneMms(Lcom/android/mms/ui/MessageItem;)V
    .locals 4
    .param p1    # Lcom/android/mms/ui/MessageItem;

    invoke-virtual {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->getAsyncDialog()Lcom/android/mms/ui/AsyncDialog;

    move-result-object v0

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$9;

    invoke-direct {v1, p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity$9;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/ui/MessageItem;)V

    new-instance v2, Lcom/android/mms/ui/MultiDeleteActivity$10;

    invoke-direct {v2, p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity$10;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/ui/MessageItem;)V

    const v3, 0x7f0b010e

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/mms/ui/AsyncDialog;->runAsync(Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    return-void
.end method

.method private getSelectedCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v0

    return v0
.end method

.method private initActivityState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    const-string v2, "is_all_selected"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v3}, Lcom/android/mms/ui/MessageListAdapter;->setItemsValue(Z[J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "select_list"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v2, v4, v1}, Lcom/android/mms/ui/MessageListAdapter;->setItemsValue(Z[J)V

    goto :goto_0
.end method

.method private initMessageList()V
    .locals 10

    const/4 v2, 0x0

    const/4 v9, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "highlight"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    move-object v5, v2

    :goto_1
    new-instance v0, Lcom/android/mms/ui/MessageListAdapter;

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/mms/ui/MessageListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ListView;ZLjava/util/regex/Pattern;Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iput-boolean v4, v0, Lcom/android/mms/ui/MessageListAdapter;->mIsDeleteMode:Z

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMessageListItemHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/MessageListAdapter;->setMsgListItemHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDataSetChangedListener:Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/MessageListAdapter;->setOnDataSetChangedListener(Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;)V

    invoke-static {p0}, Lcom/android/mms/ui/MmsPreferenceActivity;->getIsGroupMmsEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v0}, Lcom/android/mms/data/Conversation;->getRecipients()Lcom/android/mms/data/ContactList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v4, :cond_2

    move v8, v4

    :goto_2
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0, v8}, Lcom/android/mms/ui/MessageListAdapter;->setIsGroupConversation(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\\b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v5

    goto :goto_1

    :cond_2
    move v8, v9

    goto :goto_2
.end method

.method private initPlugin(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    :try_start_0
    const-class v1, Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p1, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    iput-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mOP01Plugin:Z

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mMmsDeleteAndForwardPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/util/AndroidException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    invoke-interface {v1, p0}, Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;->init(Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForwardHost;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;

    invoke-direct {v1, p1}, Lcom/mediatek/mms/ext/MmsMultiDeleteAndForwardImpl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    iput-boolean v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mOP01Plugin:Z

    const-string v1, "Mms/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mMmsDeleteAndForwardPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsDeleteAndForwardPlugin:Lcom/mediatek/mms/ext/IMmsMultiDeleteAndForward;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isMsgLocked(Ljava/util/Map$Entry;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    const/4 v6, 0x0

    if-nez p1, :cond_0

    move v5, v6

    :goto_0
    return v5

    :cond_0
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedLockedMsgIds:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v5, v2, v7

    if-nez v5, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    move v5, v6

    goto :goto_0
.end method

.method private markAsImportant([J[JZ)V
    .locals 10
    .param p1    # [J
    .param p2    # [J
    .param p3    # Z

    const/4 v0, 0x1

    const/4 v8, 0x0

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->showProgressIndication()V

    iput-boolean v8, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsLockOrUnlockFinish:Z

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "locked"

    if-eqz p3, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v9, Ljava/lang/Thread;

    new-instance v0, Lcom/android/mms/ui/MultiDeleteActivity$6;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/mms/ui/MultiDeleteActivity$6;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;[JZLandroid/content/ContentValues;[J)V

    invoke-direct {v9, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    if-eqz p1, :cond_1

    array-length v7, p1

    :goto_1
    if-eqz p2, :cond_2

    array-length v6, p2

    :goto_2
    if-eqz p3, :cond_3

    add-int v0, v7, v6

    iput v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    :goto_3
    return-void

    :cond_0
    move v0, v8

    goto :goto_0

    :cond_1
    move v7, v8

    goto :goto_1

    :cond_2
    move v6, v8

    goto :goto_2

    :cond_3
    iput v8, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    goto :goto_3
.end method

.method private markCheckedState(Z)V
    .locals 16
    .param p1    # Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/4 v14, 0x0

    move/from16 v0, p1

    invoke-virtual {v13, v0, v14}, Lcom/android/mms/ui/MessageListAdapter;->setItemsValue(Z[J)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v13}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v6, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedIpMessageIds:Ljava/util/HashSet;

    invoke-virtual {v13}, Ljava/util/HashSet;->clear()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedIpAttachMessageItem:Ljava/util/HashSet;

    invoke-virtual {v13}, Ljava/util/HashSet;->clear()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v13}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v13}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v13, "Mms/MultiDeleteActivity"

    const-string v14, "[markCheckedState] cursor is null"

    invoke-static {v13, v14}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v12

    const/4 v8, 0x0

    if-eqz p1, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v13

    if-eqz v13, :cond_4

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v9, 0x0

    const-wide/16 v13, 0x0

    cmp-long v13, v10, v13

    if-gez v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/16 v13, 0x16

    invoke-interface {v2, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/16 v13, 0x11

    invoke-interface {v2, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/16 v14, 0x82

    if-ne v13, v14, :cond_2

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationCount:I

    :cond_2
    :goto_1
    const/4 v13, 0x1

    if-ne v8, v13, :cond_3

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mImportantCount:I

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    if-nez v13, :cond_1

    :cond_4
    invoke-interface {v2, v12}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v13, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/mms/ui/MessageListItem;

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/android/mms/ui/MessageListItem;->getMessageItem()Lcom/android/mms/ui/MessageItem;

    move-result-object v13

    if-nez v13, :cond_7

    :cond_5
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/16 v13, 0xa

    invoke-interface {v2, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    goto :goto_1

    :cond_7
    if-eqz p1, :cond_8

    invoke-virtual {v6}, Lcom/android/mms/ui/MessageListItem;->getMessageItem()Lcom/android/mms/ui/MessageItem;

    move-result-object v13

    iget v13, v13, Lcom/android/mms/ui/MessageItem;->mIpMessageId:I

    if-lez v13, :cond_8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedIpMessageIds:Ljava/util/HashSet;

    invoke-virtual {v6}, Lcom/android/mms/ui/MessageListItem;->getMessageItem()Lcom/android/mms/ui/MessageItem;

    move-result-object v14

    iget-wide v14, v14, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_8
    if-eqz p1, :cond_9

    invoke-virtual {v6}, Lcom/android/mms/ui/MessageListItem;->getMessageItem()Lcom/android/mms/ui/MessageItem;

    move-result-object v13

    iget-object v13, v13, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    if-eqz v13, :cond_9

    invoke-virtual {v6}, Lcom/android/mms/ui/MessageListItem;->getMessageItem()Lcom/android/mms/ui/MessageItem;

    move-result-object v13

    iget-object v13, v13, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    instance-of v13, v13, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    if-eqz v13, :cond_9

    invoke-virtual {v6}, Lcom/android/mms/ui/MessageListItem;->getMessageItem()Lcom/android/mms/ui/MessageItem;

    move-result-object v13

    iget-object v5, v13, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    check-cast v5, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    invoke-virtual {v5}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->isInboxMsgDownloalable()Z

    move-result v13

    if-eqz v13, :cond_9

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedIpAttachMessageItem:Ljava/util/HashSet;

    invoke-virtual {v6}, Lcom/android/mms/ui/MessageListItem;->getMessageItem()Lcom/android/mms/ui/MessageItem;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_9
    move/from16 v0, p1

    invoke-virtual {v6, v0}, Lcom/android/mms/ui/MessageListItem;->setSelectedBackGroud(Z)V

    goto :goto_3

    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/android/mms/ui/MultiDeleteActivity;->updateSelectCount()V

    goto/16 :goto_0
.end method

.method private selectedMsgHasLocked()Z
    .locals 11

    const/4 v5, 0x0

    iget-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-nez v9, :cond_0

    const/4 v9, 0x0

    :goto_0
    return v9

    :cond_0
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    iput-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedLockedMsgIds:Ljava/util/HashSet;

    iget-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v9}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    iget-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v9}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    const/4 v3, 0x0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v4, 0x0

    const-wide/16 v9, 0x0

    cmp-long v9, v6, v9

    if-gez v9, :cond_4

    iget-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/16 v9, 0x16

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    :goto_1
    const/4 v9, 0x1

    if-ne v3, v9, :cond_2

    const/4 v5, 0x1

    iget-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectedLockedMsgIds:Ljava/util/HashSet;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_1

    :cond_3
    invoke-interface {v0, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v9, v5

    goto :goto_0

    :cond_4
    iget-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/16 v9, 0xa

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    goto :goto_1
.end method

.method private setDeselectAll()V
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v0}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    invoke-direct {p0, v0}, Lcom/android/mms/ui/MultiDeleteActivity;->markCheckedState(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    :cond_0
    return-void
.end method

.method private setSelectAll()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    iget-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    invoke-direct {p0, v0}, Lcom/android/mms/ui/MultiDeleteActivity;->markCheckedState(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    :cond_0
    return-void
.end method

.method private setUpActionBar()V
    .locals 4

    const/4 v3, 0x1

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Lcom/android/mms/ui/MultiDeleteActivity$1;)V

    iput-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectActionMode:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectActionMode:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mSelectMode:Landroid/view/ActionMode;

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    return-void
.end method

.method private showProgressIndication()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0b00b8

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private showReachLimitDialog(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b01e3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b01e1

    new-instance v2, Lcom/android/mms/ui/MultiDeleteActivity$13;

    invoke-direct {v2, p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity$13;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private startMsgListQuery()V
    .locals 5

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    const/16 v2, 0x2537

    invoke-virtual {v1, v2}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    :try_start_0
    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    new-instance v2, Lcom/android/mms/ui/MultiDeleteActivity$1;

    invoke-direct {v2, p0}, Lcom/android/mms/ui/MultiDeleteActivity$1;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;)V

    const-wide/16 v3, 0x32

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {p0, v0}, Landroid/database/sqlite/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    goto :goto_0
.end method

.method private updateSelectCount()V
    .locals 7

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->getSelectedCount()I

    move-result v0

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mChatSelect:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0001

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateSendFailedNotification()V
    .locals 5

    iget-object v2, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {v2}, Lcom/android/mms/data/Conversation;->getThreadId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/android/mms/ui/MultiDeleteActivity$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/android/mms/ui/MultiDeleteActivity$3;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;J)V

    const-string v4, "updateSendFailedNotification"

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method


# virtual methods
.method getAsyncDialog()Lcom/android/mms/ui/AsyncDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mAsyncDialog:Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    invoke-direct {v0, p0, p0}, Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mAsyncDialog:Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mAsyncDialog:Lcom/android/mms/ui/MultiDeleteActivity$ForwardMmsAsyncDialog;

    return-object v0
.end method

.method public notificationsReceived(Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Intent;

    const-string v5, "Mms/MultiDeleteActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notificationsReceived(): intent = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getActionTypeByAction(Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    const-string v5, "Mms/MultiDeleteActivity"

    const-string v6, "ignore a event."

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    const-string v5, "Mms/MultiDeleteActivity"

    const-string v6, "notificationsReceived(): download status notification."

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "DownloadMsgId"

    const-wide/16 v6, 0x0

    invoke-virtual {p1, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v5, "DownloadMsgStatus"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v5, "Mms/MultiDeleteActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notificationsReceived(): downloadStatus = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", id:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounter:I

    iget v6, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    if-lt v5, v6, :cond_2

    const-string v5, "Mms/MultiDeleteActivity"

    const-string v6, "get more download status."

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x2

    if-ne v1, v5, :cond_4

    iget v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounter:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounter:I

    iget v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounterSuccess:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounterSuccess:I

    :cond_3
    :goto_1
    iget v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounter:I

    iget v6, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mUnDownloadedIpMessageCount:I

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMessageListItemHandler:Landroid/os/Handler;

    const/16 v6, 0x3e9

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMessageListItemHandler:Landroid/os/Handler;

    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    const/4 v5, -0x1

    if-ne v1, v5, :cond_3

    iget v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounter:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounter:I

    iget v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounterFail:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mDownloadedIpMessageStepCounterFail:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v6, -0x1

    if-eq p2, v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v6, "contactId"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "com.mediatek.contacts.list.pickdataresult"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v0, :cond_0

    array-length v6, v0

    const/4 v7, 0x1

    if-lt v6, v7, :cond_0

    :cond_2
    const-string v2, ""

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {v0}, Lcom/android/mms/data/ContactList;->blockingGetByIds([J)Lcom/android/mms/data/ContactList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mms/data/ContactList;->serialize()Ljava/lang/String;

    move-result-object v2

    :cond_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v1, v2

    :goto_1
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v6, "com.android.mms.ui.MultiForwardMessageActivity"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "MultiForwardMessageParamMessageIds"

    iget-object v7, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mForwardMsgIds:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "MultiForwardMessageParamThreadId"

    iget-wide v7, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadId:J

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v6, "MultiForwardMessageParamNumbers"

    invoke-virtual {v5, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v6, 0x3e8

    invoke-virtual {p0, v5, v6}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_4
    move-object v1, v4

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xd2 -> :sswitch_0
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "Mms/MultiDeleteActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0, p0}, Lcom/android/mms/ui/MultiDeleteActivity;->initPlugin(Landroid/content/Context;)V

    const v1, 0x7f040044

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setProgressBarVisibility(Z)V

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const-string v1, "Mms/ipmsg/MultiDeleteActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate(): is ip service ready ?= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/mms/ipmessage/ServiceManager;->serviceIsReady()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mms/ipmessage/ServiceManager;->serviceIsReady()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Mms/ipmsg/MultiDeleteActivity"

    const-string v2, "Turn on ipmessage service by ConversationList."

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mms/ipmessage/ServiceManager;->startIpService()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "thread_id"

    invoke-virtual {v1, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadId:J

    iget-wide v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadId:J

    cmp-long v1, v1, v5

    if-nez v1, :cond_1

    const-string v1, "TAG"

    const-string v2, "mThreadId can\'t be zero"

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    iget-wide v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mThreadId:J

    invoke-static {p0, v1, v2, v4}, Lcom/android/mms/data/Conversation;->get(Landroid/content/Context;JZ)Lcom/android/mms/data/Conversation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->initMessageList()V

    invoke-direct {p0, p1}, Lcom/android/mms/ui/MultiDeleteActivity;->initActivityState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->setUpActionBar()V

    new-instance v1, Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mBackgroundQueryHandler:Lcom/android/mms/ui/MultiDeleteActivity$BackgroundQueryHandler;

    invoke-static {}, Lcom/android/mms/MmsConfig;->getAdjustFontSizeEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "message_font_size"

    const/high16 v2, 0x41900000

    invoke-static {p0, v1, v2}, Lcom/android/mms/ui/MessageUtils;->getPreferenceValueFloat(Landroid/content/Context;Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/mms/ui/MultiDeleteActivity;->setTextSize(F)V

    :cond_2
    invoke-static {p0}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v1

    if-lez v1, :cond_3

    invoke-static {p0, p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->addIpMsgNotificationListeners(Landroid/content/Context;Lcom/mediatek/mms/ipmessage/INotificationsListener;)V

    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {p0, p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->removeIpMsgNotificationListeners(Landroid/content/Context;Lcom/mediatek/mms/ipmessage/INotificationsListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mms/ui/MessageListAdapter;->setOnDataSetChangedListener(Lcom/android/mms/ui/MessageListAdapter$OnDataSetChangedListener;)V

    :cond_1
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    if-eqz p2, :cond_0

    check-cast p2, Lcom/android/mms/ui/MessageListItem;

    invoke-virtual {p2}, Lcom/android/mms/ui/MessageListItem;->onMessageListItemClick()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v4

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v5

    if-ne v4, v5, :cond_1

    const-string v4, "is_all_selected"

    const/4 v5, 0x1

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v4

    new-array v0, v4, [J

    iget-object v4, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v4}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v2, 0x0

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    const-string v4, "select_list"

    invoke-virtual {p1, v4, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mConversation:Lcom/android/mms/data/Conversation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/mms/data/Conversation;->blockMarkAsRead(Z)V

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->startMsgListQuery()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mIsSelectedAll:Z

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->checkPendingNotification()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public prepareToForwardMessage()V
    .locals 14

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v9}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-lez v9, :cond_1

    const-string v9, "Mms/MultiDeleteActivity"

    const-string v10, "sms"

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v9, "Mms/MultiDeleteActivity"

    const-string v10, "have  mms"

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0

    :cond_2
    move-object v1, v8

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_5

    iget-boolean v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationHasRun:Z

    if-nez v9, :cond_5

    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->getSelectedCount()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-string v9, "Mms/MultiDeleteActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "enter forward one mms and mMmsId is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    const-string v10, "mms"

    neg-long v11, v5

    const/4 v13, 0x0

    invoke-virtual {v9, v10, v11, v12, v13}, Lcom/android/mms/ui/MessageListAdapter;->getCachedMessageItem(Ljava/lang/String;JLandroid/database/Cursor;)Lcom/android/mms/ui/MessageItem;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/mms/ui/MultiDeleteActivity;->forwardOneMms(Lcom/android/mms/ui/MessageItem;)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/android/mms/ui/MultiDeleteActivity;->getSelectedCount()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_3

    const-string v9, "Mms/MultiDeleteActivity"

    const-string v10, "enter have  mms"

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-direct {v9, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v10, 0x7f0b01de

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x1010355

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f0b01df

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f0b01e1

    new-instance v11, Lcom/android/mms/ui/MultiDeleteActivity$7;

    invoke-direct {v11, p0, v1}, Lcom/android/mms/ui/MultiDeleteActivity$7;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const/high16 v10, 0x1040000

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    :cond_5
    const-string v9, "Mms/MultiDeleteActivity"

    const-string v10, "enter have  sms"

    invoke-static {v9, v10}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationHasRun:Z

    if-eqz v9, :cond_6

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMmsNotificationHasRun:Z

    :cond_6
    new-instance v9, Ljava/lang/Thread;

    new-instance v10, Lcom/android/mms/ui/MultiDeleteActivity$8;

    invoke-direct {v10, p0, v1}, Lcom/android/mms/ui/MultiDeleteActivity$8;-><init>(Lcom/android/mms/ui/MultiDeleteActivity;Ljava/util/ArrayList;)V

    const-string v11, "ForwardMessage"

    invoke-direct {v9, v10, v11}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    goto :goto_1
.end method

.method public setTextSize(F)V
    .locals 4
    .param p1    # F

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v3, p1}, Lcom/android/mms/ui/MessageListAdapter;->setTextSize(F)V

    :cond_0
    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v3, p0, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListView:Landroid/widget/ListView;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mms/ui/MessageListItem;

    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Lcom/android/mms/ui/MessageListItem;->setBodyTextSize(F)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
