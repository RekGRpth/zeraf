.class Lcom/android/mms/ui/MultiDeleteActivity$4;
.super Landroid/os/Handler;
.source "MultiDeleteActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/MultiDeleteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MultiDeleteActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiDeleteActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1    # Landroid/os/Message;

    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    const-string v5, "Mms/MultiDeleteActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown message: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    iget v7, p1, Landroid/os/Message;->arg1:I

    int-to-long v7, v7

    invoke-virtual {v5, v7, v8}, Lcom/android/mms/ui/MessageListAdapter;->changeSelectedState(J)V

    iget v5, p1, Landroid/os/Message;->arg2:I

    if-ne v5, v2, :cond_1

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->arg1:I

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1008(Lcom/android/mms/ui/MultiDeleteActivity;)I

    :cond_1
    :goto_1
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/android/mms/ui/MessageItem;

    iget v5, v4, Lcom/android/mms/ui/MessageItem;->mIpMessageId:I

    if-lez v5, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->arg1:I

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$800(Lcom/android/mms/ui/MultiDeleteActivity;)Ljava/util/HashSet;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->arg1:I

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    const/16 v5, 0x82

    iget v7, v4, Lcom/android/mms/ui/MessageItem;->mMessageType:I

    if-ne v5, v7, :cond_3

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->arg1:I

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1108(Lcom/android/mms/ui/MultiDeleteActivity;)I

    :cond_3
    :goto_3
    iget-object v5, v4, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    if-eqz v5, :cond_4

    iget-object v5, v4, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    instance-of v5, v5, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    if-eqz v5, :cond_4

    iget-object v1, v4, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    check-cast v1, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    invoke-virtual {v1}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->isInboxMsgDownloalable()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->arg1:I

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1208(Lcom/android/mms/ui/MultiDeleteActivity;)I

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1300(Lcom/android/mms/ui/MultiDeleteActivity;)Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_4
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v6}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1402(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v5

    if-lez v5, :cond_5

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v5

    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v6, v6, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v6}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v6

    if-ne v5, v6, :cond_5

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v2}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1402(Lcom/android/mms/ui/MultiDeleteActivity;Z)Z

    :cond_5
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1500(Lcom/android/mms/ui/MultiDeleteActivity;)V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1600(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/ActionMode;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1600(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/ActionMode;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ActionMode;->invalidate()V

    goto/16 :goto_0

    :cond_6
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1010(Lcom/android/mms/ui/MultiDeleteActivity;)I

    goto/16 :goto_1

    :cond_7
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$800(Lcom/android/mms/ui/MultiDeleteActivity;)Ljava/util/HashSet;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->arg1:I

    int-to-long v7, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_8
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1110(Lcom/android/mms/ui/MultiDeleteActivity;)I

    goto/16 :goto_3

    :cond_9
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1210(Lcom/android/mms/ui/MultiDeleteActivity;)I

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1300(Lcom/android/mms/ui/MultiDeleteActivity;)Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    :sswitch_1
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;

    move-result-object v5

    if-nez v5, :cond_a

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    new-instance v7, Landroid/app/ProgressDialog;

    iget-object v8, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {v7, v8}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v7}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1702(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;

    move-result-object v5

    iget-object v7, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v7}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v7

    const/16 v8, 0xec

    invoke-virtual {v7, v8}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;

    move-result-object v5

    new-instance v7, Lcom/android/mms/ui/MultiDeleteActivity$4$1;

    invoke-direct {v7, p0}, Lcom/android/mms/ui/MultiDeleteActivity$4$1;-><init>(Lcom/android/mms/ui/MultiDeleteActivity$4;)V

    invoke-virtual {v5, v7}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    const-string v5, "Mms/MultiDeleteActivity"

    const-string v7, "show download dialog."

    invoke-static {v5, v7}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    :cond_a
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v6}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1802(Lcom/android/mms/ui/MultiDeleteActivity;I)I

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v6}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1902(Lcom/android/mms/ui/MultiDeleteActivity;I)I

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v6}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2002(Lcom/android/mms/ui/MultiDeleteActivity;I)I

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1300(Lcom/android/mms/ui/MultiDeleteActivity;)Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/mms/ui/MessageItem;

    const-string v5, "Mms/MultiDeleteActivity"

    const-string v6, "check a item."

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v3, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    if-eqz v5, :cond_b

    iget-object v5, v3, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    instance-of v5, v5, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    if-eqz v5, :cond_b

    iget-object v1, v3, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    check-cast v1, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    invoke-virtual {v1}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->isInboxMsgDownloalable()Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v5

    iget-wide v6, v3, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-virtual {v5, v6, v7}, Lcom/mediatek/mms/ipmessage/MessageManager;->isDownloading(J)Z

    move-result v5

    if-nez v5, :cond_b

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v5

    iget-wide v6, v3, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-virtual {v5, v6, v7}, Lcom/mediatek/mms/ipmessage/MessageManager;->downloadAttach(J)V

    const-string v5, "Mms/MultiDeleteActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "request download an ipattachmessage, id:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, v3, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    const-string v5, "Mms/MultiDeleteActivity"

    const-string v6, "check a item end."

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :sswitch_2
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;

    move-result-object v5

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1700(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v8}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1702(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    :cond_c
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->notifyDataSetChanged()V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2000(Lcom/android/mms/ui/MultiDeleteActivity;)I

    move-result v5

    iget-object v7, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v7}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1200(Lcom/android/mms/ui/MultiDeleteActivity;)I

    move-result v7

    if-ne v5, v7, :cond_d

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2000(Lcom/android/mms/ui/MultiDeleteActivity;)I

    move-result v5

    iget-object v7, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v7}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2100(Lcom/android/mms/ui/MultiDeleteActivity;)I

    move-result v7

    if-ne v5, v7, :cond_d

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v7, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    const v8, 0x7f0b01ec

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_d
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v6}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1202(Lcom/android/mms/ui/MultiDeleteActivity;I)I

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2400(Lcom/android/mms/ui/MultiDeleteActivity;)Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v6}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2200(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/ActionMode;

    move-result-object v6

    iget-object v7, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v7}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2300(Lcom/android/mms/ui/MultiDeleteActivity;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v8}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2202(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5, v8}, Lcom/android/mms/ui/MultiDeleteActivity;->access$2302(Lcom/android/mms/ui/MultiDeleteActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;

    goto/16 :goto_0

    :sswitch_3
    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiDeleteActivity;->access$1500(Lcom/android/mms/ui/MultiDeleteActivity;)V

    goto/16 :goto_0

    :sswitch_4
    iget v5, p1, Landroid/os/Message;->arg1:I

    if-ne v5, v2, :cond_e

    :goto_6
    const-string v5, "Mms/MessageItemCache"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mMessageListItemHandler#handleMessage(): run adapter notify in mMessageListItemHandler. isClearCache = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5, v2}, Lcom/android/mms/ui/MessageListAdapter;->setClearCacheFlag(Z)V

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$4;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    :cond_e
    move v2, v6

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x64 -> :sswitch_4
        0x3e8 -> :sswitch_1
        0x3e9 -> :sswitch_2
        0x3ea -> :sswitch_3
    .end sparse-switch
.end method
