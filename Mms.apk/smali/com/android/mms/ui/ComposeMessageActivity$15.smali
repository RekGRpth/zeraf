.class Lcom/android/mms/ui/ComposeMessageActivity$15;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ComposeMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 19
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/android/mms/ui/ComposeMessageActivity;->access$5600(Lcom/android/mms/ui/ComposeMessageActivity;)Z

    move-result v14

    if-nez v14, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v14, v14, Lcom/android/mms/ui/ComposeMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v14}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    const/4 v14, 0x0

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const-string v14, "Mms/compose"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onCreateContextMenu(): msgId="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v14, v0, v1, v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$5700(Lcom/android/mms/ui/ComposeMessageActivity;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-object v14, v14, Lcom/android/mms/ui/ComposeMessageActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v14, v13, v10, v11, v4}, Lcom/android/mms/ui/MessageListAdapter;->getCachedMessageItem(Ljava/lang/String;JLandroid/database/Cursor;)Lcom/android/mms/ui/MessageItem;

    move-result-object v12

    if-nez v12, :cond_2

    const-string v14, "Mms/compose"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Cannot load message item for type = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", msgId = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v14, 0x7f0b0234

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    new-instance v9, Lcom/android/mms/ui/ComposeMessageActivity$MsgListMenuClickListener;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {v9, v14, v12}, Lcom/android/mms/ui/ComposeMessageActivity$MsgListMenuClickListener;-><init>(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/android/mms/ui/MessageItem;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/android/mms/MmsConfig;->isServiceEnabled(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_6

    :cond_3
    iget v14, v12, Lcom/android/mms/ui/MessageItem;->mIpMessageId:I

    if-lez v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v14

    iget-wide v15, v12, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-virtual/range {v14 .. v16}, Lcom/mediatek/mms/ipmessage/MessageManager;->getStatus(J)I

    move-result v8

    if-eqz v8, :cond_4

    const/4 v14, 0x4

    if-ne v8, v14, :cond_6

    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v14

    iget-wide v15, v12, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-virtual/range {v14 .. v16}, Lcom/mediatek/mms/ipmessage/MessageManager;->getIpMsgInfo(J)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14, v6}, Lcom/android/mms/ui/ComposeMessageActivity;->access$5800(Lcom/android/mms/ui/ComposeMessageActivity;Lcom/mediatek/mms/ipmessage/message/IpMessage;)I

    move-result v3

    const/4 v14, 0x1

    if-ne v3, v14, :cond_11

    const/4 v14, 0x0

    const/16 v15, 0xd0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd1

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_5
    :goto_1
    const/4 v14, 0x0

    const/16 v15, 0xc8

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd3

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_6
    invoke-virtual {v12}, Lcom/android/mms/ui/MessageItem;->isSms()Z

    move-result v14

    if-eqz v14, :cond_7

    iget v14, v12, Lcom/android/mms/ui/MessageItem;->mIpMessageId:I

    if-lez v14, :cond_12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v14

    iget-wide v15, v12, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-virtual/range {v14 .. v16}, Lcom/mediatek/mms/ipmessage/MessageManager;->getIpMsgInfo(J)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Lcom/mediatek/mms/ipmessage/message/IpMessage;->getType()I

    move-result v14

    if-nez v14, :cond_7

    const/4 v14, 0x0

    const/16 v15, 0xcf

    const/16 v16, 0x0

    const v17, 0x7f0b0162

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_7
    :goto_2
    invoke-virtual {v12}, Lcom/android/mms/ui/MessageItem;->isSms()Z

    move-result v14

    if-nez v14, :cond_8

    invoke-virtual {v12}, Lcom/android/mms/ui/MessageItem;->isDownloaded()Z

    move-result v14

    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14, v10, v11}, Lcom/android/mms/ui/ComposeMessageActivity;->access$5900(Lcom/android/mms/ui/ComposeMessageActivity;J)Z

    move-result v14

    if-eqz v14, :cond_9

    :cond_8
    iget v14, v12, Lcom/android/mms/ui/MessageItem;->mIpMessageId:I

    if-lez v14, :cond_14

    iget-object v14, v12, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    instance-of v14, v14, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    if-eqz v14, :cond_13

    iget-object v5, v12, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    check-cast v5, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    invoke-virtual {v5}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->isInboxMsgDownloalable()Z

    move-result v14

    if-nez v14, :cond_9

    const/4 v14, 0x0

    const/16 v15, 0xd2

    const/16 v16, 0x0

    const v17, 0x7f0b022d

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_9
    :goto_3
    iget v14, v12, Lcom/android/mms/ui/MessageItem;->mIpMessageId:I

    if-lez v14, :cond_16

    iget-object v14, v12, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    instance-of v14, v14, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    if-eqz v14, :cond_15

    iget-object v5, v12, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    check-cast v5, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    invoke-virtual {v5}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->isInboxMsgDownloalable()Z

    move-result v14

    if-nez v14, :cond_a

    const/4 v14, 0x0

    const/16 v15, 0xca

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd5

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_a
    :goto_4
    const/4 v14, 0x0

    const/16 v15, 0xc9

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd4

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :goto_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/android/mms/MmsConfig;->getIpMessagServiceId(Landroid/content/Context;)I

    move-result v14

    if-lez v14, :cond_18

    iget-boolean v14, v12, Lcom/android/mms/ui/MessageItem;->mLocked:Z

    if-eqz v14, :cond_17

    const/4 v14, 0x0

    const/16 v15, 0xcc

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd7

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :goto_6
    invoke-virtual {v12}, Lcom/android/mms/ui/MessageItem;->isMms()Z

    move-result v14

    if-eqz v14, :cond_b

    iget v14, v12, Lcom/android/mms/ui/MessageItem;->mAttachmentType:I

    packed-switch v14, :pswitch_data_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-wide v15, v12, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-static/range {v14 .. v16}, Lcom/android/mms/ui/ComposeMessageActivity;->access$6000(Lcom/android/mms/ui/ComposeMessageActivity;J)Z

    move-result v14

    if-eqz v14, :cond_b

    const/4 v14, 0x0

    const/16 v15, 0x19

    const/16 v16, 0x0

    const v17, 0x7f0b02f2

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_b
    :goto_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v0, p1

    invoke-static {v14, v0, v9, v12}, Lcom/android/mms/ui/ComposeMessageActivity;->access$6100(Lcom/android/mms/ui/ComposeMessageActivity;Landroid/view/ContextMenu;Lcom/android/mms/ui/ComposeMessageActivity$MsgListMenuClickListener;Lcom/android/mms/ui/MessageItem;)V

    const/4 v14, 0x0

    const/16 v15, 0x11

    const/16 v16, 0x0

    const v17, 0x7f0b0224

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    iget-object v14, v12, Lcom/android/mms/ui/MessageItem;->mDeliveryStatus:Lcom/android/mms/ui/MessageItem$DeliveryStatus;

    sget-object v15, Lcom/android/mms/ui/MessageItem$DeliveryStatus;->NONE:Lcom/android/mms/ui/MessageItem$DeliveryStatus;

    if-ne v14, v15, :cond_c

    iget-boolean v14, v12, Lcom/android/mms/ui/MessageItem;->mReadReport:Z

    if-eqz v14, :cond_d

    :cond_c
    const/4 v14, 0x0

    const/16 v15, 0x14

    const/16 v16, 0x0

    const v17, 0x7f0b0225

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_d
    iget-object v14, v12, Lcom/android/mms/ui/MessageItem;->mBody:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_e

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getIpMessagePlugin(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/IIpMessagePlugin;

    move-result-object v14

    invoke-interface {v14}, Lcom/mediatek/mms/ipmessage/IIpMessagePlugin;->isActualPlugin()Z

    move-result v14

    if-nez v14, :cond_e

    iget v14, v12, Lcom/android/mms/ui/MessageItem;->mMessageType:I

    const/16 v15, 0x82

    if-eq v14, v15, :cond_e

    const-string v14, "Mms/compose"

    const-string v15, "onCreateContextMenu(): add select text menu"

    invoke-static {v14, v15}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v14, 0x0

    const/16 v15, 0x24

    const/16 v16, 0x0

    const v17, 0x7f0b00ad

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_f

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/android/mms/MmsConfig;->isServiceEnabled(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_10

    :cond_f
    iget v14, v12, Lcom/android/mms/ui/MessageItem;->mIpMessageId:I

    if-lez v14, :cond_10

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v14

    iget-wide v15, v12, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-virtual/range {v14 .. v16}, Lcom/mediatek/mms/ipmessage/MessageManager;->getType(J)I

    move-result v7

    const/4 v14, 0x4

    if-lt v7, v14, :cond_10

    const/16 v14, 0xb

    if-ge v7, v14, :cond_10

    const/4 v14, 0x7

    if-eq v7, v14, :cond_10

    const-string v14, "Mms/ipmsg/compose"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onCreateContextMenu(): add MENU_EXPORT_SD_CARD. msgId = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-wide v0, v12, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", type = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v14, 0x0

    const/16 v15, 0xd1

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd0

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_10
    invoke-virtual {v12}, Lcom/android/mms/ui/MessageItem;->isSms()Z

    move-result v14

    if-eqz v14, :cond_0

    iget v14, v12, Lcom/android/mms/ui/MessageItem;->mIpMessageId:I

    if-gtz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v14}, Lcom/android/mms/ui/ComposeMessageActivity;->access$6200(Lcom/android/mms/ui/ComposeMessageActivity;)I

    move-result v14

    if-lez v14, :cond_0

    invoke-virtual {v12}, Lcom/android/mms/ui/MessageItem;->isSending()Z

    move-result v14

    if-nez v14, :cond_0

    const/4 v14, 0x0

    const/16 v15, 0x20

    const/16 v16, 0x0

    const v17, 0x7f0b0035

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_11
    const/4 v14, 0x2

    if-ne v3, v14, :cond_5

    const/4 v14, 0x0

    const/16 v15, 0xd0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd2

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_1

    :cond_12
    const/4 v14, 0x0

    const/16 v15, 0xcf

    const/16 v16, 0x0

    const v17, 0x7f0b0162

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_2

    :cond_13
    const/4 v14, 0x0

    const/16 v15, 0xd2

    const/16 v16, 0x0

    const v17, 0x7f0b022d

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_3

    :cond_14
    const/4 v14, 0x0

    const/16 v15, 0x15

    const/16 v16, 0x0

    const v17, 0x7f0b022d

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_3

    :cond_15
    const/4 v14, 0x0

    const/16 v15, 0xca

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd5

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_4

    :cond_16
    const/4 v14, 0x0

    const/16 v15, 0x12

    const/16 v16, 0x0

    const v17, 0x7f0b0226

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_5

    :cond_17
    const/4 v14, 0x0

    const/16 v15, 0xcb

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getResourceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ResourceManager;

    move-result-object v17

    const/16 v18, 0xd6

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/mms/ipmessage/ResourceManager;->getSingleString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_6

    :cond_18
    iget-boolean v14, v12, Lcom/android/mms/ui/MessageItem;->mLocked:Z

    if-eqz v14, :cond_19

    const/4 v14, 0x0

    const/16 v15, 0x1d

    const/16 v16, 0x0

    const v17, 0x7f0b02be

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_6

    :cond_19
    const/4 v14, 0x0

    const/16 v15, 0x1c

    const/16 v16, 0x0

    const v17, 0x7f0b02bd

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_6

    :pswitch_0
    const/4 v14, 0x0

    const/16 v15, 0x10

    const/16 v16, 0x0

    const v17, 0x7f0b0223

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/ComposeMessageActivity$15;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    iget-wide v15, v12, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    invoke-static/range {v14 .. v16}, Lcom/android/mms/ui/ComposeMessageActivity;->access$6000(Lcom/android/mms/ui/ComposeMessageActivity;J)Z

    move-result v14

    if-eqz v14, :cond_b

    const/4 v14, 0x0

    const/16 v15, 0x19

    const/16 v16, 0x0

    const v17, 0x7f0b02f2

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-interface {v0, v14, v15, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v14

    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
