.class Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1;
.super Ljava/lang/Object;
.source "MultiDeleteActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    invoke-direct {v1, v5, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v5

    const v6, 0x7f0e0007

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    new-instance v5, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1$1;

    invoke-direct {v5, p0}, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1$1;-><init>(Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1;)V

    invoke-virtual {v1, v5}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v5, 0x7f0f01a9

    invoke-interface {v2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const v5, 0x7f0f01aa

    invoke-interface {v2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v5, p0, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode$1;->this$1:Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity$SelectActionMode;->this$0:Lcom/android/mms/ui/MultiDeleteActivity;

    iget-object v5, v5, Lcom/android/mms/ui/MultiDeleteActivity;->mMsgListAdapter:Lcom/android/mms/ui/MessageListAdapter;

    invoke-virtual {v5}, Lcom/android/mms/ui/MessageListAdapter;->getSelectedNumber()I

    move-result v5

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lt v5, v6, :cond_2

    if-eqz v3, :cond_0

    invoke-interface {v3, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    if-eqz v4, :cond_1

    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    :goto_0
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    return-void

    :cond_2
    if-eqz v3, :cond_3

    invoke-interface {v3, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    if-eqz v4, :cond_1

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_4
    if-eqz v3, :cond_5

    invoke-interface {v3, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_5
    if-eqz v4, :cond_1

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_6
    if-eqz v3, :cond_1

    invoke-interface {v3, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v4, :cond_1

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
