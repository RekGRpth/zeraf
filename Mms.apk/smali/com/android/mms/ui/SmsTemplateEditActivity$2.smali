.class Lcom/android/mms/ui/SmsTemplateEditActivity$2;
.super Ljava/lang/Object;
.source "SmsTemplateEditActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/SmsTemplateEditActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/SmsTemplateEditActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$200(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/util/List;

    move-result-object v1

    long-to-int v3, p4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v2, v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$102(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v2, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$400(Lcom/android/mms/ui/SmsTemplateEditActivity;)Ljava/util/List;

    move-result-object v1

    long-to-int v3, p4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$302(Lcom/android/mms/ui/SmsTemplateEditActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/android/mms/ui/SmsTemplateEditActivity$2;->this$0:Lcom/android/mms/ui/SmsTemplateEditActivity;

    invoke-static {v1}, Lcom/android/mms/ui/SmsTemplateEditActivity;->access$500(Lcom/android/mms/ui/SmsTemplateEditActivity;)V

    return-void
.end method
