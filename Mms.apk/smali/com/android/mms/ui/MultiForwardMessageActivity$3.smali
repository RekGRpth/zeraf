.class Lcom/android/mms/ui/MultiForwardMessageActivity$3;
.super Ljava/lang/Object;
.source "MultiForwardMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MultiForwardMessageActivity;->checkEcmMode(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

.field final synthetic val$bCEM:Z

.field final synthetic val$slotId:I


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiForwardMessageActivity;IZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    iput p2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->val$slotId:I

    iput-boolean p3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->val$bCEM:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/16 v6, 0xa7

    const/4 v5, 0x3

    # getter for: Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$800()Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/CellConnService/CellConnMgr;->getResult()I

    move-result v0

    const-string v2, "Mms/Txn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "serviceComplete result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->resultToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$800()Lcom/mediatek/CellConnService/CellConnMgr;

    const/4 v2, 0x2

    if-eq v2, v0, :cond_0

    # getter for: Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$800()Lcom/mediatek/CellConnService/CellConnMgr;

    if-nez v0, :cond_1

    :cond_0
    const-string v2, "Mms/Txn"

    const-string v3, "send failed. check CellConn failed"

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    const/16 v3, 0xa6

    # invokes: Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V
    invoke-static {v2, v5, v6, v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$900(Lcom/android/mms/ui/MultiForwardMessageActivity;III)V

    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->val$slotId:I

    # getter for: Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$800()Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/CellConnService/CellConnMgr;->getPreferSlot()I

    move-result v3

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    # getter for: Lcom/android/mms/ui/MultiForwardMessageActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$800()Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/CellConnService/CellConnMgr;->getPreferSlot()I

    move-result v3

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v2, "Mms/Txn"

    const-string v3, "serviceComplete siminfo is null"

    invoke-static {v2, v3}, Lcom/mediatek/encapsulation/MmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    const/16 v3, 0xa4

    # invokes: Lcom/android/mms/ui/MultiForwardMessageActivity;->sendMsgToHandler(III)V
    invoke-static {v2, v5, v6, v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$900(Lcom/android/mms/ui/MultiForwardMessageActivity;III)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-virtual {v1}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSimId()J

    move-result-wide v3

    long-to-int v3, v3

    # setter for: Lcom/android/mms/ui/MultiForwardMessageActivity;->mSelectedSimId:I
    invoke-static {v2, v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1002(Lcom/android/mms/ui/MultiForwardMessageActivity;I)I

    :cond_3
    iget-object v2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    iget-boolean v3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$3;->val$bCEM:Z

    # invokes: Lcom/android/mms/ui/MultiForwardMessageActivity;->forwardDirectly(Z)V
    invoke-static {v2, v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1100(Lcom/android/mms/ui/MultiForwardMessageActivity;Z)V

    goto :goto_0
.end method
