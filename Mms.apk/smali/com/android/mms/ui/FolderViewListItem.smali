.class public Lcom/android/mms/ui/FolderViewListItem;
.super Landroid/widget/RelativeLayout;
.source "FolderViewListItem.java"

# interfaces
.implements Lcom/android/mms/data/Contact$UpdateListener;


# static fields
.field private static final DEBUG:Z = false

.field private static final STYLE_BOLD:Landroid/text/style/StyleSpan;

.field private static final TAG:Ljava/lang/String; = "FolderViewListItem"


# instance fields
.field private mAttachmentView:Landroid/view/View;

.field private mAvatarView:Landroid/widget/ImageView;

.field private mByCard:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mDateView:Landroid/widget/TextView;

.field private mErrorIndicator:Landroid/view/View;

.field private mFromView:Landroid/widget/TextView;

.field private mFview:Lcom/android/mms/data/FolderView;

.field private mHandler:Landroid/os/Handler;

.field private mLockedInd:Landroid/widget/ImageView;

.field private mMuteView:Landroid/view/View;

.field private mPresenceView:Landroid/widget/ImageView;

.field private mSubjectSingleLine:Z

.field private mSubjectView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcom/android/mms/ui/FolderViewListItem;->STYLE_BOLD:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/mms/ui/FolderViewListItem;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/mms/ui/FolderViewListItem;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/FolderViewListItem;)V
    .locals 0
    .param p0    # Lcom/android/mms/ui/FolderViewListItem;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewListItem;->updateFromView()V

    return-void
.end method

.method private formatMessage()Ljava/lang/CharSequence;
    .locals 6

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v2}, Lcom/android/mms/data/FolderView;->getmRecipientString()Lcom/android/mms/data/ContactList;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Lcom/android/mms/data/ContactList;->formatNames(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewListItem;->mContext:Landroid/content/Context;

    const v3, 0x104000e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v2}, Lcom/android/mms/data/FolderView;->getmRead()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/android/mms/ui/FolderViewListItem;->STYLE_BOLD:Landroid/text/style/StyleSpan;

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x11

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    return-object v0
.end method

.method private formatSimStatus(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewListItem;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/android/mms/ui/MessageUtils;->getSimInfo(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewListItem;->mByCard:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewListItem;->mByCard:Landroid/widget/TextView;

    const-string v2, " "

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private getKey(IJ)J
    .locals 3
    .param p1    # I
    .param p2    # J

    const-wide/32 v1, 0x186a0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    neg-long p2, p2

    :cond_0
    :goto_0
    return-wide p2

    :cond_1
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    add-long/2addr p2, v1

    goto :goto_0

    :cond_2
    add-long v0, v1, p2

    neg-long p2, v0

    goto :goto_0
.end method

.method private updateFromView()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v0}, Lcom/android/mms/data/FolderView;->getmRecipientString()Lcom/android/mms/data/ContactList;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mFromView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewListItem;->formatMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public final bind(Landroid/content/Context;Lcom/android/mms/data/FolderView;Ljava/lang/Boolean;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mms/data/FolderView;
    .param p3    # Ljava/lang/Boolean;

    const/4 v9, 0x4

    const/4 v10, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    iput-object p2, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_3

    const v1, 0x7f02012a

    :goto_0
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->getmType()I

    move-result v5

    if-ne v5, v10, :cond_5

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mAvatarView:Landroid/widget/ImageView;

    const v8, 0x7f0200e5

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->hasError()Z

    move-result v3

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->getmHasAttachment()Z

    move-result v2

    iget-object v8, p0, Lcom/android/mms/ui/FolderViewListItem;->mAttachmentView:Landroid/view/View;

    if-eqz v2, :cond_8

    move v5, v6

    :goto_2
    invoke-virtual {v8, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p0}, Lcom/android/mms/data/Contact;->addListener(Lcom/android/mms/data/Contact$UpdateListener;)V

    const-string v5, "FolderViewListItem"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bind mgViewID = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v5, Lcom/android/mms/ui/FolderViewList;->mgViewID:I

    if-ne v5, v10, :cond_9

    if-nez v3, :cond_9

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mDateView:Landroid/widget/TextView;

    const v8, 0x7f0b0312

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFromView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewListItem;->formatMessage()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mSubjectSingleLine:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mSubjectView:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setSingleLine(Z)V

    :cond_1
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mSubjectView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v8}, Lcom/android/mms/data/FolderView;->getmSubject()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/android/mms/ui/FolderViewListItem;->mErrorIndicator:Landroid/view/View;

    if-eqz v3, :cond_a

    move v5, v6

    :goto_4
    invoke-virtual {v8, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->getmSimId()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/android/mms/ui/FolderViewListItem;->formatSimStatus(I)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->isLocked()Z

    move-result v4

    iget-object v8, p0, Lcom/android/mms/ui/FolderViewListItem;->mLockedInd:Landroid/widget/ImageView;

    if-eqz v4, :cond_b

    move v5, v6

    :goto_5
    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mMuteView:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->isMute()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mMuteView:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->getmRead()Z

    move-result v5

    if-eqz v5, :cond_4

    const v1, 0x7f02002e

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mPresenceView:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    const v1, 0x7f02002d

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mPresenceView:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->getmType()I

    move-result v5

    const/4 v8, 0x2

    if-ne v5, v8, :cond_6

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mAvatarView:Landroid/widget/ImageView;

    const v8, 0x7f0200d5

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_6
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->getmType()I

    move-result v5

    const/4 v8, 0x3

    if-ne v5, v8, :cond_7

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mAvatarView:Landroid/widget/ImageView;

    const v8, 0x7f0200f0

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_7
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v5}, Lcom/android/mms/data/FolderView;->getmType()I

    move-result v5

    if-ne v5, v9, :cond_0

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mAvatarView:Landroid/widget/ImageView;

    const v8, 0x7f02009c

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_8
    move v5, v7

    goto/16 :goto_2

    :cond_9
    iget-object v5, p0, Lcom/android/mms/ui/FolderViewListItem;->mDateView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/mms/ui/FolderViewListItem;->mFview:Lcom/android/mms/data/FolderView;

    invoke-virtual {v8}, Lcom/android/mms/data/FolderView;->getmDate()J

    move-result-wide v8

    invoke-static {p1, v8, v9}, Lcom/android/mms/ui/MessageUtils;->formatTimeStampStringExtend(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_a
    move v5, v7

    goto/16 :goto_4

    :cond_b
    move v5, v7

    goto/16 :goto_5
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const-string v0, "FolderViewListItem"

    const-string v1, "onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-static {p0}, Lcom/android/mms/data/Contact;->removeListener(Lcom/android/mms/data/Contact$UpdateListener;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0f006d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mFromView:Landroid/widget/TextView;

    const v0, 0x7f0f002d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mSubjectView:Landroid/widget/TextView;

    const v0, 0x7f0f0067

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mPresenceView:Landroid/widget/ImageView;

    const v0, 0x7f0f006f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mDateView:Landroid/widget/TextView;

    const v0, 0x7f0f006b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mAttachmentView:Landroid/view/View;

    const v0, 0x7f0f006a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mErrorIndicator:Landroid/view/View;

    const v0, 0x7f0f004e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mAvatarView:Landroid/widget/ImageView;

    const v0, 0x7f0f00bb

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mByCard:Landroid/widget/TextView;

    const v0, 0x7f0f0022

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mLockedInd:Landroid/widget/ImageView;

    const v0, 0x7f0f006c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mMuteView:Landroid/view/View;

    return-void
.end method

.method public onUpdate(Lcom/android/mms/data/Contact;)V
    .locals 2
    .param p1    # Lcom/android/mms/data/Contact;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewListItem;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/mms/ui/FolderViewListItem$1;

    invoke-direct {v1, p0}, Lcom/android/mms/ui/FolderViewListItem$1;-><init>(Lcom/android/mms/ui/FolderViewListItem;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setSubjectSingleLineMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mms/ui/FolderViewListItem;->mSubjectSingleLine:Z

    return-void
.end method

.method public final unbind()V
    .locals 0

    invoke-static {p0}, Lcom/android/mms/data/Contact;->removeListener(Lcom/android/mms/data/Contact$UpdateListener;)V

    return-void
.end method
