.class Lcom/android/mms/ui/MultiForwardMessageActivity$4;
.super Ljava/lang/Object;
.source "MultiForwardMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MultiForwardMessageActivity;->doForward(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

.field final synthetic val$finalNumbers:Ljava/lang/String;

.field final synthetic val$messageIdsArray:[Ljava/lang/String;

.field final synthetic val$numbersArray:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MultiForwardMessageActivity;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    iput-object p2, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$finalNumbers:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$messageIdsArray:[Ljava/lang/String;

    iput-object p4, p0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$numbersArray:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 39

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/data/Conversation;->createNew(Landroid/content/Context;)Lcom/android/mms/data/Conversation;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$finalNumbers:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v5, v7}, Lcom/android/mms/data/ContactList;->getByNumbers(Ljava/lang/String;ZZ)Lcom/android/mms/data/ContactList;

    move-result-object v12

    invoke-virtual {v13, v12}, Lcom/android/mms/data/Conversation;->setRecipients(Lcom/android/mms/data/ContactList;)V

    invoke-virtual {v13}, Lcom/android/mms/data/Conversation;->ensureThreadId()J

    move-result-wide v35

    const/16 v34, 0x0

    const/16 v33, 0x0

    const/16 v26, 0x0

    const/16 v25, 0x0

    const/16 v21, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$messageIdsArray:[Ljava/lang/String;

    array-length v5, v5

    invoke-static {v3, v5}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1202(Lcom/android/mms/ui/MultiForwardMessageActivity;I)I

    const/16 v30, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$messageIdsArray:[Ljava/lang/String;

    array-length v0, v11

    move/from16 v22, v0

    const/4 v14, 0x0

    :goto_0
    move/from16 v0, v22

    if-ge v14, v0, :cond_13

    aget-object v15, v11, v14

    add-int/lit8 v30, v30, 0x1

    const-string v3, "-"

    invoke-virtual {v15, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1300()[Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v15, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    if-eqz v24, :cond_6

    :try_start_0
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    const-string v7, "mms"

    move-object/from16 v0, v24

    invoke-static {v3, v5, v0, v7}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1400(Lcom/android/mms/ui/MultiForwardMessageActivity;Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;)Lcom/android/mms/ui/MessageItem;

    move-result-object v4

    if-eqz v4, :cond_4

    const/16 v3, 0x82

    iget v5, v4, Lcom/android/mms/ui/MessageItem;->mMessageType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v3, v5, :cond_1

    if-eqz v24, :cond_0

    :goto_1
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v3, v4, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    if-eqz v3, :cond_2

    iget-object v3, v4, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    instance-of v3, v3, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    if-eqz v3, :cond_2

    iget-object v0, v4, Lcom/android/mms/ui/MessageItem;->mIpMessage:Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-object/from16 v17, v0

    check-cast v17, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/mms/ipmessage/message/IpAttachMessage;->isInboxMsgDownloalable()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "Mms/MultiForwardMessage"

    const-string v5, "get an undownloaded ipmessage"

    invoke-static {v3, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1508(Lcom/android/mms/ui/MultiForwardMessageActivity;)I

    if-eqz v24, :cond_0

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$numbersArray:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v7}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1000(Lcom/android/mms/ui/MultiForwardMessageActivity;)I

    move-result v6

    move-wide/from16 v7, v35

    invoke-static/range {v3 .. v8}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1600(Lcom/android/mms/ui/MultiForwardMessageActivity;Lcom/android/mms/ui/MessageItem;[Ljava/lang/String;IJ)Z

    move-result v3

    if-eqz v3, :cond_3

    add-int/lit8 v26, v26, 0x1

    :goto_3
    if-eqz v24, :cond_0

    goto :goto_1

    :cond_3
    add-int/lit8 v25, v25, 0x1

    goto :goto_3

    :cond_4
    const-string v3, "Mms/MultiForwardMessage"

    const-string v5, "forward MMs getItem: item is null"

    invoke-static {v3, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v3

    if-eqz v24, :cond_5

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v3

    :cond_6
    :try_start_2
    const-string v3, "Mms/MultiForwardMessage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "forward MMs gmmsCursor.moveToFirst failed :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1700()[Ljava/lang/String;

    move-result-object v7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v32

    :try_start_3
    const-string v3, "Mms/MultiForwardMessage"

    const-string v5, "forward IpMessage begin"

    invoke-static {v3, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v32, :cond_12

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1800(Lcom/android/mms/ui/MultiForwardMessageActivity;)Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;

    move-result-object v3

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;->mColumnSmsBody:I

    move-object/from16 v0, v32

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1800(Lcom/android/mms/ui/MultiForwardMessageActivity;)Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;

    move-result-object v3

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;->mColumnMsgId:I

    move-object/from16 v0, v32

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1800(Lcom/android/mms/ui/MultiForwardMessageActivity;)Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;

    move-result-object v3

    iget v3, v3, Lcom/android/mms/ui/MultiForwardMessageActivity$SmsColumnsMap;->mColumnSmsIpMsgId:I

    move-object/from16 v0, v32

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    if-lez v19, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$numbersArray:[Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1900(Lcom/android/mms/ui/MultiForwardMessageActivity;[Ljava/lang/String;)Z

    move-result v37

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1000(Lcom/android/mms/ui/MultiForwardMessageActivity;)I

    move-result v5

    invoke-static {v3, v5}, Lcom/android/mms/MmsConfig;->isServiceEnabled(Landroid/content/Context;I)Z

    move-result v31

    if-eqz v37, :cond_9

    if-eqz v31, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v3

    move/from16 v0, v28

    int-to-long v7, v0

    invoke-virtual {v3, v7, v8}, Lcom/mediatek/mms/ipmessage/MessageManager;->getIpMsgInfo(J)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$finalNumbers:Ljava/lang/String;

    const-string v5, ";"

    const-string v7, ","

    invoke-virtual {v3, v5, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/mediatek/mms/ipmessage/message/IpMessage;->setTo(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1000(Lcom/android/mms/ui/MultiForwardMessageActivity;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/mediatek/mms/ipmessage/message/IpMessage;->setSimId(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1000(Lcom/android/mms/ui/MultiForwardMessageActivity;)I

    move-result v5

    move-object/from16 v0, v16

    invoke-virtual {v3, v0, v5}, Lcom/mediatek/mms/ipmessage/MessageManager;->saveIpMsg(Lcom/mediatek/mms/ipmessage/message/IpMessage;I)I

    move-result v29

    if-lez v29, :cond_8

    add-int/lit8 v21, v21, 0x1

    :goto_4
    const-string v3, "Mms/MultiForwardMessage"

    const-string v5, "forward IpMessage end"

    invoke-static {v3, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_5
    if-eqz v32, :cond_0

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_8
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    :cond_9
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getMessageManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/MessageManager;

    move-result-object v3

    move/from16 v0, v28

    int-to-long v7, v0

    invoke-virtual {v3, v7, v8}, Lcom/mediatek/mms/ipmessage/MessageManager;->getIpMsgInfo(J)Lcom/mediatek/mms/ipmessage/message/IpMessage;

    move-result-object v18

    new-instance v23, Lcom/android/mms/ui/MultiForwardUtils;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, v23

    invoke-direct {v0, v3}, Lcom/android/mms/ui/MultiForwardUtils;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3, v13}, Lcom/android/mms/ui/MultiForwardUtils;->convertIpMsgToSmsOrMms(Lcom/mediatek/mms/ipmessage/message/IpMessage;Landroid/content/Context;Lcom/android/mms/data/Conversation;)Lcom/android/mms/data/WorkingMessage;

    move-result-object v38

    if-eqz v38, :cond_d

    invoke-virtual/range {v38 .. v38}, Lcom/android/mms/data/WorkingMessage;->requiresMms()Z

    move-result v3

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v5}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1000(Lcom/android/mms/ui/MultiForwardMessageActivity;)I

    move-result v5

    move-object/from16 v0, v38

    move-wide/from16 v1, v35

    invoke-static {v3, v0, v1, v2, v5}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$2000(Lcom/android/mms/ui/MultiForwardMessageActivity;Lcom/android/mms/data/WorkingMessage;JI)Z

    move-result v3

    if-eqz v3, :cond_a

    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    :cond_a
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-virtual/range {v38 .. v38}, Lcom/android/mms/data/WorkingMessage;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$numbersArray:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1000(Lcom/android/mms/ui/MultiForwardMessageActivity;)I

    move-result v8

    move-wide/from16 v9, v35

    invoke-static/range {v5 .. v10}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$2100(Lcom/android/mms/ui/MultiForwardMessageActivity;Ljava/lang/String;[Ljava/lang/String;IJ)Z

    move-result v3

    if-eqz v3, :cond_c

    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    :cond_c
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1508(Lcom/android/mms/ui/MultiForwardMessageActivity;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_4

    :catchall_1
    move-exception v3

    if-eqz v32, :cond_e

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v3

    :cond_f
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->val$numbersArray:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$1000(Lcom/android/mms/ui/MultiForwardMessageActivity;)I

    move-result v8

    move-wide/from16 v9, v35

    invoke-static/range {v5 .. v10}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$2100(Lcom/android/mms/ui/MultiForwardMessageActivity;Ljava/lang/String;[Ljava/lang/String;IJ)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v3

    if-eqz v3, :cond_10

    add-int/lit8 v34, v34, 0x1

    goto/16 :goto_4

    :cond_10
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_4

    :cond_11
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_4

    :cond_12
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_5

    :cond_13
    const-string v3, "Mms/MultiForwardMessage"

    const-string v5, "doForward end"

    invoke-static {v3, v5}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v27, Landroid/os/Message;

    invoke-direct/range {v27 .. v27}, Landroid/os/Message;-><init>()V

    const/4 v3, 0x2

    move-object/from16 v0, v27

    iput v3, v0, Landroid/os/Message;->what:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/MultiForwardMessageActivity$4;->this$0:Lcom/android/mms/ui/MultiForwardMessageActivity;

    invoke-static {v3}, Lcom/android/mms/ui/MultiForwardMessageActivity;->access$2200(Lcom/android/mms/ui/MultiForwardMessageActivity;)Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
