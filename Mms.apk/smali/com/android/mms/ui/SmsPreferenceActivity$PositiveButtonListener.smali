.class Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;
.super Ljava/lang/Object;
.source "SmsPreferenceActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/SmsPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PositiveButtonListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/SmsPreferenceActivity;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/SmsPreferenceActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/SmsPreferenceActivity;Lcom/android/mms/ui/SmsPreferenceActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/SmsPreferenceActivity;
    .param p2    # Lcom/android/mms/ui/SmsPreferenceActivity$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;-><init>(Lcom/android/mms/ui/SmsPreferenceActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    iget-object v5, p0, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v5}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$200(Lcom/android/mms/ui/SmsPreferenceActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$300(Lcom/android/mms/ui/SmsPreferenceActivity;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v4}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$200(Lcom/android/mms/ui/SmsPreferenceActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-virtual {v4}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0252

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, ""

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-virtual {v4}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;->getInstance()Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;->this$0:Lcom/android/mms/ui/SmsPreferenceActivity;

    invoke-static {v4}, Lcom/android/mms/ui/SmsPreferenceActivity;->access$400(Lcom/android/mms/ui/SmsPreferenceActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-virtual {v4}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getSlot()I

    move-result v1

    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener$1;

    invoke-direct {v5, p0, v3, v1}, Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener$1;-><init>(Lcom/android/mms/ui/SmsPreferenceActivity$PositiveButtonListener;Lcom/mediatek/encapsulation/com/android/internal/telephony/EncapsulatedTelephonyService;I)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
