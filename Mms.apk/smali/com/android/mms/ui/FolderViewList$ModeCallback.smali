.class Lcom/android/mms/ui/FolderViewList$ModeCallback;
.super Ljava/lang/Object;
.source "FolderViewList.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/FolderViewList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModeCallback"
.end annotation


# instance fields
.field private mDeleteitem:Landroid/view/MenuItem;

.field private mMultiSelectActionBarView:Landroid/view/View;

.field private mSelectedConvCount:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/android/mms/ui/FolderViewList;


# direct methods
.method private constructor <init>(Lcom/android/mms/ui/FolderViewList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/ui/FolderViewList;Lcom/android/mms/ui/FolderViewList$1;)V
    .locals 0
    .param p1    # Lcom/android/mms/ui/FolderViewList;
    .param p2    # Lcom/android/mms/ui/FolderViewList$1;

    invoke-direct {p0, p1}, Lcom/android/mms/ui/FolderViewList$ModeCallback;-><init>(Lcom/android/mms/ui/FolderViewList;)V

    return-void
.end method

.method private cancelSelect()V
    .locals 2

    const-string v0, "FolderViewList"

    const-string v1, "cancel select messages."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/FolderViewListAdapter;->clearbackupstate()V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/FolderViewListAdapter;->clearstate()V

    return-void
.end method

.method private isSelectAll(Z)V
    .locals 7
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/mms/ui/FolderViewList$ModeCallback;->cancelSelect()V

    if-eqz p1, :cond_2

    const-string v4, "FolderViewList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "select all messages, count is : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v6}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, -0x1

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v4}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v4}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/android/mms/ui/FolderViewListAdapter;->getItemId(I)J

    move-result-wide v1

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v4}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Lcom/android/mms/ui/FolderViewListAdapter;->setSelectedState(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mDeleteitem:Landroid/view/MenuItem;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :goto_1
    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mSelectedConvCount:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v5}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mms/ui/FolderViewListAdapter;->getSelectedNumber()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v4}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v4}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mDeleteitem:Landroid/view/MenuItem;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v2

    :pswitch_0
    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/FolderViewListAdapter;->getItemList()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$2800(Lcom/android/mms/ui/FolderViewList;)V

    goto :goto_0

    :cond_0
    invoke-interface {p2, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v2}, Lcom/android/mms/ui/FolderViewList$ModeCallback;->isSelectAll(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1}, Lcom/android/mms/ui/FolderViewList$ModeCallback;->isSelectAll(Z)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0f01a4
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v2}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2, v5}, Lcom/android/mms/ui/FolderViewList;->access$2502(Lcom/android/mms/ui/FolderViewList;Z)Z

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mms/ui/FolderViewListAdapter;->clearstate()V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mms/ui/FolderViewListAdapter;->clearbackupstate()V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$2600(Lcom/android/mms/ui/FolderViewList;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$2700(Lcom/android/mms/ui/FolderViewList;)[J

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$2700(Lcom/android/mms/ui/FolderViewList;)[J

    move-result-object v3

    aget-wide v3, v3, v0

    invoke-virtual {v2, v3, v4}, Lcom/android/mms/ui/FolderViewListAdapter;->setSelectedState(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "FolderViewList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateActionMode: saved selected number "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v4}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mms/ui/FolderViewListAdapter;->getSelectedNumber()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/mms/ui/FolderViewList;->access$2602(Lcom/android/mms/ui/FolderViewList;Z)Z

    :goto_1
    const v2, 0x7f0e0004

    invoke-virtual {v1, v2, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v2, 0x7f0f01a6

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mDeleteitem:Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040014

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v3, 0x7f0f0071

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mSelectedConvCount:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mSelectedConvCount:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v3}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mms/ui/FolderViewListAdapter;->getSelectedNumber()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v3, 0x7f0f0070

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0b0106

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    return v5

    :cond_2
    const-string v2, "FolderViewList"

    const-string v3, "onCreateActionMode: no need to restore adapter state"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2
    .param p1    # Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mms/ui/FolderViewListAdapter;->clearstate()V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/mms/ui/FolderViewList;->access$2502(Lcom/android/mms/ui/FolderViewList;Z)Z

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-virtual {v0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/mms/ui/FolderViewList;->mActionMode:Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v0}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040014

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    const v1, 0x7f0f0071

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mSelectedConvCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mSelectedConvCount:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v2}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mms/ui/FolderViewListAdapter;->getSelectedNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public setItemChecked(JZ)V
    .locals 4
    .param p1    # J
    .param p3    # Z

    const-string v1, "FolderViewList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "itemId ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p3, :cond_1

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v1}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/android/mms/ui/FolderViewListAdapter;->setSelectedState(J)V

    :goto_0
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v1}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mms/ui/FolderViewListAdapter;->getSelectedNumber()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mSelectedConvCount:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mDeleteitem:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    if-lez v0, :cond_2

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mDeleteitem:Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    :goto_1
    const-string v1, "FolderViewList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setItemChecked:checked count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    invoke-static {v1}, Lcom/android/mms/ui/FolderViewList;->access$1800(Lcom/android/mms/ui/FolderViewList;)Lcom/android/mms/ui/FolderViewListAdapter;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/android/mms/ui/FolderViewListAdapter;->removeSelectedState(J)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->mDeleteitem:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    iget-object v1, v1, Lcom/android/mms/ui/FolderViewList;->mActionMode:Landroid/view/ActionMode;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mms/ui/FolderViewList$ModeCallback;->this$0:Lcom/android/mms/ui/FolderViewList;

    iget-object v1, v1, Lcom/android/mms/ui/FolderViewList;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v1}, Landroid/view/ActionMode;->finish()V

    goto :goto_1
.end method
