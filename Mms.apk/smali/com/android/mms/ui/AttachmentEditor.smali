.class public Lcom/android/mms/ui/AttachmentEditor;
.super Landroid/widget/LinearLayout;
.source "AttachmentEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;
    }
.end annotation


# static fields
.field static final MSG_EDIT_SLIDESHOW:I = 0x1

.field static final MSG_PLAY_AUDIO:I = 0x8

.field static final MSG_PLAY_SLIDESHOW:I = 0x3

.field static final MSG_PLAY_VIDEO:I = 0x7

.field static final MSG_REMOVE_ATTACHMENT:I = 0xa

.field static final MSG_REMOVE_EXTERNAL_ATTACHMENT:I = 0xb

.field static final MSG_REMOVE_SLIDES_ATTACHMENT:I = 0xc

.field static final MSG_REPLACE_AUDIO:I = 0x6

.field static final MSG_REPLACE_IMAGE:I = 0x4

.field static final MSG_REPLACE_VIDEO:I = 0x5

.field static final MSG_SEND_SLIDESHOW:I = 0x2

.field static final MSG_VIEW_IMAGE:I = 0x9

.field private static final TAG:Ljava/lang/String; = "AttachmentEditor"


# instance fields
.field private mCanSend:Z

.field private final mContext:Landroid/content/Context;

.field private mFileAttachmentView:Landroid/view/View;

.field private mFlagMini:Z

.field private mHandler:Landroid/os/Handler;

.field private mMediaSize:Landroid/widget/TextView;

.field private mPresenter:Lcom/android/mms/ui/Presenter;

.field private mSendButton:Landroid/widget/Button;

.field private mSlideshow:Lcom/android/mms/model/SlideshowModel;

.field private mView:Lcom/android/mms/ui/SlideViewInterface;

.field private mWorkingMessage:Lcom/android/mms/data/WorkingMessage;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mFlagMini:Z

    iput-object p1, p0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/ui/AttachmentEditor;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/mms/ui/AttachmentEditor;

    iget-object v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private createFileAttachmentView(Lcom/android/mms/data/WorkingMessage;)Landroid/view/View;
    .locals 19
    .param p1    # Lcom/android/mms/data/WorkingMessage;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v14}, Lcom/android/mms/model/SlideshowModel;->getAttachFiles()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v14, 0x5

    invoke-static {v14}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    if-nez v2, :cond_0

    const-string v14, "AttachmentEditor"

    const-string v15, "createFileAttachmentView, oops no attach files found."

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x0

    :goto_0
    return-object v13

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v14

    if-nez v14, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_2

    const/4 v13, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_2

    const/4 v13, 0x0

    goto :goto_0

    :cond_2
    const/4 v14, 0x0

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mms/model/FileAttachmentModel;

    const-string v14, "AttachmentEditor"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "createFileAttachmentView, attach "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v14, 0x7f0f0033

    const v15, 0x7f0f00ab

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/android/mms/ui/AttachmentEditor;->getStubView(II)Landroid/view/View;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    const v14, 0x7f0f00ac

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    const v14, 0x7f0f00ad

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const/4 v8, 0x0

    const/4 v12, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v14

    if-ge v5, v14, :cond_3

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/mms/model/FileAttachmentModel;

    invoke-virtual {v14}, Lcom/android/mms/model/FileAttachmentModel;->getAttachSize()I

    move-result v14

    add-int/2addr v3, v14

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    if-eqz v6, :cond_a

    invoke-interface {v6}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_8

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-le v14, v15, :cond_5

    const-string v14, "AttachmentEditor"

    const-string v15, "createFileAttachmentView, attachFiles.size() > 1"

    invoke-static {v14, v15}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    const v15, 0x7f0b0335

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const v12, 0x7f020139

    :cond_4
    :goto_2
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    const v14, 0x7f0f00ae

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    if-eqz v6, :cond_c

    invoke-interface {v6}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_c

    int-to-long v14, v3

    invoke-static {v14, v15}, Lcom/android/mms/ui/MessageUtils;->getHumanReadableSize(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    const v14, 0x7f0f00b0

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const v14, 0x7f0f00af

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v6, :cond_e

    invoke-interface {v6}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_d

    new-instance v14, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/16 v15, 0xb

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v9, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_4

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->isVCard()Z

    move-result v14

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    const v15, 0x7f0b00b3

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->getSrc()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const v12, 0x7f0200ed

    goto :goto_2

    :cond_6
    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->isVCalendar()Z

    move-result v14

    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    const v15, 0x7f0b00b2

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->getSrc()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const v12, 0x7f0200eb

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->getSrc()Ljava/lang/String;

    move-result-object v8

    const v12, 0x7f020171

    goto/16 :goto_2

    :cond_8
    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->isVCard()Z

    move-result v14

    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    const v15, 0x7f0b00b3

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->getSrc()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const v12, 0x7f0200ed

    goto/16 :goto_2

    :cond_9
    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->isVCalendar()Z

    move-result v14

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    const v15, 0x7f0b00b2

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->getSrc()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const v12, 0x7f0200eb

    goto/16 :goto_2

    :cond_a
    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->isVCard()Z

    move-result v14

    if-eqz v14, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    const v15, 0x7f0b00b3

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->getSrc()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const v12, 0x7f0200ed

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->isVCalendar()Z

    move-result v14

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    const v15, 0x7f0b00b2

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->getSrc()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const v12, 0x7f0200eb

    goto/16 :goto_2

    :cond_c
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/android/mms/model/FileAttachmentModel;->getAttachSize()I

    move-result v15

    int-to-long v15, v15

    invoke-static/range {v15 .. v16}, Lcom/android/mms/ui/MessageUtils;->getHumanReadableSize(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x0

    invoke-static {v15}, Lcom/android/mms/MmsConfig;->getUserSetMmsSizeLimit(Z)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "K"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_d
    new-instance v14, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/16 v15, 0xa

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v9, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_e
    new-instance v14, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/16 v15, 0xa

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v9, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method private createMediaView(IIIIIIIIIILcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # Lcom/android/mms/data/WorkingMessage;

    invoke-direct {p0, p1, p2}, Lcom/android/mms/ui/AttachmentEditor;->getStubView(II)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v5, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    invoke-virtual {v5, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v5, p5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v5, p6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/android/mms/ui/AttachmentEditor;->mMediaSize:Landroid/widget/TextView;

    add-int/lit8 v7, p7, -0x1

    div-int/lit16 v7, v7, 0x400

    add-int/lit8 v4, v7, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "K/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/android/mms/MmsConfig;->getUserSetMmsSizeLimit(Z)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "K"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, Lcom/android/mms/ui/AttachmentEditor;->mMediaSize:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v7, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    move/from16 v0, p8

    invoke-direct {v7, p0, v0}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v7, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    move/from16 v0, p9

    invoke-direct {v7, p0, v0}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v7, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    move/from16 v0, p10

    invoke-direct {v7, p0, v0}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v7, p0, Lcom/android/mms/ui/AttachmentEditor;->mFlagMini:Z

    if-eqz v7, :cond_0

    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    check-cast v5, Lcom/android/mms/ui/SlideViewInterface;

    return-object v5
.end method

.method private createSlideshowView(ZLcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;
    .locals 15
    .param p1    # Z
    .param p2    # Lcom/android/mms/data/WorkingMessage;

    const v12, 0x7f0f0037

    const v13, 0x7f0f015c

    invoke-direct {p0, v12, v13}, Lcom/android/mms/ui/AttachmentEditor;->getStubView(II)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    const v12, 0x7f0f015f

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    const v12, 0x7f0f0160

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    iput-object v12, p0, Lcom/android/mms/ui/AttachmentEditor;->mSendButton:Landroid/widget/Button;

    iget-object v12, p0, Lcom/android/mms/ui/AttachmentEditor;->mSendButton:Landroid/widget/Button;

    new-instance v13, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/4 v14, 0x2

    invoke-direct {v13, p0, v14}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v12, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/mms/ui/AttachmentEditor;->updateSendButton()V

    const v12, 0x7f0f0104

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    move-object/from16 v0, p2

    iget-boolean v12, v0, Lcom/android/mms/data/WorkingMessage;->mHasDrmPart:Z

    if-eqz v12, :cond_0

    const-string v12, "AttachmentEditor"

    const-string v13, "mHasDrmPart"

    invoke-static {v12, v13}, Lcom/mediatek/encapsulation/MmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v12, p0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020135

    invoke-static {v12, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v12, p0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    sget v13, Lcom/mediatek/encapsulation/com/mediatek/internal/EncapsulatedR$drawable;->drm_red_lock:I

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    new-instance v3, Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;

    iget-object v12, p0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    invoke-direct {v3, v12}, Lcom/mediatek/encapsulation/android/drm/EncapsulatedDrmManagerClient;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v1, v5}, Lcom/mediatek/drm/OmaDrmUiUtils;->overlayBitmap(Lcom/mediatek/drm/OmaDrmClient;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v12

    if-nez v12, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    const v12, 0x7f0f0017

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    iput-object v12, p0, Lcom/android/mms/ui/AttachmentEditor;->mMediaSize:Landroid/widget/TextView;

    invoke-virtual/range {p2 .. p2}, Lcom/android/mms/data/WorkingMessage;->getCurrentMessageSize()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    div-int/lit16 v12, v12, 0x400

    add-int/lit8 v10, v12, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "K/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x0

    invoke-static {v13}, Lcom/android/mms/MmsConfig;->getUserSetMmsSizeLimit(Z)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "K"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v12, p0, Lcom/android/mms/ui/AttachmentEditor;->mMediaSize:Landroid/widget/TextView;

    invoke-virtual {v12, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setEnabled(Z)V

    new-instance v12, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/4 v13, 0x1

    invoke-direct {v12, p0, v13}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v4, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v12, p0, Lcom/android/mms/ui/AttachmentEditor;->mSendButton:Landroid/widget/Button;

    new-instance v13, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/4 v14, 0x2

    invoke-direct {v13, p0, v14}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v12, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v12, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/4 v13, 0x3

    invoke-direct {v12, p0, v13}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v8, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v12, 0x7f0f0161

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    const/4 v12, 0x5

    invoke-static {v12}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    if-eqz v7, :cond_2

    invoke-interface {v7}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    new-instance v12, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/16 v13, 0xc

    invoke-direct {v12, p0, v13}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v9, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    check-cast v11, Lcom/android/mms/ui/SlideViewInterface;

    return-object v11

    :cond_1
    new-instance v12, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/16 v13, 0xa

    invoke-direct {v12, p0, v13}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v9, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    new-instance v12, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;

    const/16 v13, 0xa

    invoke-direct {v12, p0, v13}, Lcom/android/mms/ui/AttachmentEditor$MessageOnClick;-><init>(Lcom/android/mms/ui/AttachmentEditor;I)V

    invoke-virtual {v9, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private createView(Lcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;
    .locals 21
    .param p1    # Lcom/android/mms/data/WorkingMessage;

    invoke-direct/range {p0 .. p0}, Lcom/android/mms/ui/AttachmentEditor;->inPortraitMode()Z

    move-result v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v3}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/mms/ui/AttachmentEditor;->createSlideshowView(ZLcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x5

    invoke-static {v3}, Lcom/android/mms/MmsPluginManager;->getMmsPluginObject(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;

    const/16 v16, 0x0

    const/4 v15, 0x1

    const/16 v17, 0x0

    if-eqz v19, :cond_1

    invoke-interface/range {v19 .. v19}, Lcom/mediatek/mms/ext/IMmsAttachmentEnhance;->isSupportAttachmentEnhance()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    const/16 v17, 0x1

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/mms/model/SlideshowModel;->get(I)Lcom/android/mms/model/SlideModel;

    move-result-object v20

    if-nez v20, :cond_2

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    :cond_2
    invoke-virtual/range {v20 .. v20}, Lcom/android/mms/model/SlideModel;->hasImage()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v17, :cond_3

    const v4, 0x7f0f0034

    const v5, 0x7f0f00c3

    const v6, 0x7f0f00c5

    const v7, 0x7f0f0091

    const v8, 0x7f0f00c6

    const v9, 0x7f0f0017

    invoke-virtual/range {p1 .. p1}, Lcom/android/mms/data/WorkingMessage;->getCurrentMessageSize()I

    move-result v10

    const/16 v11, 0x9

    const/4 v12, 0x4

    const/16 v13, 0xa

    move-object/from16 v3, p0

    move-object/from16 v14, p1

    invoke-direct/range {v3 .. v14}, Lcom/android/mms/ui/AttachmentEditor;->createMediaView(IIIIIIIIIILcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;

    move-result-object v3

    goto :goto_0

    :cond_3
    const v4, 0x7f0f0034

    const v5, 0x7f0f00c3

    const v6, 0x7f0f00c5

    const v7, 0x7f0f0091

    const v8, 0x7f0f00c6

    const v9, 0x7f0f0017

    invoke-virtual/range {p1 .. p1}, Lcom/android/mms/data/WorkingMessage;->getCurrentMessageSize()I

    move-result v10

    const/16 v11, 0x9

    const/4 v12, 0x4

    const/16 v13, 0xc

    move-object/from16 v3, p0

    move-object/from16 v14, p1

    invoke-direct/range {v3 .. v14}, Lcom/android/mms/ui/AttachmentEditor;->createMediaView(IIIIIIIIIILcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;

    move-result-object v3

    goto :goto_0

    :cond_4
    invoke-virtual/range {v20 .. v20}, Lcom/android/mms/model/SlideModel;->hasVideo()Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v17, :cond_5

    const v4, 0x7f0f0035

    const v5, 0x7f0f0175

    const v6, 0x7f0f0177

    const v7, 0x7f0f0178

    const v8, 0x7f0f0179

    const v9, 0x7f0f0017

    invoke-virtual/range {p1 .. p1}, Lcom/android/mms/data/WorkingMessage;->getCurrentMessageSize()I

    move-result v10

    const/4 v11, 0x7

    const/4 v12, 0x5

    const/16 v13, 0xa

    move-object/from16 v3, p0

    move-object/from16 v14, p1

    invoke-direct/range {v3 .. v14}, Lcom/android/mms/ui/AttachmentEditor;->createMediaView(IIIIIIIIIILcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;

    move-result-object v3

    goto/16 :goto_0

    :cond_5
    const v4, 0x7f0f0035

    const v5, 0x7f0f0175

    const v6, 0x7f0f0177

    const v7, 0x7f0f0178

    const v8, 0x7f0f0179

    const v9, 0x7f0f0017

    invoke-virtual/range {p1 .. p1}, Lcom/android/mms/data/WorkingMessage;->getCurrentMessageSize()I

    move-result v10

    const/4 v11, 0x7

    const/4 v12, 0x5

    const/16 v13, 0xc

    move-object/from16 v3, p0

    move-object/from16 v14, p1

    invoke-direct/range {v3 .. v14}, Lcom/android/mms/ui/AttachmentEditor;->createMediaView(IIIIIIIIIILcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;

    move-result-object v3

    goto/16 :goto_0

    :cond_6
    invoke-virtual/range {v20 .. v20}, Lcom/android/mms/model/SlideModel;->hasAudio()Z

    move-result v3

    if-eqz v3, :cond_8

    if-nez v17, :cond_7

    const v4, 0x7f0f0036

    const v5, 0x7f0f0010

    const v6, 0x7f0f0018

    const v7, 0x7f0f0019

    const v8, 0x7f0f001a

    const v9, 0x7f0f0017

    invoke-virtual/range {p1 .. p1}, Lcom/android/mms/data/WorkingMessage;->getCurrentMessageSize()I

    move-result v10

    const/16 v11, 0x8

    const/4 v12, 0x6

    const/16 v13, 0xa

    move-object/from16 v3, p0

    move-object/from16 v14, p1

    invoke-direct/range {v3 .. v14}, Lcom/android/mms/ui/AttachmentEditor;->createMediaView(IIIIIIIIIILcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;

    move-result-object v3

    goto/16 :goto_0

    :cond_7
    const v4, 0x7f0f0036

    const v5, 0x7f0f0010

    const v6, 0x7f0f0018

    const v7, 0x7f0f0019

    const v8, 0x7f0f001a

    const v9, 0x7f0f0017

    invoke-virtual/range {p1 .. p1}, Lcom/android/mms/data/WorkingMessage;->getCurrentMessageSize()I

    move-result v10

    const/16 v11, 0x8

    const/4 v12, 0x6

    const/16 v13, 0xc

    move-object/from16 v3, p0

    move-object/from16 v14, p1

    invoke-direct/range {v3 .. v14}, Lcom/android/mms/ui/AttachmentEditor;->createMediaView(IIIIIIIIIILcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;

    move-result-object v3

    goto/16 :goto_0

    :cond_8
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3
.end method

.method private getStubView(II)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method private inPortraitMode()Z
    .locals 3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateSendButton()V
    .locals 2

    iget-object v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mSendButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mSendButton:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/android/mms/ui/AttachmentEditor;->mCanSend:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mSendButton:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/android/mms/ui/AttachmentEditor;->mCanSend:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public hideView()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mView:Lcom/android/mms/ui/SlideViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mView:Lcom/android/mms/ui/SlideViewInterface;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mFileAttachmentView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mFileAttachmentView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public onTextChangeForOneSlide(Ljava/lang/CharSequence;)V
    .locals 7
    .param p1    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/ExceedMessageSizeException;
        }
    .end annotation

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mMediaSize:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mWorkingMessage:Lcom/android/mms/data/WorkingMessage;

    invoke-virtual {v4}, Lcom/android/mms/data/WorkingMessage;->hasSlideshow()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mWorkingMessage:Lcom/android/mms/data/WorkingMessage;

    invoke-virtual {v4}, Lcom/android/mms/data/WorkingMessage;->getSlideshow()Lcom/android/mms/model/SlideshowModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1, v6}, Landroid/telephony/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)[I

    move-result-object v1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mWorkingMessage:Lcom/android/mms/data/WorkingMessage;

    invoke-virtual {v4}, Lcom/android/mms/data/WorkingMessage;->hasAttachment()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mWorkingMessage:Lcom/android/mms/data/WorkingMessage;

    invoke-virtual {v4}, Lcom/android/mms/data/WorkingMessage;->getCurrentMessageSize()I

    move-result v3

    :cond_2
    add-int/lit8 v4, v3, -0x1

    div-int/lit16 v4, v4, 0x400

    add-int/lit8 v2, v4, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "K/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v6}, Lcom/android/mms/MmsConfig;->getUserSetMmsSizeLimit(Z)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "K"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mMediaSize:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setCanSend(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/mms/ui/AttachmentEditor;->mCanSend:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/mms/ui/AttachmentEditor;->mCanSend:Z

    invoke-direct {p0}, Lcom/android/mms/ui/AttachmentEditor;->updateSendButton()V

    :cond_0
    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/mms/ui/AttachmentEditor;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public update(Lcom/android/mms/data/WorkingMessage;Z)V
    .locals 0
    .param p1    # Lcom/android/mms/data/WorkingMessage;
    .param p2    # Z

    iput-boolean p2, p0, Lcom/android/mms/ui/AttachmentEditor;->mFlagMini:Z

    invoke-virtual {p0, p1}, Lcom/android/mms/ui/AttachmentEditor;->update(Lcom/android/mms/data/WorkingMessage;)Z

    return-void
.end method

.method public update(Lcom/android/mms/data/WorkingMessage;)Z
    .locals 9
    .param p1    # Lcom/android/mms/data/WorkingMessage;

    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/mms/ui/AttachmentEditor;->hideView()V

    iput-object v8, p0, Lcom/android/mms/ui/AttachmentEditor;->mView:Lcom/android/mms/ui/SlideViewInterface;

    iput-object v8, p0, Lcom/android/mms/ui/AttachmentEditor;->mFileAttachmentView:Landroid/view/View;

    iput-object p1, p0, Lcom/android/mms/ui/AttachmentEditor;->mWorkingMessage:Lcom/android/mms/data/WorkingMessage;

    invoke-virtual {p1}, Lcom/android/mms/data/WorkingMessage;->hasAttachment()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Lcom/android/mms/data/WorkingMessage;->getSlideshow()Lcom/android/mms/model/SlideshowModel;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    :try_start_0
    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v4}, Lcom/android/mms/model/SlideshowModel;->sizeOfFilesAttach()I

    move-result v4

    if-lez v4, :cond_1

    invoke-direct {p0, p1}, Lcom/android/mms/ui/AttachmentEditor;->createFileAttachmentView(Lcom/android/mms/data/WorkingMessage;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mFileAttachmentView:Landroid/view/View;

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mFileAttachmentView:Landroid/view/View;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mFileAttachmentView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v4}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v4

    if-nez v4, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/mms/ui/AttachmentEditor;->createView(Lcom/android/mms/data/WorkingMessage;)Lcom/android/mms/ui/SlideViewInterface;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mView:Lcom/android/mms/ui/SlideViewInterface;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mPresenter:Lcom/android/mms/ui/Presenter;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    iget-object v5, p0, Lcom/android/mms/ui/AttachmentEditor;->mPresenter:Lcom/android/mms/ui/Presenter;

    invoke-virtual {v5}, Lcom/android/mms/ui/Presenter;->getModel()Lcom/android/mms/model/Model;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    :cond_3
    const-string v4, "MmsThumbnailPresenter"

    iget-object v5, p0, Lcom/android/mms/ui/AttachmentEditor;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/mms/ui/AttachmentEditor;->mView:Lcom/android/mms/ui/SlideViewInterface;

    iget-object v7, p0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-static {v4, v5, v6, v7}, Lcom/android/mms/ui/PresenterFactory;->getPresenter(Ljava/lang/String;Landroid/content/Context;Lcom/android/mms/ui/ViewInterface;Lcom/android/mms/model/Model;)Lcom/android/mms/ui/Presenter;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mPresenter:Lcom/android/mms/ui/Presenter;

    :goto_1
    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mPresenter:Lcom/android/mms/ui/Presenter;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v4}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v4

    if-le v4, v3, :cond_6

    iget-object v2, p0, Lcom/android/mms/ui/AttachmentEditor;->mPresenter:Lcom/android/mms/ui/Presenter;

    invoke-virtual {v2, v8}, Lcom/android/mms/ui/Presenter;->present(Lcom/android/mms/util/ItemLoadedCallback;)V

    :cond_4
    :goto_2
    move v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mPresenter:Lcom/android/mms/ui/Presenter;

    iget-object v5, p0, Lcom/android/mms/ui/AttachmentEditor;->mView:Lcom/android/mms/ui/SlideViewInterface;

    invoke-virtual {v4, v5}, Lcom/android/mms/ui/Presenter;->setView(Lcom/android/mms/ui/ViewInterface;)V

    goto :goto_1

    :cond_6
    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v4}, Lcom/android/mms/model/SlideshowModel;->size()I

    move-result v4

    if-ne v4, v3, :cond_4

    iget-object v4, p0, Lcom/android/mms/ui/AttachmentEditor;->mSlideshow:Lcom/android/mms/model/SlideshowModel;

    invoke-virtual {v4, v2}, Lcom/android/mms/model/SlideshowModel;->get(I)Lcom/android/mms/model/SlideModel;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mms/ui/AttachmentEditor;->mPresenter:Lcom/android/mms/ui/Presenter;

    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/android/mms/model/SlideModel;->hasAudio()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {v1}, Lcom/android/mms/model/SlideModel;->hasImage()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {v1}, Lcom/android/mms/model/SlideModel;->hasVideo()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_7
    iget-object v2, p0, Lcom/android/mms/ui/AttachmentEditor;->mPresenter:Lcom/android/mms/ui/Presenter;

    invoke-virtual {v2, v8}, Lcom/android/mms/ui/Presenter;->present(Lcom/android/mms/util/ItemLoadedCallback;)V

    goto :goto_2
.end method
