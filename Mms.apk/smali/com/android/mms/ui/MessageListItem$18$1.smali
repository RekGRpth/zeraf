.class Lcom/android/mms/ui/MessageListItem$18$1;
.super Ljava/lang/Object;
.source "MessageListItem.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/ui/MessageListItem$18;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mms/ui/MessageListItem$18;


# direct methods
.method constructor <init>(Lcom/android/mms/ui/MessageListItem$18;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 10
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v6

    const-string v7, "mounted"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget-object v6, v6, Lcom/android/mms/ui/MessageListItem$18;->this$0:Lcom/android/mms/ui/MessageListItem;

    invoke-static {v6}, Lcom/android/mms/ui/MessageListItem;->access$2400(Lcom/android/mms/ui/MessageListItem;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b00fa

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    :cond_0
    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget-object v6, v6, Lcom/android/mms/ui/MessageListItem$18;->this$0:Lcom/android/mms/ui/MessageListItem;

    invoke-static {v6}, Lcom/android/mms/ui/MessageListItem;->access$000(Lcom/android/mms/ui/MessageListItem;)Lcom/android/mms/ui/MessageItem;

    move-result-object v6

    iget-wide v2, v6, Lcom/android/mms/ui/MessageItem;->mMsgId:J

    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget v6, v6, Lcom/android/mms/ui/MessageListItem$18;->val$filesize:I

    if-ne v6, v9, :cond_4

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget-object v6, v6, Lcom/android/mms/ui/MessageListItem$18;->this$0:Lcom/android/mms/ui/MessageListItem;

    invoke-static {v6, v2, v3}, Lcom/android/mms/ui/MessageListItem;->access$2500(Lcom/android/mms/ui/MessageListItem;J)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget-object v6, v6, Lcom/android/mms/ui/MessageListItem$18;->this$0:Lcom/android/mms/ui/MessageListItem;

    invoke-static {v6, v2, v3}, Lcom/android/mms/ui/MessageListItem;->access$2600(Lcom/android/mms/ui/MessageListItem;J)Z

    move-result v4

    :cond_1
    if-eqz v4, :cond_3

    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget-object v6, v6, Lcom/android/mms/ui/MessageListItem$18;->this$0:Lcom/android/mms/ui/MessageListItem;

    invoke-static {v6}, Lcom/android/mms/ui/MessageListItem;->access$2700(Lcom/android/mms/ui/MessageListItem;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b02f3

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget-object v6, v6, Lcom/android/mms/ui/MessageListItem$18;->this$0:Lcom/android/mms/ui/MessageListItem;

    invoke-static {v6}, Lcom/android/mms/ui/MessageListItem;->access$2800(Lcom/android/mms/ui/MessageListItem;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b02f4

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget v6, v6, Lcom/android/mms/ui/MessageListItem$18;->val$filesize:I

    if-le v6, v9, :cond_2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v6, "savecontent"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget-object v6, v6, Lcom/android/mms/ui/MessageListItem$18;->this$0:Lcom/android/mms/ui/MessageListItem;

    invoke-static {v6}, Lcom/android/mms/ui/MessageListItem;->access$2900(Lcom/android/mms/ui/MessageListItem;)Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/android/mms/ui/MultiSaveActivity;

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "msgid"

    invoke-virtual {v1, v6, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/mms/ui/MessageListItem$18$1;->this$1:Lcom/android/mms/ui/MessageListItem$18;

    iget-object v6, v6, Lcom/android/mms/ui/MessageListItem$18;->this$0:Lcom/android/mms/ui/MessageListItem;

    invoke-static {v6}, Lcom/android/mms/ui/MessageListItem;->access$3000(Lcom/android/mms/ui/MessageListItem;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
