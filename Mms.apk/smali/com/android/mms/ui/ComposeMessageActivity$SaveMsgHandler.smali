.class final Lcom/android/mms/ui/ComposeMessageActivity$SaveMsgHandler;
.super Landroid/os/Handler;
.source "ComposeMessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/ui/ComposeMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SaveMsgHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/ui/ComposeMessageActivity;


# direct methods
.method public constructor <init>(Lcom/android/mms/ui/ComposeMessageActivity;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/android/mms/ui/ComposeMessageActivity$SaveMsgHandler;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v4, "Mms/Txn"

    const-string v5, "exit save message thread"

    invoke-static {v4, v5}, Lcom/mediatek/encapsulation/MmsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Looper;->quit()V

    goto :goto_0

    :pswitch_2
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget v4, p1, Landroid/os/Message;->arg1:I

    int-to-long v1, v4

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$SaveMsgHandler;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v3, v1, v2}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14700(Lcom/android/mms/ui/ComposeMessageActivity;Ljava/lang/String;J)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    iget-object v4, p0, Lcom/android/mms/ui/ComposeMessageActivity$SaveMsgHandler;->this$0:Lcom/android/mms/ui/ComposeMessageActivity;

    invoke-static {v4, v0}, Lcom/android/mms/ui/ComposeMessageActivity;->access$14800(Lcom/android/mms/ui/ComposeMessageActivity;Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
