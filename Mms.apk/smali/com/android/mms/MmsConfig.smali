.class public Lcom/android/mms/MmsConfig;
.super Ljava/lang/Object;
.source "MmsConfig.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final DEFAULT_HTTP_KEY_X_WAP_PROFILE:Ljava/lang/String; = "x-wap-profile"

.field private static final DEFAULT_USER_AGENT:Ljava/lang/String; = "Android-Mms/2.0"

.field private static final LOCAL_LOGV:Z = false

.field private static final MAX_IMAGE_HEIGHT:I = 0x1e0

.field private static final MAX_IMAGE_WIDTH:I = 0x280

.field private static final MAX_RETRY_COUNT:I = 0x3

.field private static final RECIPIENTS_LIMIT:I = 0x32

.field private static final TAG:Ljava/lang/String; = "MmsConfig"

.field private static mAliasEnabled:Z

.field private static mAliasRuleMaxChars:I

.field private static mAliasRuleMinChars:I

.field private static mAllowAttachAudio:Z

.field private static mDefaultMMSMessagesPerThread:I

.field private static mDefaultSMSMessagesPerThread:I

.field private static mDeviceStorageFull:Z

.field private static mEmailGateway:Ljava/lang/String;

.field private static mEnableGroupMms:Z

.field private static mEnableMMSDeliveryReports:Z

.field private static mEnableMMSReadReports:Z

.field private static mEnableMultipartSMS:Z

.field private static mEnableSMSDeliveryReports:Z

.field private static mEnableSlideDuration:Z

.field private static mHttpParams:Ljava/lang/String;

.field private static mHttpParamsLine1Key:Ljava/lang/String;

.field private static mMaxImageHeight:I

.field private static mMaxImageWidth:I

.field private static mMaxMessageCountPerThread:I

.field private static mMaxMessageSize:I

.field private static mMaxRestrictedImageHeight:I

.field private static mMaxRestrictedImageWidth:I

.field private static mMaxSizeScaleForPendingMmsAllowed:I

.field private static mMaxSubjectLength:I

.field private static mMaxTextLength:I

.field private static mMinMessageCountPerThread:I

.field private static mMinimumSlideElementDuration:I

.field private static mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

.field private static mMmsEnabled:I

.field private static mNotifyWapMMSC:Z

.field private static mReceiveMmsSizeLimitFor2G:I

.field private static mReceiveMmsSizeLimitForTD:I

.field private static mRecipientLimit:I

.field private static mSlAutoLanuchEnabled:Z

.field private static mSmsRecipientLimit:I

.field private static mTransIdEnabled:Z

.field private static mUaProfTagName:Ljava/lang/String;

.field private static mUaProfUrl:Ljava/lang/String;

.field private static mUserAgent:Ljava/lang/String;

.field private static mUserSetMmsSizeLimit:I

.field private static sAudioTempPath:Ljava/lang/String;

.field private static sCalendarTempPath:Ljava/lang/String;

.field private static sPicTempPath:Ljava/lang/String;

.field private static sSim1Id:J

.field private static sSim2Id:J

.field private static sSlot1RetryCounter:I

.field private static sSlot1SimExist:Z

.field private static sSlot2RetryCounter:I

.field private static sSlot2SimExist:Z

.field private static sVcardTempPath:Ljava/lang/String;

.field private static sVideoTempPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-wide/16 v5, -0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    sput-boolean v2, Lcom/android/mms/MmsConfig;->mTransIdEnabled:Z

    sput v1, Lcom/android/mms/MmsConfig;->mMmsEnabled:I

    const v0, 0x100000

    sput v0, Lcom/android/mms/MmsConfig;->mMaxMessageSize:I

    const-string v0, "Android-Mms/2.0"

    sput-object v0, Lcom/android/mms/MmsConfig;->mUserAgent:Ljava/lang/String;

    const-string v0, "x-wap-profile"

    sput-object v0, Lcom/android/mms/MmsConfig;->mUaProfTagName:Ljava/lang/String;

    sput-object v3, Lcom/android/mms/MmsConfig;->mUaProfUrl:Ljava/lang/String;

    sput-object v3, Lcom/android/mms/MmsConfig;->mHttpParams:Ljava/lang/String;

    sput-object v3, Lcom/android/mms/MmsConfig;->mHttpParamsLine1Key:Ljava/lang/String;

    sput-object v3, Lcom/android/mms/MmsConfig;->mEmailGateway:Ljava/lang/String;

    const/16 v0, 0x1e0

    sput v0, Lcom/android/mms/MmsConfig;->mMaxImageHeight:I

    const/16 v0, 0x280

    sput v0, Lcom/android/mms/MmsConfig;->mMaxImageWidth:I

    const v0, 0x7fffffff

    sput v0, Lcom/android/mms/MmsConfig;->mRecipientLimit:I

    const/16 v0, 0x1f4

    sput v0, Lcom/android/mms/MmsConfig;->mDefaultSMSMessagesPerThread:I

    const/16 v0, 0x32

    sput v0, Lcom/android/mms/MmsConfig;->mDefaultMMSMessagesPerThread:I

    sput v4, Lcom/android/mms/MmsConfig;->mMinMessageCountPerThread:I

    const/16 v0, 0x1388

    sput v0, Lcom/android/mms/MmsConfig;->mMaxMessageCountPerThread:I

    const/4 v0, 0x7

    sput v0, Lcom/android/mms/MmsConfig;->mMinimumSlideElementDuration:I

    sput-boolean v2, Lcom/android/mms/MmsConfig;->mNotifyWapMMSC:Z

    sput-boolean v1, Lcom/android/mms/MmsConfig;->mAllowAttachAudio:Z

    sput-boolean v1, Lcom/android/mms/MmsConfig;->mEnableMultipartSMS:Z

    sput-boolean v1, Lcom/android/mms/MmsConfig;->mEnableSlideDuration:Z

    sput-boolean v1, Lcom/android/mms/MmsConfig;->mEnableMMSReadReports:Z

    sput-boolean v1, Lcom/android/mms/MmsConfig;->mEnableSMSDeliveryReports:Z

    sput-boolean v1, Lcom/android/mms/MmsConfig;->mEnableMMSDeliveryReports:Z

    const/4 v0, -0x1

    sput v0, Lcom/android/mms/MmsConfig;->mMaxTextLength:I

    const/4 v0, 0x4

    sput v0, Lcom/android/mms/MmsConfig;->mMaxSizeScaleForPendingMmsAllowed:I

    sput-boolean v2, Lcom/android/mms/MmsConfig;->mAliasEnabled:Z

    sput v4, Lcom/android/mms/MmsConfig;->mAliasRuleMinChars:I

    const/16 v0, 0x30

    sput v0, Lcom/android/mms/MmsConfig;->mAliasRuleMaxChars:I

    const/16 v0, 0x28

    sput v0, Lcom/android/mms/MmsConfig;->mMaxSubjectLength:I

    sput-boolean v1, Lcom/android/mms/MmsConfig;->mEnableGroupMms:Z

    const/16 v0, 0x400

    sput v0, Lcom/android/mms/MmsConfig;->mUserSetMmsSizeLimit:I

    const/16 v0, 0xc8

    sput v0, Lcom/android/mms/MmsConfig;->mReceiveMmsSizeLimitFor2G:I

    const/16 v0, 0x190

    sput v0, Lcom/android/mms/MmsConfig;->mReceiveMmsSizeLimitForTD:I

    const/16 v0, 0x4b0

    sput v0, Lcom/android/mms/MmsConfig;->mMaxRestrictedImageHeight:I

    const/16 v0, 0x640

    sput v0, Lcom/android/mms/MmsConfig;->mMaxRestrictedImageWidth:I

    const/16 v0, 0x64

    sput v0, Lcom/android/mms/MmsConfig;->mSmsRecipientLimit:I

    sput-boolean v2, Lcom/android/mms/MmsConfig;->mDeviceStorageFull:Z

    sput-object v3, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    sput-boolean v2, Lcom/android/mms/MmsConfig;->mSlAutoLanuchEnabled:Z

    const-string v0, ""

    sput-object v0, Lcom/android/mms/MmsConfig;->sPicTempPath:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/mms/MmsConfig;->sAudioTempPath:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/mms/MmsConfig;->sVideoTempPath:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/mms/MmsConfig;->sVcardTempPath:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/mms/MmsConfig;->sCalendarTempPath:Ljava/lang/String;

    sput-boolean v1, Lcom/android/mms/MmsConfig;->sSlot1SimExist:Z

    sput-boolean v1, Lcom/android/mms/MmsConfig;->sSlot2SimExist:Z

    sput v2, Lcom/android/mms/MmsConfig;->sSlot1RetryCounter:I

    sput v2, Lcom/android/mms/MmsConfig;->sSlot2RetryCounter:I

    sput-wide v5, Lcom/android/mms/MmsConfig;->sSim1Id:J

    sput-wide v5, Lcom/android/mms/MmsConfig;->sSim2Id:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static appendExtraQueryParameterForConversationDeleteAll(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1
    .param p0    # Landroid/net/Uri;

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0, p0}, Lcom/mediatek/mms/ext/IMmsConfig;->appendExtraQueryParameterForConversationDeleteAll(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static final beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .locals 4
    .param p0    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-eq v0, v2, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :cond_1
    if-eq v0, v2, :cond_2

    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "No start tag found"

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected start tag: found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    return-void
.end method

.method public static getAdjustFontSizeEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableAdjustFontSize()Z

    move-result v0

    return v0
.end method

.method public static getAliasMaxChars()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mAliasRuleMaxChars:I

    return v0
.end method

.method public static getAliasMinChars()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mAliasRuleMinChars:I

    return v0
.end method

.method public static getAllowAttachAudio()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mAllowAttachAudio:Z

    return v0
.end method

.method public static getAudioTempPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/android/mms/MmsConfig;->sAudioTempPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mms/MmsConfig;->sAudioTempPath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/mms/MmsConfig;->sAudioTempPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    sget-object v1, Lcom/android/mms/MmsConfig;->sAudioTempPath:Ljava/lang/String;

    return-object v1
.end method

.method public static getCapturePictureIntent()Landroid/content/Intent;
    .locals 3

    sget-object v1, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v1}, Lcom/mediatek/mms/ext/IMmsConfig;->getCapturePictureIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "output"

    sget-object v2, Lcom/android/mms/TempFileProvider;->SCRAP_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public static getDefaultMMSMessagesPerThread()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mDefaultMMSMessagesPerThread:I

    return v0
.end method

.method public static getDefaultSMSMessagesPerThread()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mDefaultSMSMessagesPerThread:I

    return v0
.end method

.method public static getDeliveryReportAllowed()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableReportAllowed()Z

    move-result v0

    return v0
.end method

.method public static getDeviceStorageFullStatus()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mDeviceStorageFull:Z

    return v0
.end method

.method public static getDialogForUrlEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableDialogForUrl()Z

    move-result v0

    return v0
.end method

.method public static getEmailGateway()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mEmailGateway:Ljava/lang/String;

    return-object v0
.end method

.method public static getFolderModeEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableFolderMode()Z

    move-result v0

    return v0
.end method

.method public static getForwardWithSenderEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableForwardWithSender()Z

    move-result v0

    return v0
.end method

.method public static getGroupMmsEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mEnableGroupMms:Z

    return v0
.end method

.method public static getHttpParams()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mHttpParams:Ljava/lang/String;

    return-object v0
.end method

.method public static getHttpParamsLine1Key()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mHttpParamsLine1Key:Ljava/lang/String;

    return-object v0
.end method

.method public static getHttpSocketTimeout()I
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->getHttpSocketTimeout()I

    move-result v0

    return v0
.end method

.method public static getInitQuickText()Z
    .locals 4

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "InitQuickText"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getIpMessagServiceId(Landroid/content/Context;)I
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/mms/ipmessage/ServiceManager;->getIpMessageServiceId()I

    move-result v0

    return v0
.end method

.method public static getMMSDeliveryReportsEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mEnableMMSDeliveryReports:Z

    return v0
.end method

.method public static getMMSReadReportsEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mEnableMMSReadReports:Z

    return v0
.end method

.method public static getMaxImageHeight()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMaxImageHeight:I

    return v0
.end method

.method public static getMaxImageWidth()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMaxImageWidth:I

    return v0
.end method

.method public static getMaxMessageCountPerThread()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMaxMessageCountPerThread:I

    return v0
.end method

.method public static getMaxMessageSize()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMaxMessageSize:I

    return v0
.end method

.method public static getMaxRestrictedImageHeight()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMaxRestrictedImageHeight:I

    return v0
.end method

.method public static getMaxRestrictedImageWidth()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMaxRestrictedImageWidth:I

    return v0
.end method

.method public static getMaxSizeScaleForPendingMmsAllowed()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMaxSizeScaleForPendingMmsAllowed:I

    return v0
.end method

.method public static getMaxSubjectLength()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMaxSubjectLength:I

    return v0
.end method

.method public static getMaxTextLimit()I
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->getMaxTextLimit()I

    move-result v0

    return v0
.end method

.method public static getMinMessageCountPerThread()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMinMessageCountPerThread:I

    return v0
.end method

.method public static getMinimumSlideElementDuration()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mMinimumSlideElementDuration:I

    return v0
.end method

.method public static getMmsDirMode()Z
    .locals 4

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "CmccMmsUiMode"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getMmsEnabled()Z
    .locals 2

    const/4 v0, 0x1

    sget v1, Lcom/android/mms/MmsConfig;->mMmsEnabled:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getMmsRecipientLimit()I
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->getMmsRecipientLimit()I

    move-result v0

    return v0
.end method

.method public static getMmsRetryPromptIndex()I
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->getMmsRetryPromptIndex()I

    move-result v0

    return v0
.end method

.method public static getMmsRetryScheme()[I
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->getMmsRetryScheme()[I

    move-result-object v0

    return-object v0
.end method

.method public static getMultipartSmsEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mEnableMultipartSMS:Z

    return v0
.end method

.method public static getNotifyWapMMSC()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mNotifyWapMMSC:Z

    return v0
.end method

.method public static getPicTempPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/android/mms/MmsConfig;->sPicTempPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "picture"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mms/MmsConfig;->sPicTempPath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/mms/MmsConfig;->sPicTempPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    sget-object v1, Lcom/android/mms/MmsConfig;->sPicTempPath:Ljava/lang/String;

    return-object v1
.end method

.method public static getPluginMenuIDBase()I
    .locals 1

    const/16 v0, 0x100

    return v0
.end method

.method public static getPreQuickText()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v5

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v5, "PreQuickTextNum"

    const/16 v6, 0x9

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PreQuickText"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v4
.end method

.method public static getReceiveMmsLimitFor2G()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mReceiveMmsSizeLimitFor2G:I

    return v0
.end method

.method public static getReceiveMmsLimitForTD()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mReceiveMmsSizeLimitForTD:I

    return v0
.end method

.method public static getRecipientLimit()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mRecipientLimit:I

    return v0
.end method

.method public static getSIMLongSmsConcatenateEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableSIMLongSmsConcatenate()Z

    move-result v0

    return v0
.end method

.method public static getSIMSmsAtSettingEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableSIMSmsForSetting()Z

    move-result v0

    return v0
.end method

.method public static getSMSDeliveryReportsEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mEnableSMSDeliveryReports:Z

    return v0
.end method

.method public static getShowStorageStatusEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableStorageStatusDisp()Z

    move-result v0

    return v0
.end method

.method public static getSimCardInfo()I
    .locals 4

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "CmccSimCardInfo"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getSlAutoLanuchEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mSlAutoLanuchEnabled:Z

    return v0
.end method

.method public static getSlideDurationEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mEnableSlideDuration:Z

    return v0
.end method

.method public static getSmsEncodingTypeEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableSmsEncodingType()Z

    move-result v0

    return v0
.end method

.method public static getSmsMultiSaveLocationEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableMultiSmsSaveLocation()Z

    move-result v0

    return v0
.end method

.method public static getSmsRecipientLimit()I
    .locals 1

    sget v0, Lcom/android/mms/MmsConfig;->mSmsRecipientLimit:I

    return v0
.end method

.method public static getSmsToMmsTextThreshold()I
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->getSmsToMmsTextThreshold()I

    move-result v0

    return v0
.end method

.method public static getSmsValidityPeriodEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableSmsValidityPeriod()Z

    move-result v0

    return v0
.end method

.method public static getStorageFullToastEnabled()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isEnableStorageFullToast()Z

    move-result v0

    return v0
.end method

.method public static getTransIdEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mTransIdEnabled:Z

    return v0
.end method

.method public static getUaProfTagName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mUaProfTagName:Ljava/lang/String;

    return-object v0
.end method

.method public static getUaProfUrl()Ljava/lang/String;
    .locals 4

    const-string v1, "mms"

    const-string v2, "UAProfileURL"

    sget-object v3, Lcom/android/mms/MmsConfig;->mUaProfUrl:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/mediatek/encapsulation/com/mediatek/custom/EncapsulatedCustomProperties;->getString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUserAgent()Ljava/lang/String;
    .locals 4

    const-string v1, "mms"

    const-string v2, "UserAgent"

    sget-object v3, Lcom/android/mms/MmsConfig;->mUserAgent:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/mediatek/encapsulation/com/mediatek/custom/EncapsulatedCustomProperties;->getString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUserSetMmsSizeLimit(Z)I
    .locals 1
    .param p0    # Z

    const/4 v0, 0x1

    if-ne v0, p0, :cond_0

    sget v0, Lcom/android/mms/MmsConfig;->mUserSetMmsSizeLimit:I

    mul-int/lit16 v0, v0, 0x400

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/android/mms/MmsConfig;->mUserSetMmsSizeLimit:I

    goto :goto_0
.end method

.method public static getVcalendarTempPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/android/mms/MmsConfig;->sCalendarTempPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "calendar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mms/MmsConfig;->sCalendarTempPath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/mms/MmsConfig;->sCalendarTempPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    sget-object v1, Lcom/android/mms/MmsConfig;->sCalendarTempPath:Ljava/lang/String;

    return-object v1
.end method

.method public static getVcardTempPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/android/mms/MmsConfig;->sVcardTempPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "vcard"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mms/MmsConfig;->sVcardTempPath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/mms/MmsConfig;->sVcardTempPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    sget-object v1, Lcom/android/mms/MmsConfig;->sVcardTempPath:Ljava/lang/String;

    return-object v1
.end method

.method public static getVideoTempPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/android/mms/MmsConfig;->sVideoTempPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "video"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mms/MmsConfig;->sVideoTempPath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/mms/MmsConfig;->sVideoTempPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    sget-object v1, Lcom/android/mms/MmsConfig;->sVideoTempPath:Ljava/lang/String;

    return-object v1
.end method

.method public static init(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    const-string v0, "MmsConfig"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mnc/mcc: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "gsm.sim.operator.numeric"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->initPlugin(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->loadMmsSettings(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getIpMessagePlugin(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/IIpMessagePlugin;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/mms/ipmessage/IIpMessagePlugin;->isActualPlugin()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->initializeIpMessageFilePath(Landroid/content/Context;)V

    :cond_0
    invoke-static {p0}, Lcom/android/mms/MmsConfig;->loadSimInfo(Landroid/content/Context;)V

    return-void
.end method

.method private static initPlugin(Landroid/content/Context;)V
    .locals 4
    .param p0    # Landroid/content/Context;

    :try_start_0
    const-class v1, Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    invoke-static {p0, v1, v2}, Lcom/mediatek/encapsulation/com/mediatek/pluginmanager/EncapsulatedPluginManager;->createPluginObject(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/mms/ext/IMmsConfig;

    sput-object v1, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    const-string v1, "MmsConfig"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "operator mMmsConfigPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/util/AndroidException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mediatek/mms/ext/MmsConfigImpl;

    invoke-direct {v1}, Lcom/mediatek/mms/ext/MmsConfigImpl;-><init>()V

    sput-object v1, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    const-string v1, "MmsConfig"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default mMmsConfigPlugin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static initializeIpMessageFilePath(Landroid/content/Context;)V
    .locals 9
    .param p0    # Landroid/content/Context;

    invoke-static {}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardStatus()Z

    move-result v7

    if-eqz v7, :cond_5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "picture"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/android/mms/MmsConfig;->sPicTempPath:Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    sget-object v7, Lcom/android/mms/MmsConfig;->sPicTempPath:Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "audio"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/android/mms/MmsConfig;->sAudioTempPath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    sget-object v7, Lcom/android/mms/MmsConfig;->sAudioTempPath:Ljava/lang/String;

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "video"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/android/mms/MmsConfig;->sVideoTempPath:Ljava/lang/String;

    new-instance v6, Ljava/io/File;

    sget-object v7, Lcom/android/mms/MmsConfig;->sVideoTempPath:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "vcard"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/android/mms/MmsConfig;->sVcardTempPath:Ljava/lang/String;

    new-instance v5, Ljava/io/File;

    sget-object v7, Lcom/android/mms/MmsConfig;->sVcardTempPath:Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getSDCardPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/mediatek/ipmsg/util/IpMessageUtils;->IP_MESSAGE_FILE_PATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "calendar"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/android/mms/MmsConfig;->sCalendarTempPath:Ljava/lang/String;

    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/android/mms/MmsConfig;->sCalendarTempPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    :cond_4
    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getCachePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    :cond_5
    return-void
.end method

.method public static isActivated(Landroid/content/Context;)Z
    .locals 3
    .param p0    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mms/ipmessage/ServiceManager;->serviceIsReady()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/android/mms/MmsConfig;->loadSimInfo(Landroid/content/Context;)V

    sget-boolean v1, Lcom/android/mms/MmsConfig;->sSlot1SimExist:Z

    if-nez v1, :cond_3

    :goto_1
    sget-boolean v1, Lcom/android/mms/MmsConfig;->sSlot2SimExist:Z

    if-eqz v1, :cond_0

    sget-wide v1, Lcom/android/mms/MmsConfig;->sSim2Id:J

    long-to-int v1, v1

    invoke-static {p0, v1}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    sget-wide v1, Lcom/android/mms/MmsConfig;->sSim1Id:J

    long-to-int v1, v1

    invoke-static {p0, v1}, Lcom/android/mms/MmsConfig;->isActivated(Landroid/content/Context;I)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_1
.end method

.method public static isActivated(Landroid/content/Context;I)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/mms/ipmessage/ServiceManager;->serviceIsReady()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mediatek/mms/ipmessage/ServiceManager;->isActivated(I)Z

    move-result v0

    const-string v1, "MmsConfig"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sim "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " activation status:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/encapsulation/MmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isAliasEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->mAliasEnabled:Z

    return v0
.end method

.method public static isAllowRetryForPermanentFail()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isAllowRetryForPermanentFail()Z

    move-result v0

    return v0
.end method

.method public static isNeedExitComposerAfterForward()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isNeedExitComposerAfterForward()Z

    move-result v0

    return v0
.end method

.method public static isRetainRetryIndexWhenInCall()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isRetainRetryIndexWhenInCall()Z

    move-result v0

    return v0
.end method

.method public static isSendExpiredResIfNotificationExpired()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isSendExpiredResIfNotificationExpired()Z

    move-result v0

    return v0
.end method

.method public static isServiceEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/mms/MmsConfig;->loadSimInfo(Landroid/content/Context;)V

    sget-boolean v0, Lcom/android/mms/MmsConfig;->sSlot1SimExist:Z

    if-nez v0, :cond_2

    :cond_0
    sget-boolean v0, Lcom/android/mms/MmsConfig;->sSlot2SimExist:Z

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    sget-wide v0, Lcom/android/mms/MmsConfig;->sSim1Id:J

    long-to-int v0, v0

    invoke-static {p0, v0}, Lcom/android/mms/MmsConfig;->isServiceEnabled(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    sget-wide v0, Lcom/android/mms/MmsConfig;->sSim2Id:J

    long-to-int v0, v0

    invoke-static {p0, v0}, Lcom/android/mms/MmsConfig;->isServiceEnabled(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1
.end method

.method public static isServiceEnabled(Landroid/content/Context;I)Z
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-static {p0}, Lcom/mediatek/ipmsg/util/IpMessageUtils;->getServiceManager(Landroid/content/Context;)Lcom/mediatek/mms/ipmessage/ServiceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/mms/ipmessage/ServiceManager;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public static isShowDraftIcon()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isShowDraftIcon()Z

    move-result v0

    return v0
.end method

.method public static isShowUrlDialog()Z
    .locals 1

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConfig;->isShowUrlDialog()Z

    move-result v0

    return v0
.end method

.method private static loadMmsSettings(Landroid/content/Context;)V
    .locals 13
    .param p0    # Landroid/content/Context;

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v11, 0x7f050004

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    :try_start_0
    const-string v8, "mms_config"

    invoke-static {v4, v8}, Lcom/android/mms/MmsConfig;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-static {v4}, Lcom/android/mms/MmsConfig;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    if-nez v5, :cond_3

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    :goto_1
    const/4 v2, 0x0

    invoke-static {}, Lcom/android/mms/MmsConfig;->getMmsEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    sget-object v8, Lcom/android/mms/MmsConfig;->mUaProfUrl:Ljava/lang/String;

    if-nez v8, :cond_1

    const-string v2, "uaProfUrl"

    :cond_1
    if-eqz v2, :cond_2

    const-string v8, "MmsConfig.loadMmsSettings mms_config.xml missing %s setting"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v2, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "MmsConfig"

    invoke-static {v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    const/4 v8, 0x0

    :try_start_1
    invoke-interface {v4, v8}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x0

    invoke-interface {v4, v8}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v7

    const/4 v6, 0x0

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v8

    const/4 v11, 0x4

    if-ne v8, v11, :cond_4

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    move-result-object v6

    :cond_4
    const-string v8, "MmsConfig"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "tag: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " value: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " - "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "name"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "bool"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    const-string v8, "enabledMMS"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    move v8, v9

    :goto_2
    sput v8, Lcom/android/mms/MmsConfig;->mMmsEnabled:I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v8, "MmsConfig"

    const-string v11, "loadMmsSettings caught "

    invoke-static {v8, v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    goto/16 :goto_1

    :cond_5
    move v8, v10

    goto :goto_2

    :cond_6
    :try_start_3
    const-string v8, "enabledTransID"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mTransIdEnabled:Z
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v8, "MmsConfig"

    const-string v11, "loadMmsSettings caught "

    invoke-static {v8, v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    goto/16 :goto_1

    :cond_7
    :try_start_5
    const-string v8, "enabledNotifyWapMMSC"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mNotifyWapMMSC:Z
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_6
    const-string v8, "MmsConfig"

    const-string v11, "loadMmsSettings caught "

    invoke-static {v8, v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    goto/16 :goto_1

    :cond_8
    :try_start_7
    const-string v8, "aliasEnabled"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mAliasEnabled:Z
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v8

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    throw v8

    :cond_9
    :try_start_8
    const-string v8, "allowAttachAudio"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mAllowAttachAudio:Z

    goto/16 :goto_0

    :cond_a
    const-string v8, "enableMultipartSMS"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mEnableMultipartSMS:Z

    goto/16 :goto_0

    :cond_b
    const-string v8, "enableSlideDuration"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mEnableSlideDuration:Z

    goto/16 :goto_0

    :cond_c
    const-string v8, "enableMMSReadReports"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mEnableMMSReadReports:Z

    goto/16 :goto_0

    :cond_d
    const-string v8, "enableSMSDeliveryReports"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_e

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mEnableSMSDeliveryReports:Z

    goto/16 :goto_0

    :cond_e
    const-string v8, "enableMMSDeliveryReports"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mEnableMMSDeliveryReports:Z

    goto/16 :goto_0

    :cond_f
    const-string v8, "enableGroupMms"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    sput-boolean v8, Lcom/android/mms/MmsConfig;->mEnableGroupMms:Z

    goto/16 :goto_0

    :cond_10
    const-string v8, "int"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_22

    const-string v8, "maxMessageSize"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMaxMessageSize:I

    goto/16 :goto_0

    :cond_11
    const-string v8, "maxImageHeight"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_12

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMaxImageHeight:I

    goto/16 :goto_0

    :cond_12
    const-string v8, "maxImageWidth"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMaxImageWidth:I

    goto/16 :goto_0

    :cond_13
    const-string v8, "maxRestrictedImageHeight"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_14

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMaxRestrictedImageHeight:I

    goto/16 :goto_0

    :cond_14
    const-string v8, "maxRestrictedImageWidth"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_15

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMaxRestrictedImageWidth:I

    goto/16 :goto_0

    :cond_15
    const-string v8, "defaultSMSMessagesPerThread"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_16

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mDefaultSMSMessagesPerThread:I

    goto/16 :goto_0

    :cond_16
    const-string v8, "defaultMMSMessagesPerThread"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_17

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mDefaultMMSMessagesPerThread:I

    goto/16 :goto_0

    :cond_17
    const-string v8, "minMessageCountPerThread"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_18

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMinMessageCountPerThread:I

    goto/16 :goto_0

    :cond_18
    const-string v8, "maxMessageCountPerThread"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_19

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMaxMessageCountPerThread:I

    goto/16 :goto_0

    :cond_19
    const-string v8, "smsToMmsTextThreshold"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1a

    sget-object v8, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v8, v11}, Lcom/mediatek/mms/ext/IMmsConfig;->setSmsToMmsTextThreshold(I)V

    goto/16 :goto_0

    :cond_1a
    const-string v8, "recipientLimit"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1b

    sget-object v8, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v8, v11}, Lcom/mediatek/mms/ext/IMmsConfig;->setMmsRecipientLimit(I)V

    goto/16 :goto_0

    :cond_1b
    const-string v8, "httpSocketTimeout"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1c

    sget-object v8, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v8, v11}, Lcom/mediatek/mms/ext/IMmsConfig;->setHttpSocketTimeout(I)V

    goto/16 :goto_0

    :cond_1c
    const-string v8, "minimumSlideElementDuration"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1d

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMinimumSlideElementDuration:I

    goto/16 :goto_0

    :cond_1d
    const-string v8, "maxSizeScaleForPendingMmsAllowed"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1e

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMaxSizeScaleForPendingMmsAllowed:I

    goto/16 :goto_0

    :cond_1e
    const-string v8, "aliasMinChars"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1f

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mAliasRuleMinChars:I

    goto/16 :goto_0

    :cond_1f
    const-string v8, "aliasMaxChars"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_20

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mAliasRuleMaxChars:I

    goto/16 :goto_0

    :cond_20
    const-string v8, "maxMessageTextSize"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_21

    sget-object v8, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v8, v11}, Lcom/mediatek/mms/ext/IMmsConfig;->setMaxTextLimit(I)V

    goto/16 :goto_0

    :cond_21
    const-string v8, "maxSubjectLength"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/mms/MmsConfig;->mMaxSubjectLength:I

    goto/16 :goto_0

    :cond_22
    const-string v8, "string"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "userAgent"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_23

    sput-object v6, Lcom/android/mms/MmsConfig;->mUserAgent:Ljava/lang/String;

    goto/16 :goto_0

    :cond_23
    const-string v8, "uaProfTagName"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_24

    sput-object v6, Lcom/android/mms/MmsConfig;->mUaProfTagName:Ljava/lang/String;

    goto/16 :goto_0

    :cond_24
    const-string v8, "uaProfUrl"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_25

    sget-object v8, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v8, v6}, Lcom/mediatek/mms/ext/IMmsConfig;->getUAProf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/android/mms/MmsConfig;->mUaProfUrl:Ljava/lang/String;

    goto/16 :goto_0

    :cond_25
    const-string v8, "httpParams"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_26

    sput-object v6, Lcom/android/mms/MmsConfig;->mHttpParams:Ljava/lang/String;

    goto/16 :goto_0

    :cond_26
    const-string v8, "httpParamsLine1Key"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_27

    sput-object v6, Lcom/android/mms/MmsConfig;->mHttpParamsLine1Key:Ljava/lang/String;

    goto/16 :goto_0

    :cond_27
    const-string v8, "emailGatewayNumber"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    sput-object v6, Lcom/android/mms/MmsConfig;->mEmailGateway:Ljava/lang/String;
    :try_end_8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0
.end method

.method private static loadSimInfo(Landroid/content/Context;)V
    .locals 6
    .param p0    # Landroid/content/Context;

    const/4 v5, 0x3

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    sget-wide v0, Lcom/android/mms/MmsConfig;->sSim1Id:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    sget-boolean v0, Lcom/android/mms/MmsConfig;->sSlot1SimExist:Z

    if-eqz v0, :cond_0

    invoke-static {p0, v4}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getIdBySlot(Landroid/content/Context;I)J

    move-result-wide v0

    sput-wide v0, Lcom/android/mms/MmsConfig;->sSim1Id:J

    sget v0, Lcom/android/mms/MmsConfig;->sSlot1RetryCounter:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/mms/MmsConfig;->sSlot1RetryCounter:I

    sget v0, Lcom/android/mms/MmsConfig;->sSlot1RetryCounter:I

    if-ne v0, v5, :cond_0

    sget-wide v0, Lcom/android/mms/MmsConfig;->sSim1Id:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    sput-boolean v4, Lcom/android/mms/MmsConfig;->sSlot1SimExist:Z

    :cond_0
    sget-wide v0, Lcom/android/mms/MmsConfig;->sSim2Id:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    sget-boolean v0, Lcom/android/mms/MmsConfig;->sSlot2SimExist:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->getIdBySlot(Landroid/content/Context;I)J

    move-result-wide v0

    sput-wide v0, Lcom/android/mms/MmsConfig;->sSim2Id:J

    sget v0, Lcom/android/mms/MmsConfig;->sSlot2RetryCounter:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/mms/MmsConfig;->sSlot2RetryCounter:I

    sget v0, Lcom/android/mms/MmsConfig;->sSlot2RetryCounter:I

    if-ne v0, v5, :cond_1

    sget-wide v0, Lcom/android/mms/MmsConfig;->sSim2Id:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    sput-boolean v4, Lcom/android/mms/MmsConfig;->sSlot2SimExist:Z

    :cond_1
    return-void
.end method

.method public static final nextElement(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2
    .param p0    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :cond_1
    return-void
.end method

.method public static printMmsMemStat(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0, p0, p1}, Lcom/mediatek/mms/ext/IMmsConfig;->printMmsMemStat(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static setDeviceStorageFullStatus(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/mms/MmsConfig;->mDeviceStorageFull:Z

    return-void
.end method

.method public static setExtendUrlSpan(Landroid/widget/TextView;)V
    .locals 1
    .param p0    # Landroid/widget/TextView;

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0, p0}, Lcom/mediatek/mms/ext/IMmsConfig;->setExtendUrlSpan(Landroid/widget/TextView;)V

    return-void
.end method

.method public static setInitQuickText(Z)V
    .locals 3
    .param p0    # Z

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "InitQuickText"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static setMmsDirMode(Z)V
    .locals 3
    .param p0    # Z

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "CmccMmsUiMode"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static setPreQuickText([Ljava/lang/String;)V
    .locals 5
    .param p0    # [Ljava/lang/String;

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "PreQuickTextNum"

    array-length v4, p0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v1, 0x0

    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PreQuickText"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aget-object v4, p0, v1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static setSimCardInfo(I)V
    .locals 3
    .param p0    # I

    invoke-static {}, Lcom/android/mms/MmsApp;->getApplication()Lcom/android/mms/MmsApp;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "CmccSimCardInfo"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static setSoSndTimeout(Lorg/apache/http/params/HttpParams;)V
    .locals 1
    .param p0    # Lorg/apache/http/params/HttpParams;

    sget-object v0, Lcom/android/mms/MmsConfig;->mMmsConfigPlugin:Lcom/mediatek/mms/ext/IMmsConfig;

    invoke-interface {v0, p0}, Lcom/mediatek/mms/ext/IMmsConfig;->setSoSndTimeout(Lorg/apache/http/params/HttpParams;)V

    return-void
.end method

.method public static setUserSetMmsSizeLimit(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/mms/MmsConfig;->mUserSetMmsSizeLimit:I

    return-void
.end method
