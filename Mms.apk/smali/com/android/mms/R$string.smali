.class public final Lcom/android/mms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Cancel:I = 0x7f0b0007

.field public static final Insert_sdcard:I = 0x7f0b0001

.field public static final OK:I = 0x7f0b0006

.field public static final actionbar_general_setting:I = 0x7f0b01b0

.field public static final actionbar_mms_setting:I = 0x7f0b01ae

.field public static final actionbar_notification_setting:I = 0x7f0b01af

.field public static final actionbar_sms_setting:I = 0x7f0b01ad

.field public static final add:I = 0x7f0b0200

.field public static final add_attachment:I = 0x7f0b0246

.field public static final add_attachment_activity:I = 0x7f0b0057

.field public static final add_contact_dialog_existing:I = 0x7f0b0087

.field public static final add_contact_dialog_message:I = 0x7f0b0085

.field public static final add_contact_dialog_new:I = 0x7f0b0086

.field public static final add_contacts:I = 0x7f0b0064

.field public static final add_music:I = 0x7f0b0240

.field public static final add_picture:I = 0x7f0b023e

.field public static final add_quick_text_successful:I = 0x7f0b0206

.field public static final add_shortcut:I = 0x7f0b015e

.field public static final add_slide:I = 0x7f0b0239

.field public static final add_slide_hint:I = 0x7f0b023a

.field public static final add_subject:I = 0x7f0b0247

.field public static final add_video:I = 0x7f0b0242

.field public static final adding_attachments:I = 0x7f0b026b

.field public static final adding_attachments_title:I = 0x7f0b026a

.field public static final adding_recipients:I = 0x7f0b0329

.field public static final all_threads:I = 0x7f0b024e

.field public static final allmessage:I = 0x7f0b0105

.field public static final already_have_quick_text:I = 0x7f0b0207

.field public static final anonymous_recipient:I = 0x7f0b0033

.field public static final app_label:I = 0x7f0b0214

.field public static final ask_for_automatically_resize:I = 0x7f0b0042

.field public static final attach_image:I = 0x7f0b02ce

.field public static final attach_record_sound:I = 0x7f0b02d3

.field public static final attach_record_video:I = 0x7f0b02d1

.field public static final attach_ringtone:I = 0x7f0b0053

.field public static final attach_slideshow:I = 0x7f0b02d4

.field public static final attach_sound:I = 0x7f0b02d2

.field public static final attach_take_photo:I = 0x7f0b02cf

.field public static final attach_vcalendar:I = 0x7f0b00b4

.field public static final attach_vcard:I = 0x7f0b00b6

.field public static final attach_video:I = 0x7f0b02d0

.field public static final attachment_audio:I = 0x7f0b032e

.field public static final attachment_picture:I = 0x7f0b0331

.field public static final attachment_slideshow:I = 0x7f0b032f

.field public static final attachment_video:I = 0x7f0b0330

.field public static final bcc_label:I = 0x7f0b02af

.field public static final broadcast_from_to:I = 0x7f0b0032

.field public static final building_slideshow_title:I = 0x7f0b031f

.field public static final by_card:I = 0x7f0b00fb

.field public static final call_assistant:I = 0x7f0b00d4

.field public static final call_callback:I = 0x7f0b00c9

.field public static final call_car:I = 0x7f0b00ca

.field public static final call_company_main:I = 0x7f0b00cb

.field public static final call_custom:I = 0x7f0b00c1

.field public static final call_fax_home:I = 0x7f0b00c6

.field public static final call_fax_work:I = 0x7f0b00c5

.field public static final call_home:I = 0x7f0b00c2

.field public static final call_isdn:I = 0x7f0b00cc

.field public static final call_main:I = 0x7f0b00cd

.field public static final call_mms:I = 0x7f0b00d5

.field public static final call_mobile:I = 0x7f0b00c3

.field public static final call_other:I = 0x7f0b00c8

.field public static final call_other_fax:I = 0x7f0b00ce

.field public static final call_pager:I = 0x7f0b00c7

.field public static final call_radio:I = 0x7f0b00cf

.field public static final call_telex:I = 0x7f0b00d0

.field public static final call_tty_tdd:I = 0x7f0b00d1

.field public static final call_video_call:I = 0x7f0b0100

.field public static final call_work:I = 0x7f0b00c4

.field public static final call_work_mobile:I = 0x7f0b00d2

.field public static final call_work_pager:I = 0x7f0b00d3

.field public static final cannot_add_picture_and_video:I = 0x7f0b0265

.field public static final cannot_add_recipient:I = 0x7f0b0045

.field public static final cannot_add_slide_anymore:I = 0x7f0b0264

.field public static final cannot_forward_drm_obj:I = 0x7f0b0261

.field public static final cannot_get_details:I = 0x7f0b02a7

.field public static final cannot_load_message:I = 0x7f0b0043

.field public static final cannot_play_audio:I = 0x7f0b026c

.field public static final cannot_save_message:I = 0x7f0b0266

.field public static final cannot_send_message:I = 0x7f0b025f

.field public static final cannot_send_message_reason:I = 0x7f0b0260

.field public static final cannot_send_message_reason_no_content:I = 0x7f0b005e

.field public static final cb_default_channel_name:I = 0x7f0b0062

.field public static final cb_message:I = 0x7f0b0061

.field public static final cb_read_message:I = 0x7f0b005f

.field public static final cb_read_message2:I = 0x7f0b0060

.field public static final cdma_not_support:I = 0x7f0b0104

.field public static final cell_broadcast:I = 0x7f0b0209

.field public static final cell_broadcast_settings:I = 0x7f0b020b

.field public static final cell_broadcast_title:I = 0x7f0b020a

.field public static final change:I = 0x7f0b0115

.field public static final change_default_disk:I = 0x7f0b0116

.field public static final change_duration_activity:I = 0x7f0b02fd

.field public static final changeview:I = 0x7f0b00ee

.field public static final chat:I = 0x7f0b00e8

.field public static final chat_aim:I = 0x7f0b00e0

.field public static final chat_gtalk:I = 0x7f0b00e5

.field public static final chat_icq:I = 0x7f0b00e6

.field public static final chat_jabber:I = 0x7f0b00e7

.field public static final chat_msn:I = 0x7f0b00e1

.field public static final chat_qq:I = 0x7f0b00e4

.field public static final chat_setting_saving:I = 0x7f0b019a

.field public static final chat_setting_sending:I = 0x7f0b019c

.field public static final chat_setting_updating:I = 0x7f0b019b

.field public static final chat_skype:I = 0x7f0b00e3

.field public static final chat_yahoo:I = 0x7f0b00e2

.field public static final class_0_message_activity:I = 0x7f0b0301

.field public static final close:I = 0x7f0b01d8

.field public static final compose_title:I = 0x7f0b0040

.field public static final compressing:I = 0x7f0b0251

.field public static final confirm:I = 0x7f0b02e4

.field public static final confirm_clear_search_text:I = 0x7f0b0309

.field public static final confirm_clear_search_title:I = 0x7f0b0308

.field public static final confirm_delete_SIM_message:I = 0x7f0b02a4

.field public static final confirm_delete_all_SIM_messages:I = 0x7f0b02a3

.field public static final confirm_delete_all_conversations:I = 0x7f0b02a0

.field public static final confirm_delete_all_messages:I = 0x7f0b0049

.field public static final confirm_delete_allmessage:I = 0x7f0b0107

.field public static final confirm_delete_allwappush:I = 0x7f0b01ef

.field public static final confirm_delete_conversation:I = 0x7f0b029f

.field public static final confirm_delete_locked_message:I = 0x7f0b02a2

.field public static final confirm_delete_message:I = 0x7f0b02a1

.field public static final confirm_delete_selected_messages:I = 0x7f0b0073

.field public static final confirm_delete_selected_theads:I = 0x7f0b0074

.field public static final confirm_delete_selected_wappush:I = 0x7f0b01f0

.field public static final confirm_dialog_locked_title:I = 0x7f0b029e

.field public static final confirm_dialog_title:I = 0x7f0b029d

.field public static final confirm_download_message:I = 0x7f0b00f8

.field public static final confirm_rate_limit:I = 0x7f0b02ec

.field public static final confirm_restricted_audio:I = 0x7f0b0018

.field public static final confirm_restricted_image:I = 0x7f0b0019

.field public static final confirm_restricted_video:I = 0x7f0b001a

.field public static final contact_address:I = 0x7f0b007e

.field public static final contact_email:I = 0x7f0b007c

.field public static final contact_name:I = 0x7f0b007a

.field public static final contact_organization:I = 0x7f0b007d

.field public static final contact_tel:I = 0x7f0b007b

.field public static final continue_str:I = 0x7f0b016b

.field public static final convert_to_mms:I = 0x7f0b016c

.field public static final convert_to_sms:I = 0x7f0b016d

.field public static final converting_to_picture_message:I = 0x7f0b0262

.field public static final converting_to_text_message:I = 0x7f0b0263

.field public static final copy_email:I = 0x7f0b033f

.field public static final copy_message_text:I = 0x7f0b0233

.field public static final copy_number:I = 0x7f0b0340

.field public static final copy_to_sdcard:I = 0x7f0b02f2

.field public static final copy_to_sdcard_fail:I = 0x7f0b02f4

.field public static final copy_to_sdcard_success:I = 0x7f0b02f3

.field public static final create_new_message:I = 0x7f0b002e

.field public static final cu_subject:I = 0x7f0b0013

.field public static final data_connected_confirm_message:I = 0x7f0b006a

.field public static final data_connected_confirm_title:I = 0x7f0b0069

.field public static final delete:I = 0x7f0b02a6

.field public static final delete_all_confirm:I = 0x7f0b00f4

.field public static final delete_btn_str:I = 0x7f0b0109

.field public static final delete_message:I = 0x7f0b0226

.field public static final delete_successful:I = 0x7f0b0204

.field public static final delete_thread:I = 0x7f0b022c

.field public static final delete_unlocked:I = 0x7f0b02a5

.field public static final delete_unsuccessful:I = 0x7f0b0205

.field public static final deleting:I = 0x7f0b0088

.field public static final delivered_label:I = 0x7f0b0325

.field public static final delivery_header_title:I = 0x7f0b02c4

.field public static final delivery_report_activity:I = 0x7f0b02fb

.field public static final delivery_toast_body:I = 0x7f0b02d7

.field public static final dialog_clear_description:I = 0x7f0b01a2

.field public static final dialog_clear_title:I = 0x7f0b01a1

.field public static final dialog_continue:I = 0x7f0b01e1

.field public static final dialog_email_description:I = 0x7f0b01a0

.field public static final dialog_email_title:I = 0x7f0b019f

.field public static final dialog_save_description:I = 0x7f0b019e

.field public static final dialog_save_title:I = 0x7f0b019d

.field public static final dialog_sms_limit:I = 0x7f0b01e3

.field public static final dialog_wallpaper_chooser_default:I = 0x7f0b01a6

.field public static final dialog_wallpaper_chooser_gallery:I = 0x7f0b01a4

.field public static final dialog_wallpaper_chooser_take:I = 0x7f0b01a5

.field public static final dialog_wallpaper_chooser_wallpapers:I = 0x7f0b01a3

.field public static final dialog_wallpaper_title:I = 0x7f0b01a7

.field public static final disable_notifications_dialog_message:I = 0x7f0b0097

.field public static final discard:I = 0x7f0b0248

.field public static final discard_message:I = 0x7f0b025a

.field public static final discard_message_reason:I = 0x7f0b025b

.field public static final discard_mms_content:I = 0x7f0b01df

.field public static final discard_mms_title:I = 0x7f0b01de

.field public static final discard_slideshow:I = 0x7f0b023b

.field public static final dl_expired_notification:I = 0x7f0b0056

.field public static final dl_failure_notification:I = 0x7f0b02ea

.field public static final done:I = 0x7f0b0235

.field public static final download:I = 0x7f0b022e

.field public static final download_failed_due_to_full_memory:I = 0x7f0b003b

.field public static final download_later:I = 0x7f0b02f0

.field public static final downloading:I = 0x7f0b022f

.field public static final draft_separator:I = 0x7f0b032a

.field public static final draftbox:I = 0x7f0b00eb

.field public static final drm_protected_text:I = 0x7f0b0231

.field public static final duplicated_recipient:I = 0x7f0b01f2

.field public static final duration_not_a_number:I = 0x7f0b0276

.field public static final duration_sec:I = 0x7f0b0270

.field public static final duration_selector_title:I = 0x7f0b0271

.field public static final duration_zero:I = 0x7f0b0277

.field public static final edit:I = 0x7f0b0280

.field public static final edit_slide_activity:I = 0x7f0b02ff

.field public static final edit_slideshow_activity:I = 0x7f0b02fe

.field public static final edit_text_activity:I = 0x7f0b0059

.field public static final email:I = 0x7f0b00db

.field public static final email_custom:I = 0x7f0b00da

.field public static final email_home:I = 0x7f0b00d6

.field public static final email_mobile:I = 0x7f0b00d7

.field public static final email_other:I = 0x7f0b00d9

.field public static final email_work:I = 0x7f0b00d8

.field public static final empty_vcard:I = 0x7f0b00ba

.field public static final enumeration_comma:I = 0x7f0b0333

.field public static final error_add_attachment:I = 0x7f0b0102

.field public static final error_code_label:I = 0x7f0b02ba

.field public static final error_state:I = 0x7f0b0313

.field public static final error_state_text:I = 0x7f0b0314

.field public static final error_unsupported_scheme:I = 0x7f0b0028

.field public static final exceed_message_size_limitation:I = 0x7f0b0254

.field public static final exceed_subject_length_limitation:I = 0x7f0b0096

.field public static final expire_on:I = 0x7f0b0227

.field public static final export:I = 0x7f0b015a

.field public static final export_disk_problem:I = 0x7f0b00ac

.field public static final export_message_empty:I = 0x7f0b00a9

.field public static final export_message_fail:I = 0x7f0b00a6

.field public static final export_message_ongoing:I = 0x7f0b00a1

.field public static final export_message_success:I = 0x7f0b00a3

.field public static final failed_to_add_image:I = 0x7f0b0117

.field public static final failed_to_add_media:I = 0x7f0b0256

.field public static final failed_to_parse_vcard:I = 0x7f0b00b9

.field public static final failed_to_resize_image:I = 0x7f0b0257

.field public static final fdn_check_failure:I = 0x7f0b02e7

.field public static final fdn_enabled:I = 0x7f0b005d

.field public static final file_attachment_common_name:I = 0x7f0b0335

.field public static final file_attachment_contains:I = 0x7f0b0336

.field public static final file_attachment_files:I = 0x7f0b0337

.field public static final file_attachment_import_vcard:I = 0x7f0b00b5

.field public static final file_attachment_vcalendar_name:I = 0x7f0b00b2

.field public static final file_attachment_vcard_name:I = 0x7f0b00b3

.field public static final finish:I = 0x7f0b0114

.field public static final folder_download:I = 0x7f0b00f7

.field public static final folder_recieve_date:I = 0x7f0b00f6

.field public static final folder_recipient:I = 0x7f0b00f5

.field public static final forward_from:I = 0x7f0b00af

.field public static final forward_prefix:I = 0x7f0b0259

.field public static final forward_tips_body:I = 0x7f0b01e5

.field public static final forward_tips_title:I = 0x7f0b01e4

.field public static final from_label:I = 0x7f0b02ad

.field public static final gemini_3g_indic:I = 0x7f0b0009

.field public static final get_icc_sms_capacity_failed:I = 0x7f0b0093

.field public static final has_draft:I = 0x7f0b0220

.field public static final has_invalid_recipient:I = 0x7f0b025c

.field public static final hidden_sender_address:I = 0x7f0b027b

.field public static final icc_sms_total:I = 0x7f0b0092

.field public static final icc_sms_used:I = 0x7f0b0091

.field public static final image_resolution_too_large:I = 0x7f0b001d

.field public static final image_too_large:I = 0x7f0b0041

.field public static final import_message_fail:I = 0x7f0b00a5

.field public static final import_message_list:I = 0x7f0b00a7

.field public static final import_message_list_empty:I = 0x7f0b00a8

.field public static final import_message_ongoing:I = 0x7f0b00a2

.field public static final import_message_success:I = 0x7f0b00a4

.field public static final inbox:I = 0x7f0b00e9

.field public static final inline_subject:I = 0x7f0b0230

.field public static final insert_text_title:I = 0x7f0b033d

.field public static final insufficient_drm_rights:I = 0x7f0b0232

.field public static final invalid_contact_message:I = 0x7f0b00fa

.field public static final invalid_destination:I = 0x7f0b025d

.field public static final invalid_recipient_message:I = 0x7f0b025e

.field public static final invite_friends_to_chat:I = 0x7f0b0157

.field public static final invite_friends_to_ipmsg:I = 0x7f0b0156

.field public static final invite_friends_to_ipmsg_content:I = 0x7f0b0159

.field public static final invite_friends_to_ipmsg_dialog_msg:I = 0x7f0b0158

.field public static final ip_message_activate_message:I = 0x7f0b01cf

.field public static final ip_message_activate_title:I = 0x7f0b01cc

.field public static final ip_message_convert_to_mms_message_for_recipients:I = 0x7f0b0171

.field public static final ip_message_convert_to_mms_message_for_service:I = 0x7f0b016f

.field public static final ip_message_convert_to_sms_message_for_recipients:I = 0x7f0b0172

.field public static final ip_message_convert_to_sms_message_for_service:I = 0x7f0b0170

.field public static final ip_message_current_sim_enabled:I = 0x7f0b01cd

.field public static final ip_message_delete_important:I = 0x7f0b01b5

.field public static final ip_message_enable_message:I = 0x7f0b01d2

.field public static final ip_message_enable_title:I = 0x7f0b01d1

.field public static final ip_message_load_all_message:I = 0x7f0b0178

.field public static final ip_message_switch_sim_button:I = 0x7f0b01d6

.field public static final ip_message_switch_sim_message:I = 0x7f0b01d5

.field public static final ip_message_switch_sim_successfully:I = 0x7f0b01d7

.field public static final ip_message_switch_sim_title:I = 0x7f0b01d4

.field public static final ip_message_typing_text:I = 0x7f0b014d

.field public static final ipmsg_accept:I = 0x7f0b01db

.field public static final ipmsg_activate:I = 0x7f0b012b

.field public static final ipmsg_activate_chat_frequently:I = 0x7f0b012c

.field public static final ipmsg_activate_note:I = 0x7f0b012d

.field public static final ipmsg_cancel:I = 0x7f0b0124

.field public static final ipmsg_cant_save:I = 0x7f0b0152

.field public static final ipmsg_cant_share:I = 0x7f0b0150

.field public static final ipmsg_caption_hint:I = 0x7f0b0123

.field public static final ipmsg_conversation_list_all:I = 0x7f0b0118

.field public static final ipmsg_conversation_list_group_chats:I = 0x7f0b011a

.field public static final ipmsg_conversation_list_important:I = 0x7f0b0119

.field public static final ipmsg_conversation_list_spam:I = 0x7f0b011b

.field public static final ipmsg_convert_to_ipmsg:I = 0x7f0b016e

.field public static final ipmsg_copy:I = 0x7f0b0162

.field public static final ipmsg_create_group_chat:I = 0x7f0b011c

.field public static final ipmsg_dailog_multi_forward:I = 0x7f0b0130

.field public static final ipmsg_delete:I = 0x7f0b0166

.field public static final ipmsg_dialog_send_result:I = 0x7f0b0131

.field public static final ipmsg_dismiss:I = 0x7f0b0128

.field public static final ipmsg_dismiss_content:I = 0x7f0b012e

.field public static final ipmsg_divider_never_online:I = 0x7f0b0139

.field public static final ipmsg_divider_offline:I = 0x7f0b0138

.field public static final ipmsg_divider_online:I = 0x7f0b0137

.field public static final ipmsg_dlg_send_all:I = 0x7f0b0132

.field public static final ipmsg_dlg_send_failed:I = 0x7f0b0134

.field public static final ipmsg_dlg_send_sucess:I = 0x7f0b0133

.field public static final ipmsg_enable:I = 0x7f0b0129

.field public static final ipmsg_enable_chat_frequently:I = 0x7f0b0126

.field public static final ipmsg_enable_notice:I = 0x7f0b012a

.field public static final ipmsg_err_file:I = 0x7f0b017b

.field public static final ipmsg_export_to_sdcard:I = 0x7f0b0161

.field public static final ipmsg_forward_failed:I = 0x7f0b013d

.field public static final ipmsg_forward_norecipients:I = 0x7f0b013e

.field public static final ipmsg_getsim_failed:I = 0x7f0b013b

.field public static final ipmsg_invite:I = 0x7f0b0127

.field public static final ipmsg_invite_chat_frequently:I = 0x7f0b0125

.field public static final ipmsg_ipmessage:I = 0x7f0b014f

.field public static final ipmsg_ipmsg:I = 0x7f0b012f

.field public static final ipmsg_ipmsg_hint:I = 0x7f0b0121

.field public static final ipmsg_mark_as_important:I = 0x7f0b0168

.field public static final ipmsg_mms_convert_to_ipmsg:I = 0x7f0b0174

.field public static final ipmsg_multi_forward_failed_part:I = 0x7f0b01ed

.field public static final ipmsg_multi_forward_no_sim:I = 0x7f0b0136

.field public static final ipmsg_multi_forward_tips_content:I = 0x7f0b01eb

.field public static final ipmsg_multiforward_no_message:I = 0x7f0b013f

.field public static final ipmsg_need_input_recipients:I = 0x7f0b0140

.field public static final ipmsg_no_sdcard:I = 0x7f0b0151

.field public static final ipmsg_reject:I = 0x7f0b01da

.field public static final ipmsg_remove_from_important:I = 0x7f0b0169

.field public static final ipmsg_resend:I = 0x7f0b01dc

.field public static final ipmsg_resend_discard_message:I = 0x7f0b01dd

.field public static final ipmsg_resend_via_mms:I = 0x7f0b0147

.field public static final ipmsg_resend_via_sms:I = 0x7f0b0146

.field public static final ipmsg_retry:I = 0x7f0b0165

.field public static final ipmsg_send_exception:I = 0x7f0b01f8

.field public static final ipmsg_send_invitation:I = 0x7f0b011d

.field public static final ipmsg_send_via_mms:I = 0x7f0b0164

.field public static final ipmsg_send_via_text_msg:I = 0x7f0b0163

.field public static final ipmsg_share:I = 0x7f0b0167

.field public static final ipmsg_share_title:I = 0x7f0b014e

.field public static final ipmsg_sim_status_error:I = 0x7f0b013c

.field public static final ipmsg_sms_convert_to_ipmsg:I = 0x7f0b0173

.field public static final ipmsg_sms_hint:I = 0x7f0b0122

.field public static final ipmsg_try_again:I = 0x7f0b0144

.field public static final ipmsg_try_all_again:I = 0x7f0b0145

.field public static final ipmsg_vcard_file_name:I = 0x7f0b013a

.field public static final keep_mms:I = 0x7f0b0175

.field public static final keep_sms:I = 0x7f0b0176

.field public static final kilobyte:I = 0x7f0b0228

.field public static final layout_bottom:I = 0x7f0b0274

.field public static final layout_selector_title:I = 0x7f0b0272

.field public static final layout_top:I = 0x7f0b0273

.field public static final loading_conversations:I = 0x7f0b020c

.field public static final locked_message_cannot_be_deleted:I = 0x7f0b0044

.field public static final long_press_composer_content:I = 0x7f0b01f7

.field public static final long_press_conversationlist_content:I = 0x7f0b01f6

.field public static final long_press_conversationlist_title:I = 0x7f0b01f5

.field public static final map_custom:I = 0x7f0b00df

.field public static final map_home:I = 0x7f0b00dc

.field public static final map_other:I = 0x7f0b00de

.field public static final map_work:I = 0x7f0b00dd

.field public static final mark_as_read_btn_str:I = 0x7f0b0108

.field public static final mark_as_spam:I = 0x7f0b015b

.field public static final mark_as_spam_tips:I = 0x7f0b015c

.field public static final max_recipients_message:I = 0x7f0b0080

.field public static final max_recipients_title:I = 0x7f0b007f

.field public static final me:I = 0x7f0b0065

.field public static final meet_emoticon_limit:I = 0x7f0b01e9

.field public static final menu_add_address_to_contacts:I = 0x7f0b021a

.field public static final menu_add_to_bookmark:I = 0x7f0b0026

.field public static final menu_add_to_contacts:I = 0x7f0b027a

.field public static final menu_call:I = 0x7f0b021b

.field public static final menu_call_back:I = 0x7f0b0216

.field public static final menu_cell_broadcasts:I = 0x7f0b0319

.field public static final menu_compose_new:I = 0x7f0b0218

.field public static final menu_debug_dump:I = 0x7f0b0318

.field public static final menu_delete:I = 0x7f0b021d

.field public static final menu_delete_all:I = 0x7f0b021c

.field public static final menu_delete_allwappush:I = 0x7f0b01f1

.field public static final menu_delete_messages:I = 0x7f0b02bc

.field public static final menu_edit:I = 0x7f0b02bb

.field public static final menu_forward:I = 0x7f0b022d

.field public static final menu_goto_browser:I = 0x7f0b0025

.field public static final menu_group_participants:I = 0x7f0b0326

.field public static final menu_insert_quick_text:I = 0x7f0b01fe

.field public static final menu_insert_smiley:I = 0x7f0b02f8

.field public static final menu_insert_text_vcard:I = 0x7f0b0079

.field public static final menu_item_send_by:I = 0x7f0b0068

.field public static final menu_item_sim1:I = 0x7f0b006b

.field public static final menu_item_sim2:I = 0x7f0b006c

.field public static final menu_lock:I = 0x7f0b02bd

.field public static final menu_omacp:I = 0x7f0b0030

.field public static final menu_preferences:I = 0x7f0b0219

.field public static final menu_reply:I = 0x7f0b003d

.field public static final menu_retry_sending:I = 0x7f0b004b

.field public static final menu_retry_sending_all:I = 0x7f0b004c

.field public static final menu_search:I = 0x7f0b0317

.field public static final menu_send_email:I = 0x7f0b0217

.field public static final menu_send_sms:I = 0x7f0b002f

.field public static final menu_show_icc_sms_capacity:I = 0x7f0b008f

.field public static final menu_sim_sms:I = 0x7f0b003e

.field public static final menu_undelivered_messages:I = 0x7f0b0031

.field public static final menu_unlock:I = 0x7f0b02be

.field public static final menu_view:I = 0x7f0b021e

.field public static final menu_view_contact:I = 0x7f0b0279

.field public static final menu_wappush:I = 0x7f0b01e7

.field public static final message_class_label:I = 0x7f0b02b9

.field public static final message_count_format:I = 0x7f0b032b

.field public static final message_count_notification:I = 0x7f0b032c

.field public static final message_details_title:I = 0x7f0b02a8

.field public static final message_download_failed_title:I = 0x7f0b02ed

.field public static final message_failed_body:I = 0x7f0b02ef

.field public static final message_font_size_dialog_cancel:I = 0x7f0b0113

.field public static final message_font_size_dialog_title:I = 0x7f0b0112

.field public static final message_open_email_fail:I = 0x7f0b0063

.field public static final message_options:I = 0x7f0b0234

.field public static final message_queued:I = 0x7f0b02e6

.field public static final message_saved_as_draft:I = 0x7f0b0267

.field public static final message_send_failed_title:I = 0x7f0b02ee

.field public static final message_send_read_report:I = 0x7f0b02e5

.field public static final message_size_label:I = 0x7f0b02b4

.field public static final message_timestamp_format:I = 0x7f0b0334

.field public static final message_too_big_for_video:I = 0x7f0b0255

.field public static final message_type_label:I = 0x7f0b02a9

.field public static final messagelist_sender_self:I = 0x7f0b0222

.field public static final mms:I = 0x7f0b024d

.field public static final mms_priority_label:I = 0x7f0b004a

.field public static final mms_too_big_to_download:I = 0x7f0b003c

.field public static final modify_successful:I = 0x7f0b0202

.field public static final modify_unsuccessful:I = 0x7f0b0203

.field public static final more_recipients:I = 0x7f0b0000

.field public static final more_string:I = 0x7f0b033e

.field public static final move_down:I = 0x7f0b0237

.field public static final move_up:I = 0x7f0b0236

.field public static final multi_files:I = 0x7f0b033b

.field public static final multi_forward_failed_all:I = 0x7f0b01ec

.field public static final multi_forward_sim_info:I = 0x7f0b0135

.field public static final multimedia_message:I = 0x7f0b02ab

.field public static final multimedia_notification:I = 0x7f0b02ac

.field public static final multiple_recipients:I = 0x7f0b0046

.field public static final mute_eight_hours:I = 0x7f0b01aa

.field public static final mute_four_hours:I = 0x7f0b01a9

.field public static final mute_one_hour:I = 0x7f0b01a8

.field public static final name_colon:I = 0x7f0b003f

.field public static final new_message:I = 0x7f0b0215

.field public static final nickname:I = 0x7f0b00c0

.field public static final no:I = 0x7f0b027d

.field public static final no_application_response:I = 0x7f0b0103

.field public static final no_conversations:I = 0x7f0b0316

.field public static final no_item_selected:I = 0x7f0b010d

.field public static final no_messages:I = 0x7f0b00ff

.field public static final no_sd_card:I = 0x7f0b00a0

.field public static final no_sdcard_suggestion:I = 0x7f0b01b2

.field public static final no_sim:I = 0x7f0b006f

.field public static final no_sim_1:I = 0x7f0b006d

.field public static final no_sim_2:I = 0x7f0b006e

.field public static final no_sim_card:I = 0x7f0b01cb

.field public static final no_subject:I = 0x7f0b02e8

.field public static final no_subject_view:I = 0x7f0b0221

.field public static final nointernet:I = 0x7f0b01d9

.field public static final note:I = 0x7f0b00bd

.field public static final notification_failed_multiple:I = 0x7f0b02d9

.field public static final notification_failed_multiple_title:I = 0x7f0b02da

.field public static final notification_multiple:I = 0x7f0b01f9

.field public static final notification_multiple_cb:I = 0x7f0b0054

.field public static final notification_multiple_cb_title:I = 0x7f0b0055

.field public static final notification_multiple_title:I = 0x7f0b02d8

.field public static final notification_separator:I = 0x7f0b0332

.field public static final open:I = 0x7f0b00f3

.field public static final open_keyboard_to_compose_message:I = 0x7f0b0250

.field public static final organization:I = 0x7f0b00be

.field public static final outbox:I = 0x7f0b00ea

.field public static final page:I = 0x7f0b00fc

.field public static final parse_vcard:I = 0x7f0b00bb

.field public static final pick_numbers_activity:I = 0x7f0b005a

.field public static final pick_too_many_recipients:I = 0x7f0b0328

.field public static final play:I = 0x7f0b027f

.field public static final play_as_slideshow:I = 0x7f0b00f9

.field public static final please_wait:I = 0x7f0b00b8

.field public static final prefDefault_vibrateWhen:I = 0x7f0b020d

.field public static final prefDefault_vibrate_false:I = 0x7f0b020f

.field public static final prefDefault_vibrate_true:I = 0x7f0b020e

.field public static final prefDialogTitle_vibrateWhen:I = 0x7f0b0324

.field public static final pref_auto_rotation:I = 0x7f0b017e

.field public static final pref_backage_message:I = 0x7f0b0182

.field public static final pref_chat_wallpaper:I = 0x7f0b017d

.field public static final pref_clear_chat_history:I = 0x7f0b0192

.field public static final pref_download_chat_history:I = 0x7f0b018e

.field public static final pref_email_chat_history:I = 0x7f0b0190

.field public static final pref_key_mms_priority:I = 0x7f0b01fd

.field public static final pref_messages_to_save:I = 0x7f0b0298

.field public static final pref_mms_clear_search_history_summary:I = 0x7f0b030b

.field public static final pref_mms_clear_search_history_title:I = 0x7f0b030a

.field public static final pref_mms_settings_title:I = 0x7f0b0285

.field public static final pref_mute:I = 0x7f0b0186

.field public static final pref_notification_settings_title:I = 0x7f0b0284

.field public static final pref_popup_notification:I = 0x7f0b018a

.field public static final pref_restore_message:I = 0x7f0b0184

.field public static final pref_setting_chat:I = 0x7f0b0199

.field public static final pref_setting_general:I = 0x7f0b0198

.field public static final pref_setting_mms:I = 0x7f0b0196

.field public static final pref_setting_notification:I = 0x7f0b0197

.field public static final pref_setting_sms:I = 0x7f0b0195

.field public static final pref_setting_title:I = 0x7f0b0194

.field public static final pref_show_email_address:I = 0x7f0b017f

.field public static final pref_sms_settings_title:I = 0x7f0b0286

.field public static final pref_sms_storage_title:I = 0x7f0b0287

.field public static final pref_summary_auto_delete:I = 0x7f0b028c

.field public static final pref_summary_backup_message:I = 0x7f0b0183

.field public static final pref_summary_delete_limit:I = 0x7f0b028d

.field public static final pref_summary_download_chat_history:I = 0x7f0b018f

.field public static final pref_summary_email_chat_history:I = 0x7f0b0191

.field public static final pref_summary_export_msg:I = 0x7f0b009e

.field public static final pref_summary_import_msg:I = 0x7f0b009c

.field public static final pref_summary_manage_sim_messages:I = 0x7f0b0288

.field public static final pref_summary_message_font_size:I = 0x7f0b0111

.field public static final pref_summary_mms_auto_reply_read_reports:I = 0x7f0b0095

.field public static final pref_summary_mms_auto_retrieval:I = 0x7f0b029a

.field public static final pref_summary_mms_creation_mode:I = 0x7f0b0015

.field public static final pref_summary_mms_delivery_reports:I = 0x7f0b0289

.field public static final pref_summary_mms_enable_to_send_delivery_reports:I = 0x7f0b0003

.field public static final pref_summary_mms_group_mms:I = 0x7f0b0320

.field public static final pref_summary_mms_read_reports:I = 0x7f0b028a

.field public static final pref_summary_mms_retrieval_during_roaming:I = 0x7f0b029c

.field public static final pref_summary_mms_size_limit:I = 0x7f0b0017

.field public static final pref_summary_mute:I = 0x7f0b0187

.field public static final pref_summary_notification_enabled:I = 0x7f0b0296

.field public static final pref_summary_notification_ringtone:I = 0x7f0b0193

.field public static final pref_summary_notification_vibrateWhen:I = 0x7f0b0323

.field public static final pref_summary_popup_notification:I = 0x7f0b018b

.field public static final pref_summary_restore_message:I = 0x7f0b0185

.field public static final pref_summary_show_email_address:I = 0x7f0b0180

.field public static final pref_summary_sms_delivery_reports:I = 0x7f0b028b

.field public static final pref_summary_vibrate:I = 0x7f0b0189

.field public static final pref_summary_wappush_enable:I = 0x7f0b002b

.field public static final pref_summary_wappush_sl_autoloading:I = 0x7f0b002d

.field public static final pref_title_actions_settings:I = 0x7f0b018d

.field public static final pref_title_auto_delete:I = 0x7f0b0292

.field public static final pref_title_backup_restore_settings:I = 0x7f0b0181

.field public static final pref_title_display_preference_settings:I = 0x7f0b017c

.field public static final pref_title_display_settings:I = 0x7f0b018c

.field public static final pref_title_export_msg:I = 0x7f0b009f

.field public static final pref_title_font_size_setting:I = 0x7f0b010f

.field public static final pref_title_import_msg:I = 0x7f0b009d

.field public static final pref_title_io_settings:I = 0x7f0b009b

.field public static final pref_title_long_press_operation_guide:I = 0x7f0b01f3

.field public static final pref_title_manage_sim_messages:I = 0x7f0b028e

.field public static final pref_title_message_font_size:I = 0x7f0b0110

.field public static final pref_title_mms_auto_reply_read_reports:I = 0x7f0b0094

.field public static final pref_title_mms_auto_retrieval:I = 0x7f0b0299

.field public static final pref_title_mms_creation_mode:I = 0x7f0b0014

.field public static final pref_title_mms_delete:I = 0x7f0b0294

.field public static final pref_title_mms_delivery_reports:I = 0x7f0b028f

.field public static final pref_title_mms_enable_to_send_delivery_reports:I = 0x7f0b0002

.field public static final pref_title_mms_group_mms:I = 0x7f0b0321

.field public static final pref_title_mms_read_reports:I = 0x7f0b0290

.field public static final pref_title_mms_retrieval_during_roaming:I = 0x7f0b029b

.field public static final pref_title_mms_size_limit:I = 0x7f0b0016

.field public static final pref_title_notification_enabled:I = 0x7f0b0295

.field public static final pref_title_notification_ringtone:I = 0x7f0b0297

.field public static final pref_title_notification_vibrateWhen:I = 0x7f0b0322

.field public static final pref_title_sms_delete:I = 0x7f0b0293

.field public static final pref_title_sms_delivery_reports:I = 0x7f0b0291

.field public static final pref_title_storage_status:I = 0x7f0b0089

.field public static final pref_title_wappush_enable:I = 0x7f0b002a

.field public static final pref_title_wappush_settings:I = 0x7f0b0029

.field public static final pref_title_wappush_sl_autoloading:I = 0x7f0b002c

.field public static final pref_title_watch_animation:I = 0x7f0b01f4

.field public static final pref_vibrate:I = 0x7f0b0188

.field public static final preferences_title:I = 0x7f0b0282

.field public static final preview:I = 0x7f0b026d

.field public static final preview_slideshow:I = 0x7f0b026e

.field public static final priority_high:I = 0x7f0b02b6

.field public static final priority_label:I = 0x7f0b02b5

.field public static final priority_low:I = 0x7f0b02b8

.field public static final priority_normal:I = 0x7f0b02b7

.field public static final quick_text_editor:I = 0x7f0b0201

.field public static final rate_limit_surpassed:I = 0x7f0b02eb

.field public static final received_header:I = 0x7f0b010c

.field public static final received_label:I = 0x7f0b02b1

.field public static final received_on:I = 0x7f0b0037

.field public static final recipient_label:I = 0x7f0b02cc

.field public static final recipient_list_activity:I = 0x7f0b0327

.field public static final refreshing:I = 0x7f0b021f

.field public static final remove:I = 0x7f0b024b

.field public static final remove_frome_spam:I = 0x7f0b015d

.field public static final remove_music:I = 0x7f0b0241

.field public static final remove_picture:I = 0x7f0b023f

.field public static final remove_slide:I = 0x7f0b0238

.field public static final remove_text:I = 0x7f0b023d

.field public static final remove_video:I = 0x7f0b0243

.field public static final replace:I = 0x7f0b024a

.field public static final replace_image:I = 0x7f0b026f

.field public static final reply_send:I = 0x7f0b010a

.field public static final resize:I = 0x7f0b0047

.field public static final resize_image_error_information:I = 0x7f0b0258

.field public static final restore_default:I = 0x7f0b0283

.field public static final restricted_forward_message:I = 0x7f0b0076

.field public static final restricted_forward_title:I = 0x7f0b0075

.field public static final retrying_dialog_body:I = 0x7f0b004d

.field public static final save:I = 0x7f0b030c

.field public static final save_attachment:I = 0x7f0b0338

.field public static final save_chat_history_failed:I = 0x7f0b01ab

.field public static final save_history_file:I = 0x7f0b01b1

.field public static final save_message_to_sim:I = 0x7f0b0035

.field public static final save_message_to_sim_successful:I = 0x7f0b0038

.field public static final save_message_to_sim_unsuccessful:I = 0x7f0b0039

.field public static final save_multi_attachment_notes:I = 0x7f0b0339

.field public static final save_ringtone:I = 0x7f0b02f5

.field public static final save_single_attachment_notes:I = 0x7f0b033a

.field public static final save_single_supportformat_attachment_notes:I = 0x7f0b033c

.field public static final save_wallpaper_fail:I = 0x7f0b01b4

.field public static final save_wallpaper_success:I = 0x7f0b01b3

.field public static final saved_label:I = 0x7f0b02b2

.field public static final saved_ringtone:I = 0x7f0b02f6

.field public static final saved_ringtone_fail:I = 0x7f0b02f7

.field public static final search:I = 0x7f0b0304

.field public static final search_empty:I = 0x7f0b0306

.field public static final search_hint:I = 0x7f0b0303

.field public static final search_history:I = 0x7f0b0307

.field public static final search_label:I = 0x7f0b0302

.field public static final search_setting_description:I = 0x7f0b0305

.field public static final secs:I = 0x7f0b0278

.field public static final select_all:I = 0x7f0b0072

.field public static final select_audio:I = 0x7f0b02f1

.field public static final select_bottom_text:I = 0x7f0b02d5

.field public static final select_contact_method_activity:I = 0x7f0b005c

.field public static final select_conversations:I = 0x7f0b0315

.field public static final select_different_media:I = 0x7f0b0253

.field public static final select_different_media_type:I = 0x7f0b001c

.field public static final select_layout_activity:I = 0x7f0b0058

.field public static final select_link_title:I = 0x7f0b02f9

.field public static final select_message:I = 0x7f0b0106

.field public static final select_quick_text:I = 0x7f0b01ff

.field public static final select_text:I = 0x7f0b00ad

.field public static final select_top_text:I = 0x7f0b02d6

.field public static final send:I = 0x7f0b024c

.field public static final send_by_sim1:I = 0x7f0b0067

.field public static final send_by_sim2:I = 0x7f0b0066

.field public static final send_chat_history_failed:I = 0x7f0b01ac

.field public static final send_mms:I = 0x7f0b031b

.field public static final send_using_mms_activity:I = 0x7f0b005b

.field public static final sending_message:I = 0x7f0b0312

.field public static final sent_label:I = 0x7f0b02b0

.field public static final sent_on:I = 0x7f0b0036

.field public static final sentbox:I = 0x7f0b00ec

.field public static final service_center_label:I = 0x7f0b0012

.field public static final service_message_not_found:I = 0x7f0b031d

.field public static final service_network_problem:I = 0x7f0b031e

.field public static final service_not_activated:I = 0x7f0b031c

.field public static final set:I = 0x7f0b027e

.field public static final set_service_center_OK:I = 0x7f0b0004

.field public static final set_service_center_fail:I = 0x7f0b0005

.field public static final show_icc_sms_capacity_title:I = 0x7f0b0090

.field public static final sim1_full_title:I = 0x7f0b0070

.field public static final sim2_full_title:I = 0x7f0b0071

.field public static final sim_copy_to_phone_memory:I = 0x7f0b02bf

.field public static final sim_delete:I = 0x7f0b02c0

.field public static final sim_empty:I = 0x7f0b02c3

.field public static final sim_full_body:I = 0x7f0b02dc

.field public static final sim_full_title:I = 0x7f0b02db

.field public static final sim_manage_messages_title:I = 0x7f0b02c1

.field public static final sim_selected_dialog_title:I = 0x7f0b0083

.field public static final sim_selected_dialog_title_for_activate:I = 0x7f0b01ce

.field public static final sim_selected_dialog_title_for_enable:I = 0x7f0b01d3

.field public static final sim_view:I = 0x7f0b02c2

.field public static final simbox:I = 0x7f0b00ed

.field public static final slide_number:I = 0x7f0b02fa

.field public static final slide_show_part:I = 0x7f0b023c

.field public static final slideshow_activity:I = 0x7f0b0300

.field public static final slideshow_details:I = 0x7f0b00f2

.field public static final slideshow_options:I = 0x7f0b031a

.field public static final sms_forward_setting:I = 0x7f0b00b0

.field public static final sms_forward_setting_summary:I = 0x7f0b00b1

.field public static final sms_full_body:I = 0x7f0b02de

.field public static final sms_full_title:I = 0x7f0b02dd

.field public static final sms_input_mode_dialog_title:I = 0x7f0b0099

.field public static final sms_input_mode_summary:I = 0x7f0b009a

.field public static final sms_input_mode_title:I = 0x7f0b0098

.field public static final sms_rejected_body:I = 0x7f0b02e0

.field public static final sms_rejected_title:I = 0x7f0b02df

.field public static final sms_save_location:I = 0x7f0b0008

.field public static final sms_service_center:I = 0x7f0b000a

.field public static final sms_size_limit:I = 0x7f0b01e2

.field public static final sms_validity_period:I = 0x7f0b000b

.field public static final sms_validity_period_12hours:I = 0x7f0b000f

.field public static final sms_validity_period_1day:I = 0x7f0b0010

.field public static final sms_validity_period_1hour:I = 0x7f0b000d

.field public static final sms_validity_period_6hours:I = 0x7f0b000e

.field public static final sms_validity_period_max:I = 0x7f0b0011

.field public static final sms_validity_period_nosetting:I = 0x7f0b000c

.field public static final space_not_enough:I = 0x7f0b00fd

.field public static final space_not_enough_for_audio:I = 0x7f0b00fe

.field public static final status_deferred:I = 0x7f0b004f

.field public static final status_expired:I = 0x7f0b004e

.field public static final status_failed:I = 0x7f0b02c9

.field public static final status_indeterminate:I = 0x7f0b0051

.field public static final status_label:I = 0x7f0b02cd

.field public static final status_none:I = 0x7f0b02c5

.field public static final status_pending:I = 0x7f0b02c6

.field public static final status_read:I = 0x7f0b02c7

.field public static final status_received:I = 0x7f0b02c8

.field public static final status_rejected:I = 0x7f0b02cb

.field public static final status_unreachable:I = 0x7f0b0052

.field public static final status_unread:I = 0x7f0b02ca

.field public static final status_unrecognized:I = 0x7f0b0050

.field public static final storage_dialog_attachments:I = 0x7f0b008c

.field public static final storage_dialog_available_space:I = 0x7f0b008e

.field public static final storage_dialog_database:I = 0x7f0b008d

.field public static final storage_dialog_mms:I = 0x7f0b008a

.field public static final storage_dialog_mms_size:I = 0x7f0b0101

.field public static final storage_dialog_sms:I = 0x7f0b008b

.field public static final storage_limits_activity:I = 0x7f0b02fc

.field public static final storage_limits_message:I = 0x7f0b030e

.field public static final storage_limits_setting:I = 0x7f0b030f

.field public static final storage_limits_setting_dismiss:I = 0x7f0b0310

.field public static final storage_limits_title:I = 0x7f0b030d

.field public static final strFail:I = 0x7f0b0078

.field public static final strOk:I = 0x7f0b0077

.field public static final str_ipmsg_active:I = 0x7f0b01c5

.field public static final str_ipmsg_agree_and_continue:I = 0x7f0b01ca

.field public static final str_ipmsg_allchat_empty:I = 0x7f0b01bf

.field public static final str_ipmsg_cant_share:I = 0x7f0b0148

.field public static final str_ipmsg_download_history_dlg:I = 0x7f0b011e

.field public static final str_ipmsg_emo_large:I = 0x7f0b0211

.field public static final str_ipmsg_emo_normal:I = 0x7f0b0210

.field public static final str_ipmsg_empty_activate_content:I = 0x7f0b01c0

.field public static final str_ipmsg_empty_activate_title:I = 0x7f0b01c1

.field public static final str_ipmsg_failed_title:I = 0x7f0b0143

.field public static final str_ipmsg_file_limit:I = 0x7f0b0149

.field public static final str_ipmsg_group_example_from:I = 0x7f0b01ba

.field public static final str_ipmsg_group_example_subject:I = 0x7f0b01bb

.field public static final str_ipmsg_groupchat_empty:I = 0x7f0b01b9

.field public static final str_ipmsg_important_empty:I = 0x7f0b01bc

.field public static final str_ipmsg_important_example_time:I = 0x7f0b01be

.field public static final str_ipmsg_important_example_title:I = 0x7f0b01bd

.field public static final str_ipmsg_introduction:I = 0x7f0b01c3

.field public static final str_ipmsg_invalid_file_type:I = 0x7f0b0155

.field public static final str_ipmsg_invitation_msg:I = 0x7f0b0212

.field public static final str_ipmsg_invite_later:I = 0x7f0b0213

.field public static final str_ipmsg_no_app:I = 0x7f0b014a

.field public static final str_ipmsg_no_such_file:I = 0x7f0b0153

.field public static final str_ipmsg_not_delivered_title:I = 0x7f0b0142

.field public static final str_ipmsg_over_file_limit:I = 0x7f0b0154

.field public static final str_ipmsg_replace_attach:I = 0x7f0b0179

.field public static final str_ipmsg_replace_attach_msg:I = 0x7f0b017a

.field public static final str_ipmsg_save_file:I = 0x7f0b016a

.field public static final str_ipmsg_service_title:I = 0x7f0b01c2

.field public static final str_ipmsg_spam_empty:I = 0x7f0b01b6

.field public static final str_ipmsg_spam_example_from:I = 0x7f0b01b7

.field public static final str_ipmsg_spam_example_subject:I = 0x7f0b01b8

.field public static final str_ipmsg_term_key:I = 0x7f0b01c9

.field public static final str_ipmsg_term_warn_activate:I = 0x7f0b01c8

.field public static final str_ipmsg_term_warn_welcome:I = 0x7f0b01c7

.field public static final str_ipmsg_today:I = 0x7f0b014b

.field public static final str_ipmsg_unread_format:I = 0x7f0b0141

.field public static final str_ipmsg_welcome_active:I = 0x7f0b01c6

.field public static final str_ipmsg_yesterday:I = 0x7f0b014c

.field public static final string_via:I = 0x7f0b0208

.field public static final subject_hint:I = 0x7f0b0245

.field public static final subject_label:I = 0x7f0b02b3

.field public static final suggested:I = 0x7f0b0084

.field public static final sum_search_networks:I = 0x7f0b01ee

.field public static final switch_ipmsg:I = 0x7f0b0177

.field public static final sync_mms_to_db:I = 0x7f0b010e

.field public static final test_number:I = 0x7f0b01fb

.field public static final test_operator:I = 0x7f0b01fa

.field public static final test_status:I = 0x7f0b01fc

.field public static final text_message:I = 0x7f0b02aa

.field public static final time_now:I = 0x7f0b01c4

.field public static final to_address_label:I = 0x7f0b02ae

.field public static final to_hint:I = 0x7f0b0244

.field public static final to_hint_ipmsg:I = 0x7f0b011f

.field public static final to_label:I = 0x7f0b0048

.field public static final toast_sms_forward:I = 0x7f0b01e0

.field public static final too_many_attachments:I = 0x7f0b0269

.field public static final too_many_recipients:I = 0x7f0b0268

.field public static final too_many_unsent_mms:I = 0x7f0b0311

.field public static final transmission_transiently_failed:I = 0x7f0b003a

.field public static final try_to_send:I = 0x7f0b0281

.field public static final type_audio:I = 0x7f0b02e1

.field public static final type_common_file:I = 0x7f0b00b7

.field public static final type_picture:I = 0x7f0b02e2

.field public static final type_to_compose_text_enter_to_send:I = 0x7f0b024f

.field public static final type_to_compose_text_enter_to_send_ipmsg:I = 0x7f0b0120

.field public static final type_to_compose_text_or_leave_blank:I = 0x7f0b0275

.field public static final type_to_quick_text:I = 0x7f0b01e6

.field public static final type_video:I = 0x7f0b02e3

.field public static final typing:I = 0x7f0b01d0

.field public static final undelivered_msg_dialog_body:I = 0x7f0b022a

.field public static final undelivered_msg_dialog_title:I = 0x7f0b0229

.field public static final undelivered_sms_dialog_body:I = 0x7f0b022b

.field public static final unknown_sender:I = 0x7f0b02e9

.field public static final unselect_all:I = 0x7f0b00ae

.field public static final unsupport_media_type:I = 0x7f0b001b

.field public static final unsupported_media_format:I = 0x7f0b0252

.field public static final vcard_no_details:I = 0x7f0b00bc

.field public static final via_test:I = 0x7f0b010b

.field public static final via_without_time_for_recieve:I = 0x7f0b0082

.field public static final via_without_time_for_send:I = 0x7f0b0081

.field public static final view:I = 0x7f0b0249

.field public static final view_all_location:I = 0x7f0b0160

.field public static final view_all_media:I = 0x7f0b015f

.field public static final view_delivery_report:I = 0x7f0b0225

.field public static final view_message_details:I = 0x7f0b0224

.field public static final view_mms_notification:I = 0x7f0b01ea

.field public static final view_more_conversations:I = 0x7f0b032d

.field public static final view_picture:I = 0x7f0b0034

.field public static final view_slideshow:I = 0x7f0b0223

.field public static final viewer_title_cb:I = 0x7f0b00f1

.field public static final viewer_title_sms:I = 0x7f0b00ef

.field public static final viewer_title_wappush:I = 0x7f0b00f0

.field public static final visit_website:I = 0x7f0b01e8

.field public static final website:I = 0x7f0b00bf

.field public static final whether_export_item:I = 0x7f0b00aa

.field public static final whether_import_item:I = 0x7f0b00ab

.field public static final wp_msg_created_label:I = 0x7f0b001f

.field public static final wp_msg_created_unknown:I = 0x7f0b0020

.field public static final wp_msg_expiration_label:I = 0x7f0b0027

.field public static final wp_msg_priority_high:I = 0x7f0b0024

.field public static final wp_msg_priority_label:I = 0x7f0b0021

.field public static final wp_msg_priority_low:I = 0x7f0b0022

.field public static final wp_msg_priority_medium:I = 0x7f0b0023

.field public static final wp_msg_type:I = 0x7f0b001e

.field public static final yes:I = 0x7f0b027c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
