.class public final Lcom/android/mms/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final ad_emoticon_name:I = 0x7f06001b

.field public static final ad_emoticon_name_ch:I = 0x7f06001d

.field public static final ad_emoticon_name_en:I = 0x7f06001c

.field public static final default_emoticon_name:I = 0x7f060022

.field public static final default_quick_texts:I = 0x7f060003

.field public static final default_smiley_names:I = 0x7f060001

.field public static final default_smiley_texts:I = 0x7f060000

.field public static final dynamic_emoticon_name:I = 0x7f060017

.field public static final dynamic_emoticon_name_ch:I = 0x7f06001a

.field public static final dynamic_emoticon_name_en:I = 0x7f060019

.field public static final emoticon_column:I = 0x7f060014

.field public static final emoticon_name:I = 0x7f060021

.field public static final empty_subject_strings:I = 0x7f060002

.field public static final gift_emoticon_name:I = 0x7f060023

.field public static final large_emoticon_name:I = 0x7f060015

.field public static final large_emoticon_name_ch:I = 0x7f060018

.field public static final large_emoticon_name_en:I = 0x7f060016

.field public static final prefEntries_vibrateWhen:I = 0x7f060029

.field public static final prefValues_vibrateWhen:I = 0x7f060027

.field public static final pref_key_mms_priority_choices:I = 0x7f060008

.field public static final pref_key_mms_priority_values:I = 0x7f060009

.field public static final pref_message_font_size_choices:I = 0x7f060010

.field public static final pref_message_font_size_values:I = 0x7f060011

.field public static final pref_mms_creation_mode_choices:I = 0x7f060004

.field public static final pref_mms_creation_mode_values:I = 0x7f060005

.field public static final pref_mms_size_limit_choices:I = 0x7f060006

.field public static final pref_mms_size_limit_values:I = 0x7f060007

.field public static final pref_mute_choices:I = 0x7f060024

.field public static final pref_mute_values:I = 0x7f060025

.field public static final pref_sms_input_mode_choice:I = 0x7f06000a

.field public static final pref_sms_input_mode_values:I = 0x7f06000b

.field public static final pref_sms_save_location_choices:I = 0x7f06000c

.field public static final pref_sms_save_location_values:I = 0x7f06000d

.field public static final pref_tablet_save_location_values:I = 0x7f060026

.field public static final pref_tablet_sms_save_location_choices:I = 0x7f06000e

.field public static final pref_tablet_sms_save_location_values:I = 0x7f06000f

.field public static final select_dialog_items:I = 0x7f060028

.field public static final share_column:I = 0x7f060013

.field public static final share_string_array:I = 0x7f060012

.field public static final wallpapers:I = 0x7f06002a

.field public static final xm_emoticon_name:I = 0x7f06001e

.field public static final xm_emoticon_name_ch:I = 0x7f060020

.field public static final xm_emoticon_name_en:I = 0x7f06001f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
