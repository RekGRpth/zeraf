.class public final Lcom/android/ex/chips/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final actionbar_message_title:I = 0x7f040000

.field public static final ad_emoticon_flipper:I = 0x7f040001

.field public static final ad_emoticon_grid_item:I = 0x7f040002

.field public static final advanced_checkbox_preference:I = 0x7f040003

.field public static final advanced_editor_preference:I = 0x7f040004

.field public static final alert_dialog_text_entry:I = 0x7f040005

.field public static final alert_dialog_text_view:I = 0x7f040006

.field public static final audio_attachment_view:I = 0x7f040007

.field public static final cbmessage_list_item:I = 0x7f040008

.field public static final chat_select_action_bar:I = 0x7f040009

.field public static final chips_alternate_item:I = 0x7f04000a

.field public static final chips_recipient_dropdown_item:I = 0x7f04000b

.field public static final choose_font_size_item:I = 0x7f04000c

.field public static final class_zero_view_gemini:I = 0x7f04000d

.field public static final compose_message_activity:I = 0x7f04000e

.field public static final confirm_rate_limit_activity:I = 0x7f04000f

.field public static final conversation_empty:I = 0x7f040010

.field public static final conversation_list_actionbar:I = 0x7f040011

.field public static final conversation_list_actionbar2:I = 0x7f040012

.field public static final conversation_list_item:I = 0x7f040013

.field public static final conversation_list_multi_select_actionbar:I = 0x7f040014

.field public static final conversation_list_multi_select_actionbar2:I = 0x7f040015

.field public static final conversation_list_screen:I = 0x7f040016

.field public static final conversation_list_title_drop_down_item:I = 0x7f040017

.field public static final copy_chip_dialog_layout:I = 0x7f040018

.field public static final default_emoticon_flipper:I = 0x7f040019

.field public static final default_emoticon_grid_item:I = 0x7f04001a

.field public static final delete_thread_dialog_view:I = 0x7f04001b

.field public static final delivery_report_activity:I = 0x7f04001c

.field public static final delivery_report_header:I = 0x7f04001d

.field public static final delivery_report_list_item:I = 0x7f04001e

.field public static final dynamic_emoticon_flipper:I = 0x7f04001f

.field public static final dynamic_emoticon_grid_item:I = 0x7f040020

.field public static final edit_slide_activity:I = 0x7f040021

.field public static final edit_slide_duration:I = 0x7f040022

.field public static final emoticon_grid_item:I = 0x7f040023

.field public static final emoticon_panel:I = 0x7f040024

.field public static final emoticon_panel_ipmessage:I = 0x7f040025

.field public static final emoticon_preview:I = 0x7f040026

.field public static final file_attachment_view:I = 0x7f040027

.field public static final folder_mode_actionbar:I = 0x7f040028

.field public static final folder_mode_item:I = 0x7f040029

.field public static final foldermode_main_list_item:I = 0x7f04002a

.field public static final foldermode_mms_header:I = 0x7f04002b

.field public static final foldermode_sms_viewer:I = 0x7f04002c

.field public static final foldermode_smsviewer_title:I = 0x7f04002d

.field public static final folderview_list_item:I = 0x7f04002e

.field public static final folderview_list_screen:I = 0x7f04002f

.field public static final gift_emoticon_flipper:I = 0x7f040030

.field public static final gift_emoticon_grid_item:I = 0x7f040031

.field public static final icon_list_item:I = 0x7f040032

.field public static final image_attachment_view:I = 0x7f040033

.field public static final large_emoticon_flipper:I = 0x7f040034

.field public static final large_emoticon_grid_item:I = 0x7f040035

.field public static final load_all_message:I = 0x7f040036

.field public static final media_controller:I = 0x7f040037

.field public static final message_list_item_recv:I = 0x7f040038

.field public static final message_list_item_recv_ipmsg:I = 0x7f040039

.field public static final message_list_item_send:I = 0x7f04003a

.field public static final message_list_item_send_ipmsg:I = 0x7f04003b

.field public static final mms_chips_recipient_dropdown_item:I = 0x7f04003c

.field public static final mms_downloading_view:I = 0x7f04003d

.field public static final mms_layout_view:I = 0x7f04003e

.field public static final mms_player_activity:I = 0x7f04003f

.field public static final mms_player_activity_item:I = 0x7f040040

.field public static final more_item:I = 0x7f040041

.field public static final msg_dlg_activity:I = 0x7f040042

.field public static final multi_delete_list_actionbar:I = 0x7f040043

.field public static final multi_delete_list_screen:I = 0x7f040044

.field public static final multi_forward_msg:I = 0x7f040045

.field public static final multi_save:I = 0x7f040046

.field public static final multi_save_item:I = 0x7f040047

.field public static final multi_save_list_actionbar:I = 0x7f040048

.field public static final multi_select_simsms_actionbar:I = 0x7f040049

.field public static final normal_emoticon_flipper:I = 0x7f04004a

.field public static final number_picker_dialog:I = 0x7f04004b

.field public static final on_line_divider_item:I = 0x7f04004c

.field public static final playing_audio_info:I = 0x7f04004d

.field public static final playing_page_divider:I = 0x7f04004e

.field public static final quick_text_edit_item:I = 0x7f04004f

.field public static final quick_text_list_item:I = 0x7f040050

.field public static final recipient_filter_item:I = 0x7f040051

.field public static final recipient_list_item:I = 0x7f040052

.field public static final recipients_editor:I = 0x7f040053

.field public static final resend_dialog_item:I = 0x7f040054

.field public static final retry_sending_dialog:I = 0x7f040055

.field public static final search_activity:I = 0x7f040056

.field public static final search_item:I = 0x7f040057

.field public static final select_simcard_dialog_view:I = 0x7f040058

.field public static final select_siminfo_header:I = 0x7f040059

.field public static final setting_list:I = 0x7f04005a

.field public static final share_common_panel:I = 0x7f04005b

.field public static final share_flipper:I = 0x7f04005c

.field public static final share_grid_common_item:I = 0x7f04005d

.field public static final share_grid_item:I = 0x7f04005e

.field public static final share_panel:I = 0x7f04005f

.field public static final sim_divider_item:I = 0x7f040060

.field public static final sim_list:I = 0x7f040061

.field public static final sim_selector:I = 0x7f040062

.field public static final siminfo_seleted_framelayout:I = 0x7f040063

.field public static final slideshow:I = 0x7f040064

.field public static final slideshow_attachment_view:I = 0x7f040065

.field public static final slideshow_attachment_view_portrait:I = 0x7f040066

.field public static final slideshow_edit_item:I = 0x7f040067

.field public static final smiley_menu_item:I = 0x7f040068

.field public static final sms_template_edit_activity:I = 0x7f040069

.field public static final status_bar_sms_rejected:I = 0x7f04006a

.field public static final time_divider_item:I = 0x7f04006b

.field public static final unread_divider_item:I = 0x7f04006c

.field public static final video_attachment_view:I = 0x7f04006d

.field public static final wallpaper_chooser:I = 0x7f04006e

.field public static final wallpaper_chooser_base:I = 0x7f04006f

.field public static final wallpaper_chooser_gridview_dialog:I = 0x7f040070

.field public static final wallpaper_item:I = 0x7f040071

.field public static final wallpaper_item_each:I = 0x7f040072

.field public static final widget:I = 0x7f040073

.field public static final widget_conversation:I = 0x7f040074

.field public static final widget_loading:I = 0x7f040075

.field public static final wp_message_activity:I = 0x7f040076

.field public static final wp_message_list_item:I = 0x7f040077

.field public static final xm_emoticon_flipper:I = 0x7f040078

.field public static final xm_emoticon_grid_item:I = 0x7f040079


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
