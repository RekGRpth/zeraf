.class Lcom/android/ex/chips/MTKRecipientEditTextView$9;
.super Ljava/lang/Object;
.source "MTKRecipientEditTextView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/ex/chips/MTKRecipientEditTextView;->registerGlobalLayoutListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;


# direct methods
.method constructor <init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    const/4 v0, 0x1

    const-string v1, "RecipientEditTextView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onGlobalLayout] current view width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", line count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLineCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3400(Lcom/android/ex/chips/MTKRecipientEditTextView;)I

    move-result v1

    iget-object v2, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3500(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    :goto_2
    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3700(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$9;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3600(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    goto :goto_2
.end method
