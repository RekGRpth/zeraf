.class Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;
.super Ljava/lang/Object;
.source "MTKRecipientEditTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/MTKRecipientEditTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecipientChipProcessor"
.end annotation


# instance fields
.field private mChips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/ex/chips/RecipientChip;",
            ">;"
        }
    .end annotation
.end field

.field private mSpanEnd:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSpanFlags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSpanStart:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;


# direct methods
.method public constructor <init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V
    .locals 1

    iput-object p1, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mChips:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanStart:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanEnd:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanFlags:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addChipsBackWithoutNotification(I)V
    .locals 8
    .param p1    # I

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mChips:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v5, "RecipientEditTextView"

    const-string v6, "[addChipsBackWithoutNotification]"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    new-instance v3, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {v3, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->initWatcherProcessor()V

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->removeSpanWatchers()V

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_5

    add-int/lit8 v5, v2, -0x1

    if-ne v4, v5, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->addSpanWatchers()V

    :cond_2
    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    iget-object v6, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v5}, Landroid/view/View;->requestLayout()V

    :cond_3
    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanStart:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int v1, v5, p1

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanEnd:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int v0, v5, p1

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v5, v1, v0}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3800(Lcom/android/ex/chips/MTKRecipientEditTextView;II)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v6

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanFlags:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v6, v7, v1, v0, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    iget-object v5, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3100(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    goto/16 :goto_0
.end method

.method public removeChipsWithoutNotification(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSortedRecipients()[Lcom/android/ex/chips/RecipientChip;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v3, v0

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "RecipientEditTextView"

    const-string v4, "[removeChipsWithoutNotification]"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    new-instance v1, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-direct {v1, v3}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;-><init>(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    invoke-virtual {v1}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->initWatcherProcessor()V

    invoke-virtual {v1}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->removeSpanWatchers()V

    move v2, p1

    :goto_1
    if-ge v2, p2, :cond_2

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mChips:Ljava/util/ArrayList;

    aget-object v4, v0, v2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanStart:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    aget-object v5, v0, v2

    invoke-static {v4, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$2000(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanEnd:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    aget-object v5, v0, v2

    invoke-static {v4, v5}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$2100(Lcom/android/ex/chips/MTKRecipientEditTextView;Lcom/android/ex/chips/RecipientChip;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->mSpanFlags:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v4}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v4

    aget-object v5, v0, v2

    invoke-interface {v4, v5}, Landroid/text/Spannable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-virtual {v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->getSpannable()Landroid/text/Spannable;

    move-result-object v3

    aget-object v4, v0, v2

    invoke-interface {v3, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/android/ex/chips/MTKRecipientEditTextView$watcherProcessor;->addSpanWatchers()V

    :cond_3
    iget-object v3, p0, Lcom/android/ex/chips/MTKRecipientEditTextView$RecipientChipProcessor;->this$0:Lcom/android/ex/chips/MTKRecipientEditTextView;

    invoke-static {v3}, Lcom/android/ex/chips/MTKRecipientEditTextView;->access$3100(Lcom/android/ex/chips/MTKRecipientEditTextView;)V

    goto :goto_0
.end method
