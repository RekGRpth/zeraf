.class public Lcom/android/videoeditor/service/MovieOverlay;
.super Ljava/lang/Object;
.source "MovieOverlay.java"


# static fields
.field private static final KEY_SUBTITLE:Ljava/lang/String; = "subtitle"

.field private static final KEY_TITLE:Ljava/lang/String; = "title"

.field private static final KEY_TYPE:Ljava/lang/String; = "type"

.field public static final OVERLAY_TYPE_BOTTOM_1:I = 0x1

.field public static final OVERLAY_TYPE_BOTTOM_2:I = 0x3

.field public static final OVERLAY_TYPE_CENTER_1:I = 0x0

.field public static final OVERLAY_TYPE_CENTER_2:I = 0x2


# instance fields
.field private mAppDurationMs:J

.field private mAppStartTimeMs:J

.field private mDurationMs:J

.field private mStartTimeMs:J

.field private mSubtitle:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mType:I

.field private final mUniqueId:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/videoeditor/service/MovieOverlay;-><init>(Landroid/media/videoeditor/Overlay;)V

    return-void
.end method

.method constructor <init>(Landroid/media/videoeditor/Overlay;)V
    .locals 3
    .param p1    # Landroid/media/videoeditor/Overlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mUniqueId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mStartTimeMs:J

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mAppStartTimeMs:J

    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getDuration()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mDurationMs:J

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mAppDurationMs:J

    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getUserAttributes()Ljava/util/Map;

    move-result-object v0

    const-string v1, "title"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mTitle:Ljava/lang/String;

    const-string v1, "subtitle"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mSubtitle:Ljava/lang/String;

    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mType:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mUniqueId:Ljava/lang/String;

    iput-wide p2, p0, Lcom/android/videoeditor/service/MovieOverlay;->mStartTimeMs:J

    iput-wide p2, p0, Lcom/android/videoeditor/service/MovieOverlay;->mAppStartTimeMs:J

    iput-wide p4, p0, Lcom/android/videoeditor/service/MovieOverlay;->mDurationMs:J

    iput-wide p4, p0, Lcom/android/videoeditor/service/MovieOverlay;->mAppDurationMs:J

    iput-object p6, p0, Lcom/android/videoeditor/service/MovieOverlay;->mTitle:Ljava/lang/String;

    iput-object p7, p0, Lcom/android/videoeditor/service/MovieOverlay;->mSubtitle:Ljava/lang/String;

    iput p8, p0, Lcom/android/videoeditor/service/MovieOverlay;->mType:I

    return-void
.end method

.method public static buildUserAttributes(ILjava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "type"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "subtitle"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getAttributeType(Ljava/lang/String;)Ljava/lang/Class;
    .locals 1
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    const-string v0, "type"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Ljava/lang/Integer;

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static getSubtitle(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/os/Bundle;

    const-string v0, "subtitle"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTitle(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/os/Bundle;

    const-string v0, "title"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getType(Landroid/os/Bundle;)I
    .locals 1
    .param p0    # Landroid/os/Bundle;

    const-string v0, "type"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public buildUserAttributes()Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "type"

    iget v2, p0, Lcom/android/videoeditor/service/MovieOverlay;->mType:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "title"

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieOverlay;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "subtitle"

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieOverlay;->mSubtitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/videoeditor/service/MovieOverlay;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mUniqueId:Ljava/lang/String;

    check-cast p1, Lcom/android/videoeditor/service/MovieOverlay;

    iget-object v1, p1, Lcom/android/videoeditor/service/MovieOverlay;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAppDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mAppDurationMs:J

    return-wide v0
.end method

.method public getAppStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mAppStartTimeMs:J

    return-wide v0
.end method

.method getDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mDurationMs:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mUniqueId:Ljava/lang/String;

    return-object v0
.end method

.method getStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mStartTimeMs:J

    return-wide v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mSubtitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mType:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public setAppDuration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mAppDurationMs:J

    return-void
.end method

.method public setAppStartTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mAppStartTimeMs:J

    return-void
.end method

.method setDuration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mDurationMs:J

    return-void
.end method

.method setStartTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieOverlay;->mStartTimeMs:J

    return-void
.end method

.method updateUserAttributes(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mType:I

    const-string v0, "title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mTitle:Ljava/lang/String;

    const-string v0, "subtitle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/videoeditor/service/MovieOverlay;->mSubtitle:Ljava/lang/String;

    return-void
.end method
