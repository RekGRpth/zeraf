.class public Lcom/android/videoeditor/service/MovieAudioTrack;
.super Ljava/lang/Object;
.source "MovieAudioTrack.java"


# instance fields
.field private mAppIsDuckingEnabled:Z

.field private mAppLoop:Z

.field private mAppMuted:Z

.field private mAppStartTimeMs:J

.field private mAppVolumePercent:I

.field private final mAudioBitrate:I

.field private final mAudioChannels:I

.field private final mAudioSamplingFrequency:I

.field private final mAudioType:I

.field private mBeginBoundaryTimeMs:J

.field private final mDurationMs:J

.field private mEndBoundaryTimeMs:J

.field private final mFilename:Ljava/lang/String;

.field private mIsDuckingEnabled:Z

.field private mLoop:Z

.field private mMuted:Z

.field private final mRawResourceId:I

.field private mStartTimeMs:J

.field private mTimelineDurationMs:J

.field private final mUniqueId:Ljava/lang/String;

.field private mVolumePercent:I

.field private mWaveformData:Landroid/media/videoeditor/WaveformData;


# direct methods
.method private constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    check-cast v0, Landroid/media/videoeditor/AudioTrack;

    invoke-direct {p0, v0}, Lcom/android/videoeditor/service/MovieAudioTrack;-><init>(Landroid/media/videoeditor/AudioTrack;)V

    return-void
.end method

.method constructor <init>(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mUniqueId:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mFilename:Ljava/lang/String;

    iput p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mRawResourceId:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mStartTimeMs:J

    iput-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppStartTimeMs:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mDurationMs:J

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mStartTimeMs:J

    iput-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mBeginBoundaryTimeMs:J

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mDurationMs:J

    iput-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mEndBoundaryTimeMs:J

    iput v2, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioChannels:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioType:I

    iput v2, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioBitrate:I

    iput v2, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioSamplingFrequency:I

    const/16 v0, 0x64

    iput v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mVolumePercent:I

    iput v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppVolumePercent:I

    iput-boolean v2, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mMuted:Z

    iput-boolean v2, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppMuted:Z

    iput-boolean v4, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mLoop:Z

    iput-boolean v4, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppLoop:Z

    iput-boolean v4, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mIsDuckingEnabled:Z

    iput-boolean v4, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppIsDuckingEnabled:Z

    iput-object v3, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mEndBoundaryTimeMs:J

    iget-wide v2, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mBeginBoundaryTimeMs:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mTimelineDurationMs:J

    return-void
.end method

.method constructor <init>(Landroid/media/videoeditor/AudioTrack;)V
    .locals 5
    .param p1    # Landroid/media/videoeditor/AudioTrack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mUniqueId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getFilename()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mFilename:Ljava/lang/String;

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mRawResourceId:I

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getStartTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mStartTimeMs:J

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppStartTimeMs:J

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getDuration()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mDurationMs:J

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getBoundaryBeginTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mBeginBoundaryTimeMs:J

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getBoundaryEndTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mEndBoundaryTimeMs:J

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getAudioChannels()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioChannels:I

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getAudioType()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioType:I

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getAudioBitrate()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioBitrate:I

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getAudioSamplingFrequency()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioSamplingFrequency:I

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getVolume()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mVolumePercent:I

    iput v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppVolumePercent:I

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->isMuted()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mMuted:Z

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppMuted:Z

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->isLooping()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mLoop:Z

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppLoop:Z

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->isDuckingEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mIsDuckingEnabled:Z

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppIsDuckingEnabled:Z

    :try_start_0
    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getWaveformData()Landroid/media/videoeditor/WaveformData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mWaveformData:Landroid/media/videoeditor/WaveformData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-wide v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mEndBoundaryTimeMs:J

    iget-wide v3, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mBeginBoundaryTimeMs:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mTimelineDurationMs:J

    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    goto :goto_0
.end method


# virtual methods
.method public enableAppDucking(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppIsDuckingEnabled:Z

    return-void
.end method

.method public enableAppLoop(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppLoop:Z

    return-void
.end method

.method enableDucking(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mIsDuckingEnabled:Z

    return-void
.end method

.method enableLoop(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mLoop:Z

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/videoeditor/service/MovieAudioTrack;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mUniqueId:Ljava/lang/String;

    check-cast p1, Lcom/android/videoeditor/service/MovieAudioTrack;

    iget-object v1, p1, Lcom/android/videoeditor/service/MovieAudioTrack;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAppStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppStartTimeMs:J

    return-wide v0
.end method

.method public getAppVolume()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppVolumePercent:I

    return v0
.end method

.method public getAudioBitrate()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioBitrate:I

    return v0
.end method

.method public getAudioChannels()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioChannels:I

    return v0
.end method

.method public getAudioSamplingFrequency()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioSamplingFrequency:I

    return v0
.end method

.method public getAudioType()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAudioType:I

    return v0
.end method

.method public getBoundaryBeginTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mBeginBoundaryTimeMs:J

    return-wide v0
.end method

.method public getBoundaryEndTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mEndBoundaryTimeMs:J

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mDurationMs:J

    return-wide v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mUniqueId:Ljava/lang/String;

    return-object v0
.end method

.method public getRawResourceId()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mRawResourceId:I

    return v0
.end method

.method public getStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mStartTimeMs:J

    return-wide v0
.end method

.method public getTimelineDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mTimelineDurationMs:J

    return-wide v0
.end method

.method getVolume()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mVolumePercent:I

    return v0
.end method

.method public getWaveformData()Landroid/media/videoeditor/WaveformData;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isAppDuckingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppIsDuckingEnabled:Z

    return v0
.end method

.method public isAppLooping()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppLoop:Z

    return v0
.end method

.method public isAppMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppMuted:Z

    return v0
.end method

.method isDuckingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mIsDuckingEnabled:Z

    return v0
.end method

.method isLooping()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mLoop:Z

    return v0
.end method

.method isMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mMuted:Z

    return v0
.end method

.method public setAppMute(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppMuted:Z

    return-void
.end method

.method public setAppStartTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppStartTimeMs:J

    return-void
.end method

.method public setAppVolume(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mAppVolumePercent:I

    return-void
.end method

.method setExtractBoundaries(JJ)V
    .locals 4
    .param p1    # J
    .param p3    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mBeginBoundaryTimeMs:J

    iput-wide p3, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mEndBoundaryTimeMs:J

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mEndBoundaryTimeMs:J

    iget-wide v2, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mBeginBoundaryTimeMs:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mTimelineDurationMs:J

    return-void
.end method

.method setMute(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mMuted:Z

    return-void
.end method

.method setStartTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mStartTimeMs:J

    return-void
.end method

.method setVolume(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mVolumePercent:I

    return-void
.end method

.method setWaveformData(Landroid/media/videoeditor/WaveformData;)V
    .locals 0
    .param p1    # Landroid/media/videoeditor/WaveformData;

    iput-object p1, p0, Lcom/android/videoeditor/service/MovieAudioTrack;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    return-void
.end method
