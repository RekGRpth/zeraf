.class public Lcom/android/videoeditor/service/ApiService;
.super Landroid/app/Service;
.source "ApiService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/videoeditor/service/ApiService$IntentProcessor;,
        Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;
    }
.end annotation


# static fields
.field public static final ACTION_NO_FRAME_UPDATE:I = 0x67

.field public static final ACTION_UPDATE_FRAME:I = 0x66

.field private static final DEFAULT_AUDIO_TRACK_VOLUME:I = 0x32

.field private static final DUCK_THRESHOLD:I = 0x14

.field private static final DUCK_TRACK_VOLUME:I = 0x41

.field private static final OP_AUDIO_TRACK_ADD:I = 0x1f4

.field private static final OP_AUDIO_TRACK_EXTRACT_AUDIO_WAVEFORM:I = 0x1fc

.field private static final OP_AUDIO_TRACK_EXTRACT_AUDIO_WAVEFORM_STATUS:I = 0x1fd

.field private static final OP_AUDIO_TRACK_REMOVE:I = 0x1f5

.field private static final OP_AUDIO_TRACK_SET_BOUNDARIES:I = 0x1f9

.field private static final OP_AUDIO_TRACK_SET_DUCK:I = 0x1fb

.field private static final OP_AUDIO_TRACK_SET_LOOP:I = 0x1fa

.field private static final OP_AUDIO_TRACK_SET_MUTE:I = 0x1f7

.field private static final OP_AUDIO_TRACK_SET_VOLUME:I = 0x1f6

.field private static final OP_EFFECT_ADD_COLOR:I = 0xc8

.field private static final OP_EFFECT_ADD_IMAGE_KEN_BURNS:I = 0xc9

.field private static final OP_EFFECT_REMOVE:I = 0xca

.field private static final OP_MEDIA_ITEM_ADD_IMAGE_URI:I = 0x65

.field private static final OP_MEDIA_ITEM_ADD_VIDEO_URI:I = 0x64

.field private static final OP_MEDIA_ITEM_EXTRACT_AUDIO_WAVEFORM:I = 0x6d

.field private static final OP_MEDIA_ITEM_EXTRACT_AUDIO_WAVEFORM_STATUS:I = 0x6e

.field private static final OP_MEDIA_ITEM_GET_THUMBNAILS:I = 0x70

.field private static final OP_MEDIA_ITEM_LOAD:I = 0x71

.field private static final OP_MEDIA_ITEM_LOAD_STATUS:I = 0x72

.field private static final OP_MEDIA_ITEM_MOVE:I = 0x66

.field private static final OP_MEDIA_ITEM_REMOVE:I = 0x67

.field private static final OP_MEDIA_ITEM_SET_BOUNDARIES:I = 0x6a

.field private static final OP_MEDIA_ITEM_SET_DURATION:I = 0x69

.field private static final OP_MEDIA_ITEM_SET_MUTE:I = 0x6c

.field private static final OP_MEDIA_ITEM_SET_RENDERING_MODE:I = 0x68

.field private static final OP_MEDIA_ITEM_SET_VOLUME:I = 0x6b

.field private static final OP_OVERLAY_ADD:I = 0x190

.field private static final OP_OVERLAY_REMOVE:I = 0x191

.field private static final OP_OVERLAY_SET_ATTRIBUTES:I = 0x194

.field private static final OP_OVERLAY_SET_DURATION:I = 0x193

.field private static final OP_OVERLAY_SET_START_TIME:I = 0x192

.field private static final OP_TRANSITION_GET_THUMBNAIL:I = 0x132

.field private static final OP_TRANSITION_INSERT_ALPHA:I = 0x12c

.field private static final OP_TRANSITION_INSERT_CROSSFADE:I = 0x12d

.field private static final OP_TRANSITION_INSERT_FADE_BLACK:I = 0x12e

.field private static final OP_TRANSITION_INSERT_SLIDING:I = 0x12f

.field private static final OP_TRANSITION_REMOVE:I = 0x130

.field private static final OP_TRANSITION_SET_DURATION:I = 0x131

.field private static final OP_VIDEO_EDITOR_APPLY_THEME:I = 0xb

.field private static final OP_VIDEO_EDITOR_CANCEL_EXPORT:I = 0x5

.field private static final OP_VIDEO_EDITOR_CREATE:I = 0x1

.field private static final OP_VIDEO_EDITOR_DELETE:I = 0x9

.field private static final OP_VIDEO_EDITOR_EXPORT:I = 0x4

.field private static final OP_VIDEO_EDITOR_EXPORT_STATUS:I = 0x6

.field private static final OP_VIDEO_EDITOR_GENERATE_PREVIEW_PROGRESS:I = 0xc

.field private static final OP_VIDEO_EDITOR_LOAD:I = 0x2

.field private static final OP_VIDEO_EDITOR_LOAD_PROJECTS:I = 0xd

.field private static final OP_VIDEO_EDITOR_RELEASE:I = 0x8

.field private static final OP_VIDEO_EDITOR_SAVE:I = 0x3

.field private static final OP_VIDEO_EDITOR_SET_ASPECT_RATIO:I = 0xa

.field private static final PARAM_ACTION:Ljava/lang/String; = "action"

.field private static final PARAM_ASPECT_RATIO:Ljava/lang/String; = "aspect_ratio"

.field private static final PARAM_ATTRIBUTES:Ljava/lang/String; = "attributes"

.field private static final PARAM_BEGIN_BOUNDARY:Ljava/lang/String; = "b_boundary"

.field private static final PARAM_BITRATE:Ljava/lang/String; = "bitrate"

.field private static final PARAM_CANCELLED:Ljava/lang/String; = "cancelled"

.field private static final PARAM_COUNT:Ljava/lang/String; = "count"

.field private static final PARAM_DUCK:Ljava/lang/String; = "duck"

.field private static final PARAM_DURATION:Ljava/lang/String; = "duration"

.field private static final PARAM_EFFECT_PARAM:Ljava/lang/String; = "e_param"

.field private static final PARAM_EFFECT_TYPE:Ljava/lang/String; = "e_type"

.field private static final PARAM_END_BOUNDARY:Ljava/lang/String; = "e_boundary"

.field private static final PARAM_END_TIME:Ljava/lang/String; = "e_time"

.field private static final PARAM_EXCEPTION:Ljava/lang/String; = "ex"

.field private static final PARAM_FILENAME:Ljava/lang/String; = "filename"

.field private static final PARAM_HEIGHT:Ljava/lang/String; = "height"

.field private static final PARAM_INDICES:Ljava/lang/String; = "indices"

.field private static final PARAM_INTENT:Ljava/lang/String; = "req_intent"

.field private static final PARAM_LOOP:Ljava/lang/String; = "loop"

.field private static final PARAM_MEDIA_ITEM_END_RECT:Ljava/lang/String; = "end_rect"

.field private static final PARAM_MEDIA_ITEM_RENDERING_MODE:Ljava/lang/String; = "rm"

.field private static final PARAM_MEDIA_ITEM_START_RECT:Ljava/lang/String; = "start_rect"

.field private static final PARAM_MOVIES_FILENAMES:Ljava/lang/String; = "movies"

.field private static final PARAM_MOVIE_URI:Ljava/lang/String; = "uri"

.field private static final PARAM_MUTE:Ljava/lang/String; = "mute"

.field private static final PARAM_OP:Ljava/lang/String; = "op"

.field private static final PARAM_PHOTOS_FILENAMES:Ljava/lang/String; = "images"

.field private static final PARAM_PROGRESS_VALUE:Ljava/lang/String; = "prog_value"

.field private static final PARAM_PROJECT_NAME:Ljava/lang/String; = "name"

.field private static final PARAM_PROJECT_PATH:Ljava/lang/String; = "project"

.field private static final PARAM_RELATIVE_STORYBOARD_ITEM_ID:Ljava/lang/String; = "r_item_id"

.field private static final PARAM_REQUEST_ID:Ljava/lang/String; = "rid"

.field private static final PARAM_START_TIME:Ljava/lang/String; = "s_time"

.field private static final PARAM_STORYBOARD_ITEM_ID:Ljava/lang/String; = "item_id"

.field private static final PARAM_THEME:Ljava/lang/String; = "theme"

.field private static final PARAM_TOKEN:Ljava/lang/String; = "token"

.field private static final PARAM_TRANSITION_BEHAVIOR:Ljava/lang/String; = "behavior"

.field private static final PARAM_TRANSITION_BLENDING:Ljava/lang/String; = "t_blending"

.field private static final PARAM_TRANSITION_DIRECTION:Ljava/lang/String; = "t_dir"

.field private static final PARAM_TRANSITION_INVERT:Ljava/lang/String; = "t_invert"

.field private static final PARAM_TRANSITION_MASK:Ljava/lang/String; = "t_mask"

.field private static final PARAM_VOLUME:Ljava/lang/String; = "volume"

.field private static final PARAM_WIDTH:Ljava/lang/String; = "width"

.field private static final TAG:Ljava/lang/String; = "VEApiService"

.field private static volatile mExportCancelled:Z

.field private static mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

.field private static final mIntentPool:Lcom/android/videoeditor/service/IntentPool;

.field private static final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/ApiServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final mPendingIntents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private static mVideoEditor:Landroid/media/videoeditor/VideoEditor;

.field private static mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;


# instance fields
.field private mAudioThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

.field private mHandler:Landroid/os/Handler;

.field private final mStopRunnable:Ljava/lang/Runnable;

.field private mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

.field private mVideoThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    new-instance v0, Lcom/android/videoeditor/service/IntentPool;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/IntentPool;-><init>(I)V

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/android/videoeditor/service/ApiService$1;

    invoke-direct {v0, p0}, Lcom/android/videoeditor/service/ApiService$1;-><init>(Lcom/android/videoeditor/service/ApiService;)V

    iput-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mStopRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200()Lcom/android/videoeditor/service/IntentPool;
    .locals 1

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/service/ApiService;
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/media/videoeditor/VideoEditor;
    .param p3    # Ljava/lang/Exception;
    .param p4    # Ljava/lang/Object;
    .param p5    # Ljava/lang/Object;
    .param p6    # Z

    invoke-direct/range {p0 .. p6}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/videoeditor/service/ApiService;)Lcom/android/videoeditor/service/ApiService$IntentProcessor;
    .locals 1
    .param p0    # Lcom/android/videoeditor/service/ApiService;

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mVideoThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/service/ApiService;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$600(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/videoeditor/service/ApiService;->logv(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700()Z
    .locals 1

    sget-boolean v0, Lcom/android/videoeditor/service/ApiService;->mExportCancelled:Z

    return v0
.end method

.method static synthetic access$800(Lcom/android/videoeditor/service/ApiService;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/videoeditor/service/ApiService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/service/ApiService;->exportToGallery(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static addAudioTrack(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/net/Uri;
    .param p4    # Z

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "filename"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "loop"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static addEffectColor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJII)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # J
    .param p8    # I
    .param p9    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "s_time"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p6, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "e_type"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "e_param"

    invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static addEffectKenBurns(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # J
    .param p8    # Landroid/graphics/Rect;
    .param p9    # Landroid/graphics/Rect;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xc9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "s_time"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p6, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "start_rect"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "end_rect"

    invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static addMediaItemImageUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;IJLjava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;
    .param p5    # I
    .param p6    # J
    .param p8    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x65

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "filename"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "rm"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p6, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "theme"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static addMediaItemVideoUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;ILjava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;
    .param p5    # I
    .param p6    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "filename"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "rm"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "theme"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static addOverlay(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;JJ)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;
    .param p5    # J
    .param p7    # J

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "s_time"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p7, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "attributes"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static applyTheme(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "theme"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method private applyThemeAfterMove(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Landroid/media/videoeditor/MediaItem;ILandroid/media/videoeditor/Transition;Landroid/media/videoeditor/Transition;)V
    .locals 9
    .param p1    # Landroid/media/videoeditor/VideoEditor;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/media/videoeditor/MediaItem;
    .param p4    # I
    .param p5    # Landroid/media/videoeditor/Transition;
    .param p6    # Landroid/media/videoeditor/Transition;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x0

    invoke-interface {p1}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/videoeditor/service/MovieTheme;->getTheme(Landroid/content/Context;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieTheme;

    move-result-object v4

    if-nez p4, :cond_2

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTheme;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v6

    if-eqz v6, :cond_1

    if-eqz p5, :cond_1

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTheme;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v7

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v7, v0, v8, v6}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v5

    invoke-interface {p1, v5}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    :cond_1
    :goto_1
    invoke-direct {p0, p1, p2, p3}, Lcom/android/videoeditor/service/ApiService;->applyThemeToMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Landroid/media/videoeditor/MediaItem;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v6, v1, -0x1

    if-ne p4, v6, :cond_3

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTheme;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v6

    if-eqz v6, :cond_1

    if-eqz p6, :cond_1

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTheme;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v7

    add-int/lit8 v6, v1, -0x1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v7, v0, v6, v8}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v5

    invoke-interface {p1, v5}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    goto :goto_1

    :cond_3
    invoke-interface {v2, p3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    if-le v3, p4, :cond_4

    if-eqz p6, :cond_1

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTheme;->getMidTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTheme;->getMidTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v8

    add-int/lit8 v6, p4, -0x1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/videoeditor/MediaItem;

    invoke-interface {v2, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v8, v0, v6, v7}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v5

    invoke-interface {p1, v5}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    goto :goto_1

    :cond_4
    if-eqz p5, :cond_1

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTheme;->getMidTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTheme;->getMidTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v8

    invoke-interface {v2, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/videoeditor/MediaItem;

    add-int/lit8 v7, p4, 0x1

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v8, v0, v6, v7}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v5

    invoke-interface {p1, v5}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    goto :goto_1
.end method

.method private applyThemeAfterRemove(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;ILandroid/media/videoeditor/Transition;Landroid/media/videoeditor/Transition;)Landroid/media/videoeditor/Transition;
    .locals 8
    .param p1    # Landroid/media/videoeditor/VideoEditor;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Landroid/media/videoeditor/Transition;
    .param p5    # Landroid/media/videoeditor/Transition;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    invoke-interface {p1}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    move-object v4, v6

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-virtual {p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/videoeditor/service/MovieTheme;->getTheme(Landroid/content/Context;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieTheme;

    move-result-object v3

    const/4 v4, 0x0

    if-nez p3, :cond_2

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTheme;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    if-eqz v5, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTheme;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v7

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v7, v0, v6, v5}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v4

    invoke-interface {p1, v4}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    goto :goto_0

    :cond_2
    if-ne p3, v1, :cond_3

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTheme;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    if-eqz v5, :cond_0

    if-eqz p5, :cond_0

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTheme;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v7

    add-int/lit8 v5, v1, -0x1

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v7, v0, v5, v6}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v4

    invoke-interface {p1, v4}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTheme;->getMidTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    if-eqz v5, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTheme;->getMidTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v7

    add-int/lit8 v5, p3, -0x1

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/media/videoeditor/MediaItem;

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v7, v0, v5, v6}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v4

    invoke-interface {p1, v4}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    goto :goto_0
.end method

.method private applyThemeToMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Landroid/media/videoeditor/MediaItem;)V
    .locals 36
    .param p1    # Landroid/media/videoeditor/VideoEditor;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/media/videoeditor/MediaItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface/range {p1 .. p1}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v28

    if-nez v28, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    move-result-object v19

    invoke-virtual/range {p3 .. p3}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/android/videoeditor/service/MovieTheme;->getTheme(Landroid/content/Context;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieTheme;

    move-result-object v34

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/media/videoeditor/MediaItem;

    if-nez v19, :cond_2

    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieTheme;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v18

    if-eqz v18, :cond_2

    move-object/from16 v0, v23

    move-object/from16 v1, p3

    if-ne v0, v1, :cond_2

    const/4 v3, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    :cond_2
    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieTheme;->getMidTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v30

    if-eqz v30, :cond_4

    const/16 v24, 0x0

    :goto_1
    move/from16 v0, v24

    move/from16 v1, v28

    if-ge v0, v1, :cond_4

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/media/videoeditor/MediaItem;

    move-object/from16 v0, v29

    move-object/from16 v1, p3

    if-ne v0, v1, :cond_7

    if-lez v24, :cond_3

    if-nez v19, :cond_3

    add-int/lit8 v3, v24, -0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/videoeditor/MediaItem;

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    :cond_3
    add-int/lit8 v3, v28, -0x1

    move/from16 v0, v24

    if-ge v0, v3, :cond_4

    if-nez v22, :cond_4

    add-int/lit8 v3, v24, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/videoeditor/MediaItem;

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    :cond_4
    if-nez v22, :cond_5

    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieTheme;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v21

    add-int/lit8 v3, v28, -0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/media/videoeditor/MediaItem;

    if-eqz v21, :cond_5

    move-object/from16 v0, v26

    move-object/from16 v1, p3

    if-ne v0, v1, :cond_5

    const/4 v3, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    :cond_5
    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieTheme;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v31

    if-eqz v31, :cond_0

    move-object/from16 v0, v23

    move-object/from16 v1, p3

    if-ne v0, v1, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    move-result-object v33

    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    const/4 v3, 0x0

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/videoeditor/Overlay;

    invoke-virtual {v3}, Landroid/media/videoeditor/Overlay;->getId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/media/videoeditor/MediaItem;->removeOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;

    :cond_6
    move-object/from16 v0, p3

    instance-of v3, v0, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v3, :cond_8

    move-object/from16 v3, p3

    check-cast v3, Landroid/media/videoeditor/MediaVideoItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaVideoItem;->getWidth()I

    move-result v8

    move-object/from16 v3, p3

    check-cast v3, Landroid/media/videoeditor/MediaVideoItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaVideoItem;->getHeight()I

    move-result v9

    :goto_2
    new-instance v10, Landroid/media/videoeditor/OverlayFrame;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v31 .. v31}, Lcom/android/videoeditor/service/MovieOverlay;->getType()I

    move-result v5

    invoke-virtual/range {v31 .. v31}, Lcom/android/videoeditor/service/MovieOverlay;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v31 .. v31}, Lcom/android/videoeditor/service/MovieOverlay;->getSubtitle()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v3 .. v9}, Lcom/android/videoeditor/util/ImageUtils;->buildOverlayBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual/range {v31 .. v31}, Lcom/android/videoeditor/service/MovieOverlay;->getStartTime()J

    move-result-wide v14

    invoke-virtual/range {v31 .. v31}, Lcom/android/videoeditor/service/MovieOverlay;->getDuration()J

    move-result-wide v3

    invoke-virtual/range {p3 .. p3}, Landroid/media/videoeditor/MediaItem;->getDuration()J

    move-result-wide v5

    invoke-virtual/range {v31 .. v31}, Lcom/android/videoeditor/service/MovieOverlay;->getStartTime()J

    move-result-wide v16

    sub-long v5, v5, v16

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v16

    move-object/from16 v11, p3

    invoke-direct/range {v10 .. v17}, Landroid/media/videoeditor/OverlayFrame;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Landroid/graphics/Bitmap;JJ)V

    invoke-virtual/range {v31 .. v31}, Lcom/android/videoeditor/service/MovieOverlay;->buildUserAttributes()Landroid/os/Bundle;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_3
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/String;

    invoke-static/range {v32 .. v32}, Lcom/android/videoeditor/service/MovieOverlay;->getAttributeType(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-class v4, Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, v35

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v10, v0, v3}, Landroid/media/videoeditor/Overlay;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_1

    :cond_8
    move-object/from16 v3, p3

    check-cast v3, Landroid/media/videoeditor/MediaImageItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    move-result v8

    move-object/from16 v3, p3

    check-cast v3, Landroid/media/videoeditor/MediaImageItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    move-result v9

    goto :goto_2

    :cond_9
    move-object/from16 v0, v35

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v10, v0, v3}, Landroid/media/videoeditor/Overlay;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/media/videoeditor/MediaItem;->addOverlay(Landroid/media/videoeditor/Overlay;)V

    goto/16 :goto_0
.end method

.method private applyThemeToMovie(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)V
    .locals 39
    .param p1    # Landroid/media/videoeditor/VideoEditor;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/android/videoeditor/service/MovieTheme;->getTheme(Landroid/content/Context;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieTheme;

    move-result-object v37

    invoke-interface/range {p1 .. p1}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v31

    if-lez v31, :cond_5

    const/16 v27, 0x0

    :goto_0
    move/from16 v0, v27

    move/from16 v1, v31

    if-ge v0, v1, :cond_2

    move-object/from16 v0, v30

    move/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Landroid/media/videoeditor/MediaItem;

    if-nez v27, :cond_0

    invoke-virtual/range {v32 .. v32}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    move-result-object v22

    if-eqz v22, :cond_0

    invoke-virtual/range {v22 .. v22}, Landroid/media/videoeditor/Transition;->getId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->removeTransition(Ljava/lang/String;)Landroid/media/videoeditor/Transition;

    :cond_0
    invoke-virtual/range {v32 .. v32}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    move-result-object v25

    if-eqz v25, :cond_1

    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/Transition;->getId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->removeTransition(Ljava/lang/String;)Landroid/media/videoeditor/Transition;

    :cond_1
    add-int/lit8 v27, v27, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual/range {v37 .. v37}, Lcom/android/videoeditor/service/MovieTheme;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v21

    if-eqz v21, :cond_3

    const/4 v3, 0x0

    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/media/videoeditor/MediaItem;

    const/4 v3, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    :cond_3
    invoke-virtual/range {v37 .. v37}, Lcom/android/videoeditor/service/MovieTheme;->getMidTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v33

    if-eqz v33, :cond_4

    const/16 v27, 0x0

    :goto_1
    add-int/lit8 v3, v31, -0x1

    move/from16 v0, v27

    if-ge v0, v3, :cond_4

    move-object/from16 v0, v30

    move/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/videoeditor/MediaItem;

    add-int/lit8 v4, v27, 0x1

    move-object/from16 v0, v30

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/videoeditor/MediaItem;

    move-object/from16 v0, v33

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v3, v4}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    add-int/lit8 v27, v27, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual/range {v37 .. v37}, Lcom/android/videoeditor/service/MovieTheme;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v24

    if-eqz v24, :cond_5

    add-int/lit8 v3, v31, -0x1

    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/media/videoeditor/MediaItem;

    const/4 v3, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/videoeditor/service/MovieTransition;->buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    :cond_5
    invoke-virtual/range {v37 .. v37}, Lcom/android/videoeditor/service/MovieTheme;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v34

    if-eqz v34, :cond_a

    if-lez v31, :cond_a

    const/4 v3, 0x0

    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v11}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    move-result-object v36

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    const/4 v3, 0x0

    move-object/from16 v0, v36

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/videoeditor/Overlay;

    invoke-virtual {v3}, Landroid/media/videoeditor/Overlay;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Landroid/media/videoeditor/MediaItem;->removeOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;

    :cond_6
    instance-of v3, v11, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v3, :cond_7

    move-object v3, v11

    check-cast v3, Landroid/media/videoeditor/MediaVideoItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaVideoItem;->getWidth()I

    move-result v8

    move-object v3, v11

    check-cast v3, Landroid/media/videoeditor/MediaVideoItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaVideoItem;->getHeight()I

    move-result v9

    :goto_2
    new-instance v10, Landroid/media/videoeditor/OverlayFrame;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieOverlay;->getType()I

    move-result v5

    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieOverlay;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieOverlay;->getSubtitle()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v3 .. v9}, Lcom/android/videoeditor/util/ImageUtils;->buildOverlayBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieOverlay;->getStartTime()J

    move-result-wide v14

    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieOverlay;->getDuration()J

    move-result-wide v16

    invoke-direct/range {v10 .. v17}, Landroid/media/videoeditor/OverlayFrame;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Landroid/graphics/Bitmap;JJ)V

    invoke-virtual/range {v34 .. v34}, Lcom/android/videoeditor/service/MovieOverlay;->buildUserAttributes()Landroid/os/Bundle;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :goto_3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    invoke-static/range {v35 .. v35}, Lcom/android/videoeditor/service/MovieOverlay;->getAttributeType(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-class v4, Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, v38

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v35

    invoke-virtual {v10, v0, v3}, Landroid/media/videoeditor/Overlay;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    move-object v3, v11

    check-cast v3, Landroid/media/videoeditor/MediaImageItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    move-result v8

    move-object v3, v11

    check-cast v3, Landroid/media/videoeditor/MediaImageItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    move-result v9

    goto :goto_2

    :cond_8
    move-object/from16 v0, v38

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v35

    invoke-virtual {v10, v0, v3}, Landroid/media/videoeditor/Overlay;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    invoke-virtual {v11, v10}, Landroid/media/videoeditor/MediaItem;->addOverlay(Landroid/media/videoeditor/Overlay;)V

    :cond_a
    invoke-virtual/range {v37 .. v37}, Lcom/android/videoeditor/service/MovieTheme;->getAudioTrack()Lcom/android/videoeditor/service/MovieAudioTrack;

    move-result-object v18

    if-eqz v18, :cond_d

    invoke-interface/range {p1 .. p1}, Landroid/media/videoeditor/VideoEditor;->getAllAudioTracks()Ljava/util/List;

    move-result-object v20

    :goto_4
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_b

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/videoeditor/AudioTrack;

    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/media/videoeditor/VideoEditor;->removeAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    goto :goto_4

    :cond_b
    new-instance v19, Landroid/media/videoeditor/AudioTrack;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Lcom/android/videoeditor/service/MovieAudioTrack;->getRawResourceId()I

    move-result v4

    move-object/from16 v0, v23

    invoke-static {v0, v4}, Lcom/android/videoeditor/util/FileUtils;->getAudioTrackFilename(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Landroid/media/videoeditor/AudioTrack;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Lcom/android/videoeditor/service/MovieAudioTrack;->isLooping()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual/range {v19 .. v19}, Landroid/media/videoeditor/AudioTrack;->enableLoop()V

    :cond_c
    const/16 v3, 0x14

    const/16 v4, 0x41

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/media/videoeditor/AudioTrack;->enableDucking(II)V

    const/16 v3, 0x32

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/media/videoeditor/AudioTrack;->setVolume(I)V

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/media/videoeditor/VideoEditor;->addAudioTrack(Landroid/media/videoeditor/AudioTrack;)V

    :cond_d
    return-void
.end method

.method public static cancelExportVideoEditor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/videoeditor/service/ApiService;->mExportCancelled:Z

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "filename"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method private completeRequest(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/videoeditor/service/ApiService$6;

    invoke-direct {v1, p0, p1}, Lcom/android/videoeditor/service/ApiService$6;-><init>(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/media/videoeditor/VideoEditor;
    .param p3    # Ljava/lang/Exception;
    .param p4    # Ljava/lang/Object;
    .param p5    # Ljava/lang/Object;
    .param p6    # Z

    iget-object v8, p0, Lcom/android/videoeditor/service/ApiService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/videoeditor/service/ApiService$5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p3

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/android/videoeditor/service/ApiService$5;-><init>(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Exception;Z)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private copyAudioTracks(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/AudioTrack;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieAudioTrack;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/videoeditor/AudioTrack;

    new-instance v3, Lcom/android/videoeditor/service/MovieAudioTrack;

    invoke-direct {v3, v0}, Lcom/android/videoeditor/service/MovieAudioTrack;-><init>(Landroid/media/videoeditor/AudioTrack;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private copyMediaItems(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieMediaItem;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/videoeditor/MediaItem;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    :goto_1
    new-instance v2, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-direct {v2, v1, v5}, Lcom/android/videoeditor/service/MovieMediaItem;-><init>(Landroid/media/videoeditor/MediaItem;Lcom/android/videoeditor/service/MovieTransition;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v4, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    move-result-object v6

    if-eqz v6, :cond_1

    new-instance v5, Lcom/android/videoeditor/service/MovieTransition;

    invoke-virtual {v1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/videoeditor/service/MovieTransition;-><init>(Landroid/media/videoeditor/Transition;)V

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method public static createVideoEditor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "movies"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "images"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "theme"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static deleteProject(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method private exportMovie(Landroid/media/videoeditor/VideoEditor;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/media/videoeditor/VideoEditor;
    .param p2    # Landroid/content/Intent;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/videoeditor/service/ApiService;->mExportCancelled:Z

    new-instance v0, Lcom/android/videoeditor/service/ApiService$7;

    invoke-direct {v0, p0, p2, p1}, Lcom/android/videoeditor/service/ApiService$7;-><init>(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;)V

    invoke-virtual {v0}, Lcom/android/videoeditor/service/ApiService$7;->start()V

    return-void
.end method

.method private exportToGallery(Ljava/lang/String;)Landroid/net/Uri;
    .locals 6
    .param p1    # Ljava/lang/String;

    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "mime_type"

    const-string v3, "video/mp4"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "_data"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/service/ApiService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/service/ApiService;->sendBroadcast(Landroid/content/Intent;)V

    return-object v0
.end method

.method public static exportVideoEditor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "filename"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "height"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "bitrate"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static extractAudioTrackAudioWaveform(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/android/videoeditor/service/ApiService;->isAudioTrackAudioWaveformPending(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1fc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    goto :goto_0
.end method

.method private extractAudioTrackAudioWaveform(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Landroid/media/videoeditor/AudioTrack;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/media/videoeditor/VideoEditor;
    .param p3    # Landroid/media/videoeditor/AudioTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/android/videoeditor/service/ApiService$9;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/android/videoeditor/service/ApiService$9;-><init>(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;Landroid/media/videoeditor/AudioTrack;Landroid/media/videoeditor/VideoEditor;)V

    invoke-virtual {p3, v0}, Landroid/media/videoeditor/AudioTrack;->extractAudioWaveform(Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;)V

    return-void
.end method

.method public static extractMediaItemAudioWaveform(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/android/videoeditor/service/ApiService;->isMediaItemAudioWaveformPending(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x6d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    goto :goto_0
.end method

.method private extractMediaItemAudioWaveform(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Landroid/media/videoeditor/MediaVideoItem;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/media/videoeditor/VideoEditor;
    .param p3    # Landroid/media/videoeditor/MediaVideoItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/android/videoeditor/service/ApiService$8;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/android/videoeditor/service/ApiService$8;-><init>(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;Landroid/media/videoeditor/MediaVideoItem;Landroid/media/videoeditor/VideoEditor;)V

    invoke-virtual {p3, v0}, Landroid/media/videoeditor/MediaVideoItem;->extractAudioWaveform(Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;)V

    return-void
.end method

.method private finalizeRequest(Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Intent;

    sget-object v4, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    const-string v5, "rid"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "project"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Lcom/android/videoeditor/service/ApiService;->isProjectBeingEdited(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v4, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/ApiServiceListener;

    invoke-virtual {v1, v3, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onProjectEditState(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    sget-object v4, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/videoeditor/service/ApiService;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/videoeditor/service/ApiService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v4, p0, Lcom/android/videoeditor/service/ApiService;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/videoeditor/service/ApiService;->mStopRunnable:Ljava/lang/Runnable;

    const-wide/16 v6, 0x1388

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v4, "completeRequest: Stopping service in 5000 ms"

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public static generateId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/android/videoeditor/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V
    .locals 5
    .param p1    # Landroid/media/videoeditor/VideoEditor;
    .param p2    # Z

    :try_start_0
    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    invoke-interface {p1, v1}, Landroid/media/videoeditor/VideoEditor;->generatePreview(Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;)V

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    if-eqz v1, :cond_0

    sget-object v2, Lcom/android/videoeditor/service/ApiService;->mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    const/4 v3, 0x0

    if-eqz p2, :cond_1

    const/16 v1, 0x66

    :goto_0
    const/16 v4, 0x64

    invoke-virtual {v2, v3, v1, v4}, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;->onProgress(Ljava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/16 v1, 0x67

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static getMediaItemThumbnails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJJII[I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # J
    .param p7    # J
    .param p9    # I
    .param p10    # I
    .param p11    # [I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x70

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "width"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "height"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "s_time"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "e_time"

    invoke-virtual {v0, v1, p7, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "count"

    invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "token"

    invoke-virtual {v0, v1, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "indices"

    invoke-virtual {v0, v1, p11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method private getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getTransitionThumbnails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x132

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "height"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method private declared-synchronized getVideoEditor(Ljava/lang/String;)Landroid/media/videoeditor/VideoEditor;
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0}, Landroid/media/videoeditor/VideoEditor;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static insertAlphaTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIIIZ)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # Z

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "behavior"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "t_mask"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "t_blending"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "t_invert"

    invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static insertCrossfadeTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x12d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "behavior"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static insertFadeBlackTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x12e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "behavior"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static insertSlidingTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # I
    .param p7    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x12f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "behavior"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "t_dir"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static isAudioTrackAudioWaveformPending(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v5, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    const-string v5, "op"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v5, 0x1fc

    if-ne v3, v5, :cond_0

    const-string v5, "project"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "item_id"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static isMediaItemAudioWaveformPending(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v5, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    const-string v5, "op"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v5, 0x6d

    if-ne v3, v5, :cond_0

    const-string v5, "project"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "item_id"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static isProjectBeingEdited(Ljava/lang/String;)Z
    .locals 6
    .param p0    # Ljava/lang/String;

    sget-object v4, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :sswitch_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    const-string v4, "op"

    const/4 v5, -0x1

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    const-string v4, "project"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xd -> :sswitch_0
        0x6b -> :sswitch_0
        0x6c -> :sswitch_0
        0x70 -> :sswitch_0
        0x71 -> :sswitch_0
        0x132 -> :sswitch_0
        0x1f6 -> :sswitch_0
        0x1f7 -> :sswitch_0
    .end sparse-switch
.end method

.method public static isTransitionThumbnailsPending(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v5, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    const-string v5, "op"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v5, 0x132

    if-ne v3, v5, :cond_0

    const-string v5, "project"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "item_id"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static isVideoEditorExportPending(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v5, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    const-string v5, "op"

    const/4 v6, -0x1

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v5, 0x4

    if-ne v3, v5, :cond_0

    const-string v5, "project"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "filename"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static loadMediaItem(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x71

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "filename"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "attributes"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static loadProjects(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static loadVideoEditor(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method private static logd(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "VEApiService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VEApiService"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private static logv(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "VEApiService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VEApiService"

    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static moveMediaItem(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "theme"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method private static nextMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;
    .locals 5
    .param p0    # Landroid/media/videoeditor/VideoEditor;
    .param p1    # Ljava/lang/String;

    invoke-interface {p0}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v2

    if-nez p1, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/videoeditor/MediaItem;

    :goto_0
    return-object v4

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v3}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v4, v1, -0x1

    if-ge v0, v4, :cond_2

    add-int/lit8 v4, v0, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/videoeditor/MediaItem;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static registerListener(Lcom/android/videoeditor/service/ApiServiceListener;)V
    .locals 1
    .param p0    # Lcom/android/videoeditor/service/ApiServiceListener;

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private declared-synchronized releaseEditor()V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "releaseEditor (current): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v1}, Landroid/media/videoeditor/VideoEditor;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0}, Landroid/media/videoeditor/VideoEditor;->release()V

    const/4 v0, 0x0

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized releaseEditor(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0}, Landroid/media/videoeditor/VideoEditor;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "releaseEditor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0}, Landroid/media/videoeditor/VideoEditor;->release()V

    const/4 v0, 0x0

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized releaseEditorNot(Ljava/lang/String;)Landroid/media/videoeditor/VideoEditor;
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0}, Landroid/media/videoeditor/VideoEditor;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "releaseEditorNot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v1}, Landroid/media/videoeditor/VideoEditor;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0}, Landroid/media/videoeditor/VideoEditor;->release()V

    const/4 v0, 0x0

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/videoeditor/service/ApiService;->mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    invoke-static {}, Ljava/lang/System;->gc()V

    :cond_0
    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static releaseVideoEditor(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static removeAudioTrack(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1f5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static removeEffect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static removeMediaItem(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x67

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "theme"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static removeOverlay(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x191

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static removeTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x130

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static saveVideoEditor(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setAspectRatio(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "aspect_ratio"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setAudioTrackBoundaries(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1f9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "b_boundary"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "e_boundary"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setAudioTrackDuck(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1fb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "duck"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setAudioTrackLoop(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1fa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "loop"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setAudioTrackMute(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1f7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mute"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setAudioTrackVolume(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1f6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "volume"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setMediaItemBoundaries(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x6a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "b_boundary"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "e_boundary"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setMediaItemDuration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x69

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setMediaItemMute(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x6c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mute"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setMediaItemRenderingMode(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x68

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "rm"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setMediaItemVolume(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x6b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "volume"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setOverlayDuration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x193

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setOverlayStartTime(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x192

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "s_time"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setOverlayUserAttributes(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x194

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "r_item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "attributes"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method public static setTransitionDuration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    sget-object v1, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    const-class v2, Lcom/android/videoeditor/service/ApiService;

    invoke-virtual {v1, p0, v2}, Lcom/android/videoeditor/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x131

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "project"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/videoeditor/service/ApiService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    return-void
.end method

.method private static startCommand(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/Intent;

    const/16 v5, 0x8

    invoke-static {v5}, Lcom/android/videoeditor/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "rid"

    invoke-virtual {p1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v5, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    invoke-interface {v5, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const-string v5, "project"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Lcom/android/videoeditor/service/ApiService;->isProjectBeingEdited(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v5, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/ApiServiceListener;

    invoke-virtual {v1, v3, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onProjectEditState(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    return-object v4
.end method

.method public static unregisterListener(Lcom/android/videoeditor/service/ApiServiceListener;)V
    .locals 1
    .param p0    # Lcom/android/videoeditor/service/ApiServiceListener;

    sget-object v0, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    const-string v1, "VideoServiceThread"

    invoke-direct {v0, p0, v1}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;-><init>(Lcom/android/videoeditor/service/ApiService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mVideoThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mVideoThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->start()V

    new-instance v0, Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    const-string v1, "AudioServiceThread"

    invoke-direct {v0, p0, v1}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;-><init>(Lcom/android/videoeditor/service/ApiService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mAudioThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mAudioThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->start()V

    new-instance v0, Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    const-string v1, "ThumbnailServiceThread"

    invoke-direct {v0, p0, v1}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;-><init>(Lcom/android/videoeditor/service/ApiService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->quit()V

    iput-object v1, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mAudioThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mAudioThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->quit()V

    iput-object v1, p0, Lcom/android/videoeditor/service/ApiService;->mAudioThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mVideoThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService;->mVideoThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->quit()V

    iput-object v1, p0, Lcom/android/videoeditor/service/ApiService;->mVideoThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    :cond_2
    return-void
.end method

.method public onIntentProcessed(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Exception;Z)V
    .locals 77
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/media/videoeditor/VideoEditor;
    .param p3    # Ljava/lang/Object;
    .param p4    # Ljava/lang/Object;
    .param p5    # Ljava/lang/Exception;
    .param p6    # Z

    const-string v11, "project"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v11, "op"

    const/4 v12, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v67

    sparse-switch v67, :sswitch_data_0

    if-eqz p6, :cond_0

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    :sswitch_0
    if-eqz p6, :cond_1

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/android/videoeditor/service/IntentPool;->put(Landroid/content/Intent;)V

    :cond_1
    return-void

    :sswitch_1
    if-eqz p6, :cond_2

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_2
    move-object/from16 v70, p3

    check-cast v70, Ljava/util/List;

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_1
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v70

    move-object/from16 v1, p5

    invoke-virtual {v5, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onProjectsLoaded(Ljava/util/List;Ljava/lang/Exception;)V

    goto :goto_1

    :sswitch_2
    if-eqz p6, :cond_3

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_3
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v11, :cond_4

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v11}, Lcom/android/videoeditor/service/VideoEditorProject;->release()V

    const/4 v11, 0x0

    sput-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    :cond_4
    if-eqz p5, :cond_5

    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v11}, Lcom/android/videoeditor/util/FileUtils;->deleteDir(Ljava/io/File;)Z

    :goto_2
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_3
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    sget-object v7, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz p2, :cond_6

    invoke-interface/range {p2 .. p2}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v8

    :goto_4
    if-eqz p2, :cond_7

    invoke-interface/range {p2 .. p2}, Landroid/media/videoeditor/VideoEditor;->getAllAudioTracks()Ljava/util/List;

    move-result-object v9

    :goto_5
    move-object/from16 v10, p5

    invoke-virtual/range {v5 .. v10}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorCreated(Ljava/lang/String;Lcom/android/videoeditor/service/VideoEditorProject;Ljava/util/List;Ljava/util/List;Ljava/lang/Exception;)V

    goto :goto_3

    :cond_5
    check-cast p3, Lcom/android/videoeditor/service/VideoEditorProject;

    sput-object p3, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    goto :goto_2

    :cond_6
    const/4 v8, 0x0

    goto :goto_4

    :cond_7
    const/4 v9, 0x0

    goto :goto_5

    :sswitch_3
    if-eqz p6, :cond_8

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_8
    if-eqz p3, :cond_a

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v11, :cond_9

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v11}, Lcom/android/videoeditor/service/VideoEditorProject;->release()V

    const/4 v11, 0x0

    sput-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    :cond_9
    check-cast p3, Lcom/android/videoeditor/service/VideoEditorProject;

    sput-object p3, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    :cond_a
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_6
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    sget-object v7, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-nez p5, :cond_b

    invoke-interface/range {p2 .. p2}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v8

    :goto_7
    if-nez p5, :cond_c

    invoke-interface/range {p2 .. p2}, Landroid/media/videoeditor/VideoEditor;->getAllAudioTracks()Ljava/util/List;

    move-result-object v9

    :goto_8
    move-object/from16 v10, p5

    invoke-virtual/range {v5 .. v10}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorLoaded(Ljava/lang/String;Lcom/android/videoeditor/service/VideoEditorProject;Ljava/util/List;Ljava/util/List;Ljava/lang/Exception;)V

    goto :goto_6

    :cond_b
    const/4 v8, 0x0

    goto :goto_7

    :cond_c
    const/4 v9, 0x0

    goto :goto_8

    :sswitch_4
    if-eqz p6, :cond_d

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_d
    const-string v11, "aspect_ratio"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v53

    if-nez p5, :cond_e

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_e

    move-object/from16 v0, v76

    move/from16 v1, v53

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->setAspectRatio(I)V

    :cond_e
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_9
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move/from16 v0, v53

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorAspectRatioSet(Ljava/lang/String;ILjava/lang/Exception;)V

    goto :goto_9

    :sswitch_5
    if-eqz p6, :cond_f

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_f
    const-string v11, "theme"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v72

    if-nez p5, :cond_10

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_10

    move-object/from16 v0, v76

    move-object/from16 v1, v72

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->setTheme(Ljava/lang/String;)V

    check-cast p3, Ljava/util/List;

    move-object/from16 v0, v76

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->setMediaItems(Ljava/util/List;)V

    check-cast p4, Ljava/util/List;

    move-object/from16 v0, v76

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->setAudioTracks(Ljava/util/List;)V

    :cond_10
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_a
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v72

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorThemeApplied(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_a

    :sswitch_6
    const-string v11, "attributes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v11, "action"

    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    const-string v11, "prog_value"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_b
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    invoke-virtual/range {v5 .. v10}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorGeneratePreviewProgress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_b

    :sswitch_7
    if-eqz p6, :cond_11

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_11
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_c
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v6, v11}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorExportCanceled(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    :sswitch_8
    if-eqz p6, :cond_12

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_12
    const-string v11, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v11, "ex"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_14

    const-string v11, "req_intent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v68

    check-cast v68, Landroid/content/Intent;

    move-object/from16 v0, p0

    move-object/from16 v1, v68

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    move-object/from16 v0, v68

    invoke-virtual {v11, v0}, Lcom/android/videoeditor/service/IntentPool;->put(Landroid/content/Intent;)V

    const-string v11, "ex"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v58

    check-cast v58, Ljava/lang/Exception;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    const-string v11, "cancelled"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v56

    if-nez v56, :cond_13

    if-eqz v76, :cond_13

    if-nez v58, :cond_13

    const-string v11, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v74

    check-cast v74, Landroid/net/Uri;

    move-object/from16 v0, v76

    move-object/from16 v1, v74

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->addExportedMovieUri(Landroid/net/Uri;)V

    :cond_13
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_d
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v20

    move-object/from16 v1, v58

    move/from16 v2, v56

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorExportComplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_d

    :cond_14
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_e
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "prog_value"

    const/4 v12, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    move-object/from16 v0, v20

    invoke-virtual {v5, v6, v0, v11}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorExportProgress(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_e

    :sswitch_9
    if-eqz p6, :cond_15

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_15
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_f
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, p5

    invoke-virtual {v5, v6, v0}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorSaved(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_f

    :sswitch_a
    if-eqz p6, :cond_16

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_16
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_17

    invoke-virtual/range {v76 .. v76}, Lcom/android/videoeditor/service/VideoEditorProject;->release()V

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    move-object/from16 v0, v76

    if-ne v11, v0, :cond_17

    const/4 v11, 0x0

    sput-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    :cond_17
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_10
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, p5

    invoke-virtual {v5, v6, v0}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorReleased(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_10

    :sswitch_b
    if-eqz p6, :cond_18

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_18
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_19

    invoke-virtual/range {v76 .. v76}, Lcom/android/videoeditor/service/VideoEditorProject;->release()V

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    move-object/from16 v0, v76

    if-ne v11, v0, :cond_19

    const/4 v11, 0x0

    sput-object v11, Lcom/android/videoeditor/service/ApiService;->mVideoProject:Lcom/android/videoeditor/service/VideoEditorProject;

    :cond_19
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_11
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, p5

    invoke-virtual {v5, v6, v0}, Lcom/android/videoeditor/service/ApiServiceListener;->onVideoEditorDeleted(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_11

    :sswitch_c
    if-eqz p6, :cond_1a

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_1a
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v14, p3

    check-cast v14, Lcom/android/videoeditor/service/MovieMediaItem;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_1c

    if-nez p5, :cond_1b

    if-eqz p4, :cond_1b

    move-object/from16 v11, p4

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setAspectRatio(I)V

    :cond_1b
    if-nez p5, :cond_1c

    move-object/from16 v0, v76

    invoke-virtual {v0, v14, v15}, Lcom/android/videoeditor/service/VideoEditorProject;->insertMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;)V

    :cond_1c
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_12
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-class v16, Landroid/media/videoeditor/MediaVideoItem;

    move-object/from16 v17, p4

    check-cast v17, Ljava/lang/Integer;

    move-object v11, v5

    move-object v12, v6

    move-object/from16 v18, p5

    invoke-virtual/range {v11 .. v18}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemAdded(Ljava/lang/String;Ljava/lang/String;Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Integer;Ljava/lang/Exception;)V

    goto :goto_12

    :sswitch_d
    if-eqz p6, :cond_1d

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_1d
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v14, p3

    check-cast v14, Lcom/android/videoeditor/service/MovieMediaItem;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_1f

    if-nez p5, :cond_1e

    if-eqz p4, :cond_1e

    move-object/from16 v11, p4

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setAspectRatio(I)V

    :cond_1e
    if-nez p5, :cond_1f

    move-object/from16 v0, v76

    invoke-virtual {v0, v14, v15}, Lcom/android/videoeditor/service/VideoEditorProject;->insertMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;)V

    :cond_1f
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_13
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-class v16, Landroid/media/videoeditor/MediaImageItem;

    move-object/from16 v17, p4

    check-cast v17, Ljava/lang/Integer;

    move-object v11, v5

    move-object v12, v6

    move-object/from16 v18, p5

    invoke-virtual/range {v11 .. v18}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemAdded(Ljava/lang/String;Ljava/lang/String;Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Integer;Ljava/lang/Exception;)V

    goto :goto_13

    :sswitch_e
    const-string v11, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v18

    check-cast v18, Landroid/net/Uri;

    const-string v11, "attributes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    if-eqz p6, :cond_20

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_20
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_14
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const/16 v20, 0x0

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v21, p5

    invoke-virtual/range {v16 .. v21}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaLoaded(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_14

    :sswitch_f
    if-eqz p6, :cond_21

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_21
    const-string v11, "req_intent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v68

    check-cast v68, Landroid/content/Intent;

    const-string v11, "filename"

    move-object/from16 v0, v68

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v18

    check-cast v18, Landroid/net/Uri;

    const-string v11, "attributes"

    move-object/from16 v0, v68

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v68

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    move-object/from16 v0, v68

    invoke-virtual {v11, v0}, Lcom/android/videoeditor/service/IntentPool;->put(Landroid/content/Intent;)V

    const-string v11, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    if-nez p5, :cond_22

    if-eqz v20, :cond_22

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v76

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v11, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->addDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_22
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_15
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v21, p5

    invoke-virtual/range {v16 .. v21}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaLoaded(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_15

    :sswitch_10
    if-eqz p6, :cond_23

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_23
    if-nez p5, :cond_24

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_24

    check-cast p3, Ljava/util/List;

    move-object/from16 v0, v76

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->setMediaItems(Ljava/util/List;)V

    :cond_24
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_16
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p5

    invoke-virtual {v5, v6, v11, v12, v0}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemMoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_16

    :sswitch_11
    if-eqz p6, :cond_25

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_25
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v73, p3

    check-cast v73, Lcom/android/videoeditor/service/MovieTransition;

    if-nez p5, :cond_26

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_26

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    move-object/from16 v2, v73

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->removeMediaItem(Ljava/lang/String;Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_26
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_17
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v23

    move-object/from16 v1, v73

    move-object/from16 v2, p5

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemRemoved(Ljava/lang/String;Ljava/lang/String;Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/Exception;)V

    goto :goto_17

    :sswitch_12
    if-eqz p6, :cond_27

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_27
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v11, "rm"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v71

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_28

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v61

    if-eqz v61, :cond_28

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_29

    move-object/from16 v0, v61

    move/from16 v1, v71

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/MovieMediaItem;->setRenderingMode(I)V

    :cond_28
    :goto_18
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_19
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v23

    move/from16 v1, v71

    move-object/from16 v2, p5

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemRenderingModeSet(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V

    goto :goto_19

    :cond_29
    invoke-virtual/range {v61 .. v61}, Lcom/android/videoeditor/service/MovieMediaItem;->getRenderingMode()I

    move-result v11

    move-object/from16 v0, v61

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieMediaItem;->setAppRenderingMode(I)V

    goto :goto_18

    :sswitch_13
    if-eqz p6, :cond_2a

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_2a
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_2b

    if-nez p5, :cond_2c

    check-cast p3, Lcom/android/videoeditor/service/MovieMediaItem;

    move-object/from16 v0, v76

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    :cond_2b
    :goto_1a
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_1b
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "duration"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v24

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object/from16 v26, p5

    invoke-virtual/range {v21 .. v26}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemDurationSet(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Exception;)V

    goto :goto_1b

    :cond_2c
    move-object/from16 v0, v76

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v66

    if-eqz v66, :cond_2b

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    const-wide/16 v11, 0x0

    invoke-virtual/range {v66 .. v66}, Lcom/android/videoeditor/service/MovieMediaItem;->getDuration()J

    move-result-wide v16

    move-object/from16 v0, v66

    move-wide/from16 v1, v16

    invoke-virtual {v0, v11, v12, v1, v2}, Lcom/android/videoeditor/service/MovieMediaItem;->setAppExtractBoundaries(JJ)V

    goto :goto_1a

    :sswitch_14
    if-eqz p6, :cond_2d

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_2d
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_2e

    if-nez p5, :cond_2f

    move-object/from16 v61, p3

    check-cast v61, Lcom/android/videoeditor/service/MovieMediaItem;

    move-object/from16 v0, v76

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    :cond_2e
    :goto_1c
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_1d
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "b_boundary"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v24

    const-string v11, "e_boundary"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v26

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object/from16 v28, p5

    invoke-virtual/range {v21 .. v28}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemBoundariesSet(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/Exception;)V

    goto :goto_1d

    :cond_2f
    move-object/from16 v0, v76

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v66

    if-eqz v66, :cond_2e

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    invoke-virtual/range {v66 .. v66}, Lcom/android/videoeditor/service/MovieMediaItem;->getBoundaryBeginTime()J

    move-result-wide v11

    invoke-virtual/range {v66 .. v66}, Lcom/android/videoeditor/service/MovieMediaItem;->getBoundaryEndTime()J

    move-result-wide v16

    move-object/from16 v0, v66

    move-wide/from16 v1, v16

    invoke-virtual {v0, v11, v12, v1, v2}, Lcom/android/videoeditor/service/MovieMediaItem;->setAppExtractBoundaries(JJ)V

    goto :goto_1c

    :sswitch_15
    if-eqz p6, :cond_30

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_30
    move-object/from16 v27, p3

    check-cast v27, Landroid/graphics/Bitmap;

    check-cast p4, Ljava/lang/Integer;

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Integer;->intValue()I

    move-result v28

    const/16 v75, 0x0

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_1e
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_31

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    const-string v11, "token"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v29

    move-object/from16 v24, v5

    move-object/from16 v25, v6

    move-object/from16 v30, p5

    invoke-virtual/range {v24 .. v30}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemThumbnail(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;IILjava/lang/Exception;)Z

    move-result v11

    or-int v75, v75, v11

    goto :goto_1e

    :cond_31
    if-nez v75, :cond_0

    if-eqz v27, :cond_0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    :sswitch_16
    if-eqz p6, :cond_32

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_32
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_0

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v61

    if-eqz v61, :cond_0

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_33

    const-string v11, "volume"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    move-object/from16 v0, v61

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieMediaItem;->setVolume(I)V

    goto/16 :goto_0

    :cond_33
    invoke-virtual/range {v61 .. v61}, Lcom/android/videoeditor/service/MovieMediaItem;->getVolume()I

    move-result v11

    move-object/from16 v0, v61

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieMediaItem;->setAppVolume(I)V

    goto/16 :goto_0

    :sswitch_17
    if-eqz p6, :cond_34

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_34
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_0

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v61

    if-eqz v61, :cond_0

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_35

    const-string v11, "mute"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    move-object/from16 v0, v61

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieMediaItem;->setMute(Z)V

    goto/16 :goto_0

    :cond_35
    invoke-virtual/range {v61 .. v61}, Lcom/android/videoeditor/service/MovieMediaItem;->isMuted()Z

    move-result v11

    move-object/from16 v0, v61

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieMediaItem;->setAppMute(Z)V

    goto/16 :goto_0

    :sswitch_18
    if-eqz p6, :cond_36

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_36
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v11, "prog_value"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_1f
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v23

    invoke-virtual {v5, v6, v0, v10}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemExtractAudioWaveformProgress(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1f

    :sswitch_19
    if-eqz p6, :cond_37

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_37
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-nez p5, :cond_38

    if-eqz v76, :cond_38

    if-eqz p3, :cond_38

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v61

    if-eqz v61, :cond_38

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    check-cast p3, Landroid/media/videoeditor/WaveformData;

    move-object/from16 v0, v61

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/MovieMediaItem;->setWaveformData(Landroid/media/videoeditor/WaveformData;)V

    :cond_38
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_20
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v23

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onMediaItemExtractAudioWaveformComplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_20

    :sswitch_1a
    if-eqz p6, :cond_39

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_39
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_3b

    move-object/from16 v73, p3

    check-cast v73, Landroid/media/videoeditor/Transition;

    if-nez p5, :cond_3a

    new-instance v65, Lcom/android/videoeditor/service/MovieTransition;

    move-object/from16 v0, v65

    move-object/from16 v1, v73

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/MovieTransition;-><init>(Landroid/media/videoeditor/Transition;)V

    move-object/from16 v0, v76

    move-object/from16 v1, v65

    invoke-virtual {v0, v1, v15}, Lcom/android/videoeditor/service/VideoEditorProject;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)V

    :goto_21
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_22
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v65

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v0, v15, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onTransitionInserted(Ljava/lang/String;Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_22

    :cond_3a
    const/16 v65, 0x0

    goto :goto_21

    :cond_3b
    const/16 v65, 0x0

    goto :goto_21

    :sswitch_1b
    if-eqz p6, :cond_3c

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_3c
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_3d

    if-nez p5, :cond_3d

    move-object/from16 v0, v76

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->removeTransition(Ljava/lang/String;)V

    :cond_3d
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_23
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v31

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onTransitionRemoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_23

    :sswitch_1c
    if-eqz p6, :cond_3e

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_3e
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    const-string v11, "duration"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v32

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_3f

    move-object/from16 v0, v76

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getTransition(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v73

    if-eqz v73, :cond_3f

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_40

    move-object/from16 v0, v73

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/MovieTransition;->setDuration(J)V

    :cond_3f
    :goto_24
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_25
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v29, v5

    move-object/from16 v30, v6

    move-object/from16 v34, p5

    invoke-virtual/range {v29 .. v34}, Lcom/android/videoeditor/service/ApiServiceListener;->onTransitionDurationSet(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Exception;)V

    goto :goto_25

    :cond_40
    invoke-virtual/range {v73 .. v73}, Lcom/android/videoeditor/service/MovieTransition;->getDuration()J

    move-result-wide v11

    move-object/from16 v0, v73

    invoke-virtual {v0, v11, v12}, Lcom/android/videoeditor/service/MovieTransition;->setAppDuration(J)V

    goto :goto_24

    :sswitch_1d
    if-eqz p6, :cond_41

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_41
    check-cast p3, [Landroid/graphics/Bitmap;

    move-object/from16 v55, p3

    check-cast v55, [Landroid/graphics/Bitmap;

    const/16 v75, 0x0

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_26
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_42

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v55

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v11, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onTransitionThumbnails(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Ljava/lang/Exception;)Z

    move-result v11

    or-int v75, v75, v11

    goto :goto_26

    :cond_42
    if-nez v75, :cond_0

    if-eqz v55, :cond_0

    const/16 v59, 0x0

    :goto_27
    move-object/from16 v0, v55

    array-length v11, v0

    move/from16 v0, v59

    if-ge v0, v11, :cond_0

    aget-object v11, v55, v59

    if-eqz v11, :cond_43

    aget-object v11, v55, v59

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    :cond_43
    add-int/lit8 v59, v59, 0x1

    goto :goto_27

    :sswitch_1e
    if-eqz p6, :cond_44

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_44
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v64, p3

    check-cast v64, Lcom/android/videoeditor/service/MovieOverlay;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_45

    if-nez p5, :cond_45

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    move-object/from16 v2, v64

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->addOverlay(Ljava/lang/String;Lcom/android/videoeditor/service/MovieOverlay;)V

    :cond_45
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_28
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v64

    move-object/from16 v1, v23

    move-object/from16 v2, p5

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onOverlayAdded(Ljava/lang/String;Lcom/android/videoeditor/service/MovieOverlay;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_28

    :sswitch_1f
    if-eqz p6, :cond_46

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_46
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_47

    if-nez p5, :cond_47

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->removeOverlay(Ljava/lang/String;Ljava/lang/String;)V

    :cond_47
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_29
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    move-object/from16 v2, p5

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onOverlayRemoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_29

    :sswitch_20
    if-eqz p6, :cond_48

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_48
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    const-string v11, "s_time"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v38

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_49

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->getOverlay(Ljava/lang/String;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v69

    if-eqz v69, :cond_49

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_4a

    move-object/from16 v0, v69

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/MovieOverlay;->setStartTime(J)V

    :cond_49
    :goto_2a
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_2b
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v34, v5

    move-object/from16 v35, v6

    move-object/from16 v37, v23

    move-object/from16 v40, p5

    invoke-virtual/range {v34 .. v40}, Lcom/android/videoeditor/service/ApiServiceListener;->onOverlayStartTimeSet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Exception;)V

    goto :goto_2b

    :cond_4a
    invoke-virtual/range {v69 .. v69}, Lcom/android/videoeditor/service/MovieOverlay;->getStartTime()J

    move-result-wide v11

    move-object/from16 v0, v69

    invoke-virtual {v0, v11, v12}, Lcom/android/videoeditor/service/MovieOverlay;->setAppStartTime(J)V

    goto :goto_2a

    :sswitch_21
    if-eqz p6, :cond_4b

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_4b
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    const-string v11, "duration"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v32

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_4c

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->getOverlay(Ljava/lang/String;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v69

    if-eqz v69, :cond_4c

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_4d

    move-object/from16 v0, v69

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/MovieOverlay;->setDuration(J)V

    :cond_4c
    :goto_2c
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_2d
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v40, v5

    move-object/from16 v41, v6

    move-object/from16 v42, v36

    move-object/from16 v43, v23

    move-wide/from16 v44, v32

    move-object/from16 v46, p5

    invoke-virtual/range {v40 .. v46}, Lcom/android/videoeditor/service/ApiServiceListener;->onOverlayDurationSet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Exception;)V

    goto :goto_2d

    :cond_4d
    invoke-virtual/range {v69 .. v69}, Lcom/android/videoeditor/service/MovieOverlay;->getDuration()J

    move-result-wide v11

    move-object/from16 v0, v69

    invoke-virtual {v0, v11, v12}, Lcom/android/videoeditor/service/MovieOverlay;->setAppDuration(J)V

    goto :goto_2c

    :sswitch_22
    if-eqz p6, :cond_4e

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_4e
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    const-string v11, "attributes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v44

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_4f

    if-nez p5, :cond_4f

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->getOverlay(Ljava/lang/String;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v69

    if-eqz v69, :cond_4f

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    move-object/from16 v0, v69

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/MovieOverlay;->updateUserAttributes(Landroid/os/Bundle;)V

    :cond_4f
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_2e
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v40, v5

    move-object/from16 v41, v6

    move-object/from16 v42, v36

    move-object/from16 v43, v23

    move-object/from16 v45, p5

    invoke-virtual/range {v40 .. v45}, Lcom/android/videoeditor/service/ApiServiceListener;->onOverlayUserAttributesSet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Exception;)V

    goto :goto_2e

    :sswitch_23
    if-eqz p6, :cond_50

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_50
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v63, p3

    check-cast v63, Lcom/android/videoeditor/service/MovieEffect;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_51

    if-nez p5, :cond_51

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    move-object/from16 v2, v63

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->addEffect(Ljava/lang/String;Lcom/android/videoeditor/service/MovieEffect;)V

    :cond_51
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_2f
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v63

    move-object/from16 v1, v23

    move-object/from16 v2, p5

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onEffectAdded(Ljava/lang/String;Lcom/android/videoeditor/service/MovieEffect;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2f

    :sswitch_24
    if-eqz p6, :cond_52

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_52
    const-string v11, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_53

    if-nez p5, :cond_53

    move-object/from16 v0, v76

    move-object/from16 v1, v23

    move-object/from16 v2, v57

    invoke-virtual {v0, v1, v2}, Lcom/android/videoeditor/service/VideoEditorProject;->removeEffect(Ljava/lang/String;Ljava/lang/String;)V

    :cond_53
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_30
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v57

    move-object/from16 v1, v23

    move-object/from16 v2, p5

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/android/videoeditor/service/ApiServiceListener;->onEffectRemoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_30

    :sswitch_25
    if-eqz p6, :cond_54

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_54
    move-object/from16 v62, p3

    check-cast v62, Lcom/android/videoeditor/service/MovieAudioTrack;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_55

    if-nez p5, :cond_55

    move-object/from16 v0, v76

    move-object/from16 v1, v62

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->addAudioTrack(Lcom/android/videoeditor/service/MovieAudioTrack;)V

    :cond_55
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_31
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v62

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onAudioTrackAdded(Ljava/lang/String;Lcom/android/videoeditor/service/MovieAudioTrack;Ljava/lang/Exception;)V

    goto :goto_31

    :sswitch_26
    if-eqz p6, :cond_56

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_56
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_57

    if-nez p5, :cond_57

    move-object/from16 v0, v76

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->removeAudioTrack(Ljava/lang/String;)V

    :cond_57
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_32
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v47

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onAudioTrackRemoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_32

    :sswitch_27
    if-eqz p6, :cond_58

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_58
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    const-string v11, "b_boundary"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v48

    const-string v11, "e_boundary"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v50

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_59

    if-nez p5, :cond_59

    move-object/from16 v0, v76

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getAudioTrack(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieAudioTrack;

    move-result-object v54

    if-eqz v54, :cond_59

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    move-object/from16 v0, v54

    move-wide/from16 v1, v48

    move-wide/from16 v3, v50

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/videoeditor/service/MovieAudioTrack;->setExtractBoundaries(JJ)V

    :cond_59
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_33
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v45, v5

    move-object/from16 v46, v6

    move-object/from16 v52, p5

    invoke-virtual/range {v45 .. v52}, Lcom/android/videoeditor/service/ApiServiceListener;->onAudioTrackBoundariesSet(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/Exception;)V

    goto :goto_33

    :sswitch_28
    if-eqz p6, :cond_5a

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_5a
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_0

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, v76

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getAudioTrack(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieAudioTrack;

    move-result-object v54

    if-eqz v54, :cond_0

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_5b

    const-string v11, "loop"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieAudioTrack;->enableLoop(Z)V

    goto/16 :goto_0

    :cond_5b
    invoke-virtual/range {v54 .. v54}, Lcom/android/videoeditor/service/MovieAudioTrack;->isLooping()Z

    move-result v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieAudioTrack;->enableAppLoop(Z)V

    goto/16 :goto_0

    :sswitch_29
    if-eqz p6, :cond_5c

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_5c
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_0

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, v76

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getAudioTrack(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieAudioTrack;

    move-result-object v54

    if-eqz v54, :cond_0

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_5d

    const-string v11, "duck"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieAudioTrack;->enableDucking(Z)V

    goto/16 :goto_0

    :cond_5d
    invoke-virtual/range {v54 .. v54}, Lcom/android/videoeditor/service/MovieAudioTrack;->isDuckingEnabled()Z

    move-result v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieAudioTrack;->enableAppDucking(Z)V

    goto/16 :goto_0

    :sswitch_2a
    if-eqz p6, :cond_5e

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_5e
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_0

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, v76

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getAudioTrack(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieAudioTrack;

    move-result-object v54

    if-eqz v54, :cond_0

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_5f

    const-string v11, "volume"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieAudioTrack;->setVolume(I)V

    goto/16 :goto_0

    :cond_5f
    invoke-virtual/range {v54 .. v54}, Lcom/android/videoeditor/service/MovieAudioTrack;->getVolume()I

    move-result v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieAudioTrack;->setAppVolume(I)V

    goto/16 :goto_0

    :sswitch_2b
    if-eqz p6, :cond_60

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_60
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-eqz v76, :cond_0

    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, v76

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getAudioTrack(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieAudioTrack;

    move-result-object v54

    if-eqz v54, :cond_0

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    if-nez p5, :cond_61

    const-string v11, "mute"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieAudioTrack;->setMute(Z)V

    goto/16 :goto_0

    :cond_61
    invoke-virtual/range {v54 .. v54}, Lcom/android/videoeditor/service/MovieAudioTrack;->isMuted()Z

    move-result v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/MovieAudioTrack;->setAppMute(Z)V

    goto/16 :goto_0

    :sswitch_2c
    if-eqz p6, :cond_62

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_62
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    const-string v11, "prog_value"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_34
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v47

    invoke-virtual {v5, v6, v0, v10}, Lcom/android/videoeditor/service/ApiServiceListener;->onAudioTrackExtractAudioWaveformProgress(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_34

    :sswitch_2d
    if-eqz p6, :cond_63

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->finalizeRequest(Landroid/content/Intent;)V

    :cond_63
    const-string v11, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v76

    if-nez p5, :cond_64

    if-eqz v76, :cond_64

    if-eqz p3, :cond_64

    move-object/from16 v0, v76

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getAudioTrack(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieAudioTrack;

    move-result-object v54

    if-eqz v54, :cond_64

    const/4 v11, 0x0

    move-object/from16 v0, v76

    invoke-virtual {v0, v11}, Lcom/android/videoeditor/service/VideoEditorProject;->setClean(Z)V

    check-cast p3, Landroid/media/videoeditor/WaveformData;

    move-object/from16 v0, v54

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/service/MovieAudioTrack;->setWaveformData(Landroid/media/videoeditor/WaveformData;)V

    :cond_64
    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mListeners:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_35
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/ApiServiceListener;

    move-object/from16 v0, v47

    move-object/from16 v1, p5

    invoke-virtual {v5, v6, v0, v1}, Lcom/android/videoeditor/service/ApiServiceListener;->onAudioTrackExtractAudioWaveformComplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_35

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_9
        0x4 -> :sswitch_0
        0x5 -> :sswitch_7
        0x6 -> :sswitch_8
        0x8 -> :sswitch_a
        0x9 -> :sswitch_b
        0xa -> :sswitch_4
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xd -> :sswitch_1
        0x64 -> :sswitch_c
        0x65 -> :sswitch_d
        0x66 -> :sswitch_10
        0x67 -> :sswitch_11
        0x68 -> :sswitch_12
        0x69 -> :sswitch_13
        0x6a -> :sswitch_14
        0x6b -> :sswitch_16
        0x6c -> :sswitch_17
        0x6d -> :sswitch_19
        0x6e -> :sswitch_18
        0x70 -> :sswitch_15
        0x71 -> :sswitch_e
        0x72 -> :sswitch_f
        0xc8 -> :sswitch_23
        0xc9 -> :sswitch_23
        0xca -> :sswitch_24
        0x12c -> :sswitch_1a
        0x12d -> :sswitch_1a
        0x12e -> :sswitch_1a
        0x12f -> :sswitch_1a
        0x130 -> :sswitch_1b
        0x131 -> :sswitch_1c
        0x132 -> :sswitch_1d
        0x190 -> :sswitch_1e
        0x191 -> :sswitch_1f
        0x192 -> :sswitch_20
        0x193 -> :sswitch_21
        0x194 -> :sswitch_22
        0x1f4 -> :sswitch_25
        0x1f5 -> :sswitch_26
        0x1f6 -> :sswitch_2a
        0x1f7 -> :sswitch_2b
        0x1f9 -> :sswitch_27
        0x1fa -> :sswitch_28
        0x1fb -> :sswitch_29
        0x1fc -> :sswitch_2d
        0x1fd -> :sswitch_2c
    .end sparse-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 14
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v11, "op"

    const/4 v12, -0x1

    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    const-string v11, "VEApiService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "No thread assigned: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v11, 0x2

    return v11

    :sswitch_0
    iget-object v11, p0, Lcom/android/videoeditor/service/ApiService;->mVideoThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v11, p1}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->submit(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_1
    iget-object v11, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v11, p1}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->submit(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_2
    const-string v11, "project"

    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v11, "item_id"

    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v11, "token"

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    iget-object v11, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v11}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->getIntentQueueIterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Intent;

    const-string v11, "op"

    const/4 v12, -0x1

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v11, "project"

    invoke-virtual {v8, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v11, "item_id"

    invoke-virtual {v8, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v11, "token"

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    if-ne v5, v4, :cond_0

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    if-eq v9, v10, :cond_0

    iget-object v11, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v11, v8}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->cancel(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Canceled operation: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " for media item"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mPendingIntents:Ljava/util/Map;

    const-string v12, "rid"

    invoke-virtual {v8, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v11, Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;

    invoke-virtual {v11, v8}, Lcom/android/videoeditor/service/IntentPool;->put(Landroid/content/Intent;)V

    :cond_1
    iget-object v11, p0, Lcom/android/videoeditor/service/ApiService;->mThumbnailThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v11, p1}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->submit(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v11, p0, Lcom/android/videoeditor/service/ApiService;->mAudioThread:Lcom/android/videoeditor/service/ApiService$IntentProcessor;

    invoke-virtual {v11, p1}, Lcom/android/videoeditor/service/ApiService$IntentProcessor;->submit(Landroid/content/Intent;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xd -> :sswitch_0
        0x64 -> :sswitch_0
        0x65 -> :sswitch_0
        0x66 -> :sswitch_0
        0x67 -> :sswitch_0
        0x68 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6a -> :sswitch_0
        0x6b -> :sswitch_3
        0x6c -> :sswitch_3
        0x6d -> :sswitch_0
        0x6e -> :sswitch_0
        0x70 -> :sswitch_2
        0x71 -> :sswitch_0
        0x72 -> :sswitch_0
        0xc8 -> :sswitch_0
        0xc9 -> :sswitch_0
        0xca -> :sswitch_0
        0x12c -> :sswitch_0
        0x12d -> :sswitch_0
        0x12e -> :sswitch_0
        0x12f -> :sswitch_0
        0x130 -> :sswitch_0
        0x131 -> :sswitch_0
        0x132 -> :sswitch_1
        0x190 -> :sswitch_0
        0x191 -> :sswitch_0
        0x192 -> :sswitch_0
        0x193 -> :sswitch_0
        0x194 -> :sswitch_0
        0x1f4 -> :sswitch_0
        0x1f5 -> :sswitch_0
        0x1f6 -> :sswitch_3
        0x1f7 -> :sswitch_3
        0x1f9 -> :sswitch_0
        0x1fa -> :sswitch_3
        0x1fb -> :sswitch_3
        0x1fc -> :sswitch_0
        0x1fd -> :sswitch_0
    .end sparse-switch
.end method

.method public processIntent(Landroid/content/Intent;)V
    .locals 124
    .param p1    # Landroid/content/Intent;

    const-string v4, "op"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v113

    const/4 v6, 0x0

    :try_start_0
    const-string v4, "project"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v118

    sparse-switch v113, :sswitch_data_0

    move-object/from16 v0, p0

    move-object/from16 v1, v118

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/ApiService;->getVideoEditor(Ljava/lang/String;)Landroid/media/videoeditor/VideoEditor;

    move-result-object v6

    if-nez v6, :cond_0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid project path: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v118

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " for operation: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v113

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v88

    invoke-virtual/range {v88 .. v88}, Ljava/lang/Exception;->printStackTrace()V

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x1

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    :goto_0
    return-void

    :cond_0
    :sswitch_0
    sparse-switch v113, :sswitch_data_1

    :try_start_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unhandled operation: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v113

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :sswitch_1
    const-string v4, "OP_LOAD_PROJECTS"

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/util/FileUtils;->getProjectsRootDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v96

    if-eqz v96, :cond_3

    invoke-virtual/range {v96 .. v96}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v100

    if-eqz v100, :cond_3

    const/16 v102, 0x0

    :goto_1
    move-object/from16 v0, v100

    array-length v4, v0

    move/from16 v0, v102

    if-ge v0, v4, :cond_2

    aget-object v4, v100, v102

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    aget-object v4, v100, v102

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v117

    const/4 v4, 0x0

    :try_start_2
    move-object/from16 v0, v117

    invoke-static {v4, v0}, Lcom/android/videoeditor/service/VideoEditorProject;->fromXml(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v4

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    :goto_2
    add-int/lit8 v102, v102, 0x1

    goto :goto_1

    :catch_1
    move-exception v88

    :try_start_3
    const-string v4, "VEApiService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "processIntent: Project file not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v117

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, v117

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/android/videoeditor/util/FileUtils;->deleteDir(Ljava/io/File;)Z

    goto :goto_2

    :catch_2
    move-exception v88

    invoke-virtual/range {v88 .. v88}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    new-instance v4, Lcom/android/videoeditor/service/ApiService$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/android/videoeditor/service/ApiService$2;-><init>(Lcom/android/videoeditor/service/ApiService;)V

    invoke-static {v8, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_3
    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v10}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_VIDEO_EDITOR_CREATE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v118

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    invoke-direct/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->releaseEditor()V

    invoke-static/range {v118 .. v118}, Landroid/media/videoeditor/VideoEditorFactory;->create(Ljava/lang/String;)Landroid/media/videoeditor/VideoEditor;

    move-result-object v6

    const-string v4, "movies"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v111

    const/16 v102, 0x0

    :goto_3
    move-object/from16 v0, v111

    array-length v4, v0

    move/from16 v0, v102

    if-ge v0, v4, :cond_4

    new-instance v9, Landroid/media/videoeditor/MediaVideoItem;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v4

    aget-object v5, v111, v102

    const/4 v7, 0x0

    invoke-direct {v9, v6, v4, v5, v7}, Landroid/media/videoeditor/MediaVideoItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v6, v9}, Landroid/media/videoeditor/VideoEditor;->addMediaItem(Landroid/media/videoeditor/MediaItem;)V

    add-int/lit8 v102, v102, 0x1

    goto :goto_3

    :cond_4
    const-string v4, "images"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v116

    const/16 v102, 0x0

    :goto_4
    move-object/from16 v0, v116

    array-length v4, v0

    move/from16 v0, v102

    if-ge v0, v4, :cond_5

    new-instance v9, Landroid/media/videoeditor/MediaImageItem;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v11

    aget-object v12, v116, v102

    invoke-static {}, Lcom/android/videoeditor/util/MediaItemUtils;->getDefaultImageDuration()J

    move-result-wide v13

    const/4 v15, 0x0

    move-object v10, v6

    invoke-direct/range {v9 .. v15}, Landroid/media/videoeditor/MediaImageItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JI)V

    invoke-interface {v6, v9}, Landroid/media/videoeditor/VideoEditor;->addMediaItem(Landroid/media/videoeditor/MediaItem;)V

    add-int/lit8 v102, v102, 0x1

    goto :goto_4

    :cond_5
    const-string v4, "name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v4, "theme"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v6, v1}, Lcom/android/videoeditor/service/ApiService;->applyThemeToMovie(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)V

    :cond_6
    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v106

    invoke-interface/range {v106 .. v106}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_7

    const/4 v4, 0x0

    move-object/from16 v0, v106

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getAspectRatio()I

    move-result v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->setAspectRatio(I)V

    :cond_7
    new-instance v10, Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    const/16 v20, 0x14

    const/16 v21, 0x0

    const/16 v23, 0x0

    move-object v11, v6

    move-object/from16 v12, v118

    invoke-direct/range {v10 .. v23}, Lcom/android/videoeditor/service/VideoEditorProject;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JJJILandroid/net/Uri;Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/videoeditor/service/ApiService;->copyMediaItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/android/videoeditor/service/VideoEditorProject;->setMediaItems(Ljava/util/List;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllAudioTracks()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/videoeditor/service/ApiService;->copyAudioTracks(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/android/videoeditor/service/VideoEditorProject;->setAudioTracks(Ljava/util/List;)V

    sput-object v6, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    new-instance v4, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    move-object/from16 v0, p0

    move-object/from16 v1, v118

    invoke-direct {v4, v0, v1}, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;-><init>(Lcom/android/videoeditor/service/ApiService;Ljava/lang/String;)V

    sput-object v4, Lcom/android/videoeditor/service/ApiService;->mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v18, v10

    invoke-direct/range {v14 .. v20}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v88

    if-eqz v6, :cond_8

    :try_start_5
    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->release()V

    const/4 v6, 0x0

    :cond_8
    throw v88

    :sswitch_3
    move-object/from16 v0, p0

    move-object/from16 v1, v118

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/ApiService;->releaseEditorNot(Ljava/lang/String;)Landroid/media/videoeditor/VideoEditor;

    move-result-object v6

    if-nez v6, :cond_a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_VIDEO_EDITOR_LOAD: Loading: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v118

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/4 v4, 0x0

    :try_start_6
    move-object/from16 v0, v118

    invoke-static {v0, v4}, Landroid/media/videoeditor/VideoEditorFactory;->load(Ljava/lang/String;Z)Landroid/media/videoeditor/VideoEditor;

    move-result-object v6

    move-object/from16 v0, v118

    invoke-static {v6, v0}, Lcom/android/videoeditor/service/VideoEditorProject;->fromXml(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v10

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/videoeditor/service/ApiService;->copyMediaItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/android/videoeditor/service/VideoEditorProject;->setMediaItems(Ljava/util/List;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllAudioTracks()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/videoeditor/service/ApiService;->copyAudioTracks(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/android/videoeditor/service/VideoEditorProject;->setAudioTracks(Ljava/util/List;)V

    sput-object v6, Lcom/android/videoeditor/service/ApiService;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    new-instance v4, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    move-object/from16 v0, p0

    move-object/from16 v1, v118

    invoke-direct {v4, v0, v1}, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;-><init>(Lcom/android/videoeditor/service/ApiService;Ljava/lang/String;)V

    sput-object v4, Lcom/android/videoeditor/service/ApiService;->mGeneratePreviewListener:Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    move-object/from16 v18, v10

    invoke-direct/range {v14 .. v20}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v88

    if-eqz v6, :cond_9

    :try_start_7
    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->release()V

    const/4 v6, 0x0

    :cond_9
    throw v88

    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_VIDEO_EDITOR_LOAD: Was already loaded: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v118

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x1

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    invoke-direct/range {v14 .. v20}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_4
    const-string v4, "OP_VIDEO_EDITOR_SET_ASPECT_RATIO"

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "aspect_ratio"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->setAspectRatio(I)V

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    invoke-direct/range {v14 .. v20}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_5
    const-string v4, "OP_VIDEO_EDITOR_APPLY_THEME"

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "theme"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->applyThemeToMovie(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/videoeditor/service/ApiService;->copyMediaItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v18

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllAudioTracks()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/videoeditor/service/ApiService;->copyAudioTracks(Ljava/util/List;)Ljava/util/List;

    move-result-object v19

    const/16 v17, 0x0

    const/16 v20, 0x0

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v6

    invoke-direct/range {v14 .. v20}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_6
    const-string v4, "OP_VIDEO_EDITOR_EXPORT"

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v6, v1}, Lcom/android/videoeditor/service/ApiService;->exportMovie(Landroid/media/videoeditor/VideoEditor;Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_7
    const-string v4, "OP_VIDEO_EDITOR_CANCEL_EXPORT"

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->cancelExport(Ljava/lang/String;)V

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x1

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    move-object/from16 v25, v6

    invoke-direct/range {v23 .. v29}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_8
    const-string v4, "OP_VIDEO_EDITOR_EXPORT_STATUS"

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x1

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    move-object/from16 v25, v6

    invoke-direct/range {v23 .. v29}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_VIDEO_EDITOR_SAVE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v118

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->save()V

    move-object/from16 v0, p0

    move-object/from16 v1, v118

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/ApiService;->getProject(Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v10

    if-eqz v10, :cond_b

    invoke-virtual {v10}, Lcom/android/videoeditor/service/VideoEditorProject;->saveToXml()V

    :cond_b
    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x1

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    move-object/from16 v25, v6

    invoke-direct/range {v23 .. v29}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_VIDEO_EDITOR_RELEASE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v118

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v118

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/ApiService;->releaseEditor(Ljava/lang/String;)V

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x1

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    move-object/from16 v25, v6

    invoke-direct/range {v23 .. v29}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_VIDEO_EDITOR_DELETE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v118

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v118

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/ApiService;->releaseEditor(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, v118

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/android/videoeditor/util/FileUtils;->deleteDir(Ljava/io/File;)Z

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x1

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    move-object/from16 v25, v6

    invoke-direct/range {v23 .. v29}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_ADD_VIDEO_URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v24

    check-cast v24, Landroid/net/Uri;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    const/16 v34, 0x0

    const/16 v95, 0x0

    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/4 v4, 0x0

    const-string v5, "_data"

    aput-object v5, v25, v4

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    invoke-virtual/range {v23 .. v28}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v95

    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v4, 0x0

    move-object/from16 v0, v95

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v34

    :cond_c
    if-eqz v95, :cond_d

    :try_start_9
    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->close()V

    :cond_d
    if-nez v34, :cond_f

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Media file not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :catchall_0
    move-exception v4

    if-eqz v95, :cond_e

    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v4

    :cond_f
    new-instance v9, Landroid/media/videoeditor/MediaVideoItem;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "rm"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    move-object/from16 v0, v34

    invoke-direct {v9, v6, v4, v0, v5}, Landroid/media/videoeditor/MediaVideoItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v9, v4}, Landroid/media/videoeditor/VideoEditor;->insertMediaItem(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_11

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaItem;->getAspectRatio()I

    move-result v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->setAspectRatio(I)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    :goto_5
    const-string v4, "theme"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_10

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v6, v1, v9}, Lcom/android/videoeditor/service/ApiService;->applyThemeToMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Landroid/media/videoeditor/MediaItem;)V

    :cond_10
    const/16 v28, 0x0

    new-instance v29, Lcom/android/videoeditor/service/MovieMediaItem;

    move-object/from16 v0, v29

    invoke-direct {v0, v9}, Lcom/android/videoeditor/service/MovieMediaItem;-><init>(Landroid/media/videoeditor/MediaItem;)V

    const/16 v31, 0x0

    move-object/from16 v25, p0

    move-object/from16 v26, p1

    move-object/from16 v27, v6

    invoke-direct/range {v25 .. v31}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_11
    const/16 v30, 0x0

    goto :goto_5

    :sswitch_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_ADD_IMAGE_URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v24

    check-cast v24, Landroid/net/Uri;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    const/16 v34, 0x0

    const/16 v95, 0x0

    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/4 v4, 0x0

    const-string v5, "_data"

    aput-object v5, v25, v4

    const/4 v4, 0x1

    const-string v5, "mime_type"

    aput-object v5, v25, v4

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    invoke-virtual/range {v23 .. v28}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v95

    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_12

    const/4 v4, 0x0

    move-object/from16 v0, v95

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    const/4 v4, 0x1

    move-object/from16 v0, v95

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v109

    const-string v4, "image/jpeg"

    move-object/from16 v0, v109

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v4

    if-eqz v4, :cond_12

    :try_start_b
    new-instance v114, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "gallery_image_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v114

    move-object/from16 v1, v118

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v34

    move-object/from16 v1, v114

    invoke-static {v0, v1}, Lcom/android/videoeditor/util/ImageUtils;->transformJpeg(Ljava/lang/String;Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-virtual/range {v114 .. v114}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v34

    :cond_12
    :goto_6
    if-eqz v95, :cond_13

    :try_start_c
    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->close()V

    :cond_13
    if-nez v34, :cond_15

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Media file not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    :catch_5
    move-exception v88

    :try_start_d
    const-string v4, "VEApiService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not transform JPEG: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v34

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v88

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_6

    :catchall_1
    move-exception v4

    if-eqz v95, :cond_14

    :try_start_e
    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->close()V

    :cond_14
    throw v4

    :cond_15
    new-instance v9, Landroid/media/videoeditor/MediaImageItem;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v35

    const-string v4, "rm"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v37

    move-object/from16 v31, v9

    move-object/from16 v32, v6

    invoke-direct/range {v31 .. v37}, Landroid/media/videoeditor/MediaImageItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JI)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v9, v4}, Landroid/media/videoeditor/VideoEditor;->insertMediaItem(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_17

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaItem;->getAspectRatio()I

    move-result v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->setAspectRatio(I)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    :goto_7
    const-string v4, "theme"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_16

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v6, v1, v9}, Lcom/android/videoeditor/service/ApiService;->applyThemeToMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Landroid/media/videoeditor/MediaItem;)V

    :cond_16
    const/16 v28, 0x0

    new-instance v29, Lcom/android/videoeditor/service/MovieMediaItem;

    move-object/from16 v0, v29

    invoke-direct {v0, v9}, Lcom/android/videoeditor/service/MovieMediaItem;-><init>(Landroid/media/videoeditor/MediaItem;)V

    const/16 v31, 0x0

    move-object/from16 v25, p0

    move-object/from16 v26, p1

    move-object/from16 v27, v6

    invoke-direct/range {v25 .. v31}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_17
    const/16 v30, 0x0

    goto :goto_7

    :sswitch_e
    const-string v4, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v24

    check-cast v24, Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_LOAD: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v120, p1

    new-instance v4, Lcom/android/videoeditor/service/ApiService$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v118

    move-object/from16 v2, v120

    move-object/from16 v3, v24

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/android/videoeditor/service/ApiService$3;-><init>(Lcom/android/videoeditor/service/ApiService;Ljava/lang/String;Landroid/content/Intent;Landroid/net/Uri;)V

    invoke-virtual {v4}, Lcom/android/videoeditor/service/ApiService$3;->start()V

    goto/16 :goto_0

    :sswitch_f
    const-string v4, "req_intent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v40

    check-cast v40, Landroid/content/Intent;

    const-string v4, "ex"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    const-string v4, "ex"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v38

    check-cast v38, Ljava/lang/Exception;

    const/16 v39, 0x0

    const/16 v41, 0x1

    move-object/from16 v35, p0

    move-object/from16 v36, p1

    move-object/from16 v37, v6

    invoke-direct/range {v35 .. v41}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :cond_18
    const/16 v44, 0x0

    const-string v4, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    const/16 v47, 0x1

    move-object/from16 v41, p0

    move-object/from16 v42, p1

    move-object/from16 v43, v6

    move-object/from16 v46, v40

    invoke-direct/range {v41 .. v47}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_10
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_MOVE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v106

    invoke-interface/range {v106 .. v106}, Ljava/util/List;->size()I

    move-result v107

    const/16 v45, -0x1

    const/16 v44, 0x0

    const/16 v102, 0x0

    :goto_8
    move/from16 v0, v102

    move/from16 v1, v107

    if-ge v0, v1, :cond_19

    move-object/from16 v0, v106

    move/from16 v1, v102

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v108

    check-cast v108, Landroid/media/videoeditor/MediaItem;

    invoke-virtual/range {v108 .. v108}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    move-object/from16 v44, v108

    move/from16 v45, v102

    :cond_19
    const/4 v4, -0x1

    move/from16 v0, v45

    if-ne v0, v4, :cond_1b

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Moved MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1a
    add-int/lit8 v102, v102, 0x1

    goto :goto_8

    :cond_1b
    invoke-virtual/range {v44 .. v44}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    move-result-object v46

    invoke-virtual/range {v44 .. v44}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    move-result-object v47

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v92

    move-object/from16 v0, v105

    move-object/from16 v1, v92

    invoke-interface {v6, v0, v1}, Landroid/media/videoeditor/VideoEditor;->moveMediaItem(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "theme"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_1c

    move-object/from16 v41, p0

    move-object/from16 v42, v6

    move-object/from16 v43, v22

    invoke-direct/range {v41 .. v47}, Lcom/android/videoeditor/service/ApiService;->applyThemeAfterMove(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Landroid/media/videoeditor/MediaItem;ILandroid/media/videoeditor/Transition;Landroid/media/videoeditor/Transition;)V

    :cond_1c
    move-object/from16 v0, p0

    move-object/from16 v1, v106

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/ApiService;->copyMediaItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v52

    const/16 v51, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    move-object/from16 v48, p0

    move-object/from16 v49, p1

    move-object/from16 v50, v6

    invoke-direct/range {v48 .. v54}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_11
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_REMOVE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getAllMediaItems()Ljava/util/List;

    move-result-object v106

    invoke-interface/range {v106 .. v106}, Ljava/util/List;->size()I

    move-result v107

    const/16 v56, -0x1

    const/16 v119, 0x0

    const/16 v102, 0x0

    :goto_9
    move/from16 v0, v102

    move/from16 v1, v107

    if-ge v0, v1, :cond_1d

    move-object/from16 v0, v106

    move/from16 v1, v102

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    move-object/from16 v0, v106

    move/from16 v1, v102

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v119

    check-cast v119, Landroid/media/videoeditor/MediaItem;

    move/from16 v56, v102

    :cond_1d
    if-nez v119, :cond_1f

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1e
    add-int/lit8 v102, v102, 0x1

    goto :goto_9

    :cond_1f
    invoke-virtual/range {v119 .. v119}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    move-result-object v46

    invoke-virtual/range {v119 .. v119}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    move-result-object v47

    move-object/from16 v0, v105

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->removeMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    const/16 v61, 0x0

    const-string v4, "theme"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_20

    invoke-interface/range {v106 .. v106}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_20

    move-object/from16 v53, p0

    move-object/from16 v54, v6

    move-object/from16 v55, v22

    move-object/from16 v57, v46

    move-object/from16 v58, v47

    invoke-direct/range {v53 .. v58}, Lcom/android/videoeditor/service/ApiService;->applyThemeAfterRemove(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;ILandroid/media/videoeditor/Transition;Landroid/media/videoeditor/Transition;)Landroid/media/videoeditor/Transition;

    move-result-object v67

    if-eqz v67, :cond_20

    new-instance v61, Lcom/android/videoeditor/service/MovieTransition;

    move-object/from16 v0, v61

    move-object/from16 v1, v67

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/MovieTransition;-><init>(Landroid/media/videoeditor/Transition;)V

    :cond_20
    const/16 v60, 0x0

    const/16 v62, 0x0

    const/16 v63, 0x0

    move-object/from16 v57, p0

    move-object/from16 v58, p1

    move-object/from16 v59, v6

    invoke-direct/range {v57 .. v63}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_12
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_SET_RENDERING_MODE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v105

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_21

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_21
    const-string v4, "rm"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->setRenderingMode(I)V

    const/16 v65, 0x0

    const/16 v66, 0x0

    const/16 v67, 0x0

    const/16 v68, 0x0

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v64, v6

    invoke-direct/range {v62 .. v68}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_13
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_SET_DURATION: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v105

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    check-cast v9, Landroid/media/videoeditor/MediaImageItem;

    if-nez v9, :cond_22

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_22
    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v97

    move-wide/from16 v0, v97

    invoke-virtual {v9, v0, v1}, Landroid/media/videoeditor/MediaImageItem;->setDuration(J)V

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaImageItem;->getAllEffects()Ljava/util/List;

    move-result-object v99

    invoke-interface/range {v99 .. v99}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v103

    :goto_a
    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v76

    check-cast v76, Landroid/media/videoeditor/Effect;

    move-object/from16 v0, v76

    move-wide/from16 v1, v97

    invoke-virtual {v0, v1, v2}, Landroid/media/videoeditor/Effect;->setDuration(J)V

    goto :goto_a

    :cond_23
    const/16 v65, 0x0

    new-instance v66, Lcom/android/videoeditor/service/MovieMediaItem;

    move-object/from16 v0, v66

    invoke-direct {v0, v9}, Lcom/android/videoeditor/service/MovieMediaItem;-><init>(Landroid/media/videoeditor/MediaItem;)V

    const/16 v67, 0x0

    const/16 v68, 0x0

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v64, v6

    invoke-direct/range {v62 .. v68}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_14
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    move-object/from16 v0, v105

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    check-cast v9, Landroid/media/videoeditor/MediaVideoItem;

    if-nez v9, :cond_24

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_24
    const-string v4, "b_boundary"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v7, "e_boundary"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    invoke-virtual {v9, v4, v5, v11, v12}, Landroid/media/videoeditor/MediaVideoItem;->setExtractBoundaries(JJ)V

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getAllOverlays()Ljava/util/List;

    move-result-object v115

    invoke-interface/range {v115 .. v115}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_25

    const/4 v4, 0x0

    move-object/from16 v0, v115

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v84

    check-cast v84, Landroid/media/videoeditor/Overlay;

    invoke-virtual/range {v84 .. v84}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    move-result-wide v4

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    move-result-wide v11

    cmp-long v4, v4, v11

    if-gez v4, :cond_26

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    move-result-wide v4

    move-object/from16 v0, v84

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/Overlay;->setStartTime(J)V

    invoke-virtual/range {v84 .. v84}, Landroid/media/videoeditor/Overlay;->getDuration()J

    move-result-wide v4

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getTimelineDuration()J

    move-result-wide v11

    invoke-static {v4, v5, v11, v12}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    move-object/from16 v0, v84

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/Overlay;->setDuration(J)V

    :cond_25
    :goto_b
    const/16 v65, 0x0

    new-instance v66, Lcom/android/videoeditor/service/MovieMediaItem;

    move-object/from16 v0, v66

    invoke-direct {v0, v9}, Lcom/android/videoeditor/service/MovieMediaItem;-><init>(Landroid/media/videoeditor/MediaItem;)V

    const/16 v67, 0x0

    const/16 v68, 0x0

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v64, v6

    invoke-direct/range {v62 .. v68}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_26
    invoke-virtual/range {v84 .. v84}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    move-result-wide v4

    invoke-virtual/range {v84 .. v84}, Landroid/media/videoeditor/Overlay;->getDuration()J

    move-result-wide v11

    add-long/2addr v4, v11

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    move-result-wide v11

    cmp-long v4, v4, v11

    if-lez v4, :cond_25

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    move-result-wide v4

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    move-result-wide v11

    invoke-virtual/range {v84 .. v84}, Landroid/media/videoeditor/Overlay;->getDuration()J

    move-result-wide v14

    sub-long/2addr v11, v14

    invoke-static {v4, v5, v11, v12}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    move-object/from16 v0, v84

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/Overlay;->setStartTime(J)V

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    move-result-wide v4

    invoke-virtual/range {v84 .. v84}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    move-result-wide v11

    sub-long/2addr v4, v11

    move-object/from16 v0, v84

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/Overlay;->setDuration(J)V

    goto :goto_b

    :sswitch_15
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_GET_THUMBNAILS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v105

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_27

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_27
    move-object/from16 v123, v6

    const-string v4, "width"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v63

    const-string v4, "height"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v64

    const-string v4, "s_time"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v65

    const-string v4, "e_time"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v67

    const-string v4, "count"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v69

    const-string v4, "indices"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v70

    new-instance v71, Lcom/android/videoeditor/service/ApiService$4;

    move-object/from16 v0, v71

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, v123

    invoke-direct {v0, v1, v2, v3}, Lcom/android/videoeditor/service/ApiService$4;-><init>(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;)V

    move-object/from16 v62, v9

    invoke-virtual/range {v62 .. v71}, Landroid/media/videoeditor/MediaItem;->getThumbnailList(IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;)V

    const/16 v65, 0x0

    const/16 v66, 0x0

    const/16 v67, 0x0

    const/16 v68, 0x1

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v64, v6

    invoke-direct/range {v62 .. v68}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :sswitch_16
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_SET_VOLUME: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v105

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-eqz v9, :cond_28

    instance-of v4, v9, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v4, :cond_28

    check-cast v9, Landroid/media/videoeditor/MediaVideoItem;

    const-string v4, "volume"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaVideoItem;->setVolume(I)V

    const/16 v65, 0x0

    const/16 v66, 0x0

    const/16 v67, 0x0

    const/16 v68, 0x0

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v64, v6

    invoke-direct/range {v62 .. v68}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_28
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :sswitch_17
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_SET_MUTE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v105

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-eqz v9, :cond_29

    instance-of v4, v9, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v4, :cond_29

    check-cast v9, Landroid/media/videoeditor/MediaVideoItem;

    const-string v4, "mute"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaVideoItem;->setMute(Z)V

    const/16 v65, 0x0

    const/16 v66, 0x0

    const/16 v67, 0x0

    const/16 v68, 0x0

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v64, v6

    invoke-direct/range {v62 .. v68}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_29
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :sswitch_18
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v105

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_MEDIA_ITEM_EXTRACT_AUDIO_WAVEFORM: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v105

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v105

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-eqz v9, :cond_2b

    instance-of v4, v9, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v4, :cond_2b

    move-object v0, v9

    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    move-object/from16 v110, v0

    invoke-virtual/range {v110 .. v110}, Landroid/media/videoeditor/MediaVideoItem;->getWaveformData()Landroid/media/videoeditor/WaveformData;

    move-result-object v66

    if-nez v66, :cond_2a

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v110

    invoke-direct {v0, v1, v6, v2}, Lcom/android/videoeditor/service/ApiService;->extractMediaItemAudioWaveform(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Landroid/media/videoeditor/MediaVideoItem;)V

    const/16 v65, 0x0

    invoke-virtual/range {v110 .. v110}, Landroid/media/videoeditor/MediaVideoItem;->getWaveformData()Landroid/media/videoeditor/WaveformData;

    move-result-object v66

    const/16 v67, 0x0

    const/16 v68, 0x1

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v64, v6

    invoke-direct/range {v62 .. v68}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :cond_2a
    const/16 v65, 0x0

    const/16 v67, 0x0

    const/16 v68, 0x1

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v64, v6

    invoke-direct/range {v62 .. v68}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :cond_2b
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v105

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :sswitch_19
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_TRANSITION_INSERT_ALPHA: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v92

    if-eqz v92, :cond_2c

    move-object/from16 v0, v92

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v69

    :goto_c
    const-string v4, "t_mask"

    const/high16 v5, 0x7f050000

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v104

    move-object/from16 v0, v92

    invoke-static {v6, v0}, Lcom/android/videoeditor/service/ApiService;->nextMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v70

    new-instance v67, Landroid/media/videoeditor/TransitionAlpha;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v68

    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v71

    const-string v4, "behavior"

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v73

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    move/from16 v0, v104

    invoke-static {v4, v0}, Lcom/android/videoeditor/util/FileUtils;->getMaskFilename(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v74

    const-string v4, "t_blending"

    const/16 v5, 0x64

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v75

    const-string v4, "t_invert"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v76

    invoke-direct/range {v67 .. v76}, Landroid/media/videoeditor/TransitionAlpha;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JILjava/lang/String;IZ)V

    move-object/from16 v0, v67

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    const/16 v74, 0x0

    const/16 v76, 0x0

    const/16 v77, 0x0

    move-object/from16 v71, p0

    move-object/from16 v72, p1

    move-object/from16 v73, v6

    move-object/from16 v75, v67

    invoke-direct/range {v71 .. v77}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2c
    const/16 v69, 0x0

    goto :goto_c

    :sswitch_1a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_TRANSITION_INSERT_CROSSFADE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v92

    if-eqz v92, :cond_2d

    move-object/from16 v0, v92

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v69

    :goto_d
    move-object/from16 v0, v92

    invoke-static {v6, v0}, Lcom/android/videoeditor/service/ApiService;->nextMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v70

    new-instance v67, Landroid/media/videoeditor/TransitionCrossfade;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v68

    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v71

    const-string v4, "behavior"

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v73

    invoke-direct/range {v67 .. v73}, Landroid/media/videoeditor/TransitionCrossfade;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    move-object/from16 v0, v67

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    const/16 v74, 0x0

    const/16 v76, 0x0

    const/16 v77, 0x0

    move-object/from16 v71, p0

    move-object/from16 v72, p1

    move-object/from16 v73, v6

    move-object/from16 v75, v67

    invoke-direct/range {v71 .. v77}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2d
    const/16 v69, 0x0

    goto :goto_d

    :sswitch_1b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_TRANSITION_INSERT_FADE_TO_BLACK: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v92

    if-eqz v92, :cond_2e

    move-object/from16 v0, v92

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v69

    :goto_e
    move-object/from16 v0, v92

    invoke-static {v6, v0}, Lcom/android/videoeditor/service/ApiService;->nextMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v70

    new-instance v67, Landroid/media/videoeditor/TransitionFadeBlack;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v68

    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v71

    const-string v4, "behavior"

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v73

    invoke-direct/range {v67 .. v73}, Landroid/media/videoeditor/TransitionFadeBlack;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    move-object/from16 v0, v67

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    const/16 v74, 0x0

    const/16 v76, 0x0

    const/16 v77, 0x0

    move-object/from16 v71, p0

    move-object/from16 v72, p1

    move-object/from16 v73, v6

    move-object/from16 v75, v67

    invoke-direct/range {v71 .. v77}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2e
    const/16 v69, 0x0

    goto :goto_e

    :sswitch_1c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_TRANSITION_INSERT_SLIDING: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v92

    if-eqz v92, :cond_2f

    move-object/from16 v0, v92

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v69

    :goto_f
    move-object/from16 v0, v92

    invoke-static {v6, v0}, Lcom/android/videoeditor/service/ApiService;->nextMediaItem(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v70

    new-instance v67, Landroid/media/videoeditor/TransitionSliding;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v68

    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v71

    const-string v4, "behavior"

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v73

    const-string v4, "t_dir"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v74

    invoke-direct/range {v67 .. v74}, Landroid/media/videoeditor/TransitionSliding;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JII)V

    move-object/from16 v0, v67

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->addTransition(Landroid/media/videoeditor/Transition;)V

    const/16 v74, 0x0

    const/16 v76, 0x0

    const/16 v77, 0x0

    move-object/from16 v71, p0

    move-object/from16 v72, p1

    move-object/from16 v73, v6

    move-object/from16 v75, v67

    invoke-direct/range {v71 .. v77}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2f
    const/16 v69, 0x0

    goto :goto_f

    :sswitch_1d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_TRANSITION_REMOVE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->removeTransition(Ljava/lang/String;)Landroid/media/videoeditor/Transition;

    const/16 v74, 0x0

    const/16 v75, 0x0

    const/16 v76, 0x0

    const/16 v77, 0x0

    move-object/from16 v71, p0

    move-object/from16 v72, p1

    move-object/from16 v73, v6

    invoke-direct/range {v71 .. v77}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_1e
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v121

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_TRANSITION_SET_DURATION: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v121

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v121

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getTransition(Ljava/lang/String;)Landroid/media/videoeditor/Transition;

    move-result-object v67

    if-nez v67, :cond_30

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Transition not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v121

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_30
    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    move-object/from16 v0, v67

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/Transition;->setDuration(J)V

    const/16 v74, 0x0

    const/16 v75, 0x0

    const/16 v76, 0x0

    const/16 v77, 0x0

    move-object/from16 v71, p0

    move-object/from16 v72, p1

    move-object/from16 v73, v6

    invoke-direct/range {v71 .. v77}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_1f
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v121

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_TRANSITION_GET_THUMBNAIL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v121

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v121

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getTransition(Ljava/lang/String;)Landroid/media/videoeditor/Transition;

    move-result-object v67

    if-nez v67, :cond_31

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Transition not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v121

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_31
    const-string v4, "height"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v101

    invoke-virtual/range {v67 .. v67}, Landroid/media/videoeditor/Transition;->getAfterMediaItem()Landroid/media/videoeditor/MediaItem;

    move-result-object v69

    const/4 v4, 0x2

    new-array v0, v4, [Landroid/graphics/Bitmap;

    move-object/from16 v75, v0

    if-eqz v69, :cond_32

    const/4 v4, 0x0

    invoke-virtual/range {v69 .. v69}, Landroid/media/videoeditor/MediaItem;->getWidth()I

    move-result v5

    mul-int v5, v5, v101

    invoke-virtual/range {v69 .. v69}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    move-result v7

    div-int/2addr v5, v7

    invoke-virtual/range {v69 .. v69}, Landroid/media/videoeditor/MediaItem;->getTimelineDuration()J

    move-result-wide v11

    move-object/from16 v0, v69

    move/from16 v1, v101

    invoke-virtual {v0, v5, v1, v11, v12}, Landroid/media/videoeditor/MediaItem;->getThumbnail(IIJ)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v75, v4

    :goto_10
    invoke-virtual/range {v67 .. v67}, Landroid/media/videoeditor/Transition;->getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;

    move-result-object v70

    if-eqz v70, :cond_33

    const/4 v4, 0x1

    invoke-virtual/range {v70 .. v70}, Landroid/media/videoeditor/MediaItem;->getWidth()I

    move-result v5

    mul-int v5, v5, v101

    invoke-virtual/range {v70 .. v70}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    move-result v7

    div-int/2addr v5, v7

    const-wide/16 v11, 0x0

    move-object/from16 v0, v70

    move/from16 v1, v101

    invoke-virtual {v0, v5, v1, v11, v12}, Landroid/media/videoeditor/MediaItem;->getThumbnail(IIJ)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v75, v4

    :goto_11
    const/16 v74, 0x0

    const/16 v76, 0x0

    const/16 v77, 0x1

    move-object/from16 v71, p0

    move-object/from16 v72, p1

    move-object/from16 v73, v6

    invoke-direct/range {v71 .. v77}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :cond_32
    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-object v5, v75, v4

    goto :goto_10

    :cond_33
    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v75, v4

    goto :goto_11

    :sswitch_20
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_EFFECT_ADD_COLOR: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_34

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_34
    invoke-virtual {v9}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    move-result-object v99

    invoke-interface/range {v99 .. v99}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v103

    :goto_12
    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_35

    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v76

    check-cast v76, Landroid/media/videoeditor/Effect;

    invoke-virtual/range {v76 .. v76}, Landroid/media/videoeditor/Effect;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->removeEffect(Ljava/lang/String;)Landroid/media/videoeditor/Effect;

    goto :goto_12

    :cond_35
    new-instance v76, Landroid/media/videoeditor/EffectColor;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v78

    const-string v4, "s_time"

    const-wide/16 v11, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v79

    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v81

    const-string v4, "e_type"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v83

    const-string v4, "e_param"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v84

    move-object/from16 v77, v9

    invoke-direct/range {v76 .. v84}, Landroid/media/videoeditor/EffectColor;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJII)V

    move-object/from16 v0, v76

    invoke-virtual {v9, v0}, Landroid/media/videoeditor/MediaItem;->addEffect(Landroid/media/videoeditor/Effect;)V

    const/16 v80, 0x0

    new-instance v81, Lcom/android/videoeditor/service/MovieEffect;

    move-object/from16 v0, v81

    move-object/from16 v1, v76

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/MovieEffect;-><init>(Landroid/media/videoeditor/Effect;)V

    const/16 v82, 0x0

    const/16 v83, 0x0

    move-object/from16 v77, p0

    move-object/from16 v78, p1

    move-object/from16 v79, v6

    invoke-direct/range {v77 .. v83}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_21
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_EFFECT_ADD_IMAGE_KEN_BURNS: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_36

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_36
    invoke-virtual {v9}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    move-result-object v99

    invoke-interface/range {v99 .. v99}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v103

    :goto_13
    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_37

    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v76

    check-cast v76, Landroid/media/videoeditor/Effect;

    invoke-virtual/range {v76 .. v76}, Landroid/media/videoeditor/Effect;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->removeEffect(Ljava/lang/String;)Landroid/media/videoeditor/Effect;

    goto :goto_13

    :cond_37
    new-instance v76, Landroid/media/videoeditor/EffectKenBurns;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v78

    const-string v4, "start_rect"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v79

    check-cast v79, Landroid/graphics/Rect;

    const-string v4, "end_rect"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v80

    check-cast v80, Landroid/graphics/Rect;

    const-string v4, "s_time"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v81

    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v83

    move-object/from16 v77, v9

    invoke-direct/range {v76 .. v84}, Landroid/media/videoeditor/EffectKenBurns;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Landroid/graphics/Rect;Landroid/graphics/Rect;JJ)V

    move-object/from16 v0, v76

    invoke-virtual {v9, v0}, Landroid/media/videoeditor/MediaItem;->addEffect(Landroid/media/videoeditor/Effect;)V

    const/16 v80, 0x0

    new-instance v81, Lcom/android/videoeditor/service/MovieEffect;

    move-object/from16 v0, v81

    move-object/from16 v1, v76

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/MovieEffect;-><init>(Landroid/media/videoeditor/Effect;)V

    const/16 v82, 0x0

    const/16 v83, 0x0

    move-object/from16 v77, p0

    move-object/from16 v78, p1

    move-object/from16 v79, v6

    invoke-direct/range {v77 .. v83}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_22
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_EFFECT_REMOVE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_38

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_38
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->removeEffect(Ljava/lang/String;)Landroid/media/videoeditor/Effect;

    const/16 v80, 0x0

    const/16 v81, 0x0

    const/16 v82, 0x0

    const/16 v83, 0x0

    move-object/from16 v77, p0

    move-object/from16 v78, p1

    move-object/from16 v79, v6

    invoke-direct/range {v77 .. v83}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_23
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_OVERLAY_ADD: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_39

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_39
    invoke-virtual {v9}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    move-result-object v115

    invoke-interface/range {v115 .. v115}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v103

    :goto_14
    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3a

    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v84

    check-cast v84, Landroid/media/videoeditor/Overlay;

    invoke-virtual/range {v84 .. v84}, Landroid/media/videoeditor/Overlay;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->removeOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;

    goto :goto_14

    :cond_3a
    instance-of v4, v9, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v4, :cond_3b

    move-object v0, v9

    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    move-object v4, v0

    invoke-virtual {v4}, Landroid/media/videoeditor/MediaVideoItem;->getWidth()I

    move-result v82

    move-object v0, v9

    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    move-object v4, v0

    invoke-virtual {v4}, Landroid/media/videoeditor/MediaVideoItem;->getHeight()I

    move-result v83

    :goto_15
    const-string v4, "attributes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v122

    invoke-static/range {v122 .. v122}, Lcom/android/videoeditor/service/MovieOverlay;->getType(Landroid/os/Bundle;)I

    move-result v79

    invoke-static/range {v122 .. v122}, Lcom/android/videoeditor/service/MovieOverlay;->getTitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v80

    invoke-static/range {v122 .. v122}, Lcom/android/videoeditor/service/MovieOverlay;->getSubtitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v81

    new-instance v84, Landroid/media/videoeditor/OverlayFrame;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v86

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v77

    const/16 v78, 0x0

    invoke-static/range {v77 .. v83}, Lcom/android/videoeditor/util/ImageUtils;->buildOverlayBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v87

    const-string v4, "s_time"

    const-wide/16 v11, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v88

    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v90

    move-object/from16 v85, v9

    invoke-direct/range {v84 .. v91}, Landroid/media/videoeditor/OverlayFrame;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Landroid/graphics/Bitmap;JJ)V

    invoke-virtual/range {v122 .. v122}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v103

    :goto_16
    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3d

    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v112

    check-cast v112, Ljava/lang/String;

    invoke-static/range {v112 .. v112}, Lcom/android/videoeditor/service/MovieOverlay;->getAttributeType(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-class v5, Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3c

    move-object/from16 v0, v122

    move-object/from16 v1, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v84

    move-object/from16 v1, v112

    invoke-virtual {v0, v1, v4}, Landroid/media/videoeditor/OverlayFrame;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_16

    :cond_3b
    move-object v0, v9

    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    move-object v4, v0

    invoke-virtual {v4}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    move-result v82

    move-object v0, v9

    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    move-object v4, v0

    invoke-virtual {v4}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    move-result v83

    goto/16 :goto_15

    :cond_3c
    move-object/from16 v0, v122

    move-object/from16 v1, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v84

    move-object/from16 v1, v112

    invoke-virtual {v0, v1, v4}, Landroid/media/videoeditor/OverlayFrame;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_16

    :cond_3d
    move-object/from16 v0, v84

    invoke-virtual {v9, v0}, Landroid/media/videoeditor/MediaItem;->addOverlay(Landroid/media/videoeditor/Overlay;)V

    const/16 v88, 0x0

    new-instance v89, Lcom/android/videoeditor/service/MovieOverlay;

    move-object/from16 v0, v89

    move-object/from16 v1, v84

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/MovieOverlay;-><init>(Landroid/media/videoeditor/Overlay;)V

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_24
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_OVERLAY_REMOVE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_3e

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3e
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->removeOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;

    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_25
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_OVERLAY_SET_START_TIME: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_3f

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3f
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->getOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;

    move-result-object v84

    if-nez v84, :cond_40

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Overlay not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_40
    const-string v4, "s_time"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    move-object/from16 v0, v84

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/Overlay;->setStartTime(J)V

    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_26
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_OVERLAY_SET_DURATION: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_41

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_41
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->getOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;

    move-result-object v84

    if-nez v84, :cond_42

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Overlay not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_42
    const-string v4, "duration"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    move-object/from16 v0, v84

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/Overlay;->setDuration(J)V

    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_27
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_OVERLAY_SET_ATTRIBUTES: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v9

    if-nez v9, :cond_43

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MediaItem not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "r_item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_43
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/media/videoeditor/MediaItem;->getOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;

    move-result-object v84

    if-nez v84, :cond_44

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Overlay not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_44
    instance-of v4, v9, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v4, :cond_45

    move-object v0, v9

    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    move-object v4, v0

    invoke-virtual {v4}, Landroid/media/videoeditor/MediaVideoItem;->getWidth()I

    move-result v82

    check-cast v9, Landroid/media/videoeditor/MediaVideoItem;

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaVideoItem;->getHeight()I

    move-result v83

    :goto_17
    const-string v4, "attributes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v122

    invoke-static/range {v122 .. v122}, Lcom/android/videoeditor/service/MovieOverlay;->getType(Landroid/os/Bundle;)I

    move-result v79

    invoke-static/range {v122 .. v122}, Lcom/android/videoeditor/service/MovieOverlay;->getTitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v80

    invoke-static/range {v122 .. v122}, Lcom/android/videoeditor/service/MovieOverlay;->getSubtitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v81

    move-object/from16 v0, v84

    check-cast v0, Landroid/media/videoeditor/OverlayFrame;

    move-object v4, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v77

    const/16 v78, 0x0

    invoke-static/range {v77 .. v83}, Lcom/android/videoeditor/util/ImageUtils;->buildOverlayBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/videoeditor/OverlayFrame;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual/range {v122 .. v122}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v103

    :goto_18
    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_47

    invoke-interface/range {v103 .. v103}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v112

    check-cast v112, Ljava/lang/String;

    invoke-static/range {v112 .. v112}, Lcom/android/videoeditor/service/MovieOverlay;->getAttributeType(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-class v5, Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_46

    move-object/from16 v0, v122

    move-object/from16 v1, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v84

    move-object/from16 v1, v112

    invoke-virtual {v0, v1, v4}, Landroid/media/videoeditor/Overlay;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_18

    :cond_45
    move-object v0, v9

    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    move-object v4, v0

    invoke-virtual {v4}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    move-result v82

    check-cast v9, Landroid/media/videoeditor/MediaImageItem;

    invoke-virtual {v9}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    move-result v83

    goto :goto_17

    :cond_46
    move-object/from16 v0, v122

    move-object/from16 v1, v112

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v84

    move-object/from16 v1, v112

    invoke-virtual {v0, v1, v4}, Landroid/media/videoeditor/Overlay;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_18

    :cond_47
    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_28
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_AUDIO_TRACK_ADD: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v24

    check-cast v24, Landroid/net/Uri;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    const/16 v34, 0x0

    const/16 v95, 0x0

    :try_start_f
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/service/ApiService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/4 v4, 0x0

    const-string v5, "_data"

    aput-object v5, v25, v4

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    invoke-virtual/range {v23 .. v28}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v95

    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_48

    const/4 v4, 0x0

    move-object/from16 v0, v95

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    move-result-object v34

    :cond_48
    if-eqz v95, :cond_49

    :try_start_10
    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->close()V

    :cond_49
    if-nez v34, :cond_4b

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Media file not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :catchall_2
    move-exception v4

    if-eqz v95, :cond_4a

    invoke-interface/range {v95 .. v95}, Landroid/database/Cursor;->close()V

    :cond_4a
    throw v4

    :cond_4b
    new-instance v93, Landroid/media/videoeditor/AudioTrack;

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v93

    move-object/from16 v1, v34

    invoke-direct {v0, v6, v4, v1}, Landroid/media/videoeditor/AudioTrack;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, 0x14

    const/16 v5, 0x41

    move-object/from16 v0, v93

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/AudioTrack;->enableDucking(II)V

    const/16 v4, 0x32

    move-object/from16 v0, v93

    invoke-virtual {v0, v4}, Landroid/media/videoeditor/AudioTrack;->setVolume(I)V

    const-string v4, "loop"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_4c

    invoke-virtual/range {v93 .. v93}, Landroid/media/videoeditor/AudioTrack;->enableLoop()V

    :goto_19
    move-object/from16 v0, v93

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->addAudioTrack(Landroid/media/videoeditor/AudioTrack;)V

    const/16 v88, 0x0

    new-instance v89, Lcom/android/videoeditor/service/MovieAudioTrack;

    move-object/from16 v0, v89

    move-object/from16 v1, v93

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/MovieAudioTrack;-><init>(Landroid/media/videoeditor/AudioTrack;)V

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4c
    invoke-virtual/range {v93 .. v93}, Landroid/media/videoeditor/AudioTrack;->disableLoop()V

    goto :goto_19

    :sswitch_29
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_AUDIO_TRACK_REMOVE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Landroid/media/videoeditor/VideoEditor;->removeAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_2a
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_AUDIO_TRACK_SET_BOUNDARIES: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v94

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v94

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    move-result-object v93

    if-nez v93, :cond_4d

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AudioTrack not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v94

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4d
    const-string v4, "b_boundary"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v7, "e_boundary"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    move-object/from16 v0, v93

    invoke-virtual {v0, v4, v5, v11, v12}, Landroid/media/videoeditor/AudioTrack;->setExtractBoundaries(JJ)V

    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_2b
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_AUDIO_TRACK_SET_LOOP: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v94

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v94

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    move-result-object v93

    if-nez v93, :cond_4e

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AudioTrack not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v94

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4e
    const-string v4, "loop"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_4f

    invoke-virtual/range {v93 .. v93}, Landroid/media/videoeditor/AudioTrack;->enableLoop()V

    :goto_1a
    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4f
    invoke-virtual/range {v93 .. v93}, Landroid/media/videoeditor/AudioTrack;->disableLoop()V

    goto :goto_1a

    :sswitch_2c
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_AUDIO_TRACK_SET_DUCK: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v94

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v94

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    move-result-object v93

    if-nez v93, :cond_50

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AudioTrack not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v94

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_50
    const-string v4, "duck"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_51

    const/16 v4, 0x14

    const/16 v5, 0x41

    move-object/from16 v0, v93

    invoke-virtual {v0, v4, v5}, Landroid/media/videoeditor/AudioTrack;->enableDucking(II)V

    :goto_1b
    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_51
    invoke-virtual/range {v93 .. v93}, Landroid/media/videoeditor/AudioTrack;->disableDucking()V

    goto :goto_1b

    :sswitch_2d
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_AUDIO_TRACK_SET_VOLUME: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v94

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v94

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    move-result-object v93

    if-nez v93, :cond_52

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AudioTrack not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v94

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_52
    const-string v4, "volume"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    move-object/from16 v0, v93

    invoke-virtual {v0, v4}, Landroid/media/videoeditor/AudioTrack;->setVolume(I)V

    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_2e
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_AUDIO_TRACK_SET_MUTE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v94

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v94

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    move-result-object v93

    if-nez v93, :cond_53

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AudioTrack not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v94

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_53
    const-string v4, "mute"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    move-object/from16 v0, v93

    invoke-virtual {v0, v4}, Landroid/media/videoeditor/AudioTrack;->setMute(Z)V

    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/android/videoeditor/service/ApiService;->generatePreview(Landroid/media/videoeditor/VideoEditor;Z)V

    invoke-direct/range {p0 .. p1}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_2f
    const-string v4, "item_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OP_AUDIO_TRACK_EXTRACT_AUDIO_WAVEFORM: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v94

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/videoeditor/service/ApiService;->logd(Ljava/lang/String;)V

    move-object/from16 v0, v94

    invoke-interface {v6, v0}, Landroid/media/videoeditor/VideoEditor;->getAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    move-result-object v93

    if-nez v93, :cond_54

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AudioTrack not found: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v94

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_54
    invoke-virtual/range {v93 .. v93}, Landroid/media/videoeditor/AudioTrack;->getWaveformData()Landroid/media/videoeditor/WaveformData;

    move-result-object v66

    if-nez v66, :cond_55

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v93

    invoke-direct {v0, v1, v6, v2}, Lcom/android/videoeditor/service/ApiService;->extractAudioTrackAudioWaveform(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Landroid/media/videoeditor/AudioTrack;)V

    const/16 v88, 0x0

    invoke-virtual/range {v93 .. v93}, Landroid/media/videoeditor/AudioTrack;->getWaveformData()Landroid/media/videoeditor/WaveformData;

    move-result-object v89

    const/16 v90, 0x0

    const/16 v91, 0x1

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/16 :goto_0

    :cond_55
    const/16 v88, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x1

    move-object/from16 v85, p0

    move-object/from16 v86, p1

    move-object/from16 v87, v6

    move-object/from16 v89, v66

    invoke-direct/range {v85 .. v91}, Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x9 -> :sswitch_0
        0xd -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_9
        0x4 -> :sswitch_6
        0x5 -> :sswitch_7
        0x6 -> :sswitch_8
        0x8 -> :sswitch_a
        0x9 -> :sswitch_b
        0xa -> :sswitch_4
        0xb -> :sswitch_5
        0xd -> :sswitch_1
        0x64 -> :sswitch_c
        0x65 -> :sswitch_d
        0x66 -> :sswitch_10
        0x67 -> :sswitch_11
        0x68 -> :sswitch_12
        0x69 -> :sswitch_13
        0x6a -> :sswitch_14
        0x6b -> :sswitch_16
        0x6c -> :sswitch_17
        0x6d -> :sswitch_18
        0x70 -> :sswitch_15
        0x71 -> :sswitch_e
        0x72 -> :sswitch_f
        0xc8 -> :sswitch_20
        0xc9 -> :sswitch_21
        0xca -> :sswitch_22
        0x12c -> :sswitch_19
        0x12d -> :sswitch_1a
        0x12e -> :sswitch_1b
        0x12f -> :sswitch_1c
        0x130 -> :sswitch_1d
        0x131 -> :sswitch_1e
        0x132 -> :sswitch_1f
        0x190 -> :sswitch_23
        0x191 -> :sswitch_24
        0x192 -> :sswitch_25
        0x193 -> :sswitch_26
        0x194 -> :sswitch_27
        0x1f4 -> :sswitch_28
        0x1f5 -> :sswitch_29
        0x1f6 -> :sswitch_2d
        0x1f7 -> :sswitch_2e
        0x1f9 -> :sswitch_2a
        0x1fa -> :sswitch_2b
        0x1fb -> :sswitch_2c
        0x1fc -> :sswitch_2f
    .end sparse-switch
.end method
