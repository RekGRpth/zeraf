.class public final Lcom/android/videoeditor/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/videoeditor/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final activity_background:I = 0x7f020000

.field public static final add_audio_track:I = 0x7f020001

.field public static final add_clip_big:I = 0x7f020002

.field public static final add_transition_focused:I = 0x7f020003

.field public static final add_transition_normal:I = 0x7f020004

.field public static final add_transition_pressed:I = 0x7f020005

.field public static final add_transition_selector:I = 0x7f020006

.field public static final add_video_project_big:I = 0x7f020007

.field public static final background_selector:I = 0x7f020008

.field public static final border:I = 0x7f020009

.field public static final btn_arrow_left:I = 0x7f02000a

.field public static final btn_arrow_right:I = 0x7f02000b

.field public static final btn_playback_ic_next:I = 0x7f02000c

.field public static final btn_playback_ic_pause:I = 0x7f02000d

.field public static final btn_playback_ic_play:I = 0x7f02000e

.field public static final btn_playback_ic_prev:I = 0x7f02000f

.field public static final btn_playback_ic_rev:I = 0x7f020010

.field public static final buttons_background:I = 0x7f020011

.field public static final buttons_background_zoom:I = 0x7f020012

.field public static final effects_generic:I = 0x7f020013

.field public static final effects_gradient:I = 0x7f020014

.field public static final effects_negative:I = 0x7f020015

.field public static final effects_pan_zoom:I = 0x7f020016

.field public static final effects_sepia:I = 0x7f020017

.field public static final ic_clip_highlight:I = 0x7f020018

.field public static final ic_drag_clip:I = 0x7f020019

.field public static final ic_drag_clip_left:I = 0x7f02001a

.field public static final ic_drag_clip_right:I = 0x7f02001b

.field public static final ic_menu_add_clip_normal:I = 0x7f02001c

.field public static final ic_menu_add_effect_selector:I = 0x7f02001d

.field public static final ic_menu_add_effects:I = 0x7f02001e

.field public static final ic_menu_add_effects_disabled:I = 0x7f02001f

.field public static final ic_menu_add_title:I = 0x7f020020

.field public static final ic_menu_add_title_disabled:I = 0x7f020021

.field public static final ic_menu_add_title_selector:I = 0x7f020022

.field public static final ic_menu_add_trans_end:I = 0x7f020023

.field public static final ic_menu_add_trans_end_disabled:I = 0x7f020024

.field public static final ic_menu_add_trans_end_selector:I = 0x7f020025

.field public static final ic_menu_add_trans_start:I = 0x7f020026

.field public static final ic_menu_add_trans_start_disabled:I = 0x7f020027

.field public static final ic_menu_add_trans_start_selector:I = 0x7f020028

.field public static final ic_menu_add_video:I = 0x7f020029

.field public static final ic_menu_delete:I = 0x7f02002a

.field public static final ic_menu_delete_disabled:I = 0x7f02002b

.field public static final ic_menu_delete_selector:I = 0x7f02002c

.field public static final ic_menu_duck:I = 0x7f02002d

.field public static final ic_menu_edit:I = 0x7f02002e

.field public static final ic_menu_edit_disabled:I = 0x7f02002f

.field public static final ic_menu_edit_effect_selector:I = 0x7f020030

.field public static final ic_menu_edit_effects:I = 0x7f020031

.field public static final ic_menu_edit_effects_disabled:I = 0x7f020032

.field public static final ic_menu_edit_selector:I = 0x7f020033

.field public static final ic_menu_remove_effect_selector:I = 0x7f020034

.field public static final ic_menu_remove_effects:I = 0x7f020035

.field public static final ic_menu_remove_effects_disabled:I = 0x7f020036

.field public static final ic_menu_unmute:I = 0x7f020037

.field public static final ic_playhead:I = 0x7f020038

.field public static final item_progress_left:I = 0x7f020039

.field public static final item_progress_right:I = 0x7f02003a

.field public static final list_pressed_holo_dark:I = 0x7f02003b

.field public static final list_selected_holo_dark:I = 0x7f02003c

.field public static final overlay_background_1:I = 0x7f02003d

.field public static final overlay_background_2:I = 0x7f02003e

.field public static final playhead:I = 0x7f02003f

.field public static final playhead_move_not_ok:I = 0x7f020040

.field public static final playhead_move_ok:I = 0x7f020041

.field public static final project_picker_item_selector:I = 0x7f020042

.field public static final round_button_normal:I = 0x7f020043

.field public static final round_button_pressed:I = 0x7f020044

.field public static final surface_background:I = 0x7f020045

.field public static final theme_preview_film:I = 0x7f020046

.field public static final theme_preview_rock_and_roll:I = 0x7f020047

.field public static final theme_preview_surfing:I = 0x7f020048

.field public static final theme_preview_travel:I = 0x7f020049

.field public static final timeline_background_line:I = 0x7f02004a

.field public static final timeline_background_noline:I = 0x7f02004b

.field public static final timeline_item_normal:I = 0x7f02004c

.field public static final timeline_item_pressed:I = 0x7f02004d

.field public static final timeline_item_selected:I = 0x7f02004e

.field public static final timeline_item_selector:I = 0x7f02004f

.field public static final timeline_loading:I = 0x7f020050

.field public static final timeline_transition_normal:I = 0x7f020051

.field public static final timeline_transparent_normal_item_selector:I = 0x7f020052

.field public static final transition_alpha_contour:I = 0x7f020053

.field public static final transition_alpha_diagonal:I = 0x7f020054

.field public static final transition_crossfade:I = 0x7f020055

.field public static final transition_fade_black:I = 0x7f020056

.field public static final transition_item_selector:I = 0x7f020057

.field public static final transition_sliding_bottom_out_top_in:I = 0x7f020058

.field public static final transition_sliding_left_out_right_in:I = 0x7f020059

.field public static final transition_sliding_right_out_left_in:I = 0x7f02005a

.field public static final transition_sliding_top_out_bottom_in:I = 0x7f02005b

.field public static final zoom_thumb_selector:I = 0x7f02005c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
