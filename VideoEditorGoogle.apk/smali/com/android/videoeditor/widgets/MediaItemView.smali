.class public Lcom/android/videoeditor/widgets/MediaItemView;
.super Landroid/view/View;
.source "MediaItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/videoeditor/widgets/MediaItemView$1;,
        Lcom/android/videoeditor/widgets/MediaItemView$MediaItemShadowBuilder;,
        Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;,
        Lcom/android/videoeditor/widgets/MediaItemView$MyGestureListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaItemView"

.field private static sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

.field private static sEmptyFrameDrawable:Landroid/graphics/drawable/Drawable;

.field private static sGenerationCounter:I

.field private static sThumbnailCache:Lcom/android/videoeditor/widgets/ThumbnailCache;


# instance fields
.field private mBeginTimeMs:J

.field private mEndTimeMs:J

.field private mGeneratingEffectProgress:I

.field private final mGeneratingEffectProgressDestRect:Landroid/graphics/Rect;

.field private mGeneration:I

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

.field private mIsPlaying:Z

.field private mIsScrolling:Z

.field private mLeftState:[I

.field private mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

.field private mNumberOfThumbnails:I

.field private mPending:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mProjectPath:Ljava/lang/String;

.field private mRightState:[I

.field private mScreenWidth:I

.field private final mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

.field private mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

.field private mScrollX:I

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I

.field private mTimeline:Lcom/android/videoeditor/widgets/MediaLinearLayout;

.field private mWantThumbnails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/videoeditor/widgets/MediaItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/videoeditor/widgets/MediaItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v6, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v4, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020006

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sput-object v4, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020050

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sput-object v4, Lcom/android/videoeditor/widgets/MediaItemView;->sEmptyFrameDrawable:Landroid/graphics/drawable/Drawable;

    new-instance v4, Lcom/android/videoeditor/widgets/ThumbnailCache;

    const/high16 v5, 0x300000

    invoke-direct {v4, v5}, Lcom/android/videoeditor/widgets/ThumbnailCache;-><init>(I)V

    sput-object v4, Lcom/android/videoeditor/widgets/MediaItemView;->sThumbnailCache:Lcom/android/videoeditor/widgets/ThumbnailCache;

    :cond_0
    const-string v4, "window"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScreenWidth:I

    new-instance v4, Landroid/view/GestureDetector;

    new-instance v5, Lcom/android/videoeditor/widgets/MediaItemView$MyGestureListener;

    invoke-direct {v5, p0, v6}, Lcom/android/videoeditor/widgets/MediaItemView$MyGestureListener;-><init>(Lcom/android/videoeditor/widgets/MediaItemView;Lcom/android/videoeditor/widgets/MediaItemView$1;)V

    invoke-direct {v4, p1, v5}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v4, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;

    invoke-direct {v4, p0, v6}, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;-><init>(Lcom/android/videoeditor/widgets/MediaItemView;Lcom/android/videoeditor/widgets/MediaItemView$1;)V

    iput-object v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-static {p1}, Lcom/android/videoeditor/widgets/ProgressBar;->getProgressBar(Landroid/content/Context;)Lcom/android/videoeditor/widgets/ProgressBar;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070008

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07000f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v1, v4

    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v3}, Lcom/android/videoeditor/widgets/ProgressBar;->getHeight()I

    move-result v6

    sub-int v6, v1, v6

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingBottom()I

    move-result v8

    sub-int v8, v1, v8

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgressDestRect:Landroid/graphics/Rect;

    const/4 v4, -0x1

    iput v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgress:I

    sget-object v4, Landroid/view/View;->EMPTY_STATE_SET:[I

    iput-object v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mLeftState:[I

    sget-object v4, Landroid/view/View;->EMPTY_STATE_SET:[I

    iput-object v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mRightState:[I

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mWantThumbnails:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mPending:Ljava/util/HashSet;

    sget v4, Lcom/android/videoeditor/widgets/MediaItemView;->sGenerationCounter:I

    add-int/lit8 v5, v4, 0x1

    sput v5, Lcom/android/videoeditor/widgets/MediaItemView;->sGenerationCounter:I

    iput v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneration:I

    return-void
.end method

.method static synthetic access$1000(Lcom/android/videoeditor/widgets/MediaItemView;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getOneThumbnail()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/videoeditor/widgets/MediaItemView;)Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaItemView;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/videoeditor/widgets/MediaItemView;)Z
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->hasSpaceForAddTransitionIcons()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/videoeditor/widgets/MediaItemView;)Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaItemView;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    return-object v0
.end method

.method static synthetic access$500()Landroid/graphics/drawable/Drawable;
    .locals 1

    sget-object v0, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$602(Lcom/android/videoeditor/widgets/MediaItemView;Z)Z
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaItemView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mIsScrolling:Z

    return p1
.end method

.method static synthetic access$702(Lcom/android/videoeditor/widgets/MediaItemView;I)I
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaItemView;
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollX:I

    return p1
.end method

.method static synthetic access$800(Lcom/android/videoeditor/widgets/MediaItemView;)I
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getShadowWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/android/videoeditor/widgets/MediaItemView;)I
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getShadowHeight()I

    move-result v0

    return v0
.end method

.method private static clamp(III)I
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # I

    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private drawAddTransitionIcons(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->hasSpaceForAddTransitionIcons()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mLeftState:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    sget-object v0, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v2

    sget-object v3, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v4

    sget-object v5, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v0, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mRightState:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    sget-object v0, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    sget-object v2, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v4

    sget-object v5, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v0, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method private drawThumbnails(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    const/4 v12, 0x0

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mWantThumbnails:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getLeft()I

    move-result v10

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v11

    add-int/2addr v10, v11

    iget v11, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollX:I

    sub-int v4, v10, v11

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getRight()I

    move-result v10

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollX:I

    sub-int v5, v10, v11

    iget v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScreenWidth:I

    if-ge v4, v10, :cond_0

    if-lez v5, :cond_0

    if-lt v4, v5, :cond_1

    :cond_0
    return-void

    :cond_1
    rsub-int/lit8 v10, v4, 0x0

    iget v11, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    div-int v7, v10, v11

    iget v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScreenWidth:I

    add-int/lit8 v10, v10, -0x1

    sub-int/2addr v10, v4

    iget v11, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    div-int v1, v10, v11

    iget v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mNumberOfThumbnails:I

    add-int/lit8 v10, v10, -0x1

    invoke-static {v7, v12, v10}, Lcom/android/videoeditor/widgets/MediaItemView;->clamp(III)I

    move-result v7

    iget v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mNumberOfThumbnails:I

    add-int/lit8 v10, v10, -0x1

    invoke-static {v1, v12, v10}, Lcom/android/videoeditor/widgets/MediaItemView;->clamp(III)I

    move-result v1

    new-instance v3, Lcom/android/videoeditor/widgets/ThumbnailKey;

    invoke-direct {v3}, Lcom/android/videoeditor/widgets/ThumbnailKey;-><init>()V

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v3, Lcom/android/videoeditor/widgets/ThumbnailKey;->mediaItemId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v10

    iget v11, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    mul-int/2addr v11, v7

    add-int v8, v10, v11

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getHeight()I

    move-result v10

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingBottom()I

    move-result v11

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailHeight:I

    sub-int/2addr v10, v11

    div-int/lit8 v6, v10, 0x2

    add-int/2addr v9, v6

    move v2, v7

    :goto_0
    if-gt v2, v1, :cond_0

    iput v2, v3, Lcom/android/videoeditor/widgets/ThumbnailKey;->index:I

    sget-object v10, Lcom/android/videoeditor/widgets/MediaItemView;->sThumbnailCache:Lcom/android/videoeditor/widgets/ThumbnailCache;

    invoke-virtual {v10, v3}, Lcom/android/videoeditor/widgets/ThumbnailCache;->get(Lcom/android/videoeditor/widgets/ThumbnailKey;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v10, Lcom/android/videoeditor/widgets/MediaItemView;->sEmptyFrameDrawable:Landroid/graphics/drawable/Drawable;

    iget v11, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    add-int/2addr v11, v8

    iget v12, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailHeight:I

    add-int/2addr v12, v9

    invoke-virtual {v10, v8, v9, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v10, Lcom/android/videoeditor/widgets/MediaItemView;->sEmptyFrameDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mPending:Ljava/util/HashSet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mWantThumbnails:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    iget v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    add-int/2addr v8, v10

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    int-to-float v10, v8

    int-to-float v11, v9

    const/4 v12, 0x0

    invoke-virtual {p1, v0, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private getOneThumbnail()Landroid/graphics/Bitmap;
    .locals 4

    new-instance v2, Lcom/android/videoeditor/widgets/ThumbnailKey;

    invoke-direct {v2}, Lcom/android/videoeditor/widgets/ThumbnailKey;-><init>()V

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/android/videoeditor/widgets/ThumbnailKey;->mediaItemId:Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    iget v3, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mNumberOfThumbnails:I

    if-ge v1, v3, :cond_1

    iput v1, v2, Lcom/android/videoeditor/widgets/ThumbnailKey;->index:I

    sget-object v3, Lcom/android/videoeditor/widgets/MediaItemView;->sThumbnailCache:Lcom/android/videoeditor/widgets/ThumbnailCache;

    invoke-virtual {v3, v2}, Lcom/android/videoeditor/widgets/ThumbnailCache;->get(Lcom/android/videoeditor/widgets/ThumbnailKey;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getShadowHeight()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getHeight()I

    move-result v0

    return v0
.end method

.method private getShadowWidth()I
    .locals 4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingBottom()I

    move-result v3

    sub-int v0, v2, v3

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getWidth()I

    move-result v2

    mul-int/2addr v2, v0

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getHeight()I

    move-result v3

    div-int v1, v2, v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    return v2
.end method

.method private hasSpaceForAddTransitionIcons()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mTimeline:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->isTrimming()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    sget-object v2, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private releaseBitmapsAndClear()V
    .locals 2

    sget-object v0, Lcom/android/videoeditor/widgets/MediaItemView;->sThumbnailCache:Lcom/android/videoeditor/widgets/ThumbnailCache;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/ThumbnailCache;->clearForMediaItemId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mPending:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    sget v0, Lcom/android/videoeditor/widgets/MediaItemView;->sGenerationCounter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/videoeditor/widgets/MediaItemView;->sGenerationCounter:I

    iput v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneration:I

    return-void
.end method

.method private requestThumbnails()V
    .locals 13

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mWantThumbnails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v11, v0, [I

    const/4 v12, 0x0

    :goto_0
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mWantThumbnails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v12, v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mWantThumbnails:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v11, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mPending:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mWantThumbnails:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mProjectPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    iget v4, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailHeight:I

    iget-wide v5, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mBeginTimeMs:J

    iget-wide v7, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mEndTimeMs:J

    iget v9, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mNumberOfThumbnails:I

    iget v10, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneration:I

    invoke-static/range {v0 .. v11}, Lcom/android/videoeditor/service/ApiService;->getMediaItemThumbnails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJJII[I)V

    return-void
.end method


# virtual methods
.method public getShadowBuilder()Landroid/view/View$DragShadowBuilder;
    .locals 1

    new-instance v0, Lcom/android/videoeditor/widgets/MediaItemView$MediaItemShadowBuilder;

    invoke-direct {v0, p0, p0}, Lcom/android/videoeditor/widgets/MediaItemView$MediaItemShadowBuilder;-><init>(Lcom/android/videoeditor/widgets/MediaItemView;Landroid/view/View;)V

    return-object v0
.end method

.method public isGeneratingEffect()Z
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgress:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/MovieMediaItem;

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->addScrollListener(Lcom/android/videoeditor/widgets/ScrollViewListener;)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->getScrollX()I

    move-result v0

    iput v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollX:I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080033

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mTimeline:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->removeScrollListener(Lcom/android/videoeditor/widgets/ScrollViewListener;)V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->releaseBitmapsAndClear()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgress:I

    if-ltz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/videoeditor/widgets/ProgressBar;->getProgressBar(Landroid/content/Context;)Lcom/android/videoeditor/widgets/ProgressBar;

    move-result-object v0

    iget v2, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgress:I

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgressDestRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v5

    sub-int v5, v1, v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/videoeditor/widgets/ProgressBar;->draw(Landroid/graphics/Canvas;ILandroid/graphics/Rect;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaItemView;->drawThumbnails(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaItemView;->drawAddTransitionIcons(Landroid/graphics/Canvas;)V

    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mIsPlaying:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mTimeline:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->isTrimming()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mIsScrolling:Z

    if-eqz v0, :cond_5

    :cond_3
    const/4 v6, 0x1

    :goto_2
    if-nez v6, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mWantThumbnails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->requestThumbnails()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mTimeline:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->hasItemSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    const/high16 v0, -0x1000000

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v0, 0xc0

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public onLayoutPerformed(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/16 v3, 0x800

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailHeight:I

    iget v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailHeight:I

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getWidth()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getHeight()I

    move-result v2

    div-int/2addr v1, v2

    iput v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    :goto_0
    iget v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    if-gt v1, v3, :cond_0

    iget v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailHeight:I

    if-le v1, v3, :cond_1

    :cond_0
    iget v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailHeight:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailHeight:I

    iget v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v2

    sub-int v0, v1, v2

    iget v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mThumbnailWidth:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mNumberOfThumbnails:I

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppBoundaryBeginTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mBeginTimeMs:J

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppBoundaryEndTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mEndTimeMs:J

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->releaseBitmapsAndClear()V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->invalidate()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    sget-object v0, Landroid/view/View;->EMPTY_STATE_SET:[I

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mLeftState:[I

    sget-object v0, Landroid/view/View;->EMPTY_STATE_SET:[I

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mRightState:[I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->hasSpaceForAddTransitionIcons()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    sget-object v1, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/view/View;->PRESSED_WINDOW_FOCUSED_STATE_SET:[I

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mLeftState:[I

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->invalidate()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    sget-object v2, Lcom/android/videoeditor/widgets/MediaItemView;->sAddTransitionDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/view/View;->PRESSED_WINDOW_FOCUSED_STATE_SET:[I

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mRightState:[I

    goto :goto_1

    :pswitch_2
    sget-object v0, Landroid/view/View;->EMPTY_STATE_SET:[I

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mRightState:[I

    sget-object v0, Landroid/view/View;->EMPTY_STATE_SET:[I

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mLeftState:[I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->invalidate()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public resetGeneratingEffectProgress()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/MediaItemView;->setGeneratingEffectProgress(I)V

    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;II)Z
    .locals 5
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneration:I

    if-eq p3, v2, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mPending:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "MediaItemView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "received unasked bitmap, index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    const-string v2, "MediaItemView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive null bitmap for index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mPending:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/videoeditor/widgets/ThumbnailKey;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/android/videoeditor/widgets/ThumbnailKey;-><init>(Ljava/lang/String;I)V

    sget-object v1, Lcom/android/videoeditor/widgets/MediaItemView;->sThumbnailCache:Lcom/android/videoeditor/widgets/ThumbnailCache;

    invoke-virtual {v1, v0, p1}, Lcom/android/videoeditor/widgets/ThumbnailCache;->put(Lcom/android/videoeditor/widgets/ThumbnailKey;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->invalidate()V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setGeneratingEffectProgress(I)V
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    iput p1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgress:I

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->releaseBitmapsAndClear()V

    :goto_0
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->invalidate()V

    return-void

    :cond_0
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgress:I

    goto :goto_0

    :cond_1
    iput p1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGeneratingEffectProgress:I

    goto :goto_0
.end method

.method public setGestureListener(Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    return-void
.end method

.method public setPlaybackMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mIsPlaying:Z

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaItemView;->invalidate()V

    return-void
.end method

.method public setProjectPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaItemView;->mProjectPath:Ljava/lang/String;

    return-void
.end method
