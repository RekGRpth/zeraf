.class Lcom/android/videoeditor/widgets/ProgressBar;
.super Ljava/lang/Object;
.source "ProgressBar.java"


# static fields
.field private static mProgressBar:Lcom/android/videoeditor/widgets/ProgressBar;


# instance fields
.field private final mProgressLeftBitmap:Landroid/graphics/Bitmap;

.field private final mProgressRightBitmap:Landroid/graphics/Bitmap;

.field private final mProgressSrcRect:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020039

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressLeftBitmap:Landroid/graphics/Bitmap;

    const v1, 0x7f02003a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressRightBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressLeftBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressLeftBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressSrcRect:Landroid/graphics/Rect;

    return-void
.end method

.method public static getProgressBar(Landroid/content/Context;)Lcom/android/videoeditor/widgets/ProgressBar;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressBar:Lcom/android/videoeditor/widgets/ProgressBar;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/videoeditor/widgets/ProgressBar;

    invoke-direct {v0, p0}, Lcom/android/videoeditor/widgets/ProgressBar;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressBar:Lcom/android/videoeditor/widgets/ProgressBar;

    :cond_0
    sget-object v0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressBar:Lcom/android/videoeditor/widgets/ProgressBar;

    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;ILandroid/graphics/Rect;II)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # Landroid/graphics/Rect;
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    sparse-switch p2, :sswitch_data_0

    sub-int v0, p5, p4

    mul-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x64

    iput v0, p3, Landroid/graphics/Rect;->right:I

    if-lez p2, :cond_0

    iput p4, p3, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressLeftBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressSrcRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v1, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    const/16 v0, 0x64

    if-ge p2, v0, :cond_1

    iget v0, p3, Landroid/graphics/Rect;->right:I

    iput v0, p3, Landroid/graphics/Rect;->left:I

    iput p5, p3, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressRightBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressSrcRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v1, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    :goto_0
    return-void

    :sswitch_0
    iput p4, p3, Landroid/graphics/Rect;->left:I

    iput p5, p3, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressRightBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressSrcRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v1, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    :sswitch_1
    iput p4, p3, Landroid/graphics/Rect;->left:I

    iput p5, p3, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressLeftBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressSrcRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v1, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public getHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ProgressBar;->mProgressLeftBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    return v0
.end method
