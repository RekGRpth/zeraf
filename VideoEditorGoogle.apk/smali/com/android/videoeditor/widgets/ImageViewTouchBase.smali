.class public Lcom/android/videoeditor/widgets/ImageViewTouchBase;
.super Landroid/widget/ImageView;
.source "ImageViewTouchBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/videoeditor/widgets/ImageViewTouchBase$ImageTouchEventListener;
    }
.end annotation


# static fields
.field private static final MIN_ZOOM_SCALE:F = 1.0f

.field private static final SCALE_RATE:F = 1.25f


# instance fields
.field private mBaseMatrix:Landroid/graphics/Matrix;

.field private mBitmapDisplayed:Landroid/graphics/Bitmap;

.field private final mDisplayMatrix:Landroid/graphics/Matrix;

.field private mEventListener:Lcom/android/videoeditor/widgets/ImageViewTouchBase$ImageTouchEventListener;

.field private final mMatrixValues:[F

.field private mMaxZoom:F

.field private mOnLayoutRunnable:Ljava/lang/Runnable;

.field private mStretch:Z

.field private mSuppMatrix:Landroid/graphics/Matrix;

.field private mThisHeight:I

.field private mThisWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, -0x1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMatrixValues:[F

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisWidth:I

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisHeight:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mStretch:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMatrixValues:[F

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisWidth:I

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisHeight:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mStretch:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMatrixValues:[F

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisWidth:I

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisHeight:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mStretch:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method private center(ZZ)V
    .locals 12
    .param p1    # Z
    .param p2    # Z

    const/high16 v11, 0x40000000

    const/4 v10, 0x0

    iget-object v8, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    new-instance v4, Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {v4, v10, v10, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getHeight()I

    move-result v5

    int-to-float v8, v5

    cmpg-float v8, v2, v8

    if-gez v8, :cond_3

    int-to-float v8, v5

    sub-float/2addr v8, v2

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v8, v9

    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getWidth()I

    move-result v6

    int-to-float v8, v6

    cmpg-float v8, v7, v8

    if-gez v8, :cond_5

    int-to-float v8, v6

    sub-float/2addr v8, v7

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v8, v9

    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    iget v8, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_4

    iget v8, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v8

    goto :goto_1

    :cond_4
    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v8, v9

    goto :goto_1

    :cond_5
    iget v8, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_6

    iget v8, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v8

    goto :goto_2

    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v6

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    int-to-float v8, v6

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v8, v9

    goto :goto_2
.end method

.method private correctedZoomScale(F)F
    .locals 2
    .param p1    # F

    move v0, p1

    iget v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMaxZoom:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    iget v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMaxZoom:F

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/high16 v1, 0x3f800000

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    const/high16 v0, 0x3f800000

    goto :goto_0
.end method

.method private getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 2

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private getProperBaseMatrix(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V
    .locals 10
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Landroid/graphics/Matrix;

    const/high16 v8, 0x41200000

    const/high16 v9, 0x40000000

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getWidth()I

    move-result v7

    int-to-float v4, v7

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getHeight()I

    move-result v7

    int-to-float v3, v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v5, v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v0, v7

    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    iget-boolean v7, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mStretch:Z

    if-eqz v7, :cond_0

    div-float v7, v4, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v6

    div-float v7, v3, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v6, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {p2, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    mul-float v7, v5, v2

    sub-float v7, v4, v7

    div-float/2addr v7, v9

    mul-float v8, v0, v2

    sub-float v8, v3, v8

    div-float/2addr v8, v9

    invoke-virtual {p2, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :goto_0
    return-void

    :cond_0
    sub-float v7, v4, v5

    div-float/2addr v7, v9

    sub-float v8, v3, v0

    div-float/2addr v8, v9

    invoke-virtual {p2, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0
.end method

.method private getValue(Landroid/graphics/Matrix;I)F
    .locals 1
    .param p1    # Landroid/graphics/Matrix;
    .param p2    # I

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMatrixValues:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMatrixValues:[F

    aget v0, v0, p2

    return v0
.end method

.method private maxZoom()F
    .locals 4

    iget-object v2, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    const/high16 v2, 0x3f800000

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisWidth:I

    int-to-float v3, v3

    div-float v1, v2, v3

    iget-object v2, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisHeight:I

    int-to-float v3, v3

    div-float v0, v2, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const/high16 v3, 0x40800000

    mul-float/2addr v2, v3

    goto :goto_0
.end method

.method public static maxZoom(IIII)F
    .locals 4
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    int-to-float v2, p0

    int-to-float v3, p2

    div-float v1, v2, v3

    int-to-float v2, p1

    int-to-float v3, p3

    div-float v0, v2, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const/high16 v3, 0x40800000

    mul-float/2addr v2, v3

    return v2
.end method

.method private panBy(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mEventListener:Lcom/android/videoeditor/widgets/ImageViewTouchBase$ImageTouchEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mEventListener:Lcom/android/videoeditor/widgets/ImageViewTouchBase$ImageTouchEventListener;

    invoke-interface {v0, p1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase$ImageTouchEventListener;->onImageTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImageBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getMaxZoom()F
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMaxZoom:F

    return v0
.end method

.method public getScale()F
    .locals 2

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v0

    return v0
.end method

.method public mapRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1    # Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    sub-int v1, p4, p2

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisWidth:I

    sub-int v1, p5, p3

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mThisHeight:I

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v1, v2}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getProperBaseMatrix(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public postTranslateCenter(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-direct {p0, v1, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->center(ZZ)V

    return-void
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    :cond_0
    return-void
.end method

.method public setEventListener(Lcom/android/videoeditor/widgets/ImageViewTouchBase$ImageTouchEventListener;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/widgets/ImageViewTouchBase$ImageTouchEventListener;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mEventListener:Lcom/android/videoeditor/widgets/ImageViewTouchBase$ImageTouchEventListener;

    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    :cond_0
    iput-object p1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mStretch:Z

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getWidth()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v1, Lcom/android/videoeditor/widgets/ImageViewTouchBase$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/videoeditor/widgets/ImageViewTouchBase$1;-><init>(Lcom/android/videoeditor/widgets/ImageViewTouchBase;Landroid/graphics/Bitmap;Z)V

    iput-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getProperBaseMatrix(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    :cond_1
    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->maxZoom()F

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMaxZoom:F

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public zoomIn()V
    .locals 1

    const/high16 v0, 0x3fa00000

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->zoomIn(F)V

    return-void
.end method

.method public zoomIn(F)V
    .locals 5
    .param p1    # F

    const/high16 v4, 0x40000000

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getScale()F

    move-result v2

    iget v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mMaxZoom:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v4

    iget-object v2, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_0
    return-void
.end method

.method public zoomOut()V
    .locals 1

    const/high16 v0, 0x3fa00000

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->zoomOut(F)V

    return-void
.end method

.method public zoomOut(F)V
    .locals 7
    .param p1    # F

    const/4 v6, 0x1

    const/high16 v4, 0x40000000

    const/high16 v5, 0x3f800000

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getScale()F

    move-result v3

    cmpl-float v3, v3, v5

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mBitmapDisplayed:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v4

    new-instance v2, Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-direct {v2, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    div-float v3, v5, p1

    div-float v4, v5, p1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1

    iget-object v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v5, v5, v0, v1}, Landroid/graphics/Matrix;->setScale(FFFF)V

    :goto_0
    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    invoke-direct {p0, v6, v6}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->center(ZZ)V

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v4, v5, p1

    div-float/2addr v5, p1

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_0
.end method

.method public zoomTo(F)V
    .locals 4
    .param p1    # F

    const/high16 v3, 0x40000000

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->zoomTo(FFF)V

    return-void
.end method

.method public zoomTo(FFF)V
    .locals 5
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->correctedZoomScale(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getScale()F

    move-result v2

    div-float v1, v0, v2

    iget-object v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    invoke-direct {p0, v4, v4}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->center(ZZ)V

    return-void
.end method

.method public zoomToOffset(FFF)V
    .locals 5
    .param p1    # F
    .param p2    # F
    .param p3    # F

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->correctedZoomScale(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getScale()F

    move-result v2

    div-float v1, v0, v2

    iget-object v3, p0, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    neg-float v3, p2

    neg-float v4, p3

    invoke-direct {p0, v3, v4}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->panBy(FF)V

    return-void
.end method

.method public zoomToPoint(FFF)V
    .locals 4
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/high16 v3, 0x40000000

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    sub-float v2, v0, p2

    sub-float v3, v1, p3

    invoke-direct {p0, v2, v3}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->panBy(FF)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/videoeditor/widgets/ImageViewTouchBase;->zoomTo(FFF)V

    return-void
.end method
