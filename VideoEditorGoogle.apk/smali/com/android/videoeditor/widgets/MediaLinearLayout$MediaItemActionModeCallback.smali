.class Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;
.super Ljava/lang/Object;
.source "MediaLinearLayout.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/videoeditor/widgets/MediaLinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaItemActionModeCallback"
.end annotation


# instance fields
.field private final mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

.field final synthetic this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;


# direct methods
.method public constructor <init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)V
    .locals 0
    .param p2    # Lcom/android/videoeditor/service/MovieMediaItem;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    return-void
.end method

.method private applyEffect(Landroid/view/MenuItem;)V
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2, v4, v4}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addEffect(ILjava/lang/String;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-interface {p1, v5}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v4}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addEffect(ILjava/lang/String;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-interface {p1, v5}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v4}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addEffect(ILjava/lang/String;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-interface {p1, v5}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/android/videoeditor/KenBurnsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "media_item_id"

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "filename"

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "width"

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "height"

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f08004e
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v2, 0x1

    return v2

    :pswitch_1
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->editOverlay(Lcom/android/videoeditor/service/MovieMediaItem;)V
    invoke-static {v2, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$300(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeOverlay(Lcom/android/videoeditor/service/MovieMediaItem;)V
    invoke-static {v2, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$400(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;
    invoke-static {v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$100(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v2

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/videoeditor/service/VideoEditorProject;->getPreviousMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v1

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->pickTransition(Lcom/android/videoeditor/service/MovieMediaItem;)Z
    invoke-static {v2, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$500(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)Z

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->pickTransition(Lcom/android/videoeditor/service/MovieMediaItem;)Z
    invoke-static {v2, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$500(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)Z

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->applyEffect(Landroid/view/MenuItem;)V

    goto :goto_0

    :pswitch_6
    invoke-interface {p2}, Landroid/view/MenuItem;->isChecked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "media_item_id"

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0xe

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    :pswitch_7
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "media_item_id"

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "rendering_mode"

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppRenderingMode()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0xc

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    :pswitch_8
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "media_item_id"

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0xa

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080049
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # setter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;
    invoke-static {v1, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$002(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0b0002

    invoke-virtual {v1, v2, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v1, 0x1

    return v1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3
    .param p1    # Landroid/view/ActionMode;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->getMediaItemView(Ljava/lang/String;)Landroid/view/View;
    invoke-static {v1, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$600(Lcom/android/videoeditor/widgets/MediaLinearLayout;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;
    invoke-static {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$700(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;
    invoke-static {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$800(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/widgets/HandleView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/videoeditor/widgets/HandleView;->endMove()V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;
    invoke-static {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$900(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/widgets/HandleView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/videoeditor/widgets/HandleView;->endMove()V

    :cond_0
    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->unSelect(Landroid/view/View;)V
    invoke-static {v1, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$1000(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    const/4 v2, 0x1

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->showAddMediaItemButtons(Z)V
    invoke-static {v1, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$1100(Lcom/android/videoeditor/widgets/MediaLinearLayout;Z)V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    const/4 v2, 0x0

    # setter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;
    invoke-static {v1, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$002(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 13
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const v12, 0x7f080052

    const v11, 0x7f08004e

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;
    invoke-static {v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$100(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/videoeditor/service/ApiService;->isProjectBeingEdited(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPlaybackInProgress:Z
    invoke-static {v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$200(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Z

    move-result v10

    if-nez v10, :cond_2

    move v3, v8

    :goto_0
    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->isImage()Z

    move-result v10

    if-nez v10, :cond_0

    invoke-interface {p2, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getEffect()Lcom/android/videoeditor/service/MovieEffect;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getEffect()Lcom/android/videoeditor/service/MovieEffect;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieEffect;->getType()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    invoke-interface {p2, v12}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    :goto_1
    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    const v10, 0x7f08004b

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    invoke-interface {v10, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v10, 0x7f080049

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v10

    if-nez v10, :cond_4

    const v10, 0x7f090014

    :goto_2
    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v10, 0x7f08004a

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v10

    if-eqz v10, :cond_5

    move v10, v8

    :goto_3
    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v3, :cond_6

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v10

    if-eqz v10, :cond_6

    move v10, v8

    :goto_4
    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v10, 0x7f08004c

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v10

    if-nez v10, :cond_7

    move v10, v8

    :goto_5
    invoke-interface {v1, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v3, :cond_8

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v10

    if-nez v10, :cond_8

    move v10, v8

    :goto_6
    invoke-interface {v1, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v10, 0x7f08004d

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v10

    if-nez v10, :cond_9

    move v10, v8

    :goto_7
    invoke-interface {v4, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v3, :cond_a

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->mMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v10

    if-nez v10, :cond_a

    move v10, v8

    :goto_8
    invoke-interface {v4, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v10, 0x7f080053

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;
    invoke-static {v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$100(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/videoeditor/service/VideoEditorProject;->hasMultipleAspectRatios()Z

    move-result v10

    invoke-interface {v6, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz v3, :cond_1

    iget-object v10, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;
    invoke-static {v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$100(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/videoeditor/service/VideoEditorProject;->hasMultipleAspectRatios()Z

    move-result v10

    if-eqz v10, :cond_1

    move v9, v8

    :cond_1
    invoke-interface {v6, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    return v8

    :cond_2
    move v3, v9

    goto/16 :goto_0

    :pswitch_0
    invoke-interface {p2, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    goto/16 :goto_1

    :pswitch_1
    const v10, 0x7f08004f

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    goto/16 :goto_1

    :pswitch_2
    const v10, 0x7f080050

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    goto/16 :goto_1

    :pswitch_3
    const v10, 0x7f080051

    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    goto/16 :goto_1

    :cond_3
    invoke-interface {p2, v12}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    goto/16 :goto_1

    :cond_4
    const v10, 0x7f090015

    goto/16 :goto_2

    :cond_5
    move v10, v9

    goto/16 :goto_3

    :cond_6
    move v10, v9

    goto/16 :goto_4

    :cond_7
    move v10, v9

    goto/16 :goto_5

    :cond_8
    move v10, v9

    goto/16 :goto_6

    :cond_9
    move v10, v9

    goto :goto_7

    :cond_a
    move v10, v9

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
