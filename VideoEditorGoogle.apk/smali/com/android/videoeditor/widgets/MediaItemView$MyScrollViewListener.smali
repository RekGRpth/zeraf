.class Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;
.super Ljava/lang/Object;
.source "MediaItemView.java"

# interfaces
.implements Lcom/android/videoeditor/widgets/ScrollViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/videoeditor/widgets/MediaItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyScrollViewListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/videoeditor/widgets/MediaItemView;


# direct methods
.method private constructor <init>(Lcom/android/videoeditor/widgets/MediaItemView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;->this$0:Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/videoeditor/widgets/MediaItemView;Lcom/android/videoeditor/widgets/MediaItemView$1;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/widgets/MediaItemView;
    .param p2    # Lcom/android/videoeditor/widgets/MediaItemView$1;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;-><init>(Lcom/android/videoeditor/widgets/MediaItemView;)V

    return-void
.end method


# virtual methods
.method public onScrollBegin(Landroid/view/View;IIZ)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;->this$0:Lcom/android/videoeditor/widgets/MediaItemView;

    const/4 v1, 0x1

    # setter for: Lcom/android/videoeditor/widgets/MediaItemView;->mIsScrolling:Z
    invoke-static {v0, v1}, Lcom/android/videoeditor/widgets/MediaItemView;->access$602(Lcom/android/videoeditor/widgets/MediaItemView;Z)Z

    return-void
.end method

.method public onScrollEnd(Landroid/view/View;IIZ)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;->this$0:Lcom/android/videoeditor/widgets/MediaItemView;

    const/4 v1, 0x0

    # setter for: Lcom/android/videoeditor/widgets/MediaItemView;->mIsScrolling:Z
    invoke-static {v0, v1}, Lcom/android/videoeditor/widgets/MediaItemView;->access$602(Lcom/android/videoeditor/widgets/MediaItemView;Z)Z

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;->this$0:Lcom/android/videoeditor/widgets/MediaItemView;

    # setter for: Lcom/android/videoeditor/widgets/MediaItemView;->mScrollX:I
    invoke-static {v0, p2}, Lcom/android/videoeditor/widgets/MediaItemView;->access$702(Lcom/android/videoeditor/widgets/MediaItemView;I)I

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;->this$0:Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/MediaItemView;->invalidate()V

    return-void
.end method

.method public onScrollProgress(Landroid/view/View;IIZ)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;->this$0:Lcom/android/videoeditor/widgets/MediaItemView;

    # setter for: Lcom/android/videoeditor/widgets/MediaItemView;->mScrollX:I
    invoke-static {v0, p2}, Lcom/android/videoeditor/widgets/MediaItemView;->access$702(Lcom/android/videoeditor/widgets/MediaItemView;I)I

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaItemView$MyScrollViewListener;->this$0:Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/MediaItemView;->invalidate()V

    return-void
.end method
