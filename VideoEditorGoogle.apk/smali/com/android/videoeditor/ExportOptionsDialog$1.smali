.class final Lcom/android/videoeditor/ExportOptionsDialog$1;
.super Ljava/lang/Object;
.source "ExportOptionsDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/videoeditor/ExportOptionsDialog;->create(Landroid/content/Context;Lcom/android/videoeditor/ExportOptionsDialog$ExportOptionsListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$aspectRatio:I

.field final synthetic val$myView:Landroid/view/View;

.field final synthetic val$positiveListener:Lcom/android/videoeditor/ExportOptionsDialog$ExportOptionsListener;


# direct methods
.method constructor <init>(Landroid/view/View;ILcom/android/videoeditor/ExportOptionsDialog$ExportOptionsListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/videoeditor/ExportOptionsDialog$1;->val$myView:Landroid/view/View;

    iput p2, p0, Lcom/android/videoeditor/ExportOptionsDialog$1;->val$aspectRatio:I

    iput-object p3, p0, Lcom/android/videoeditor/ExportOptionsDialog$1;->val$positiveListener:Lcom/android/videoeditor/ExportOptionsDialog$ExportOptionsListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v4, p0, Lcom/android/videoeditor/ExportOptionsDialog$1;->val$myView:Landroid/view/View;

    const v5, 0x7f08000d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v4

    iget v5, p0, Lcom/android/videoeditor/ExportOptionsDialog$1;->val$aspectRatio:I

    # invokes: Lcom/android/videoeditor/ExportOptionsDialog;->indexToMovieHeight(II)I
    invoke-static {v4, v5}, Lcom/android/videoeditor/ExportOptionsDialog;->access$000(II)I

    move-result v1

    iget-object v4, p0, Lcom/android/videoeditor/ExportOptionsDialog$1;->val$myView:Landroid/view/View;

    const v5, 0x7f08000e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v4

    # invokes: Lcom/android/videoeditor/ExportOptionsDialog;->indexToMovieBitrate(I)I
    invoke-static {v4}, Lcom/android/videoeditor/ExportOptionsDialog;->access$100(I)I

    move-result v0

    iget-object v4, p0, Lcom/android/videoeditor/ExportOptionsDialog$1;->val$positiveListener:Lcom/android/videoeditor/ExportOptionsDialog$ExportOptionsListener;

    invoke-interface {v4, v1, v0}, Lcom/android/videoeditor/ExportOptionsDialog$ExportOptionsListener;->onExportOptions(II)V

    return-void
.end method
