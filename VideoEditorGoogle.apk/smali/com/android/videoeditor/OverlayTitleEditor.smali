.class public Lcom/android/videoeditor/OverlayTitleEditor;
.super Lcom/android/videoeditor/NoSearchActivity;
.source "OverlayTitleEditor.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "OverlayTitleEditor"

.field public static final PARAM_MEDIA_ITEM_ID:Ljava/lang/String; = "media_item_id"

.field public static final PARAM_OVERLAY_ATTRIBUTES:Ljava/lang/String; = "attributes"

.field public static final PARAM_OVERLAY_ID:Ljava/lang/String; = "overlay_id"

.field private static final REQUEST_CODE_PICK_TITLE_TEMPLATE:I = 0x1


# instance fields
.field private mOverlayBitmap:Landroid/graphics/Bitmap;

.field private mOverlayChangeTitleTemplateButton:Landroid/widget/Button;

.field private mOverlayImageView:Landroid/widget/ImageView;

.field private mOverlayType:I

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private mSubtitleView:Landroid/widget/TextView;

.field private final mTextWatcher:Landroid/text/TextWatcher;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/videoeditor/NoSearchActivity;-><init>()V

    new-instance v0, Lcom/android/videoeditor/OverlayTitleEditor$1;

    invoke-direct {v0, p0}, Lcom/android/videoeditor/OverlayTitleEditor$1;-><init>(Lcom/android/videoeditor/OverlayTitleEditor;)V

    iput-object v0, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/android/videoeditor/OverlayTitleEditor;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/OverlayTitleEditor;

    invoke-direct {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->updatePreviewImage()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/videoeditor/OverlayTitleEditor;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/OverlayTitleEditor;

    invoke-direct {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->launchOverlayTitleTemplatePicker()V

    return-void
.end method

.method private launchOverlayTitleTemplatePicker()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/videoeditor/OverlayTitleTemplatePicker;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/videoeditor/OverlayTitleEditor;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updatePreviewImage()V
    .locals 7

    iget-object v1, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayBitmap:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayType:I

    iget-object v0, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mSubtitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mPreviewWidth:I

    iget v6, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mPreviewHeight:I

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/android/videoeditor/util/ImageUtils;->buildOverlayBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    const-string v1, "OverlayTitleEditor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid request code received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    const-string v1, "attributes"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/android/videoeditor/service/MovieOverlay;->getType(Landroid/os/Bundle;)I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayType:I

    invoke-direct {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->updatePreviewImage()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClickHandler(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v5, "media_item_id"

    invoke-virtual {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "media_item_id"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "overlay_id"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v5, "overlay_id"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const v5, 0x7f08001f

    invoke-virtual {p0, v5}, Lcom/android/videoeditor/OverlayTitleEditor;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f080020

    invoke-virtual {p0, v5}, Lcom/android/videoeditor/OverlayTitleEditor;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget v5, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayType:I

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/android/videoeditor/service/MovieOverlay;->buildUserAttributes(ILjava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "attributes"

    invoke-virtual {v1, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v5, -0x1

    invoke-virtual {p0, v5, v1}, Lcom/android/videoeditor/OverlayTitleEditor;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->finish()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f080021
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    invoke-super {p0, p1}, Lcom/android/videoeditor/NoSearchActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f040010

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/OverlayTitleEditor;->setContentView(I)V

    invoke-virtual {p0, v4}, Lcom/android/videoeditor/OverlayTitleEditor;->setFinishOnTouchOutside(Z)V

    const v2, 0x7f08001d

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/OverlayTitleEditor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayImageView:Landroid/widget/ImageView;

    const v2, 0x7f08001e

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/OverlayTitleEditor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayChangeTitleTemplateButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayChangeTitleTemplateButton:Landroid/widget/Button;

    new-instance v3, Lcom/android/videoeditor/OverlayTitleEditor$2;

    invoke-direct {v3, p0}, Lcom/android/videoeditor/OverlayTitleEditor$2;-><init>(Lcom/android/videoeditor/OverlayTitleEditor;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f08001f

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/OverlayTitleEditor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mTitleView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v2, 0x7f080020

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/OverlayTitleEditor;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mSubtitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mSubtitleView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020013

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mPreviewWidth:I

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mPreviewHeight:I

    invoke-virtual {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "attributes"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/android/videoeditor/service/MovieOverlay;->getType(Landroid/os/Bundle;)I

    move-result v2

    iput v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayType:I

    iget-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mTitleView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/videoeditor/service/MovieOverlay;->getTitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mSubtitleView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/videoeditor/service/MovieOverlay;->getSubtitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-direct {p0}, Lcom/android/videoeditor/OverlayTitleEditor;->updatePreviewImage()V

    return-void

    :cond_0
    iput v4, p0, Lcom/android/videoeditor/OverlayTitleEditor;->mOverlayType:I

    goto :goto_0
.end method
