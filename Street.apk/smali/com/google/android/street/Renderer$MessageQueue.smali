.class Lcom/google/android/street/Renderer$MessageQueue;
.super Ljava/lang/Object;
.source "Renderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/Renderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageQueue"
.end annotation


# instance fields
.field mFree:Lcom/google/android/street/Renderer$Message;

.field mHead:Lcom/google/android/street/Renderer$Message;

.field mTail:Lcom/google/android/street/Renderer$Message;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/street/Renderer$1;)V
    .locals 0
    .param p1    # Lcom/google/android/street/Renderer$1;

    invoke-direct {p0}, Lcom/google/android/street/Renderer$MessageQueue;-><init>()V

    return-void
.end method


# virtual methods
.method get()Lcom/google/android/street/Renderer$Message;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/street/Renderer$MessageQueue;->mHead:Lcom/google/android/street/Renderer$Message;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/street/Renderer$Message;->next:Lcom/google/android/street/Renderer$Message;

    iput-object v1, p0, Lcom/google/android/street/Renderer$MessageQueue;->mHead:Lcom/google/android/street/Renderer$Message;

    iput-object v2, v0, Lcom/google/android/street/Renderer$Message;->next:Lcom/google/android/street/Renderer$Message;

    iget-object v1, p0, Lcom/google/android/street/Renderer$MessageQueue;->mHead:Lcom/google/android/street/Renderer$Message;

    if-nez v1, :cond_0

    iput-object v2, p0, Lcom/google/android/street/Renderer$MessageQueue;->mTail:Lcom/google/android/street/Renderer$Message;

    :cond_0
    return-object v0
.end method

.method isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/Renderer$MessageQueue;->mHead:Lcom/google/android/street/Renderer$Message;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method recycle(Lcom/google/android/street/Renderer$Message;)V
    .locals 1
    .param p1    # Lcom/google/android/street/Renderer$Message;

    iget-object v0, p0, Lcom/google/android/street/Renderer$MessageQueue;->mFree:Lcom/google/android/street/Renderer$Message;

    iput-object v0, p1, Lcom/google/android/street/Renderer$Message;->next:Lcom/google/android/street/Renderer$Message;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/google/android/street/Renderer$Message;->obj:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/street/Renderer$MessageQueue;->mFree:Lcom/google/android/street/Renderer$Message;

    return-void
.end method

.method send(IIILjava/lang/Object;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/street/Renderer$MessageQueue;->mFree:Lcom/google/android/street/Renderer$Message;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/street/Renderer$Message;->next:Lcom/google/android/street/Renderer$Message;

    iput-object v1, p0, Lcom/google/android/street/Renderer$MessageQueue;->mFree:Lcom/google/android/street/Renderer$Message;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/street/Renderer$Message;->next:Lcom/google/android/street/Renderer$Message;

    :goto_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/street/Renderer$Message;->set(IIILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/street/Renderer$MessageQueue;->mTail:Lcom/google/android/street/Renderer$Message;

    if-nez v1, :cond_1

    iput-object v0, p0, Lcom/google/android/street/Renderer$MessageQueue;->mTail:Lcom/google/android/street/Renderer$Message;

    iput-object v0, p0, Lcom/google/android/street/Renderer$MessageQueue;->mHead:Lcom/google/android/street/Renderer$Message;

    :goto_1
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/street/Renderer$Message;

    invoke-direct {v0}, Lcom/google/android/street/Renderer$Message;-><init>()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/street/Renderer$MessageQueue;->mTail:Lcom/google/android/street/Renderer$Message;

    iput-object v0, v1, Lcom/google/android/street/Renderer$Message;->next:Lcom/google/android/street/Renderer$Message;

    iput-object v0, p0, Lcom/google/android/street/Renderer$MessageQueue;->mTail:Lcom/google/android/street/Renderer$Message;

    goto :goto_1
.end method

.method sendCoalesce(IIILjava/lang/Object;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/street/Renderer$MessageQueue;->mTail:Lcom/google/android/street/Renderer$Message;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/street/Renderer$Message;->what:I

    if-ne v1, p1, :cond_0

    iput p2, v0, Lcom/google/android/street/Renderer$Message;->arg1:I

    iput p3, v0, Lcom/google/android/street/Renderer$Message;->arg2:I

    iput-object p4, v0, Lcom/google/android/street/Renderer$Message;->obj:Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/street/Renderer$MessageQueue;->send(IIILjava/lang/Object;)V

    goto :goto_0
.end method
