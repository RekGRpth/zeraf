.class public Lcom/google/android/street/DepthMap;
.super Ljava/lang/Object;
.source "DepthMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/DepthMap$Point;,
        Lcom/google/android/street/DepthMap$DepthMapPlane;
    }
.end annotation


# instance fields
.field private final compressedDepthMap:[B

.field private final compressedPanoMap:[B

.field private height:I

.field private isDecompressed:Z

.field private panoIds:[Ljava/lang/String;

.field private panoIndices:[B

.field private panoPoints:[Lcom/google/android/street/DepthMap$Point;

.field private planeIndices:[B

.field private planes:[Lcom/google/android/street/DepthMap$DepthMapPlane;

.field private width:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/DepthMap;->compressedDepthMap:[B

    iput-object v0, p0, Lcom/google/android/street/DepthMap;->compressedPanoMap:[B

    return-void
.end method

.method public constructor <init>([B[B)V
    .locals 1
    .param p1    # [B
    .param p2    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/street/DepthMap;->compressedDepthMap:[B

    invoke-direct {p0}, Lcom/google/android/street/DepthMap;->readCompressedDepthMap()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :cond_0
    iput-object p2, p0, Lcom/google/android/street/DepthMap;->compressedPanoMap:[B

    invoke-direct {p0}, Lcom/google/android/street/DepthMap;->readCompressedPanoMap()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/DepthMap;->isDecompressed:Z

    return-void
.end method

.method private floorAndUnwrap(FI)I
    .locals 1
    .param p1    # F
    .param p2    # I

    float-to-int v0, p1

    if-lt v0, p2, :cond_1

    sub-int/2addr v0, p2

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-gez v0, :cond_0

    add-int/2addr v0, p2

    goto :goto_0
.end method

.method private getInputStream([B)Lcom/google/android/street/LEDataInputStream;
    .locals 3
    .param p1    # [B

    new-instance v0, Lcom/google/android/street/LEDataInputStream;

    new-instance v1, Ljava/util/zip/InflaterInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lcom/google/android/street/LEDataInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method private getMatrixCol(F)I
    .locals 2
    .param p1    # F

    iget v0, p0, Lcom/google/android/street/DepthMap;->width:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    iget v1, p0, Lcom/google/android/street/DepthMap;->width:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/DepthMap;->floorAndUnwrap(FI)I

    move-result v0

    return v0
.end method

.method private getMatrixRow(F)I
    .locals 2
    .param p1    # F

    const/high16 v0, 0x3f800000

    const/high16 v1, 0x40000000

    mul-float/2addr v1, p1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/street/DepthMap;->height:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/street/DepthMap;->height:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/DepthMap;->floorAndUnwrap(FI)I

    move-result v0

    return v0
.end method

.method private readCompressedDepthMap()Z
    .locals 13

    const/16 v11, 0x8

    const/4 v12, 0x0

    iget-object v10, p0, Lcom/google/android/street/DepthMap;->compressedDepthMap:[B

    if-nez v10, :cond_0

    move v10, v12

    :goto_0
    return v10

    :cond_0
    iget-object v10, p0, Lcom/google/android/street/DepthMap;->compressedDepthMap:[B

    invoke-direct {p0, v10}, Lcom/google/android/street/DepthMap;->getInputStream([B)Lcom/google/android/street/LEDataInputStream;

    move-result-object v4

    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedByte()I

    move-result v2

    if-eq v2, v11, :cond_1

    const-string v10, "Depth map has unexpected header size"

    invoke-static {v10}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    move v10, v12

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedShort()I

    move-result v5

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedShort()I

    move-result v10

    iput v10, p0, Lcom/google/android/street/DepthMap;->width:I

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedShort()I

    move-result v10

    iput v10, p0, Lcom/google/android/street/DepthMap;->height:I

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedByte()I

    move-result v9

    if-eq v9, v11, :cond_2

    const-string v10, "Unexpected plane indices offset"

    invoke-static {v10}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    move v10, v12

    goto :goto_0

    :cond_2
    iget v10, p0, Lcom/google/android/street/DepthMap;->width:I

    iget v11, p0, Lcom/google/android/street/DepthMap;->height:I

    mul-int/2addr v10, v11

    new-array v10, v10, [B

    iput-object v10, p0, Lcom/google/android/street/DepthMap;->planeIndices:[B

    iget-object v10, p0, Lcom/google/android/street/DepthMap;->planeIndices:[B

    invoke-virtual {v4, v10}, Lcom/google/android/street/LEDataInputStream;->readFully([B)V

    new-array v10, v5, [Lcom/google/android/street/DepthMap$DepthMapPlane;

    iput-object v10, p0, Lcom/google/android/street/DepthMap;->planes:[Lcom/google/android/street/DepthMap$DepthMapPlane;

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readFloat()F

    move-result v6

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readFloat()F

    move-result v7

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readFloat()F

    move-result v8

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readFloat()F

    move-result v0

    iget-object v10, p0, Lcom/google/android/street/DepthMap;->planes:[Lcom/google/android/street/DepthMap$DepthMapPlane;

    new-instance v11, Lcom/google/android/street/DepthMap$DepthMapPlane;

    invoke-direct {v11, v6, v7, v8, v0}, Lcom/google/android/street/DepthMap$DepthMapPlane;-><init>(FFFF)V

    aput-object v11, v10, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v10

    move-object v1, v10

    const-string v10, "Unable to decompress depth map"

    invoke-static {v10, v1}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v10, v12

    goto :goto_0

    :cond_3
    const/4 v10, 0x1

    goto :goto_0
.end method

.method private readCompressedPanoMap()Z
    .locals 15

    const/16 v12, 0x8

    const/4 v14, 0x0

    iget-object v11, p0, Lcom/google/android/street/DepthMap;->compressedPanoMap:[B

    if-nez v11, :cond_0

    move v11, v14

    :goto_0
    return v11

    :cond_0
    iget-object v11, p0, Lcom/google/android/street/DepthMap;->compressedPanoMap:[B

    invoke-direct {p0, v11}, Lcom/google/android/street/DepthMap;->getInputStream([B)Lcom/google/android/street/LEDataInputStream;

    move-result-object v4

    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedByte()I

    move-result v2

    if-eq v2, v12, :cond_1

    const-string v11, "Pano map has unexpected header size"

    invoke-static {v11}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    move v11, v14

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedShort()I

    move-result v7

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedShort()I

    move-result v10

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedShort()I

    move-result v1

    iget v11, p0, Lcom/google/android/street/DepthMap;->width:I

    if-ne v11, v10, :cond_2

    iget v11, p0, Lcom/google/android/street/DepthMap;->height:I

    if-eq v11, v1, :cond_3

    :cond_2
    const-string v11, "Pano matrix dimensions don\'t match plane matrix."

    invoke-static {v11}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    move v11, v14

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readUnsignedByte()I

    move-result v8

    if-eq v8, v12, :cond_4

    const-string v11, "Unexpected pano indices offset"

    invoke-static {v11}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    move v11, v14

    goto :goto_0

    :cond_4
    iget v11, p0, Lcom/google/android/street/DepthMap;->width:I

    iget v12, p0, Lcom/google/android/street/DepthMap;->height:I

    mul-int/2addr v11, v12

    new-array v11, v11, [B

    iput-object v11, p0, Lcom/google/android/street/DepthMap;->panoIndices:[B

    iget-object v11, p0, Lcom/google/android/street/DepthMap;->panoIndices:[B

    invoke-virtual {v4, v11}, Lcom/google/android/street/LEDataInputStream;->readFully([B)V

    new-array v11, v7, [Ljava/lang/String;

    iput-object v11, p0, Lcom/google/android/street/DepthMap;->panoIds:[Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/street/DepthMap;->panoIds:[Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput-object v13, v11, v12

    const/16 v11, 0x16

    new-array v9, v11, [B

    const/4 v3, 0x1

    :goto_1
    if-ge v3, v7, :cond_5

    invoke-virtual {v4, v9}, Lcom/google/android/street/LEDataInputStream;->readFully([B)V

    iget-object v11, p0, Lcom/google/android/street/DepthMap;->panoIds:[Ljava/lang/String;

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v9}, Ljava/lang/String;-><init>([B)V

    aput-object v12, v11, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    new-array v11, v7, [Lcom/google/android/street/DepthMap$Point;

    iput-object v11, p0, Lcom/google/android/street/DepthMap;->panoPoints:[Lcom/google/android/street/DepthMap$Point;

    iget-object v11, p0, Lcom/google/android/street/DepthMap;->panoPoints:[Lcom/google/android/street/DepthMap$Point;

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput-object v13, v11, v12

    const/4 v3, 0x1

    :goto_2
    if-ge v3, v7, :cond_6

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readFloat()F

    move-result v5

    invoke-virtual {v4}, Lcom/google/android/street/LEDataInputStream;->readFloat()F

    move-result v6

    iget-object v11, p0, Lcom/google/android/street/DepthMap;->panoPoints:[Lcom/google/android/street/DepthMap$Point;

    new-instance v12, Lcom/google/android/street/DepthMap$Point;

    invoke-direct {v12, v5, v6}, Lcom/google/android/street/DepthMap$Point;-><init>(FF)V

    aput-object v12, v11, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :catch_0
    move-exception v11

    move-object v0, v11

    const-string v11, "Unable to decompress pano map"

    invoke-static {v11, v0}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v11, v14

    goto/16 :goto_0

    :cond_6
    const/4 v11, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public assertDecompressed()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/street/DepthMap;->isDecompressed:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Depth map must be decompressed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public compress()Z
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/street/DepthMap;->isDecompressed:Z

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    iput-object v1, p0, Lcom/google/android/street/DepthMap;->planeIndices:[B

    iput-object v1, p0, Lcom/google/android/street/DepthMap;->planes:[Lcom/google/android/street/DepthMap$DepthMapPlane;

    iput-object v1, p0, Lcom/google/android/street/DepthMap;->panoIndices:[B

    iput-object v1, p0, Lcom/google/android/street/DepthMap;->panoIds:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/street/DepthMap;->panoPoints:[Lcom/google/android/street/DepthMap$Point;

    iput-boolean v2, p0, Lcom/google/android/street/DepthMap;->isDecompressed:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public computeDepthAndNormal(FF[F)F
    .locals 8
    .param p1    # F
    .param p2    # F
    .param p3    # [F

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/street/DepthMap;->getPlane(FF)Lcom/google/android/street/DepthMap$DepthMapPlane;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    if-eqz p3, :cond_1

    array-length v3, p3

    if-lt v3, v7, :cond_1

    iget v3, v2, Lcom/google/android/street/DepthMap$DepthMapPlane;->nx:F

    aput v3, p3, v4

    iget v3, v2, Lcom/google/android/street/DepthMap$DepthMapPlane;->nz:F

    neg-float v3, v3

    aput v3, p3, v5

    iget v3, v2, Lcom/google/android/street/DepthMap$DepthMapPlane;->ny:F

    aput v3, p3, v6

    :cond_1
    new-array v1, v7, [F

    invoke-static {p1, p2, v1, v4}, Lcom/google/android/street/StreetMath;->sphericalToRectangularCoords(FF[FI)V

    aget v3, v1, v4

    aget v4, v1, v6

    neg-float v4, v4

    aget v5, v1, v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/street/DepthMap$DepthMapPlane;->directionDepth(FFF)F

    move-result v0

    move v3, v0

    goto :goto_0
.end method

.method public decompress()Z
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/street/DepthMap;->isDecompressed:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/street/DepthMap;->readCompressedDepthMap()Z

    invoke-direct {p0}, Lcom/google/android/street/DepthMap;->readCompressedPanoMap()Z

    iput-boolean v1, p0, Lcom/google/android/street/DepthMap;->isDecompressed:Z

    move v0, v1

    goto :goto_0
.end method

.method public getCompressedDepthMap()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/DepthMap;->compressedDepthMap:[B

    return-object v0
.end method

.method public getCompressedPanoMap()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/DepthMap;->compressedPanoMap:[B

    return-object v0
.end method

.method public getPanoId(FF[F)Ljava/lang/String;
    .locals 11
    .param p1    # F
    .param p2    # F
    .param p3    # [F

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    invoke-direct {p0, p1}, Lcom/google/android/street/DepthMap;->getMatrixCol(F)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/google/android/street/DepthMap;->getMatrixRow(F)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/street/DepthMap;->panoIndices:[B

    iget v8, p0, Lcom/google/android/street/DepthMap;->width:I

    mul-int/2addr v8, v6

    add-int/2addr v8, v0

    aget-byte v7, v7, v8

    and-int/lit16 v4, v7, 0xff

    if-eqz p3, :cond_1

    if-lez v4, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/google/android/street/DepthMap;->getPlane(FF)Lcom/google/android/street/DepthMap$DepthMapPlane;

    move-result-object v5

    const/4 v7, 0x3

    new-array v2, v7, [F

    const/4 v7, 0x0

    invoke-static {p1, p2, v2, v7}, Lcom/google/android/street/StreetMath;->sphericalToRectangularCoords(FF[FI)V

    const/4 v7, 0x0

    aget v7, v2, v7

    const/4 v8, 0x2

    aget v8, v2, v8

    neg-float v8, v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v5, v7, v8, v9}, Lcom/google/android/street/DepthMap$DepthMapPlane;->directionDepth(FFF)F

    move-result v1

    const/4 v3, 0x0

    :goto_0
    const/4 v7, 0x3

    if-ge v3, v7, :cond_0

    aget v7, v2, v3

    mul-float/2addr v7, v1

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    aget v7, v2, v7

    iget-object v8, p0, Lcom/google/android/street/DepthMap;->panoPoints:[Lcom/google/android/street/DepthMap$Point;

    aget-object v8, v8, v4

    iget v8, v8, Lcom/google/android/street/DepthMap$Point;->x:F

    sub-float/2addr v7, v8

    const/4 v8, 0x1

    aget v8, v2, v8

    const/4 v9, 0x2

    aget v9, v2, v9

    iget-object v10, p0, Lcom/google/android/street/DepthMap;->panoPoints:[Lcom/google/android/street/DepthMap$Point;

    aget-object v10, v10, v4

    iget v10, v10, Lcom/google/android/street/DepthMap$Point;->y:F

    add-float/2addr v9, v10

    invoke-static {v7, v8, v9, p3}, Lcom/google/android/street/StreetMath;->rectangularToSphericalCoords(FFF[F)V

    :cond_1
    iget-object v7, p0, Lcom/google/android/street/DepthMap;->panoIds:[Ljava/lang/String;

    aget-object v7, v7, v4

    return-object v7
.end method

.method public getPanoId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    iget-object v0, p0, Lcom/google/android/street/DepthMap;->panoIds:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getPanoPoint(FF)Lcom/google/android/street/DepthMap$Point;
    .locals 5
    .param p1    # F
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    invoke-direct {p0, p1}, Lcom/google/android/street/DepthMap;->getMatrixCol(F)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/google/android/street/DepthMap;->getMatrixRow(F)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/street/DepthMap;->panoIndices:[B

    iget v4, p0, Lcom/google/android/street/DepthMap;->width:I

    mul-int/2addr v4, v2

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    and-int/lit16 v1, v3, 0xff

    invoke-virtual {p0, v1}, Lcom/google/android/street/DepthMap;->getPanoPoint(I)Lcom/google/android/street/DepthMap$Point;

    move-result-object v3

    return-object v3
.end method

.method public getPanoPoint(I)Lcom/google/android/street/DepthMap$Point;
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    iget-object v0, p0, Lcom/google/android/street/DepthMap;->panoPoints:[Lcom/google/android/street/DepthMap$Point;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getPlane(FF)Lcom/google/android/street/DepthMap$DepthMapPlane;
    .locals 2
    .param p1    # F
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/street/DepthMap;->getPlaneIndex(FF)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/street/DepthMap;->planes:[Lcom/google/android/street/DepthMap$DepthMapPlane;

    aget-object v1, v1, v0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPlaneIndex(FF)I
    .locals 5
    .param p1    # F
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    invoke-direct {p0, p1}, Lcom/google/android/street/DepthMap;->getMatrixCol(F)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/google/android/street/DepthMap;->getMatrixRow(F)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/street/DepthMap;->planeIndices:[B

    iget v4, p0, Lcom/google/android/street/DepthMap;->width:I

    mul-int/2addr v4, v2

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    and-int/lit16 v1, v3, 0xff

    return v1
.end method

.method public isGround(FF)Z
    .locals 2
    .param p1    # F
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/street/DepthMap;->getPlane(FF)Lcom/google/android/street/DepthMap$DepthMapPlane;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/street/DepthMap$DepthMapPlane;->isGround()Z

    move-result v1

    goto :goto_0
.end method

.method public numPanos()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/street/DepthMap;->assertDecompressed()V

    iget-object v0, p0, Lcom/google/android/street/DepthMap;->panoIds:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method
