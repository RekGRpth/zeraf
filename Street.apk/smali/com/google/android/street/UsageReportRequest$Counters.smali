.class Lcom/google/android/street/UsageReportRequest$Counters;
.super Ljava/lang/Object;
.source "UsageReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/UsageReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Counters"
.end annotation


# instance fields
.field navigationCount:I

.field panningCount:I

.field final sceneType:I

.field zoomingCount:I


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/street/UsageReportRequest$Counters;->sceneType:I

    return-void
.end method

.method static setPositiveInt(ILcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;I)V
    .locals 0
    .param p0    # I
    .param p1    # Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;
    .param p2    # I

    if-lez p0, :cond_0

    invoke-virtual {p1, p2, p0}, Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_0
    return-void
.end method


# virtual methods
.method addViewpointProto(Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4
    .param p1    # Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/android/street/UsageReportRequest$Counters;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, v3}, Lcom/google/mobile/googlenav/common/io/protocol/ProtoBufUtil;->addProtoBuf(Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget v1, p0, Lcom/google/android/street/UsageReportRequest$Counters;->sceneType:I

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/street/UsageReportRequest$Counters;->setPositiveInt(ILcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;I)V

    iget v1, p0, Lcom/google/android/street/UsageReportRequest$Counters;->panningCount:I

    const/4 v2, 0x2

    invoke-static {v1, v0, v2}, Lcom/google/android/street/UsageReportRequest$Counters;->setPositiveInt(ILcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;I)V

    iget v1, p0, Lcom/google/android/street/UsageReportRequest$Counters;->zoomingCount:I

    const/4 v2, 0x3

    invoke-static {v1, v0, v2}, Lcom/google/android/street/UsageReportRequest$Counters;->setPositiveInt(ILcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;I)V

    iget v1, p0, Lcom/google/android/street/UsageReportRequest$Counters;->navigationCount:I

    invoke-static {v1, v0, v3}, Lcom/google/android/street/UsageReportRequest$Counters;->setPositiveInt(ILcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;I)V

    goto :goto_0
.end method

.method isEmpty()Z
    .locals 1

    iget v0, p0, Lcom/google/android/street/UsageReportRequest$Counters;->panningCount:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/street/UsageReportRequest$Counters;->zoomingCount:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/street/UsageReportRequest$Counters;->navigationCount:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
