.class Lcom/google/android/street/TrackballGestureDetector;
.super Ljava/lang/Object;
.source "TrackballGestureDetector.java"


# instance fields
.field private mAlwaysInTapRegion:Z

.field private mCurrentDownX:F

.field private mCurrentDownY:F

.field private mDownTime:J

.field private mFirstDownX:F

.field private mFirstDownY:F

.field private mHandler:Landroid/os/Handler;

.field private mInLongPress:Z

.field private mIsDoubleTap:Z

.field private mIsScroll:Z

.field private mIsTap:Z

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mLongPressEndTime:J

.field private mLongPressRepeatRateMs:I

.field private mLongpressTimeMs:I

.field private mOurLongPressRunnable:Ljava/lang/Runnable;

.field private mPressed:Z

.field private mScrollX:F

.field private mScrollY:F

.field private mUserLongPressRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/street/TrackballGestureDetector;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x5dc

    iput v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongpressTimeMs:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongPressRepeatRateMs:I

    new-instance v0, Lcom/google/android/street/TrackballGestureDetector$1;

    invoke-direct {v0, p0}, Lcom/google/android/street/TrackballGestureDetector$1;-><init>(Lcom/google/android/street/TrackballGestureDetector;)V

    iput-object v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mOurLongPressRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/street/TrackballGestureDetector;)V
    .locals 0
    .param p0    # Lcom/google/android/street/TrackballGestureDetector;

    invoke-direct {p0}, Lcom/google/android/street/TrackballGestureDetector;->dispatchLongPress()V

    return-void
.end method

.method private dispatchLongPress()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mInLongPress:Z

    iget-object v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mUserLongPressRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mUserLongPressRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mUserLongPressRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongPressRepeatRateMs:I

    if-lez v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongPressEndTime:J

    iget v2, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongPressRepeatRateMs:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongPressEndTime:J

    iget-object v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/street/TrackballGestureDetector;->mOurLongPressRunnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongPressEndTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public analyze(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput-boolean v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mIsScroll:Z

    iput-boolean v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mIsTap:Z

    iput-boolean v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mIsDoubleTap:Z

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iput v2, p0, Lcom/google/android/street/TrackballGestureDetector;->mLastMotionX:F

    iput v3, p0, Lcom/google/android/street/TrackballGestureDetector;->mLastMotionY:F

    iget v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mCurrentDownX:F

    iput v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mFirstDownX:F

    iget v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mCurrentDownY:F

    iput v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mFirstDownY:F

    iput v2, p0, Lcom/google/android/street/TrackballGestureDetector;->mCurrentDownX:F

    iput v3, p0, Lcom/google/android/street/TrackballGestureDetector;->mCurrentDownY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mDownTime:J

    iput-boolean v7, p0, Lcom/google/android/street/TrackballGestureDetector;->mAlwaysInTapRegion:Z

    iput-boolean v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mInLongPress:Z

    iput-boolean v7, p0, Lcom/google/android/street/TrackballGestureDetector;->mPressed:Z

    iget-object v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/street/TrackballGestureDetector;->mOurLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-wide v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mDownTime:J

    iget v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongpressTimeMs:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongPressEndTime:J

    iget-object v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/street/TrackballGestureDetector;->mOurLongPressRunnable:Ljava/lang/Runnable;

    iget-wide v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mLongPressEndTime:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :pswitch_1
    iget-boolean v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mInLongPress:Z

    if-nez v4, :cond_0

    iget v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mLastMotionX:F

    sub-float/2addr v4, v2

    iput v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mScrollX:F

    iget v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mLastMotionY:F

    sub-float/2addr v4, v3

    iput v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mScrollY:F

    iput v2, p0, Lcom/google/android/street/TrackballGestureDetector;->mLastMotionX:F

    iput v3, p0, Lcom/google/android/street/TrackballGestureDetector;->mLastMotionY:F

    iget v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mCurrentDownX:F

    sub-float v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/google/android/street/TrackballGestureDetector;->mCurrentDownY:F

    sub-float v5, v3, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v1, v4

    const/4 v4, 0x5

    if-le v1, v4, :cond_1

    iput-boolean v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mAlwaysInTapRegion:Z

    iget-object v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/street/TrackballGestureDetector;->mOurLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_1
    iput-boolean v7, p0, Lcom/google/android/street/TrackballGestureDetector;->mIsScroll:Z

    goto :goto_0

    :pswitch_2
    iput-boolean v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mPressed:Z

    iget-object v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/street/TrackballGestureDetector;->mOurLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mInLongPress:Z

    if-eqz v4, :cond_2

    iput-boolean v6, p0, Lcom/google/android/street/TrackballGestureDetector;->mInLongPress:Z

    goto :goto_0

    :cond_2
    iget-boolean v4, p0, Lcom/google/android/street/TrackballGestureDetector;->mAlwaysInTapRegion:Z

    if-eqz v4, :cond_0

    iput-boolean v7, p0, Lcom/google/android/street/TrackballGestureDetector;->mIsTap:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public isScroll()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mIsScroll:Z

    return v0
.end method

.method public isTap()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/street/TrackballGestureDetector;->mIsTap:Z

    return v0
.end method
