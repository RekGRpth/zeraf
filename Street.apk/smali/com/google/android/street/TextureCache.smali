.class Lcom/google/android/street/TextureCache;
.super Lcom/google/android/street/LRUCache;
.source "TextureCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/street/LRUCache",
        "<",
        "Lcom/google/android/street/PanoramaImageKey;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActiveTextures:Ljava/util/BitSet;

.field private mGl:Ljavax/microedition/khronos/opengles/GL10;

.field private final mInitializedTextures:Ljava/util/BitSet;

.field private final mTextureIds:[I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/street/LRUCache;-><init>(I)V

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0, p1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/street/TextureCache;->mActiveTextures:Ljava/util/BitSet;

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0, p1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/street/TextureCache;->mInitializedTextures:Ljava/util/BitSet;

    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/street/TextureCache;->mTextureIds:[I

    return-void
.end method

.method private checkTexImage(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 0
    .param p1    # Lcom/google/android/street/PanoramaImageKey;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Ljavax/microedition/khronos/opengles/GL10;

    return-void
.end method

.method private checkgl(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "glError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method private finished()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/street/TextureCache;->mTextureIds:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/TextureCache;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/TextureCache;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    iget v1, p0, Lcom/google/android/street/TextureCache;->mMaxEntries:I

    iget-object v2, p0, Lcom/google/android/street/TextureCache;->mTextureIds:[I

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/street/TextureCache;->clear()V

    iget-object v0, p0, Lcom/google/android/street/TextureCache;->mInitializedTextures:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    return-void
.end method

.method private insertImageImp(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1    # Lcom/google/android/street/PanoramaImageKey;
    .param p2    # Landroid/graphics/Bitmap;

    const v4, 0x47012f00

    const/4 v6, 0x0

    const/16 v5, 0xde1

    invoke-virtual {p0, p1}, Lcom/google/android/street/TextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Already cached "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/street/TextureCache;->ensureSpaceForInsertion()V

    iget-object v2, p0, Lcom/google/android/street/TextureCache;->mActiveTextures:Ljava/util/BitSet;

    invoke-virtual {v2, v6}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/street/TextureCache;->mMaxEntries:I

    if-lt v1, v2, :cond_1

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Ran out of texture cache slots."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/TextureCache;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    if-nez v0, :cond_2

    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/street/TextureCache;->mTextureIds:[I

    aget v2, v2, v1

    invoke-interface {v0, v5, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    iget-object v2, p0, Lcom/google/android/street/TextureCache;->mInitializedTextures:Ljava/util/BitSet;

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_3

    const/16 v2, 0x2801

    const/high16 v3, 0x46180000

    invoke-interface {v0, v5, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v2, 0x2800

    const v3, 0x46180400

    invoke-interface {v0, v5, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v2, 0x2802

    invoke-interface {v0, v5, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v2, 0x2803

    invoke-interface {v0, v5, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const v4, 0x45f00800

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    invoke-static {v5, v6, p2, v6}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/street/TextureCache;->checkTexImage(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v2, p0, Lcom/google/android/street/TextureCache;->mInitializedTextures:Ljava/util/BitSet;

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->set(I)V

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/street/TextureCache;->checkgl(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v2, p0, Lcom/google/android/street/TextureCache;->mActiveTextures:Ljava/util/BitSet;

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->set(I)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/street/TextureCache;->insert(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-static {v5, v6, p2, v6}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/street/TextureCache;->checkTexImage(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_1
.end method


# virtual methods
.method public getTextureId(Lcom/google/android/street/PanoramaImageKey;)I
    .locals 3
    .param p1    # Lcom/google/android/street/PanoramaImageKey;

    invoke-virtual {p0, p1}, Lcom/google/android/street/TextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/street/TextureCache;->mTextureIds:[I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget v1, v1, v2

    goto :goto_0
.end method

.method public initialize(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    iput-object p1, p0, Lcom/google/android/street/TextureCache;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    iget v0, p0, Lcom/google/android/street/TextureCache;->mMaxEntries:I

    iget-object v1, p0, Lcom/google/android/street/TextureCache;->mTextureIds:[I

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    return-void
.end method

.method public insertImage(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Lcom/google/android/street/PanoramaImageKey;
    .param p2    # Landroid/graphics/Bitmap;

    if-eqz p2, :cond_0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/street/TextureCache;->insertImageImp(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    throw v0
.end method

.method protected onEject(Lcom/google/android/street/PanoramaImageKey;Ljava/lang/Integer;)V
    .locals 3
    .param p1    # Lcom/google/android/street/PanoramaImageKey;
    .param p2    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/street/TextureCache;->mActiveTextures:Ljava/util/BitSet;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ejecting unused texture "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/TextureCache;->mActiveTextures:Ljava/util/BitSet;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    return-void
.end method

.method protected bridge synthetic onEject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/street/PanoramaImageKey;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/street/TextureCache;->onEject(Lcom/google/android/street/PanoramaImageKey;Ljava/lang/Integer;)V

    return-void
.end method

.method public shutdown()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/street/TextureCache;->finished()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/TextureCache;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    return-void
.end method
