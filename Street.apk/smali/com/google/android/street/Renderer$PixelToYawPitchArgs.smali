.class Lcom/google/android/street/Renderer$PixelToYawPitchArgs;
.super Ljava/lang/Object;
.source "Renderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/Renderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PixelToYawPitchArgs"
.end annotation


# instance fields
.field public mDone:Z

.field public final mIsVehicleRelative:Z

.field public final mX:F

.field public final mY:F

.field public mYawPitchOut:[F


# direct methods
.method constructor <init>(FFZ)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mX:F

    iput p2, p0, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mY:F

    iput-boolean p3, p0, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mIsVehicleRelative:Z

    return-void
.end method
