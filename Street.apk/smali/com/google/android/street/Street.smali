.class public Lcom/google/android/street/Street;
.super Landroid/app/Activity;
.source "Street.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/Street$DrdListener;
    }
.end annotation


# static fields
.field public static final IN_EMULATOR:Z

.field private static mGlobalStateInitialized:Z

.field private static sStartFrameConfig:Lcom/google/android/street/PanoramaConfig;

.field private static final sStartFrameLock:Ljava/lang/Object;

.field private static sStartFrameReason:Ljava/lang/String;

.field private static sStartFrameTimestampMs:J


# instance fields
.field private mActionBarInUse:Z

.field private mDefaultDisplay:Landroid/view/Display;

.field private volatile mDrdNetworkError:I

.field private mHasCompass:Z

.field private mHttpCache:Lcom/google/android/street/HttpCache;

.field private mIsTablet:Z

.field private mLocation:Landroid/location/Location;

.field private mMapZoom:I

.field private mNetworkAvailable:Z

.field private mNetworkStateChangedFilter:Landroid/content/IntentFilter;

.field private mNetworkStateIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mPanoramaManager:Lcom/google/android/street/PanoramaManager;

.field private mRegisteredForNetworkConnectivity:Z

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mStreetView:Lcom/google/android/street/StreetView;

.field protected mTitleText:Landroid/widget/TextView;

.field private timer:Lcom/google/android/street/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/street/Street;->sStartFrameLock:Ljava/lang/Object;

    const-string v0, "Cold start"

    sput-object v0, Lcom/google/android/street/Street;->sStartFrameReason:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/street/Street;->sStartFrameTimestampMs:J

    invoke-static {}, Lcom/google/android/street/Street;->inEmulator()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/street/Street;->IN_EMULATOR:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/street/Street;->mDrdNetworkError:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/street/Street;->mMapZoom:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/Street;->timer:Lcom/google/android/street/Timer;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Street;->mHasCompass:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/street/Street;)I
    .locals 1
    .param p0    # Lcom/google/android/street/Street;

    iget v0, p0, Lcom/google/android/street/Street;->mDrdNetworkError:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/street/Street;I)I
    .locals 0
    .param p0    # Lcom/google/android/street/Street;
    .param p1    # I

    iput p1, p0, Lcom/google/android/street/Street;->mDrdNetworkError:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/street/Street;)Lcom/google/android/street/StreetView;
    .locals 1
    .param p0    # Lcom/google/android/street/Street;

    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/street/Street;)Z
    .locals 1
    .param p0    # Lcom/google/android/street/Street;

    invoke-direct {p0}, Lcom/google/android/street/Street;->checkNetworkAvailability()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/street/Street;Z)V
    .locals 0
    .param p0    # Lcom/google/android/street/Street;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/street/Street;->onNetworkToggle(Z)V

    return-void
.end method

.method private static canReadUserLocation(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/google/android/street/GoogleLocationSettingHelper;->isEnforceable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/street/GoogleLocationSettingHelper;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkNetworkAvailability()Z
    .locals 1

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/street/Street;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/ConnectivityManager;

    invoke-virtual {p0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getLatLonAndOrientation(Z)Ljava/lang/String;
    .locals 4
    .param p1    # Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v2}, Lcom/google/android/street/StreetView;->getPanoramaConfig()Lcom/google/android/street/PanoramaConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cbll="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/street/PanoramaConfig;->mLatLng:Lcom/google/android/street/MapPoint;

    invoke-virtual {v3}, Lcom/google/android/street/MapPoint;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v2}, Lcom/google/android/street/StreetView;->getUserOrientation()Lcom/google/android/street/UserOrientation;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/street/Street;->getOrientation(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getOrientation(Z)Ljava/lang/String;
    .locals 6
    .param p1    # Z

    const/high16 v5, 0x42300000

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v3}, Lcom/google/android/street/StreetView;->getUserOrientation()Lcom/google/android/street/UserOrientation;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "&cbp=1,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v3

    const/high16 v4, 0x3f000000

    sub-float/2addr v3, v4

    const/high16 v4, -0x3ccc0000

    mul-float v0, v3, v4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ",,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v3

    add-float/2addr v3, v5

    add-float/2addr v3, v5

    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v4

    add-float/2addr v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getStateString(Z)Ljava/lang/String;
    .locals 3
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/street/Street;->getLatLonAndOrientation(Z)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/street/Street;->mMapZoom:I

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&mz="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/street/Street;->mMapZoom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private gotoMap()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->getPanoramaConfig()Lcom/google/android/street/PanoramaConfig;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "geo:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mLatLng:Lcom/google/android/street/MapPoint;

    invoke-virtual {v0}, Lcom/google/android/street/MapPoint;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->getUserOrientation()Lcom/google/android/street/UserOrientation;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    const-string v2, "?cbp=1,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ",,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    :goto_0
    iget v2, p0, Lcom/google/android/street/Street;->mMapZoom:I

    if-eqz v2, :cond_0

    if-nez v0, :cond_2

    const/16 v0, 0x3f

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    const-string v0, "z="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/android/street/Street;->mMapZoom:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/street/Street;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x26

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private static inEmulator()Z
    .locals 2

    const-string v1, "QEMU"

    invoke-static {v1}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "StreetView"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logEndFrame(Lcom/google/android/street/PanoramaConfig;)V
    .locals 6

    sget-object v0, Lcom/google/android/street/Street;->sStartFrameLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/android/street/Street;->sStartFrameReason:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/street/Street;->sStartFrameConfig:Lcom/google/android/street/PanoramaConfig;

    if-eq v1, p0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/street/Street;->sStartFrameReason:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " latency: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/google/android/street/Street;->sStartFrameTimestampMs:J

    sub-long/2addr v1, v4

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/street/Street;->sStartFrameReason:Ljava/lang/String;

    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/street/Street;->sStartFrameConfig:Lcom/google/android/street/PanoramaConfig;

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static logI(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "StreetView"

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logI(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/Throwable;

    const-string v0, "StreetView"

    invoke-static {v0, p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public static noteStartFrame(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/street/Street;->noteStartFrame(Ljava/lang/String;Lcom/google/android/street/PanoramaConfig;)V

    return-void
.end method

.method public static noteStartFrame(Ljava/lang/String;Lcom/google/android/street/PanoramaConfig;)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/street/PanoramaConfig;

    sget-object v0, Lcom/google/android/street/Street;->sStartFrameLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/android/street/Street;->sStartFrameReason:Ljava/lang/String;

    if-nez v1, :cond_0

    sput-object p0, Lcom/google/android/street/Street;->sStartFrameReason:Ljava/lang/String;

    sput-object p1, Lcom/google/android/street/Street;->sStartFrameConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    sput-wide v1, Lcom/google/android/street/Street;->sStartFrameTimestampMs:J

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onNetworkToggle(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/street/Street;->mRegisteredForNetworkConnectivity:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/street/Street;->mNetworkAvailable:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/street/Street;->mNetworkAvailable:Z

    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->invalidate()V

    :cond_0
    return-void
.end method

.method private static parseFloat(Ljava/lang/String;)F
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    double-to-float v1, v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private processIntent(Landroid/os/Bundle;)V
    .locals 43
    .param p1    # Landroid/os/Bundle;

    const/16 v36, 0x0

    const/16 v26, 0x0

    const/16 v32, 0x0

    const/16 v29, 0x0

    const/16 v24, 0x0

    const/16 v21, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v13, 0x0

    const/16 v22, 0x0

    const/high16 v23, -0x40800000

    const/high16 v30, -0x40800000

    const/16 v33, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mSavedInstanceState:Landroid/os/Bundle;

    move-object v5, v0

    if-eqz v5, :cond_1

    if-nez p1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 p1, v0

    :cond_0
    const/4 v5, 0x0

    move-object v0, v5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/street/Street;->mSavedInstanceState:Landroid/os/Bundle;

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/street/Street;->getIntent()Landroid/content/Intent;

    move-result-object v25

    if-eqz v25, :cond_3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/street/Street;->setIntent(Landroid/content/Intent;)V

    invoke-virtual/range {v25 .. v25}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v15, 0x1

    invoke-virtual/range {v25 .. v25}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v18

    const-string v5, "google.streetview"

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v36

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/google/android/street/StreetView;->hasPanorama()Z

    move-result v5

    if-eqz v5, :cond_4

    if-nez v15, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/google/android/street/StreetView;->reloadPanorama()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/google/android/street/StreetView;->invalidate()V

    move-object/from16 v10, v16

    move-object/from16 v9, v21

    move-object/from16 v8, v29

    move-object/from16 v6, v32

    move-object/from16 v7, v26

    :goto_0
    return-void

    :cond_3
    if-nez p1, :cond_2

    move-object/from16 v10, v16

    move-object/from16 v9, v21

    move-object/from16 v8, v29

    move-object/from16 v6, v32

    move-object/from16 v7, v26

    goto :goto_0

    :cond_4
    if-nez p1, :cond_5

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/street/StreetView;->toggleCompassMode(ZZ)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    const/4 v6, 0x0

    new-instance v7, Lcom/google/android/street/MapPoint;

    const-wide v39, 0x4042e5ed288ce704L

    const-wide v41, -0x3fa166b851eb851fL

    move-object v0, v7

    move-wide/from16 v1, v39

    move-wide/from16 v3, v41

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/street/MapPoint;-><init>(DD)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/street/StreetView;->loadPanorama(Ljava/lang/String;Lcom/google/android/street/MapPoint;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/MapPoint;Lcom/google/android/street/PanoramaConfig;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/google/android/street/StreetView;->invalidate()V

    move-object/from16 v10, v16

    move-object/from16 v9, v21

    move-object/from16 v8, v29

    move-object/from16 v6, v32

    move-object/from16 v7, v26

    goto :goto_0

    :cond_5
    if-nez v36, :cond_14

    if-eqz p1, :cond_14

    const-string v5, "streetview"

    move-object/from16 v0, p1

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_14

    const-string v5, "streetview"

    move-object/from16 v0, p1

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    const-string v5, "panoramaConfig"

    move-object/from16 v0, p1

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_14

    :try_start_0
    const-string v5, "panoramaConfig"

    move-object/from16 v0, p1

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/street/PanoramaConfig;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-eqz v36, :cond_13

    const/16 v11, 0x2c

    const/16 v12, 0x5f

    const/16 v5, 0x2c

    const/16 v6, 0x5f

    :try_start_1
    move-object/from16 v0, v36

    move v1, v5

    move v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v35

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "http://example.com/cb?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object v0, v5

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    const-string v5, "cbll"

    move-object/from16 v0, v19

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    if-eqz v27, :cond_12

    const/16 v5, 0x5f

    const/16 v6, 0x2c

    move-object/from16 v0, v27

    move v1, v5

    move v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/street/MapPoint;->parse(Ljava/lang/String;)Lcom/google/android/street/MapPoint;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v7

    if-eqz v7, :cond_6

    const/16 v24, 0x1

    :cond_6
    :goto_2
    :try_start_2
    const-string v5, "panoid"

    move-object/from16 v0, v19

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v6

    if-eqz v6, :cond_7

    const/16 v24, 0x1

    :cond_7
    :try_start_3
    const-string v5, "cbp"

    move-object/from16 v0, v19

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    if-eqz v31, :cond_11

    const-string v5, "_"

    move-object/from16 v0, v31

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    array-length v5, v14

    const/4 v8, 0x4

    if-lt v5, v8, :cond_11

    new-instance v8, Lcom/google/android/street/UserOrientation;

    invoke-direct {v8}, Lcom/google/android/street/UserOrientation;-><init>()V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_4

    :try_start_4
    array-length v5, v14

    const/4 v9, 0x4

    if-ne v5, v9, :cond_c

    const/4 v5, 0x1

    aget-object v5, v14, v5

    invoke-static {v5}, Lcom/google/android/street/Street;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {v8, v5}, Lcom/google/android/street/UserOrientation;->setYaw(F)V

    const/4 v5, 0x2

    aget-object v5, v14, v5

    invoke-static {v5}, Lcom/google/android/street/Street;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {v8, v5}, Lcom/google/android/street/UserOrientation;->setTilt(F)V

    const/4 v5, 0x3

    aget-object v5, v14, v5

    invoke-static {v5}, Lcom/google/android/street/Street;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {v8, v5}, Lcom/google/android/street/UserOrientation;->setZoom(F)V

    :goto_3
    const/4 v5, 0x0

    move v0, v5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/street/Street;->mMapZoom:I

    const-string v5, "mz"

    move-object/from16 v0, v19

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    if-eqz v28, :cond_8

    const/4 v5, 0x1

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v5, v9}, Ljava/lang/Math;->max(II)I

    move-result v5

    move v0, v5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/street/Street;->mMapZoom:I

    :cond_8
    const-string v5, "faceto_ll"

    move-object/from16 v0, v19

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_10

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_10

    const/16 v5, 0x5f

    const/16 v9, 0x2c

    move-object/from16 v0, v20

    move v1, v5

    move v2, v9

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/street/MapPoint;->parse(Ljava/lang/String;)Lcom/google/android/street/MapPoint;
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v9

    :goto_4
    :try_start_5
    const-string v5, "title"

    move-object/from16 v0, v19

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    if-eqz v37, :cond_9

    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_9

    const/16 v5, 0x5f

    const/16 v11, 0x2c

    move-object/from16 v0, v37

    move v1, v5

    move v2, v11

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v33

    :cond_9
    const-string v5, "dir_arrow"

    move-object/from16 v0, v19

    move-object v1, v5

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_a

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_a

    const-string v5, "_"

    move-object/from16 v0, v17

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    array-length v5, v14

    const/4 v11, 0x2

    if-ne v5, v11, :cond_a

    const/16 v22, 0x1

    const/4 v5, 0x0

    aget-object v5, v14, v5

    invoke-static {v5}, Lcom/google/android/street/Street;->parseFloat(Ljava/lang/String;)F

    move-result v23

    const/4 v5, 0x1

    aget-object v5, v14, v5

    invoke-static {v5}, Lcom/google/android/street/Street;->parseFloat(Ljava/lang/String;)F
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_5

    move-result v30

    :cond_a
    :goto_5
    if-nez v24, :cond_b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Couldn\'t parse "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object v0, v5

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    :cond_b
    :goto_6
    if-eqz v24, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v12}, Lcom/google/android/street/StreetView;->toggleCompassMode(ZZ)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/street/StreetView;->loadPanorama(Ljava/lang/String;Lcom/google/android/street/MapPoint;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/MapPoint;Lcom/google/android/street/PanoramaConfig;)V

    if-eqz v33, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    move-object v0, v5

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/google/android/street/StreetView;->setStatusOverride(Ljava/lang/String;)V

    :goto_7
    if-eqz v22, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    move-object v0, v5

    move/from16 v1, v23

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/StreetView;->setDirectionsArrowParams(FF)V

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/google/android/street/StreetView;->invalidate()V

    goto/16 :goto_0

    :catch_0
    move-exception v5

    move-object/from16 v10, v16

    goto/16 :goto_1

    :cond_c
    const/4 v5, 0x1

    :try_start_6
    aget-object v5, v14, v5

    invoke-static {v5}, Lcom/google/android/street/Street;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {v8, v5}, Lcom/google/android/street/UserOrientation;->setYaw(F)V

    const/4 v5, 0x3

    aget-object v5, v14, v5

    invoke-static {v5}, Lcom/google/android/street/Street;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {v8, v5}, Lcom/google/android/street/UserOrientation;->setZoom(F)V

    const/4 v5, 0x4

    aget-object v5, v14, v5

    invoke-static {v5}, Lcom/google/android/street/Street;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v9, -0x3d4c0000

    const/high16 v11, 0x42b40000

    invoke-static {v5, v9, v11}, Lcom/google/android/street/StreetMath;->clamp(FFF)F

    move-result v34

    const/high16 v5, 0x42b40000

    sub-float v5, v5, v34

    const/high16 v9, 0x43340000

    div-float v38, v5, v9

    move-object v0, v8

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/google/android/street/UserOrientation;->setTilt(F)V
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_3

    :catch_1
    move-exception v5

    move-object/from16 v9, v21

    goto/16 :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/google/android/street/StreetView;->clearStatusOverride()V

    goto :goto_7

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/google/android/street/StreetView;->clearDirectionsArrowParams()V

    goto :goto_8

    :cond_f
    const-string v5, "Got a bad Intent. Exiting."

    invoke-static {v5}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/street/Street;->finish()V

    goto :goto_8

    :catch_2
    move-exception v5

    move-object/from16 v9, v21

    move-object/from16 v8, v29

    move-object/from16 v6, v32

    move-object/from16 v7, v26

    goto/16 :goto_5

    :catch_3
    move-exception v5

    move-object/from16 v9, v21

    move-object/from16 v8, v29

    move-object/from16 v6, v32

    goto/16 :goto_5

    :catch_4
    move-exception v5

    move-object/from16 v9, v21

    move-object/from16 v8, v29

    goto/16 :goto_5

    :catch_5
    move-exception v5

    goto/16 :goto_5

    :cond_10
    move-object/from16 v9, v21

    goto/16 :goto_4

    :cond_11
    move-object/from16 v8, v29

    goto/16 :goto_3

    :cond_12
    move-object/from16 v7, v26

    goto/16 :goto_2

    :cond_13
    move-object/from16 v9, v21

    move-object/from16 v8, v29

    move-object/from16 v6, v32

    move-object/from16 v7, v26

    goto/16 :goto_6

    :cond_14
    move-object/from16 v10, v16

    goto/16 :goto_1
.end method

.method private reportInappropriateImage()V
    .locals 4

    iget-object v3, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v3}, Lcom/google/android/street/StreetView;->getPanoramaConfig()Lcom/google/android/street/PanoramaConfig;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/street/Street;->getOrientation(Z)Ljava/lang/String;

    move-result-object v0

    iget-object v3, v1, Lcom/google/android/street/PanoramaConfig;->mPanoId:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/street/StreetUrl;->getReportInappropriateImageUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/street/Street;->showUrl(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showUrl(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/street/Street;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not start activty"

    invoke-static {v1, v0}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public getApproximateLocation()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/Street;->mLocation:Landroid/location/Location;

    return-object v0
.end method

.method public getDefaultDisplay()Landroid/view/Display;
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/Street;->mDefaultDisplay:Landroid/view/Display;

    if-nez v0, :cond_0

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/street/Street;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/street/Street;->mDefaultDisplay:Landroid/view/Display;

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/Street;->mDefaultDisplay:Landroid/view/Display;

    return-object v0
.end method

.method public getSensorManager()Landroid/hardware/SensorManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/Street;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/google/android/street/Street;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/street/Street;->mSensorManager:Landroid/hardware/SensorManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/Street;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x7

    const/4 v8, 0x0

    const/4 v10, 0x1

    const-string v7, "Warm start onCreate()"

    invoke-static {v7}, Lcom/google/android/street/Street;->noteStartFrame(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget-boolean v7, Lcom/google/android/street/Street;->mGlobalStateInitialized:Z

    if-nez v7, :cond_0

    sput-boolean v10, Lcom/google/android/street/Street;->mGlobalStateInitialized:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/street/Street;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v7, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v4, v7, 0xf

    const/4 v7, 0x4

    if-ne v4, v7, :cond_6

    move v2, v10

    :goto_0
    invoke-static {}, Lcom/google/mobile/googlenav/android/AndroidBuilds;->isHoneycombSdk()Z

    move-result v7

    if-eqz v7, :cond_7

    if-eqz v2, :cond_7

    move v7, v10

    :goto_1
    iput-boolean v7, p0, Lcom/google/android/street/Street;->mIsTablet:Z

    invoke-static {}, Lcom/google/mobile/googlenav/android/AndroidBuilds;->isHoneycombSdk()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/street/Street;->mActionBarInUse:Z

    iget-boolean v7, p0, Lcom/google/android/street/Street;->mActionBarInUse:Z

    if-eqz v7, :cond_8

    const/high16 v7, 0x7f060000

    invoke-virtual {p0, v7}, Lcom/google/android/street/Street;->setTheme(I)V

    :goto_2
    const v7, 0x7f030002

    invoke-virtual {p0, v7}, Lcom/google/android/street/Street;->setContentView(I)V

    iget-boolean v7, p0, Lcom/google/android/street/Street;->mActionBarInUse:Z

    if-nez v7, :cond_1

    invoke-virtual {p0}, Lcom/google/android/street/Street;->getWindow()Landroid/view/Window;

    move-result-object v7

    const/high16 v8, 0x7f030000

    invoke-virtual {v7, v9, v8}, Landroid/view/Window;->setFeatureInt(II)V

    const v7, 0x7f080001

    invoke-virtual {p0, v7}, Lcom/google/android/street/Street;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/google/android/street/Street;->mTitleText:Landroid/widget/TextView;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/street/Street;->getCacheDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    new-instance v7, Lcom/google/android/street/HttpCache;

    const/4 v8, 0x3

    const/16 v9, 0x64

    invoke-direct {v7, v8, v0, v9}, Lcom/google/android/street/HttpCache;-><init>(ILjava/lang/String;I)V

    iput-object v7, p0, Lcom/google/android/street/Street;->mHttpCache:Lcom/google/android/street/HttpCache;

    new-instance v7, Lcom/google/android/street/PanoramaManager;

    iget-object v8, p0, Lcom/google/android/street/Street;->mHttpCache:Lcom/google/android/street/HttpCache;

    invoke-direct {v7, v8}, Lcom/google/android/street/PanoramaManager;-><init>(Lcom/google/android/street/HttpCache;)V

    iput-object v7, p0, Lcom/google/android/street/Street;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    iput-object v7, p0, Lcom/google/android/street/Street;->mNetworkStateChangedFilter:Landroid/content/IntentFilter;

    iget-object v7, p0, Lcom/google/android/street/Street;->mNetworkStateChangedFilter:Landroid/content/IntentFilter;

    const-string v8, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v7, Lcom/google/android/street/Street$1;

    invoke-direct {v7, p0}, Lcom/google/android/street/Street$1;-><init>(Lcom/google/android/street/Street;)V

    iput-object v7, p0, Lcom/google/android/street/Street;->mNetworkStateIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0}, Lcom/google/android/street/Street;->checkNetworkAvailability()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/street/Street;->mNetworkAvailable:Z

    invoke-static {}, Lcom/google/mobile/googlenav/android/AndroidBuilds;->isGingerbreadSdk()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/google/android/street/Street;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "android.hardware.sensor.compass"

    invoke-virtual {v7, v8}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/street/Street;->mHasCompass:Z

    :cond_2
    const/4 v7, -0x1

    iput v7, p0, Lcom/google/android/street/Street;->mDrdNetworkError:I

    invoke-static {p0}, Lcom/google/android/street/Street;->canReadUserLocation(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    const-string v7, "location"

    invoke-virtual {p0, v7}, Lcom/google/android/street/Street;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/LocationManager;

    invoke-virtual {v3}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v7

    const-string v8, "network"

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :try_start_0
    const-string v7, "network"

    invoke-virtual {v3, v7}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/street/Street;->mLocation:Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    const-string v7, "StreetView"

    new-instance v8, Lcom/google/android/street/Street$DrdListener;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/google/android/street/Street$DrdListener;-><init>(Lcom/google/android/street/Street;Lcom/google/android/street/Street$1;)V

    invoke-static {p0, v7, v8}, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->setupConfigAndDrd(Landroid/content/Context;Ljava/lang/String;Lcom/google/mobile/googlenav/datarequest/DataRequestListener;)V

    const v7, 0x7f080006

    invoke-virtual {p0, v7}, Lcom/google/android/street/Street;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/street/StreetView;

    iput-object v7, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    iget-object v7, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    iget-object v8, p0, Lcom/google/android/street/Street;->timer:Lcom/google/android/street/Timer;

    invoke-virtual {v7, v8}, Lcom/google/android/street/StreetView;->setTimer(Lcom/google/android/street/Timer;)V

    iget-object v7, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    iget-object v8, p0, Lcom/google/android/street/Street;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-boolean v9, p0, Lcom/google/android/street/Street;->mIsTablet:Z

    invoke-virtual {v7, p0, v8, v9}, Lcom/google/android/street/StreetView;->initialize(Lcom/google/android/street/Street;Lcom/google/android/street/PanoramaManager;Z)V

    iget-object v7, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v7}, Lcom/google/android/street/StreetView;->requestFocus()Z

    invoke-static {p0}, Lcom/google/android/street/WhatsNewDialog;->shouldShow(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p0, v10}, Lcom/google/android/street/Street;->showDialog(I)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/street/Street;->getIntent()Landroid/content/Intent;

    move-result-object v7

    if-nez v7, :cond_5

    invoke-direct {p0, p1}, Lcom/google/android/street/Street;->processIntent(Landroid/os/Bundle;)V

    :cond_5
    return-void

    :cond_6
    move v2, v8

    goto/16 :goto_0

    :cond_7
    move v7, v8

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0, v9}, Lcom/google/android/street/Street;->requestWindowFeature(I)Z

    goto/16 :goto_2

    :catch_0
    move-exception v7

    goto :goto_3
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/street/WhatsNewDialog;

    invoke-direct {v0, p0}, Lcom/google/android/street/WhatsNewDialog;-><init>(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/street/Street;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-boolean v0, p0, Lcom/google/android/street/Street;->mHasCompass:Z

    if-nez v0, :cond_0

    const v0, 0x7f080010

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-static {}, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->cleanupConfigAndDrd()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v3}, Lcom/google/android/street/StreetView;->getPanoramaConfig()Lcom/google/android/street/PanoramaConfig;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v3, 0x1

    move v1, v3

    :goto_0
    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_1
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v3}, Lcom/google/android/street/StreetView;->invalidate()V

    :goto_2
    return v0

    :cond_1
    const/4 v3, 0x0

    move v1, v3

    goto :goto_0

    :pswitch_0
    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/street/Street;->gotoMap()V

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x29
        :pswitch_0
    .end packed-switch
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/google/android/street/Street;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->getPanoramaConfig()Lcom/google/android/street/PanoramaConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v3

    :goto_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    if-eqz v0, :cond_3

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/street/Street;->mActionBarInUse:Z

    if-eqz v0, :cond_1

    invoke-static {v1}, Lcom/google/android/street/ActionBarUtil;->isHomeId(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/street/Street;->finish()V

    move v0, v3

    :goto_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v1}, Lcom/google/android/street/StreetView;->invalidate()V

    :goto_2
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/street/Street;->gotoMap()V

    move v0, v3

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/street/Street;->reportInappropriateImage()V

    move v0, v3

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->toggleCompassMode()V

    move v0, v3

    goto :goto_1

    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://m.google.com/tospage?hl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/mobile/googlenav/common/Config;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/street/Street;->showUrl(Ljava/lang/String;)V

    move v0, v3

    goto :goto_1

    :pswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://m.google.com/privacy?hl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/mobile/googlenav/common/Config;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/street/Street;->showUrl(Ljava/lang/String;)V

    move v0, v3

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f08000e
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->onPause()V

    iget-object v0, p0, Lcom/google/android/street/Street;->mNetworkStateIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/street/Street;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/Street;->mRegisteredForNetworkConnectivity:Z

    return-void
.end method

.method protected onRestart()V
    .locals 1

    const-string v0, "Warm start onRestart()"

    invoke-static {v0}, Lcom/google/android/street/Street;->noteStartFrame(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iput-object p1, p0, Lcom/google/android/street/Street;->mSavedInstanceState:Landroid/os/Bundle;

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "Warm start onResume()"

    invoke-static {v0}, Lcom/google/android/street/Street;->noteStartFrame(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->onResume()V

    iget-object v0, p0, Lcom/google/android/street/Street;->mNetworkStateIntentReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/street/Street;->mNetworkStateChangedFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/street/Street;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Street;->mRegisteredForNetworkConnectivity:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/street/Street;->processIntent(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;

    invoke-virtual {v2}, Lcom/google/android/street/StreetView;->getPanoramaConfig()Lcom/google/android/street/PanoramaConfig;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/street/Street;->getStateString(Z)Ljava/lang/String;

    move-result-object v0

    const-string v2, "streetview"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "panoramaConfig"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    const-string v0, "Warm start onStart()"

    invoke-static {v0}, Lcom/google/android/street/Street;->noteStartFrame(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public reportProgress(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public reportTransitionProgress(I)V
    .locals 2
    .param p1    # I

    const/16 v1, 0x4e20

    if-le p1, v1, :cond_0

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    const v1, 0x7f080007

    invoke-virtual {p0, v1}, Lcom/google/android/street/Street;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v1, 0x8

    move v0, v1

    goto :goto_0
.end method

.method public setStatusText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-boolean v0, p0, Lcom/google/android/street/Street;->mActionBarInUse:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/street/ActionBarUtil;->setActionBarTitle(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/Street;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public wasNetworkAvailable()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/street/Street;->mNetworkAvailable:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/street/Street;->mDrdNetworkError:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
