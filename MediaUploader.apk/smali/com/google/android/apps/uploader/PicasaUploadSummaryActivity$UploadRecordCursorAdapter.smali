.class Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;
.super Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;
.source "PicasaUploadSummaryActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadRecordCursorAdapter"
.end annotation


# instance fields
.field private mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    const v0, 0x7f030004

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;I)V

    return-void
.end method

.method private getAsyncTask(Landroid/content/Context;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;-><init>(Landroid/content/Context;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    return-object v0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-static {p3}, Lcom/google/android/picasasync/UploadedEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/picasasync/UploadedEntry;

    move-result-object v10

    iget-object v0, v10, Lcom/google/android/picasasync/UploadedEntry;->contentUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, v10, Lcom/google/android/picasasync/UploadedEntry;->account:Ljava/lang/String;

    iget-object v4, v10, Lcom/google/android/picasasync/UploadedEntry;->displayName:Ljava/lang/String;

    iget-object v5, v10, Lcom/google/android/picasasync/UploadedEntry;->albumTitle:Ljava/lang/String;

    iget-wide v6, v10, Lcom/google/android/picasasync/UploadedEntry;->bytesTotal:J

    iget v8, v10, Lcom/google/android/picasasync/UploadedEntry;->state:I

    iget-object v9, v10, Lcom/google/android/picasasync/UploadedEntry;->thumbnail:[B

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->bindViewCommon(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI[B)V

    const v0, 0x7f04001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-wide v3, v10, Lcom/google/android/picasasync/UploadedEntry;->uploadedTime:J

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getDate(J)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$1000(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onAccountListReady([Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onAddAccountResult(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onAlbumCreated(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;J)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Ljava/lang/String;
    .param p4    # J

    return-void
.end method

.method public onAlbumListReady(Ljava/lang/String;Landroid/database/Cursor;ZJ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Z
    .param p4    # J

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/picasasync/UploadedEntry;

    new-instance v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    invoke-direct {v2}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;-><init>()V

    iget-object v0, v6, Lcom/google/android/picasasync/UploadedEntry;->albumId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->id:J

    iget-object v0, v6, Lcom/google/android/picasasync/UploadedEntry;->albumTitle:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->title:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/android/picasasync/UploadedEntry;->contentUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {p0, v7}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->getAsyncTask(Landroid/content/Context;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    move-result-object v0

    iget-object v1, v6, Lcom/google/android/picasasync/UploadedEntry;->account:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/picasasync/UploadedEntry;->caption:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    new-array v5, v11, [Landroid/net/Uri;

    aput-object v8, v5, v10

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v5, Landroid/content/ComponentName;

    const-class v9, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;

    invoke-direct {v5, v7, v9}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->submitUploads(Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ComponentName;)V

    iget-object v0, v6, Lcom/google/android/picasasync/UploadedEntry;->displayName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getAlternativeDisplayName(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$800(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    const v2, 0x7f05002d

    new-array v3, v11, [Ljava/lang/Object;

    aput-object v0, v3, v10

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public onError(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onUploadRequestsSubmitted(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    return-void
.end method
