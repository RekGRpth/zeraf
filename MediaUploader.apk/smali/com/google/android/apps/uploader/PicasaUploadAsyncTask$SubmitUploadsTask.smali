.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;
.super Landroid/os/AsyncTask;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubmitUploadsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

.field private final mCaption:Ljava/lang/String;

.field private final mComponentName:Landroid/content/ComponentName;

.field private final mContentUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ComponentName;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;
    .param p4    # Ljava/lang/String;
    .param p6    # Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/content/ComponentName;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAccount:Ljava/lang/String;

    invoke-static {p3}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iput-object p4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mCaption:Ljava/lang/String;

    invoke-static {p5}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/ArrayList;

    iput-object p6, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mComponentName:Landroid/content/ComponentName;

    return-void
.end method

.method private startUploadService(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->startUploadServiceApi16(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->startUploadServiceApi15OrOlder(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private startUploadServiceApi15OrOlder(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/Context;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/google/android/picasasync/PicasaUploadService;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "com.google.android.uploader.NEW_REQUESTS"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v5, "com.google.android.uploader.extra.REQUESTS"

    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private startUploadServiceApi16(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v1, v0}, Landroid/content/ClipData;->newRawUri(Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    new-instance v5, Landroid/content/ClipData$Item;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {v5, v0}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v3, v5}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/Context;

    move-result-object v1

    const-class v4, Lcom/google/android/picasasync/PicasaUploadService;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.uploader.NEW_REQUESTS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.uploader.extra.REQUESTS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;
    .locals 21
    .param p1    # [Ljava/lang/Void;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAccount:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iget-wide v2, v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->id:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iget-object v2, v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->title:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mCaption:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mComponentName:Landroid/content/ComponentName;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "album_id_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mPreferences:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$500(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iget-wide v3, v3, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->id:J

    invoke-interface {v2, v15, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "last_account"

    invoke-interface {v2, v3, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 v14, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v14, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/ArrayList;

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/net/Uri;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getDisplayName(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1600(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_0

    const-string v2, "video/"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v7, v7, 0x1

    :cond_0
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v8, v0, v1, v9, v12}, Lcom/google/android/picasasync/PicasaUploadHelper;->newUploadRequestBuilder(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->setCaption(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->setAlbumTitle(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;

    move-result-object v2

    if-nez v13, :cond_1

    const-string v13, ""

    :cond_1
    invoke-virtual {v2, v13}, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->setDisplayName(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->getRequest()Landroid/content/ContentValues;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->startUploadService(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    new-instance v2, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAccount:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mAlbum:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->mContentUris:Ljava/util/ArrayList;

    sub-int v6, v16, v7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;-><init>(Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/util/ArrayList;II)V

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onUploadRequestsSubmitted(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$SubmitUploadsTask;->onPostExecute(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V

    return-void
.end method
