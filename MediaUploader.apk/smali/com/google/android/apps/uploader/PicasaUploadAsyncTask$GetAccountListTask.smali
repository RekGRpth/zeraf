.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;
.super Landroid/os/AsyncTask;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetAccountListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mErrorCode:I

.field private mLastAccount:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
    .param p2    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->doInBackground([Ljava/lang/Void;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[Ljava/lang/String;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$400(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.google"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v3, v1

    new-array v0, v3, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    aget-object v3, v1, v2

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mPreferences:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$500(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "last_account"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->mLastAccount:Ljava/lang/String;

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->onPostExecute([Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute([Ljava/lang/String;)V
    .locals 3
    .param p1    # [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetAccountListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$600(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;

    move-result-object v0

    if-eq p0, v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->mErrorCode:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->mLastAccount:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onAccountListReady([Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetAccountListTask;->mErrorCode:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onError(ILjava/lang/String;)V

    goto :goto_0
.end method
