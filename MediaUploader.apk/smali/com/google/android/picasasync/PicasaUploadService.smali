.class public final Lcom/google/android/picasasync/PicasaUploadService;
.super Landroid/app/Service;
.source "PicasaUploadService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
    }
.end annotation


# static fields
.field private static final PROJECTION_COUNT:[Ljava/lang/String;


# instance fields
.field private mForeground:Z

.field private mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

.field private volatile mNewBatch:Z

.field private mShowNotification:Z

.field private volatile mStalled:Z

.field private mThreadRunning:Z

.field private mUploadAccountRequested:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "COUNT(*)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/picasasync/PicasaUploadService;->PROJECTION_COUNT:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/picasasync/PicasaUploadService;)Z
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;

    iget-boolean v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mForeground:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/picasasync/PicasaUploadService;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/picasasync/PicasaUploadService;->mForeground:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/picasasync/PicasaUploadService;)Z
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;

    iget-boolean v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mShowNotification:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/picasasync/PicasaUploadService;)Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/picasasync/PicasaUploadService;)Z
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;

    iget-boolean v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mThreadRunning:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/picasasync/PicasaUploadService;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/picasasync/PicasaUploadService;->mThreadRunning:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/picasasync/PicasaUploadService;)Z
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;

    iget-boolean v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mStalled:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/picasasync/PicasaUploadService;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/picasasync/PicasaUploadService;->mStalled:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/picasasync/PicasaUploadService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mUploadAccountRequested:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/picasasync/PicasaUploadService;->mUploadAccountRequested:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaUploadService;->startUploadThreadInternal(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/picasasync/PicasaUploadService;)Z
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;

    iget-boolean v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mNewBatch:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/picasasync/PicasaUploadService;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/picasasync/PicasaUploadService;->mNewBatch:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/picasasync/PicasaUploadService;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaUploadService;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaUploadService;->handleUploadProgressReportInBackground(Landroid/content/Intent;)V

    return-void
.end method

.method private static getPauseMessage(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f050016

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f050015

    goto :goto_0

    :pswitch_2
    const v0, 0x7f050014

    goto :goto_0

    :pswitch_3
    const v0, 0x7f050013

    goto :goto_0

    :pswitch_4
    const v0, 0x7f050011

    goto :goto_0

    :pswitch_5
    const v0, 0x7f050012

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static getProgressNotificationApi10(Landroid/content/Context;IILjava/lang/String;I)Landroid/app/Notification;
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const v10, 0x7f040017

    const v9, 0x7f040016

    const v8, 0x1080088

    const/4 v7, 0x1

    const/4 v6, 0x0

    if-nez p2, :cond_0

    const v3, 0x7f05000a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/app/Notification;

    const v3, 0x1080089

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v3, v1, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, p0, v1, v3, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v0, Landroid/app/Notification;->flags:I

    :goto_0
    return-object v0

    :cond_0
    const v3, 0x7f050009

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030002

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v3, 0x7f040015

    invoke-virtual {v2, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v3, 0x7f040014

    invoke-virtual {v2, v3, v8}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    if-eqz p3, :cond_1

    if-ne p1, v7, :cond_2

    invoke-virtual {v2, v10, p3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const/16 v3, 0x64

    invoke-virtual {v2, v9, v3, p4, v6}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    :cond_1
    :goto_1
    new-instance v0, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v8, v1, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Landroid/app/Notification;->flags:I

    iput-object v2, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/google/android/picasasync/PicasaUploadService;->getPauseMessage(I)I

    move-result v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v10, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const/16 v3, 0x8

    invoke-virtual {v2, v9, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method private static getProgressNotificationApi11OrLater(Landroid/content/Context;IILjava/lang/String;I)Landroid/app/Notification;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x1

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    if-nez p2, :cond_1

    const v1, 0x1080089

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v1, 0x7f05000a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    return-object v1

    :cond_1
    const v1, 0x1080088

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v1, 0x7f050009

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    if-eqz p3, :cond_0

    if-ne p1, v4, :cond_2

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, p4, v5}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/google/android/picasasync/PicasaUploadService;->getPauseMessage(I)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_0
.end method

.method private static getStartupNotificationApi10(Landroid/content/Context;)Landroid/app/Notification;
    .locals 6
    .param p0    # Landroid/content/Context;

    const/4 v5, 0x0

    const v2, 0x7f05000a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/app/Notification;

    const v2, 0x1080088

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v2, v1, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {v0, p0, v1, v5, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method private static getStartupNotificationApi11OrLater(Landroid/content/Context;)Landroid/app/Notification;
    .locals 2
    .param p0    # Landroid/content/Context;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080088

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v1, 0x7f050008

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method

.method private handleUploadProgressReport(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/picasasync/PicasaUploadService$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/picasasync/PicasaUploadService$3;-><init>(Lcom/google/android/picasasync/PicasaUploadService;Landroid/content/Intent;)V

    const-string v2, "upload-progress-report-thread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private handleUploadProgressReportInBackground(Landroid/content/Intent;)V
    .locals 13

    const-string v0, "manual_upload_progress"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v0, "manual_upload_uploader_state"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    const-string v0, "manual_upload_record_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v0, "manual_upload_display_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v6, v0

    :goto_0
    if-eqz v6, :cond_0

    const-string v0, "manual_upload_content_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    const-string v3, "manual_upload_state"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "manual_upload_upload_id"

    const-wide/16 v10, 0x0

    invoke-virtual {p1, v4, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v10, "PicasaUploadService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "got upload result: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, "/"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, "% "

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaUploadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/picasasync/PicasaFacade;->uploadsUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/picasasync/PicasaUploadService;->PROJECTION_COUNT:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v6, :cond_1

    const-string v2, "PicasaUploadService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pending uploads="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_3

    invoke-static {v1, v8, v0, v9, v7}, Lcom/google/android/picasasync/PicasaUploadService;->getProgressNotificationApi11OrLater(Landroid/content/Context;IILjava/lang/String;I)Landroid/app/Notification;

    move-result-object v0

    :goto_1
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v1, v3, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_2
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    :cond_3
    invoke-static {v1, v8, v0, v9, v7}, Lcom/google/android/picasasync/PicasaUploadService;->getProgressNotificationApi10(Landroid/content/Context;IILjava/lang/String;I)Landroid/app/Notification;

    move-result-object v0

    goto :goto_1
.end method

.method private insertUploadRequests(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/picasasync/PicasaUploadService$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/picasasync/PicasaUploadService$2;-><init>(Lcom/google/android/picasasync/PicasaUploadService;Ljava/util/ArrayList;)V

    const-string v2, "media-uploader-upload-process-thread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private startUploadThreadInternal(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mThreadRunning:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/picasasync/PicasaUploadService$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/picasasync/PicasaUploadService$1;-><init>(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)V

    const-string v2, "media-uploader-upload-thread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    new-instance v0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;-><init>(Lcom/google/android/picasasync/PicasaUploadService;Lcom/google/android/picasasync/PicasaUploadService$1;)V

    iput-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v0, "com.google.android.uploader.CANCEL_NOTIFICATION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v6, p0, Lcom/google/android/picasasync/PicasaUploadService;->mShowNotification:Z

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->showOrCancelNotification(Landroid/app/Notification;)V

    :cond_0
    :goto_0
    return v5

    :cond_1
    const-string v0, "com.google.android.uploader.manual_upload_report"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaUploadService;->handleUploadProgressReport(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mForeground:Z

    if-nez v0, :cond_3

    const-string v0, "PicasaUploadService"

    const-string v1, "startForeground"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/google/android/picasasync/PicasaUploadService;->mForeground:Z

    iput-boolean v5, p0, Lcom/google/android/picasasync/PicasaUploadService;->mShowNotification:Z

    const-string v0, "com.google.android.uploader.NEW_REQUESTS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mNewBatch:Z

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-class v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-direct {v4, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_4

    invoke-static {v3}, Lcom/google/android/picasasync/PicasaUploadService;->getStartupNotificationApi11OrLater(Landroid/content/Context;)Landroid/app/Notification;

    move-result-object v0

    move-object v1, v0

    :goto_1
    const/high16 v0, 0x8000000

    invoke-static {v3, v6, v4, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/picasasync/PicasaUploadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-virtual {p0, v5, v1}, Lcom/google/android/picasasync/PicasaUploadService;->startForeground(ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    invoke-virtual {v0, v5}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->removeMessages(I)V

    :cond_3
    const-string v0, "com.google.android.uploader.NEW_REQUESTS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "com.google.android.uploader.extra.REQUESTS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/picasasync/PicasaUploadService;->insertUploadRequests(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_4
    invoke-static {v3}, Lcom/google/android/picasasync/PicasaUploadService;->getStartupNotificationApi10(Landroid/content/Context;)Landroid/app/Notification;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_5
    const-string v0, "com.google.android.uploader.START_UPLOAD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.uploader.extra.ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    invoke-virtual {v1, v0}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->startUploadThread(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
