.class Lcom/google/android/picasasync/PicasaUploadService$2;
.super Ljava/lang/Object;
.source "PicasaUploadService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/picasasync/PicasaUploadService;->insertUploadRequests(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/picasasync/PicasaUploadService;

.field final synthetic val$requests:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/google/android/picasasync/PicasaUploadService;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/picasasync/PicasaUploadService$2;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    iput-object p2, p0, Lcom/google/android/picasasync/PicasaUploadService$2;->val$requests:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v5, p0, Lcom/google/android/picasasync/PicasaUploadService$2;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-static {v5}, Lcom/google/android/picasasync/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/UploadsManager;

    move-result-object v4

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/picasasync/PicasaUploadService$2;->val$requests:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    instance-of v5, v2, Landroid/content/ContentValues;

    if-eqz v5, :cond_0

    check-cast v2, Landroid/content/ContentValues;

    invoke-static {v2}, Lcom/google/android/picasasync/UploadTaskEntry;->createNew(Landroid/content/ContentValues;)Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/google/android/picasasync/UploadsManager;->addManualUpload(Lcom/google/android/picasasync/UploadTaskEntry;)J

    if-nez v0, :cond_0

    move-object v0, v3

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/google/android/picasasync/PicasaUploadService$2;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
    invoke-static {v5}, Lcom/google/android/picasasync/PicasaUploadService;->access$300(Lcom/google/android/picasasync/PicasaUploadService;)Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v0}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    return-void
.end method
