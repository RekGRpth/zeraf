.class Lcom/google/android/picasasync/PicasaUploadService$1;
.super Ljava/lang/Object;
.source "PicasaUploadService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/picasasync/PicasaUploadService;->startUploadThreadInternal(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/picasasync/PicasaUploadService;

.field final synthetic val$account:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    iput-object p2, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->val$account:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    const-wide/16 v13, 0x0

    const/4 v12, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-static {v8}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v1

    new-instance v3, Landroid/content/SyncResult;

    invoke-direct {v3}, Landroid/content/SyncResult;-><init>()V

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->val$account:Ljava/lang/String;

    invoke-virtual {v1, v8, v3}, Lcom/google/android/picasasync/PicasaSyncManager;->createSession(Ljava/lang/String;Landroid/content/SyncResult;)Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mNewBatch:Z
    invoke-static {v8}, Lcom/google/android/picasasync/PicasaUploadService;->access$800(Lcom/google/android/picasasync/PicasaUploadService;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "PicasaUploadService"

    const-string v9, "new upload batch"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mNewBatch:Z
    invoke-static {v8, v6}, Lcom/google/android/picasasync/PicasaUploadService;->access$802(Lcom/google/android/picasasync/PicasaUploadService;Z)Z

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaSyncManager;->resetSyncStates()V

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaSyncManager;->requestAccountSync()V

    :try_start_0
    const-string v8, "PicasaUploadService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": start upload"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v2}, Lcom/google/android/picasasync/PicasaSyncManager;->performSync(Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->isSyncCancelled()Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "PicasaUploadService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": sync cancelled"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v8, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v8, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v8, v8, v13

    if-lez v8, :cond_3

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-static {v9}, Lcom/google/android/picasasync/PicasaFacade;->hasNetworkConnectivity(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {}, Lcom/google/android/picasasync/PicasaFacade;->isExternalStorageMounted()Z

    move-result v9

    if-nez v9, :cond_2

    :cond_1
    move v6, v7

    :cond_2
    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mStalled:Z
    invoke-static {v8, v6}, Lcom/google/android/picasasync/PicasaUploadService;->access$502(Lcom/google/android/picasasync/PicasaUploadService;Z)Z

    :cond_3
    iget-object v6, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
    invoke-static {v6}, Lcom/google/android/picasasync/PicasaUploadService;->access$300(Lcom/google/android/picasasync/PicasaUploadService;)Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    move-result-object v6

    invoke-virtual {v6, v12}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->sendEmptyMessage(I)Z

    :goto_1
    return-void

    :cond_4
    const-string v8, "PicasaUploadService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": sync finished"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v8, "PicasaUploadService"

    const-string v9, "performSync error"

    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->isSyncCancelled()Z

    move-result v8

    if-eqz v8, :cond_8

    const-string v8, "PicasaUploadService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": sync cancelled"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget-object v8, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v8, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v8, v8, v13

    if-lez v8, :cond_7

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-static {v9}, Lcom/google/android/picasasync/PicasaFacade;->hasNetworkConnectivity(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-static {}, Lcom/google/android/picasasync/PicasaFacade;->isExternalStorageMounted()Z

    move-result v9

    if-nez v9, :cond_6

    :cond_5
    move v6, v7

    :cond_6
    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mStalled:Z
    invoke-static {v8, v6}, Lcom/google/android/picasasync/PicasaUploadService;->access$502(Lcom/google/android/picasasync/PicasaUploadService;Z)Z

    :cond_7
    iget-object v6, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
    invoke-static {v6}, Lcom/google/android/picasasync/PicasaUploadService;->access$300(Lcom/google/android/picasasync/PicasaUploadService;)Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    move-result-object v6

    invoke-virtual {v6, v12}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_8
    const-string v8, "PicasaUploadService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": sync finished"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catchall_0
    move-exception v8

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->isSyncCancelled()Z

    move-result v9

    if-eqz v9, :cond_c

    const-string v9, "PicasaUploadService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": sync cancelled"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    iget-object v9, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v9, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v9, v9, v13

    if-lez v9, :cond_b

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    iget-object v10, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-static {v10}, Lcom/google/android/picasasync/PicasaFacade;->hasNetworkConnectivity(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-static {}, Lcom/google/android/picasasync/PicasaFacade;->isExternalStorageMounted()Z

    move-result v10

    if-nez v10, :cond_a

    :cond_9
    move v6, v7

    :cond_a
    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mStalled:Z
    invoke-static {v9, v6}, Lcom/google/android/picasasync/PicasaUploadService;->access$502(Lcom/google/android/picasasync/PicasaUploadService;Z)Z

    :cond_b
    iget-object v6, p0, Lcom/google/android/picasasync/PicasaUploadService$1;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
    invoke-static {v6}, Lcom/google/android/picasasync/PicasaUploadService;->access$300(Lcom/google/android/picasasync/PicasaUploadService;)Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    move-result-object v6

    invoke-virtual {v6, v12}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->sendEmptyMessage(I)Z

    throw v8

    :cond_c
    const-string v9, "PicasaUploadService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": sync finished"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
