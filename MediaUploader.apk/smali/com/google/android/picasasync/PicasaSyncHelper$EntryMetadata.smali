.class final Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;
.super Ljava/lang/Object;
.source "PicasaSyncHelper.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PicasaSyncHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EntryMetadata"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field public dateEdited:J

.field public id:J

.field public survived:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;->survived:Z

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1
    .param p1    # J
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;->survived:Z

    iput-wide p1, p0, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;->id:J

    iput-wide p3, p0, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;->dateEdited:J

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;)I
    .locals 4
    .param p1    # Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;

    iget-wide v0, p0, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;->id:J

    iget-wide v2, p1, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;->id:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/gallery3d/common/Utils;->compare(JJ)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;

    invoke-virtual {p0, p1}, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;->compareTo(Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;)I

    move-result v0

    return v0
.end method

.method public updateId(J)Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/picasasync/PicasaSyncHelper$EntryMetadata;->id:J

    return-object p0
.end method
