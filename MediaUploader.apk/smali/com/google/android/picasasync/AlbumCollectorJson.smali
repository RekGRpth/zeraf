.class Lcom/google/android/picasasync/AlbumCollectorJson;
.super Lcom/google/android/picasasync/PicasaJsonReaderParser;
.source "AlbumCollectorJson.java"


# static fields
.field private static final sAlbumEntryFieldMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0xa

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/google/android/picasasync/AlbumCollectorJson;->sAlbumEntryFieldMap:Ljava/util/Map;

    sget-object v1, Lcom/google/android/picasasync/AlbumEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    sget-object v0, Lcom/google/android/picasasync/AlbumCollectorJson;->sAlbumEntryFieldMap:Ljava/util/Map;

    const-string v2, "gphoto$id"

    const-string v3, "_id"

    invoke-virtual {v1, v3}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/AlbumCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "gphoto$albumType"

    const-string v3, "album_type"

    invoke-virtual {v1, v3}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/AlbumCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "gphoto$user"

    const-string v3, "user"

    invoke-virtual {v1, v3}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/AlbumCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "title"

    const-string v3, "title"

    invoke-virtual {v1, v3}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/AlbumCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "gphoto$numphotos"

    const-string v3, "num_photos"

    invoke-virtual {v1, v3}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/AlbumCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "published"

    new-instance v3, Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;

    const-string v4, "date_published"

    invoke-direct {v3, v4, v5}, Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "updated"

    new-instance v3, Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;

    const-string v4, "date_updated"

    invoke-direct {v3, v4, v5}, Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "app$edited"

    new-instance v3, Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;

    const-string v4, "date_edited"

    invoke-direct {v3, v4, v5}, Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/picasasync/PicasaApi$EntryHandler;)V
    .locals 0
    .param p1    # Lcom/google/android/picasasync/PicasaApi$EntryHandler;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaJsonReaderParser;-><init>(Lcom/google/android/picasasync/PicasaApi$EntryHandler;)V

    return-void
.end method

.method public static parseOneAlbumEntry(Ljava/io/InputStream;Landroid/content/ContentValues;)V
    .locals 6
    .param p0    # Ljava/io/InputStream;
    .param p1    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v3, Lcom/google/android/picasasync/JsonReader;

    new-instance v4, Ljava/io/InputStreamReader;

    const-string v5, "UTF-8"

    invoke-direct {v4, p0, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/google/android/picasasync/JsonReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v3}, Lcom/google/android/picasasync/JsonReader;->beginObject()V

    :goto_0
    invoke-virtual {v3}, Lcom/google/android/picasasync/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/google/android/picasasync/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    const-string v4, "entry"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v2, Lcom/google/android/picasasync/AlbumCollectorJson$1;

    invoke-direct {v2}, Lcom/google/android/picasasync/AlbumCollectorJson$1;-><init>()V

    new-instance v0, Lcom/google/android/picasasync/AlbumCollectorJson;

    invoke-direct {v0, v2}, Lcom/google/android/picasasync/AlbumCollectorJson;-><init>(Lcom/google/android/picasasync/PicasaApi$EntryHandler;)V

    invoke-virtual {v0}, Lcom/google/android/picasasync/AlbumCollectorJson;->getEntryFieldMap()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v0, v3, v4, p1}, Lcom/google/android/picasasync/AlbumCollectorJson;->parseObject(Lcom/google/android/picasasync/JsonReader;Ljava/util/Map;Landroid/content/ContentValues;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {v3}, Lcom/google/android/picasasync/JsonReader;->skipValue()V

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/picasasync/JsonReader;->endObject()V

    goto :goto_1
.end method


# virtual methods
.method protected getEntryFieldMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/picasasync/PicasaJsonReaderParser$ObjectField;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/picasasync/AlbumCollectorJson;->sAlbumEntryFieldMap:Ljava/util/Map;

    return-object v0
.end method
