.class Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
.super Landroid/os/Handler;
.source "PicasaUploadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PicasaUploadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field private mLastNotification:Landroid/app/Notification;

.field final synthetic this$0:Lcom/google/android/picasasync/PicasaUploadService;


# direct methods
.method private constructor <init>(Lcom/google/android/picasasync/PicasaUploadService;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->mLastNotification:Landroid/app/Notification;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/picasasync/PicasaUploadService;Lcom/google/android/picasasync/PicasaUploadService$1;)V
    .locals 0
    .param p1    # Lcom/google/android/picasasync/PicasaUploadService;
    .param p2    # Lcom/google/android/picasasync/PicasaUploadService$1;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;-><init>(Lcom/google/android/picasasync/PicasaUploadService;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mForeground:Z
    invoke-static {v2, v1}, Lcom/google/android/picasasync/PicasaUploadService;->access$102(Lcom/google/android/picasasync/PicasaUploadService;Z)Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    iget-object v3, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mShowNotification:Z
    invoke-static {v3}, Lcom/google/android/picasasync/PicasaUploadService;->access$200(Lcom/google/android/picasasync/PicasaUploadService;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/picasasync/PicasaUploadService;->stopForeground(Z)V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->mLastNotification:Landroid/app/Notification;

    invoke-virtual {p0, v0}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->showOrCancelNotification(Landroid/app/Notification;)V

    const-string v0, "PicasaUploadService"

    const-string v1, "stopForground"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
    invoke-static {v0}, Lcom/google/android/picasasync/PicasaUploadService;->access$300(Lcom/google/android/picasasync/PicasaUploadService;)Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    move-result-object v0

    const/4 v1, 0x4

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_1
    const-string v0, "PicasaUploadService"

    const-string v1, "stop service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-virtual {v0}, Lcom/google/android/picasasync/PicasaUploadService;->stopSelf()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->startUploadThread(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mThreadRunning:Z
    invoke-static {v2}, Lcom/google/android/picasasync/PicasaUploadService;->access$400(Lcom/google/android/picasasync/PicasaUploadService;)Z

    move-result v2

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mThreadRunning:Z
    invoke-static {v2, v1}, Lcom/google/android/picasasync/PicasaUploadService;->access$402(Lcom/google/android/picasasync/PicasaUploadService;Z)Z

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mStalled:Z
    invoke-static {v1}, Lcom/google/android/picasasync/PicasaUploadService;->access$500(Lcom/google/android/picasasync/PicasaUploadService;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mUploadAccountRequested:Ljava/lang/String;
    invoke-static {v0, v3}, Lcom/google/android/picasasync/PicasaUploadService;->access$602(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mUploadAccountRequested:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/picasasync/PicasaUploadService;->access$600(Lcom/google/android/picasasync/PicasaUploadService;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mUploadAccountRequested:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/picasasync/PicasaUploadService;->access$600(Lcom/google/android/picasasync/PicasaUploadService;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/picasasync/PicasaUploadService;->startUploadThreadInternal(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/picasasync/PicasaUploadService;->access$700(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mUploadAccountRequested:Ljava/lang/String;
    invoke-static {v0, v3}, Lcom/google/android/picasasync/PicasaUploadService;->access$602(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mHandler:Lcom/google/android/picasasync/PicasaUploadService$MyHandler;
    invoke-static {v1}, Lcom/google/android/picasasync/PicasaUploadService;->access$300(Lcom/google/android/picasasync/PicasaUploadService;)Lcom/google/android/picasasync/PicasaUploadService$MyHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/app/Notification;

    invoke-virtual {p0, v0}, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->showOrCancelNotification(Landroid/app/Notification;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method showOrCancelNotification(Landroid/app/Notification;)V
    .locals 4
    .param p1    # Landroid/app/Notification;

    const/4 v3, 0x2

    const/4 v2, 0x1

    iput-object p1, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->mLastNotification:Landroid/app/Notification;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mShowNotification:Z
    invoke-static {v0}, Lcom/google/android/picasasync/PicasaUploadService;->access$200(Lcom/google/android/picasasync/PicasaUploadService;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mForeground:Z
    invoke-static {v0}, Lcom/google/android/picasasync/PicasaUploadService;->access$100(Lcom/google/android/picasasync/PicasaUploadService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/picasasync/PicasaUploadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v2, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-virtual {v0, v2}, Lcom/google/android/picasasync/PicasaUploadService;->stopForeground(Z)V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/picasasync/PicasaUploadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v3, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    invoke-virtual {v0, v2}, Lcom/google/android/picasasync/PicasaUploadService;->stopForeground(Z)V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/picasasync/PicasaUploadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method startUploadThread(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mStalled:Z
    invoke-static {v0, v1}, Lcom/google/android/picasasync/PicasaUploadService;->access$502(Lcom/google/android/picasasync/PicasaUploadService;Z)Z

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mUploadAccountRequested:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/picasasync/PicasaUploadService;->access$600(Lcom/google/android/picasasync/PicasaUploadService;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # getter for: Lcom/google/android/picasasync/PicasaUploadService;->mThreadRunning:Z
    invoke-static {v0}, Lcom/google/android/picasasync/PicasaUploadService;->access$400(Lcom/google/android/picasasync/PicasaUploadService;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # invokes: Lcom/google/android/picasasync/PicasaUploadService;->startUploadThreadInternal(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/picasasync/PicasaUploadService;->access$700(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadService$MyHandler;->this$0:Lcom/google/android/picasasync/PicasaUploadService;

    # setter for: Lcom/google/android/picasasync/PicasaUploadService;->mUploadAccountRequested:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/picasasync/PicasaUploadService;->access$602(Lcom/google/android/picasasync/PicasaUploadService;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
