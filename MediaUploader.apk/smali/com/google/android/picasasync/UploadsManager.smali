.class Lcom/google/android/picasasync/UploadsManager;
.super Ljava/lang/Object;
.source "UploadsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/UploadsManager$ManualUploadTaskProvider;,
        Lcom/google/android/picasasync/UploadsManager$UploadTask;
    }
.end annotation


# static fields
.field private static EXIF_TAG_APERTURE:Ljava/lang/reflect/Field;

.field private static EXIF_TAG_EXPOSURE_TIME:Ljava/lang/reflect/Field;

.field private static EXIF_TAG_ISO:Ljava/lang/reflect/Field;

.field private static final EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

.field private static final PHOTO_TABLE_NAME:Ljava/lang/String;

.field private static final PROJECTION_COUNT:[Ljava/lang/String;

.field private static final PROJECTION_DATA:[Ljava/lang/String;

.field private static final PROJECTION_ENABLE_ACCOUNT_WIFI:[Ljava/lang/String;

.field private static final PROJECTION_FINGERPRINT:[Ljava/lang/String;

.field private static final UPLOAD_RECORD_TABLE_NAME:Ljava/lang/String;

.field private static final UPLOAD_TASK_TABLE_NAME:Ljava/lang/String;

.field private static sInstance:Lcom/google/android/picasasync/UploadsManager;


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mContext:Landroid/content/Context;

.field private mCurrent:Lcom/google/android/picasasync/UploadsManager$UploadTask;

.field private mExternalStorageFsId:I

.field private final mHandler:Landroid/os/Handler;

.field private volatile mIsExternalStorageFsIdReady:Z

.field private final mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

.field private mProblematicAccounts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReloadSystemSettingDelay:J

.field private mResetDelay:J

.field private final mSettings:Landroid/content/SharedPreferences;

.field private final mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

.field private mSyncOnBattery:Z

.field private mSyncOnRoaming:Z

.field private mSyncedAccountAlbumPairs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUploadUrl:Ljava/lang/String;

.field private final mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

.field private mWifiOnlyPhoto:Z

.field private mWifiOnlyVideo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/picasasync/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->UPLOAD_TASK_TABLE_NAME:Ljava/lang/String;

    sget-object v0, Lcom/google/android/picasasync/UploadedEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->UPLOAD_RECORD_TABLE_NAME:Ljava/lang/String;

    sget-object v0, Lcom/google/android/picasasync/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->PHOTO_TABLE_NAME:Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "fingerprint"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->PROJECTION_FINGERPRINT:[Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sync_on_wifi_only"

    aput-object v1, v0, v3

    const-string v1, "video_upload_wifi_only"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "sync_on_roaming"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "sync_on_battery"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->PROJECTION_ENABLE_ACCOUNT_WIFI:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->PROJECTION_DATA:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "COUNT(*)"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->PROJECTION_COUNT:[Ljava/lang/String;

    const-string v0, "content://media/external/fs_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    sput-object v5, Lcom/google/android/picasasync/UploadsManager;->EXIF_TAG_APERTURE:Ljava/lang/reflect/Field;

    sput-object v5, Lcom/google/android/picasasync/UploadsManager;->EXIF_TAG_EXPOSURE_TIME:Ljava/lang/reflect/Field;

    sput-object v5, Lcom/google/android/picasasync/UploadsManager;->EXIF_TAG_ISO:Ljava/lang/reflect/Field;

    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const-class v0, Landroid/media/ExifInterface;

    const-string v1, "TAG_APERTURE"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->EXIF_TAG_APERTURE:Ljava/lang/reflect/Field;

    const-class v0, Landroid/media/ExifInterface;

    const-string v1, "TAG_EXPOSURE_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->EXIF_TAG_EXPOSURE_TIME:Ljava/lang/reflect/Field;

    const-class v0, Landroid/media/ExifInterface;

    const-string v1, "TAG_ISO"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->EXIF_TAG_ISO:Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "PicasaUploader"

    const-string v2, "get exif fields"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mProblematicAccounts:Ljava/util/HashSet;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncedAccountAlbumPairs:Ljava/util/HashSet;

    iput-boolean v5, p0, Lcom/google/android/picasasync/UploadsManager;->mIsExternalStorageFsIdReady:Z

    const-wide/16 v1, 0x3a98

    iput-wide v1, p0, Lcom/google/android/picasasync/UploadsManager;->mResetDelay:J

    const-wide/16 v1, 0x1f4

    iput-wide v1, p0, Lcom/google/android/picasasync/UploadsManager;->mReloadSystemSettingDelay:J

    iput-object p1, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mAccountManager:Landroid/accounts/AccountManager;

    new-instance v1, Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-direct {v1, p1}, Lcom/google/android/picasasync/UploadsDatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-static {p1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mSettings:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/android/picasasync/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "picasa-uploads-manager"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-direct {p0, v0}, Lcom/google/android/picasasync/UploadsManager;->initHandler(Landroid/os/HandlerThread;)Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->loadSavedStates()V

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->isAndroidUpgraded()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->reset()V

    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/UploadsManager;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    new-instance v3, Lcom/google/android/picasasync/UploadsManager$1;

    iget-object v4, p0, Lcom/google/android/picasasync/UploadsManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/picasasync/UploadsManager$1;-><init>(Lcom/google/android/picasasync/UploadsManager;Landroid/os/Handler;)V

    invoke-virtual {v1, v2, v5, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->reloadSystemSettingsInternal()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/picasasync/UploadsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->onFsIdChangedInternal()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/picasasync/UploadsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->reloadSystemSettingsInternal()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;I)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/Uploader$UploadProgressListener;Landroid/content/SyncResult;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # Lcom/google/android/picasasync/Uploader$UploadProgressListener;
    .param p3    # Landroid/content/SyncResult;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/picasasync/UploadsManager;->doUpload(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/Uploader$UploadProgressListener;Landroid/content/SyncResult;)Lcom/google/android/picasasync/UploadedEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;Landroid/content/SyncResult;)Z
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # Lcom/google/android/picasasync/UploadedEntry;
    .param p3    # Landroid/content/SyncResult;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/picasasync/UploadsManager;->writeToPhotoTable(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;Landroid/content/SyncResult;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/UploadsManager;->updateTaskStateAndProgressInDb(Lcom/google/android/picasasync/UploadTaskEntry;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/picasasync/UploadsManager;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mProblematicAccounts:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/picasasync/UploadsManager;Ljava/lang/String;)Lcom/google/android/picasasync/UploadTaskEntry;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/UploadsManager;->getNextManualUploadFromDb(Ljava/lang/String;)Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/picasasync/UploadsManager;Z)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/UploadsManager;->notifyManualUploadDbChanges(Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # Lcom/google/android/picasasync/UploadedEntry;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/picasasync/UploadsManager;->sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/picasasync/UploadsManager;)Lcom/google/android/picasasync/PicasaDatabaseHelper;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$1900()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/picasasync/UploadsManager;->PHOTO_TABLE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/picasasync/UploadsManager;J)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/picasasync/UploadsManager;->cancelTaskInternal(J)V

    return-void
.end method

.method static synthetic access$2000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/picasasync/UploadsManager;->PROJECTION_FINGERPRINT:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/picasasync/UploadsManager;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncedAccountAlbumPairs:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/picasasync/UploadsManager;)Z
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mIsExternalStorageFsIdReady:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/picasasync/UploadsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->reset()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/picasasync/UploadsManager;)Lcom/google/android/picasasync/PicasaSyncHelper;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadsManager$UploadTask;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Lcom/google/android/picasasync/UploadsManager$UploadTask;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/UploadsManager;->setCurrentUploadTask(Lcom/google/android/picasasync/UploadsManager$UploadTask;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/picasasync/UploadsManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # I
    .param p3    # Ljava/lang/Throwable;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/picasasync/UploadsManager;J)Z
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/picasasync/UploadsManager;->removeTaskFromDb(J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/picasasync/UploadsManager;Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/UploadsManager;
    .param p1    # Lcom/google/android/picasasync/UploadedEntry;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/UploadsManager;->recordResult(Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;

    move-result-object v0

    return-object v0
.end method

.method private accountExists(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/picasasync/UploadsManager;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v5, "com.google"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private declared-synchronized cancelTaskInternal(J)V
    .locals 2
    .param p1    # J

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mCurrent:Lcom/google/android/picasasync/UploadsManager$UploadTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mCurrent:Lcom/google/android/picasasync/UploadsManager$UploadTask;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/picasasync/UploadsManager$UploadTask;->cancelTask(J)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/picasasync/UploadsManager;->getTaskFromDb(J)Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/picasasync/UploadsManager;->removeTaskFromDb(J)Z

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V

    new-instance v1, Lcom/google/android/picasasync/UploadedEntry;

    invoke-direct {v1, v0}, Lcom/google/android/picasasync/UploadedEntry;-><init>(Lcom/google/android/picasasync/UploadTaskEntry;)V

    invoke-direct {p0, v1}, Lcom/google/android/picasasync/UploadsManager;->recordResult(Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/picasasync/UploadsManager;->notifyManualUploadDbChanges(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private doUpload(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/Uploader$UploadProgressListener;Landroid/content/SyncResult;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 12

    const/4 v11, 0x5

    const-wide/16 v9, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->isReadyForUpload()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, 0x3e8

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadUrl:Ljava/lang/String;

    :goto_0
    new-instance v4, Lcom/google/android/picasasync/GDataUploader;

    iget-object v5, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/google/android/picasasync/GDataUploader;-><init>(Landroid/content/Context;)V

    :try_start_1
    invoke-virtual {v4, p1, p2}, Lcom/google/android/picasasync/GDataUploader;->upload(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/Uploader$UploadProgressListener;)Lcom/google/android/picasasync/UploadedEntry;
    :try_end_1
    .catch Lcom/google/android/picasasync/Uploader$MediaFileChangedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/picasasync/Uploader$RestartException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/picasasync/Uploader$UnauthorizedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/picasasync/Uploader$PicasaQuotaException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/android/picasasync/Uploader$LocalIoException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    :goto_1
    return-object v0

    :cond_0
    :try_start_2
    monitor-exit p0

    move-object v0, v3

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "media file changed: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x6

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    move-object v0, v3

    goto :goto_1

    :catch_1
    move-exception v5

    :try_start_4
    const-string v6, "PicasaUploader"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "transient error on "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    monitor-enter p0

    :try_start_5
    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->isUploading()Z

    move-result v4

    if-eqz v4, :cond_4

    add-int/lit8 v0, v0, 0x1

    if-le v0, v11, :cond_3

    const-string v0, "PicasaUploader"

    const-string v1, "   max retries reached; skip the upload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x5

    new-instance v1, Ljava/io/IOException;

    const-string v2, "max retries reached"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(ILjava/lang/Throwable;)V

    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numSkippedEntries:J

    add-long/2addr v1, v9

    iput-wide v1, v0, Landroid/content/SyncStats;->numSkippedEntries:J

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v0, v3

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_6
    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unauthorized "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v5, v9

    iput-wide v5, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const/16 v1, 0x9

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    move-object v0, v3

    goto/16 :goto_1

    :catch_3
    move-exception v0

    :try_start_7
    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upload quota exceeded "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xa

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    move-object v0, v3

    goto/16 :goto_1

    :catch_4
    move-exception v0

    :try_start_8
    invoke-static {}, Lcom/google/android/picasasync/PicasaFacade;->isExternalStorageMounted()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upload failed for "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    add-long/2addr v5, v9

    iput-wide v5, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    invoke-virtual {v0}, Lcom/google/android/picasasync/Uploader$LocalIoException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/io/FileNotFoundException;

    if-eqz v1, :cond_1

    const/16 v1, 0xd

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :goto_2
    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    move-object v0, v3

    goto/16 :goto_1

    :cond_1
    const/4 v1, 0x5

    :try_start_9
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    throw v0

    :cond_2
    :try_start_a
    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upload failed for "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " on media unmounted: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v5, v9

    iput-wide v5, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const/4 v1, 0x6

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V

    goto :goto_2

    :catch_5
    move-exception v0

    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upload failed for "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ":"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "; retry later"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v5, v9

    iput-wide v5, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const/4 v1, 0x6

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    move-object v0, v3

    goto/16 :goto_1

    :catch_6
    move-exception v0

    :try_start_b
    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "upload failed for "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x5

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/picasasync/UploadsManager;->setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V

    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numSkippedEntries:J

    add-long/2addr v1, v9

    iput-wide v1, v0, Landroid/content/SyncStats;->numSkippedEntries:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    invoke-virtual {v4}, Lcom/google/android/picasasync/GDataUploader;->close()V

    move-object v0, v3

    goto/16 :goto_1

    :cond_3
    :try_start_c
    const-string v4, "PicasaUploader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "   back off "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    invoke-virtual {p0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_7
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :goto_3
    const-wide/16 v4, 0x2

    mul-long/2addr v1, v4

    :cond_4
    :try_start_e
    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->shouldRetry()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V

    monitor-exit p0

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    throw v0

    :catch_7
    move-exception v4

    :try_start_f
    const-string v4, "PicasaUploader"

    const-string v5, "waiting being interrupted!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    move-object v0, v3

    goto/16 :goto_1
.end method

.method private static generateImageThumbnail(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v5, 0x1

    iput-boolean v5, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v0, v5, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    const/high16 v5, 0x43000000

    iget v6, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v7, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Lcom/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(F)I

    move-result v5

    iput v5, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v5, 0x0

    iput-boolean v5, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/picasasync/UploadsManager;->getOrientation(Ljava/io/InputStream;)I

    move-result v2

    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v0, v5, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v5, v2, v6}, Lcom/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    :goto_0
    return-object v4

    :catch_0
    move-exception v3

    :try_start_1
    const-string v5, "PicasaUploader"

    const-string v6, "failed to generate thumbnail"

    invoke-static {v5, v6, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v4
.end method

.method private static generateThumbnail(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)[B
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "image"

    invoke-virtual {p3, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p1, p2}, Lcom/google/android/picasasync/UploadsManager;->generateImageThumbnail(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    const/16 v3, 0x80

    const/4 v4, 0x1

    invoke-static {v0, v3, v4}, Lcom/android/gallery3d/common/BitmapUtils;->resizeAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    const/high16 v2, 0x10000

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/high16 v3, 0x10000

    invoke-direct {v1, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    invoke-static {v0}, Lcom/android/gallery3d/common/BitmapUtils;->recycleSilently(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    :goto_1
    return-object v3

    :cond_1
    const-string v3, "video"

    invoke-virtual {p3, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p0, p2}, Lcom/google/android/picasasync/UploadsManager;->generateVideoThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static generateVideoThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "android.media.MediaMetadataRetriever"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    :try_start_2
    const-string v0, "setDataSource"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/net/Uri;

    aput-object v6, v4, v5

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-virtual {v0, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x9

    if-gt v0, v4, :cond_1

    const-string v0, "captureFrame"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v2, :cond_0

    :try_start_3
    const-string v1, "release"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_4
    const-string v0, "getEmbeddedPicture"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    if-eqz v0, :cond_2

    const/4 v4, 0x0

    array-length v5, v0

    invoke-static {v0, v4, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz v2, :cond_0

    :try_start_5
    const-string v1, "release"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_2
    :try_start_6
    const-string v0, "getFrameAtTime"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v2, :cond_0

    :try_start_7
    const-string v1, "release"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_1
    :try_start_8
    const-string v4, "PicasaUploader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "generateVideoThumbnail:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    if-eqz v2, :cond_3

    :try_start_9
    const-string v0, "release"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    :cond_3
    :goto_2
    move-object v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_3
    if-eqz v2, :cond_4

    :try_start_a
    const-string v1, "release"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    :cond_4
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v0

    move-object v2, v1

    goto :goto_1

    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v1

    goto/16 :goto_0
.end method

.method private static getFsId(Landroid/content/Context;)I
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/picasasync/UploadsManager;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return v1

    :cond_0
    :try_start_1
    const-string v1, "PicasaUploader"

    const-string v2, "No FSID on this device!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, -0x1

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/UploadsManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/picasasync/UploadsManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/picasasync/UploadsManager;->sInstance:Lcom/google/android/picasasync/UploadsManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/picasasync/UploadsManager;

    invoke-direct {v0, p0}, Lcom/google/android/picasasync/UploadsManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/picasasync/UploadsManager;->sInstance:Lcom/google/android/picasasync/UploadsManager;

    :cond_0
    sget-object v0, Lcom/google/android/picasasync/UploadsManager;->sInstance:Lcom/google/android/picasasync/UploadsManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getNextManualUploadFromDb(Ljava/lang/String;)Lcom/google/android/picasasync/UploadTaskEntry;
    .locals 9

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    aput-object p1, v4, v1

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/picasasync/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/picasasync/UploadsManager;->UPLOAD_TASK_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/picasasync/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "priority=? AND mime_type LIKE ?"

    const-string v7, "priority,_id"

    move-object v6, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v5

    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v1}, Lcom/google/android/picasasync/UploadTaskEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/picasasync/UploadsManager;->mProblematicAccounts:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/picasasync/UploadsManager;->accountExists(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v5, v0

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v0, "PicasaUploader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid account, remove all uploads in DB: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/picasasync/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v3, Lcom/google/android/picasasync/UploadsManager;->UPLOAD_TASK_TABLE_NAME:Ljava/lang/String;

    const-string v4, "account=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/UploadsManager;->getNextManualUploadFromDb(Ljava/lang/String;)Lcom/google/android/picasasync/UploadTaskEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getOrientation(Ljava/io/InputStream;)I
    .locals 4
    .param p0    # Ljava/io/InputStream;

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Lcom/android/gallery3d/exif/ExifReader;

    invoke-direct {v3}, Lcom/android/gallery3d/exif/ExifReader;-><init>()V

    invoke-virtual {v3, p0}, Lcom/android/gallery3d/exif/ExifReader;->read(Ljava/io/InputStream;)Lcom/android/gallery3d/exif/ExifData;

    move-result-object v0

    const/16 v3, 0x112

    invoke-virtual {v0, v3}, Lcom/android/gallery3d/exif/ExifData;->getTag(S)Lcom/android/gallery3d/exif/ExifTag;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/android/gallery3d/exif/ExifTag;->getUnsignedShort(I)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    packed-switch v3, :pswitch_data_0

    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    :goto_0
    return v2

    :pswitch_0
    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :pswitch_1
    const/16 v2, 0x5a

    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :pswitch_2
    const/16 v2, 0x10e

    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :pswitch_3
    const/16 v2, 0xb4

    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getRecordCount(Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    sget-object v1, Lcom/google/android/picasasync/UploadsManager;->UPLOAD_RECORD_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/picasasync/UploadsManager;->PROJECTION_COUNT:[Ljava/lang/String;

    const-string v3, "_id<?"

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return v0

    :cond_0
    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v8

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getTaskFromDb(J)Lcom/google/android/picasasync/UploadTaskEntry;
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/picasasync/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/picasasync/UploadTaskEntry;->fromDb(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v0

    return-object v0
.end method

.method private initHandler(Landroid/os/HandlerThread;)Landroid/os/Handler;
    .locals 2
    .param p1    # Landroid/os/HandlerThread;

    new-instance v0, Lcom/google/android/picasasync/UploadsManager$2;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/picasasync/UploadsManager$2;-><init>(Lcom/google/android/picasasync/UploadsManager;Landroid/os/Looper;)V

    return-object v0
.end method

.method private isAndroidUpgraded()Z
    .locals 4

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mSettings:Landroid/content/SharedPreferences;

    const-string v2, "system_release"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mSettings:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "system_release"

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "System upgrade from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadSavedStates()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSettings:Landroid/content/SharedPreferences;

    const-string v1, "external_storage_fsid"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mIsExternalStorageFsIdReady:Z

    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mIsExternalStorageFsIdReady:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSettings:Landroid/content/SharedPreferences;

    const-string v1, "external_storage_fsid"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/picasasync/UploadsManager;->mExternalStorageFsId:I

    :cond_0
    return-void
.end method

.method private notifyManualUploadDbChanges(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/picasasync/PicasaFacade;->uploadsUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    if-eqz p1, :cond_0

    sget-object v1, Lcom/google/android/picasasync/PicasaFacade;->uploadRecordsUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    :cond_0
    return-void
.end method

.method private declared-synchronized onFsIdChangedInternal()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/picasasync/PicasaFacade;->isExternalStorageMounted()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/picasasync/UploadsManager;->getFsId(Landroid/content/Context;)I

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/picasasync/UploadsManager;->mIsExternalStorageFsIdReady:Z

    if-nez v1, :cond_1

    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set fsid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/picasasync/UploadsManager;->mIsExternalStorageFsIdReady:Z

    iput v0, p0, Lcom/google/android/picasasync/UploadsManager;->mExternalStorageFsId:I

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mSettings:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "external_storage_fsid"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_1
    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasks(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_1
    :try_start_2
    iget v1, p0, Lcom/google/android/picasasync/UploadsManager;->mExternalStorageFsId:I

    if-ne v1, v0, :cond_2

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncedAccountAlbumPairs:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    goto :goto_1

    :cond_2
    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fsid changed from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/picasasync/UploadsManager;->mExternalStorageFsId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v0, p0, Lcom/google/android/picasasync/UploadsManager;->mExternalStorageFsId:I

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mSettings:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "external_storage_fsid"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->reset()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private static purgeRecords(Landroid/database/sqlite/SQLiteDatabase;IJ)V
    .locals 6

    const-string v0, "PicasaUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "target purge count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x0

    :goto_0
    add-long v2, p2, v0

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    sget-object v0, Lcom/google/android/picasasync/UploadsManager;->UPLOAD_RECORD_TABLE_NAME:Ljava/lang/String;

    const-string v1, "_id<?"

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    const-string v1, "PicasaUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " purged"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-static {p0, v2, v3}, Lcom/google/android/picasasync/UploadsManager;->getRecordCount(Landroid/database/sqlite/SQLiteDatabase;J)I

    move-result v4

    if-le v4, p1, :cond_1

    move-wide p2, v2

    goto :goto_0

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method private recordResult(Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;
    .locals 10
    .param p1    # Lcom/google/android/picasasync/UploadedEntry;

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/picasasync/UploadedEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v1, v0, p1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    const-wide v3, 0x3f40624dd2f1a9fcL

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    sget-object v1, Lcom/google/android/picasasync/UploadsManager;->UPLOAD_RECORD_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/picasasync/UploadsManager;->PROJECTION_COUNT:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v9

    if-eqz v9, :cond_1

    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/16 v1, 0x7d0

    if-le v8, v1, :cond_1

    div-int/lit8 v1, v8, 0x2

    iget-wide v2, p1, Lcom/google/android/picasasync/UploadedEntry;->id:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/picasasync/UploadsManager;->purgeRecords(Landroid/database/sqlite/SQLiteDatabase;IJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :try_start_2
    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_3
    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method private declared-synchronized reloadSystemSettingsInternal()V
    .locals 13

    const/4 v11, 0x1

    const/4 v12, 0x0

    monitor-enter p0

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/picasasync/PicasaFacade;->settingsUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/picasasync/UploadsManager;->PROJECTION_ENABLE_ACCOUNT_WIFI:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    if-eqz v6, :cond_0

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "PicasaUploader"

    const-string v1, "   failed to query system settings"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/picasasync/UploadsManager;->mReloadSystemSettingDelay:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-wide v0, p0, Lcom/google/android/picasasync/UploadsManager;->mReloadSystemSettingDelay:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/picasasync/UploadsManager;->mReloadSystemSettingDelay:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    :try_start_3
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_8

    move v9, v11

    :goto_1
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_9

    move v10, v11

    :goto_2
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_a

    move v8, v11

    :goto_3
    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_b

    move v7, v11

    :goto_4
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/google/android/picasasync/UploadsManager;->mReloadSystemSettingDelay:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mWifiOnlyPhoto:Z

    if-ne v9, v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mWifiOnlyVideo:Z

    if-ne v10, v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncOnRoaming:Z

    if-ne v8, v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncOnBattery:Z

    if-eq v7, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasks(J)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mWifiOnlyPhoto:Z

    if-eq v9, v0, :cond_4

    const-string v0, "PicasaUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   wifiOnlyPhoto changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mWifiOnlyVideo:Z

    if-eq v10, v0, :cond_5

    const-string v0, "PicasaUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   wifiOnlyVideo changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncOnRoaming:Z

    if-eq v8, v0, :cond_6

    const-string v0, "PicasaUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   syncOnRoaming changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncOnBattery:Z

    if-eq v7, v0, :cond_7

    const-string v0, "PicasaUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   syncOnBattery changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iput-boolean v9, p0, Lcom/google/android/picasasync/UploadsManager;->mWifiOnlyPhoto:Z

    iput-boolean v10, p0, Lcom/google/android/picasasync/UploadsManager;->mWifiOnlyVideo:Z

    iput-boolean v8, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncOnRoaming:Z

    iput-boolean v7, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncOnBattery:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_8
    move v9, v12

    goto/16 :goto_1

    :cond_9
    move v10, v12

    goto/16 :goto_2

    :cond_a
    move v8, v12

    goto/16 :goto_3

    :cond_b
    move v7, v12

    goto/16 :goto_4

    :catchall_1
    move-exception v0

    :try_start_5
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private removeTaskFromDb(J)Z
    .locals 2
    .param p1    # J

    sget-object v0, Lcom/google/android/picasasync/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/gallery3d/common/EntrySchema;->deleteWithId(Landroid/database/sqlite/SQLiteDatabase;J)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized reset()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->resetStates()V

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->resetUploadDatabase()V

    invoke-direct {p0}, Lcom/google/android/picasasync/UploadsManager;->reloadSystemSettingsInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private resetStates()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mSyncedAccountAlbumPairs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    return-void
.end method

.method private resetUploadDatabase()V
    .locals 5

    :try_start_0
    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/UploadsDatabaseHelper;->reset()V

    const-wide/16 v1, 0x3a98

    iput-wide v1, p0, Lcom/google/android/picasasync/UploadsManager;->mResetDelay:J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "PicasaUploader"

    const-string v2, "DB not ready for reset?"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/google/android/picasasync/UploadsManager;->mResetDelay:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-wide v1, p0, Lcom/google/android/picasasync/UploadsManager;->mResetDelay:J

    const-wide/16 v3, 0x2

    mul-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/picasasync/UploadsManager;->mResetDelay:J

    goto :goto_0
.end method

.method private sendManualUploadReport(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;I)V
    .locals 5
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # Lcom/google/android/picasasync/UploadedEntry;
    .param p3    # I

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.uploader.manual_upload_report"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v2, "manual_upload_upload_id"

    iget-wide v3, p1, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "manual_upload_content_uri"

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "manual_upload_state"

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "manual_upload_uploader_state"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "manual_upload_progress"

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getPercentageUploaded()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "manual_upload_display_name"

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_1

    const-string v2, "manual_upload_record_id"

    iget-wide v3, p2, Lcom/google/android/picasasync/UploadedEntry;->id:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_1
    iget-object v2, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private declared-synchronized setCurrentUploadTask(Lcom/google/android/picasasync/UploadsManager$UploadTask;)V
    .locals 1
    .param p1    # Lcom/google/android/picasasync/UploadsManager$UploadTask;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/picasasync/UploadsManager;->mCurrent:Lcom/google/android/picasasync/UploadsManager$UploadTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setState(Lcom/google/android/picasasync/UploadTaskEntry;I)V
    .locals 1
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # I

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1, p2}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setState(Lcom/google/android/picasasync/UploadTaskEntry;ILjava/lang/Throwable;)V
    .locals 1
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;
    .param p2    # I
    .param p3    # Ljava/lang/Throwable;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1, p2, p3}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(ILjava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized updateTaskStateAndProgressInDb(Lcom/google/android/picasasync/UploadTaskEntry;)V
    .locals 2
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->isReadyForUpload()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/picasasync/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private writeToPhotoTable(Lcom/google/android/picasasync/UploadTaskEntry;Lcom/google/android/picasasync/UploadedEntry;Landroid/content/SyncResult;)Z
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getUserId(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    const-string v0, "PicasaUploader"

    const-string v1, "no user owns the photo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v3, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    new-instance v3, Lcom/google/android/picasasync/PhotoEntry;

    invoke-direct {v3}, Lcom/google/android/picasasync/PhotoEntry;-><init>()V

    iget-wide v4, p2, Lcom/google/android/picasasync/UploadedEntry;->idFromServer:J

    iput-wide v4, v3, Lcom/google/android/picasasync/PhotoEntry;->id:J

    iput-wide v1, v3, Lcom/google/android/picasasync/PhotoEntry;->userId:J

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, v3, Lcom/google/android/picasasync/PhotoEntry;->albumId:J

    iget-object v0, p2, Lcom/google/android/picasasync/UploadedEntry;->fingerprint:[B

    iput-object v0, v3, Lcom/google/android/picasasync/PhotoEntry;->fingerprint:[B

    iget v0, p2, Lcom/google/android/picasasync/UploadedEntry;->fingerprintHash:I

    iput v0, v3, Lcom/google/android/picasasync/PhotoEntry;->fingerprintHash:I

    iget-wide v0, p2, Lcom/google/android/picasasync/UploadedEntry;->timestamp:J

    iput-wide v0, v3, Lcom/google/android/picasasync/PhotoEntry;->dateTaken:J

    sget-object v0, Lcom/google/android/picasasync/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v1, p0, Lcom/google/android/picasasync/UploadsManager;->mPicasaDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addManualUpload(Lcom/google/android/picasasync/UploadTaskEntry;)J
    .locals 12
    .param p1    # Lcom/google/android/picasasync/UploadTaskEntry;

    const/4 v11, 0x1

    :try_start_0
    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/google/android/picasasync/PicasaUploadHelper;->setContentType(Landroid/content/ContentResolver;Lcom/google/android/picasasync/UploadTaskEntry;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getMimeType()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v3, v8, v9}, Lcom/google/android/picasasync/UploadsManager;->generateThumbnail(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)[B

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p1, v5}, Lcom/google/android/picasasync/UploadTaskEntry;->setThumbnail([B)V

    :cond_0
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Lcom/google/android/picasasync/UploadTaskEntry;->setPriority(I)V

    invoke-static {v3, p1}, Lcom/google/android/picasasync/PicasaUploadHelper;->setFileSize(Landroid/content/ContentResolver;Lcom/google/android/picasasync/UploadTaskEntry;)V

    iget-object v7, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    invoke-virtual {v7}, Lcom/google/android/picasasync/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v7, Lcom/google/android/picasasync/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v7, v0, p1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    move-result-wide v1

    const-string v7, "PicasaUploader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "manual upload: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v9, p1, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " type="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-wide v1

    :catch_0
    move-exception v4

    const-string v7, "PicasaUploader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to insert upload request "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    instance-of v7, v4, Ljava/io/FileNotFoundException;

    if-eqz v7, :cond_1

    const/16 v7, 0xd

    invoke-virtual {p1, v7, v4}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(ILjava/lang/Throwable;)V

    :goto_1
    new-instance v7, Lcom/google/android/picasasync/UploadedEntry;

    invoke-direct {v7, p1}, Lcom/google/android/picasasync/UploadedEntry;-><init>(Lcom/google/android/picasasync/UploadTaskEntry;)V

    invoke-direct {p0, v7}, Lcom/google/android/picasasync/UploadsManager;->recordResult(Lcom/google/android/picasasync/UploadedEntry;)Lcom/google/android/picasasync/UploadedEntry;

    invoke-direct {p0, v11}, Lcom/google/android/picasasync/UploadsManager;->notifyManualUploadDbChanges(Z)V

    const-wide/16 v1, 0x0

    goto :goto_0

    :cond_1
    const/16 v7, 0xb

    invoke-virtual {p1, v7, v4}, Lcom/google/android/picasasync/UploadTaskEntry;->setState(ILjava/lang/Throwable;)V

    goto :goto_1
.end method

.method public cancelTask(J)V
    .locals 3
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public getManualPhotoUploadTaskProvider()Lcom/google/android/picasasync/SyncTaskProvider;
    .locals 2

    new-instance v0, Lcom/google/android/picasasync/UploadsManager$ManualUploadTaskProvider;

    const-string v1, "image/%"

    invoke-direct {v0, p0, v1}, Lcom/google/android/picasasync/UploadsManager$ManualUploadTaskProvider;-><init>(Lcom/google/android/picasasync/UploadsManager;Ljava/lang/String;)V

    return-object v0
.end method

.method public getManualVideoUploadTaskProvider()Lcom/google/android/picasasync/SyncTaskProvider;
    .locals 2

    new-instance v0, Lcom/google/android/picasasync/UploadsManager$ManualUploadTaskProvider;

    const-string v1, "%"

    invoke-direct {v0, p0, v1}, Lcom/google/android/picasasync/UploadsManager$ManualUploadTaskProvider;-><init>(Lcom/google/android/picasasync/UploadsManager;Ljava/lang/String;)V

    return-object v0
.end method

.method public getUploadsDatabaseHelper()Lcom/google/android/picasasync/UploadsDatabaseHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mUploadsDbHelper:Lcom/google/android/picasasync/UploadsDatabaseHelper;

    return-object v0
.end method

.method public reloadSystemSettings()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/picasasync/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
