.class public Lb/x;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static synthetic f:Z


# instance fields
.field private final a:Lb/ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/ac",
            "<",
            "Lb/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lb/P;

.field private final c:Ljava/util/concurrent/locks/Lock;

.field private d:Z

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/x;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/x;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lb/ac;

    const-class v1, Lb/c;

    sget-object v2, Lb/d;->b:Lb/d;

    invoke-virtual {v2}, Lb/d;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lb/ac;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lb/x;->a:Lb/ac;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lb/x;->c:Ljava/util/concurrent/locks/Lock;

    new-instance v0, Lb/P;

    invoke-direct {v0}, Lb/P;-><init>()V

    iput-object v0, p0, Lb/x;->b:Lb/P;

    iget-object v0, p0, Lb/x;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/c;

    sget-boolean v1, Lb/x;->f:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/x;->d:Z

    iput-object p1, p0, Lb/x;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(J)Lb/c;
    .locals 2

    iget-boolean v0, p0, Lb/x;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/x;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/c;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/x;->d:Z

    iget-object v0, p0, Lb/x;->b:Lb/P;

    invoke-virtual {v0}, Lb/P;->d()V

    :cond_2
    iget-object v0, p0, Lb/x;->b:Lb/P;

    invoke-virtual {v0, p1, p2}, Lb/P;->a(J)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/x;->d:Z

    iget-object v0, p0, Lb/x;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/c;

    sget-boolean v1, Lb/x;->f:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final a()Ljava/nio/channels/SelectableChannel;
    .locals 1

    iget-object v0, p0, Lb/x;->b:Lb/P;

    invoke-virtual {v0}, Lb/P;->b()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lb/c;)V
    .locals 2

    iget-object v0, p0, Lb/x;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lb/x;->a:Lb/ac;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lb/ac;->a(Ljava/lang/Object;Z)V

    iget-object v0, p0, Lb/x;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    iget-object v1, p0, Lb/x;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/x;->b:Lb/P;

    invoke-virtual {v0}, Lb/P;->c()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lb/x;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lb/x;->b:Lb/P;

    invoke-virtual {v0}, Lb/P;->a()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lb/x;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
