.class public final Lb/j;
.super Lb/k;
.source "SourceFile"


# instance fields
.field private a:Lb/y;

.field private final b:[B

.field private c:Lb/q;


# direct methods
.method public constructor <init>(I)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0, p1}, Lb/k;-><init>(I)V

    const/16 v0, 0xa

    new-array v0, v0, [B

    iput-object v0, p0, Lb/j;->b:[B

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2, v2}, Lb/j;->a([BIIZ)V

    return-void
.end method


# virtual methods
.method public final a(Lb/q;)V
    .locals 0

    iput-object p1, p0, Lb/j;->c:Lb/q;

    return-void
.end method

.method protected final a()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lb/j;->c()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget-object v2, p0, Lb/j;->a:Lb/y;

    invoke-virtual {v2}, Lb/y;->f()[B

    move-result-object v2

    iget-object v3, p0, Lb/j;->a:Lb/y;

    invoke-virtual {v3}, Lb/y;->g()I

    move-result v3

    iget-object v4, p0, Lb/j;->a:Lb/y;

    invoke-virtual {v4}, Lb/y;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    invoke-virtual {p0, v2, v3, v1, v0}, Lb/j;->a([BIIZ)V

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lb/j;->c:Lb/q;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb/j;->c:Lb/q;

    invoke-interface {v2}, Lb/q;->a()Lb/y;

    move-result-object v2

    iput-object v2, p0, Lb/j;->a:Lb/y;

    iget-object v2, p0, Lb/j;->a:Lb/y;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb/j;->a:Lb/y;

    invoke-virtual {v2}, Lb/y;->g()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const/16 v3, 0xff

    if-ge v2, v3, :cond_2

    iget-object v3, p0, Lb/j;->b:[B

    int-to-byte v2, v2

    aput-byte v2, v3, v0

    iget-object v2, p0, Lb/j;->b:[B

    iget-object v3, p0, Lb/j;->a:Lb/y;

    invoke-virtual {v3}, Lb/y;->c()I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    iget-object v2, p0, Lb/j;->b:[B

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3, v0, v0}, Lb/j;->a([BIIZ)V

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lb/j;->b:[B

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lb/j;->a:Lb/y;

    invoke-virtual {v2}, Lb/y;->c()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lb/j;->b:[B

    const/16 v3, 0xa

    invoke-virtual {p0, v2, v3, v0, v0}, Lb/j;->a([BIIZ)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
