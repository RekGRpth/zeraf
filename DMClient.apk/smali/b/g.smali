.class public final Lb/g;
.super Lb/h;
.source "SourceFile"


# instance fields
.field private final a:[B

.field private b:Lb/y;

.field private final c:J

.field private d:Lb/p;


# direct methods
.method public constructor <init>(IJ)V
    .locals 3

    invoke-direct {p0, p1}, Lb/h;-><init>(I)V

    iput-wide p2, p0, Lb/g;->c:J

    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lb/g;->a:[B

    iget-object v0, p0, Lb/g;->a:[B

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lb/g;->a([BII)V

    return-void
.end method


# virtual methods
.method public final a(Lb/p;)V
    .locals 0

    iput-object p1, p0, Lb/g;->d:Lb/p;

    return-void
.end method

.method protected final a()Z
    .locals 12

    const/4 v11, 0x2

    const-wide/16 v9, 0x1

    const-wide/16 v7, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lb/g;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lb/g;->a:[B

    aget-byte v0, v0, v1

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lb/g;->a:[B

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1, v2}, Lb/g;->a([BII)V

    :goto_1
    move v0, v2

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lb/g;->d()V

    move v0, v1

    goto :goto_0

    :cond_1
    if-gez v0, :cond_2

    and-int/lit16 v0, v0, 0xff

    :cond_2
    iget-wide v3, p0, Lb/g;->c:J

    cmp-long v3, v3, v7

    if-ltz v3, :cond_3

    add-int/lit8 v3, v0, -0x1

    int-to-long v3, v3

    iget-wide v5, p0, Lb/g;->c:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_3

    invoke-virtual {p0}, Lb/g;->d()V

    move v0, v1

    goto :goto_0

    :cond_3
    new-instance v1, Lb/y;

    add-int/lit8 v0, v0, -0x1

    invoke-direct {v1, v0}, Lb/y;-><init>(I)V

    iput-object v1, p0, Lb/g;->b:Lb/y;

    iget-object v0, p0, Lb/g;->a:[B

    invoke-virtual {p0, v0, v2, v11}, Lb/g;->a([BII)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lb/g;->a:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v3

    cmp-long v0, v3, v7

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lb/g;->d()V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-wide v5, p0, Lb/g;->c:J

    cmp-long v0, v5, v7

    if-ltz v0, :cond_5

    sub-long v5, v3, v9

    iget-wide v7, p0, Lb/g;->c:J

    cmp-long v0, v5, v7

    if-lez v0, :cond_5

    invoke-virtual {p0}, Lb/g;->d()V

    move v0, v1

    goto :goto_0

    :cond_5
    sub-long v5, v3, v9

    const-wide/32 v7, 0x7fffffff

    cmp-long v0, v5, v7

    if-lez v0, :cond_6

    invoke-virtual {p0}, Lb/g;->d()V

    move v0, v1

    goto :goto_0

    :cond_6
    sub-long v0, v3, v9

    long-to-int v0, v0

    new-instance v1, Lb/y;

    invoke-direct {v1, v0}, Lb/y;-><init>(I)V

    iput-object v1, p0, Lb/g;->b:Lb/y;

    iget-object v0, p0, Lb/g;->a:[B

    invoke-virtual {p0, v0, v2, v11}, Lb/g;->a([BII)V

    move v0, v2

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lb/g;->a:[B

    aget-byte v0, v0, v1

    iget-object v1, p0, Lb/g;->b:Lb/y;

    and-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Lb/y;->a(I)V

    iget-object v0, p0, Lb/g;->b:Lb/y;

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lb/g;->a(Lb/y;I)V

    move v0, v2

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lb/g;->d:Lb/p;

    if-nez v0, :cond_7

    move v0, v1

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lb/g;->d:Lb/p;

    iget-object v3, p0, Lb/g;->b:Lb/y;

    invoke-interface {v0, v3}, Lb/p;->a(Lb/y;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lb/g;->a:[B

    invoke-virtual {p0, v0, v2, v1}, Lb/g;->a([BII)V

    move v0, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
