.class public final Lb/B;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field A:I

.field B:I

.field C:I

.field D:I

.field E:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lb/h;",
            ">;"
        }
    .end annotation
.end field

.field F:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lb/k;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lb/T$a;",
            ">;"
        }
    .end annotation
.end field

.field a:I

.field b:I

.field c:J

.field d:B

.field e:[B

.field f:Ljava/lang/String;

.field g:I

.field h:I

.field i:I

.field j:I

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:J

.field r:I

.field s:I

.field t:I

.field u:I

.field v:Z

.field w:Z

.field x:Z

.field y:Z

.field z:I


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0x64

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x3e8

    iput v0, p0, Lb/B;->a:I

    const/16 v0, 0x3e8

    iput v0, p0, Lb/B;->b:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lb/B;->c:J

    iput-byte v3, p0, Lb/B;->d:B

    iput v5, p0, Lb/B;->g:I

    const/16 v0, 0x2710

    iput v0, p0, Lb/B;->h:I

    iput v4, p0, Lb/B;->i:I

    iput v3, p0, Lb/B;->j:I

    iput v3, p0, Lb/B;->k:I

    iput v2, p0, Lb/B;->l:I

    iput v2, p0, Lb/B;->m:I

    iput v5, p0, Lb/B;->n:I

    iput v3, p0, Lb/B;->o:I

    iput v5, p0, Lb/B;->p:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lb/B;->q:J

    iput v2, p0, Lb/B;->r:I

    iput v2, p0, Lb/B;->s:I

    iput v4, p0, Lb/B;->t:I

    iput v3, p0, Lb/B;->u:I

    iput-boolean v4, p0, Lb/B;->v:Z

    iput-boolean v4, p0, Lb/B;->w:Z

    iput-boolean v3, p0, Lb/B;->x:Z

    iput-boolean v3, p0, Lb/B;->y:Z

    iput v2, p0, Lb/B;->z:I

    iput v2, p0, Lb/B;->A:I

    iput v2, p0, Lb/B;->B:I

    iput v2, p0, Lb/B;->C:I

    iput v3, p0, Lb/B;->D:I

    iput-object v6, p0, Lb/B;->e:[B

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/B;->G:Ljava/util/List;

    iput-object v6, p0, Lb/B;->E:Ljava/lang/Class;

    iput-object v6, p0, Lb/B;->F:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Z
    .locals 6

    const/16 v5, 0xff

    const/4 v4, -0x1

    const/16 v3, 0x16

    const/4 v0, 0x0

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    :cond_0
    :goto_0
    :sswitch_0
    return v1

    :sswitch_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lb/B;->a:I

    iget v2, p0, Lb/B;->a:I

    if-gez v2, :cond_0

    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto :goto_0

    :sswitch_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lb/B;->b:I

    iget v2, p0, Lb/B;->b:I

    if-gez v2, :cond_0

    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto :goto_0

    :sswitch_3
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lb/B;->c:J

    goto :goto_0

    :sswitch_4
    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    :goto_1
    if-eqz p2, :cond_1

    array-length v2, p2

    if-le v2, v5, :cond_4

    :cond_1
    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto :goto_0

    :cond_2
    instance-of v2, p2, [B

    if-eqz v2, :cond_3

    check-cast p2, [B

    goto :goto_1

    :cond_3
    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto :goto_0

    :cond_4
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lb/B;->e:[B

    iget-object v0, p0, Lb/B;->e:[B

    array-length v0, v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lb/B;->d:B

    goto :goto_0

    :sswitch_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->g:I

    goto :goto_0

    :sswitch_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->h:I

    goto :goto_0

    :sswitch_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->j:I

    goto :goto_0

    :sswitch_8
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->k:I

    goto :goto_0

    :sswitch_9
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->m:I

    goto/16 :goto_0

    :sswitch_a
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lb/B;->n:I

    iget v2, p0, Lb/B;->n:I

    if-ge v2, v4, :cond_0

    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto/16 :goto_0

    :sswitch_b
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lb/B;->o:I

    iget v2, p0, Lb/B;->o:I

    if-gez v2, :cond_0

    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto/16 :goto_0

    :sswitch_c
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->p:I

    goto/16 :goto_0

    :sswitch_d
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lb/B;->q:J

    goto/16 :goto_0

    :sswitch_e
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->i:I

    goto/16 :goto_0

    :sswitch_f
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->r:I

    goto/16 :goto_0

    :sswitch_10
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lb/B;->s:I

    goto/16 :goto_0

    :sswitch_11
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lb/B;->t:I

    iget v2, p0, Lb/B;->t:I

    if-eqz v2, :cond_0

    iget v2, p0, Lb/B;->t:I

    if-eq v2, v1, :cond_0

    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto/16 :goto_0

    :sswitch_12
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lb/B;->z:I

    iget v2, p0, Lb/B;->z:I

    if-eq v2, v4, :cond_0

    iget v2, p0, Lb/B;->z:I

    if-eqz v2, :cond_0

    iget v2, p0, Lb/B;->z:I

    if-eq v2, v1, :cond_0

    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto/16 :goto_0

    :sswitch_13
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lb/B;->u:I

    iget v2, p0, Lb/B;->u:I

    if-eqz v2, :cond_0

    iget v2, p0, Lb/B;->u:I

    if-eq v2, v1, :cond_0

    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto/16 :goto_0

    :sswitch_14
    check-cast p2, Ljava/lang/String;

    if-nez p2, :cond_5

    iget-object v0, p0, Lb/B;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_7

    :cond_6
    invoke-static {v3}, Lb/ae;->a(I)V

    move v1, v0

    goto/16 :goto_0

    :cond_7
    new-instance v2, Lb/T$a;

    invoke-direct {v2}, Lb/T$a;-><init>()V

    iget v3, p0, Lb/B;->t:I

    if-ne v3, v1, :cond_8

    move v0, v1

    :cond_8
    invoke-virtual {v2, p2, v0}, Lb/T$a;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lb/B;->G:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_15
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_9

    :try_start_0
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v2, Lb/k;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lb/B;->F:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_9
    instance-of v0, p2, Ljava/lang/Class;

    if-eqz v0, :cond_a

    check-cast p2, Ljava/lang/Class;

    iput-object p2, p0, Lb/B;->F:Ljava/lang/Class;

    goto/16 :goto_0

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "encoder "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_16
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_b

    :try_start_1
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v2, Lb/h;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lb/B;->E:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_b
    instance-of v0, p2, Ljava/lang/Class;

    if-eqz v0, :cond_c

    check-cast p2, Ljava/lang/Class;

    iput-object p2, p0, Lb/B;->E:Ljava/lang/Class;

    goto/16 :goto_0

    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "decoder "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x8 -> :sswitch_5
        0x9 -> :sswitch_6
        0xb -> :sswitch_7
        0xc -> :sswitch_8
        0x11 -> :sswitch_9
        0x12 -> :sswitch_a
        0x13 -> :sswitch_c
        0x15 -> :sswitch_b
        0x16 -> :sswitch_d
        0x17 -> :sswitch_1
        0x18 -> :sswitch_2
        0x19 -> :sswitch_e
        0x1b -> :sswitch_f
        0x1c -> :sswitch_10
        0x1f -> :sswitch_11
        0x22 -> :sswitch_12
        0x23 -> :sswitch_0
        0x24 -> :sswitch_0
        0x25 -> :sswitch_0
        0x26 -> :sswitch_14
        0x27 -> :sswitch_13
        0x3e9 -> :sswitch_15
        0x3ea -> :sswitch_16
    .end sparse-switch
.end method
