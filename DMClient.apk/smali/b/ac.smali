.class public Lb/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static synthetic f:Z


# instance fields
.field private final a:Lb/ad;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/ad",
            "<TT;>;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I

.field private final e:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/ac;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lb/ad;

    invoke-direct {v0, p1, p2}, Lb/ad;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lb/ac;->a:Lb/ad;

    iget-object v0, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v0}, Lb/ad;->c()I

    move-result v0

    iput v0, p0, Lb/ac;->d:I

    iput v0, p0, Lb/ac;->c:I

    iput v0, p0, Lb/ac;->b:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v1}, Lb/ad;->c()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lb/ac;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget v0, p0, Lb/ac;->d:I

    iget-object v1, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v1}, Lb/ad;->c()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v0}, Lb/ad;->f()V

    iget-object v0, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v0}, Lb/ad;->d()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    iget-object v0, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v0, p1}, Lb/ad;->a(Ljava/lang/Object;)V

    if-nez p2, :cond_0

    iget-object v0, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v0}, Lb/ad;->c()I

    move-result v0

    iput v0, p0, Lb/ac;->d:I

    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, Lb/ac;->b:I

    iget v2, p0, Lb/ac;->d:I

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lb/ac;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v2, p0, Lb/ac;->b:I

    iget v3, p0, Lb/ac;->d:I

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lb/ac;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v1, p0, Lb/ac;->d:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget v0, p0, Lb/ac;->d:I

    iput v0, p0, Lb/ac;->b:I

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget v1, p0, Lb/ac;->d:I

    iput v1, p0, Lb/ac;->b:I

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v3, -0x1

    iget-object v1, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v1}, Lb/ad;->a()I

    move-result v1

    iget v2, p0, Lb/ac;->c:I

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lb/ac;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lb/ac;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    iput v2, p0, Lb/ac;->c:I

    :cond_2
    iget v2, p0, Lb/ac;->c:I

    if-eq v1, v2, :cond_3

    iget v1, p0, Lb/ac;->c:I

    if-ne v1, v3, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lb/ac;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v0}, Lb/ad;->e()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lb/ac;->c()Z

    move-result v0

    sget-boolean v1, Lb/ac;->f:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/ac;->a:Lb/ad;

    invoke-virtual {v0}, Lb/ad;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
