.class public final Lb/F;
.super Lb/G;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/F$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/nio/channels/SelectableChannel;",
            "Lb/F$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private volatile c:Z

.field private volatile d:Z

.field private e:Ljava/lang/Thread;

.field private f:Ljava/nio/channels/Selector;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "poller"

    invoke-direct {p0, v0}, Lb/F;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lb/G;-><init>()V

    iput-object p1, p0, Lb/F;->g:Ljava/lang/String;

    iput-boolean v0, p0, Lb/F;->b:Z

    iput-boolean v0, p0, Lb/F;->c:Z

    iput-boolean v0, p0, Lb/F;->d:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lb/F;->a:Ljava/util/Map;

    :try_start_0
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v0

    iput-object v0, p0, Lb/F;->f:Ljava/nio/channels/Selector;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method private final a(Ljava/nio/channels/SelectableChannel;IZ)V
    .locals 3

    iget-object v0, p0, Lb/F;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/F$a;

    if-eqz p3, :cond_0

    iget v1, v0, Lb/F$a;->c:I

    xor-int/lit8 v2, p2, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Lb/F$a;->c:I

    :goto_0
    iget-object v1, v0, Lb/F$a;->b:Ljava/nio/channels/SelectionKey;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lb/F$a;->b:Ljava/nio/channels/SelectionKey;

    iget v0, v0, Lb/F$a;->c:I

    invoke-virtual {v1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    :goto_1
    return-void

    :cond_0
    iget v1, v0, Lb/F$a;->c:I

    or-int/2addr v1, p2

    iput v1, v0, Lb/F$a;->c:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/F;->b:Z

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-boolean v0, p0, Lb/F;->d:Z

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lb/F;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lb/F;->f:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lb/F;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/F$a;

    iput-boolean v1, v0, Lb/F$a;->d:Z

    iput-boolean v1, p0, Lb/F;->b:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lb/F;->a(I)V

    return-void
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;Lb/t;)V
    .locals 2

    iget-object v0, p0, Lb/F;->a:Ljava/util/Map;

    new-instance v1, Lb/F$a;

    invoke-direct {v1, p2}, Lb/F$a;-><init>(Lb/t;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lb/F;->a(I)V

    return-void
.end method

.method public final b()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lb/F;->g:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lb/F;->e:Ljava/lang/Thread;

    iget-object v0, p0, Lb/F;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public final b(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;IZ)V

    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/F;->c:Z

    iget-object v0, p0, Lb/F;->f:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    return-void
.end method

.method public final c(Ljava/nio/channels/SelectableChannel;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, v0}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;IZ)V

    return-void
.end method

.method public final d(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;IZ)V

    return-void
.end method

.method public final e(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;IZ)V

    return-void
.end method

.method public final f(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;IZ)V

    return-void
.end method

.method public final run()V
    .locals 12

    const/4 v11, 0x1

    const/4 v3, 0x0

    move v4, v3

    :cond_0
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lb/F;->c:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_1

    :goto_1
    iput-boolean v11, p0, Lb/F;->d:Z

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lb/F;->e()J

    move-result-wide v5

    iget-boolean v1, p0, Lb/F;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lb/F;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    iput-boolean v1, p0, Lb/F;->b:Z

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v1

    :try_start_2
    iget-object v7, p0, Lb/F;->f:Ljava/nio/channels/Selector;

    invoke-virtual {v7, v5, v6}, Ljava/nio/channels/Selector;->select(J)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v7

    if-nez v7, :cond_9

    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-eqz v7, :cond_4

    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long v1, v7, v1

    const-wide/16 v7, 0x2

    div-long/2addr v5, v7
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    cmp-long v1, v1, v5

    if-gez v1, :cond_7

    :cond_4
    add-int/lit8 v1, v4, 0x1

    :goto_3
    const/16 v2, 0xa

    if-le v1, v2, :cond_e

    :try_start_4
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v1

    :try_start_5
    iget-object v2, p0, Lb/F;->f:Ljava/nio/channels/Selector;

    invoke-virtual {v2}, Ljava/nio/channels/Selector;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :goto_4
    :try_start_6
    iput-object v1, p0, Lb/F;->f:Ljava/nio/channels/Selector;

    iget-object v1, p0, Lb/F;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    iput-boolean v1, p0, Lb/F;->b:Z

    move v4, v3

    goto :goto_0

    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/channels/SelectableChannel;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb/F$a;

    iget-object v8, v1, Lb/F$a;->b:Ljava/nio/channels/SelectionKey;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    if-nez v8, :cond_6

    :try_start_7
    iget-object v8, p0, Lb/F;->f:Ljava/nio/channels/Selector;

    iget v9, v1, Lb/F$a;->c:I

    iget-object v10, v1, Lb/F$a;->a:Lb/t;

    invoke-virtual {v2, v8, v9, v10}, Ljava/nio/channels/SelectableChannel;->register(Ljava/nio/channels/Selector;ILjava/lang/Object;)Ljava/nio/channels/SelectionKey;

    move-result-object v2

    iput-object v2, v1, Lb/F$a;->b:Ljava/nio/channels/SelectionKey;
    :try_end_7
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_2

    :cond_6
    :try_start_8
    iget-boolean v2, v1, Lb/F$a;->d:Z

    if-eqz v2, :cond_2

    iget-object v1, v1, Lb/F$a;->b:Ljava/nio/channels/SelectionKey;

    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->cancel()V

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    :catch_2
    move-exception v1

    :try_start_9
    new-instance v2, Lb/ae$a;

    invoke-direct {v2, v1}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v2

    :cond_7
    move v1, v3

    goto :goto_3

    :catch_3
    move-exception v1

    new-instance v2, Lb/ae$a;

    invoke-direct {v2, v1}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb/F$a;

    const/4 v4, 0x0

    iput-object v4, v1, Lb/F$a;->b:Ljava/nio/channels/SelectionKey;

    goto :goto_5

    :cond_9
    iget-object v1, p0, Lb/F;->f:Ljava/nio/channels/Selector;

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_a
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/nio/channels/SelectionKey;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb/t;

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :try_start_a
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v1}, Lb/t;->g_()V

    :cond_b
    :goto_7
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Lb/t;->h_()V

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_6

    :cond_c
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isAcceptable()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-interface {v1}, Lb/t;->e()V

    goto :goto_7

    :cond_d
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v1}, Lb/t;->d()V
    :try_end_a
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    goto :goto_7

    :catch_5
    move-exception v2

    goto/16 :goto_4

    :cond_e
    move v4, v1

    goto/16 :goto_0
.end method
