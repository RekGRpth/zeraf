.class public final Lb/Y;
.super Lb/h;
.source "SourceFile"


# instance fields
.field private final a:[B

.field private b:Lb/y;

.field private c:Lb/p;

.field private final d:J

.field private e:I


# direct methods
.method public constructor <init>(IJLb/p;)V
    .locals 3

    invoke-direct {p0, p1}, Lb/h;-><init>(I)V

    iput-wide p2, p0, Lb/Y;->d:J

    iput-object p4, p0, Lb/Y;->c:Lb/p;

    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lb/Y;->a:[B

    iget-object v0, p0, Lb/Y;->a:[B

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v1, v2}, Lb/Y;->a([BII)V

    return-void
.end method


# virtual methods
.method public final a(Lb/p;)V
    .locals 0

    iput-object p1, p0, Lb/Y;->c:Lb/p;

    return-void
.end method

.method protected final a()Z
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v7, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lb/Y;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lb/Y;->a:[B

    aget-byte v0, v0, v1

    if-gez v0, :cond_0

    and-int/lit16 v0, v0, 0xff

    :cond_0
    iget-wide v3, p0, Lb/Y;->d:J

    cmp-long v3, v3, v8

    if-ltz v3, :cond_1

    int-to-long v3, v0

    iget-wide v5, p0, Lb/Y;->d:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    invoke-virtual {p0}, Lb/Y;->d()V

    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v1, Lb/y;

    invoke-direct {v1, v0}, Lb/y;-><init>(I)V

    iput-object v1, p0, Lb/Y;->b:Lb/y;

    iget-object v0, p0, Lb/Y;->b:Lb/y;

    iget v1, p0, Lb/Y;->e:I

    invoke-virtual {v0, v1}, Lb/y;->a(I)V

    iget-object v0, p0, Lb/Y;->b:Lb/y;

    invoke-virtual {v0}, Lb/y;->f()[B

    move-result-object v0

    iget-object v1, p0, Lb/Y;->b:Lb/y;

    invoke-virtual {v1}, Lb/y;->g()I

    move-result v1

    invoke-virtual {p0, v0, v1, v7}, Lb/Y;->a([BII)V

    move v0, v2

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lb/Y;->a:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v3

    iget-wide v5, p0, Lb/Y;->d:J

    cmp-long v0, v5, v8

    if-ltz v0, :cond_2

    iget-wide v5, p0, Lb/Y;->d:J

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lb/Y;->d()V

    move v0, v1

    goto :goto_0

    :cond_2
    const-wide/32 v5, 0x7fffffff

    cmp-long v0, v3, v5

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lb/Y;->d()V

    move v0, v1

    goto :goto_0

    :cond_3
    new-instance v0, Lb/y;

    long-to-int v1, v3

    invoke-direct {v0, v1}, Lb/y;-><init>(I)V

    iput-object v0, p0, Lb/Y;->b:Lb/y;

    iget-object v0, p0, Lb/Y;->b:Lb/y;

    iget v1, p0, Lb/Y;->e:I

    invoke-virtual {v0, v1}, Lb/y;->a(I)V

    iget-object v0, p0, Lb/Y;->b:Lb/y;

    invoke-virtual {v0}, Lb/y;->f()[B

    move-result-object v0

    iget-object v1, p0, Lb/Y;->b:Lb/y;

    invoke-virtual {v1}, Lb/y;->g()I

    move-result v1

    invoke-virtual {p0, v0, v1, v7}, Lb/Y;->a([BII)V

    move v0, v2

    goto :goto_0

    :pswitch_2
    iput v1, p0, Lb/Y;->e:I

    iget-object v0, p0, Lb/Y;->a:[B

    aget-byte v0, v0, v1

    and-int/lit8 v3, v0, 0x1

    if-lez v3, :cond_4

    iget v3, p0, Lb/Y;->e:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lb/Y;->e:I

    :cond_4
    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_5

    iget-object v0, p0, Lb/Y;->a:[B

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1, v2}, Lb/Y;->a([BII)V

    :goto_1
    move v0, v2

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lb/Y;->a:[B

    invoke-virtual {p0, v0, v2, v1}, Lb/Y;->a([BII)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lb/Y;->c:Lb/p;

    if-nez v0, :cond_6

    move v0, v1

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lb/Y;->c:Lb/p;

    iget-object v3, p0, Lb/Y;->b:Lb/y;

    invoke-interface {v0, v3}, Lb/p;->a(Lb/y;)Z

    move-result v0

    if-nez v0, :cond_8

    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->b(I)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lb/Y;->d()V

    :cond_7
    move v0, v1

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lb/Y;->a:[B

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v2, v1}, Lb/Y;->a([BII)V

    move v0, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
