.class public Lb/N;
.super Lb/Q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/N$a;,
        Lb/N$b;
    }
.end annotation


# static fields
.field private static synthetic n:Z


# instance fields
.field private final b:Lb/l;

.field private c:Z

.field private d:Z

.field private e:Lb/y;

.field private f:Lb/y;

.field private g:Z

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lb/E;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lb/b;",
            "Lb/N$a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lb/E;

.field private k:Z

.field private l:I

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/N;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/N;->n:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;II)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lb/Q;-><init>(Lb/e;II)V

    iput-boolean v1, p0, Lb/N;->c:Z

    iput-boolean v1, p0, Lb/N;->d:Z

    iput-boolean v1, p0, Lb/N;->g:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lb/N;->j:Lb/E;

    iput-boolean v1, p0, Lb/N;->k:Z

    invoke-static {}, Lb/X;->a()I

    move-result v0

    iput v0, p0, Lb/N;->l:I

    iput-boolean v1, p0, Lb/N;->m:Z

    iget-object v0, p0, Lb/N;->a:Lb/B;

    const/4 v1, 0x6

    iput v1, v0, Lb/B;->l:I

    new-instance v0, Lb/l;

    invoke-direct {v0}, Lb/l;-><init>()V

    iput-object v0, p0, Lb/N;->b:Lb/l;

    new-instance v0, Lb/y;

    invoke-direct {v0}, Lb/y;-><init>()V

    iput-object v0, p0, Lb/N;->e:Lb/y;

    new-instance v0, Lb/y;

    invoke-direct {v0}, Lb/y;-><init>()V

    iput-object v0, p0, Lb/N;->f:Lb/y;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lb/N;->h:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lb/N;->i:Ljava/util/Map;

    iget-object v0, p0, Lb/N;->a:Lb/B;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lb/B;->y:Z

    return-void
.end method

.method private m(Lb/E;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lb/E;->c()Lb/y;

    move-result-object v3

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v3}, Lb/y;->g()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    iget v0, p0, Lb/N;->l:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lb/N;->l:I

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    new-instance v0, Lb/b;

    invoke-direct {v0, v3}, Lb/b;-><init>(Ljava/nio/ByteBuffer;)V

    :cond_1
    invoke-virtual {p1, v0}, Lb/E;->a(Lb/b;)V

    new-instance v1, Lb/N$a;

    invoke-direct {v1, p0, p1, v2}, Lb/N$a;-><init>(Lb/N;Lb/E;Z)V

    iget-object v3, p0, Lb/N;->i:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    goto :goto_0

    :cond_2
    new-instance v0, Lb/b;

    invoke-virtual {v3}, Lb/y;->f()[B

    move-result-object v3

    invoke-direct {v0, v3}, Lb/b;-><init>([B)V

    iget-object v3, p0, Lb/N;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    iget-object v0, p0, Lb/N;->j:Lb/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/N;->j:Lb/E;

    invoke-virtual {v0}, Lb/E;->e()V

    const/4 v0, 0x0

    iput-object v0, p0, Lb/N;->j:Lb/E;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/N;->k:Z

    :cond_0
    return-void
.end method

.method public final a(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/N;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/N;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->c(Lb/E;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lb/N;->m(Lb/E;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/N;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lb/N;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->a(Lb/E;)V

    goto :goto_0
.end method

.method public final a(Lb/E;Z)V
    .locals 1

    sget-boolean v0, Lb/N;->n:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Lb/N;->m(Lb/E;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/N;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->a(Lb/E;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lb/N;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/16 v2, 0x21

    if-eq p1, v2, :cond_0

    const/16 v1, 0x16

    invoke-static {v1}, Lb/ae;->a(I)V

    :goto_0
    return v0

    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v1, :cond_1

    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lb/N;->m:Z

    move v0, v1

    goto :goto_0
.end method

.method protected a(Lb/y;I)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lb/N;->k:Z

    if-nez v0, :cond_3

    sget-boolean v0, Lb/N;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/N;->j:Lb/E;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lb/N;->k:Z

    new-instance v0, Lb/b;

    invoke-virtual {p1}, Lb/y;->f()[B

    move-result-object v3

    invoke-direct {v0, v3}, Lb/b;-><init>([B)V

    iget-object v3, p0, Lb/N;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/N$a;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lb/N$a;->a(Lb/N$a;)Lb/E;

    move-result-object v3

    iput-object v3, p0, Lb/N;->j:Lb/E;

    iget-object v3, p0, Lb/N;->j:Lb/E;

    invoke-virtual {v3}, Lb/E;->d()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0, v1}, Lb/N$a;->a(Lb/N$a;Z)V

    iput-object v4, p0, Lb/N;->j:Lb/E;

    iget-boolean v0, p0, Lb/N;->m:Z

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lb/N;->k:Z

    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->a(I)V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lb/N;->m:Z

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lb/N;->k:Z

    const/16 v0, 0x41

    invoke-static {v0}, Lb/ae;->a(I)V

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v0

    iput-boolean v0, p0, Lb/N;->k:Z

    iget-object v0, p0, Lb/N;->j:Lb/E;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lb/N;->j:Lb/E;

    invoke-virtual {v0, p1}, Lb/E;->a(Lb/y;)Z

    move-result v0

    if-nez v0, :cond_5

    iput-object v4, p0, Lb/N;->j:Lb/E;

    :cond_4
    :goto_1
    move v0, v2

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lb/N;->k:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lb/N;->j:Lb/E;

    invoke-virtual {v0}, Lb/E;->f()V

    iput-object v4, p0, Lb/N;->j:Lb/E;

    goto :goto_1
.end method

.method protected a_(I)Lb/y;
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-boolean v0, p0, Lb/N;->c:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lb/N;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/N;->e:Lb/y;

    iput-object v1, p0, Lb/N;->e:Lb/y;

    iput-boolean v4, p0, Lb/N;->d:Z

    :goto_0
    invoke-virtual {v0}, Lb/y;->d()Z

    move-result v1

    iput-boolean v1, p0, Lb/N;->g:Z

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lb/N;->f:Lb/y;

    iput-object v1, p0, Lb/N;->f:Lb/y;

    iput-boolean v5, p0, Lb/N;->c:Z

    goto :goto_0

    :cond_1
    new-array v2, v4, [Lb/E;

    :cond_2
    iget-object v0, p0, Lb/N;->b:Lb/l;

    invoke-virtual {v0, v2}, Lb/l;->a([Lb/E;)Lb/y;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lb/y;->a()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_3
    if-nez v0, :cond_4

    move-object v0, v1

    goto :goto_1

    :cond_4
    sget-boolean v1, Lb/N;->n:Z

    if-nez v1, :cond_5

    aget-object v1, v2, v5

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    iget-boolean v1, p0, Lb/N;->g:Z

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lb/y;->d()Z

    move-result v1

    iput-boolean v1, p0, Lb/N;->g:Z

    goto :goto_1

    :cond_6
    iput-object v0, p0, Lb/N;->f:Lb/y;

    iput-boolean v4, p0, Lb/N;->c:Z

    aget-object v0, v2, v5

    invoke-virtual {v0}, Lb/E;->a()Lb/b;

    move-result-object v1

    new-instance v0, Lb/y;

    invoke-virtual {v1}, Lb/b;->a()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lb/y;-><init>([B)V

    invoke-virtual {v0, v4}, Lb/y;->a(I)V

    iput-boolean v4, p0, Lb/N;->d:Z

    goto :goto_1
.end method

.method public final b(Lb/E;)V
    .locals 3

    iget-object v0, p0, Lb/N;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    sget-boolean v0, Lb/N;->n:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb/N$a;

    invoke-static {v1}, Lb/N$a;->a(Lb/N$a;)Lb/E;

    move-result-object v1

    if-ne v1, p1, :cond_0

    sget-boolean v1, Lb/N;->n:Z

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb/N$a;

    invoke-static {v1}, Lb/N$a;->b(Lb/N$a;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/N$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lb/N$a;->a(Lb/N$a;Z)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final c(Lb/E;)V
    .locals 2

    iget-object v0, p0, Lb/N;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/N;->i:Ljava/util/Map;

    invoke-virtual {p1}, Lb/E;->a()Lb/b;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/N$a;

    sget-boolean v1, Lb/N;->n:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/N;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->b(Lb/E;)V

    iget-object v0, p0, Lb/N;->j:Lb/E;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lb/N;->j:Lb/E;

    :cond_1
    return-void
.end method
