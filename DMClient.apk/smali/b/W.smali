.class public Lb/W;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/W$a;
    }
.end annotation


# static fields
.field private static synthetic f:Z


# instance fields
.field private a:I

.field private b:B

.field private c:S

.field private d:S

.field private e:[Lb/W;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/W;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/W;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte v0, p0, Lb/W;->b:B

    iput-short v0, p0, Lb/W;->c:S

    iput-short v0, p0, Lb/W;->d:S

    iput v0, p0, Lb/W;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lb/W;->e:[Lb/W;

    return-void
.end method

.method private a([BIILb/W$a;Ljava/lang/Object;)V
    .locals 7

    const/4 v0, 0x0

    move-object v4, p4

    :goto_0
    iget v1, p0, Lb/W;->a:I

    if-lez v1, :cond_0

    invoke-virtual {v4, p1, p2, p5}, Lb/W$a;->a([BILjava/lang/Object;)V

    :cond_0
    if-lt p2, p3, :cond_1

    add-int/lit16 v3, p2, 0x100

    invoke-static {p1, v3}, Lb/X;->a([BI)[B

    move-result-object v1

    sget-boolean v2, Lb/W;->f:Z

    if-nez v2, :cond_2

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    move v3, p3

    move-object v1, p1

    :cond_2
    iget-short v2, p0, Lb/W;->c:S

    if-nez v2, :cond_4

    :cond_3
    return-void

    :cond_4
    iget-short v2, p0, Lb/W;->c:S

    const/4 v5, 0x1

    if-ne v2, v5, :cond_5

    iget-byte v2, p0, Lb/W;->b:B

    aput-byte v2, v1, p2

    add-int/lit8 p2, p2, 0x1

    iget-object v2, p0, Lb/W;->e:[Lb/W;

    aget-object p0, v2, v0

    move p3, v3

    move-object p1, v1

    goto :goto_0

    :cond_5
    move v6, v0

    :goto_1
    iget-short v0, p0, Lb/W;->c:S

    if-eq v6, v0, :cond_3

    iget-byte v0, p0, Lb/W;->b:B

    add-int/2addr v0, v6

    int-to-byte v0, v0

    aput-byte v0, v1, p2

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    aget-object v0, v0, v6

    if-eqz v0, :cond_6

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    aget-object v0, v0, v6

    add-int/lit8 v2, p2, 0x1

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lb/W;->a([BIILb/W$a;Ljava/lang/Object;)V

    :cond_6
    add-int/lit8 v0, v6, 0x1

    int-to-short v0, v0

    move v6, v0

    goto :goto_1
.end method

.method private static a([Lb/W;SZ)[Lb/W;
    .locals 1

    const-class v0, Lb/W;

    invoke-static {v0, p0, p1, p2}, Lb/X;->a(Ljava/lang/Class;[Ljava/lang/Object;IZ)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lb/W;

    return-object v0
.end method


# virtual methods
.method public final a(Lb/W$a;Ljava/lang/Object;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    move-object v0, p0

    move v3, v2

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lb/W;->a([BIILb/W$a;Ljava/lang/Object;)V

    return-void
.end method

.method public final a([B)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget v3, p0, Lb/W;->a:I

    if-lez v3, :cond_1

    move v1, v2

    :cond_0
    :goto_1
    return v1

    :cond_1
    array-length v3, p1

    if-eq v3, v0, :cond_0

    aget-byte v3, p1, v0

    iget-byte v4, p0, Lb/W;->b:B

    if-lt v3, v4, :cond_0

    iget-byte v4, p0, Lb/W;->b:B

    iget-short v5, p0, Lb/W;->c:S

    add-int/2addr v4, v5

    if-ge v3, v4, :cond_0

    iget-short v4, p0, Lb/W;->c:S

    if-ne v4, v2, :cond_3

    iget-object v3, p0, Lb/W;->e:[Lb/W;

    aget-object p0, v3, v1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lb/W;->e:[Lb/W;

    iget-byte v5, p0, Lb/W;->b:B

    sub-int/2addr v3, v5

    aget-object p0, v4, v3

    if-nez p0, :cond_2

    goto :goto_1
.end method

.method public final a([BI)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    :goto_0
    if-eqz p1, :cond_0

    array-length v0, p1

    if-ne v0, p2, :cond_2

    :cond_0
    iget v0, p0, Lb/W;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/W;->a:I

    iget v0, p0, Lb/W;->a:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    aget-byte v3, p1, p2

    iget-byte v0, p0, Lb/W;->b:B

    if-lt v3, v0, :cond_3

    iget-byte v0, p0, Lb/W;->b:B

    iget-short v4, p0, Lb/W;->c:S

    add-int/2addr v0, v4

    if-lt v3, v0, :cond_4

    :cond_3
    iget-short v0, p0, Lb/W;->c:S

    if-nez v0, :cond_6

    iput-byte v3, p0, Lb/W;->b:B

    iput-short v1, p0, Lb/W;->c:S

    const/4 v0, 0x0

    iput-object v0, p0, Lb/W;->e:[Lb/W;

    :cond_4
    :goto_2
    iget-short v0, p0, Lb/W;->c:S

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    if-nez v0, :cond_5

    new-array v0, v1, [Lb/W;

    iput-object v0, p0, Lb/W;->e:[Lb/W;

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    new-instance v3, Lb/W;

    invoke-direct {v3}, Lb/W;-><init>()V

    aput-object v3, v0, v2

    iget-short v0, p0, Lb/W;->d:S

    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    iput-short v0, p0, Lb/W;->d:S

    :cond_5
    iget-object v0, p0, Lb/W;->e:[Lb/W;

    aget-object p0, v0, v2

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_6
    iget-short v0, p0, Lb/W;->c:S

    if-ne v0, v1, :cond_8

    iget-byte v4, p0, Lb/W;->b:B

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    aget-object v5, v0, v2

    iget-byte v0, p0, Lb/W;->b:B

    if-ge v0, v3, :cond_7

    iget-byte v0, p0, Lb/W;->b:B

    sub-int v0, v3, v0

    :goto_3
    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    iput-short v0, p0, Lb/W;->c:S

    iget-short v0, p0, Lb/W;->c:S

    new-array v0, v0, [Lb/W;

    iput-object v0, p0, Lb/W;->e:[Lb/W;

    iget-byte v0, p0, Lb/W;->b:B

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lb/W;->b:B

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-byte v6, p0, Lb/W;->b:B

    sub-int/2addr v4, v6

    aput-object v5, v0, v4

    goto :goto_2

    :cond_7
    iget-byte v0, p0, Lb/W;->b:B

    sub-int/2addr v0, v3

    goto :goto_3

    :cond_8
    iget-byte v0, p0, Lb/W;->b:B

    if-ge v0, v3, :cond_9

    iget-byte v0, p0, Lb/W;->b:B

    sub-int v0, v3, v0

    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    iput-short v0, p0, Lb/W;->c:S

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-short v4, p0, Lb/W;->c:S

    invoke-static {v0, v4, v1}, Lb/W;->a([Lb/W;SZ)[Lb/W;

    move-result-object v0

    iput-object v0, p0, Lb/W;->e:[Lb/W;

    goto :goto_2

    :cond_9
    iget-byte v0, p0, Lb/W;->b:B

    iget-short v4, p0, Lb/W;->c:S

    add-int/2addr v0, v4

    sub-int/2addr v0, v3

    int-to-short v0, v0

    iput-short v0, p0, Lb/W;->c:S

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-short v4, p0, Lb/W;->c:S

    invoke-static {v0, v4, v2}, Lb/W;->a([Lb/W;SZ)[Lb/W;

    move-result-object v0

    iput-object v0, p0, Lb/W;->e:[Lb/W;

    iput-byte v3, p0, Lb/W;->b:B

    goto/16 :goto_2

    :cond_a
    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-byte v4, p0, Lb/W;->b:B

    sub-int v4, v3, v4

    aget-object v0, v0, v4

    if-nez v0, :cond_b

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-byte v4, p0, Lb/W;->b:B

    sub-int v4, v3, v4

    new-instance v5, Lb/W;

    invoke-direct {v5}, Lb/W;-><init>()V

    aput-object v5, v0, v4

    iget-short v0, p0, Lb/W;->d:S

    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    iput-short v0, p0, Lb/W;->d:S

    :cond_b
    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-byte v4, p0, Lb/W;->b:B

    sub-int/2addr v3, v4

    aget-object p0, v0, v3

    add-int/lit8 p2, p2, 0x1

    goto/16 :goto_0
.end method

.method public final b([BI)Z
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    array-length v0, p1

    if-ne v0, p2, :cond_3

    :cond_0
    iget v0, p0, Lb/W;->a:I

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    iget v0, p0, Lb/W;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/W;->a:I

    iget v0, p0, Lb/W;->a:I

    if-nez v0, :cond_1

    move v1, v4

    goto :goto_0

    :cond_3
    aget-byte v2, p1, p2

    iget-short v0, p0, Lb/W;->c:S

    if-eqz v0, :cond_1

    iget-byte v0, p0, Lb/W;->b:B

    if-lt v2, v0, :cond_1

    iget-byte v0, p0, Lb/W;->b:B

    iget-short v5, p0, Lb/W;->c:S

    add-int/2addr v0, v5

    if-ge v2, v0, :cond_1

    iget-short v0, p0, Lb/W;->c:S

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    aget-object v0, v0, v1

    :goto_1
    if-eqz v0, :cond_1

    add-int/lit8 v5, p2, 0x1

    invoke-virtual {v0, p1, v5}, Lb/W;->b([BI)Z

    move-result v5

    iget v6, v0, Lb/W;->a:I

    if-nez v6, :cond_5

    iget-short v0, v0, Lb/W;->d:S

    if-nez v0, :cond_5

    move v0, v4

    :goto_2
    if-eqz v0, :cond_c

    sget-boolean v0, Lb/W;->f:Z

    if-nez v0, :cond_6

    iget-short v0, p0, Lb/W;->c:S

    if-gtz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-byte v5, p0, Lb/W;->b:B

    sub-int v5, v2, v5

    aget-object v0, v0, v5

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    iget-short v0, p0, Lb/W;->c:S

    if-ne v0, v4, :cond_7

    iput-object v3, p0, Lb/W;->e:[Lb/W;

    iput-short v1, p0, Lb/W;->c:S

    iget-short v0, p0, Lb/W;->d:S

    add-int/lit8 v0, v0, -0x1

    int-to-short v0, v0

    iput-short v0, p0, Lb/W;->d:S

    sget-boolean v0, Lb/W;->f:Z

    if-nez v0, :cond_c

    iget-short v0, p0, Lb/W;->d:S

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_7
    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-byte v6, p0, Lb/W;->b:B

    sub-int v6, v2, v6

    aput-object v3, v0, v6

    sget-boolean v0, Lb/W;->f:Z

    if-nez v0, :cond_8

    iget-short v0, p0, Lb/W;->d:S

    if-gt v0, v4, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    iget-short v0, p0, Lb/W;->d:S

    add-int/lit8 v0, v0, -0x1

    int-to-short v0, v0

    iput-short v0, p0, Lb/W;->d:S

    iget-short v0, p0, Lb/W;->d:S

    if-ne v0, v4, :cond_d

    move v0, v1

    :goto_3
    iget-short v2, p0, Lb/W;->c:S

    if-lt v0, v2, :cond_9

    move-object v0, v3

    :goto_4
    sget-boolean v2, Lb/W;->f:Z

    if-nez v2, :cond_b

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_9
    iget-object v2, p0, Lb/W;->e:[Lb/W;

    aget-object v2, v2, v0

    if-eqz v2, :cond_a

    iget-object v2, p0, Lb/W;->e:[Lb/W;

    aget-object v2, v2, v0

    iget-byte v6, p0, Lb/W;->b:B

    add-int/2addr v0, v6

    int-to-byte v0, v0

    iput-byte v0, p0, Lb/W;->b:B

    move-object v0, v2

    goto :goto_4

    :cond_a
    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    goto :goto_3

    :cond_b
    iput-object v3, p0, Lb/W;->e:[Lb/W;

    new-array v2, v4, [Lb/W;

    aput-object v0, v2, v1

    iput-object v2, p0, Lb/W;->e:[Lb/W;

    iput-short v4, p0, Lb/W;->c:S

    :cond_c
    :goto_5
    move v1, v5

    goto/16 :goto_0

    :cond_d
    iget-byte v0, p0, Lb/W;->b:B

    if-ne v2, v0, :cond_13

    iget-byte v1, p0, Lb/W;->b:B

    move v0, v4

    :goto_6
    iget-short v2, p0, Lb/W;->c:S

    if-lt v0, v2, :cond_e

    move v0, v1

    :goto_7
    sget-boolean v1, Lb/W;->f:Z

    if-nez v1, :cond_10

    iget-byte v1, p0, Lb/W;->b:B

    if-ne v0, v1, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_e
    iget-object v2, p0, Lb/W;->e:[Lb/W;

    aget-object v2, v2, v0

    if-eqz v2, :cond_f

    iget-byte v1, p0, Lb/W;->b:B

    add-int/2addr v0, v1

    int-to-byte v0, v0

    goto :goto_7

    :cond_f
    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    goto :goto_6

    :cond_10
    sget-boolean v1, Lb/W;->f:Z

    if-nez v1, :cond_11

    iget-byte v1, p0, Lb/W;->b:B

    if-gt v0, v1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_11
    sget-boolean v1, Lb/W;->f:Z

    if-nez v1, :cond_12

    iget-short v1, p0, Lb/W;->c:S

    iget-byte v2, p0, Lb/W;->b:B

    sub-int v2, v0, v2

    if-gt v1, v2, :cond_12

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_12
    iget-short v1, p0, Lb/W;->c:S

    iget-byte v2, p0, Lb/W;->b:B

    sub-int v2, v0, v2

    sub-int/2addr v1, v2

    int-to-short v1, v1

    iput-short v1, p0, Lb/W;->c:S

    iget-object v1, p0, Lb/W;->e:[Lb/W;

    iget-short v2, p0, Lb/W;->c:S

    invoke-static {v1, v2, v4}, Lb/W;->a([Lb/W;SZ)[Lb/W;

    move-result-object v1

    iput-object v1, p0, Lb/W;->e:[Lb/W;

    iput-byte v0, p0, Lb/W;->b:B

    goto :goto_5

    :cond_13
    iget-byte v0, p0, Lb/W;->b:B

    iget-short v3, p0, Lb/W;->c:S

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, -0x1

    if-ne v2, v0, :cond_c

    iget-short v0, p0, Lb/W;->c:S

    :goto_8
    iget-short v2, p0, Lb/W;->c:S

    if-lt v4, v2, :cond_14

    :goto_9
    sget-boolean v2, Lb/W;->f:Z

    if-nez v2, :cond_16

    iget-short v2, p0, Lb/W;->c:S

    if-ne v0, v2, :cond_16

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_14
    iget-object v2, p0, Lb/W;->e:[Lb/W;

    iget-short v3, p0, Lb/W;->c:S

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v4

    aget-object v2, v2, v3

    if-eqz v2, :cond_15

    iget-short v0, p0, Lb/W;->c:S

    sub-int/2addr v0, v4

    int-to-short v0, v0

    goto :goto_9

    :cond_15
    add-int/lit8 v2, v4, 0x1

    int-to-short v4, v2

    goto :goto_8

    :cond_16
    iput-short v0, p0, Lb/W;->c:S

    iget-object v0, p0, Lb/W;->e:[Lb/W;

    iget-short v2, p0, Lb/W;->c:S

    invoke-static {v0, v2, v1}, Lb/W;->a([Lb/W;SZ)[Lb/W;

    move-result-object v0

    iput-object v0, p0, Lb/W;->e:[Lb/W;

    goto/16 :goto_5
.end method
