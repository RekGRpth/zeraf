.class public Lb/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:B

.field private b:I

.field private c:I

.field private d:[B

.field private e:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lb/y;->b(B)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lb/y;->b(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lb/y;->b(B)V

    invoke-direct {p0, p1}, Lb/y;->b(I)V

    return-void
.end method

.method public constructor <init>(Lb/y;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-byte v0, p1, Lb/y;->a:B

    iput-byte v0, p0, Lb/y;->a:B

    iget v0, p1, Lb/y;->b:I

    iput v0, p0, Lb/y;->b:I

    iget v0, p1, Lb/y;->c:I

    iput v0, p0, Lb/y;->c:I

    iget-object v0, p1, Lb/y;->e:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lb/y;->e:Ljava/nio/ByteBuffer;

    iget-object v0, p1, Lb/y;->d:[B

    iput-object v0, p0, Lb/y;->d:[B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lb/y;-><init>([BZ)V

    return-void
.end method

.method private constructor <init>([BZ)V
    .locals 1

    invoke-direct {p0}, Lb/y;-><init>()V

    if-eqz p1, :cond_0

    array-length v0, p1

    iput v0, p0, Lb/y;->c:I

    iput-object p1, p0, Lb/y;->d:[B

    :cond_0
    return-void
.end method

.method private final b(B)V
    .locals 2

    const/4 v1, 0x0

    iput-byte p1, p0, Lb/y;->a:B

    const/4 v0, 0x0

    iput v0, p0, Lb/y;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lb/y;->c:I

    iput-object v1, p0, Lb/y;->d:[B

    iput-object v1, p0, Lb/y;->e:Ljava/nio/ByteBuffer;

    return-void
.end method

.method private final b(I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iput p1, p0, Lb/y;->c:I

    iget-byte v0, p0, Lb/y;->a:B

    const/16 v1, 0x67

    if-ne v0, v1, :cond_0

    iput v2, p0, Lb/y;->b:I

    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lb/y;->e:Ljava/nio/ByteBuffer;

    iput-object v3, p0, Lb/y;->d:[B

    :goto_0
    return-void

    :cond_0
    iput v2, p0, Lb/y;->b:I

    new-array v0, p1, [B

    iput-object v0, p0, Lb/y;->d:[B

    iput-object v3, p0, Lb/y;->e:Ljava/nio/ByteBuffer;

    goto :goto_0
.end method


# virtual methods
.method public final a(B)V
    .locals 2

    iget-object v0, p0, Lb/y;->d:[B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget v0, p0, Lb/y;->b:I

    or-int/2addr v0, p1

    iput v0, p0, Lb/y;->b:I

    return-void
.end method

.method public final a([BI)V
    .locals 4

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lb/y;->d:[B

    const/4 v2, 0x1

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final a([BII)V
    .locals 2

    if-eqz p3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lb/y;->d:[B

    invoke-static {p1, v0, v1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    iget v0, p0, Lb/y;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    iget-byte v0, p0, Lb/y;->a:B

    const/16 v1, 0x68

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lb/y;->b:I

    return v0
.end method

.method public final d()Z
    .locals 1

    iget v0, p0, Lb/y;->b:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    const/16 v0, 0x68

    iput-byte v0, p0, Lb/y;->a:B

    const/4 v0, 0x0

    iput v0, p0, Lb/y;->b:I

    return-void
.end method

.method public final f()[B
    .locals 5

    iget-object v0, p0, Lb/y;->d:[B

    if-nez v0, :cond_0

    iget-byte v0, p0, Lb/y;->a:B

    const/16 v1, 0x67

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lb/y;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/y;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iput-object v0, p0, Lb/y;->d:[B

    :cond_0
    :goto_0
    iget-object v0, p0, Lb/y;->d:[B

    return-object v0

    :cond_1
    iget v0, p0, Lb/y;->c:I

    new-array v0, v0, [B

    iput-object v0, p0, Lb/y;->d:[B

    iget-object v0, p0, Lb/y;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iget-object v1, p0, Lb/y;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    iget-object v2, p0, Lb/y;->d:[B

    const/4 v3, 0x0

    iget v4, p0, Lb/y;->c:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lb/y;->c:I

    return v0
.end method

.method public final h()V
    .locals 2

    iget-byte v0, p0, Lb/y;->a:B

    const/16 v1, 0x65

    if-lt v0, v1, :cond_0

    iget-byte v0, p0, Lb/y;->a:B

    const/16 v1, 0x69

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lb/y;->b(B)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lb/y;->a:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lb/y;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lb/y;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
