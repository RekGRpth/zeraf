.class public Lb/D;
.super Lb/Q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/D$a;
    }
.end annotation


# static fields
.field private static synthetic c:Z


# instance fields
.field private b:Lb/E;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/D;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/D;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lb/e;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lb/Q;-><init>(Lb/e;II)V

    iget-object v0, p0, Lb/D;->a:Lb/B;

    const/4 v1, 0x0

    iput v1, v0, Lb/B;->l:I

    return-void
.end method


# virtual methods
.method protected final a(Lb/E;)V
    .locals 0

    return-void
.end method

.method protected final a(Lb/E;Z)V
    .locals 1

    sget-boolean v0, Lb/D;->c:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/D;->b:Lb/E;

    if-nez v0, :cond_1

    iput-object p1, p0, Lb/D;->b:Lb/E;

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lb/E;->a(Z)V

    goto :goto_0
.end method

.method protected final a(Lb/y;I)Z
    .locals 1

    iget-object v0, p0, Lb/D;->b:Lb/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/D;->b:Lb/E;

    invoke-virtual {v0, p1}, Lb/E;->a(Lb/y;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/D;->b:Lb/E;

    invoke-virtual {v0}, Lb/E;->f()V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final a_(I)Lb/y;
    .locals 1

    iget-object v0, p0, Lb/D;->b:Lb/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/D;->b:Lb/E;

    invoke-virtual {v0}, Lb/E;->c()Lb/y;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method protected final b(Lb/E;)V
    .locals 0

    return-void
.end method

.method protected final c(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/D;->b:Lb/E;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lb/D;->b:Lb/E;

    :cond_0
    return-void
.end method
