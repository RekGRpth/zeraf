.class public abstract Lb/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static synthetic c:[I


# instance fields
.field private final a:Lb/e;

.field private final b:I


# direct methods
.method protected constructor <init>(Lb/ag;)V
    .locals 2

    iget-object v0, p1, Lb/ag;->a:Lb/e;

    iget v1, p1, Lb/ag;->b:I

    invoke-direct {p0, v0, v1}, Lb/ag;-><init>(Lb/e;I)V

    return-void
.end method

.method protected constructor <init>(Lb/e;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb/ag;->a:Lb/e;

    iput p2, p0, Lb/ag;->b:I

    return-void
.end method

.method private static synthetic a()[I
    .locals 3

    sget-object v0, Lb/ag;->c:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lb/c$a;->values()[Lb/c$a;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lb/c$a;->f:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_1
    :try_start_1
    sget-object v1, Lb/c$a;->g:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_2
    :try_start_2
    sget-object v1, Lb/c$a;->d:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_3
    :try_start_3
    sget-object v1, Lb/c$a;->e:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_4
    :try_start_4
    sget-object v1, Lb/c$a;->p:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_5
    :try_start_5
    sget-object v1, Lb/c$a;->h:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_6
    :try_start_6
    sget-object v1, Lb/c$a;->c:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_7
    :try_start_7
    sget-object v1, Lb/c$a;->i:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    :goto_8
    :try_start_8
    sget-object v1, Lb/c$a;->j:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_9
    :try_start_9
    sget-object v1, Lb/c$a;->b:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_a
    :try_start_a
    sget-object v1, Lb/c$a;->n:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_b
    :try_start_b
    sget-object v1, Lb/c$a;->o:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_c
    :try_start_c
    sget-object v1, Lb/c$a;->a:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_d
    :try_start_d
    sget-object v1, Lb/c$a;->l:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_e
    :try_start_e
    sget-object v1, Lb/c$a;->m:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_f
    :try_start_f
    sget-object v1, Lb/c$a;->k:Lb/c$a;

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_10
    sput-object v0, Lb/ag;->c:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_f

    :catch_2
    move-exception v1

    goto :goto_e

    :catch_3
    move-exception v1

    goto :goto_d

    :catch_4
    move-exception v1

    goto :goto_c

    :catch_5
    move-exception v1

    goto :goto_b

    :catch_6
    move-exception v1

    goto :goto_a

    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_8

    :catch_9
    move-exception v1

    goto :goto_7

    :catch_a
    move-exception v1

    goto :goto_6

    :catch_b
    move-exception v1

    goto/16 :goto_5

    :catch_c
    move-exception v1

    goto/16 :goto_4

    :catch_d
    move-exception v1

    goto/16 :goto_3

    :catch_e
    move-exception v1

    goto/16 :goto_2

    :catch_f
    move-exception v1

    goto/16 :goto_1
.end method

.method private b(Lb/c;)V
    .locals 2

    iget-object v0, p0, Lb/ag;->a:Lb/e;

    invoke-virtual {p1}, Lb/c;->a()Lb/ag;

    move-result-object v1

    iget v1, v1, Lb/ag;->b:I

    invoke-virtual {v0, v1, p1}, Lb/e;->a(ILb/c;)V

    return-void
.end method


# virtual methods
.method protected a(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final a(Lb/C;I)V
    .locals 3

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->l:Lb/c$a;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lb/c;-><init>(Lb/ag;Lb/c$a;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final a(Lb/C;Lb/C;)V
    .locals 2

    invoke-virtual {p1}, Lb/C;->h()V

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->c:Lb/c$a;

    invoke-direct {v0, p1, v1, p2}, Lb/c;-><init>(Lb/ag;Lb/c$a;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final a(Lb/C;Lb/E;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lb/ag;->a(Lb/C;Lb/E;Z)V

    return-void
.end method

.method protected final a(Lb/C;Lb/E;Z)V
    .locals 2

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lb/C;->h()V

    :cond_0
    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->e:Lb/c$a;

    invoke-direct {v0, p1, v1, p2}, Lb/c;-><init>(Lb/ag;Lb/c$a;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final a(Lb/E;J)V
    .locals 3

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->g:Lb/c$a;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lb/c;-><init>(Lb/ag;Lb/c$a;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final a(Lb/E;Ljava/lang/Object;)V
    .locals 2

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->h:Lb/c$a;

    invoke-direct {v0, p1, v1, p2}, Lb/c;-><init>(Lb/ag;Lb/c$a;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final a(Lb/O;Lb/o;)V
    .locals 2

    invoke-virtual {p1}, Lb/O;->h()V

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->d:Lb/c$a;

    invoke-direct {v0, p1, v1, p2}, Lb/c;-><init>(Lb/ag;Lb/c$a;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected a(Lb/Q;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final a(Lb/c;)V
    .locals 2

    invoke-static {}, Lb/ag;->a()[I

    move-result-object v0

    invoke-virtual {p1}, Lb/c;->b()Lb/c$a;

    move-result-object v1

    invoke-virtual {v1}, Lb/c$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    invoke-virtual {p0}, Lb/ag;->g()V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p1, Lb/c;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lb/ag;->a(J)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lb/ag;->k()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lb/ag;->s()V

    invoke-virtual {p0}, Lb/ag;->f_()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p1, Lb/c;->a:Ljava/lang/Object;

    check-cast v0, Lb/C;

    invoke-virtual {p0, v0}, Lb/ag;->c(Lb/C;)V

    invoke-virtual {p0}, Lb/ag;->f_()V

    goto :goto_0

    :pswitch_5
    iget-object v0, p1, Lb/c;->a:Ljava/lang/Object;

    check-cast v0, Lb/o;

    invoke-virtual {p0, v0}, Lb/ag;->a(Lb/o;)V

    invoke-virtual {p0}, Lb/ag;->f_()V

    goto :goto_0

    :pswitch_6
    iget-object v0, p1, Lb/c;->a:Ljava/lang/Object;

    check-cast v0, Lb/E;

    invoke-virtual {p0, v0}, Lb/ag;->h(Lb/E;)V

    invoke-virtual {p0}, Lb/ag;->f_()V

    goto :goto_0

    :pswitch_7
    iget-object v0, p1, Lb/c;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lb/ag;->a(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0}, Lb/ag;->c_()V

    goto :goto_0

    :pswitch_9
    invoke-virtual {p0}, Lb/ag;->e_()V

    goto :goto_0

    :pswitch_a
    iget-object v0, p1, Lb/c;->a:Ljava/lang/Object;

    check-cast v0, Lb/C;

    invoke-virtual {p0, v0}, Lb/ag;->b(Lb/C;)V

    goto :goto_0

    :pswitch_b
    iget-object v0, p1, Lb/c;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lb/ag;->b(I)V

    goto :goto_0

    :pswitch_c
    invoke-virtual {p0}, Lb/ag;->n()V

    goto :goto_0

    :pswitch_d
    iget-object v0, p1, Lb/c;->a:Ljava/lang/Object;

    check-cast v0, Lb/Q;

    invoke-virtual {p0, v0}, Lb/ag;->a(Lb/Q;)V

    goto :goto_0

    :pswitch_e
    invoke-virtual {p0}, Lb/ag;->o()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method protected a(Lb/o;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final b(Ljava/lang/String;)Lb/e$a;
    .locals 1

    iget-object v0, p0, Lb/ag;->a:Lb/e;

    invoke-virtual {v0, p1}, Lb/e;->a(Ljava/lang/String;)Lb/e$a;

    move-result-object v0

    return-object v0
.end method

.method protected final b(J)Lb/s;
    .locals 1

    iget-object v0, p0, Lb/ag;->a:Lb/e;

    invoke-virtual {v0, p1, p2}, Lb/e;->a(J)Lb/s;

    move-result-object v0

    return-object v0
.end method

.method protected b(I)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected b(Lb/C;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final b(Lb/C;Lb/C;)V
    .locals 2

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->k:Lb/c$a;

    invoke-direct {v0, p1, v1, p2}, Lb/c;-><init>(Lb/ag;Lb/c$a;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final b(Lb/Q;)V
    .locals 1

    iget-object v0, p0, Lb/ag;->a:Lb/e;

    invoke-virtual {v0, p1}, Lb/e;->b(Lb/Q;)V

    return-void
.end method

.method protected c(Lb/C;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final c(Lb/Q;)V
    .locals 1

    iget-object v0, p0, Lb/ag;->a:Lb/e;

    invoke-virtual {v0, p1}, Lb/e;->a(Lb/Q;)V

    return-void
.end method

.method protected c_()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final d(Lb/C;)V
    .locals 2

    invoke-virtual {p1}, Lb/C;->h()V

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->b:Lb/c$a;

    invoke-direct {v0, p1, v1}, Lb/c;-><init>(Lb/ag;Lb/c$a;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final d(Lb/Q;)V
    .locals 3

    new-instance v0, Lb/c;

    iget-object v1, p0, Lb/ag;->a:Lb/e;

    invoke-virtual {v1}, Lb/e;->c()Lb/ag;

    move-result-object v1

    sget-object v2, Lb/c$a;->n:Lb/c$a;

    invoke-direct {v0, v1, v2, p1}, Lb/c;-><init>(Lb/ag;Lb/c$a;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final e(Lb/C;)V
    .locals 2

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->m:Lb/c$a;

    invoke-direct {v0, p1, v1}, Lb/c;-><init>(Lb/ag;Lb/c$a;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected e_()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected f_()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected g()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected h(Lb/E;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final j(Lb/E;)V
    .locals 2

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->f:Lb/c$a;

    invoke-direct {v0, p1, v1}, Lb/c;-><init>(Lb/ag;Lb/c$a;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected k()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final k(Lb/E;)V
    .locals 2

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->i:Lb/c$a;

    invoke-direct {v0, p1, v1}, Lb/c;-><init>(Lb/ag;Lb/c$a;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final l(Lb/E;)V
    .locals 2

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->j:Lb/c$a;

    invoke-direct {v0, p1, v1}, Lb/c;-><init>(Lb/ag;Lb/c$a;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected n()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected o()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected s()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final u()I
    .locals 1

    iget v0, p0, Lb/ag;->b:I

    return v0
.end method

.method protected final v()V
    .locals 3

    new-instance v0, Lb/c;

    sget-object v1, Lb/c$a;->a:Lb/c$a;

    invoke-direct {v0, p0, v1}, Lb/c;-><init>(Lb/ag;Lb/c$a;)V

    iget-object v1, p0, Lb/ag;->a:Lb/e;

    iget v2, p0, Lb/ag;->b:I

    invoke-virtual {v1, v2, v0}, Lb/e;->a(ILb/c;)V

    return-void
.end method

.method protected final w()V
    .locals 3

    new-instance v0, Lb/c;

    iget-object v1, p0, Lb/ag;->a:Lb/e;

    invoke-virtual {v1}, Lb/e;->c()Lb/ag;

    move-result-object v1

    sget-object v2, Lb/c$a;->o:Lb/c$a;

    invoke-direct {v0, v1, v2}, Lb/c;-><init>(Lb/ag;Lb/c$a;)V

    invoke-direct {p0, v0}, Lb/ag;->b(Lb/c;)V

    return-void
.end method

.method protected final x()V
    .locals 3

    new-instance v0, Lb/c;

    const/4 v1, 0x0

    sget-object v2, Lb/c$a;->p:Lb/c$a;

    invoke-direct {v0, v1, v2}, Lb/c;-><init>(Lb/ag;Lb/c$a;)V

    iget-object v1, p0, Lb/ag;->a:Lb/e;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lb/e;->a(ILb/c;)V

    return-void
.end method
