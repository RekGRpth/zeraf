.class public Lb/U;
.super Lb/C;
.source "SourceFile"

# interfaces
.implements Lb/t;


# static fields
.field private static synthetic l:Z


# instance fields
.field private final b:Lb/r;

.field private final c:Lb/a;

.field private d:Ljava/nio/channels/SocketChannel;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lb/O;

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Lb/Q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/U;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/U;->l:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/s;Lb/O;Lb/B;Lb/a;Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p3}, Lb/C;-><init>(Lb/s;Lb/B;)V

    new-instance v0, Lb/r;

    invoke-direct {v0, p1}, Lb/r;-><init>(Lb/s;)V

    iput-object v0, p0, Lb/U;->b:Lb/r;

    iput-object p4, p0, Lb/U;->c:Lb/a;

    const/4 v0, 0x0

    iput-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    iput-boolean v1, p0, Lb/U;->e:Z

    iput-boolean p5, p0, Lb/U;->f:Z

    iput-boolean v1, p0, Lb/U;->g:Z

    iput-object p2, p0, Lb/U;->h:Lb/O;

    iget-object v0, p0, Lb/U;->a:Lb/B;

    iget v0, v0, Lb/B;->n:I

    iput v0, p0, Lb/U;->i:I

    sget-boolean v0, Lb/U;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/U;->c:Lb/a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/U;->c:Lb/a;

    invoke-virtual {v0}, Lb/a;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/U;->j:Ljava/lang/String;

    invoke-virtual {p2}, Lb/O;->r()Lb/Q;

    move-result-object v0

    iput-object v0, p0, Lb/U;->k:Lb/Q;

    return-void
.end method

.method private a()V
    .locals 3

    :try_start_0
    sget-boolean v0, Lb/U;->l:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lb/U;->q()V

    :cond_0
    invoke-direct {p0}, Lb/U;->p()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    iput-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-static {v0}, Lb/X;->a(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    iget-object v1, p0, Lb/U;->c:Lb/a;

    invoke-virtual {v1}, Lb/a;->b()Lb/a$a;

    move-result-object v1

    invoke-interface {v1}, Lb/a$a;->a()Ljava/net/SocketAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/U;->b:Lb/r;

    iget-object v1, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->a(Ljava/nio/channels/SelectableChannel;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/U;->e:Z

    iget-object v0, p0, Lb/U;->b:Lb/r;

    invoke-virtual {v0}, Lb/r;->d()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lb/U;->b:Lb/r;

    iget-object v1, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->a(Ljava/nio/channels/SelectableChannel;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/U;->e:Z

    iget-object v0, p0, Lb/U;->b:Lb/r;

    iget-object v1, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->e(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/U;->k:Lb/Q;

    iget-object v1, p0, Lb/U;->j:Ljava/lang/String;

    invoke-static {}, Lb/ae;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lb/Q;->a(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private p()V
    .locals 5

    const/4 v4, 0x1

    iget v0, p0, Lb/U;->i:I

    invoke-static {}, Lb/X;->a()I

    move-result v1

    iget-object v2, p0, Lb/U;->a:Lb/B;

    iget v2, v2, Lb/B;->n:I

    rem-int/2addr v1, v2

    add-int/2addr v0, v1

    iget-object v1, p0, Lb/U;->a:Lb/B;

    iget v1, v1, Lb/B;->o:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lb/U;->a:Lb/B;

    iget v1, v1, Lb/B;->o:I

    iget-object v2, p0, Lb/U;->a:Lb/B;

    iget v2, v2, Lb/B;->n:I

    if-le v1, v2, :cond_0

    iget v1, p0, Lb/U;->i:I

    shl-int/lit8 v1, v1, 0x1

    iput v1, p0, Lb/U;->i:I

    iget v1, p0, Lb/U;->i:I

    iget-object v2, p0, Lb/U;->a:Lb/B;

    iget v2, v2, Lb/B;->o:I

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lb/U;->a:Lb/B;

    iget v1, v1, Lb/B;->o:I

    iput v1, p0, Lb/U;->i:I

    :cond_0
    iget-object v1, p0, Lb/U;->b:Lb/r;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3, v4}, Lb/r;->a(JI)V

    iget-object v1, p0, Lb/U;->k:Lb/Q;

    iget-object v2, p0, Lb/U;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lb/Q;->b(Ljava/lang/String;I)V

    iput-boolean v4, p0, Lb/U;->g:Z

    return-void
.end method

.method private q()V
    .locals 3

    sget-boolean v0, Lb/U;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->close()V

    iget-object v0, p0, Lb/U;->k:Lb/Q;

    iget-object v1, p0, Lb/U;->j:Ljava/lang/String;

    iget-object v2, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1, v2}, Lb/Q;->b(Ljava/lang/String;Ljava/nio/channels/SelectableChannel;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lb/ae;->a(Ljava/io/IOException;)V

    iget-object v0, p0, Lb/U;->k:Lb/Q;

    iget-object v1, p0, Lb/U;->j:Ljava/lang/String;

    invoke-static {}, Lb/ae;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lb/Q;->c(Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/U;->g:Z

    invoke-direct {p0}, Lb/U;->a()V

    return-void
.end method

.method public final b(I)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lb/U;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/U;->b:Lb/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lb/r;->b(I)V

    iput-boolean v2, p0, Lb/U;->g:Z

    :cond_0
    iget-boolean v0, p0, Lb/U;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/U;->b:Lb/r;

    iget-object v1, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->b(Ljava/nio/channels/SelectableChannel;)V

    iput-boolean v2, p0, Lb/U;->e:Z

    :cond_1
    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lb/U;->q()V

    :cond_2
    invoke-super {p0, p1}, Lb/C;->b(I)V

    return-void
.end method

.method public final d()V
    .locals 6

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x1

    :try_start_0
    iget-object v2, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    move-result v2

    sget-boolean v4, Lb/U;->l:Z

    if-nez v4, :cond_0

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    :catch_0
    move-exception v2

    move v2, v0

    move-object v0, v1

    :goto_0
    iget-object v4, p0, Lb/U;->b:Lb/r;

    iget-object v5, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v4, v5}, Lb/r;->b(Ljava/nio/channels/SelectableChannel;)V

    iput-boolean v3, p0, Lb/U;->e:Z

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lb/U;->q()V

    invoke-direct {p0}, Lb/U;->p()V

    :goto_1
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;
    :try_end_1
    .catch Ljava/net/ConnectException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move v2, v3

    goto :goto_0

    :catch_1
    move-exception v2

    move v2, v0

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v2

    move v2, v0

    move-object v0, v1

    goto :goto_0

    :catch_3
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_1
    iput-object v1, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    :try_start_2
    invoke-static {v0}, Lb/X;->a(Ljava/nio/channels/SocketChannel;)V

    iget-object v1, p0, Lb/U;->a:Lb/B;

    iget v1, v1, Lb/B;->z:I

    iget-object v2, p0, Lb/U;->a:Lb/B;

    iget v2, v2, Lb/B;->A:I

    iget-object v3, p0, Lb/U;->a:Lb/B;

    iget v3, v3, Lb/B;->B:I

    iget-object v4, p0, Lb/U;->a:Lb/B;

    iget v4, v4, Lb/B;->C:I

    invoke-static {v0, v1, v2, v3, v4}, Lb/X;->a(Ljava/nio/channels/SocketChannel;IIII)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_4

    :try_start_3
    new-instance v1, Lb/R;

    iget-object v2, p0, Lb/U;->a:Lb/B;

    iget-object v3, p0, Lb/U;->j:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lb/R;-><init>(Ljava/nio/channels/SocketChannel;Lb/B;Ljava/lang/String;)V
    :try_end_3
    .catch Lb/ae$b; {:try_start_3 .. :try_end_3} :catch_5

    iget-object v2, p0, Lb/U;->h:Lb/O;

    invoke-virtual {p0, v2, v1}, Lb/U;->a(Lb/O;Lb/o;)V

    invoke-virtual {p0}, Lb/U;->b_()V

    iget-object v1, p0, Lb/U;->k:Lb/Q;

    iget-object v2, p0, Lb/U;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lb/Q;->a(Ljava/lang/String;Ljava/nio/channels/SelectableChannel;)V

    goto :goto_1

    :catch_4
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_5
    move-exception v0

    iget-object v0, p0, Lb/U;->k:Lb/Q;

    iget-object v1, p0, Lb/U;->j:Ljava/lang/String;

    invoke-static {}, Lb/ae;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lb/Q;->a(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public final e()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final f()V
    .locals 1

    sget-boolean v0, Lb/U;->l:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/U;->g:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lb/U;->l:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lb/U;->e:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    sget-boolean v0, Lb/U;->l:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/U;->d:Ljava/nio/channels/SocketChannel;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    return-void
.end method

.method public final g_()V
    .locals 0

    return-void
.end method

.method public final h_()V
    .locals 0

    return-void
.end method

.method protected final s()V
    .locals 1

    iget-object v0, p0, Lb/U;->b:Lb/r;

    invoke-virtual {v0, p0}, Lb/r;->a(Lb/t;)V

    iget-boolean v0, p0, Lb/U;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lb/U;->p()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lb/U;->a()V

    goto :goto_0
.end method
