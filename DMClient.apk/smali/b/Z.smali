.class public final Lb/Z;
.super Lb/k;
.source "SourceFile"


# instance fields
.field private a:Lb/y;

.field private final b:[B

.field private c:Lb/q;


# direct methods
.method public constructor <init>(ILb/q;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0, p1}, Lb/k;-><init>(I)V

    const/16 v0, 0x9

    new-array v0, v0, [B

    iput-object v0, p0, Lb/Z;->b:[B

    iput-object p2, p0, Lb/Z;->c:Lb/q;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2, v2}, Lb/Z;->a([BIIZ)V

    return-void
.end method


# virtual methods
.method public final a(Lb/q;)V
    .locals 0

    iput-object p1, p0, Lb/Z;->c:Lb/q;

    return-void
.end method

.method protected final a()Z
    .locals 6

    const/16 v4, 0xff

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lb/Z;->c()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget-object v2, p0, Lb/Z;->a:Lb/y;

    invoke-virtual {v2}, Lb/y;->f()[B

    move-result-object v2

    iget-object v3, p0, Lb/Z;->a:Lb/y;

    invoke-virtual {v3}, Lb/y;->g()I

    move-result v3

    iget-object v4, p0, Lb/Z;->a:Lb/y;

    invoke-virtual {v4}, Lb/y;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    invoke-virtual {p0, v2, v3, v1, v0}, Lb/Z;->a([BIIZ)V

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lb/Z;->c:Lb/q;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb/Z;->c:Lb/q;

    invoke-interface {v2}, Lb/q;->a()Lb/y;

    move-result-object v2

    iput-object v2, p0, Lb/Z;->a:Lb/y;

    iget-object v2, p0, Lb/Z;->a:Lb/y;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb/Z;->a:Lb/y;

    invoke-virtual {v2}, Lb/y;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v1

    :goto_2
    iget-object v3, p0, Lb/Z;->a:Lb/y;

    invoke-virtual {v3}, Lb/y;->g()I

    move-result v3

    if-le v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x2

    :cond_2
    iget-object v3, p0, Lb/Z;->b:[B

    int-to-byte v2, v2

    aput-byte v2, v3, v0

    iget-object v2, p0, Lb/Z;->a:Lb/y;

    invoke-virtual {v2}, Lb/y;->g()I

    move-result v2

    if-le v2, v4, :cond_3

    iget-object v3, p0, Lb/Z;->b:[B

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lb/Z;->b:[B

    const/16 v3, 0x9

    invoke-virtual {p0, v2, v3, v0, v0}, Lb/Z;->a([BIIZ)V

    :goto_3
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lb/Z;->b:[B

    int-to-byte v2, v2

    aput-byte v2, v3, v1

    iget-object v2, p0, Lb/Z;->b:[B

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3, v0, v0}, Lb/Z;->a([BIIZ)V

    goto :goto_3

    :cond_4
    move v2, v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
