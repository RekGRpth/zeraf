.class public Lb/f;
.super Lb/Q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/f$a;
    }
.end annotation


# static fields
.field private static synthetic f:Z


# instance fields
.field private final b:Lb/l;

.field private final c:Lb/w;

.field private d:Z

.field private e:Lb/y;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/f;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/f;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lb/Q;-><init>(Lb/e;II)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/f;->d:Z

    iget-object v0, p0, Lb/f;->a:Lb/B;

    const/4 v1, 0x5

    iput v1, v0, Lb/B;->l:I

    new-instance v0, Lb/l;

    invoke-direct {v0}, Lb/l;-><init>()V

    iput-object v0, p0, Lb/f;->b:Lb/l;

    new-instance v0, Lb/w;

    invoke-direct {v0}, Lb/w;-><init>()V

    iput-object v0, p0, Lb/f;->c:Lb/w;

    iget-object v0, p0, Lb/f;->a:Lb/B;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lb/B;->y:Z

    return-void
.end method

.method private a()Lb/y;
    .locals 2

    iget-boolean v0, p0, Lb/f;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/f;->e:Lb/y;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lb/f;->d:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lb/f;->e:Lb/y;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lb/f;->b:Lb/l;

    invoke-virtual {v0}, Lb/l;->a()Lb/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/y;->c()I

    move-result v1

    and-int/lit8 v1, v1, 0x40

    if-nez v1, :cond_1

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/f;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->c(Lb/E;)V

    return-void
.end method

.method protected final a(Lb/E;Z)V
    .locals 1

    sget-boolean v0, Lb/f;->f:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/f;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->a(Lb/E;)V

    iget-object v0, p0, Lb/f;->c:Lb/w;

    invoke-virtual {v0, p1}, Lb/w;->a(Lb/E;)V

    return-void
.end method

.method protected a(Lb/y;I)Z
    .locals 1

    iget-object v0, p0, Lb/f;->c:Lb/w;

    invoke-virtual {v0, p1}, Lb/w;->a(Lb/y;)Z

    move-result v0

    return v0
.end method

.method protected a_(I)Lb/y;
    .locals 1

    invoke-direct {p0}, Lb/f;->a()Lb/y;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/f;->c:Lb/w;

    invoke-virtual {v0, p1}, Lb/w;->c(Lb/E;)V

    return-void
.end method

.method protected final c(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/f;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->b(Lb/E;)V

    iget-object v0, p0, Lb/f;->c:Lb/w;

    invoke-virtual {v0, p1}, Lb/w;->b(Lb/E;)V

    return-void
.end method
