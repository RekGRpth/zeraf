.class public final Lb/af;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    return-void
.end method

.method public static a(I)Lb/e;
    .locals 2

    if-ltz p0, :cond_2

    new-instance v0, Lb/e;

    invoke-direct {v0}, Lb/e;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/e;->a()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lb/e;->a(II)V

    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "io_threds must not be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
