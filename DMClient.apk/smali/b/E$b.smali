.class final enum Lb/E$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/E;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lb/E$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lb/E$b;

.field public static final enum b:Lb/E$b;

.field public static final enum c:Lb/E$b;

.field public static final enum d:Lb/E$b;

.field public static final enum e:Lb/E$b;

.field public static final enum f:Lb/E$b;

.field private static final synthetic g:[Lb/E$b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lb/E$b;

    const-string v1, "active"

    invoke-direct {v0, v1, v3}, Lb/E$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/E$b;->a:Lb/E$b;

    new-instance v0, Lb/E$b;

    const-string v1, "delimited"

    invoke-direct {v0, v1, v4}, Lb/E$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/E$b;->b:Lb/E$b;

    new-instance v0, Lb/E$b;

    const-string v1, "pending"

    invoke-direct {v0, v1, v5}, Lb/E$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/E$b;->c:Lb/E$b;

    new-instance v0, Lb/E$b;

    const-string v1, "terminating"

    invoke-direct {v0, v1, v6}, Lb/E$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/E$b;->d:Lb/E$b;

    new-instance v0, Lb/E$b;

    const-string v1, "terminated"

    invoke-direct {v0, v1, v7}, Lb/E$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/E$b;->e:Lb/E$b;

    new-instance v0, Lb/E$b;

    const-string v1, "double_terminated"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lb/E$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lb/E$b;->f:Lb/E$b;

    const/4 v0, 0x6

    new-array v0, v0, [Lb/E$b;

    sget-object v1, Lb/E$b;->a:Lb/E$b;

    aput-object v1, v0, v3

    sget-object v1, Lb/E$b;->b:Lb/E$b;

    aput-object v1, v0, v4

    sget-object v1, Lb/E$b;->c:Lb/E$b;

    aput-object v1, v0, v5

    sget-object v1, Lb/E$b;->d:Lb/E$b;

    aput-object v1, v0, v6

    sget-object v1, Lb/E$b;->e:Lb/E$b;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lb/E$b;->f:Lb/E$b;

    aput-object v2, v0, v1

    sput-object v0, Lb/E$b;->g:[Lb/E$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lb/E$b;
    .locals 1

    const-class v0, Lb/E$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lb/E$b;

    return-object v0
.end method

.method public static values()[Lb/E$b;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lb/E$b;->g:[Lb/E$b;

    array-length v1, v0

    new-array v2, v1, [Lb/E$b;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
