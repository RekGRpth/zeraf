.class public Lb/aa;
.super Lb/Q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/aa$a;
    }
.end annotation


# static fields
.field private static g:Lb/z$a;

.field private static h:Lb/z$a;

.field private static synthetic i:Z


# instance fields
.field private final b:Lb/z;

.field private final c:Lb/i;

.field private d:Z

.field private e:Z

.field private final f:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lb/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/aa;->i:Z

    new-instance v0, Lb/aa$1;

    invoke-direct {v0}, Lb/aa$1;-><init>()V

    sput-object v0, Lb/aa;->g:Lb/z$a;

    new-instance v0, Lb/aa$2;

    invoke-direct {v0}, Lb/aa$2;-><init>()V

    sput-object v0, Lb/aa;->h:Lb/z$a;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;II)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Lb/Q;-><init>(Lb/e;II)V

    iget-object v0, p0, Lb/aa;->a:Lb/B;

    const/16 v1, 0x9

    iput v1, v0, Lb/B;->l:I

    iput-boolean v2, p0, Lb/aa;->d:Z

    iput-boolean v2, p0, Lb/aa;->e:Z

    new-instance v0, Lb/z;

    invoke-direct {v0}, Lb/z;-><init>()V

    iput-object v0, p0, Lb/aa;->b:Lb/z;

    new-instance v0, Lb/i;

    invoke-direct {v0}, Lb/i;-><init>()V

    iput-object v0, p0, Lb/aa;->c:Lb/i;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lb/aa;->f:Ljava/util/Deque;

    return-void
.end method

.method static synthetic a(Lb/aa;)Lb/i;
    .locals 1

    iget-object v0, p0, Lb/aa;->c:Lb/i;

    return-object v0
.end method

.method static synthetic b(Lb/aa;)Ljava/util/Deque;
    .locals 1

    iget-object v0, p0, Lb/aa;->f:Ljava/util/Deque;

    return-object v0
.end method


# virtual methods
.method protected final a(Lb/E;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lb/E;->c()Lb/y;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    invoke-virtual {v1}, Lb/y;->f()[B

    move-result-object v2

    invoke-virtual {v1}, Lb/y;->g()I

    move-result v0

    if-lez v0, :cond_0

    aget-byte v0, v2, v5

    if-eqz v0, :cond_2

    aget-byte v0, v2, v5

    if-ne v0, v6, :cond_0

    :cond_2
    aget-byte v0, v2, v5

    if-nez v0, :cond_4

    iget-object v0, p0, Lb/aa;->b:Lb/z;

    invoke-virtual {v0, v2, v6, p1}, Lb/z;->b([BILb/E;)Z

    move-result v0

    :goto_1
    iget-object v3, p0, Lb/aa;->a:Lb/B;

    iget v3, v3, Lb/B;->l:I

    const/16 v4, 0x9

    if-ne v3, v4, :cond_0

    if-nez v0, :cond_3

    aget-byte v0, v2, v5

    if-ne v0, v6, :cond_0

    iget-boolean v0, p0, Lb/aa;->d:Z

    if-eqz v0, :cond_0

    :cond_3
    iget-object v0, p0, Lb/aa;->f:Ljava/util/Deque;

    new-instance v2, Lb/b;

    invoke-virtual {v1}, Lb/y;->f()[B

    move-result-object v1

    invoke-direct {v2, v1}, Lb/b;-><init>([B)V

    invoke-interface {v0, v2}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lb/aa;->b:Lb/z;

    invoke-virtual {v0, v2, v6, p1}, Lb/z;->a([BILb/E;)Z

    move-result v0

    goto :goto_1
.end method

.method protected final a(Lb/E;Z)V
    .locals 2

    sget-boolean v0, Lb/aa;->i:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/aa;->c:Lb/i;

    invoke-virtual {v0, p1}, Lb/i;->a(Lb/E;)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lb/aa;->b:Lb/z;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lb/z;->a([BLb/E;)Z

    :cond_1
    invoke-virtual {p0, p1}, Lb/aa;->a(Lb/E;)V

    return-void
.end method

.method public final a(ILjava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/16 v2, 0x28

    if-eq p1, v2, :cond_0

    const/16 v1, 0x16

    invoke-static {v1}, Lb/ae;->a(I)V

    :goto_0
    return v0

    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v1, :cond_1

    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lb/aa;->d:Z

    move v0, v1

    goto :goto_0
.end method

.method protected final a(Lb/y;I)Z
    .locals 5

    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v0

    iget-boolean v1, p0, Lb/aa;->e:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lb/aa;->b:Lb/z;

    invoke-virtual {p1}, Lb/y;->f()[B

    move-result-object v2

    invoke-virtual {p1}, Lb/y;->g()I

    move-result v3

    sget-object v4, Lb/aa;->g:Lb/z$a;

    invoke-virtual {v1, v2, v3, v4, p0}, Lb/z;->a([BILb/z$a;Ljava/lang/Object;)V

    :cond_0
    iget-object v1, p0, Lb/aa;->c:Lb/i;

    invoke-virtual {v1, p1, p2}, Lb/i;->b(Lb/y;I)Z

    if-nez v0, :cond_1

    iget-object v1, p0, Lb/aa;->c:Lb/i;

    invoke-virtual {v1}, Lb/i;->a()V

    :cond_1
    iput-boolean v0, p0, Lb/aa;->e:Z

    const/4 v0, 0x1

    return v0
.end method

.method protected a_(I)Lb/y;
    .locals 2

    iget-object v0, p0, Lb/aa;->f:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lb/aa;->f:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/b;

    new-instance v1, Lb/y;

    invoke-virtual {v0}, Lb/b;->a()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lb/y;-><init>([B)V

    move-object v0, v1

    goto :goto_0
.end method

.method protected final b(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/aa;->c:Lb/i;

    invoke-virtual {v0, p1}, Lb/i;->d(Lb/E;)V

    return-void
.end method

.method protected final c(Lb/E;)V
    .locals 2

    iget-object v0, p0, Lb/aa;->b:Lb/z;

    sget-object v1, Lb/aa;->h:Lb/z$a;

    invoke-virtual {v0, p1, v1, p0}, Lb/z;->a(Lb/E;Lb/z$a;Ljava/lang/Object;)Z

    iget-object v0, p0, Lb/aa;->c:Lb/i;

    invoke-virtual {v0, p1}, Lb/i;->c(Lb/E;)V

    return-void
.end method
