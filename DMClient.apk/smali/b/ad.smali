.class public Lb/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/ad$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private volatile a:Lb/ad$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/ad",
            "<TT;>.a;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Lb/ad$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/ad",
            "<TT;>.a;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Lb/ad$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/ad",
            "<TT;>.a;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Lb/ad$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/ad",
            "<TT;>.a;"
        }
    .end annotation
.end field

.field private final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final i:I

.field private j:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;I)V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb/ad;->h:Ljava/lang/Class;

    iput p2, p0, Lb/ad;->i:I

    iput v2, p0, Lb/ad;->j:I

    new-instance v0, Lb/ad$a;

    iget v1, p0, Lb/ad;->j:I

    invoke-direct {v0, p0, p1, p2, v1}, Lb/ad$a;-><init>(Lb/ad;Ljava/lang/Class;II)V

    iput-object v0, p0, Lb/ad;->a:Lb/ad$a;

    iget v0, p0, Lb/ad;->j:I

    add-int/2addr v0, p2

    iput v0, p0, Lb/ad;->j:I

    iput v2, p0, Lb/ad;->b:I

    iput v2, p0, Lb/ad;->d:I

    iget-object v0, p0, Lb/ad;->a:Lb/ad$a;

    iput-object v0, p0, Lb/ad;->c:Lb/ad$a;

    iget-object v0, p0, Lb/ad;->a:Lb/ad$a;

    iput-object v0, p0, Lb/ad;->g:Lb/ad$a;

    iget-object v0, p0, Lb/ad;->a:Lb/ad$a;

    iput-object v0, p0, Lb/ad;->e:Lb/ad$a;

    const/4 v0, 0x1

    iput v0, p0, Lb/ad;->f:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lb/ad;->a:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->b:[I

    iget v1, p0, Lb/ad;->b:I

    aget v0, v0, v1

    return v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lb/ad;->c:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->a:[Ljava/lang/Object;

    iget v1, p0, Lb/ad;->d:I

    aput-object p1, v0, v1

    iget-object v0, p0, Lb/ad;->e:Lb/ad$a;

    iput-object v0, p0, Lb/ad;->c:Lb/ad$a;

    iget v0, p0, Lb/ad;->f:I

    iput v0, p0, Lb/ad;->d:I

    iget v0, p0, Lb/ad;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/ad;->f:I

    iget v0, p0, Lb/ad;->f:I

    iget v1, p0, Lb/ad;->i:I

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lb/ad;->g:Lb/ad$a;

    iget-object v1, p0, Lb/ad;->a:Lb/ad$a;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lb/ad;->g:Lb/ad$a;

    iget-object v1, v1, Lb/ad$a;->d:Lb/ad$a;

    iput-object v1, p0, Lb/ad;->g:Lb/ad$a;

    iget-object v1, p0, Lb/ad;->e:Lb/ad$a;

    iput-object v0, v1, Lb/ad$a;->d:Lb/ad$a;

    iget-object v1, p0, Lb/ad;->e:Lb/ad$a;

    iput-object v1, v0, Lb/ad$a;->c:Lb/ad$a;

    :goto_1
    iget-object v0, p0, Lb/ad;->e:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->d:Lb/ad$a;

    iput-object v0, p0, Lb/ad;->e:Lb/ad$a;

    const/4 v0, 0x0

    iput v0, p0, Lb/ad;->f:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lb/ad;->e:Lb/ad$a;

    new-instance v1, Lb/ad$a;

    iget-object v2, p0, Lb/ad;->h:Ljava/lang/Class;

    iget v3, p0, Lb/ad;->i:I

    iget v4, p0, Lb/ad;->j:I

    invoke-direct {v1, p0, v2, v3, v4}, Lb/ad$a;-><init>(Lb/ad;Ljava/lang/Class;II)V

    iput-object v1, v0, Lb/ad$a;->d:Lb/ad$a;

    iget v0, p0, Lb/ad;->j:I

    iget v1, p0, Lb/ad;->i:I

    add-int/2addr v0, v1

    iput v0, p0, Lb/ad;->j:I

    iget-object v0, p0, Lb/ad;->e:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->d:Lb/ad$a;

    iget-object v1, p0, Lb/ad;->e:Lb/ad$a;

    iput-object v1, v0, Lb/ad$a;->c:Lb/ad$a;

    goto :goto_1
.end method

.method public final b()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lb/ad;->a:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->a:[Ljava/lang/Object;

    iget v1, p0, Lb/ad;->b:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final c()I
    .locals 2

    iget-object v0, p0, Lb/ad;->c:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->b:[I

    iget v1, p0, Lb/ad;->d:I

    aget v0, v0, v1

    return v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lb/ad;->c:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->a:[Ljava/lang/Object;

    iget v1, p0, Lb/ad;->d:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final e()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v0, p0, Lb/ad;->a:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->a:[Ljava/lang/Object;

    iget v1, p0, Lb/ad;->b:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lb/ad;->a:Lb/ad$a;

    iget-object v1, v1, Lb/ad$a;->a:[Ljava/lang/Object;

    iget v2, p0, Lb/ad;->b:I

    aput-object v3, v1, v2

    iget v1, p0, Lb/ad;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lb/ad;->b:I

    iget v1, p0, Lb/ad;->b:I

    iget v2, p0, Lb/ad;->i:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lb/ad;->a:Lb/ad$a;

    iget-object v1, v1, Lb/ad$a;->d:Lb/ad$a;

    iput-object v1, p0, Lb/ad;->a:Lb/ad$a;

    iget-object v1, p0, Lb/ad;->a:Lb/ad$a;

    iput-object v3, v1, Lb/ad$a;->c:Lb/ad$a;

    const/4 v1, 0x0

    iput v1, p0, Lb/ad;->b:I

    :cond_0
    return-object v0
.end method

.method public final f()V
    .locals 2

    iget v0, p0, Lb/ad;->d:I

    if-lez v0, :cond_0

    iget v0, p0, Lb/ad;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/ad;->d:I

    :goto_0
    iget v0, p0, Lb/ad;->f:I

    if-lez v0, :cond_1

    iget v0, p0, Lb/ad;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/ad;->f:I

    :goto_1
    return-void

    :cond_0
    iget v0, p0, Lb/ad;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/ad;->d:I

    iget-object v0, p0, Lb/ad;->c:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->c:Lb/ad$a;

    iput-object v0, p0, Lb/ad;->c:Lb/ad$a;

    goto :goto_0

    :cond_1
    iget v0, p0, Lb/ad;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/ad;->f:I

    iget-object v0, p0, Lb/ad;->e:Lb/ad$a;

    iget-object v0, v0, Lb/ad$a;->c:Lb/ad$a;

    iput-object v0, p0, Lb/ad;->e:Lb/ad$a;

    iget-object v0, p0, Lb/ad;->e:Lb/ad$a;

    const/4 v1, 0x0

    iput-object v1, v0, Lb/ad$a;->d:Lb/ad$a;

    goto :goto_1
.end method
