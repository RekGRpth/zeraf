.class public Lb/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lb/t;


# static fields
.field private static synthetic c:Z


# instance fields
.field private a:Lb/F;

.field private b:Lb/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/r;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/r;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lb/r;->a(Lb/s;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x0

    sget-boolean v0, Lb/r;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/r;->a:Lb/F;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object v1, p0, Lb/r;->a:Lb/F;

    iput-object v1, p0, Lb/r;->b:Lb/t;

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lb/r;->b:Lb/t;

    invoke-interface {v0, p1}, Lb/t;->a(I)V

    return-void
.end method

.method public final a(JI)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p1, p2, p0, p3}, Lb/F;->a(JLb/t;I)V

    return-void
.end method

.method public final a(Lb/s;)V
    .locals 1

    sget-boolean v0, Lb/r;->c:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lb/r;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p1}, Lb/s;->j()Lb/F;

    move-result-object v0

    iput-object v0, p0, Lb/r;->a:Lb/F;

    return-void
.end method

.method public final a(Lb/t;)V
    .locals 0

    iput-object p1, p0, Lb/r;->b:Lb/t;

    return-void
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p1, p0}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;Lb/t;)V

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p0, p1}, Lb/F;->a(Lb/t;I)V

    return-void
.end method

.method public final b(Ljava/nio/channels/SelectableChannel;)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;)V

    return-void
.end method

.method public final c(Ljava/nio/channels/SelectableChannel;)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p1}, Lb/F;->b(Ljava/nio/channels/SelectableChannel;)V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lb/r;->b:Lb/t;

    invoke-interface {v0}, Lb/t;->d()V

    return-void
.end method

.method public final d(Ljava/nio/channels/SelectableChannel;)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p1}, Lb/F;->d(Ljava/nio/channels/SelectableChannel;)V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lb/r;->b:Lb/t;

    invoke-interface {v0}, Lb/t;->e()V

    return-void
.end method

.method public final e(Ljava/nio/channels/SelectableChannel;)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p1}, Lb/F;->f(Ljava/nio/channels/SelectableChannel;)V

    return-void
.end method

.method public final f(Ljava/nio/channels/SelectableChannel;)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p1}, Lb/F;->c(Ljava/nio/channels/SelectableChannel;)V

    return-void
.end method

.method public final g(Ljava/nio/channels/SelectableChannel;)V
    .locals 1

    iget-object v0, p0, Lb/r;->a:Lb/F;

    invoke-virtual {v0, p1}, Lb/F;->e(Ljava/nio/channels/SelectableChannel;)V

    return-void
.end method

.method public final g_()V
    .locals 1

    iget-object v0, p0, Lb/r;->b:Lb/t;

    invoke-interface {v0}, Lb/t;->g_()V

    return-void
.end method

.method public final h_()V
    .locals 1

    iget-object v0, p0, Lb/r;->b:Lb/t;

    invoke-interface {v0}, Lb/t;->h_()V

    return-void
.end method
