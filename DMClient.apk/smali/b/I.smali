.class public Lb/I;
.super Lb/Q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/I$a;
    }
.end annotation


# static fields
.field private static synthetic c:Z


# instance fields
.field private final b:Lb/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/I;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/I;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lb/Q;-><init>(Lb/e;II)V

    iget-object v0, p0, Lb/I;->a:Lb/B;

    const/4 v1, 0x7

    iput v1, v0, Lb/B;->l:I

    new-instance v0, Lb/l;

    invoke-direct {v0}, Lb/l;-><init>()V

    iput-object v0, p0, Lb/I;->b:Lb/l;

    return-void
.end method


# virtual methods
.method protected final a(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/I;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->c(Lb/E;)V

    return-void
.end method

.method protected final a(Lb/E;Z)V
    .locals 1

    sget-boolean v0, Lb/I;->c:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/I;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->a(Lb/E;)V

    return-void
.end method

.method public final a_(I)Lb/y;
    .locals 1

    iget-object v0, p0, Lb/I;->b:Lb/l;

    invoke-virtual {v0}, Lb/l;->a()Lb/y;

    move-result-object v0

    return-object v0
.end method

.method protected final c(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/I;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->b(Lb/E;)V

    return-void
.end method
