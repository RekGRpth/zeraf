.class public final Lb/S;
.super Lb/ab;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/S$a;
    }
.end annotation


# direct methods
.method public constructor <init>(Lb/e;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lb/ab;-><init>(Lb/e;II)V

    iget-object v0, p0, Lb/S;->a:Lb/B;

    const/4 v1, 0x2

    iput v1, v0, Lb/B;->l:I

    iget-object v0, p0, Lb/S;->a:Lb/B;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lb/B;->x:Z

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Z
    .locals 6

    const/4 v5, 0x7

    const/4 v4, 0x6

    const/4 v3, 0x1

    const/4 v0, 0x0

    if-eq p1, v4, :cond_0

    if-eq p1, v5, :cond_0

    const/16 v1, 0x16

    invoke-static {v1}, Lb/ae;->a(I)V

    :goto_0
    return v0

    :cond_0
    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    :goto_1
    new-instance v1, Lb/y;

    array-length v2, p2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Lb/y;-><init>(I)V

    if-ne p1, v4, :cond_4

    invoke-virtual {v1, v3}, Lb/y;->a(B)V

    :cond_1
    :goto_2
    invoke-virtual {v1, p2, v3}, Lb/y;->a([BI)V

    invoke-super {p0, v1, v0}, Lb/ab;->a(Lb/y;I)Z

    move-result v0

    goto :goto_0

    :cond_2
    instance-of v1, p2, [B

    if-eqz v1, :cond_3

    check-cast p2, [B

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    if-ne p1, v5, :cond_1

    invoke-virtual {v1, v0}, Lb/y;->a(B)V

    goto :goto_2
.end method

.method protected final a(Lb/y;I)Z
    .locals 1

    const/16 v0, 0x2d

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, 0x0

    return v0
.end method
