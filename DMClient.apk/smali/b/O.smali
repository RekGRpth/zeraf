.class public Lb/O;
.super Lb/C;
.source "SourceFile"

# interfaces
.implements Lb/E$a;
.implements Lb/p;
.implements Lb/q;
.implements Lb/t;


# static fields
.field private static j:I

.field private static synthetic p:Z


# instance fields
.field private b:Z

.field private c:Lb/E;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lb/E;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:Lb/o;

.field private h:Lb/Q;

.field private i:Lb/s;

.field private k:Z

.field private l:Z

.field private m:Z

.field private final n:Lb/a;

.field private o:Lb/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/O;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/O;->p:Z

    const/16 v0, 0x20

    sput v0, Lb/O;->j:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p4}, Lb/C;-><init>(Lb/s;Lb/B;)V

    new-instance v0, Lb/r;

    invoke-direct {v0, p1}, Lb/r;-><init>(Lb/s;)V

    iput-object v0, p0, Lb/O;->o:Lb/r;

    iput-boolean p2, p0, Lb/O;->b:Z

    iput-object v2, p0, Lb/O;->c:Lb/E;

    iput-boolean v1, p0, Lb/O;->e:Z

    iput-boolean v1, p0, Lb/O;->f:Z

    iput-object v2, p0, Lb/O;->g:Lb/o;

    iput-object p3, p0, Lb/O;->h:Lb/Q;

    iput-object p1, p0, Lb/O;->i:Lb/s;

    iput-boolean v1, p0, Lb/O;->k:Z

    iput-boolean v1, p0, Lb/O;->l:Z

    iput-boolean v1, p0, Lb/O;->m:Z

    iput-object p5, p0, Lb/O;->n:Lb/a;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lb/O;->d:Ljava/util/Set;

    return-void
.end method

.method public static a(Lb/s;ZLb/Q;Lb/B;Lb/a;)Lb/O;
    .locals 6

    const/4 v2, 0x1

    iget v0, p3, Lb/B;->l:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p3, Lb/B;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lb/M$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/M$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lb/f$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/f$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lb/L$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/L$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lb/N$b;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/N$b;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lb/H$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/H$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lb/aa$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/aa$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_6
    new-instance v0, Lb/S$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/S$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_7
    new-instance v0, Lb/ab$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/ab$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_8
    new-instance v0, Lb/J$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/J$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_9
    new-instance v0, Lb/I$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/I$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    :pswitch_a
    new-instance v0, Lb/D$a;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb/D$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_9
        :pswitch_8
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private a(Z)V
    .locals 6

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/O;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/O;->a:Lb/B;

    iget-wide v0, v0, Lb/B;->c:J

    invoke-virtual {p0, v0, v1}, Lb/O;->b(J)Lb/s;

    move-result-object v1

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lb/O;->n:Lb/a;

    invoke-virtual {v0}, Lb/a;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "tcp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lb/U;

    iget-object v3, p0, Lb/O;->a:Lb/B;

    iget-object v4, p0, Lb/O;->n:Lb/a;

    move-object v2, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lb/U;-><init>(Lb/s;Lb/O;Lb/B;Lb/a;Z)V

    invoke-virtual {p0, v0}, Lb/O;->a(Lb/C;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lb/O;->n:Lb/a;

    invoke-virtual {v0}, Lb/a;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ipc"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lb/v;

    iget-object v3, p0, Lb/O;->a:Lb/B;

    iget-object v4, p0, Lb/O;->n:Lb/a;

    move-object v2, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lb/v;-><init>(Lb/s;Lb/O;Lb/B;Lb/a;Z)V

    invoke-virtual {p0, v0}, Lb/O;->a(Lb/C;)V

    goto :goto_0

    :cond_4
    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private y()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/O;->f:Z

    invoke-super {p0, v0}, Lb/C;->b(I)V

    return-void
.end method


# virtual methods
.method public final a()Lb/y;
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lb/O;->l:Z

    if-nez v0, :cond_0

    new-instance v0, Lb/y;

    iget-object v1, p0, Lb/O;->a:Lb/B;

    iget-byte v1, v1, Lb/B;->d:B

    invoke-direct {v0, v1}, Lb/y;-><init>(I)V

    iget-object v1, p0, Lb/O;->a:Lb/B;

    iget-object v1, v1, Lb/B;->e:[B

    iget-object v2, p0, Lb/O;->a:Lb/B;

    iget-byte v2, v2, Lb/B;->d:B

    invoke-virtual {v0, v1, v3, v2}, Lb/y;->a([BII)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lb/O;->l:Z

    iput-boolean v3, p0, Lb/O;->e:Z

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->c()Lb/y;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lb/y;->d()Z

    move-result v1

    iput-boolean v1, p0, Lb/O;->e:Z

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    const/4 v1, 0x0

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_0

    sget v0, Lb/O;->j:I

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-boolean v1, p0, Lb/O;->k:Z

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0, v1}, Lb/E;->a(Z)V

    return-void
.end method

.method public final a(Lb/E;)V
    .locals 1

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lb/O;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iput-object p1, p0, Lb/O;->c:Lb/E;

    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0, p0}, Lb/E;->a(Lb/E$a;)V

    return-void
.end method

.method protected final a(Lb/o;)V
    .locals 7

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lb/O;->l()Z

    move-result v0

    if-nez v0, :cond_2

    new-array v0, v4, [Lb/ag;

    aput-object p0, v0, v5

    iget-object v1, p0, Lb/O;->h:Lb/Q;

    aput-object v1, v0, v6

    new-array v1, v4, [Lb/E;

    new-array v2, v4, [I

    iget-object v3, p0, Lb/O;->a:Lb/B;

    iget v3, v3, Lb/B;->b:I

    aput v3, v2, v5

    iget-object v3, p0, Lb/O;->a:Lb/B;

    iget v3, v3, Lb/B;->a:I

    aput v3, v2, v6

    new-array v3, v4, [Z

    iget-object v4, p0, Lb/O;->a:Lb/B;

    iget-boolean v4, v4, Lb/B;->v:Z

    aput-boolean v4, v3, v5

    iget-object v4, p0, Lb/O;->a:Lb/B;

    iget-boolean v4, v4, Lb/B;->w:Z

    aput-boolean v4, v3, v6

    invoke-static {v0, v1, v2, v3}, Lb/E;->a([Lb/ag;[Lb/E;[I[Z)V

    aget-object v0, v1, v5

    invoke-virtual {v0, p0}, Lb/E;->a(Lb/E$a;)V

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    aget-object v0, v1, v5

    iput-object v0, p0, Lb/O;->c:Lb/E;

    iget-object v0, p0, Lb/O;->h:Lb/Q;

    aget-object v1, v1, v6

    invoke-virtual {p0, v0, v1}, Lb/O;->a(Lb/C;Lb/E;)V

    :cond_2
    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lb/O;->g:Lb/o;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    iput-object p1, p0, Lb/O;->g:Lb/o;

    iget-object v0, p0, Lb/O;->g:Lb/o;

    iget-object v1, p0, Lb/O;->i:Lb/s;

    invoke-interface {v0, v1, p0}, Lb/o;->a(Lb/s;Lb/O;)V

    return-void
.end method

.method public a(Lb/y;)Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lb/O;->m:Z

    if-nez v1, :cond_1

    const/16 v1, 0x40

    invoke-virtual {p1, v1}, Lb/y;->a(I)V

    iput-boolean v0, p0, Lb/O;->m:Z

    iget-object v1, p0, Lb/O;->a:Lb/B;

    iget-boolean v1, v1, Lb/B;->y:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lb/O;->c:Lb/E;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v1, p1}, Lb/E;->a(Lb/y;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b(I)V
    .locals 5

    const/4 v0, 0x1

    sget-boolean v1, Lb/O;->p:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lb/O;->f:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lb/O;->c:Lb/E;

    if-nez v1, :cond_1

    invoke-direct {p0}, Lb/O;->y()V

    :goto_0
    return-void

    :cond_1
    iput-boolean v0, p0, Lb/O;->f:Z

    if-lez p1, :cond_3

    sget-boolean v1, Lb/O;->p:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lb/O;->k:Z

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iget-object v1, p0, Lb/O;->o:Lb/r;

    int-to-long v2, p1

    sget v4, Lb/O;->j:I

    invoke-virtual {v1, v2, v3, v4}, Lb/r;->a(JI)V

    iput-boolean v0, p0, Lb/O;->k:Z

    :cond_3
    iget-object v1, p0, Lb/O;->c:Lb/E;

    if-eqz p1, :cond_4

    :goto_1
    invoke-virtual {v1, v0}, Lb/E;->a(Z)V

    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->b()Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eq v0, p1, :cond_0

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/O;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/O;->g:Lb/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/O;->g:Lb/o;

    invoke-interface {v0}, Lb/o;->c()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->b()Z

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eq v0, p1, :cond_0

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/O;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/O;->g:Lb/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/O;->g:Lb/o;

    invoke-interface {v0}, Lb/o;->b()V

    :cond_1
    return-void
.end method

.method public final f()V
    .locals 2

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lb/O;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/O;->o:Lb/r;

    sget v1, Lb/O;->j:I

    invoke-virtual {v0, v1}, Lb/r;->b(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/O;->k:Z

    :cond_1
    iget-object v0, p0, Lb/O;->g:Lb/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/O;->g:Lb/o;

    invoke-interface {v0}, Lb/o;->a()V

    :cond_2
    return-void
.end method

.method public final f(Lb/E;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Must Override"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g(Lb/E;)V
    .locals 1

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lb/O;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-ne v0, p1, :cond_2

    const/4 v0, 0x0

    iput-object v0, p0, Lb/O;->c:Lb/E;

    :goto_0
    iget-boolean v0, p0, Lb/O;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/O;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lb/O;->y()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lb/O;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final g_()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final h_()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected p()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/O;->l:Z

    iput-boolean v0, p0, Lb/O;->m:Z

    return-void
.end method

.method public final q()V
    .locals 1

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->f()V

    :cond_0
    return-void
.end method

.method public final r()Lb/Q;
    .locals 1

    iget-object v0, p0, Lb/O;->h:Lb/Q;

    return-object v0
.end method

.method protected final s()V
    .locals 1

    iget-object v0, p0, Lb/O;->o:Lb/r;

    invoke-virtual {v0, p0}, Lb/r;->a(Lb/t;)V

    iget-boolean v0, p0, Lb/O;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lb/O;->a(Z)V

    :cond_0
    return-void
.end method

.method public final t()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iput-object v3, p0, Lb/O;->g:Lb/o;

    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->e()V

    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->f()V

    :goto_0
    iget-boolean v0, p0, Lb/O;->e:Z

    if-nez v0, :cond_3

    :cond_0
    iget-boolean v0, p0, Lb/O;->b:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lb/O;->b_()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->b()Z

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lb/O;->a()Lb/y;

    move-result-object v0

    if-nez v0, :cond_4

    sget-boolean v0, Lb/O;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/O;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v0}, Lb/y;->h()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lb/O;->a:Lb/B;

    iget v0, v0, Lb/B;->u:I

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lb/O;->n:Lb/a;

    invoke-virtual {v0}, Lb/a;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "pgm"

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lb/O;->n:Lb/a;

    invoke-virtual {v0}, Lb/a;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "epgm"

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->l()V

    iget-object v0, p0, Lb/O;->c:Lb/E;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lb/E;->a(Z)V

    iget-object v0, p0, Lb/O;->d:Ljava/util/Set;

    iget-object v1, p0, Lb/O;->c:Lb/E;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object v3, p0, Lb/O;->c:Lb/E;

    :cond_6
    invoke-virtual {p0}, Lb/O;->p()V

    iget-object v0, p0, Lb/O;->a:Lb/B;

    iget v0, v0, Lb/B;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    invoke-direct {p0, v2}, Lb/O;->a(Z)V

    :cond_7
    iget-object v0, p0, Lb/O;->c:Lb/E;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/O;->a:Lb/B;

    iget v0, v0, Lb/B;->l:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lb/O;->a:Lb/B;

    iget v0, v0, Lb/B;->l:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    :cond_8
    iget-object v0, p0, Lb/O;->c:Lb/E;

    invoke-virtual {v0}, Lb/E;->l()V

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lb/O;->a:Lb/B;

    iget v1, v1, Lb/B;->D:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
