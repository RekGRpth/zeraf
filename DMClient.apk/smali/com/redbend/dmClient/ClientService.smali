.class public Lcom/redbend/dmClient/ClientService;
.super Lcom/redbend/dmcFramework/DmClientBaseService;
.source "ClientService.java"


# static fields
.field public static final DMS_CONFIGURED:I = 0x1397

.field private static final LOG_TAG:Ljava/lang/String; = "ClientService"

.field public static final PROXY_CONFIGURED:I = 0x1398

.field public static final SCREEN_BATTERY_LOW:I = 0x138c

.field public static final SCREEN_CONFIRM_UPDATE:I = 0x138a

.field public static final SCREEN_CONNECTING:I = 0x13a3

.field public static final SCREEN_DOWNLOAD_POLLING:I = 0x13a6

.field public static final SCREEN_FORCE_UPDATE:I = 0x13a5

.field public static final SCREEN_IN_PROGRESS:I = 0x139b

.field public static final SCREEN_IN_ROAMING:I = 0x13a2

.field public static final SCREEN_NETWORK_NOT_AVAILABLE:I = 0x1396

.field public static final SCREEN_NO_STORAGE_SPACE:I = 0x139f

.field public static final SCREEN_NO_UPDATE:I = 0x13a4

.field public static final SCREEN_PROGRESS_BAR:I = 0x138e

.field public static final SCREEN_REST_DOWNLOAD:I = 0x139d

.field public static final SCREEN_SESSION_IN_PROGRESS:I = 0x13a7

.field public static final SCREEN_UPDATE_CANCELLED:I = 0x1390

.field public static final SCREEN_UPDATE_COMPLETE:I = 0x13a0

.field public static final SCREEN_UPDATE_ERROR:I = 0x1399

.field public static final SCREEN_UPDATE_FAILED:I = 0x1395

.field public static final SCREEN_UPDATE_NOW:I = 0x1393

.field public static final SCREEN_UPDATE_NOW_ON_ALARM:I = 0x1394

.field public static final SCREEN_UPDATE_RECORD:I = 0x13a1

.field public static final SCREEN_UPDATE_STATUS_UNKNOWN:I = 0x139a

.field public static final SCREEN_WIRELESS_DOWNLOAD_INTERRUPTION:I = 0x139e

.field public static final SCREEN_WIRELESS_NOT_CONNECTED:I = 0x139c

.field public static final UPDATE_PROGRESS_BAR:I = 0x138f

.field public static final infoMessageTextExtra:Ljava/lang/String; = "infoMessageText"


# instance fields
.field private ipl:Lcom/redbend/dmClient/Ipl;


# direct methods
.method public constructor <init>(Lcom/redbend/dmcFramework/DmcService;)V
    .locals 2
    .param p1    # Lcom/redbend/dmcFramework/DmcService;

    invoke-direct {p0, p1}, Lcom/redbend/dmcFramework/DmClientBaseService;-><init>(Lcom/redbend/dmcFramework/DmcService;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {p0}, Lcom/redbend/dmClient/ClientService;->setClientService()V

    new-instance v0, Lcom/redbend/dmClient/Ipl;

    invoke-virtual {p1}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/redbend/dmClient/Ipl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    return-void
.end method

.method private startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    move-object v0, p2

    check-cast v0, Ljava/lang/Integer;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v2}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "nextStateExtra"

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v2, v1}, Lcom/redbend/dmcFramework/DmcService;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public dispatch(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x1388

    if-ge v4, v5, :cond_0

    const-string v4, "ClientService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error - calling application dispatcher with generic dispatcher action "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const-class v4, Lcom/redbend/dmClient/ConfirmUpdate;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    const-class v4, Lcom/redbend/dmClient/BatteryLow;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_3
    const-class v4, Lcom/redbend/dmClient/ProgressBarScreen;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_4
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    :try_start_0
    iget-object v4, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v4}, Lcom/redbend/dmcFramework/DmcService;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v3, v4, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    check-cast v3, Lcom/redbend/dmClient/ProgressBarScreen;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/redbend/dmClient/ProgressBarScreen;->updateProgress(I)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "ClientService"

    const-string v5, "Screen not loaded yet"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_5
    const-class v4, Lcom/redbend/dmClient/UpdateCancelled;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_6
    const-class v4, Lcom/redbend/dmClient/UpdateNow;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_7
    const-class v4, Lcom/redbend/dmClient/UpdateNowOnAlarm;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_8
    const-class v4, Lcom/redbend/dmClient/UpdateFailed;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_9
    const-class v4, Lcom/redbend/dmClient/UpdateComplete;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_a
    const-class v4, Lcom/redbend/dmClient/NetworkNotAvailable;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_b
    const-class v4, Lcom/redbend/dmClient/UpdateError;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_c
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v4}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/redbend/dmClient/UpdateInProgress;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v4, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v4, v1}, Lcom/redbend/dmcFramework/DmcService;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_d
    const-class v4, Lcom/redbend/dmClient/WirelessNotConnected;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_e
    const-class v4, Lcom/redbend/dmClient/RestDownload;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_f
    const-class v4, Lcom/redbend/dmClient/WirelessDownloadInterruption;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_10
    const-class v4, Lcom/redbend/dmClient/NoStorageSpace;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_11
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v4}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/redbend/dmClient/ShowUnreadInfoMessage;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "notificationObjExtra"

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/os/Bundle;

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v5, "nextStateExtra"

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/os/Bundle;

    const-string v6, "nextState"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v4, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v4, v1}, Lcom/redbend/dmcFramework/DmcService;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_12
    const-class v4, Lcom/redbend/dmClient/InRoaming;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_13
    const-class v4, Lcom/redbend/dmClient/Connecting;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_14
    const-class v4, Lcom/redbend/dmClient/NoUpdate;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_15
    const-class v4, Lcom/redbend/dmClient/ForceUpdate;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_16
    const-class v4, Lcom/redbend/dmClient/StartDownloadFromPolling;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_17
    const-class v4, Lcom/redbend/dmClient/SessionInProgress;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/redbend/dmClient/ClientService;->startSimpleScreen(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x138a
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_9
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public doInitTaskAfterBoot()V
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->doInitTaskAfterBoot()V

    return-void
.end method

.method public getBootDelayInterval()I
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getBootDelayInterval()I

    move-result v0

    return v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFirmwareVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getManName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getManName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getModelName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPackageStorageSize(Ljava/lang/String;)J
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0, p1}, Lcom/redbend/dmClient/Ipl;->getPackageStorageSize(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPollingInterval()I
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getPollingInterval()I

    move-result v0

    return v0
.end method

.method public getSeedByMacAddress()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v1}, Lcom/redbend/dmClient/Ipl;->getMacAddressFromFile()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    iget-object v1, v1, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v1}, Lcom/redbend/dmcFramework/DmcUtils;->getSeedByMacAddress()I

    move-result v0

    :cond_0
    return v0
.end method

.method public getUpdatePackagePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getUpdatePackagePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateResult()I
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getUpdateResult()I

    move-result v0

    return v0
.end method

.method public getUserAgentName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getUserAgentName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBatterySufficient()Z
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->isBatterySufficient()Z

    move-result v0

    return v0
.end method

.method public native isWiFiOnly()Z
.end method

.method public screenBatteryLow(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x138c

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenConfirmUpdate(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x138a

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenConnecting(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x13a3

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenForceUpdate(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x13a5

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenInProgress()V
    .locals 3

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x139b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenInRoaming(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x13a2

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenNetworkNotAvailable(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x1396

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenNoStorageSpace(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x139f

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenNoUpdate(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x13a4

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenProgressBar(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x138e

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenRestDownload(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x139d

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenSessionInProgress(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x13a7

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenStartDownloadPolling(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x13a6

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenUpdateCancelled(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x1390

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenUpdateComplete(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x13a0

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenUpdateError(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x1399

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenUpdateFailed(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x1395

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenUpdateNow(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x1393

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenUpdateNowOnAlarm(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x1394

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenUpdateRecord(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "msg"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "nextState"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v2, 0x13a1

    invoke-virtual {v1, v2, v0}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenUpdateStatusUnknown(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x139a

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenWirelessDownloadInterruption(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x139e

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenWirelessNotConnected(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x139c

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public native setClientService()V
.end method

.method public setFotaAPN(Z)Z
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0, p1}, Lcom/redbend/dmClient/Ipl;->setFotaAPN(Z)Z

    move-result v0

    return v0
.end method

.method public startUpdate()Z
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->ipl:Lcom/redbend/dmClient/Ipl;

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->startUpdate()Z

    move-result v0

    return v0
.end method

.method public updateNotificationIcon(IZ)V
    .locals 10
    .param p1    # I
    .param p2    # Z

    const v3, 0x7f0b0040

    const v1, 0x7f0b0039

    const/4 v7, 0x0

    const v2, 0x7f02005b

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v0, v3}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x1

    const/4 v9, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v0, "ClientService"

    const-string v1, "UN SUPPORTED NOTIFICATION FORMAT"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    iget-object v0, v0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    if-eqz p2, :cond_1

    const/4 v3, 0x1

    :goto_1
    iget-object v1, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v5, 0x7f0b0025

    invoke-virtual {v1, v5}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v5

    move v1, p1

    invoke-virtual/range {v0 .. v9}, Lcom/redbend/dmcFramework/DmcUtils;->showNotification(IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    return-void

    :pswitch_0
    const v2, 0x7f02005c

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b0034

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b003c

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    goto :goto_0

    :pswitch_1
    const v2, 0x7f02005e

    invoke-virtual {p0}, Lcom/redbend/dmClient/ClientService;->isWiFiOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b0036

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_2
    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b003d

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b0035

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :pswitch_2
    const v2, 0x7f02005d

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b0037

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b003e

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    goto :goto_0

    :pswitch_3
    const v2, 0x7f02005a

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b0038

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b003f

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x1

    goto :goto_0

    :pswitch_4
    const v2, 0x7f02005b

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v0, v3}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :pswitch_5
    const v2, 0x7f020065

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b003a

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b0041

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :pswitch_6
    const v2, 0x7f020063

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b003b

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    const v1, 0x7f0b0042

    invoke-virtual {v0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_1
    move v3, v7

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public updateProgressBar(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmClient/ClientService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x138f

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method
