.class public Lcom/redbend/dmClient/ConfirmUpdate;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "ConfirmUpdate.java"


# static fields
.field public static first:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/redbend/dmClient/ConfirmUpdate;->first:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/redbend/dmClient/ConfirmUpdate;)Lcom/redbend/dmcFramework/DmcApplication;
    .locals 1
    .param p0    # Lcom/redbend/dmClient/ConfirmUpdate;

    iget-object v0, p0, Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;

    return-object v0
.end method

.method static synthetic access$100(Lcom/redbend/dmClient/ConfirmUpdate;)Lcom/redbend/dmcFramework/DmcApplication;
    .locals 1
    .param p0    # Lcom/redbend/dmClient/ConfirmUpdate;

    iget-object v0, p0, Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;

    return-object v0
.end method

.method static synthetic access$200(Lcom/redbend/dmClient/ConfirmUpdate;)Lcom/redbend/dmcFramework/DmcApplication;
    .locals 1
    .param p0    # Lcom/redbend/dmClient/ConfirmUpdate;

    iget-object v0, p0, Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;

    return-object v0
.end method

.method static synthetic access$300(Lcom/redbend/dmClient/ConfirmUpdate;)Lcom/redbend/dmcFramework/DmcApplication;
    .locals 1
    .param p0    # Lcom/redbend/dmClient/ConfirmUpdate;

    iget-object v0, p0, Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v2, "ConfirmUpdate"

    iput-object v2, p0, Lcom/redbend/dmClient/ConfirmUpdate;->LOG_TAG:Ljava/lang/String;

    iget-object v2, p0, Lcom/redbend/dmClient/ConfirmUpdate;->LOG_TAG:Ljava/lang/String;

    const-string v3, "onCreate()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;

    invoke-virtual {v2}, Lcom/redbend/dmcFramework/DmcApplication;->getConfig()Lcom/gmobi/utils/AppConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/gmobi/utils/AppConfig;->menuIsOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/redbend/dmClient/ConfirmUpdate;->setActionMenuEnable()V

    :cond_0
    const v2, 0x7f03001c

    invoke-virtual {p0, v2}, Lcom/redbend/dmClient/ConfirmUpdate;->setContentView(I)V

    sget-boolean v2, Lcom/redbend/dmClient/ConfirmUpdate;->first:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    sput-boolean v2, Lcom/redbend/dmClient/ConfirmUpdate;->first:Z

    new-instance v0, Lcom/redbend/dmClient/Ipl;

    invoke-direct {v0, p0}, Lcom/redbend/dmClient/Ipl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/redbend/dmClient/Ipl;->getManName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/redbend/dmClient/ConfirmUpdate;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "Starmobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, p0}, Lcom/redbend/dmClient/Ipl;->createTipsDialog(Landroid/content/Context;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method

.method public setContentView(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/redbend/dmClient/ConfirmUpdate;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setContentView()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->setContentView(I)V

    const v1, 0x7f06003e

    invoke-virtual {p0, v1}, Lcom/redbend/dmClient/ConfirmUpdate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/redbend/dmClient/ConfirmUpdate$1;

    invoke-direct {v1, p0}, Lcom/redbend/dmClient/ConfirmUpdate$1;-><init>(Lcom/redbend/dmClient/ConfirmUpdate;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
