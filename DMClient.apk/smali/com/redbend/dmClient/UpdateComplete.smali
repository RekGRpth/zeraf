.class public Lcom/redbend/dmClient/UpdateComplete;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "UpdateComplete.java"


# instance fields
.field wi:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v4, "UpdateComplete"

    iput-object v4, p0, Lcom/redbend/dmClient/UpdateComplete;->LOG_TAG:Ljava/lang/String;

    iget-object v4, p0, Lcom/redbend/dmClient/UpdateComplete;->LOG_TAG:Ljava/lang/String;

    const-string v5, "onCreate()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/redbend/dmClient/UpdateComplete;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    const v4, 0x10000006

    iget-object v5, p0, Lcom/redbend/dmClient/UpdateComplete;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/redbend/dmClient/UpdateComplete;->wi:Landroid/os/PowerManager$WakeLock;

    const v4, 0x7f03002f

    invoke-virtual {p0, v4}, Lcom/redbend/dmClient/UpdateComplete;->setContentView(I)V

    new-instance v1, Lcom/redbend/dmClient/Ipl;

    invoke-direct {v1, p0}, Lcom/redbend/dmClient/Ipl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/redbend/dmClient/Ipl;->getManName()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/redbend/dmClient/UpdateComplete;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "Q-Smart"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/redbend/dmClient/UpdateComplete;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Post locat Data."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/redbend/dmClient/HttpService;

    invoke-direct {v0, p0}, Lcom/redbend/dmClient/HttpService;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/redbend/dmClient/HttpService;->postLocalData()V

    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/redbend/dmClient/UpdateComplete;->finishOnStop:Z

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/redbend/dmClient/UpdateComplete;->wi:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x3a98

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    invoke-super {p0}, Lcom/redbend/dmcFramework/DmcActivity;->onResume()V

    return-void
.end method
