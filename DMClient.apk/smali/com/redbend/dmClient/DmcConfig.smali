.class public Lcom/redbend/dmClient/DmcConfig;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "DmcConfig.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/redbend/dmClient/DmcConfig;I)V
    .locals 0
    .param p0    # Lcom/redbend/dmClient/DmcConfig;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/redbend/dmClient/DmcConfig;->saveCntSet(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/redbend/dmClient/DmcConfig;Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Lcom/redbend/dmClient/DmcConfig;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/redbend/dmClient/DmcConfig;->saveBooleanData(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/redbend/dmClient/DmcConfig;)V
    .locals 0
    .param p0    # Lcom/redbend/dmClient/DmcConfig;

    invoke-direct {p0}, Lcom/redbend/dmClient/DmcConfig;->back2ConfirmUpdate()V

    return-void
.end method

.method private back2ConfirmUpdate()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/redbend/dmClient/ConfirmUpdate;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/redbend/dmClient/DmcConfig;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private saveBooleanData(Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static {p0, p1, p2}, Lcom/gmobi/utils/AppSettingStorageHelper;->setAppConfig(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method private saveCntSet(I)V
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "CntAuto"

    :goto_0
    const-string v1, "CntSet"

    invoke-static {p0, v1, v0}, Lcom/gmobi/utils/AppSettingStorageHelper;->setAppConfig(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const-string v0, "CntWifiOnly"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f060043
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v3, "Config"

    iput-object v3, p0, Lcom/redbend/dmClient/DmcConfig;->LOG_TAG:Ljava/lang/String;

    iget-object v3, p0, Lcom/redbend/dmClient/DmcConfig;->LOG_TAG:Ljava/lang/String;

    const-string v4, "onCreate()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmClient/DmcConfig;->setButtonFuncDesable()V

    const v3, 0x7f03001a

    invoke-virtual {p0, v3}, Lcom/redbend/dmClient/DmcConfig;->setContentView(I)V

    new-instance v2, Lcom/redbend/dmcFramework/DmcUtils;

    invoke-direct {v2, p0}, Lcom/redbend/dmcFramework/DmcUtils;-><init>(Landroid/content/Context;)V

    const v3, 0x7f060042

    invoke-virtual {p0, v3}, Lcom/redbend/dmClient/DmcConfig;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Lcom/redbend/dmcFramework/DmcUtils;->getConnectCfgSet()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CntWifiOnly"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f060043

    invoke-virtual {v1, v3}, Landroid/widget/RadioGroup;->check(I)V

    :goto_0
    const v3, 0x7f060045

    invoke-virtual {p0, v3}, Lcom/redbend/dmClient/DmcConfig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v2}, Lcom/redbend/dmcFramework/DmcUtils;->getRoamingSet()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void

    :cond_0
    const v3, 0x7f060044

    invoke-virtual {v1, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0
.end method

.method public setContentView(I)V
    .locals 4
    .param p1    # I

    iget-object v2, p0, Lcom/redbend/dmClient/DmcConfig;->LOG_TAG:Ljava/lang/String;

    const-string v3, "setContentView()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->setContentView(I)V

    const v2, 0x7f06003e

    invoke-virtual {p0, v2}, Lcom/redbend/dmClient/DmcConfig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lcom/redbend/dmClient/DmcConfig$1;

    invoke-direct {v2, p0}, Lcom/redbend/dmClient/DmcConfig$1;-><init>(Lcom/redbend/dmClient/DmcConfig;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f060041

    invoke-virtual {p0, v2}, Lcom/redbend/dmClient/DmcConfig;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/redbend/dmClient/DmcConfig$2;

    invoke-direct {v2, p0}, Lcom/redbend/dmClient/DmcConfig$2;-><init>(Lcom/redbend/dmClient/DmcConfig;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
