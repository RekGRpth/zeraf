.class Lcom/redbend/dmClient/DmcConfig$1;
.super Ljava/lang/Object;
.source "DmcConfig.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/redbend/dmClient/DmcConfig;->setContentView(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/redbend/dmClient/DmcConfig;


# direct methods
.method constructor <init>(Lcom/redbend/dmClient/DmcConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/redbend/dmClient/DmcConfig$1;->this$0:Lcom/redbend/dmClient/DmcConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v2, p0, Lcom/redbend/dmClient/DmcConfig$1;->this$0:Lcom/redbend/dmClient/DmcConfig;

    const v3, 0x7f060042

    invoke-virtual {v2, v3}, Lcom/redbend/dmClient/DmcConfig;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iget-object v2, p0, Lcom/redbend/dmClient/DmcConfig$1;->this$0:Lcom/redbend/dmClient/DmcConfig;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    # invokes: Lcom/redbend/dmClient/DmcConfig;->saveCntSet(I)V
    invoke-static {v2, v3}, Lcom/redbend/dmClient/DmcConfig;->access$000(Lcom/redbend/dmClient/DmcConfig;I)V

    iget-object v2, p0, Lcom/redbend/dmClient/DmcConfig$1;->this$0:Lcom/redbend/dmClient/DmcConfig;

    const v3, 0x7f060045

    invoke-virtual {v2, v3}, Lcom/redbend/dmClient/DmcConfig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/redbend/dmClient/DmcConfig$1;->this$0:Lcom/redbend/dmClient/DmcConfig;

    const-string v3, "RoamingSet"

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    # invokes: Lcom/redbend/dmClient/DmcConfig;->saveBooleanData(Ljava/lang/String;Z)V
    invoke-static {v2, v3, v4}, Lcom/redbend/dmClient/DmcConfig;->access$100(Lcom/redbend/dmClient/DmcConfig;Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/redbend/dmClient/DmcConfig$1;->this$0:Lcom/redbend/dmClient/DmcConfig;

    # invokes: Lcom/redbend/dmClient/DmcConfig;->back2ConfirmUpdate()V
    invoke-static {v2}, Lcom/redbend/dmClient/DmcConfig;->access$200(Lcom/redbend/dmClient/DmcConfig;)V

    return-void
.end method
