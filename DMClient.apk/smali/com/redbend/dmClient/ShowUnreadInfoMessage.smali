.class public Lcom/redbend/dmClient/ShowUnreadInfoMessage;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "ShowUnreadInfoMessage.java"


# instance fields
.field private bundle:Landroid/os/Bundle;

.field private messageText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "ShowUnreadInfoMessage"

    iput-object v0, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->LOG_TAG:Ljava/lang/String;

    iget-object v0, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notificationObjExtra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->bundle:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->bundle:Landroid/os/Bundle;

    const-string v1, "msg"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->messageText:Ljava/lang/String;

    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->setContentView(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->finishOnStop:Z

    return-void
.end method

.method public setContentView(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setContentView()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->setContentView(I)V

    const v1, 0x7f06003c

    invoke-virtual {p0, v1}, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->messageText:Ljava/lang/String;

    if-nez v1, :cond_0

    const v1, 0x7f0b0048

    invoke-virtual {p0, v1}, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->messageText:Ljava/lang/String;

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b002d

    invoke-virtual {p0, v2}, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/redbend/dmClient/ShowUnreadInfoMessage;->messageText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
