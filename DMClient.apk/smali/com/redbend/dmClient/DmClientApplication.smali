.class public Lcom/redbend/dmClient/DmClientApplication;
.super Lcom/redbend/dmcFramework/DmcApplication;
.source "DmClientApplication.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcApplication;-><init>()V

    return-void
.end method


# virtual methods
.method public getClientServiceClassName()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/redbend/dmClient/ClientService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    invoke-super {p0}, Lcom/redbend/dmcFramework/DmcApplication;->onCreate()V

    const/4 v3, 0x0

    sput-boolean v3, Lcom/gmobi/trade/TradeService;->DEBUG:Z

    const-string v0, ""

    const-string v1, ""

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "MainActivity"

    const-string v4, "com.redbend.dmcFramework.DmcMain"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0, v1, v2}, Lcom/gmobi/trade/TradeService;->start(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method
