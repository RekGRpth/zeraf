.class public Lcom/redbend/dmClient/Notification;
.super Landroid/app/Activity;
.source "Notification.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "Notification"

    sput-object v0, Lcom/redbend/dmClient/Notification;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/redbend/dmClient/Notification;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "notificationIdExtra"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget-object v2, Lcom/redbend/dmClient/Notification;->LOG_TAG:Ljava/lang/String;

    const-string v3, "onCreate()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/redbend/dmClient/Notification;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/redbend/dmcFramework/DmcService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "serviceStartReason"

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "notificationIdExtra"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/redbend/dmClient/Notification;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
