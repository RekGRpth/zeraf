.class public Lcom/redbend/dmClient/UpdateNow;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "UpdateNow.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/redbend/dmClient/UpdateNow;)Lcom/redbend/dmcFramework/DmcApplication;
    .locals 1
    .param p0    # Lcom/redbend/dmClient/UpdateNow;

    iget-object v0, p0, Lcom/redbend/dmClient/UpdateNow;->a:Lcom/redbend/dmcFramework/DmcApplication;

    return-object v0
.end method

.method static synthetic access$100(Lcom/redbend/dmClient/UpdateNow;)Lcom/redbend/dmcFramework/DmcApplication;
    .locals 1
    .param p0    # Lcom/redbend/dmClient/UpdateNow;

    iget-object v0, p0, Lcom/redbend/dmClient/UpdateNow;->a:Lcom/redbend/dmcFramework/DmcApplication;

    return-object v0
.end method


# virtual methods
.method public createTipsDialog(Landroid/content/Context;)Landroid/app/AlertDialog;
    .locals 7
    .param p1    # Landroid/content/Context;

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const v4, 0x7f0b004f

    invoke-virtual {p0, v4}, Lcom/redbend/dmClient/UpdateNow;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v4, 0x78

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMinimumHeight(I)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0b0013

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0b0015

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v3, "UpdateNow"

    iput-object v3, p0, Lcom/redbend/dmClient/UpdateNow;->LOG_TAG:Ljava/lang/String;

    iget-object v3, p0, Lcom/redbend/dmClient/UpdateNow;->LOG_TAG:Ljava/lang/String;

    const-string v4, "onCreate()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v3, 0x7f030033

    invoke-virtual {p0, v3}, Lcom/redbend/dmClient/UpdateNow;->setContentView(I)V

    new-instance v1, Lcom/redbend/dmClient/Ipl;

    invoke-direct {v1, p0}, Lcom/redbend/dmClient/Ipl;-><init>(Landroid/content/Context;)V

    move-object v2, p0

    const v3, 0x7f06003e

    invoke-virtual {p0, v3}, Lcom/redbend/dmClient/UpdateNow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v3, Lcom/redbend/dmClient/UpdateNow$1;

    invoke-direct {v3, p0, v1, v2}, Lcom/redbend/dmClient/UpdateNow$1;-><init>(Lcom/redbend/dmClient/UpdateNow;Lcom/redbend/dmClient/Ipl;Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
