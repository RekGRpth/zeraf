.class Lcom/redbend/vdm/comm/CommConnProxy;
.super Ljava/lang/Object;
.source "CommConnProxy.java"


# static fields
.field private static _connId:I


# instance fields
.field private _connections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/redbend/vdm/comm/CommRawConnection;",
            ">;"
        }
    .end annotation
.end field

.field private _factory:Lcom/redbend/vdm/comm/CommFactory;

.field private _timeout:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/redbend/vdm/comm/CommConnProxy;->initIDs()V

    const/4 v0, 0x0

    sput v0, Lcom/redbend/vdm/comm/CommConnProxy;->_connId:I

    return-void
.end method

.method constructor <init>(Lcom/redbend/vdm/comm/CommFactory;)V
    .locals 1
    .param p1    # Lcom/redbend/vdm/comm/CommFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1e

    iput v0, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_timeout:I

    invoke-direct {p0}, Lcom/redbend/vdm/comm/CommConnProxy;->initInstance()V

    iput-object p1, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_factory:Lcom/redbend/vdm/comm/CommFactory;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_connections:Ljava/util/ArrayList;

    return-void
.end method

.method private static native initIDs()V
.end method

.method private native initInstance()V
.end method


# virtual methods
.method close(Lcom/redbend/vdm/comm/CommRawConnection;)V
    .locals 0
    .param p1    # Lcom/redbend/vdm/comm/CommRawConnection;

    invoke-interface {p1}, Lcom/redbend/vdm/comm/CommRawConnection;->close()V

    return-void
.end method

.method protected native destroyInstance()V
.end method

.method init(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/redbend/vdm/comm/CommRawConnection;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/redbend/vdm/comm/VdmCommException;
        }
    .end annotation

    iget-object v1, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_factory:Lcom/redbend/vdm/comm/CommFactory;

    invoke-interface {v1}, Lcom/redbend/vdm/comm/CommFactory;->createRawConnection()Lcom/redbend/vdm/comm/CommRawConnection;

    move-result-object v0

    invoke-static {}, Lcom/redbend/vdm/comm/CommHttpAuth$Level;->values()[Lcom/redbend/vdm/comm/CommHttpAuth$Level;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-interface {v0, p1, v1, p3, p4}, Lcom/redbend/vdm/comm/CommRawConnection;->init(Ljava/lang/String;Lcom/redbend/vdm/comm/CommHttpAuth$Level;Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_timeout:I

    invoke-interface {v0, v1}, Lcom/redbend/vdm/comm/CommRawConnection;->setConnectionTimeout(I)V

    iget-object v1, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_connections:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method open(Ljava/lang/String;Lcom/redbend/vdm/comm/CommRawConnection;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/redbend/vdm/comm/CommRawConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/redbend/vdm/comm/VdmCommException;
        }
    .end annotation

    invoke-interface {p2, p1}, Lcom/redbend/vdm/comm/CommRawConnection;->open(Ljava/lang/String;)V

    sget v0, Lcom/redbend/vdm/comm/CommConnProxy;->_connId:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/redbend/vdm/comm/CommConnProxy;->_connId:I

    return v0
.end method

.method receive([BLcom/redbend/vdm/comm/CommRawConnection;)I
    .locals 1
    .param p1    # [B
    .param p2    # Lcom/redbend/vdm/comm/CommRawConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/redbend/vdm/comm/VdmCommException;
        }
    .end annotation

    invoke-interface {p2, p1}, Lcom/redbend/vdm/comm/CommRawConnection;->receive([B)I

    move-result v0

    return v0
.end method

.method send([BLcom/redbend/vdm/comm/CommRawConnection;)V
    .locals 0
    .param p1    # [B
    .param p2    # Lcom/redbend/vdm/comm/CommRawConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/redbend/vdm/comm/VdmCommException;
        }
    .end annotation

    invoke-interface {p2, p1}, Lcom/redbend/vdm/comm/CommRawConnection;->send([B)V

    return-void
.end method

.method public setConnectionTimeout(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_timeout:I

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_connections:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_connections:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/redbend/vdm/comm/CommRawConnection;

    invoke-interface {v0, p1}, Lcom/redbend/vdm/comm/CommRawConnection;->setConnectionTimeout(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method term(Lcom/redbend/vdm/comm/CommRawConnection;)V
    .locals 1
    .param p1    # Lcom/redbend/vdm/comm/CommRawConnection;

    iget-object v0, p0, Lcom/redbend/vdm/comm/CommConnProxy;->_connections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
