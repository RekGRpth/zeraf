.class Lcom/redbend/vdm/comm/VdmRawConnection;
.super Ljava/lang/Object;
.source "VdmRawConnection.java"

# interfaces
.implements Lcom/redbend/vdm/comm/CommRawConnection;


# static fields
.field private static final DEAFULT_HTTPS_PORT:I = 0x1bb

.field private static final DEAFULT_HTTP_PORT:I = 0x50

.field protected static final SOCKET_TIMEOUT:I = 0x1e


# instance fields
.field private _conn:Ljava/net/Socket;

.field private _in:Ljava/io/DataInputStream;

.field private _out:Ljava/io/DataOutputStream;

.field private _proxy:Ljava/lang/String;

.field private _proxyAuthLevel:Lcom/redbend/vdm/comm/CommHttpAuth$Level;

.field private _proxyUsernamePassword:Ljava/lang/String;

.field private _timeout:I

.field private _userAgent:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxy:Ljava/lang/String;

    iput-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_userAgent:Ljava/lang/String;

    sget-object v0, Lcom/redbend/vdm/comm/CommHttpAuth$Level;->NONE:Lcom/redbend/vdm/comm/CommHttpAuth$Level;

    iput-object v0, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxyAuthLevel:Lcom/redbend/vdm/comm/CommHttpAuth$Level;

    iput-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxyUsernamePassword:Ljava/lang/String;

    iput-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    iput-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_in:Ljava/io/DataInputStream;

    iput-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_out:Ljava/io/DataOutputStream;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_in:Ljava/io/DataInputStream;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_in:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    :cond_0
    iget-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_out:Ljava/io/DataOutputStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_out:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    :cond_1
    iget-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "vDM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RawConnection#close: Caught IOException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public init(Ljava/lang/String;Lcom/redbend/vdm/comm/CommHttpAuth$Level;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/redbend/vdm/comm/CommHttpAuth$Level;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/redbend/vdm/comm/VdmCommException;
        }
    .end annotation

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxy:Ljava/lang/String;

    :cond_0
    if-eqz p4, :cond_2

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_userAgent:Ljava/lang/String;

    :goto_0
    if-eqz p2, :cond_1

    iput-object p2, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxyAuthLevel:Lcom/redbend/vdm/comm/CommHttpAuth$Level;

    :cond_1
    if-eqz p3, :cond_3

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxyUsernamePassword:Ljava/lang/String;

    :goto_1
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lcom/redbend/vdm/comm/VdmRawConnection;->setConnectionTimeout(I)V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/String;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_userAgent:Ljava/lang/String;

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/String;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxyUsernamePassword:Ljava/lang/String;

    goto :goto_1
.end method

.method public open(Ljava/lang/String;)V
    .locals 21
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/redbend/vdm/comm/VdmCommException;
        }
    .end annotation

    const/16 v17, 0x0

    const/16 v19, 0x0

    new-instance v4, Ljava/lang/String;

    const-string v3, ""

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const-string v3, "vDM"

    const-string v5, "VdmRawConnection#open"

    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v20, Ljava/net/URL;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_11
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    :try_start_1
    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->getPort()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxy:Ljava/lang/String;

    if-eqz v3, :cond_1

    new-instance v18, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxy:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7

    :try_start_2
    invoke-virtual/range {v18 .. v18}, Ljava/net/URL;->getPort()I

    move-result v16

    const/4 v3, -0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_0

    new-instance v3, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->BAD_URL:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v5, v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v3, v5}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v3
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    :catch_0
    move-exception v11

    move-object/from16 v19, v20

    move-object/from16 v17, v18

    :goto_0
    const-string v3, "vDM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VdmRawConnection#open: SocketException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_SOCKET_ERROR:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v5, v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v3, v5}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v3

    :cond_0
    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8

    move-result-object v4

    move-object/from16 v17, v18

    :cond_1
    :try_start_4
    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_2

    const-string v3, "https"

    invoke-virtual {v15, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "http"

    invoke-virtual {v15, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    new-instance v3, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_BAD_PROTOCOL:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v5, v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v3, v5}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v3

    :catch_1
    move-exception v11

    move-object/from16 v19, v20

    goto :goto_0

    :cond_3
    const-string v3, "https"

    invoke-virtual {v15, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, -0x1

    if-ne v14, v3, :cond_4

    const/16 v14, 0x1bb

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxy:Ljava/lang/String;

    if-nez v3, :cond_5

    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v12

    check-cast v12, Ljavax/net/ssl/HttpsURLConnection;

    new-instance v3, Lcom/redbend/vdm/comm/VdmHostnameVerifier;

    invoke-direct {v3}, Lcom/redbend/vdm/comm/VdmHostnameVerifier;-><init>()V

    invoke-virtual {v12, v3}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    invoke-virtual {v12}, Ljavax/net/ssl/HttpsURLConnection;->getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    new-instance v5, Ljava/net/InetSocketAddress;

    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v14}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_timeout:I

    invoke-virtual {v3, v5, v6}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Ljava/net/Socket;->setSoLinger(ZI)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_timeout:I

    invoke-virtual {v3, v5}, Ljava/net/Socket;->setSoTimeout(I)V

    new-instance v3, Ljava/io/DataOutputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_out:Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/DataInputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_in:Ljava/io/DataInputStream;

    return-void

    :cond_5
    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v13

    check-cast v13, Ljavax/net/ssl/HttpsURLConnection;

    new-instance v3, Lcom/redbend/vdm/comm/VdmHostnameVerifier;

    invoke-direct {v3}, Lcom/redbend/vdm/comm/VdmHostnameVerifier;-><init>()V

    invoke-virtual {v13, v3}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    new-instance v2, Lcom/redbend/vdm/comm/SSLTunnelSocketFactory;

    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxyAuthLevel:Lcom/redbend/vdm/comm/CommHttpAuth$Level;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxyUsernamePassword:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_userAgent:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_timeout:I

    invoke-direct/range {v2 .. v8}, Lcom/redbend/vdm/comm/SSLTunnelSocketFactory;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/redbend/vdm/comm/CommHttpAuth$Level;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v13, v2}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v14}, Lcom/redbend/vdm/comm/SSLTunnelSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;
    :try_end_4
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    goto :goto_1

    :catch_2
    move-exception v11

    move-object/from16 v19, v20

    :goto_2
    const-string v3, "vDM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VdmRawConnection#open: SocketTimeoutException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_SOCKET_TIMEOUT:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v5, v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v3, v5}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v3

    :cond_6
    :try_start_5
    new-instance v9, Ljava/lang/String;

    const-string v3, ""

    invoke-direct {v9, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/4 v10, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_proxy:Ljava/lang/String;

    if-nez v3, :cond_8

    const/4 v3, -0x1

    if-ne v14, v3, :cond_7

    const/16 v14, 0x50

    :cond_7
    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v9

    move v10, v14

    :goto_3
    new-instance v3, Ljava/net/Socket;

    invoke-direct {v3}, Ljava/net/Socket;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_conn:Ljava/net/Socket;

    new-instance v5, Ljava/net/InetSocketAddress;

    invoke-direct {v5, v9, v10}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/redbend/vdm/comm/VdmRawConnection;->_timeout:I

    invoke-virtual {v3, v5, v6}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V
    :try_end_5
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/net/MalformedURLException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    goto/16 :goto_1

    :catch_3
    move-exception v11

    move-object/from16 v19, v20

    :goto_4
    const-string v3, "vDM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VdmRawConnection#open: IllegalArgumentException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->BAD_URL:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v5, v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v3, v5}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v3

    :cond_8
    :try_start_6
    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->getPort()I
    :try_end_6
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/net/MalformedURLException; {:try_start_6 .. :try_end_6} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    move-result v10

    goto :goto_3

    :catch_4
    move-exception v11

    :goto_5
    const-string v3, "vDM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VdmRawConnection#open: MalformedURLException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->BAD_URL:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v5, v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v3, v5}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v3

    :catch_5
    move-exception v11

    :goto_6
    const-string v3, "vDM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VdmRawConnection#open: UnknownHostException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->BAD_URL:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v5, v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v3, v5}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v3

    :catch_6
    move-exception v11

    :goto_7
    const-string v3, "vDM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VdmRawConnection#open: IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_SOCKET_ERROR:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v5, v5, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v3, v5}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v3

    :catch_7
    move-exception v11

    move-object/from16 v19, v20

    goto :goto_7

    :catch_8
    move-exception v11

    move-object/from16 v19, v20

    move-object/from16 v17, v18

    goto :goto_7

    :catch_9
    move-exception v11

    move-object/from16 v19, v20

    goto :goto_6

    :catch_a
    move-exception v11

    move-object/from16 v19, v20

    move-object/from16 v17, v18

    goto :goto_6

    :catch_b
    move-exception v11

    move-object/from16 v19, v20

    goto/16 :goto_5

    :catch_c
    move-exception v11

    move-object/from16 v19, v20

    move-object/from16 v17, v18

    goto/16 :goto_5

    :catch_d
    move-exception v11

    goto/16 :goto_4

    :catch_e
    move-exception v11

    move-object/from16 v19, v20

    move-object/from16 v17, v18

    goto/16 :goto_4

    :catch_f
    move-exception v11

    goto/16 :goto_2

    :catch_10
    move-exception v11

    move-object/from16 v19, v20

    move-object/from16 v17, v18

    goto/16 :goto_2

    :catch_11
    move-exception v11

    goto/16 :goto_0
.end method

.method public receive([B)I
    .locals 5
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/redbend/vdm/comm/VdmCommException;
        }
    .end annotation

    const-string v2, "vDM"

    const-string v3, "RawConnection#receive"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_in:Ljava/io/DataInputStream;

    invoke-virtual {v2, p1}, Ljava/io/DataInputStream;->read([B)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    const-string v2, "vDM"

    const-string v3, "RawConnection#receive: nothing received"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v3, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_SOCKET_ERROR:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v3, v3, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v2, v3}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v2
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v1

    const-string v2, "vDM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RawConnection#receive: SocketTimeoutException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v3, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_SOCKET_TIMEOUT:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v3, v3, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v2, v3}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v2

    :catch_1
    move-exception v1

    const-string v2, "vDM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RawConnection#receive: IOException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v3, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_SOCKET_ERROR:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v3, v3, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v2, v3}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v2

    :cond_0
    return v0
.end method

.method public send([B)V
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/redbend/vdm/comm/VdmCommException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_out:Ljava/io/DataOutputStream;

    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    const-string v1, "vDM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RawConnection#send: SocketTimeoutException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v2, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_SOCKET_TIMEOUT:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v2, v2, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v1, v2}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v1

    :catch_1
    move-exception v0

    const-string v1, "vDM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RawConnection#send: IOException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/redbend/vdm/comm/VdmCommException;

    sget-object v2, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->COMMS_SOCKET_ERROR:Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;

    iget v2, v2, Lcom/redbend/vdm/comm/VdmCommException$VdmCommError;->val:I

    invoke-direct {v1, v2}, Lcom/redbend/vdm/comm/VdmCommException;-><init>(I)V

    throw v1
.end method

.method public setConnectionTimeout(I)V
    .locals 1
    .param p1    # I

    if-lez p1, :cond_0

    mul-int/lit16 v0, p1, 0x3e8

    iput v0, p0, Lcom/redbend/vdm/comm/VdmRawConnection;->_timeout:I

    :cond_0
    return-void
.end method
