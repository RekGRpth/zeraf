.class public Lcom/redbend/dmcFramework/Hex;
.super Ljava/lang/Object;
.source "Hex.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "HEX"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public Decode(Ljava/lang/String;)[B
    .locals 8
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    div-int/lit8 v0, v5, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    rem-int/lit8 v5, v5, 0x2

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    new-array v4, v0, [B

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_1

    add-int/lit8 v5, v2, 0x2

    :try_start_0
    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_1
    :try_start_1
    div-int/lit8 v5, v2, 0x2

    const/16 v6, 0x10

    invoke-static {v3, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v4, v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v5, "HEX"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_1
    return-object v4
.end method

.method public Encode([B)Ljava/lang/String;
    .locals 7
    .param p1    # [B

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    array-length v5, p1

    if-gtz v5, :cond_1

    :cond_0
    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_1
    const/16 v5, 0x10

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "1"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "2"

    aput-object v6, v3, v5

    const/4 v5, 0x3

    const-string v6, "3"

    aput-object v6, v3, v5

    const/4 v5, 0x4

    const-string v6, "4"

    aput-object v6, v3, v5

    const/4 v5, 0x5

    const-string v6, "5"

    aput-object v6, v3, v5

    const/4 v5, 0x6

    const-string v6, "6"

    aput-object v6, v3, v5

    const/4 v5, 0x7

    const-string v6, "7"

    aput-object v6, v3, v5

    const/16 v5, 0x8

    const-string v6, "8"

    aput-object v6, v3, v5

    const/16 v5, 0x9

    const-string v6, "9"

    aput-object v6, v3, v5

    const/16 v5, 0xa

    const-string v6, "A"

    aput-object v6, v3, v5

    const/16 v5, 0xb

    const-string v6, "B"

    aput-object v6, v3, v5

    const/16 v5, 0xc

    const-string v6, "C"

    aput-object v6, v3, v5

    const/16 v5, 0xd

    const-string v6, "D"

    aput-object v6, v3, v5

    const/16 v5, 0xe

    const-string v6, "E"

    aput-object v6, v3, v5

    const/16 v5, 0xf

    const-string v6, "F"

    aput-object v6, v3, v5

    new-instance v2, Ljava/lang/StringBuffer;

    array-length v5, p1

    mul-int/lit8 v5, v5, 0x2

    invoke-direct {v2, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    :goto_1
    array-length v5, p1

    if-ge v1, v5, :cond_2

    aget-byte v5, p1, v1

    and-int/lit16 v5, v5, 0xf0

    int-to-byte v0, v5

    ushr-int/lit8 v5, v0, 0x4

    int-to-byte v0, v5

    and-int/lit8 v5, v0, 0xf

    int-to-byte v0, v5

    aget-object v5, v3, v0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    aget-byte v5, p1, v1

    and-int/lit8 v5, v5, 0xf

    int-to-byte v0, v5

    aget-object v5, v3, v0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    goto/16 :goto_0
.end method
