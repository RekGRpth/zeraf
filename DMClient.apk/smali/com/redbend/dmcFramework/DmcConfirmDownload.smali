.class public Lcom/redbend/dmcFramework/DmcConfirmDownload;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "DmcConfirmDownload.java"


# instance fields
.field DownloadDescriptor:Ljava/lang/String;

.field LOG_TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    const-string v0, "DmcConfirmDownload"

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcConfirmDownload;->LOG_TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcConfirmDownload;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0b004b

    invoke-virtual {p0, v1}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0b004c

    invoke-virtual {p0, v1}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "confirmDownloadBundleExtra"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "description"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0b004d

    invoke-virtual {p0, v1}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "confirmDownloadBundleExtra"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "fileSize"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {p0, v1}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcConfirmDownload;->DownloadDescriptor:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const v0, 0x7f03001b

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->setContentView(I)V

    return-void
.end method

.method public setContentView(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcConfirmDownload;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setContentView()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->setContentView(I)V

    const v1, 0x7f060046

    invoke-virtual {p0, v1}, Lcom/redbend/dmcFramework/DmcConfirmDownload;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcConfirmDownload;->DownloadDescriptor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
