.class public Lcom/redbend/dmcFramework/DmcMain;
.super Landroid/app/Activity;
.source "DmcMain.java"


# instance fields
.field protected LOG_TAG:Ljava/lang/String;

.field private dialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "DmcMain"

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcMain;->LOG_TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "DmcMain"

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcMain;->LOG_TAG:Ljava/lang/String;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcMain;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ""

    const v1, 0x7f0b004a

    invoke-virtual {p0, v1}, Lcom/redbend/dmcFramework/DmcMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcMain;->dialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcMain;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcMain;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/redbend/dmcFramework/DmcService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "serviceStartReason"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcMain;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
