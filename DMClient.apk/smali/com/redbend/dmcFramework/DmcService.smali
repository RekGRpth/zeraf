.class public Lcom/redbend/dmcFramework/DmcService;
.super Landroid/app/Service;
.source "DmcService.java"


# static fields
.field public static final ACTIVITY_LOADED:I = 0xd

.field public static final ACTIVITY_PAUSED:I = 0xc

.field public static final APPLICATION_FREEZE:I = 0x10

.field public static final BUTTON_CLICKED:I = 0x2

.field public static final CFG_CNT_DEFAULT:Ljava/lang/String; = "CntAuto"

.field public static final CFG_CNT_WIFI_ONLY:Ljava/lang/String; = "CntWifiOnly"

.field public static final CFG_CONNECT_KEY:Ljava/lang/String; = "CntSet"

.field public static final CFG_ROAMING_KEY:Ljava/lang/String; = "RoamingSet"

.field public static final CFG_ROAM_DEFAULT:Z = true

.field public static final EXIT_APP:I = 0x5

.field public static final LIST_ITEM_SELECTED:I = 0x8

.field private static final LOG_TAG:Ljava/lang/String; = "DmcService"

.field public static final SCREEN_CHOICELISTQUERY:I = 0x7

.field public static final SCREEN_CONFIRM_DOWNLOAD:I = 0x11

.field public static final SCREEN_CONFQUERY:I = 0x6

.field public static final SCREEN_INFOMSG:I = 0x3

.field public static final SCREEN_UNLOCKED:I = 0x12

.field public static final SERVICE_STARTED_BY_ALARM:I = 0xb

.field public static final SERVICE_STARTED_FROM_BOOT:I = 0xa

.field public static final SERVICE_STARTED_FROM_ICON:I = 0x1

.field public static final SERVICE_STARTED_FROM_NOTIFICATION_ICON:I = 0x9

.field public static final SERVICE_STARTED_FROM_WAPPUSH:I = 0x4

.field public static final STARTED_BY_ALARM:I = 0x5

.field public static final STARTED_FROM_BOOT:I = 0x4

.field public static final STARTED_FROM_ICON:I = 0x1

.field public static final STARTED_FROM_NOTIFICATION:I = 0x3

.field public static final STARTED_FROM_WAPPUSH:I = 0x2

.field public static final START_APPLICATION_SERVICE_ACTIONS:I = 0x1388

.field public static final WIFI_ACTION:Ljava/lang/String; = "android.net.wifi.WIFI_STATE_CHANGED"

.field public static final WIFI_OFF:I = 0xf

.field public static final WIFI_ON:I = 0xe

.field public static final alarmIdExtra:Ljava/lang/String; = "alarmIdExtra"

.field public static final clientServiceClassExtra:Ljava/lang/String; = "clientServiceClassExtra"

.field public static final clientServiceExtra:Ljava/lang/String; = "clientService"

.field public static final confirmDownloadBundleExtra:Ljava/lang/String; = "confirmDownloadBundleExtra"

.field public static final nextStateExtra:Ljava/lang/String; = "nextStateExtra"

.field public static final notificationIdExtra:Ljava/lang/String; = "notificationIdExtra"

.field public static final notificationObjExtra:Ljava/lang/String; = "notificationObjExtra"

.field public static final startServiceReasonExtra:Ljava/lang/String; = "serviceStartReason"

.field public static final uiAlertBundleExtra:Ljava/lang/String; = "uiAlertBundleExtra"

.field public static final uiAlertTextExtra:Ljava/lang/String; = "uiAlertTextExtra"

.field public static final wapPushMessageExtra:Ljava/lang/String; = "wapPushMessageExtra"


# instance fields
.field private a:Lcom/redbend/dmcFramework/DmcApplication;

.field private clientService:Lcom/redbend/dmcFramework/DmClientBaseService;

.field private cls:Landroid/os/Bundle;

.field private comm:Lcom/redbend/vdm/comm/VdmComm;

.field private final mDmcWifiStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mMainHandler:Landroid/os/Handler;

.field public u:Lcom/redbend/dmcFramework/DmcUtils;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->comm:Lcom/redbend/vdm/comm/VdmComm;

    new-instance v0, Lcom/redbend/dmcFramework/WifiStatusReceiver;

    invoke-direct {v0}, Lcom/redbend/dmcFramework/WifiStatusReceiver;-><init>()V

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->mDmcWifiStatusReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/redbend/dmcFramework/DmcService;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/redbend/dmcFramework/DmcService;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/redbend/dmcFramework/DmcService;->dispatch(Landroid/os/Message;)V

    return-void
.end method

.method private dispatch(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    iget v3, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x1388

    if-lt v3, v4, :cond_1

    iget-object v3, p0, Lcom/redbend/dmcFramework/DmcService;->clientService:Lcom/redbend/dmcFramework/DmClientBaseService;

    invoke-virtual {v3, p1}, Lcom/redbend/dmcFramework/DmClientBaseService;->dispatch(Landroid/os/Message;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->applicationIconClicked()V

    goto :goto_0

    :pswitch_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/redbend/dmcFramework/DmcService;->buttonClicked(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/redbend/dmcFramework/DmcService;->listItemSelected(I)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/redbend/dmcFramework/DmcChoiceListQuery;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "uiAlertBundleExtra"

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/Bundle;

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v4, "nextStateExtra"

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/Bundle;

    const-string v5, "nextState"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/redbend/dmcFramework/DmcConfirmationQuery;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "uiAlertTextExtra"

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/Bundle;

    const-string v5, "displayText"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "nextStateExtra"

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/Bundle;

    const-string v5, "nextState"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/redbend/dmcFramework/DmcInfoMessage;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "uiAlertTextExtra"

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/Bundle;

    const-string v5, "displayText"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "nextStateExtra"

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/Bundle;

    const-string v5, "nextState"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcService;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_6
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/redbend/dmcFramework/DmcConfirmDownload;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "confirmDownloadBundleExtra"

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/Bundle;

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v4, "nextStateExtra"

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/Bundle;

    const-string v5, "nextState"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcService;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, [B

    move-object v2, v3

    check-cast v2, [B

    array-length v3, v2

    invoke-virtual {p0, v2, v3}, Lcom/redbend/dmcFramework/DmcService;->wapPushReceived([BI)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/redbend/dmcFramework/DmcService;->notificationIconClicked(I)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/redbend/dmcFramework/DmcService;->AlarmExpired(I)V

    goto/16 :goto_0

    :pswitch_a
    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->stopSelf()V

    iget-object v3, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v3, v3, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v3, v3, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/redbend/dmcFramework/DmcActivity;->moveTaskToBack(Z)Z

    iget-object v3, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    goto/16 :goto_0

    :cond_2
    const-string v3, "DmcService"

    const-string v4, "exit_app() called with null currentActivity"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_b
    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->bootCompleted()V

    goto/16 :goto_0

    :pswitch_c
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/redbend/dmcFramework/DmcService;->activityPaused(I)V

    goto/16 :goto_0

    :pswitch_d
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/redbend/dmcFramework/DmcService;->activityLoaded(I)V

    goto/16 :goto_0

    :pswitch_e
    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->wifiOn()V

    goto/16 :goto_0

    :pswitch_f
    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->wifiOff()V

    goto/16 :goto_0

    :pswitch_10
    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->applicationFreezed()V

    goto/16 :goto_0

    :pswitch_11
    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->screenUnLocked()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_7
        :pswitch_a
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_6
        :pswitch_11
    .end packed-switch
.end method

.method public static getStorageDirectories()[Ljava/lang/String;
    .locals 10

    const/4 v2, 0x0

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    const-string v9, "/proc/mounts"

    invoke-direct {v8, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    const-string v8, "vfat"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "/mnt"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    :cond_1
    new-instance v7, Ljava/util/StringTokenizer;

    const-string v8, " "

    invoke-direct {v7, v4, v8}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v8

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    :cond_2
    :goto_2
    return-object v2

    :cond_3
    :try_start_3
    const-string v8, "/dev/block/vold"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "/mnt/secure"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "/mnt/asec"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "/mnt/obb"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "/dev/mapper"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "tmpfs"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_1
    move-exception v8

    move-object v0, v1

    :goto_3
    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception v8

    goto :goto_2

    :cond_4
    :try_start_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v2, v8, [Ljava/lang/String;

    const/4 v3, 0x0

    :goto_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v3, v8, :cond_5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    aput-object v8, v2, v3
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    if-eqz v1, :cond_7

    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    move-object v0, v1

    goto :goto_2

    :catch_3
    move-exception v8

    move-object v0, v1

    goto :goto_2

    :catchall_0
    move-exception v8

    :goto_5
    if-eqz v0, :cond_6

    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :cond_6
    :goto_6
    throw v8

    :catch_4
    move-exception v8

    goto :goto_2

    :catch_5
    move-exception v9

    goto :goto_6

    :catchall_1
    move-exception v8

    move-object v0, v1

    goto :goto_5

    :catch_6
    move-exception v8

    goto :goto_3

    :catch_7
    move-exception v8

    goto :goto_1

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method

.method private installFileIfNeeded(Ljava/lang/String;)V
    .locals 14
    .param p1    # Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v5, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/dmcmain.cfg"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-nez v6, :cond_3

    invoke-static {}, Lcom/redbend/dmcFramework/DmcService;->getStorageDirectories()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v7, 0x0

    :goto_1
    array-length v10, v2

    if-ge v7, v10, :cond_0

    aget-object v1, v2, v7

    :try_start_1
    new-instance v6, Ljava/io/FileInputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/dmcmain.cfg"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    if-eqz v6, :cond_2

    const-string v10, "DmcService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/dmcmain.cfg , found!"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_3
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "vDMC"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "tree.xml not found. Setting default "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v9

    const/4 v8, 0x0

    if-nez v6, :cond_4

    :try_start_2
    invoke-virtual {v9, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v8

    :goto_4
    const/4 v10, 0x0

    invoke-virtual {p0, p1, v10}, Lcom/redbend/dmcFramework/DmcService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/InputStream;->available()I

    move-result v10

    new-array v0, v10, [B

    invoke-virtual {v8, v0}, Ljava/io/InputStream;->read([B)I

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    invoke-virtual {v4, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_5
    new-instance v5, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x1

    invoke-virtual {v5, v10, v13}, Ljava/io/File;->setReadable(ZZ)Z

    :cond_1
    return-void

    :catch_0
    move-exception v3

    const/4 v6, 0x0

    goto/16 :goto_0

    :catch_1
    move-exception v3

    const/4 v6, 0x0

    goto/16 :goto_2

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    :cond_3
    const-string v10, "DmcService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/dmcmain.cfg, found!"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_4
    :try_start_3
    const-string v10, "tree_lab.xml"

    invoke-virtual {v9, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_4

    :catch_2
    move-exception v3

    const-string v10, "vDMC"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Could not set default "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method private registerIntentReceivers()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcService;->mDmcWifiStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/redbend/dmcFramework/DmcService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private unregisterIntentReceivers()V
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->mDmcWifiStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method


# virtual methods
.method public native AlarmExpired(I)V
.end method

.method public GetPkgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v0}, Lcom/redbend/dmcFramework/DmcUtils;->GetPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public native activityLoaded(I)V
.end method

.method public native activityPaused(I)V
.end method

.method public appExit()V
    .locals 3

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public native applicationFreezed()V
.end method

.method public native applicationIconClicked()V
.end method

.method public native bootCompleted()V
.end method

.method public native buttonClicked(Ljava/lang/String;)V
.end method

.method public deleteAlarm(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v0, p1}, Lcom/redbend/dmcFramework/DmcUtils;->deleteAlarm(I)V

    return-void
.end method

.method public getDevId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v0}, Lcom/redbend/dmcFramework/DmcUtils;->getIMEI()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeedByMacAddress()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v0}, Lcom/redbend/dmcFramework/DmcUtils;->getSeedByMacAddress()I

    move-result v0

    return v0
.end method

.method public getSystemLang()Ljava/lang/String;
    .locals 2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public isAlarmSetById(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v0, p1}, Lcom/redbend/dmcFramework/DmcUtils;->isAlarmSetById(I)Z

    move-result v0

    return v0
.end method

.method public isNetworkAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v0}, Lcom/redbend/dmcFramework/DmcUtils;->isNetworkAvailable()Z

    move-result v0

    return v0
.end method

.method public isRoaming()Z
    .locals 5

    const/4 v4, 0x0

    const v1, 0x7f0b000d

    invoke-virtual {p0, v1}, Lcom/redbend/dmcFramework/DmcService;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "micromax"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "DmcService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "domain="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isRoaming() always return false."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v4
.end method

.method public isWirelessNetworkConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v0}, Lcom/redbend/dmcFramework/DmcUtils;->isWirelessNetworkConnected()Z

    move-result v0

    return v0
.end method

.method public native listItemSelected(I)V
.end method

.method public native notificationIconClicked(I)V
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    const-string v1, "DmcService"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "dmc"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    new-instance v1, Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/redbend/dmcFramework/DmcUtils;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    :try_start_0
    new-instance v1, Lcom/redbend/vdm/comm/VdmComm;

    new-instance v2, Lcom/redbend/vdm/comm/VdmCommFactory;

    invoke-direct {v2}, Lcom/redbend/vdm/comm/VdmCommFactory;-><init>()V

    invoke-direct {v1, v2}, Lcom/redbend/vdm/comm/VdmComm;-><init>(Lcom/redbend/vdm/comm/CommFactory;)V

    iput-object v1, p0, Lcom/redbend/dmcFramework/DmcService;->comm:Lcom/redbend/vdm/comm/VdmComm;

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcService;->comm:Lcom/redbend/vdm/comm/VdmComm;

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Lcom/redbend/vdm/comm/VdmComm;->setConnectionTimeout(I)V
    :try_end_0
    .catch Lcom/redbend/vdm/comm/VdmCommException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v1, "tree.xml"

    invoke-direct {p0, v1}, Lcom/redbend/dmcFramework/DmcService;->installFileIfNeeded(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcService;->registerIntentReceivers()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "DmcService"

    const-string v2, "Could not initialize VdmComm"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/redbend/vdm/comm/VdmCommException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "DmcService"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcService;->unregisterIntentReceivers()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 13
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v10, "DmcService"

    const-string v11, "onStartCommand()"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {p0, v10, v11}, Lcom/redbend/dmcFramework/DmcService;->startForeground(ILandroid/app/Notification;)V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getApplication()Landroid/app/Application;

    move-result-object v10

    check-cast v10, Lcom/redbend/dmcFramework/DmcApplication;

    iput-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    new-instance v10, Lcom/redbend/dmcFramework/DmcService$1;

    invoke-direct {v10, p0}, Lcom/redbend/dmcFramework/DmcService$1;-><init>(Lcom/redbend/dmcFramework/DmcService;)V

    iput-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->mMainHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v11, p0, Lcom/redbend/dmcFramework/DmcService;->mMainHandler:Landroid/os/Handler;

    iput-object v11, v10, Lcom/redbend/dmcFramework/DmcApplication;->serviceHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/redbend/dmcFramework/DmcApplication;->setBootFlag(Z)V

    :try_start_0
    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    invoke-virtual {v10}, Lcom/redbend/dmcFramework/DmcApplication;->getClientServiceClassName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v10, 0x1

    new-array v8, v10, [Ljava/lang/Class;

    const/4 v10, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    aput-object v11, v8, v10

    invoke-virtual {v2, v8}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    const/4 v10, 0x1

    new-array v1, v10, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p0, v1, v10

    invoke-virtual {v3, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/redbend/dmcFramework/DmClientBaseService;

    iput-object v9, p0, Lcom/redbend/dmcFramework/DmcService;->clientService:Lcom/redbend/dmcFramework/DmClientBaseService;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->comm:Lcom/redbend/vdm/comm/VdmComm;

    invoke-virtual {p0, v10}, Lcom/redbend/dmcFramework/DmcService;->setService(Lcom/redbend/vdm/comm/VdmComm;)V

    const-string v10, "serviceStartReason"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    const-string v10, "DmcService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Received bad start reason "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v10, 0x2

    :goto_1
    return v10

    :catch_0
    move-exception v4

    const-string v10, "DmcService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Received bad client service class name:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    invoke-virtual {v12}, Lcom/redbend/dmcFramework/DmcApplication;->getClientServiceClassName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto :goto_1

    :pswitch_0
    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    const-string v10, "wapPushMessageExtra"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v7

    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/4 v11, 0x4

    invoke-virtual {v10, v11, v7}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    const-string v10, "notificationIdExtra"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v11, 0x9

    new-instance v12, Ljava/lang/Integer;

    invoke-direct {v12, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v10, v11, v12}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_3
    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v11, 0xa

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_4
    const-string v10, "alarmIdExtra"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v10, -0x1

    if-ne v0, v10, :cond_0

    const-string v10, "DmcService"

    const-string v11, "Wrong Alarm ID -1"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    iget-object v10, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v11, 0xb

    new-instance v12, Ljava/lang/Integer;

    invoke-direct {v12, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v10, v11, v12}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public screenChoiceListQuery(Ljava/lang/String;I[Ljava/lang/String;IZI)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # [Ljava/lang/String;
    .param p4    # I
    .param p5    # Z
    .param p6    # I

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "displayText"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "itemsCount"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "items"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "DefaultSelection"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "isMultiSelect"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "nextState"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenConfirmDownload(Ljava/lang/String;JI)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # I

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "fileSize"

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "description"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "nextState"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenConfirmationQuery(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "displayText"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "nextState"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public screenInfoMessage(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "displayText"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    const-string v1, "nextState"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcService;->cls:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    return-void
.end method

.method public native screenUnLocked()V
.end method

.method public setAlarmByInterval(IJZ)V
    .locals 1
    .param p1    # I
    .param p2    # J
    .param p4    # Z

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->u:Lcom/redbend/dmcFramework/DmcUtils;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/redbend/dmcFramework/DmcUtils;->setAlarmByInterval(IJZ)V

    return-void
.end method

.method public native setService(Lcom/redbend/vdm/comm/VdmComm;)V
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v0, v0, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x10000000

    or-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Service;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcService;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v0, v0, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/redbend/dmcFramework/DmcActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public storeDefaultDmTree()V
    .locals 11

    const/4 v10, 0x0

    const-string v4, "tree.xml"

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v6

    :try_start_0
    invoke-virtual {v6, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {p0, v4, v7}, Lcom/redbend/dmcFramework/DmcService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v7

    new-array v0, v7, [B

    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v3, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcService;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x1

    invoke-virtual {v3, v7, v10}, Ljava/io/File;->setReadable(ZZ)Z

    return-void

    :catch_0
    move-exception v1

    const-string v7, "vDMC"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not set default "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public native wapPushReceived([BI)V
.end method

.method public native wifiOff()V
.end method

.method public native wifiOn()V
.end method
