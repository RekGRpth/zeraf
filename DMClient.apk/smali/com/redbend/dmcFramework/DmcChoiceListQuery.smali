.class public Lcom/redbend/dmcFramework/DmcChoiceListQuery;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "DmcChoiceListQuery.java"


# instance fields
.field private LOG_TAG:Ljava/lang/String;

.field private a:Lcom/redbend/dmcFramework/DmcApplication;

.field private defaultSelection:I

.field private displayText:Ljava/lang/String;

.field private isMultiSelect:Z

.field private listItems:[Ljava/lang/String;

.field private selectedItemIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->selectedItemIndex:I

    return-void
.end method

.method static synthetic access$002(Lcom/redbend/dmcFramework/DmcChoiceListQuery;I)I
    .locals 0
    .param p0    # Lcom/redbend/dmcFramework/DmcChoiceListQuery;
    .param p1    # I

    iput p1, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->selectedItemIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/redbend/dmcFramework/DmcChoiceListQuery;)Lcom/redbend/dmcFramework/DmcApplication;
    .locals 1
    .param p0    # Lcom/redbend/dmcFramework/DmcChoiceListQuery;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->a:Lcom/redbend/dmcFramework/DmcApplication;

    return-object v0
.end method

.method private addNumbersToItems([Ljava/lang/String;)V
    .locals 3
    .param p1    # [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v2, v0, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "DmcChoiceListQuery"

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->LOG_TAG:Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/redbend/dmcFramework/DmcApplication;

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uiAlertBundleExtra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "displayText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->displayText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uiAlertBundleExtra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isMultiSelect"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->isMultiSelect:Z

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uiAlertBundleExtra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "defaultSelection"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->defaultSelection:I

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uiAlertBundleExtra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->listItems:[Ljava/lang/String;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->listItems:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->addNumbersToItems([Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const v0, 0x7f030019

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->setContentView(I)V

    return-void
.end method

.method public setContentView(I)V
    .locals 6
    .param p1    # I

    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->LOG_TAG:Ljava/lang/String;

    const-string v3, "setContentView()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->setContentView(I)V

    const v2, 0x7f060040

    invoke-virtual {p0, v2}, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-boolean v2, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->isMultiSelect:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    :goto_0
    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->displayText:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->displayText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->displayText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/ListView;->setSelectionAfterHeaderView()V

    iget v2, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->defaultSelection:I

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030018

    const v4, 0x7f06003c

    iget-object v5, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->listItems:[Ljava/lang/String;

    invoke-direct {v2, p0, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lcom/redbend/dmcFramework/DmcChoiceListQuery$1;

    invoke-direct {v2, p0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery$1;-><init>(Lcom/redbend/dmcFramework/DmcChoiceListQuery;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget v2, p0, Lcom/redbend/dmcFramework/DmcChoiceListQuery;->selectedItemIndex:I

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    new-instance v2, Lcom/redbend/dmcFramework/DmcChoiceListQuery$2;

    invoke-direct {v2, p0}, Lcom/redbend/dmcFramework/DmcChoiceListQuery$2;-><init>(Lcom/redbend/dmcFramework/DmcChoiceListQuery;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto :goto_0
.end method
