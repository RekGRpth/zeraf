.class public abstract Lcom/a/e/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/a/e/d$a;,
        Lcom/a/e/d$b;,
        Lcom/a/e/d$c;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/a/e/d;
    .locals 2

    new-instance v0, Lcom/a/e/d$c;

    invoke-direct {v0, p0, p1}, Lcom/a/e/d$c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Lcom/a/e/d$1;

    invoke-direct {v1, v0}, Lcom/a/e/d$1;-><init>(Lcom/a/e/d$c;)V

    return-object v1
.end method

.method static b(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 1

    new-instance v0, Lorg/json/JSONTokener;

    invoke-direct {v0, p0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method abstract a()Landroid/database/sqlite/SQLiteDatabase;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/a/e/d$a;
.end method
