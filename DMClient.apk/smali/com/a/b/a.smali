.class public final Lcom/a/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Lcom/a/b/b;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/a/b/c",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a(Lcom/a/b/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/b/d",
            "<TA;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcom/a/b/d;

    if-eqz v0, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    check-cast v0, Lcom/a/b/d;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v1, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "TA;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcom/a/b/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    check-cast v0, Lcom/a/b/d;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d;->a(Ljava/lang/Object;Lcom/a/b/b;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/a/b/a;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d;->a(Ljava/lang/Object;Lcom/a/b/b;)V

    goto :goto_1
.end method
