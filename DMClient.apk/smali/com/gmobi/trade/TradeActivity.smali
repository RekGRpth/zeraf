.class public Lcom/gmobi/trade/TradeActivity;
.super La/a/b/h;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gmobi/trade/TradeActivity$CustomCordovaWebViewClient;
    }
.end annotation


# static fields
.field public static final EXTRA_ERROR_URL:Ljava/lang/String; = "Url"

.field public static final EXTRA_LOADING_MESSAGE:Ljava/lang/String; = "LoadingMessage"

.field public static final EXTRA_LOADING_TITLE:Ljava/lang/String; = "LoadingTitle"

.field public static final EXTRA_SPLASH:Ljava/lang/String; = "Splash"

.field public static final EXTRA_URL:Ljava/lang/String; = "Url"

.field public static final RESULT_ERROR:Ljava/lang/String; = "error"

.field public static final RESULT_PREFIX:Ljava/lang/String; = "result://"

.field public static final RESULT_SUCCESS:Ljava/lang/String; = "success"


# instance fields
.field a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, La/a/b/h;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/gmobi/trade/TradeActivity;->a:Z

    return-void
.end method


# virtual methods
.method public addService(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/gmobi/trade/TradeActivity;->addService(Ljava/lang/String;Ljava/lang/Class;Z)V

    return-void
.end method

.method public addService(Ljava/lang/String;Ljava/lang/Class;Z)V
    .locals 7

    iget-object v0, p0, Lcom/gmobi/trade/TradeActivity;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/TradeActivity;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->a:La/a/b/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/TradeActivity;->appView:La/a/b/e;

    iget-object v6, v0, La/a/b/e;->a:La/a/b/a/e;

    new-instance v0, Lcom/gmobi/trade/TradeActivity$1;

    const-string v3, ""

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/gmobi/trade/TradeActivity$1;-><init>(Lcom/gmobi/trade/TradeActivity;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Class;)V

    invoke-virtual {v6, v0}, La/a/b/a/e;->a(La/a/b/a/d;)V

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityResult("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/gmobi/trade/a/e;->j()Lcom/gmobi/a/d;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/gmobi/a/d;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, La/a/b/h;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0, p1}, La/a/b/h;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "http://127.0.0.1*"

    invoke-static {v0, v1}, La/a/b/b;->a(Ljava/lang/String;Z)V

    const-string v0, ".*"

    invoke-static {v0, v1}, La/a/b/b;->a(Ljava/lang/String;Z)V

    const-string v0, "file://*"

    invoke-static {v0, v1}, La/a/b/b;->a(Ljava/lang/String;Z)V

    const-string v0, "loadUrlTimeoutValue"

    const v1, 0x493e0

    invoke-virtual {p0, v0, v1}, Lcom/gmobi/trade/TradeActivity;->setIntegerProperty(Ljava/lang/String;I)V

    const-string v0, "loadingDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/gmobi/trade/TradeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "LoadingTitle"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/gmobi/trade/TradeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "LoadingMessage"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/gmobi/trade/TradeActivity;->setStringProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "errorUrl"

    invoke-virtual {p0}, Lcom/gmobi/trade/TradeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "Url"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/gmobi/trade/TradeActivity;->setStringProperty(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, La/a/b/e;

    invoke-direct {v0, p0}, La/a/b/e;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/gmobi/trade/TradeActivity$2;

    invoke-direct {v1, p0, p0}, Lcom/gmobi/trade/TradeActivity$2;-><init>(Lcom/gmobi/trade/TradeActivity;La/a/b/h;)V

    new-instance v2, La/a/b/d;

    invoke-direct {v2, p0, v0}, La/a/b/d;-><init>(La/a/b/a/a;La/a/b/e;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/gmobi/trade/TradeActivity;->init(La/a/b/e;La/a/b/f;La/a/b/d;)V

    sget-boolean v0, Lcom/gmobi/trade/TradeService;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/gmobi/trade/TradeActivity;->clearCache()V

    :cond_0
    const-string v0, "RewardService"

    const-class v1, Lcom/gmobi/trade/a/d;

    invoke-virtual {p0, v0, v1}, Lcom/gmobi/trade/TradeActivity;->addService(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "App"

    const-class v1, La/a/b/a;

    invoke-virtual {p0, v0, v1}, Lcom/gmobi/trade/TradeActivity;->addService(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "Device"

    const-class v1, La/a/b/g;

    invoke-virtual {p0, v0, v1}, Lcom/gmobi/trade/TradeActivity;->addService(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "NetworkStatus"

    const-class v1, La/a/b/m;

    invoke-virtual {p0, v0, v1}, Lcom/gmobi/trade/TradeActivity;->addService(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "Notification"

    const-class v1, La/a/b/n;

    invoke-virtual {p0, v0, v1}, Lcom/gmobi/trade/TradeActivity;->addService(Ljava/lang/String;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/gmobi/trade/TradeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, La/a/b/h;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 4

    const-string v0, "TradeActivity onDestroy"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/gmobi/trade/a/e;->a(Landroid/app/Activity;)V

    iget-boolean v0, p0, Lcom/gmobi/trade/TradeActivity;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/gmobi/trade/TradeActivity$4;

    invoke-direct {v1, p0}, Lcom/gmobi/trade/TradeActivity$4;-><init>(Lcom/gmobi/trade/TradeActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    invoke-super {p0}, La/a/b/h;->onDestroy()V

    return-void
.end method

.method public onReceivedError(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] @ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/gmobi/trade/TradeActivity;->spinnerStop()V

    const/4 v0, -0x6

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/gmobi/trade/TradeActivity;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/gmobi/trade/TradeActivity$3;

    invoke-direct {v1, p0}, Lcom/gmobi/trade/TradeActivity$3;-><init>(Lcom/gmobi/trade/TradeActivity;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, La/a/b/h;->onStart()V

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/gmobi/trade/a/e;->a(Landroid/app/Activity;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    const-string v0, "TradeActivity onStop"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-super {p0}, La/a/b/h;->onStop()V

    return-void
.end method
