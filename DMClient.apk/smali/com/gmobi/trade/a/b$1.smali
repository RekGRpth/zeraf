.class final Lcom/gmobi/trade/a/b$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gmobi/trade/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/b;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    iget-object v3, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/gmobi/trade/a/b;->g:Z

    iget-object v0, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-object v3, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-object v3, v3, Lcom/gmobi/trade/a/b;->f:Landroid/net/ConnectivityManager;

    invoke-virtual {v3, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    iput-boolean v1, v0, Lcom/gmobi/trade/a/b;->p:Z

    iget-object v0, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-object v1, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-object v1, v1, Lcom/gmobi/trade/a/b;->f:Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    iput-boolean v1, v0, Lcom/gmobi/trade/a/b;->q:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Connectivity changed: connected="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-boolean v1, v1, Lcom/gmobi/trade/a/b;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " wifi :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-boolean v1, v1, Lcom/gmobi/trade/a/b;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " gprs :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-boolean v1, v1, Lcom/gmobi/trade/a/b;->q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-boolean v0, v0, Lcom/gmobi/trade/a/b;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    iget-boolean v0, v0, Lcom/gmobi/trade/a/b;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    invoke-static {v0}, Lcom/gmobi/trade/a/b;->a(Lcom/gmobi/trade/a/b;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/gmobi/trade/a/b$1;->a:Lcom/gmobi/trade/a/b;

    invoke-static {v0}, Lcom/gmobi/trade/a/b;->b(Lcom/gmobi/trade/a/b;)V

    goto :goto_1
.end method
