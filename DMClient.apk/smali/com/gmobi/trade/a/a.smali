.class public final Lcom/gmobi/trade/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gmobi/trade/a/a$a;
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/gmobi/trade/a/a$a;

.field c:Ljava/util/Timer;

.field d:Lcom/a/a/b;

.field e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/a/a/b;Lcom/gmobi/trade/a/a$a;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/a;->b:Lcom/gmobi/trade/a/a$a;

    iput-object v0, p0, Lcom/gmobi/trade/a/a;->c:Ljava/util/Timer;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/a;->e:Ljava/util/Set;

    new-instance v0, Lcom/gmobi/trade/a/a$1;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/a$1;-><init>(Lcom/gmobi/trade/a/a;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/a;->f:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/gmobi/trade/a/a;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/gmobi/trade/a/a;->b:Lcom/gmobi/trade/a/a$a;

    iput-object p2, p0, Lcom/gmobi/trade/a/a;->d:Lcom/a/a/b;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/a;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    iget-object v0, p0, Lcom/gmobi/trade/a/a;->c:Ljava/util/Timer;

    if-nez v0, :cond_0

    const-string v0, "AppUsageMonitor starts"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/a;->c:Ljava/util/Timer;

    iget-object v0, p0, Lcom/gmobi/trade/a/a;->c:Ljava/util/Timer;

    new-instance v1, Lcom/gmobi/trade/a/a$2;

    invoke-direct {v1, p0}, Lcom/gmobi/trade/a/a$2;-><init>(Lcom/gmobi/trade/a/a;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    :cond_0
    return-void
.end method
