.class final Lcom/gmobi/trade/a/b$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gmobi/trade/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field private a:La/b/a$a;

.field private b:La/b/a$b;

.field private c:Z

.field private d:Z

.field private synthetic e:Lcom/gmobi/trade/a/b;


# direct methods
.method public constructor <init>(Lcom/gmobi/trade/a/b;)V
    .locals 2

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean v0, p0, Lcom/gmobi/trade/a/b$a;->c:Z

    iput-boolean v0, p0, Lcom/gmobi/trade/a/b$a;->d:Z

    const/4 v0, 0x1

    invoke-static {v0}, La/b/a;->a(I)La/b/a$a;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/b$a;->a:La/b/a$a;

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->a:La/b/a$a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, La/b/a$a;->a(I)La/b/a$b;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    return-void
.end method

.method private b()Z
    .locals 9

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/e/g;->m(Landroid/content/Context;)[Landroid/content/pm/PackageInfo;

    move-result-object v4

    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    move v0, v2

    :goto_0
    array-length v7, v4

    if-lt v0, v7, :cond_0

    invoke-virtual {v5}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/e/g;->n(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Push thread "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is connecting!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v6}, Lcom/gmobi/trade/a/b;->e()Lorg/json/JSONObject;

    move-result-object v6

    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "emails"

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "Device Info : "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v7, "last"

    iget-object v8, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v8}, Lcom/gmobi/trade/a/b;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "sid"

    iget-object v8, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v8}, Lcom/gmobi/trade/a/b;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "device"

    invoke-virtual {v0, v7, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v6, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v6, v6, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v8, v8, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v8, v8, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "api/push/connect"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v7, v0}, Lcom/a/d/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v0

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/a/c/a;->a(Z)V

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/a/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/a/c/a;->g()I

    move-result v7

    const/16 v8, 0xc8

    if-eq v7, v8, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Status code : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/a/c/a;->g()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->d(Ljava/lang/String;)V

    :goto_2
    move v0, v2

    :goto_3
    return v0

    :cond_0
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    aget-object v8, v4, v0

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    aget-object v8, v4, v0

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    aget-object v8, v4, v0

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v8, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    :try_start_1
    invoke-static {v6}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/a/a/a/a$a;->b(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    if-eqz v7, :cond_3

    const-string v0, "server"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, "did"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, "sid"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad connection info : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->d(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v6, "last"

    invoke-virtual {v7, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v6, "server"

    invoke-virtual {v7, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/gmobi/trade/a/b;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->k:Ljava/lang/String;

    const-string v6, "://"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v6, -0x1

    if-ne v0, v6, :cond_5

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "tcp://"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v8, v8, Lcom/gmobi/trade/a/b;->k:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/gmobi/trade/a/b;->k:Ljava/lang/String;

    :cond_5
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v6, "fileUrl"

    invoke-virtual {v7, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/gmobi/trade/a/b;->l:Ljava/lang/String;

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v6, "chs"

    invoke-virtual {v7, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    iput-object v6, v0, Lcom/gmobi/trade/a/b;->m:Lorg/json/JSONArray;

    const-string v0, "ac"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    move-object v0, v3

    :goto_4
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v0, v4, v5}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;Lorg/json/JSONArray;)V

    :cond_6
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v3, "did"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v3, "sid"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/gmobi/trade/a/b;->c(Ljava/lang/String;)V

    const-string v0, "brand"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v3, "brand"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/gmobi/trade/a/b;->d(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v3, "enabled"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v0, Lcom/gmobi/trade/a/b;->h:Z

    const-string v0, "update"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "update"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    :cond_8
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v3, "messages"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-boolean v0, v0, Lcom/gmobi/trade/a/b;->h:Z

    if-nez v0, :cond_9

    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v3, v3, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-class v4, Lcom/gmobi/trade/ActionMonitor;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v3, v3, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const-string v0, "Cancel Alarm!"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :cond_9
    move v0, v1

    goto/16 :goto_3

    :cond_a
    const-string v0, "ac"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_4
.end method

.method private c()Z
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v1}, Lcom/gmobi/trade/a/b;->e()Lorg/json/JSONObject;

    move-result-object v1

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "last"

    iget-object v4, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v4}, Lcom/gmobi/trade/a/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "sid"

    iget-object v4, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v4}, Lcom/gmobi/trade/a/b;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "device"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v1, v1, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v4, v4, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v4, v4, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "api/push/receive/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Lcom/a/d/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(Z)V

    invoke-virtual {v1}, Lcom/a/c/a;->g()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/a/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/a/a/a/a$a;->b(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iget-object v2, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v3, "last"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;)V

    const-string v2, "messages"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    iget-object v2, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v2, v1}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    invoke-virtual {v0}, La/b/a$b;->a()V

    :cond_0
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->a:La/b/a$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->a:La/b/a$a;

    invoke-virtual {v0}, La/b/a$a;->a()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gmobi/trade/a/b$a;->a:La/b/a$a;

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v2, v2, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v2, v2, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "api/push/disconnect/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-virtual {v2}, Lcom/gmobi/trade/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/d/d;->a(Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/c/a;->f()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Close Push Socket "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " !"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/gmobi/trade/a/b$a;->d:Z

    return-void
.end method

.method public final destroy()V
    .locals 2

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Push thread "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is terminating!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/gmobi/trade/a/b$a;->c:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final run()V
    .locals 11

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x3

    move v7, v0

    :cond_0
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/gmobi/trade/a/b$a;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-boolean v0, v0, Lcom/gmobi/trade/a/b;->h:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/gmobi/trade/a/b$a;->d()V

    :goto_2
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-boolean v0, v0, Lcom/gmobi/trade/a/b;->g:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-boolean v0, v0, Lcom/gmobi/trade/a/b;->h:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/gmobi/trade/a/b;->t:J

    invoke-direct {p0}, Lcom/gmobi/trade/a/b$a;->b()Z

    move-result v0

    if-nez v0, :cond_4

    const-wide/16 v0, 0x1388

    invoke-static {v0, v1}, Lcom/gmobi/trade/a/b$a;->sleep(J)V

    add-int/lit8 v0, v7, -0x1

    if-lez v0, :cond_3

    const-string v1, "Retry to connect ..."

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    move v7, v0

    goto :goto_0

    :cond_3
    const-string v0, "Unable to connect!"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-direct {p0}, Lcom/gmobi/trade/a/b$a;->d()V

    goto :goto_2

    :cond_4
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    iget-object v1, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v1, v1, Lcom/gmobi/trade/a/b;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, La/b/a$b;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Push service is connecting to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v1, v1, Lcom/gmobi/trade/a/b;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    move v0, v5

    :goto_3
    iget-object v1, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v1, v1, Lcom/gmobi/trade/a/b;->m:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lt v0, v1, :cond_7

    move v4, v5

    :cond_5
    :goto_4
    iget-boolean v0, p0, Lcom/gmobi/trade/a/b$a;->c:Z

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v2

    iget-object v8, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iput-wide v9, v8, Lcom/gmobi/trade/a/b;->t:J

    iget-object v8, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, La/b/a$b;->a(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_8

    const-wide/16 v8, 0x3e8

    invoke-static {v8, v9}, Lcom/gmobi/trade/a/b$a;->sleep(J)V

    if-nez v4, :cond_6

    const-wide/16 v8, 0x2710

    cmp-long v0, v0, v8

    if-lez v0, :cond_6

    const-string v0, "Fails to create long push connection"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/gmobi/trade/a/b$a;->d()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    :cond_6
    iget-boolean v0, p0, Lcom/gmobi/trade/a/b$a;->d:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/gmobi/trade/a/b$a;->d:Z

    invoke-direct {p0}, Lcom/gmobi/trade/a/b$a;->c()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/gmobi/trade/a/b$a;->d()V

    throw v0

    :cond_7
    :try_start_4
    iget-object v1, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-object v1, v1, Lcom/gmobi/trade/a/b;->m:Lorg/json/JSONArray;

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Push service subscribs to "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v4, v1}, La/b/a$b;->a([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Push topic : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    invoke-virtual {v0}, La/b/a$b;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/gmobi/trade/a/b$a;->b:La/b/a$b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, La/b/a$b;->a(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Push content : "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/a/a/a/a$a;->b(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_10

    const-string v1, "action"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "action"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "push"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    const-string v4, "messages"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;)V

    move v4, v6

    goto/16 :goto_4

    :cond_9
    invoke-direct {p0}, Lcom/gmobi/trade/a/b$a;->c()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v0, 0x0

    :cond_a
    iget-object v8, p0, Lcom/gmobi/trade/a/b$a;->e:Lcom/gmobi/trade/a/b;

    iget-boolean v8, v8, Lcom/gmobi/trade/a/b;->j:Z

    if-eqz v8, :cond_b

    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Lcom/gmobi/trade/a/b$a;->sleep(J)V

    invoke-static {}, Lcom/a/e/e;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Enter quick polling mode !"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/a/e/e;->a(Z)V

    goto/16 :goto_4

    :cond_b
    const-wide/16 v8, 0x3e8

    div-long/2addr v0, v8

    const-wide/16 v8, 0x3c

    cmp-long v8, v0, v8

    if-gez v8, :cond_c

    const/4 v0, 0x5

    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "Polling after "

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "s"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/gmobi/trade/a/b$a;->sleep(J)V

    goto/16 :goto_4

    :cond_c
    const-wide/16 v8, 0xb4

    cmp-long v8, v0, v8

    if-gez v8, :cond_d

    const/16 v0, 0xa

    goto :goto_5

    :cond_d
    const-wide/16 v8, 0x258

    cmp-long v0, v0, v8

    if-gez v0, :cond_e

    const/16 v0, 0x1e

    goto :goto_5

    :cond_e
    const/16 v0, 0xb4

    goto :goto_5

    :cond_f
    const-wide/16 v0, 0x1388

    invoke-static {v0, v1}, Lcom/gmobi/trade/a/b$a;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_10
    move v4, v6

    goto/16 :goto_4
.end method
