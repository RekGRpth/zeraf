.class public final Lcom/gmobi/trade/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/gmobi/trade/Actions;


# instance fields
.field private A:Lcom/gmobi/trade/ICallback;

.field private B:Landroid/os/Bundle;

.field private C:Lcom/a/d/c;

.field private D:Lcom/a/d/c;

.field private E:Lcom/a/d/d;

.field private F:Lcom/gmobi/trade/a/a;

.field private G:Lcom/gmobi/trade/a/f;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Lcom/gmobi/a/d;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Lcom/a/e/d;

.field private P:Landroid/app/DownloadManager;

.field private Q:Landroid/net/wifi/WifiManager;

.field private R:Lcom/a/a/b;

.field a:Landroid/content/Context;

.field b:Landroid/app/Activity;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Lcom/gmobi/trade/a/c;

.field k:Lcom/gmobi/trade/a/b;

.field l:Ljava/lang/String;

.field m:Ljava/lang/String;

.field n:Ljava/lang/String;

.field o:Ljava/lang/String;

.field p:Landroid/telephony/TelephonyManager;

.field q:Ljava/lang/Thread$UncaughtExceptionHandler;

.field r:Z

.field s:Ljava/lang/String;

.field t:Z

.field u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:I

.field private y:Landroid/os/Handler;

.field private z:Lcom/gmobi/trade/ICallback;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->b:Landroid/app/Activity;

    const-string v0, "http://as{mcc}.generalmobi.com/gmas/0.4/"

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->c:Ljava/lang/String;

    const-string v0, "http://charge{mcc}.go2rewards.com/charge/"

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->d:Ljava/lang/String;

    const-string v0, "http://billing{mcc}.generalmobi.com/billing/1.0/"

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->e:Ljava/lang/String;

    const-string v0, "http://api.generalmobi.com/"

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->h:Ljava/lang/String;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->i:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/gmobi/trade/a/e;->x:I

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->y:Landroid/os/Handler;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->z:Lcom/gmobi/trade/ICallback;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->A:Lcom/gmobi/trade/ICallback;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->B:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->F:Lcom/gmobi/trade/a/a;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->k:Lcom/gmobi/trade/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->l:Ljava/lang/String;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->G:Lcom/gmobi/trade/a/f;

    const-string v0, "Loading"

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->H:Ljava/lang/String;

    const-string v0, "Please wait ..."

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->I:Ljava/lang/String;

    const-string v0, "Go2 rewards"

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->J:Ljava/lang/String;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->K:Lcom/gmobi/a/d;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->M:Ljava/lang/String;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->N:Ljava/lang/String;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->q:Ljava/lang/Thread$UncaughtExceptionHandler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/gmobi/trade/a/e;->r:Z

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->s:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/gmobi/trade/a/e;->t:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->u:Ljava/util/Map;

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->R:Lcom/a/a/b;

    return-void
.end method

.method static synthetic a(Lcom/gmobi/trade/a/e;)Lcom/gmobi/a/d;
    .locals 1

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->K:Lcom/gmobi/a/d;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 6

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/gmobi/trade/a/e;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "user_id"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string v0, "UTF-8"

    invoke-static {v1, v0}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)I
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v0

    :cond_0
    const v1, -0x55555556

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    move v2, v0

    :goto_1
    if-lt v2, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    and-int/lit8 v0, v2, 0x1

    if-nez v0, :cond_2

    shl-int/lit8 v0, v1, 0x7

    aget-char v5, v4, v2

    shr-int/lit8 v6, v1, 0x3

    mul-int/2addr v5, v6

    xor-int/2addr v0, v5

    :goto_2
    xor-int/2addr v1, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    shl-int/lit8 v0, v1, 0xb

    aget-char v5, v4, v2

    shr-int/lit8 v6, v1, 0x5

    xor-int/2addr v5, v6

    add-int/2addr v0, v5

    xor-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public static l()Lcom/gmobi/trade/a/e;
    .locals 1

    invoke-static {}, Lcom/gmobi/trade/TradeService;->getInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/gmobi/trade/a/e;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->M:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    const-string v1, "UNIQUE_DEVICE_ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->M:Ljava/lang/String;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->M:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->M:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    const-string v1, "UNIQUE_DEVICE_ID"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->M:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->M:Ljava/lang/String;

    goto :goto_0
.end method

.method private n()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    const-string v1, "UNIQUE_USER_ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static o()Ljava/lang/String;
    .locals 2

    const-string v0, "android"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";VERSION/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";MANUFACTURER/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";MODEL/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";BOARD/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";BRAND/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";DEVICE/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";HARDWARE/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";PRODUCT/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 3

    iget-boolean v0, p0, Lcom/gmobi/trade/a/e;->t:Z

    if-eqz v0, :cond_0

    const-string v0, "already connecting"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->s:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "already get session"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/gmobi/trade/a/e;->t:Z

    new-instance v0, Lcom/gmobi/trade/a/e$15;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/e$15;-><init>(Lcom/gmobi/trade/a/e;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    new-instance v2, Lcom/gmobi/trade/a/e$16;

    invoke-direct {v2, p0, v0}, Lcom/gmobi/trade/a/e$16;-><init>(Lcom/gmobi/trade/a/e;Lcom/gmobi/trade/a/c$a;)V

    new-instance v0, Lcom/gmobi/trade/a/c$6;

    invoke-direct {v0, v1, v2}, Lcom/gmobi/trade/a/c$6;-><init>(Lcom/gmobi/trade/a/c;Lcom/gmobi/trade/a/c$a;)V

    invoke-virtual {v0}, Lcom/gmobi/trade/a/c$6;->start()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->g()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/gmobi/trade/a/c;->a(Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V

    goto :goto_0
.end method

.method private static q()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Z)I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    if-nez v1, :cond_1

    const-string v1, "Remote service has not been created!"

    invoke-static {v1}, Lcom/a/e/e;->d(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/gmobi/trade/a/e;->p()V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    new-instance v2, Lcom/gmobi/trade/a/e$17;

    invoke-direct {v2, p0}, Lcom/gmobi/trade/a/e$17;-><init>(Lcom/gmobi/trade/a/e;)V

    new-instance v3, Lcom/gmobi/trade/a/c$8;

    invoke-direct {v3, v1, v2}, Lcom/gmobi/trade/a/c$8;-><init>(Lcom/gmobi/trade/a/c;Lcom/gmobi/trade/a/c$a;)V

    invoke-virtual {v1, v3}, Lcom/gmobi/trade/a/c;->a(Ljava/lang/Runnable;)V

    :cond_2
    iget v1, p0, Lcom/gmobi/trade/a/e;->x:I

    if-ltz v1, :cond_0

    iget v0, p0, Lcom/gmobi/trade/a/e;->x:I

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->N:Ljava/lang/String;

    return-object v0
.end method

.method final a(I)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/gmobi/trade/a/e;->x:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/gmobi/trade/a/e;->x:I

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->z:Lcom/gmobi/trade/ICallback;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 4

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->P:Landroid/app/DownloadManager;

    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [J

    const/4 v3, 0x0

    aput-wide p1, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v1, "local_uri"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Install "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v3, "application/vnd.android.package-archive"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/e;->b:Landroid/app/Activity;

    return-void
.end method

.method public final a(Lcom/gmobi/trade/ICallback;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/e;->z:Lcom/gmobi/trade/ICallback;

    return-void
.end method

.method final a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V
    .locals 7

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/gmobi/trade/a/e;->y:Landroid/os/Handler;

    new-instance v0, Lcom/gmobi/trade/a/e$18;

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/gmobi/trade/a/e$18;-><init>(Lcom/gmobi/trade/a/e;ZLcom/gmobi/trade/ICallback;Landroid/os/Bundle;Ljava/lang/Throwable;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Lcom/gmobi/trade/IPushCallback;)V
    .locals 2

    const-string v0, "*"

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->k:Lcom/gmobi/trade/a/b;

    invoke-virtual {v1, v0, p1}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;Lcom/gmobi/trade/IPushCallback;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    const-string v1, "UNIQUE_USER_ID"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Save User Id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/gmobi/trade/ICallback;Landroid/os/Bundle;)V
    .locals 11

    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "earn/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v9, v9, v1}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, p2, v1, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    const-string v0, "sso/connect"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/gmobi/trade/a/e;->o()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/a/e/g;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/gmobi/trade/a/e;->q()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/gmobi/trade/a/e;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, La/a/a/a/b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v5, "appId"

    iget-object v6, p0, Lcom/gmobi/trade/a/e;->v:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "imsi"

    iget-object v6, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "imei"

    iget-object v6, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "ua"

    invoke-virtual {v4, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "screen"

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "auth"

    invoke-virtual {v4, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "timeStamp"

    invoke-virtual {v4, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sc"

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/gmobi/trade/a/e;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "lang"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SsoConnect Params : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    new-instance v1, Lcom/gmobi/trade/a/e$5;

    invoke-direct {v1, p0, p2}, Lcom/gmobi/trade/a/e$5;-><init>(Lcom/gmobi/trade/a/e;Lcom/gmobi/trade/ICallback;)V

    new-instance v2, Lcom/gmobi/trade/a/c$2;

    invoke-direct {v2, v0, v4, v1}, Lcom/gmobi/trade/a/c$2;-><init>(Lcom/gmobi/trade/a/c;Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V

    invoke-virtual {v2}, Lcom/gmobi/trade/a/c$2;->start()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_3
    const-string v0, "sso/register"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    if-nez p3, :cond_4

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :cond_4
    const-string v0, "username"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "password"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_5

    if-nez v1, :cond_6

    :cond_5
    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Lcom/gmobi/trade/a/e;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/a/e/g;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/gmobi/trade/a/e;->q()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/gmobi/trade/a/e;->w:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, La/a/a/a/b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    :try_start_1
    const-string v7, "appId"

    iget-object v8, p0, Lcom/gmobi/trade/a/e;->v:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "imsi"

    iget-object v8, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "imei"

    iget-object v8, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "ua"

    invoke-virtual {v6, v7, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "screen"

    invoke-virtual {v6, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "username"

    invoke-virtual {v6, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "password"

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "auth"

    invoke-virtual {v6, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "timeStamp"

    invoke-virtual {v6, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sc"

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/gmobi/trade/a/e;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "lang"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SsoRegister Params : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    new-instance v1, Lcom/gmobi/trade/a/e$6;

    invoke-direct {v1, p0, p2}, Lcom/gmobi/trade/a/e$6;-><init>(Lcom/gmobi/trade/a/e;Lcom/gmobi/trade/ICallback;)V

    new-instance v2, Lcom/gmobi/trade/a/c$3;

    invoke-direct {v2, v0, v6, v1}, Lcom/gmobi/trade/a/c$3;-><init>(Lcom/gmobi/trade/a/c;Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V

    invoke-virtual {v2}, Lcom/gmobi/trade/a/c$3;->start()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "sso/login"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    if-nez p3, :cond_8

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :cond_8
    const-string v0, "username"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "password"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_9

    if-nez v1, :cond_a

    :cond_9
    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_a
    invoke-static {}, Lcom/gmobi/trade/a/e;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/a/e/g;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/gmobi/trade/a/e;->q()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/gmobi/trade/a/e;->w:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, La/a/a/a/b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    :try_start_2
    const-string v7, "appId"

    iget-object v8, p0, Lcom/gmobi/trade/a/e;->v:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "imsi"

    iget-object v8, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "imei"

    iget-object v8, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "ua"

    invoke-virtual {v6, v7, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "screen"

    invoke-virtual {v6, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "username"

    invoke-virtual {v6, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "password"

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "auth"

    invoke-virtual {v6, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "timeStamp"

    invoke-virtual {v6, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sc"

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/gmobi/trade/a/e;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "lang"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SsoLogin Params : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    new-instance v1, Lcom/gmobi/trade/a/e$7;

    invoke-direct {v1, p0, p2}, Lcom/gmobi/trade/a/e$7;-><init>(Lcom/gmobi/trade/a/e;Lcom/gmobi/trade/ICallback;)V

    new-instance v2, Lcom/gmobi/trade/a/c$4;

    invoke-direct {v2, v0, v6, v1}, Lcom/gmobi/trade/a/c$4;-><init>(Lcom/gmobi/trade/a/c;Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V

    invoke-virtual {v2}, Lcom/gmobi/trade/a/c$4;->start()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_b
    const-string v0, "sso/check"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    if-nez p3, :cond_c

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :cond_c
    const-string v0, "username"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_d

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_d
    invoke-static {}, Lcom/gmobi/trade/a/e;->q()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->w:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, La/a/a/a/b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    :try_start_3
    const-string v4, "appId"

    iget-object v5, p0, Lcom/gmobi/trade/a/e;->v:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v4, "username"

    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "auth"

    invoke-virtual {v3, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "timeStamp"

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sc"

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/gmobi/trade/a/e;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SsoCheck Params : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    new-instance v1, Lcom/gmobi/trade/a/e$8;

    invoke-direct {v1, p0, p2}, Lcom/gmobi/trade/a/e$8;-><init>(Lcom/gmobi/trade/a/e;Lcom/gmobi/trade/ICallback;)V

    new-instance v2, Lcom/gmobi/trade/a/c$5;

    invoke-direct {v2, v0, v3, v1}, Lcom/gmobi/trade/a/c$5;-><init>(Lcom/gmobi/trade/a/c;Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V

    invoke-virtual {v2}, Lcom/gmobi/trade/a/c$5;->start()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_e
    const-string v0, "data/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x5

    :try_start_4
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "type"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "custom_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "data"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    const-string v2, "data"

    invoke-static {v0}, Landroid/a/a/a/a$a;->b(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_f
    const-string v0, "at"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->c()Lcom/a/e/d$a;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/a/e/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Record Custom Data "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Lcom/gmobi/trade/ICallback;->onResult(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    if-eqz p2, :cond_0

    invoke-interface {p2, v10, v9, v0}, Lcom/gmobi/trade/ICallback;->onResult(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_10
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->A:Lcom/gmobi/trade/ICallback;

    if-eqz v0, :cond_11

    invoke-virtual {p0, p2, v10, v9, v9}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :cond_11
    iput-object p2, p0, Lcom/gmobi/trade/a/e;->A:Lcom/gmobi/trade/ICallback;

    invoke-direct {p0}, Lcom/gmobi/trade/a/e;->p()V

    iput-object p3, p0, Lcom/gmobi/trade/a/e;->B:Landroid/os/Bundle;

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    const-class v2, Lcom/gmobi/trade/TradeActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v0, "LoadingTitle"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->H:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "LoadingMessage"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->I:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Url"

    const-string v0, "://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_12

    :goto_1
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "#/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->G:Lcom/gmobi/trade/a/f;

    if-eqz v3, :cond_13

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->G:Lcom/gmobi/trade/a/f;

    invoke-virtual {v3}, Lcom/gmobi/trade/a/f;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_13

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "file:///"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->G:Lcom/gmobi/trade/a/f;

    invoke-virtual {v3}, Lcom/gmobi/trade/a/f;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "#/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_13
    if-eqz p3, :cond_14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x3f

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-ne v0, v5, :cond_15

    const-string v0, "?"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p3}, Lcom/gmobi/trade/a/e;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_14
    move-object p1, v0

    goto :goto_1

    :cond_15
    const-string v0, "&"

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    const/4 v3, 0x1

    const/4 v2, -0x1

    packed-switch p4, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-nez p2, :cond_2

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "market://details?id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "runRewardAction : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    new-instance v1, Lcom/gmobi/trade/a/e$4;

    invoke-direct {v1, p0, p1, p4}, Lcom/gmobi/trade/a/e$4;-><init>(Lcom/gmobi/trade/a/e;Ljava/lang/String;I)V

    invoke-virtual {v0, p1, p4, v1}, Lcom/gmobi/trade/a/c;->a(Ljava/lang/String;ILcom/gmobi/trade/a/c$a;)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-static {}, Lcom/a/e/f;->a()I

    move-result v1

    const-string v3, ""

    const v4, 0x108004e

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p3

    invoke-static/range {v0 .. v6}, Lcom/a/e/f;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;Landroid/app/PendingIntent;)I

    move-result v0

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->y:Landroid/os/Handler;

    new-instance v2, Lcom/gmobi/trade/a/e$3;

    invoke-direct {v2, p0, v0}, Lcom/gmobi/trade/a/e$3;-><init>(Lcom/gmobi/trade/a/e;I)V

    const-wide/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_1

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/app/DownloadManager$Request;

    invoke-direct {v1, v0}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".apk"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    invoke-virtual {v1, v4}, Landroid/app/DownloadManager$Request;->setShowRunningNotification(Z)Landroid/app/DownloadManager$Request;

    invoke-virtual {v1, v4}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    if-eqz p4, :cond_0

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setAllowedNetworkTypes(I)Landroid/app/DownloadManager$Request;

    :cond_0
    invoke-virtual {v1, p1}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->P:Landroid/app/DownloadManager;

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->u:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Ljava/lang/String;ZLcom/gmobi/trade/ICallback;)V
    .locals 3

    new-instance v0, Lcom/gmobi/trade/a/e$9;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/gmobi/trade/a/e$9;-><init>(Lcom/gmobi/trade/a/e;Ljava/lang/String;ZLcom/gmobi/trade/ICallback;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->K:Lcom/gmobi/a/d;

    invoke-virtual {v1}, Lcom/gmobi/a/d;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/gmobi/trade/a/e;->K:Lcom/gmobi/a/d;

    new-instance v2, Lcom/gmobi/trade/a/e$10;

    invoke-direct {v2, p0, p3, v0}, Lcom/gmobi/trade/a/e$10;-><init>(Lcom/gmobi/trade/a/e;Lcom/gmobi/trade/ICallback;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/gmobi/a/d;->a(Lcom/gmobi/a/d$d;)V

    goto :goto_0
.end method

.method public final a(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->A:Lcom/gmobi/trade/ICallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->A:Lcom/gmobi/trade/ICallback;

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    iput-object v1, p0, Lcom/gmobi/trade/a/e;->A:Lcom/gmobi/trade/ICallback;

    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 7

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->q:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->q:Ljava/lang/Thread$UncaughtExceptionHandler;

    new-instance v0, Lcom/gmobi/trade/a/e$1;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/e$1;-><init>(Lcom/gmobi/trade/a/e;)V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->y:Landroid/os/Handler;

    if-nez p4, :cond_2

    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    :cond_2
    invoke-virtual {p1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "go2."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".ini"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    sput-boolean v2, Lcom/gmobi/trade/TradeService;->DEBUG:Z

    :try_start_0
    invoke-static {v3}, Lcom/a/e/a;->a(Ljava/io/File;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_3
    :goto_1
    sget-boolean v2, Lcom/gmobi/trade/TradeService;->DEBUG:Z

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "go2."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x3

    invoke-static {p1, v1, v2, v3}, Lcom/a/e/e;->a(Landroid/content/Context;Ljava/lang/String;II)V

    :cond_4
    if-eqz v0, :cond_5

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    array-length v2, v1

    const/4 v0, 0x0

    :goto_2
    if-lt v0, v2, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    if-eqz v0, :cond_9

    const-string v0, "TradeService should not been started again!"

    invoke-static {v0}, Lcom/a/e/e;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    if-ne v0, p1, :cond_8

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_6
    aget-object v3, v1, v0

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_7

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, "//"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    iput-object p1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->p:Landroid/telephony/TelephonyManager;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->Q:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->P:Landroid/app/DownloadManager;

    new-instance v0, Lcom/gmobi/trade/a/e$11;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/e$11;-><init>(Lcom/gmobi/trade/a/e;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lcom/gmobi/a/d;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/gmobi/a/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->K:Lcom/gmobi/a/d;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->K:Lcom/gmobi/a/d;

    sget-boolean v0, Lcom/gmobi/trade/TradeService;->DEBUG:Z

    const-string v0, "MainActivity"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "MainActivity"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->N:Ljava/lang/String;

    :cond_a
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->p:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->L:Ljava/lang/String;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->p:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Real IMSI : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->p:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Real IMEI : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "IMSI"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "IMSI"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    :cond_b
    const-string v0, "IMEI"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "IMEI"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    :cond_c
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_19

    :cond_d
    new-instance v0, Lcom/gmobi/trade/a/e$12;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/e$12;-><init>(Lcom/gmobi/trade/a/e;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->y:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, ""

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IMSI is null and set a dummy IMSI as "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->c(Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_f

    :cond_e
    new-instance v0, Lcom/gmobi/trade/a/e$13;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/e$13;-><init>(Lcom/gmobi/trade/a/e;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->y:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-direct {p0}, Lcom/gmobi/trade/a/e;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IMEI is null and set a dummy IMEI as "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->c(Ljava/lang/String;)V

    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IMSI : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IMEI : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MCC : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "PageUrlBase"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "PageUrlBase"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->d:Ljava/lang/String;

    :cond_10
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->d:Ljava/lang/String;

    const-string v2, "{mcc}"

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    if-nez v0, :cond_1a

    const-string v0, ""

    :goto_4
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->d:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Page Url Base : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "ApiUrlBase"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "ApiUrlBase"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->c:Ljava/lang/String;

    :cond_11
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->c:Ljava/lang/String;

    const-string v2, "{mcc}"

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    if-nez v0, :cond_1b

    const-string v0, ""

    :goto_5
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->c:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "API Url Base : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "SdkUrlBase"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "SdkUrlBase"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    :cond_12
    iget-object v1, p0, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    const-string v2, "{mcc}"

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    if-nez v0, :cond_1c

    const-string v0, ""

    :goto_6
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SDK Url Base : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "BillingUrlBase"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "BillingUrlBase"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->e:Ljava/lang/String;

    :cond_13
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->e:Ljava/lang/String;

    const-string v2, "{mcc}"

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    if-nez v0, :cond_1d

    const-string v0, ""

    :goto_7
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->e:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Billing Url Base : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "LoadingTitle"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "LoadingTitle"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->H:Ljava/lang/String;

    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loading Title : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "LoadingMessage"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "LoadingMessage"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->I:Ljava/lang/String;

    :cond_15
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loading Message : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "Channel"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "Channel"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->J:Ljava/lang/String;

    :cond_16
    sget-boolean v0, Lcom/gmobi/trade/TradeService;->DEBUG:Z

    if-eqz v0, :cond_1e

    const/4 v0, 0x2

    invoke-static {v0}, La/a/b/a/c;->a(I)V

    :goto_8
    iput-object p2, p0, Lcom/gmobi/trade/a/e;->v:Ljava/lang/String;

    iput-object p3, p0, Lcom/gmobi/trade/a/e;->w:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "App Id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Secret Key : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/gmobi/trade/a/e;->r:Z

    const/4 v0, 0x0

    move v1, v0

    :goto_9
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1f

    const-string v0, "PushChannel"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    const-string v0, "PushChannel"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->l:Ljava/lang/String;

    :cond_17
    :goto_a
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    const-string v1, "com.gmobi.PushServiceDatabase"

    invoke-static {v0, v1}, Lcom/a/e/d;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/a/e/d;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->O:Lcom/a/e/d;

    new-instance v0, Lcom/gmobi/trade/a/a;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v2

    new-instance v3, Lcom/gmobi/trade/a/e$14;

    invoke-direct {v3, p0}, Lcom/gmobi/trade/a/e$14;-><init>(Lcom/gmobi/trade/a/e;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/trade/a/a;-><init>(Landroid/content/Context;Lcom/a/a/b;Lcom/gmobi/trade/a/a$a;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->F:Lcom/gmobi/trade/a/a;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->F:Lcom/gmobi/trade/a/a;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/a;->a()V

    new-instance v0, Lcom/a/d/f;

    invoke-direct {v0}, Lcom/a/d/f;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->C:Lcom/a/d/c;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->C:Lcom/a/d/c;

    invoke-interface {v0}, Lcom/a/d/c;->a()V

    new-instance v0, Lcom/a/d/a;

    invoke-direct {v0}, Lcom/a/d/a;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->D:Lcom/a/d/c;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->D:Lcom/a/d/c;

    invoke-interface {v0}, Lcom/a/d/c;->a()V

    new-instance v0, Lcom/a/d/b;

    const-string v1, "SDK 1.0"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->C:Lcom/a/d/c;

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->D:Lcom/a/d/c;

    invoke-direct {v0, v1, v2, v3}, Lcom/a/d/b;-><init>(Ljava/lang/String;Lcom/a/d/c;Lcom/a/d/c;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->E:Lcom/a/d/d;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->E:Lcom/a/d/d;

    invoke-interface {v0}, Lcom/a/d/d;->a()V

    new-instance v0, Lcom/gmobi/trade/a/c;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->E:Lcom/a/d/d;

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p4}, Lcom/gmobi/trade/a/c;-><init>(Lcom/a/d/d;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    const-string v0, "ForceRemote"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_18

    new-instance v0, Lcom/gmobi/trade/a/f;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->E:Lcom/a/d/d;

    iget-object v3, p0, Lcom/gmobi/trade/a/e;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/trade/a/f;-><init>(Landroid/content/Context;Lcom/a/d/d;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->G:Lcom/gmobi/trade/a/f;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->G:Lcom/gmobi/trade/a/f;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/f;->start()V

    :cond_18
    new-instance v0, Lcom/gmobi/trade/a/b;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/gmobi/trade/a/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->k:Lcom/gmobi/trade/a/b;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/e/g;->f(Landroid/content/Context;)Landroid/location/Location;

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_19
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    goto/16 :goto_3

    :cond_1a
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    goto/16 :goto_4

    :cond_1b
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    goto/16 :goto_5

    :cond_1c
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    goto/16 :goto_6

    :cond_1d
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->o:Ljava/lang/String;

    goto/16 :goto_7

    :cond_1e
    const/4 v0, 0x6

    invoke-static {v0}, La/a/b/a/c;->a(I)V

    goto/16 :goto_8

    :cond_1f
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iget-boolean v3, p0, Lcom/gmobi/trade/a/e;->r:Z

    if-nez v3, :cond_20

    const-string v3, "com.android.vending"

    iget-object v4, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "com.android.vending -> "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    if-eqz v3, :cond_20

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    const-string v3, "com.google."

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/gmobi/trade/a/e;->r:Z

    const-string v0, "Google Play in installed in this device!"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :cond_20
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_9

    :cond_21
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "app_channel"

    const-string v3, "string"

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_22

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_17

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->l:Ljava/lang/String;

    goto/16 :goto_a

    :cond_22
    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "app_domain"

    const-string v3, "string"

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_17

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_17

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->l:Ljava/lang/String;

    goto/16 :goto_a

    :catch_0
    move-exception v2

    goto/16 :goto_1
.end method

.method public final b()Lcom/a/e/d$a;
    .locals 2

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->O:Lcom/a/e/d;

    const-string v1, "Popups"

    invoke-virtual {v0, v1}, Lcom/a/e/d;->a(Ljava/lang/String;)Lcom/a/e/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "recordReward : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    new-instance v1, Lcom/gmobi/trade/a/e$2;

    invoke-direct {v1, p0}, Lcom/gmobi/trade/a/e$2;-><init>(Lcom/gmobi/trade/a/e;)V

    invoke-virtual {v0, p1, v2, v1}, Lcom/gmobi/trade/a/c;->a(Ljava/lang/String;ILcom/gmobi/trade/a/c$a;)V

    return-void
.end method

.method public final b(Z)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lcom/gmobi/trade/a/e;->a(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final c()Lcom/a/e/d$a;
    .locals 2

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->O:Lcom/a/e/d;

    const-string v1, "Data"

    invoke-virtual {v0, v1}, Lcom/a/e/d;->a(Ljava/lang/String;)Lcom/a/e/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->Q:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    return-void
.end method

.method public final d()Lcom/a/e/d$a;
    .locals 2

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->O:Lcom/a/e/d;

    const-string v1, "AppStatus"

    invoke-virtual {v0, v1}, Lcom/a/e/d;->a(Ljava/lang/String;)Lcom/a/e/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/gmobi/trade/a/b;
    .locals 1

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->k:Lcom/gmobi/trade/a/b;

    return-object v0
.end method

.method public final f()Lcom/a/d/d;
    .locals 1

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->E:Lcom/a/d/d;

    return-object v0
.end method

.method public final g()Lorg/json/JSONObject;
    .locals 5

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "sdk_version"

    const-string v2, "1.0"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "app_id"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "imsi"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "imei"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "device_id"

    invoke-direct {p0}, Lcom/gmobi/trade/a/e;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ua"

    invoke-static {}, Lcom/gmobi/trade/a/e;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sc"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-static {v2}, Lcom/gmobi/trade/a/e;->c(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "screen"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "platform"

    const-string v2, "android"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "platform_ver"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "channel"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->J:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "serviceId"

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "lang"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "lang_country"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "phone_number"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->L:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "appId"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->v:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "appSecret"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->w:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "apiUrlBase"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "billingUrlBase"

    iget-object v2, p0, Lcom/gmobi/trade/a/e;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "googlePlayInstalled"

    iget-boolean v2, p0, Lcom/gmobi/trade/a/e;->r:Z

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->B:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->B:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "extra"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    invoke-direct {p0}, Lcom/gmobi/trade/a/e;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "user_id"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Device Info: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :goto_1
    return-object v1

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lcom/gmobi/trade/a/e;->B:Landroid/os/Bundle;

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final h()[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [Ljava/lang/String;

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    return-object v3

    :cond_0
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/gmobi/a/d;
    .locals 1

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->K:Lcom/gmobi/a/d;

    return-object v0
.end method

.method public final k()Lcom/a/a/b;
    .locals 3

    iget-object v0, p0, Lcom/gmobi/trade/a/e;->R:Lcom/a/a/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/a/a/b;

    iget-object v1, p0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    const-string v2, "com.gmobi.TradeServiceStorage"

    invoke-direct {v0, v1, v2}, Lcom/a/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/e;->R:Lcom/a/a/b;

    :cond_0
    iget-object v0, p0, Lcom/gmobi/trade/a/e;->R:Lcom/a/a/b;

    return-object v0
.end method
