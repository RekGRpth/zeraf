.class final Lcom/gmobi/trade/a/b$2;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gmobi/trade/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/b;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/gmobi/trade/a/b$2;)Lcom/gmobi/trade/a/b;
    .locals 1

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    return-object v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Screen Off"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->o:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, v0, Lcom/gmobi/trade/a/b;->o:Ljava/util/Timer;

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->o:Ljava/util/Timer;

    new-instance v1, Lcom/gmobi/trade/a/b$2$1;

    invoke-direct {v1, p0}, Lcom/gmobi/trade/a/b$2$1;-><init>(Lcom/gmobi/trade/a/b$2;)V

    sget-wide v2, Lcom/gmobi/trade/a/b;->a:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iput-boolean v4, v0, Lcom/gmobi/trade/a/b;->i:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Screen On"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->o:Ljava/util/Timer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iput-object v3, v0, Lcom/gmobi/trade/a/b;->o:Ljava/util/Timer;

    :cond_3
    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    invoke-static {v0}, Lcom/gmobi/trade/a/b;->a(Lcom/gmobi/trade/a/b;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iput-boolean v2, v0, Lcom/gmobi/trade/a/b;->i:Z

    iget-object v0, p0, Lcom/gmobi/trade/a/b$2;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->r:Lcom/a/e/d$a;

    invoke-virtual {v0, v3, v2, v2}, Lcom/a/e/d$a;->a(Lcom/gmobi/a/d$b;ZI)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v2, :cond_1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/e/d$b;

    invoke-virtual {v0}, Lcom/a/e/d$b;->b()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0}, Lcom/a/e/d$b;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "TYPE"

    const-string v4, "popup"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "JSON"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "DOC_ID"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/gmobi/trade/ActionActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
