.class final Lcom/gmobi/trade/a/a$2;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/a;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/a$2;->a:Lcom/gmobi/trade/a/a;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    const/16 v9, 0x64

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/gmobi/trade/a/e;->e()Lcom/gmobi/trade/a/b;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/gmobi/trade/a/e;->e()Lcom/gmobi/trade/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/gmobi/trade/a/b;->f()Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    :goto_0
    iget-object v3, p0, Lcom/gmobi/trade/a/a$2;->a:Lcom/gmobi/trade/a/a;

    iget-object v0, v3, Lcom/gmobi/trade/a/a;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, v3, Lcom/gmobi/trade/a/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v0, v3, Lcom/gmobi/trade/a/a;->a:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v9}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, v9, v7}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/gmobi/trade/a/a$2;->a:Lcom/gmobi/trade/a/a;

    iget-object v0, v0, Lcom/gmobi/trade/a/a;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    return-void

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v6, v3, Lcom/gmobi/trade/a/a;->e:Ljava/util/Set;

    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RecentTaskInfo;

    new-instance v5, Landroid/content/Intent;

    iget-object v6, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v6, v0, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    if-eqz v6, :cond_4

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :cond_4
    invoke-virtual {v5}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const v6, -0x200001

    and-int/2addr v0, v6

    const/high16 v6, 0x10000000

    or-int/2addr v0, v6

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {v4, v5, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v5, v3, Lcom/gmobi/trade/a/a;->e:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/gmobi/trade/a/a$2;->a:Lcom/gmobi/trade/a/a;

    iget-object v3, v3, Lcom/gmobi/trade/a/a;->d:Lcom/a/a/b;

    invoke-virtual {v3, v0}, Lcom/a/a/b;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/gmobi/trade/a/a$2;->a:Lcom/gmobi/trade/a/a;

    iget-object v3, v3, Lcom/gmobi/trade/a/a;->d:Lcom/a/a/b;

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, Lcom/a/a/b;->a(Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v8, :cond_6

    iget-object v3, p0, Lcom/gmobi/trade/a/a$2;->a:Lcom/gmobi/trade/a/a;

    iget-object v3, v3, Lcom/gmobi/trade/a/a;->b:Lcom/gmobi/trade/a/a$a;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/gmobi/trade/a/a$2;->a:Lcom/gmobi/trade/a/a;

    iget-object v3, v3, Lcom/gmobi/trade/a/a;->b:Lcom/gmobi/trade/a/a$a;

    invoke-interface {v3, v0}, Lcom/gmobi/trade/a/a$a;->a(Ljava/lang/String;)V

    :cond_6
    if-eqz v2, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/gmobi/trade/a/e;->e()Lcom/gmobi/trade/a/b;

    move-result-object v3

    invoke-virtual {v3, v0, v8}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;I)V

    goto/16 :goto_3

    :cond_7
    move-object v2, v0

    goto/16 :goto_0
.end method
