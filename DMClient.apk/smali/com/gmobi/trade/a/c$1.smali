.class final Lcom/gmobi/trade/a/c$1;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/c;->a(Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/c;

.field private final synthetic b:Lorg/json/JSONObject;

.field private final synthetic c:Lcom/gmobi/trade/a/c$a;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/c;Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/c$1;->a:Lcom/gmobi/trade/a/c;

    iput-object p2, p0, Lcom/gmobi/trade/a/c$1;->b:Lorg/json/JSONObject;

    iput-object p3, p0, Lcom/gmobi/trade/a/c$1;->c:Lcom/gmobi/trade/a/c$a;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    :goto_0
    iget-object v0, p0, Lcom/gmobi/trade/a/c$1;->a:Lcom/gmobi/trade/a/c;

    iget-object v1, p0, Lcom/gmobi/trade/a/c$1;->a:Lcom/gmobi/trade/a/c;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, v1, Lcom/gmobi/trade/a/c;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/gmobi/trade/a/c;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/gmobi/trade/a/c$1;->b:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/gmobi/trade/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/gmobi/trade/a/c$b;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connect Resp : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/gmobi/trade/a/c$b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/gmobi/trade/a/c$b;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget v1, v0, Lcom/gmobi/trade/a/c$b;->a:I

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    iget-object v0, v0, Lcom/gmobi/trade/a/c$b;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/a/a/a/a$a;->b(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lcom/gmobi/trade/a/c$1;->c:Lcom/gmobi/trade/a/c$a;

    invoke-interface {v1, v0}, Lcom/gmobi/trade/a/c$a;->a(Lorg/json/JSONObject;)V

    return-void

    :cond_0
    :try_start_0
    const-string v0, "Retry connecting"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-wide/16 v0, 0x1388

    invoke-static {v0, v1}, Lcom/gmobi/trade/a/c$1;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
