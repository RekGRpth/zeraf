.class final Lcom/gmobi/trade/a/e$18;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Z

.field private final synthetic b:Lcom/gmobi/trade/ICallback;

.field private final synthetic c:Landroid/os/Bundle;

.field private final synthetic d:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/e;ZLcom/gmobi/trade/ICallback;Landroid/os/Bundle;Ljava/lang/Throwable;)V
    .locals 0

    iput-boolean p2, p0, Lcom/gmobi/trade/a/e$18;->a:Z

    iput-object p3, p0, Lcom/gmobi/trade/a/e$18;->b:Lcom/gmobi/trade/ICallback;

    iput-object p4, p0, Lcom/gmobi/trade/a/e$18;->c:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/gmobi/trade/a/e$18;->d:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Trigger Callback : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/gmobi/trade/a/e$18;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e$18;->b:Lcom/gmobi/trade/ICallback;

    iget-boolean v1, p0, Lcom/gmobi/trade/a/e$18;->a:Z

    iget-object v2, p0, Lcom/gmobi/trade/a/e$18;->c:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/gmobi/trade/a/e$18;->d:Ljava/lang/Throwable;

    invoke-interface {v0, v1, v2, v3}, Lcom/gmobi/trade/ICallback;->onResult(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    return-void
.end method
