.class final Lcom/gmobi/trade/a/e$9$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/gmobi/a/d$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/e$9;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/gmobi/trade/a/e$9;

.field private final synthetic b:Z

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Lcom/gmobi/trade/ICallback;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/e$9;ZLjava/lang/String;Lcom/gmobi/trade/ICallback;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/e$9$1;->a:Lcom/gmobi/trade/a/e$9;

    iput-boolean p2, p0, Lcom/gmobi/trade/a/e$9$1;->b:Z

    iput-object p3, p0, Lcom/gmobi/trade/a/e$9$1;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/gmobi/trade/a/e$9$1;->d:Lcom/gmobi/trade/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/gmobi/a/e;Lcom/gmobi/a/g;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Purchase finished: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", purchase: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/gmobi/a/e;->d()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/gmobi/trade/a/e$9$1;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/gmobi/a/e;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/gmobi/a/e;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/gmobi/trade/a/e$9$1;->b:Z

    iget-boolean v2, p0, Lcom/gmobi/trade/a/e$9$1;->b:Z

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Force consume : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/gmobi/trade/a/e$9$1;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->c(Ljava/lang/String;)V

    move v0, v1

    :cond_0
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/gmobi/a/e;->a()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/gmobi/trade/a/e$9$1;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :try_start_0
    iget-object v2, p0, Lcom/gmobi/trade/a/e$9$1;->a:Lcom/gmobi/trade/a/e$9;

    invoke-static {v2}, Lcom/gmobi/trade/a/e$9;->a(Lcom/gmobi/trade/a/e$9;)Lcom/gmobi/trade/a/e;

    move-result-object v2

    invoke-static {v2}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/a/e;)Lcom/gmobi/a/d;

    move-result-object v2

    const/4 v3, 0x1

    new-instance v4, Lcom/gmobi/trade/a/e$9$1$1;

    invoke-direct {v4, p0}, Lcom/gmobi/trade/a/e$9$1$1;-><init>(Lcom/gmobi/trade/a/e$9$1;)V

    invoke-virtual {v2, v3, v0, v4}, Lcom/gmobi/a/d;->a(ZLjava/util/List;Lcom/gmobi/a/d$e;)V

    iget-object v2, p0, Lcom/gmobi/trade/a/e$9$1;->a:Lcom/gmobi/trade/a/e$9;

    invoke-static {v2}, Lcom/gmobi/trade/a/e$9;->a(Lcom/gmobi/trade/a/e$9;)Lcom/gmobi/trade/a/e;

    move-result-object v2

    invoke-static {v2}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/a/e;)Lcom/gmobi/a/d;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lcom/gmobi/a/d;->a(ZLjava/util/List;)Lcom/gmobi/a/f;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Inventory : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/gmobi/trade/a/e$9$1;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/gmobi/a/f;->a(Ljava/lang/String;)Lcom/gmobi/a/g;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Purchase : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/gmobi/trade/a/e$9$1;->d:Lcom/gmobi/trade/ICallback;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/gmobi/trade/ICallback;->onResult(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/gmobi/trade/a/e$9$1;->a:Lcom/gmobi/trade/a/e$9;

    invoke-static {v0}, Lcom/gmobi/trade/a/e$9;->a(Lcom/gmobi/trade/a/e$9;)Lcom/gmobi/trade/a/e;

    move-result-object v0

    invoke-static {v0}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/a/e;)Lcom/gmobi/a/d;

    move-result-object v0

    new-instance v2, Lcom/gmobi/trade/a/e$9$1$2;

    iget-object v3, p0, Lcom/gmobi/trade/a/e$9$1;->d:Lcom/gmobi/trade/ICallback;

    iget-object v4, p0, Lcom/gmobi/trade/a/e$9$1;->c:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/gmobi/trade/a/e$9$1;->b:Z

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/gmobi/trade/a/e$9$1$2;-><init>(Lcom/gmobi/trade/a/e$9$1;Lcom/gmobi/trade/ICallback;Ljava/lang/String;Z)V

    invoke-virtual {v0, p2, v2}, Lcom/gmobi/a/d;->a(Lcom/gmobi/a/g;Lcom/gmobi/a/d$a;)V
    :try_end_0
    .catch Lcom/gmobi/a/c; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "result"

    invoke-virtual {p1}, Lcom/gmobi/a/e;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/gmobi/trade/a/e$9$1;->b:Z

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/gmobi/trade/a/e$9$1;->a:Lcom/gmobi/trade/a/e$9;

    invoke-static {v1}, Lcom/gmobi/trade/a/e$9;->a(Lcom/gmobi/trade/a/e$9;)Lcom/gmobi/trade/a/e;

    move-result-object v1

    invoke-static {v1}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/a/e;)Lcom/gmobi/a/d;

    move-result-object v1

    new-instance v2, Lcom/gmobi/trade/a/e$9$1$3;

    iget-object v3, p0, Lcom/gmobi/trade/a/e$9$1;->d:Lcom/gmobi/trade/ICallback;

    invoke-direct {v2, p0, v3, v0}, Lcom/gmobi/trade/a/e$9$1$3;-><init>(Lcom/gmobi/trade/a/e$9$1;Lcom/gmobi/trade/ICallback;Landroid/os/Bundle;)V

    invoke-virtual {v1, p2, v2}, Lcom/gmobi/a/d;->a(Lcom/gmobi/a/g;Lcom/gmobi/a/d$a;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e$9$1;->d:Lcom/gmobi/trade/ICallback;

    invoke-interface {v0, v7, v6, v6}, Lcom/gmobi/trade/ICallback;->onResult(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/gmobi/trade/a/e$9$1;->d:Lcom/gmobi/trade/ICallback;

    invoke-interface {v0, v7, v6, v6}, Lcom/gmobi/trade/ICallback;->onResult(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/gmobi/trade/a/e$9$1;->d:Lcom/gmobi/trade/ICallback;

    invoke-interface {v2, v1, v0, v6}, Lcom/gmobi/trade/ICallback;->onResult(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
