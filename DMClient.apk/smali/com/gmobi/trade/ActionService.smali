.class public Lcom/gmobi/trade/ActionService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field public static final ACTION_APP_ADDED:Ljava/lang/String; = "ACTION_APP_ADDED"

.field public static final ACTION_KEEP_ACTIVE:Ljava/lang/String; = "ACTION_KEEP_ACTIVE"

.field public static final PARAM_APP_ID:Ljava/lang/String; = "APP_ID"

.field public static final PARAM_DELAY:Ljava/lang/String; = "DELAY"

.field public static final PARAM_DOWNLOAD_ID:Ljava/lang/String; = "DOWNLOAD_ID"

.field public static final PARAM_ID:Ljava/lang/String; = "ID"

.field public static final PARAM_SMS_MESSAGE:Ljava/lang/String; = "SMS_MESSAGE"

.field public static final PARAM_SMS_NUMBER:Ljava/lang/String; = "SMS_NUMBER"

.field public static final PARAM_URL:Ljava/lang/String; = "URL"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    const-string v0, "ActionService onCreate"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    const-string v0, "ActionService onDestroy"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    const-string v0, "ActionService onLowMemory"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onLowMemory()V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 5

    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ACTION_APP_ADDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "APP_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Billing Service runRewardAction "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":2"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/gmobi/trade/TradeService;->getInstance()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/gmobi/trade/ActionService;->getApplication()Landroid/app/Application;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4}, Lcom/gmobi/trade/TradeService;->start(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    invoke-static {}, Lcom/gmobi/trade/TradeService;->getInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/gmobi/trade/a/e;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "ACTION_KEEP_ACTIVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Keep Active ..."

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
