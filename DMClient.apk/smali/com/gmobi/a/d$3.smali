.class final Lcom/gmobi/a/d$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gmobi/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/a/d;

.field private final synthetic b:Ljava/util/List;

.field private final synthetic c:Lcom/gmobi/a/d$a;

.field private final synthetic d:Landroid/os/Handler;

.field private final synthetic e:Lcom/gmobi/a/d$b;


# direct methods
.method constructor <init>(Lcom/gmobi/a/d;Ljava/util/List;Lcom/gmobi/a/d$a;Landroid/os/Handler;Lcom/gmobi/a/d$b;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/a/d$3;->a:Lcom/gmobi/a/d;

    iput-object p2, p0, Lcom/gmobi/a/d$3;->b:Ljava/util/List;

    iput-object p3, p0, Lcom/gmobi/a/d$3;->c:Lcom/gmobi/a/d$a;

    iput-object p4, p0, Lcom/gmobi/a/d$3;->d:Landroid/os/Handler;

    iput-object p5, p0, Lcom/gmobi/a/d$3;->e:Lcom/gmobi/a/d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/gmobi/a/d$3;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/gmobi/a/d$3;->a:Lcom/gmobi/a/d;

    invoke-virtual {v0}, Lcom/gmobi/a/d;->b()V

    iget-object v0, p0, Lcom/gmobi/a/d$3;->c:Lcom/gmobi/a/d$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/a/d$3;->d:Landroid/os/Handler;

    new-instance v1, Lcom/gmobi/a/d$3$1;

    iget-object v3, p0, Lcom/gmobi/a/d$3;->c:Lcom/gmobi/a/d$a;

    iget-object v4, p0, Lcom/gmobi/a/d$3;->b:Ljava/util/List;

    invoke-direct {v1, p0, v3, v4, v2}, Lcom/gmobi/a/d$3$1;-><init>(Lcom/gmobi/a/d$3;Lcom/gmobi/a/d$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lcom/gmobi/a/d$3;->e:Lcom/gmobi/a/d$b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/gmobi/a/d$3;->d:Landroid/os/Handler;

    new-instance v1, Lcom/gmobi/a/d$3$2;

    iget-object v3, p0, Lcom/gmobi/a/d$3;->e:Lcom/gmobi/a/d$b;

    iget-object v4, p0, Lcom/gmobi/a/d$3;->b:Ljava/util/List;

    invoke-direct {v1, p0, v3, v4, v2}, Lcom/gmobi/a/d$3$2;-><init>(Lcom/gmobi/a/d$3;Lcom/gmobi/a/d$b;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/gmobi/a/g;

    :try_start_0
    iget-object v1, p0, Lcom/gmobi/a/d$3;->a:Lcom/gmobi/a/d;

    const-string v4, "consume"

    invoke-virtual {v1, v4}, Lcom/gmobi/a/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/gmobi/a/c; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v4, v0, Lcom/gmobi/a/g;->b:Ljava/lang/String;

    iget-object v5, v0, Lcom/gmobi/a/g;->a:Ljava/lang/String;

    if-eqz v4, :cond_3

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Can\'t consume "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ". No token."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->d(Ljava/lang/String;)V

    new-instance v1, Lcom/gmobi/a/c;

    const/16 v4, -0x3ef

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PurchaseInfo is missing token for sku: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/gmobi/a/c;-><init>(ILjava/lang/String;)V

    throw v1
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/gmobi/a/c; {:try_start_1 .. :try_end_1} :catch_1

    :catch_0
    move-exception v1

    :try_start_2
    new-instance v4, Lcom/gmobi/a/c;

    const/16 v5, -0x3e9

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Remote exception while consuming. PurchaseInfo: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0, v1}, Lcom/gmobi/a/c;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v4
    :try_end_2
    .catch Lcom/gmobi/a/c; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/gmobi/a/c;->a()Lcom/gmobi/a/e;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    :try_start_3
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Consuming sku: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", token: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v6, v1, Lcom/gmobi/a/d;->c:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v7, 0x3

    iget-object v1, v1, Lcom/gmobi/a/d;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v7, v1, v4}, Lcom/android/vending/billing/IInAppBillingService;->consumePurchase(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Successfully consumed sku: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/gmobi/a/c; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    new-instance v1, Lcom/gmobi/a/e;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Successful consume of sku "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/gmobi/a/g;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lcom/gmobi/a/e;-><init>(ILjava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/gmobi/a/c; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    :cond_5
    :try_start_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Error consuming consuming sku "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ". "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Lcom/gmobi/a/d;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v4, Lcom/gmobi/a/c;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error consuming sku "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/gmobi/a/c;-><init>(ILjava/lang/String;)V

    throw v4
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/gmobi/a/c; {:try_start_5 .. :try_end_5} :catch_1
.end method
