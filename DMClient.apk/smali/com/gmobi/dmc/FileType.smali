.class public final enum Lcom/gmobi/dmc/FileType;
.super Ljava/lang/Enum;
.source "FileType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/gmobi/dmc/FileType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/gmobi/dmc/FileType;

.field public static final enum ASF:Lcom/gmobi/dmc/FileType;

.field public static final enum AVI:Lcom/gmobi/dmc/FileType;

.field public static final enum BMP:Lcom/gmobi/dmc/FileType;

.field public static final enum DBX:Lcom/gmobi/dmc/FileType;

.field public static final enum DWG:Lcom/gmobi/dmc/FileType;

.field public static final enum EML:Lcom/gmobi/dmc/FileType;

.field public static final enum EPS:Lcom/gmobi/dmc/FileType;

.field public static final enum GIF:Lcom/gmobi/dmc/FileType;

.field public static final enum HTML:Lcom/gmobi/dmc/FileType;

.field public static final enum JPEG:Lcom/gmobi/dmc/FileType;

.field public static final enum MDB:Lcom/gmobi/dmc/FileType;

.field public static final enum MID:Lcom/gmobi/dmc/FileType;

.field public static final enum MOV:Lcom/gmobi/dmc/FileType;

.field public static final enum MPG:Lcom/gmobi/dmc/FileType;

.field public static final enum PDF:Lcom/gmobi/dmc/FileType;

.field public static final enum PNG:Lcom/gmobi/dmc/FileType;

.field public static final enum PSD:Lcom/gmobi/dmc/FileType;

.field public static final enum PST:Lcom/gmobi/dmc/FileType;

.field public static final enum PWL:Lcom/gmobi/dmc/FileType;

.field public static final enum QDF:Lcom/gmobi/dmc/FileType;

.field public static final enum RAM:Lcom/gmobi/dmc/FileType;

.field public static final enum RAR:Lcom/gmobi/dmc/FileType;

.field public static final enum RM:Lcom/gmobi/dmc/FileType;

.field public static final enum RTF:Lcom/gmobi/dmc/FileType;

.field public static final enum TIFF:Lcom/gmobi/dmc/FileType;

.field public static final enum WAV:Lcom/gmobi/dmc/FileType;

.field public static final enum WPD:Lcom/gmobi/dmc/FileType;

.field public static final enum XLS_DOC:Lcom/gmobi/dmc/FileType;

.field public static final enum XML:Lcom/gmobi/dmc/FileType;

.field public static final enum ZIP:Lcom/gmobi/dmc/FileType;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "JPEG"

    const-string v2, "FFD8FF"

    invoke-direct {v0, v1, v4, v2}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->JPEG:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "PNG"

    const-string v2, "89504E47"

    invoke-direct {v0, v1, v5, v2}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->PNG:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "GIF"

    const-string v2, "47494638"

    invoke-direct {v0, v1, v6, v2}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->GIF:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "TIFF"

    const-string v2, "49492A00"

    invoke-direct {v0, v1, v7, v2}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->TIFF:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "BMP"

    const-string v2, "424D"

    invoke-direct {v0, v1, v8, v2}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->BMP:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "DWG"

    const/4 v2, 0x5

    const-string v3, "41433130"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->DWG:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "PSD"

    const/4 v2, 0x6

    const-string v3, "38425053"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->PSD:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "RTF"

    const/4 v2, 0x7

    const-string v3, "7B5C727466"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->RTF:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "XML"

    const/16 v2, 0x8

    const-string v3, "3C3F786D6C"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->XML:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "HTML"

    const/16 v2, 0x9

    const-string v3, "68746D6C3E"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->HTML:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "EML"

    const/16 v2, 0xa

    const-string v3, "44656C69766572792D646174653A"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->EML:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "DBX"

    const/16 v2, 0xb

    const-string v3, "CFAD12FEC5FD746F"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->DBX:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "PST"

    const/16 v2, 0xc

    const-string v3, "2142444E"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->PST:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "XLS_DOC"

    const/16 v2, 0xd

    const-string v3, "D0CF11E0"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->XLS_DOC:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "MDB"

    const/16 v2, 0xe

    const-string v3, "5374616E64617264204A"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->MDB:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "WPD"

    const/16 v2, 0xf

    const-string v3, "FF575043"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->WPD:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "EPS"

    const/16 v2, 0x10

    const-string v3, "252150532D41646F6265"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->EPS:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "PDF"

    const/16 v2, 0x11

    const-string v3, "255044462D312E"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->PDF:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "QDF"

    const/16 v2, 0x12

    const-string v3, "AC9EBD8F"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->QDF:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "PWL"

    const/16 v2, 0x13

    const-string v3, "E3828596"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->PWL:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "ZIP"

    const/16 v2, 0x14

    const-string v3, "504B0304"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->ZIP:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "RAR"

    const/16 v2, 0x15

    const-string v3, "52617221"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->RAR:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "WAV"

    const/16 v2, 0x16

    const-string v3, "57415645"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->WAV:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "AVI"

    const/16 v2, 0x17

    const-string v3, "41564920"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->AVI:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "RAM"

    const/16 v2, 0x18

    const-string v3, "2E7261FD"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->RAM:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "RM"

    const/16 v2, 0x19

    const-string v3, "2E524D46"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->RM:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "MPG"

    const/16 v2, 0x1a

    const-string v3, "000001BA"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->MPG:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "MOV"

    const/16 v2, 0x1b

    const-string v3, "6D6F6F76"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->MOV:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "ASF"

    const/16 v2, 0x1c

    const-string v3, "3026B2758E66CF11"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->ASF:Lcom/gmobi/dmc/FileType;

    new-instance v0, Lcom/gmobi/dmc/FileType;

    const-string v1, "MID"

    const/16 v2, 0x1d

    const-string v3, "4D546864"

    invoke-direct {v0, v1, v2, v3}, Lcom/gmobi/dmc/FileType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/gmobi/dmc/FileType;->MID:Lcom/gmobi/dmc/FileType;

    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/gmobi/dmc/FileType;

    sget-object v1, Lcom/gmobi/dmc/FileType;->JPEG:Lcom/gmobi/dmc/FileType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/gmobi/dmc/FileType;->PNG:Lcom/gmobi/dmc/FileType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/gmobi/dmc/FileType;->GIF:Lcom/gmobi/dmc/FileType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/gmobi/dmc/FileType;->TIFF:Lcom/gmobi/dmc/FileType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/gmobi/dmc/FileType;->BMP:Lcom/gmobi/dmc/FileType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/gmobi/dmc/FileType;->DWG:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/gmobi/dmc/FileType;->PSD:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/gmobi/dmc/FileType;->RTF:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/gmobi/dmc/FileType;->XML:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/gmobi/dmc/FileType;->HTML:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/gmobi/dmc/FileType;->EML:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/gmobi/dmc/FileType;->DBX:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/gmobi/dmc/FileType;->PST:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/gmobi/dmc/FileType;->XLS_DOC:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/gmobi/dmc/FileType;->MDB:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/gmobi/dmc/FileType;->WPD:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/gmobi/dmc/FileType;->EPS:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/gmobi/dmc/FileType;->PDF:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/gmobi/dmc/FileType;->QDF:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/gmobi/dmc/FileType;->PWL:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/gmobi/dmc/FileType;->ZIP:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/gmobi/dmc/FileType;->RAR:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/gmobi/dmc/FileType;->WAV:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/gmobi/dmc/FileType;->AVI:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/gmobi/dmc/FileType;->RAM:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/gmobi/dmc/FileType;->RM:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/gmobi/dmc/FileType;->MPG:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/gmobi/dmc/FileType;->MOV:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/gmobi/dmc/FileType;->ASF:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/gmobi/dmc/FileType;->MID:Lcom/gmobi/dmc/FileType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/gmobi/dmc/FileType;->$VALUES:[Lcom/gmobi/dmc/FileType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const-string v0, ""

    iput-object v0, p0, Lcom/gmobi/dmc/FileType;->value:Ljava/lang/String;

    iput-object p3, p0, Lcom/gmobi/dmc/FileType;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/gmobi/dmc/FileType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/gmobi/dmc/FileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/gmobi/dmc/FileType;

    return-object v0
.end method

.method public static values()[Lcom/gmobi/dmc/FileType;
    .locals 1

    sget-object v0, Lcom/gmobi/dmc/FileType;->$VALUES:[Lcom/gmobi/dmc/FileType;

    invoke-virtual {v0}, [Lcom/gmobi/dmc/FileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/gmobi/dmc/FileType;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/gmobi/dmc/FileType;->value:Ljava/lang/String;

    return-object v0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/gmobi/dmc/FileType;->value:Ljava/lang/String;

    return-void
.end method
