.class public La/a/b/h;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements La/a/b/a/a;


# static fields
.field public static TAG:Ljava/lang/String;

.field private static b:I

.field private static c:I

.field private static d:I


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field protected activityResultCallback:La/a/b/a/b;

.field protected activityResultKeepRunning:Z

.field protected appView:La/a/b/e;

.field protected cancelLoadUrl:Z

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field protected keepRunning:Z

.field protected loadUrlTimeoutValue:I

.field protected root:Landroid/widget/LinearLayout;

.field protected spinnerDialog:Landroid/app/ProgressDialog;

.field protected splashDialog:Landroid/app/Dialog;

.field protected splashscreen:I

.field protected splashscreenTime:I

.field protected webViewClient:La/a/b/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "DroidGap"

    sput-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, La/a/b/h;->b:I

    const/4 v0, 0x1

    sput v0, La/a/b/h;->c:I

    const/4 v0, 0x2

    sput v0, La/a/b/h;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v1, p0, La/a/b/h;->cancelLoadUrl:Z

    iput-object v2, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, La/a/b/h;->a:Ljava/util/concurrent/ExecutorService;

    iput v1, p0, La/a/b/h;->e:I

    iput-object v2, p0, La/a/b/h;->activityResultCallback:La/a/b/a/b;

    const/high16 v0, -0x1000000

    iput v0, p0, La/a/b/h;->f:I

    iput v1, p0, La/a/b/h;->splashscreen:I

    const/16 v0, 0xbb8

    iput v0, p0, La/a/b/h;->splashscreenTime:I

    const/16 v0, 0x4e20

    iput v0, p0, La/a/b/h;->loadUrlTimeoutValue:I

    const/4 v0, 0x1

    iput-boolean v0, p0, La/a/b/h;->keepRunning:Z

    return-void
.end method


# virtual methods
.method public addService(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->a:La/a/b/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->a:La/a/b/a/e;

    invoke-virtual {v0, p1, p2}, La/a/b/a/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public backHistory()Z
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0}, La/a/b/e;->b()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelLoadUrl()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, La/a/b/h;->cancelLoadUrl:Z

    return-void
.end method

.method public clearAuthenticationTokens()V
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->b:La/a/b/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->b:La/a/b/f;

    invoke-virtual {v0}, La/a/b/f;->clearAuthenticationTokens()V

    :cond_0
    return-void
.end method

.method public clearCache()V
    .locals 2

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-nez v0, :cond_0

    invoke-virtual {p0}, La/a/b/h;->init()V

    :cond_0
    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, La/a/b/e;->clearCache(Z)V

    return-void
.end method

.method public clearHistory()V
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0}, La/a/b/e;->clearHistory()V

    return-void
.end method

.method public displayError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    new-instance v0, La/a/b/h$4;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, La/a/b/h$4;-><init>(La/a/b/h;La/a/b/h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, La/a/b/h;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public endActivity()V
    .locals 1

    sget v0, La/a/b/h;->d:I

    iput v0, p0, La/a/b/h;->e:I

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 0

    return-object p0
.end method

.method public getAuthenticationToken$4d90f81c(Ljava/lang/String;Ljava/lang/String;)Landroid/a/a/a/a$a;
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->b:La/a/b/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->b:La/a/b/f;

    invoke-virtual {v0, p1, p2}, La/a/b/f;->getAuthenticationToken$4d90f81c(Ljava/lang/String;Ljava/lang/String;)Landroid/a/a/a/a$a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBooleanProperty(Ljava/lang/String;Z)Z
    .locals 2

    invoke-virtual {p0}, La/a/b/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1
.end method

.method public getContext()Landroid/content/Context;
    .locals 2

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "This will be deprecated December 2012"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public getDoubleProperty(Ljava/lang/String;D)D
    .locals 2

    invoke-virtual {p0}, La/a/b/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-wide p2

    :cond_1
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_1
.end method

.method public getIntegerProperty(Ljava/lang/String;I)I
    .locals 2

    invoke-virtual {p0}, La/a/b/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1
.end method

.method public getStringProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, La/a/b/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p2, v0

    goto :goto_0
.end method

.method public getThreadPool()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, La/a/b/h;->a:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public init()V
    .locals 3

    new-instance v1, La/a/b/e;

    invoke-direct {v1, p0}, La/a/b/e;-><init>(Landroid/content/Context;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_0

    new-instance v0, La/a/b/f;

    invoke-direct {v0, p0, v1}, La/a/b/f;-><init>(La/a/b/a/a;La/a/b/e;)V

    :goto_0
    new-instance v2, La/a/b/d;

    invoke-direct {v2, p0, v1}, La/a/b/d;-><init>(La/a/b/a/a;La/a/b/e;)V

    invoke-virtual {p0, v1, v0, v2}, La/a/b/h;->init(La/a/b/e;La/a/b/f;La/a/b/d;)V

    return-void

    :cond_0
    new-instance v0, La/a/b/j;

    invoke-direct {v0, p0, v1}, La/a/b/j;-><init>(La/a/b/a/a;La/a/b/e;)V

    goto :goto_0
.end method

.method public init(La/a/b/e;La/a/b/f;La/a/b/d;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v3, -0x1

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "DroidGap.init()"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, La/a/b/e;->setId(I)V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p2}, La/a/b/e;->a(La/a/b/f;)V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p3}, La/a/b/e;->a(La/a/b/d;)V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {p2, v0}, La/a/b/f;->setWebView(La/a/b/e;)V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {p3, v0}, La/a/b/d;->a(La/a/b/e;)V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, La/a/b/e;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v0, "disallowOverscroll"

    invoke-virtual {p0, v0, v4}, La/a/b/h;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, La/a/b/e;->setOverScrollMode(I)V

    :cond_0
    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, La/a/b/e;->setVisibility(I)V

    iget-object v0, p0, La/a/b/h;->root:Landroid/widget/LinearLayout;

    iget-object v1, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, La/a/b/h;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, La/a/b/h;->setContentView(Landroid/view/View;)V

    iput-boolean v4, p0, La/a/b/h;->cancelLoadUrl:Z

    return-void
.end method

.method public isUrlWhiteListed(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, La/a/b/b;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-nez v0, :cond_0

    invoke-virtual {p0}, La/a/b/h;->init()V

    :cond_0
    const-string v0, "backgroundColor"

    const/high16 v1, -0x1000000

    invoke-virtual {p0, v0, v1}, La/a/b/h;->getIntegerProperty(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, La/a/b/h;->f:I

    iget-object v0, p0, La/a/b/h;->root:Landroid/widget/LinearLayout;

    iget v1, p0, La/a/b/h;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    const-string v0, "keepRunning"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, La/a/b/h;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, La/a/b/h;->keepRunning:Z

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0}, La/a/b/e;->canGoBack()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    const-string v0, "loadingDialog"

    invoke-virtual {p0, v0, v2}, La/a/b/h;->getStringProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    const-string v2, ""

    const-string v1, "Loading Application..."

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_5

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-lez v2, :cond_4

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v1, v0}, La/a/b/h;->spinnerStart(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p1}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    return-void

    :cond_3
    const-string v0, "loadingPageDialog"

    invoke-virtual {p0, v0, v2}, La/a/b/h;->getStringProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v1, ""

    goto :goto_1

    :cond_5
    move-object v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method public loadUrl(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-nez v0, :cond_0

    invoke-virtual {p0}, La/a/b/h;->init()V

    :cond_0
    iput p2, p0, La/a/b/h;->splashscreenTime:I

    const-string v0, "splashscreen"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, La/a/b/h;->getIntegerProperty(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, La/a/b/h;->splashscreen:I

    iget v0, p0, La/a/b/h;->splashscreenTime:I

    invoke-virtual {p0, v0}, La/a/b/h;->showSplashScreen(I)V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p1, p2}, La/a/b/e;->a(Ljava/lang/String;I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "Incoming Result"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Request code = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0}, La/a/b/e;->a()La/a/b/d;

    move-result-object v0

    invoke-virtual {v0}, La/a/b/d;->a()Landroid/webkit/ValueCallback;

    move-result-object v1

    const/16 v0, 0x1435

    if-ne p1, v0, :cond_3

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v2, "did we get here?"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    const/4 v0, -0x1

    if-eq p2, v0, :cond_5

    :cond_2
    const/4 v0, 0x0

    :goto_1
    sget-object v2, La/a/b/h;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, La/a/b/h;->activityResultCallback:La/a/b/a/b;

    if-nez v0, :cond_4

    iget-object v0, p0, La/a/b/h;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->a:La/a/b/a/e;

    iget-object v1, p0, La/a/b/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, La/a/b/a/e;->a(Ljava/lang/String;)La/a/b/a/b;

    move-result-object v0

    iput-object v0, p0, La/a/b/h;->activityResultCallback:La/a/b/a/b;

    iget-object v0, p0, La/a/b/h;->activityResultCallback:La/a/b/a/b;

    :cond_4
    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "We have a callback to send this result to"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, La/a/b/a/b;->f()V

    goto :goto_0

    :cond_5
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/16 v6, 0x800

    const/16 v5, 0x400

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-static {p0}, La/a/b/b;->a(Landroid/app/Activity;)V

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "DroidGap.onCreate()"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "callbackClass"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, La/a/b/h;->g:Ljava/lang/String;

    :cond_0
    const-string v0, "showTitle"

    invoke-virtual {p0, v0, v2}, La/a/b/h;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, La/a/b/h;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->requestFeature(I)Z

    :cond_1
    const-string v0, "setFullscreen"

    invoke-virtual {p0, v0, v2}, La/a/b/h;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, La/a/b/h;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Landroid/view/Window;->setFlags(II)V

    :goto_0
    invoke-virtual {p0}, La/a/b/h;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    new-instance v2, La/a/b/k;

    invoke-direct {v2, p0, v1, v0}, La/a/b/k;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, La/a/b/h;->root:Landroid/widget/LinearLayout;

    iget-object v0, p0, La/a/b/h;->root:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, La/a/b/h;->root:Landroid/widget/LinearLayout;

    iget v1, p0, La/a/b/h;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    iget-object v0, p0, La/a/b/h;->root:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, La/a/b/h;->setVolumeControlStream(I)V

    return-void

    :cond_2
    invoke-virtual {p0}, La/a/b/h;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6, v6}, Landroid/view/Window;->setFlags(II)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    const-string v0, "onCreateOptionsMenu"

    invoke-virtual {p0, v0, p1}, La/a/b/h;->postMessage(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 2

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    invoke-virtual {p0}, La/a/b/h;->removeSplashScreen()V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0}, La/a/b/e;->c()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, La/a/b/h;->endActivity()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0}, La/a/b/e;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p1, p2}, La/a/b/e;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0}, La/a/b/e;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v1}, La/a/b/e;->e()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/16 v0, 0x52

    if-ne p1, v0, :cond_2

    :cond_1
    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p1, p2}, La/a/b/e;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMessage(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onMessage("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "splashscreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "hide"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, La/a/b/h;->removeSplashScreen()V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    iget-object v0, p0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const-string v0, "splashscreen"

    invoke-virtual {p0, v0, v3}, La/a/b/h;->getIntegerProperty(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, La/a/b/h;->splashscreen:I

    iget v0, p0, La/a/b/h;->splashscreenTime:I

    invoke-virtual {p0, v0}, La/a/b/h;->showSplashScreen(I)V

    goto :goto_0

    :cond_3
    const-string v0, "spinner"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "stop"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, La/a/b/h;->spinnerStop()V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, v3}, La/a/b/e;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const-string v0, "onReceivedError"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    check-cast p2, Lorg/json/JSONObject;

    :try_start_0
    const-string v0, "errorCode"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "description"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "url"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, La/a/b/h;->onReceivedError(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :cond_5
    const-string v0, "exit"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, La/a/b/h;->endActivity()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p1}, La/a/b/e;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    const-string v0, "onOptionsItemSelected"

    invoke-virtual {p0, v0, p1}, La/a/b/h;->postMessage(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "Paused the application!"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, La/a/b/h;->e:I

    sget v1, La/a/b/h;->d:I

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-boolean v1, p0, La/a/b/h;->keepRunning:Z

    invoke-virtual {v0, v1}, La/a/b/e;->b(Z)V

    invoke-virtual {p0}, La/a/b/h;->removeSplashScreen()V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    const-string v0, "onPrepareOptionsMenu"

    invoke-virtual {p0, v0, p1}, La/a/b/h;->postMessage(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onReceivedError(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    const-string v0, "errorUrl"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, La/a/b/h;->getStringProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, La/a/b/b;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, La/a/b/h$2;

    invoke-direct {v1, p0, p0, v0}, La/a/b/h$2;-><init>(La/a/b/h;La/a/b/h;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, La/a/b/h;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x2

    if-ne p1, v0, :cond_2

    const/4 v2, 0x0

    :goto_1
    new-instance v0, La/a/b/h$3;

    move-object v1, p0

    move-object v3, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, La/a/b/h$3;-><init>(La/a/b/h;ZLa/a/b/h;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, La/a/b/h;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "Resuming the App"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, La/a/b/h;->e:I

    if-nez v0, :cond_1

    sget v0, La/a/b/h;->c:I

    iput v0, p0, La/a/b/h;->e:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-boolean v1, p0, La/a/b/h;->keepRunning:Z

    iget-boolean v2, p0, La/a/b/h;->activityResultKeepRunning:Z

    invoke-virtual {v0, v1}, La/a/b/e;->c(Z)V

    iget-boolean v0, p0, La/a/b/h;->keepRunning:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, La/a/b/h;->activityResultKeepRunning:Z

    if-eqz v0, :cond_0

    :cond_2
    iget-boolean v0, p0, La/a/b/h;->activityResultKeepRunning:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, La/a/b/h;->activityResultKeepRunning:Z

    iput-boolean v0, p0, La/a/b/h;->keepRunning:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/h;->activityResultKeepRunning:Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, La/a/b/h;->activityResultCallback:La/a/b/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->activityResultCallback:La/a/b/a/b;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "callbackClass"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public postMessage(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p1, p2}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public removeAuthenticationToken$4d90f81c(Ljava/lang/String;Ljava/lang/String;)Landroid/a/a/a/a$a;
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->b:La/a/b/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->b:La/a/b/f;

    invoke-virtual {v0, p1, p2}, La/a/b/f;->removeAuthenticationToken$4d90f81c(Ljava/lang/String;Ljava/lang/String;)Landroid/a/a/a/a$a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeSplashScreen()V
    .locals 1

    iget-object v0, p0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public sendJavascript(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->d:La/a/b/l;

    invoke-virtual {v0, p1}, La/a/b/l;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setActivityResultCallback(La/a/b/a/b;)V
    .locals 0

    iput-object p1, p0, La/a/b/h;->activityResultCallback:La/a/b/a/b;

    return-void
.end method

.method public setAuthenticationToken$351a0a40(Landroid/a/a/a/a$a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->b:La/a/b/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    iget-object v0, v0, La/a/b/e;->b:La/a/b/f;

    invoke-virtual {v0, p1, p2, p3}, La/a/b/f;->setAuthenticationToken$351a0a40(Landroid/a/a/a/a$a;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setBooleanProperty(Ljava/lang/String;Z)V
    .locals 2

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "Setting boolean properties in DroidGap will be deprecated in 3.0 on July 2013, please use config.xml"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, La/a/b/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-void
.end method

.method public setDoubleProperty(Ljava/lang/String;D)V
    .locals 2

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "Setting double properties in DroidGap will be deprecated in 3.0 on July 2013, please use config.xml"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, La/a/b/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    return-void
.end method

.method public setIntegerProperty(Ljava/lang/String;I)V
    .locals 2

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "Setting integer properties in DroidGap will be deprecated in 3.1 on August 2013, please use config.xml"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, La/a/b/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-void
.end method

.method public setStringProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    sget-object v0, La/a/b/h;->TAG:Ljava/lang/String;

    const-string v1, "Setting string properties in DroidGap will be deprecated in 3.0 on July 2013, please use config.xml"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, La/a/b/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method

.method protected showSplashScreen(I)V
    .locals 1

    new-instance v0, La/a/b/h$5;

    invoke-direct {v0, p0, p0, p1}, La/a/b/h$5;-><init>(La/a/b/h;La/a/b/h;I)V

    invoke-virtual {p0, v0}, La/a/b/h;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public showWebPage(Ljava/lang/String;ZZLjava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->appView:La/a/b/e;

    invoke-virtual {v0, p1, p2, p3}, La/a/b/e;->a(Ljava/lang/String;ZZ)V

    :cond_0
    return-void
.end method

.method public spinnerStart(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x1

    iget-object v0, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;

    :cond_0
    new-instance v5, La/a/b/h$1;

    invoke-direct {v5, p0, p0}, La/a/b/h$1;-><init>(La/a/b/h;La/a/b/h;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method public spinnerStop()V
    .locals 1

    :try_start_0
    iget-object v0, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, La/a/b/h;->spinnerDialog:Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public startActivityForResult(La/a/b/a/b;Landroid/content/Intent;I)V
    .locals 1

    iput-object p1, p0, La/a/b/h;->activityResultCallback:La/a/b/a/b;

    iget-boolean v0, p0, La/a/b/h;->keepRunning:Z

    iput-boolean v0, p0, La/a/b/h;->activityResultKeepRunning:Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/h;->keepRunning:Z

    :cond_0
    invoke-super {p0, p2, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
