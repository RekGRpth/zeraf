.class final La/a/b/h$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = La/a/b/h;->onReceivedError(ILjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Z

.field private final synthetic b:La/a/b/h;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(La/a/b/h;ZLa/a/b/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-boolean p2, p0, La/a/b/h$3;->a:Z

    iput-object p3, p0, La/a/b/h$3;->b:La/a/b/h;

    iput-object p4, p0, La/a/b/h$3;->c:Ljava/lang/String;

    iput-object p5, p0, La/a/b/h$3;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-boolean v0, p0, La/a/b/h$3;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/h$3;->b:La/a/b/h;

    iget-object v0, v0, La/a/b/h;->appView:La/a/b/e;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, La/a/b/e;->setVisibility(I)V

    iget-object v0, p0, La/a/b/h$3;->b:La/a/b/h;

    const-string v1, "Application Error"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, La/a/b/h$3;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, La/a/b/h$3;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OK"

    iget-boolean v4, p0, La/a/b/h$3;->a:Z

    invoke-virtual {v0, v1, v2, v3, v4}, La/a/b/h;->displayError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method
