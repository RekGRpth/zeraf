.class public final La/a/b/e;
.super Landroid/webkit/WebView;
.source "SourceFile"


# static fields
.field private static o:Landroid/widget/FrameLayout$LayoutParams;


# instance fields
.field public a:La/a/b/a/e;

.field b:La/a/b/f;

.field c:I

.field d:La/a/b/l;

.field e:La/a/b/i;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/content/BroadcastReceiver;

.field private i:La/a/b/a/a;

.field private j:La/a/b/d;

.field private k:Z

.field private l:J

.field private m:Landroid/view/View;

.field private n:Landroid/webkit/WebChromeClient$CustomViewCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    sput-object v0, La/a/b/e;->o:Landroid/widget/FrameLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    const/16 v5, 0x400

    const/16 v8, 0xb

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, La/a/b/e;->f:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, La/a/b/e;->g:Ljava/util/ArrayList;

    iput v1, p0, La/a/b/e;->c:I

    const-wide/16 v2, 0x0

    iput-wide v2, p0, La/a/b/e;->l:J

    const-class v2, La/a/b/a/a;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    check-cast p1, La/a/b/a/a;

    iput-object p1, p0, La/a/b/e;->i:La/a/b/a/a;

    :goto_0
    const-string v2, "true"

    const-string v3, "fullscreen"

    const-string v4, "false"

    invoke-direct {p0, v3, v4}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v2}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x800

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    iget-object v2, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v2}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Landroid/view/Window;->setFlags(II)V

    :cond_0
    invoke-virtual {p0, v1}, La/a/b/e;->setInitialScale(I)V

    invoke-virtual {p0, v1}, La/a/b/e;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {p0}, La/a/b/e;->requestFocusFromTouch()Z

    invoke-virtual {p0}, La/a/b/e;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    sget-object v3, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    :try_start_0
    const-class v3, Landroid/webkit/WebSettings;

    const-string v4, "setNavDump"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v4, v8, :cond_1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    :cond_1
    :goto_1
    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xf

    if-le v3, v4, :cond_2

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    :cond_2
    iget-object v3, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v3}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "database"

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v4, v8, :cond_3

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setDatabasePath(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setGeolocationDatabasePath(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    const-wide/32 v3, 0x500000

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    iget-object v3, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v3}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "database"

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    invoke-direct {p0}, La/a/b/e;->f()V

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, La/a/b/e;->h:Landroid/content/BroadcastReceiver;

    if-nez v3, :cond_4

    new-instance v3, La/a/b/e$1;

    invoke-direct {v3, p0}, La/a/b/e$1;-><init>(La/a/b/e;)V

    iput-object v3, p0, La/a/b/e;->h:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v3}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, La/a/b/e;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_4
    new-instance v2, La/a/b/a/e;

    iget-object v3, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-direct {v2, p0, v3}, La/a/b/a/e;-><init>(La/a/b/e;La/a/b/a/a;)V

    iput-object v2, p0, La/a/b/e;->a:La/a/b/a/e;

    new-instance v2, La/a/b/l;

    iget-object v3, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-direct {v2, p0, v3}, La/a/b/l;-><init>(La/a/b/e;La/a/b/a/a;)V

    iput-object v2, p0, La/a/b/e;->d:La/a/b/l;

    new-instance v2, La/a/b/i;

    iget-object v3, p0, La/a/b/e;->a:La/a/b/a/e;

    iget-object v4, p0, La/a/b/e;->d:La/a/b/l;

    invoke-direct {v2, v3, v4}, La/a/b/i;-><init>(La/a/b/a/e;La/a/b/l;)V

    iput-object v2, p0, La/a/b/e;->e:La/a/b/i;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v8, :cond_7

    const/16 v3, 0xd

    if-gt v2, v3, :cond_7

    :goto_2
    if-nez v0, :cond_5

    const/16 v0, 0x9

    if-ge v2, v0, :cond_8

    :cond_5
    const-string v0, "CordovaWebView"

    const-string v1, "Disabled addJavascriptInterface() bridge since Android version is old."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    return-void

    :cond_6
    const-string v2, "CordovaWebView"

    const-string v3, "Your activity must implement CordovaInterface to work"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v3

    const-string v3, "CordovaWebView"

    const-string v4, "We are on a modern version of Android, we will deprecate HTC 2.3 devices in 2.8"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_1
    move-exception v3

    const-string v3, "CordovaWebView"

    const-string v4, "Doing the NavDump failed with bad arguments"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_2
    move-exception v3

    const-string v3, "CordovaWebView"

    const-string v4, "This should never happen: IllegalAccessException means this isn\'t Android anymore"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_3
    move-exception v3

    const-string v3, "CordovaWebView"

    const-string v4, "This should never happen: InvocationTargetException means this isn\'t Android anymore."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    if-ge v2, v8, :cond_9

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "CordovaWebView"

    const-string v1, "Disabled addJavascriptInterface() bridge callback due to a bug on the 2.3 emulator"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_9
    iget-object v0, p0, La/a/b/e;->e:La/a/b/i;

    const-string v1, "_cordovaNative"

    invoke-virtual {p0, v0, v1}, La/a/b/e;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v0}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method static synthetic a(La/a/b/e;)V
    .locals 0

    invoke-direct {p0}, La/a/b/e;->f()V

    return-void
.end method

.method static synthetic b(La/a/b/e;)La/a/b/a/a;
    .locals 1

    iget-object v0, p0, La/a/b/e;->i:La/a/b/a/a;

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 6

    const-string v0, "CordovaWebView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ">>> loadUrl("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    invoke-virtual {v0}, La/a/b/a/e;->a()V

    iget v4, p0, La/a/b/e;->c:I

    const-string v0, "loadUrlTimeoutValue"

    const-string v1, "20000"

    invoke-direct {p0, v0, v1}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    new-instance v5, La/a/b/e$2;

    invoke-direct {v5, p0, p0, p1}, La/a/b/e$2;-><init>(La/a/b/e;La/a/b/e;Ljava/lang/String;)V

    new-instance v0, La/a/b/e$3;

    move-object v1, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, La/a/b/e$3;-><init>(La/a/b/e;ILa/a/b/e;ILjava/lang/Runnable;)V

    iget-object v1, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v1}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, La/a/b/e$4;

    invoke-direct {v2, p0, v0, p0, p1}, La/a/b/e$4;-><init>(La/a/b/e;Ljava/lang/Runnable;La/a/b/e;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private f()V
    .locals 1

    invoke-virtual {p0}, La/a/b/e;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()La/a/b/d;
    .locals 1

    iget-object v0, p0, La/a/b/e;->j:La/a/b/d;

    return-object v0
.end method

.method public final a(La/a/b/a/f;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, La/a/b/e;->d:La/a/b/l;

    invoke-virtual {v0, p1, p2}, La/a/b/l;->a(La/a/b/a/f;Ljava/lang/String;)V

    return-void
.end method

.method public final a(La/a/b/d;)V
    .locals 0

    iput-object p1, p0, La/a/b/e;->j:La/a/b/d;

    invoke-super {p0, p1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    return-void
.end method

.method public final a(La/a/b/f;)V
    .locals 0

    iput-object p1, p0, La/a/b/e;->b:La/a/b/f;

    invoke-super {p0, p1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    invoke-virtual {v0, p1}, La/a/b/a/e;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 2

    const-string v0, "CordovaWebView"

    const-string v1, "showing Custom View"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, La/a/b/e;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-interface {p2}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, La/a/b/e;->m:Landroid/view/View;

    iput-object p2, p0, La/a/b/e;->n:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-virtual {p0}, La/a/b/e;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget-object v1, La/a/b/e;->o:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, La/a/b/e;->setVisibility(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->bringToFront()V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x3

    invoke-static {v0}, La/a/b/a/c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "javascript:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CordovaWebView"

    const-string v1, ">>> loadUrlNow()"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "file://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "javascript:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, La/a/b/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-super {p0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 5

    const-string v0, "url"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "javascript:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, La/a/b/e;->canGoBack()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CordovaWebView"

    const-string v1, "DroidGap.loadUrl(%s, %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "splashscreen"

    const-string v1, "show"

    invoke-virtual {p0, v0, v1}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    invoke-direct {p0, p1}, La/a/b/e;->d(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, La/a/b/e;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    invoke-virtual {v0, p1, p2}, La/a/b/a/e;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;ZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    const-string v0, "CordovaWebView"

    const-string v1, "showWebPage(%s, %b, %b, HashMap"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p3, :cond_0

    invoke-virtual {p0}, La/a/b/e;->clearHistory()V

    :cond_0
    if-nez p2, :cond_3

    const-string v0, "file://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, La/a/b/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0, p1}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    const-string v0, "CordovaWebView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "showWebPage: Cannot load URL into webview since it is not in white list.  Loading into browser instead. (URL="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, La/a/b/a/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v1}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "CordovaWebView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading url "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, La/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v1}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "CordovaWebView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading url "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, La/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, La/a/b/e;->k:Z

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, La/a/b/e;->d:La/a/b/l;

    invoke-virtual {v0, p1}, La/a/b/l;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Z)V
    .locals 2

    const-string v0, "CordovaWebView"

    const-string v1, "Handle the pause"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "javascript:try{cordova.fireDocumentEvent(\'pause\');}catch(e){console.log(\'exception firing pause event from native\');};"

    invoke-virtual {p0, v0}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    invoke-virtual {v0, p1}, La/a/b/a/e;->a(Z)V

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, La/a/b/e;->pauseTimers()V

    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 7

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, La/a/b/e;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebBackForwardList;->getSize()I

    move-result v2

    :goto_0
    if-lt v0, v2, :cond_1

    invoke-super {p0}, Landroid/webkit/WebView;->goBack()V

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {v1, v0}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CordovaWebView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "The URL at index: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    const-string v0, "javascript:try{cordova.require(\'cordova/channel\').onDestroy.fire();}catch(e){console.log(\'exception firing destroy event from native\');};"

    invoke-direct {p0, v0}, La/a/b/e;->d(Ljava/lang/String;)V

    const-string v0, "about:blank"

    invoke-virtual {p0, v0}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    invoke-virtual {v0}, La/a/b/a/e;->b()V

    :cond_0
    iget-object v0, p0, La/a/b/e;->h:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v0}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, La/a/b/e;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "CordovaWebView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error unregistering configuration receiver: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    const-string v0, "volumeup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, La/a/b/e;->f:Ljava/util/ArrayList;

    const/16 v1, 0x18

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "volumedown"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/b/e;->f:Ljava/util/ArrayList;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    const-string v0, "javascript:try{cordova.fireDocumentEvent(\'resume\');}catch(e){console.log(\'exception firing resume event from native\');};"

    invoke-virtual {p0, v0}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/e;->a:La/a/b/a/e;

    invoke-virtual {v0, p1}, La/a/b/a/e;->b(Z)V

    :cond_0
    invoke-virtual {p0}, La/a/b/e;->resumeTimers()V

    return-void
.end method

.method public final d()V
    .locals 2

    const-string v0, "CordovaWebView"

    const-string v1, "Hidding Custom View"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, La/a/b/e;->m:Landroid/view/View;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, La/a/b/e;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, La/a/b/e;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, La/a/b/e;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, La/a/b/e;->m:Landroid/view/View;

    iget-object v0, p0, La/a/b/e;->n:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, La/a/b/e;->setVisibility(I)V

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, La/a/b/e;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final loadUrl(Ljava/lang/String;)V
    .locals 2

    const-string v0, "about:blank"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "javascript:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, La/a/b/e;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "url"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, La/a/b/e;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, La/a/b/e;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, La/a/b/e;->f:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x19

    if-ne p1, v0, :cond_0

    const-string v0, "CordovaWebView"

    const-string v2, "Down Key Hit"

    invoke-static {v0, v2}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "javascript:cordova.fireDocumentEvent(\'volumedownbutton\');"

    invoke-virtual {p0, v0}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    const-string v0, "CordovaWebView"

    const-string v2, "Up Key Hit"

    invoke-static {v0, v2}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "javascript:cordova.fireDocumentEvent(\'volumeupbutton\');"

    invoke-virtual {p0, v0}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_5

    invoke-virtual {p0}, La/a/b/e;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, La/a/b/e;->getUrl()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CordovaWebView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "The current URL is: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "CordovaWebView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "The URL at item 0 is:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_4

    iget-boolean v0, p0, La/a/b/e;->k:Z

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    const/16 v0, 0x52

    if-ne p1, v0, :cond_7

    invoke-virtual {p0}, La/a/b/e;->getFocusedChild()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v0, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v0}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v4, "input_method"

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    iget-object v0, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v0}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->openOptionsMenu()V

    :cond_6
    move v0, v1

    goto/16 :goto_0

    :cond_7
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    iget-object v1, p0, La/a/b/e;->m:Landroid/view/View;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, La/a/b/e;->d()V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_1
    return v0

    :cond_2
    iget-boolean v1, p0, La/a/b/e;->k:Z

    if-eqz v1, :cond_3

    const-string v1, "javascript:cordova.fireDocumentEvent(\'backbutton\');"

    invoke-virtual {p0, v1}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, La/a/b/e;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, La/a/b/e;->i:La/a/b/a/a;

    invoke-interface {v0}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_4
    const/16 v1, 0x52

    if-ne p1, v1, :cond_6

    iget-wide v0, p0, La/a/b/e;->l:J

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_5

    const-string v0, "javascript:cordova.fireDocumentEvent(\'menubutton\');"

    invoke-virtual {p0, v0}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, La/a/b/e;->l:J

    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    :cond_6
    const/16 v1, 0x54

    if-ne p1, v1, :cond_7

    const-string v1, "javascript:cordova.fireDocumentEvent(\'searchbutton\');"

    invoke-virtual {p0, v1}, La/a/b/e;->loadUrl(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, La/a/b/e;->g:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public final restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .locals 3

    invoke-super {p0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    move-result-object v0

    const-string v1, "CordovaWebView"

    const-string v2, "WebView restoration crew now restoring!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, La/a/b/e;->a:La/a/b/a/e;

    invoke-virtual {v1}, La/a/b/a/e;->a()V

    return-object v0
.end method
