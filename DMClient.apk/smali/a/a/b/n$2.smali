.class final La/a/b/n$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = La/a/b/n;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;La/b/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:La/a/b/a/a;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Lorg/json/JSONArray;

.field private final synthetic e:La/b/a;


# direct methods
.method constructor <init>(La/a/b/n;La/a/b/a/a;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;La/b/a;)V
    .locals 0

    iput-object p2, p0, La/a/b/n$2;->a:La/a/b/a/a;

    iput-object p3, p0, La/a/b/n$2;->b:Ljava/lang/String;

    iput-object p4, p0, La/a/b/n$2;->c:Ljava/lang/String;

    iput-object p5, p0, La/a/b/n$2;->d:Lorg/json/JSONArray;

    iput-object p6, p0, La/a/b/n$2;->e:La/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, La/a/b/n$2;->a:La/a/b/a/a;

    invoke-interface {v1}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, La/a/b/n$2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, La/a/b/n$2;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, La/a/b/n$2;->d:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_0

    :try_start_0
    iget-object v1, p0, La/a/b/n$2;->d:Lorg/json/JSONArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, La/a/b/n$2$1;

    iget-object v3, p0, La/a/b/n$2;->e:La/b/a;

    invoke-direct {v2, p0, v3}, La/a/b/n$2$1;-><init>(La/a/b/n$2;La/b/a;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_0
    iget-object v1, p0, La/a/b/n$2;->d:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-le v1, v4, :cond_1

    :try_start_1
    iget-object v1, p0, La/a/b/n$2;->d:Lorg/json/JSONArray;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, La/a/b/n$2$2;

    iget-object v3, p0, La/a/b/n$2;->e:La/b/a;

    invoke-direct {v2, p0, v3}, La/a/b/n$2$2;-><init>(La/a/b/n$2;La/b/a;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    iget-object v1, p0, La/a/b/n$2;->d:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-le v1, v5, :cond_2

    :try_start_2
    iget-object v1, p0, La/a/b/n$2;->d:Lorg/json/JSONArray;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, La/a/b/n$2$3;

    iget-object v3, p0, La/a/b/n$2;->e:La/b/a;

    invoke-direct {v2, p0, v3}, La/a/b/n$2$3;-><init>(La/a/b/n$2;La/b/a;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_2
    new-instance v1, La/a/b/n$2$4;

    iget-object v2, p0, La/a/b/n$2;->e:La/b/a;

    invoke-direct {v1, p0, v2}, La/a/b/n$2$4;-><init>(La/a/b/n$2;La/b/a;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_0
.end method
