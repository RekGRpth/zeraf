.class public La/a/b/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static synthetic a:Z


# instance fields
.field public c:La/a/b/e;

.field public d:La/a/b/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, La/a/b/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, La/a/b/a/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()V
    .locals 0

    return-void
.end method

.method public static c()V
    .locals 0

    return-void
.end method

.method public static d()V
    .locals 0

    return-void
.end method

.method public static e()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public static f()V
    .locals 0

    return-void
.end method

.method public static g()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static h()Landroid/webkit/WebResourceResponse;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public static i()V
    .locals 0

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(La/a/b/a/a;La/a/b/e;)V
    .locals 1

    sget-boolean v0, La/a/b/a/b;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/b/a/b;->d:La/a/b/a/a;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, La/a/b/a/b;->d:La/a/b/a/a;

    iput-object p2, p0, La/a/b/a/b;->c:La/a/b/e;

    return-void
.end method

.method public a(Ljava/lang/String;La/a/b/c;La/b/a;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;La/b/a;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0, p3}, La/a/b/a/b;->a(Ljava/lang/String;Lorg/json/JSONArray;La/b/a;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONArray;La/b/a;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, La/a/b/c;

    invoke-direct {v0, p2}, La/a/b/c;-><init>(Lorg/json/JSONArray;)V

    invoke-virtual {p0, p1, v0, p3}, La/a/b/a/b;->a(Ljava/lang/String;La/a/b/c;La/b/a;)Z

    move-result v0

    return v0
.end method
