.class public final La/a/b/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "La/a/b/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:La/a/b/a/a;

.field private final d:La/a/b/e;

.field private e:Z

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "PluginManager"

    sput-object v0, La/a/b/a/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(La/a/b/e;La/a/b/a/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, La/a/b/a/e;->f:Ljava/util/HashMap;

    iput-object p2, p0, La/a/b/a/e;->c:La/a/b/a/a;

    iput-object p1, p0, La/a/b/a/e;->d:La/a/b/e;

    const/4 v0, 0x1

    iput-boolean v0, p0, La/a/b/a/e;->e:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)La/a/b/a/b;
    .locals 3

    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, La/a/b/a/d;->b:La/a/b/a/b;

    if-nez v1, :cond_1

    iget-object v1, p0, La/a/b/a/e;->d:La/a/b/e;

    iget-object v2, p0, La/a/b/a/e;->c:La/a/b/a/a;

    invoke-virtual {v0, v1, v2}, La/a/b/a/d;->a(La/a/b/e;La/a/b/a/a;)La/a/b/a/b;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, La/a/b/a/e;->c:La/a/b/a/a;

    invoke-interface {v0, p1, p2}, La/a/b/a/a;->onMessage(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    iget-object v2, v0, La/a/b/a/d;->b:La/a/b/a/b;

    if-eqz v2, :cond_1

    iget-object v0, v0, La/a/b/a/d;->b:La/a/b/a/b;

    invoke-static {}, La/a/b/a/b;->e()Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a()V
    .locals 11

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v9, 0x0

    sget-object v0, La/a/b/a/e;->a:Ljava/lang/String;

    const-string v1, "init()"

    invoke-static {v0, v1}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, La/a/b/a/e;->e:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, La/a/b/a/e;->c:La/a/b/a/a;

    invoke-interface {v0}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "config"

    const-string v2, "xml"

    iget-object v4, p0, La/a/b/a/e;->c:La/a/b/a/a;

    invoke-interface {v4}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/b/a/e;->c:La/a/b/a/a;

    invoke-interface {v0}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "plugins"

    const-string v2, "xml"

    iget-object v4, p0, La/a/b/a/e;->c:La/a/b/a/a;

    invoke-interface {v4}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    sget-object v1, La/a/b/a/e;->a:Ljava/lang/String;

    const-string v2, "Using plugins.xml instead of config.xml.  plugins.xml will eventually be deprecated"

    invoke-static {v1, v2}, La/a/b/a/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, La/a/b/a/e;->c:La/a/b/a/a;

    invoke-interface {v1}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v6

    const/4 v2, -0x1

    const-string v1, ""

    const-string v0, ""

    move v4, v2

    move v2, v3

    :goto_0
    if-ne v4, v5, :cond_4

    :cond_1
    iput-boolean v3, p0, La/a/b/a/e;->e:Z

    :cond_2
    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    return-void

    :cond_4
    const/4 v7, 0x2

    if-ne v4, v7, :cond_a

    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "plugin"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    const-string v0, "name"

    invoke-interface {v6, v9, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "value"

    invoke-interface {v6, v9, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v7, "true"

    const-string v8, "onload"

    invoke-interface {v6, v9, v8}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    new-instance v8, La/a/b/a/d;

    invoke-direct {v8, v1, v0, v7}, La/a/b/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0, v8}, La/a/b/a/e;->a(La/a/b/a/d;)V

    move v10, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v10

    :goto_2
    :try_start_0
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    move v10, v0

    move-object v0, v1

    move-object v1, v2

    move v2, v10

    goto :goto_0

    :cond_5
    const-string v8, "url-filter"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v7, p0, La/a/b/a/e;->f:Ljava/util/HashMap;

    const-string v8, "value"

    invoke-interface {v6, v9, v8}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v10, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v10

    goto :goto_2

    :cond_6
    const-string v8, "feature"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    const-string v2, "name"

    invoke-interface {v6, v9, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-object v2, v1

    move-object v1, v0

    move v0, v5

    goto :goto_2

    :cond_7
    const-string v8, "param"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    if-eqz v2, :cond_d

    const-string v7, "name"

    invoke-interface {v6, v9, v7}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "service"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    const-string v1, "value"

    invoke-interface {v6, v9, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_8
    :goto_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_d

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_d

    const-string v7, "true"

    const-string v8, "onload"

    invoke-interface {v6, v9, v8}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    new-instance v8, La/a/b/a/d;

    invoke-direct {v8, v1, v0, v7}, La/a/b/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0, v8}, La/a/b/a/e;->a(La/a/b/a/d;)V

    const-string v1, ""

    const-string v0, ""

    move v10, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v10

    goto :goto_2

    :cond_9
    const-string v8, "package"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v0, "value"

    invoke-interface {v6, v9, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_a
    const/4 v7, 0x3

    if-ne v4, v7, :cond_d

    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "feature"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    const-string v1, ""

    const-string v0, ""

    move-object v2, v1

    move-object v1, v0

    move v0, v3

    goto/16 :goto_2

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    move v10, v0

    move-object v0, v1

    move-object v1, v2

    move v2, v10

    goto/16 :goto_0

    :catch_1
    move-exception v7

    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    move v10, v0

    move-object v0, v1

    move-object v1, v2

    move v2, v10

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0, v3}, La/a/b/a/e;->a(Z)V

    invoke-virtual {p0}, La/a/b/a/e;->b()V

    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    iput-object v9, v0, La/a/b/a/d;->b:La/a/b/a/b;

    goto :goto_4

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    iget-boolean v2, v0, La/a/b/a/d;->c:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, La/a/b/a/e;->d:La/a/b/e;

    iget-object v3, p0, La/a/b/a/e;->c:La/a/b/a/a;

    invoke-virtual {v0, v2, v3}, La/a/b/a/d;->a(La/a/b/e;La/a/b/a/a;)La/a/b/a/b;

    goto/16 :goto_1

    :cond_d
    move v10, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v10

    goto/16 :goto_2
.end method

.method public final a(La/a/b/a/d;)V
    .locals 2

    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    iget-object v1, p1, La/a/b/a/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 3

    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    iget-object v2, v0, La/a/b/a/d;->b:La/a/b/a/b;

    if-eqz v2, :cond_0

    iget-object v0, v0, La/a/b/a/d;->b:La/a/b/a/b;

    invoke-static {}, La/a/b/a/b;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, La/a/b/a/d;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, La/a/b/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, La/a/b/a/e;->a(La/a/b/a/d;)V

    return-void
.end method

.method public final a(Z)V
    .locals 3

    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    iget-object v2, v0, La/a/b/a/d;->b:La/a/b/a/b;

    if-eqz v2, :cond_0

    iget-object v0, v0, La/a/b/a/d;->b:La/a/b/a/b;

    invoke-static {}, La/a/b/a/b;->b()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0, p1}, La/a/b/a/e;->a(Ljava/lang/String;)La/a/b/a/b;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, La/a/b/a/e;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "exec() call to unknown plugin: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, La/a/b/a/f;

    sget-object v2, La/a/b/a/f$a;->c:La/a/b/a/f$a;

    invoke-direct {v1, v2}, La/a/b/a/f;-><init>(La/a/b/a/f$a;)V

    iget-object v2, p0, La/a/b/a/e;->d:La/a/b/e;

    invoke-virtual {v2, v1, p3}, La/a/b/e;->a(La/a/b/a/f;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    :try_start_0
    new-instance v2, La/b/a;

    iget-object v3, p0, La/a/b/a/e;->d:La/a/b/e;

    invoke-direct {v2, p3, v3}, La/b/a;-><init>(Ljava/lang/String;La/a/b/e;)V

    invoke-virtual {v1, p2, p4, v2}, La/a/b/a/b;->a(Ljava/lang/String;Ljava/lang/String;La/b/a;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, La/a/b/a/f;

    sget-object v2, La/a/b/a/f$a;->d:La/a/b/a/f$a;

    invoke-direct {v1, v2}, La/a/b/a/f;-><init>(La/a/b/a/f$a;)V

    iget-object v2, p0, La/a/b/a/e;->d:La/a/b/e;

    invoke-virtual {v2, v1, p3}, La/a/b/e;->a(La/a/b/a/f;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v1, La/a/b/a/f;

    sget-object v2, La/a/b/a/f$a;->e:La/a/b/a/f$a;

    invoke-direct {v1, v2}, La/a/b/a/f;-><init>(La/a/b/a/f$a;)V

    iget-object v2, p0, La/a/b/a/e;->d:La/a/b/e;

    invoke-virtual {v2, v1, p3}, La/a/b/e;->a(La/a/b/a/f;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {v2}, La/b/a;->a()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    iget-object v2, v0, La/a/b/a/d;->b:La/a/b/a/b;

    if-eqz v2, :cond_0

    iget-object v0, v0, La/a/b/a/d;->b:La/a/b/a/b;

    invoke-virtual {v0}, La/a/b/a/b;->a()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 3

    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    iget-object v2, v0, La/a/b/a/d;->b:La/a/b/a/b;

    if-eqz v2, :cond_0

    iget-object v0, v0, La/a/b/a/d;->b:La/a/b/a/b;

    invoke-static {}, La/a/b/a/b;->c()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 3

    iget-object v0, p0, La/a/b/a/e;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, La/a/b/a/e;->a(Ljava/lang/String;)La/a/b/a/b;

    invoke-static {}, La/a/b/a/b;->g()Z

    move-result v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 3

    iget-object v0, p0, La/a/b/a/e;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, La/a/b/a/e;->a(Ljava/lang/String;)La/a/b/a/b;

    invoke-static {}, La/a/b/a/b;->h()Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, La/a/b/a/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/d;

    iget-object v0, v0, La/a/b/a/d;->b:La/a/b/a/b;

    if-eqz v0, :cond_0

    invoke-static {}, La/a/b/a/b;->i()V

    goto :goto_0
.end method
