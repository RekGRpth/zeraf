.class public final enum La/a/b/a/f$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/a/b/a/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "La/a/b/a/f$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:La/a/b/a/f$a;

.field public static final enum b:La/a/b/a/f$a;

.field public static final enum c:La/a/b/a/f$a;

.field public static final enum d:La/a/b/a/f$a;

.field public static final enum e:La/a/b/a/f$a;

.field public static final enum f:La/a/b/a/f$a;

.field private static enum g:La/a/b/a/f$a;

.field private static enum h:La/a/b/a/f$a;

.field private static enum i:La/a/b/a/f$a;

.field private static enum j:La/a/b/a/f$a;

.field private static final synthetic k:[La/a/b/a/f$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "NO_RESULT"

    invoke-direct {v0, v1, v3}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->a:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "OK"

    invoke-direct {v0, v1, v4}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->b:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "CLASS_NOT_FOUND_EXCEPTION"

    invoke-direct {v0, v1, v5}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->c:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "ILLEGAL_ACCESS_EXCEPTION"

    invoke-direct {v0, v1, v6}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->g:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "INSTANTIATION_EXCEPTION"

    invoke-direct {v0, v1, v7}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->h:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "MALFORMED_URL_EXCEPTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->i:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "IO_EXCEPTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->j:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "INVALID_ACTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->d:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "JSON_EXCEPTION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->e:La/a/b/a/f$a;

    new-instance v0, La/a/b/a/f$a;

    const-string v1, "ERROR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, La/a/b/a/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/a/f$a;->f:La/a/b/a/f$a;

    const/16 v0, 0xa

    new-array v0, v0, [La/a/b/a/f$a;

    sget-object v1, La/a/b/a/f$a;->a:La/a/b/a/f$a;

    aput-object v1, v0, v3

    sget-object v1, La/a/b/a/f$a;->b:La/a/b/a/f$a;

    aput-object v1, v0, v4

    sget-object v1, La/a/b/a/f$a;->c:La/a/b/a/f$a;

    aput-object v1, v0, v5

    sget-object v1, La/a/b/a/f$a;->g:La/a/b/a/f$a;

    aput-object v1, v0, v6

    sget-object v1, La/a/b/a/f$a;->h:La/a/b/a/f$a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, La/a/b/a/f$a;->i:La/a/b/a/f$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, La/a/b/a/f$a;->j:La/a/b/a/f$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, La/a/b/a/f$a;->d:La/a/b/a/f$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, La/a/b/a/f$a;->e:La/a/b/a/f$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, La/a/b/a/f$a;->f:La/a/b/a/f$a;

    aput-object v2, v0, v1

    sput-object v0, La/a/b/a/f$a;->k:[La/a/b/a/f$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)La/a/b/a/f$a;
    .locals 1

    const-class v0, La/a/b/a/f$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, La/a/b/a/f$a;

    return-object v0
.end method

.method public static values()[La/a/b/a/f$a;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, La/a/b/a/f$a;->k:[La/a/b/a/f$a;

    array-length v1, v0

    new-array v2, v1, [La/a/b/a/f$a;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
