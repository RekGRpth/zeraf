.class final La/a/b/h$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = La/a/b/h;->showSplashScreen(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:La/a/b/h;

.field private final synthetic b:La/a/b/h;

.field private final synthetic c:I


# direct methods
.method constructor <init>(La/a/b/h;La/a/b/h;I)V
    .locals 0

    iput-object p1, p0, La/a/b/h$5;->a:La/a/b/h;

    iput-object p2, p0, La/a/b/h$5;->b:La/a/b/h;

    iput p3, p0, La/a/b/h$5;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const/4 v4, -0x1

    const/16 v5, 0x400

    iget-object v0, p0, La/a/b/h$5;->a:La/a/b/h;

    invoke-virtual {v0}, La/a/b/h;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, La/a/b/h$5;->b:La/a/b/h;

    invoke-virtual {v2}, La/a/b/h;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setMinimumWidth(I)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, La/a/b/h$5;->b:La/a/b/h;

    const-string v2, "backgroundColor"

    const/high16 v3, -0x1000000

    invoke-virtual {v0, v2, v3}, La/a/b/h;->getIntegerProperty(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    invoke-direct {v0, v4, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, La/a/b/h$5;->b:La/a/b/h;

    iget v0, v0, La/a/b/h;->splashscreen:I

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, La/a/b/h$5;->a:La/a/b/h;

    new-instance v2, Landroid/app/Dialog;

    iget-object v3, p0, La/a/b/h$5;->b:La/a/b/h;

    const v4, 0x1030010

    invoke-direct {v2, v3, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, v0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    iget-object v0, p0, La/a/b/h$5;->a:La/a/b/h;

    invoke-virtual {v0}, La/a/b/h;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-ne v0, v5, :cond_0

    iget-object v0, p0, La/a/b/h$5;->a:La/a/b/h;

    iget-object v0, v0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Landroid/view/Window;->setFlags(II)V

    :cond_0
    iget-object v0, p0, La/a/b/h$5;->a:La/a/b/h;

    iget-object v0, v0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, La/a/b/h$5;->a:La/a/b/h;

    iget-object v0, v0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, La/a/b/h$5;->a:La/a/b/h;

    iget-object v0, v0, La/a/b/h;->splashDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, La/a/b/h$5$1;

    invoke-direct {v1, p0}, La/a/b/h$5$1;-><init>(La/a/b/h$5;)V

    iget v2, p0, La/a/b/h$5;->c:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
