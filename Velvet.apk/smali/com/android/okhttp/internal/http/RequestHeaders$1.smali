.class Lcom/android/okhttp/internal/http/RequestHeaders$1;
.super Ljava/lang/Object;
.source "RequestHeaders.java"

# interfaces
.implements Lcom/android/okhttp/internal/http/HeaderParser$CacheControlHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/okhttp/internal/http/RequestHeaders;-><init>(Ljava/net/URI;Lcom/android/okhttp/internal/http/RawHeaders;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/okhttp/internal/http/RequestHeaders;


# direct methods
.method constructor <init>(Lcom/android/okhttp/internal/http/RequestHeaders;)V
    .locals 0

    iput-object p1, p0, Lcom/android/okhttp/internal/http/RequestHeaders$1;->this$0:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handle(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    const-string v0, "no-cache"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/RequestHeaders$1;->this$0:Lcom/android/okhttp/internal/http/RequestHeaders;

    # setter for: Lcom/android/okhttp/internal/http/RequestHeaders;->noCache:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->access$002(Lcom/android/okhttp/internal/http/RequestHeaders;Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "max-age"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/RequestHeaders$1;->this$0:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-static {p2}, Lcom/android/okhttp/internal/http/HeaderParser;->parseSeconds(Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/android/okhttp/internal/http/RequestHeaders;->maxAgeSeconds:I
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->access$102(Lcom/android/okhttp/internal/http/RequestHeaders;I)I

    goto :goto_0

    :cond_2
    const-string v0, "max-stale"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/okhttp/internal/http/RequestHeaders$1;->this$0:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-static {p2}, Lcom/android/okhttp/internal/http/HeaderParser;->parseSeconds(Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/android/okhttp/internal/http/RequestHeaders;->maxStaleSeconds:I
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->access$202(Lcom/android/okhttp/internal/http/RequestHeaders;I)I

    goto :goto_0

    :cond_3
    const-string v0, "min-fresh"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/okhttp/internal/http/RequestHeaders$1;->this$0:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-static {p2}, Lcom/android/okhttp/internal/http/HeaderParser;->parseSeconds(Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/android/okhttp/internal/http/RequestHeaders;->minFreshSeconds:I
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->access$302(Lcom/android/okhttp/internal/http/RequestHeaders;I)I

    goto :goto_0

    :cond_4
    const-string v0, "only-if-cached"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/RequestHeaders$1;->this$0:Lcom/android/okhttp/internal/http/RequestHeaders;

    # setter for: Lcom/android/okhttp/internal/http/RequestHeaders;->onlyIfCached:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->access$402(Lcom/android/okhttp/internal/http/RequestHeaders;Z)Z

    goto :goto_0
.end method
