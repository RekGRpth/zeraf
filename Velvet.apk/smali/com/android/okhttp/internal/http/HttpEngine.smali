.class public Lcom/android/okhttp/internal/http/HttpEngine;
.super Ljava/lang/Object;
.source "HttpEngine.java"


# static fields
.field private static final GATEWAY_TIMEOUT_RESPONSE:Ljava/net/CacheResponse;


# instance fields
.field private automaticallyReleaseConnectionToPool:Z

.field private cacheRequest:Ljava/net/CacheRequest;

.field private cacheResponse:Ljava/net/CacheResponse;

.field private cachedResponseBody:Ljava/io/InputStream;

.field private cachedResponseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

.field protected connection:Lcom/android/okhttp/Connection;

.field private connectionReleased:Z

.field protected final method:Ljava/lang/String;

.field protected final policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

.field private requestBodyOut:Ljava/io/OutputStream;

.field final requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

.field private responseBodyIn:Ljava/io/InputStream;

.field responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

.field private responseSource:Lcom/android/okhttp/ResponseSource;

.field private responseTransferIn:Ljava/io/InputStream;

.field protected routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

.field sentRequestMillis:J

.field private transparentGzip:Z

.field private transport:Lcom/android/okhttp/internal/http/Transport;

.field final uri:Ljava/net/URI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/okhttp/internal/http/HttpEngine$1;

    invoke-direct {v0}, Lcom/android/okhttp/internal/http/HttpEngine$1;-><init>()V

    sput-object v0, Lcom/android/okhttp/internal/http/HttpEngine;->GATEWAY_TIMEOUT_RESPONSE:Ljava/net/CacheResponse;

    return-void
.end method

.method public constructor <init>(Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)V
    .locals 4
    .param p1    # Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/okhttp/internal/http/RawHeaders;
    .param p4    # Lcom/android/okhttp/Connection;
    .param p5    # Lcom/android/okhttp/internal/http/RetryableOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iput-object p2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->method:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iput-object p5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/Platform;->toUriLenient(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v1

    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Lcom/android/okhttp/internal/http/RequestHeaders;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    new-instance v3, Lcom/android/okhttp/internal/http/RawHeaders;

    invoke-direct {v3, p3}, Lcom/android/okhttp/internal/http/RawHeaders;-><init>(Lcom/android/okhttp/internal/http/RawHeaders;)V

    invoke-direct {v1, v2, v3}, Lcom/android/okhttp/internal/http/RequestHeaders;-><init>(Ljava/net/URI;Lcom/android/okhttp/internal/http/RawHeaders;)V

    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static getDefaultUserAgent()Ljava/lang/String;
    .locals 2

    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Java"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "java.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getOriginAddress(Ljava/net/URL;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    if-lez v1, :cond_0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/okhttp/internal/Util;->getDefaultPort(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private initContentStream(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseTransferIn:Ljava/io/InputStream;

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transparentGzip:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;->isContentEncodingGzip()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;->stripContentEncoding()V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;->stripContentLength()V

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, p1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyIn:Ljava/io/InputStream;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyIn:Ljava/io/InputStream;

    goto :goto_0
.end method

.method private initResponseSource()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v5, Lcom/android/okhttp/ResponseSource;->NETWORK:Lcom/android/okhttp/ResponseSource;

    iput-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v5}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getUseCaches()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->method:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v8}, Lcom/android/okhttp/internal/http/RequestHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/okhttp/internal/http/RawHeaders;->toMultimap(Z)Ljava/util/Map;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, Ljava/net/ResponseCache;->get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getBody()Ljava/io/InputStream;

    move-result-object v5

    iput-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    invoke-virtual {p0, v0}, Lcom/android/okhttp/internal/http/HttpEngine;->acceptCacheResponseType(Ljava/net/CacheResponse;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    if-nez v5, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_3
    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/RawHeaders;->fromMultimap(Ljava/util/Map;Z)Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v3

    new-instance v5, Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-direct {v5, v6, v3}, Lcom/android/okhttp/internal/http/ResponseHeaders;-><init>(Ljava/net/URI;Lcom/android/okhttp/internal/http/RawHeaders;)V

    iput-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v5, v1, v2, v6}, Lcom/android/okhttp/internal/http/ResponseHeaders;->chooseResponseSource(JLcom/android/okhttp/internal/http/RequestHeaders;)Lcom/android/okhttp/ResponseSource;

    move-result-object v5

    iput-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    sget-object v6, Lcom/android/okhttp/ResponseSource;->CACHE:Lcom/android/okhttp/ResponseSource;

    if-ne v5, v6, :cond_4

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Ljava/net/CacheResponse;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    invoke-direct {p0, v5, v6}, Lcom/android/okhttp/internal/http/HttpEngine;->setResponse(Lcom/android/okhttp/internal/http/ResponseHeaders;Ljava/io/InputStream;)V

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    sget-object v6, Lcom/android/okhttp/ResponseSource;->CONDITIONAL_CACHE:Lcom/android/okhttp/ResponseSource;

    if-ne v5, v6, :cond_5

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Ljava/net/CacheResponse;

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    sget-object v6, Lcom/android/okhttp/ResponseSource;->NETWORK:Lcom/android/okhttp/ResponseSource;

    if-ne v5, v6, :cond_6

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_6
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
.end method

.method private maybeCache()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getUseCaches()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->isCacheable(Lcom/android/okhttp/internal/http/RequestHeaders;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getHttpConnectionToCache()Ljava/net/HttpURLConnection;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/ResponseCache;->put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheRequest:Ljava/net/CacheRequest;

    goto :goto_0
.end method

.method private prepareRawRequestHeaders()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequestLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/RawHeaders;->setRequestLine(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->getUserAgent()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-static {}, Lcom/android/okhttp/internal/http/HttpEngine;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->setUserAgent(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->getHost()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v1

    invoke-static {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->getOriginAddress(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->setHost(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->getConnection()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    const-string v1, "Keep-Alive"

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->setConnection(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->getAcceptEncoding()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transparentGzip:Z

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    const-string v1, "gzip"

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->setAcceptEncoding(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasRequestBody()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->getContentType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    const-string v1, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->setContentType(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getIfModifiedSince()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/RequestHeaders;->setIfModifiedSince(Ljava/util/Date;)V

    :cond_6
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->cookieHandler:Ljava/net/CookieHandler;

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/RequestHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/RawHeaders;->toMultimap(Z)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->addCookies(Ljava/util/Map;)V

    :cond_7
    return-void
.end method

.method public static requestPath(Ljava/net/URL;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "/"

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private requestString()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->includeAuthorityInRequestLine()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->requestPath(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private sendSocketRequest()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->connect()V

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0, p0}, Lcom/android/okhttp/Connection;->newTransport(Lcom/android/okhttp/internal/http/HttpEngine;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/okhttp/internal/http/Transport;

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasRequestBody()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v0}, Lcom/android/okhttp/internal/http/Transport;->createRequestBody()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    :cond_2
    return-void
.end method

.method private setResponse(Lcom/android/okhttp/internal/http/ResponseHeaders;Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Lcom/android/okhttp/internal/http/ResponseHeaders;
    .param p2    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyIn:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    if-eqz p2, :cond_1

    invoke-direct {p0, p2}, Lcom/android/okhttp/internal/http/HttpEngine;->initContentStream(Ljava/io/InputStream;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected acceptCacheResponseType(Ljava/net/CacheResponse;)Z
    .locals 1
    .param p1    # Ljava/net/CacheResponse;

    const/4 v0, 0x1

    return v0
.end method

.method public final automaticallyReleaseConnectionToPool()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->automaticallyReleaseConnectionToPool:Z

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connectionReleased:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connectionPool:Lcom/android/okhttp/ConnectionPool;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0, v1}, Lcom/android/okhttp/ConnectionPool;->recycle(Lcom/android/okhttp/Connection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    :cond_0
    return-void
.end method

.method protected final connect()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/net/UnknownHostException;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v3, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v4, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    :goto_1
    new-instance v0, Lcom/android/okhttp/Address;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-static {v2}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URI;)I

    move-result v2

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->requestedProxy:Ljava/net/Proxy;

    invoke-direct/range {v0 .. v5}, Lcom/android/okhttp/Address;-><init>(Ljava/lang/String;ILjavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Ljava/net/Proxy;)V

    new-instance v1, Lcom/android/okhttp/internal/http/RouteSelector;

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v4, v2, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->proxySelector:Ljava/net/ProxySelector;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v5, v2, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connectionPool:Lcom/android/okhttp/ConnectionPool;

    sget-object v6, Lcom/android/okhttp/internal/Dns;->DEFAULT:Lcom/android/okhttp/internal/Dns;

    move-object v2, v0

    invoke-direct/range {v1 .. v6}, Lcom/android/okhttp/internal/http/RouteSelector;-><init>(Lcom/android/okhttp/Address;Ljava/net/URI;Ljava/net/ProxySelector;Lcom/android/okhttp/ConnectionPool;Lcom/android/okhttp/internal/Dns;)V

    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    :cond_3
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RouteSelector;->next()Lcom/android/okhttp/Connection;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->isConnected()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getConnectTimeout()I

    move-result v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getReadTimeout()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getTunnelConfig()Lcom/android/okhttp/TunnelRequest;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/okhttp/Connection;->connect(IILcom/android/okhttp/TunnelRequest;)V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connectionPool:Lcom/android/okhttp/ConnectionPool;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0, v1}, Lcom/android/okhttp/ConnectionPool;->maybeShare(Lcom/android/okhttp/Connection;)V

    :cond_4
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {p0, v0}, Lcom/android/okhttp/internal/http/HttpEngine;->connected(Lcom/android/okhttp/Connection;)V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->getProxy()Ljava/net/Proxy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->requestedProxy:Ljava/net/Proxy;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequestLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/RawHeaders;->setRequestLine(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move-object v3, v4

    goto :goto_1
.end method

.method protected connected(Lcom/android/okhttp/Connection;)V
    .locals 0
    .param p1    # Lcom/android/okhttp/Connection;

    return-void
.end method

.method public final getCacheResponse()Ljava/net/CacheResponse;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Ljava/net/CacheResponse;

    return-object v0
.end method

.method public final getConnection()Lcom/android/okhttp/Connection;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    return-object v0
.end method

.method public final getRequestBody()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    return-object v0
.end method

.method getRequestLine()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "HTTP/1.1"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->requestString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "HTTP/1.0"

    goto :goto_0
.end method

.method public final getResponseBody()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyIn:Ljava/io/InputStream;

    return-object v0
.end method

.method public final getResponseCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RawHeaders;->getResponseCode()I

    move-result v0

    return v0
.end method

.method public final getResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    return-object v0
.end method

.method protected getTunnelConfig()Lcom/android/okhttp/TunnelRequest;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method hasRequestBody()Z
    .locals 2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->method:Ljava/lang/String;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->method:Ljava/lang/String;

    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasResponse()Z
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasResponseBody()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/RawHeaders;->getResponseCode()I

    move-result v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->method:Ljava/lang/String;

    const-string v4, "HEAD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v2, v3, :cond_3

    const/16 v3, 0x130

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getContentLength()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/ResponseHeaders;->isChunked()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method protected includeAuthorityInRequestLine()Z
    .locals 2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->usingProxy()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->getProxy()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final readResponse()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponse()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "readResponse() without sendRequest()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    invoke-virtual {v0}, Lcom/android/okhttp/ResponseSource;->requiresConnection()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    instance-of v0, v0, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    check-cast v0, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RetryableOutputStream;->contentLength()I

    move-result v0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v1, v0}, Lcom/android/okhttp/internal/http/RequestHeaders;->setContentLength(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v0}, Lcom/android/okhttp/internal/http/Transport;->writeRequestHeaders()V

    :cond_4
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    instance-of v0, v0, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    check-cast v0, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    invoke-interface {v1, v0}, Lcom/android/okhttp/internal/http/Transport;->writeRequestBody(Lcom/android/okhttp/internal/http/RetryableOutputStream;)V

    :cond_5
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v0}, Lcom/android/okhttp/internal/http/Transport;->flushRequest()V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v0}, Lcom/android/okhttp/internal/http/Transport;->readResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-wide v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/okhttp/internal/http/ResponseHeaders;->setLocalTimestamps(JJ)V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    sget-object v1, Lcom/android/okhttp/ResponseSource;->CONDITIONAL_CACHE:Lcom/android/okhttp/ResponseSource;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->validate(Lcom/android/okhttp/internal/http/ResponseHeaders;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/okhttp/internal/http/HttpEngine;->release(Z)V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->combine(Lcom/android/okhttp/internal/http/ResponseHeaders;)Lcom/android/okhttp/internal/http/ResponseHeaders;

    move-result-object v0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    invoke-direct {p0, v0, v1}, Lcom/android/okhttp/internal/http/HttpEngine;->setResponse(Lcom/android/okhttp/internal/http/ResponseHeaders;Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    instance-of v0, v0, Lcom/android/okhttp/OkResponseCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    check-cast v0, Lcom/android/okhttp/OkResponseCache;

    invoke-interface {v0}, Lcom/android/okhttp/OkResponseCache;->trackConditionalCacheHit()V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Ljava/net/CacheResponse;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getHttpConnectionToCache()Ljava/net/HttpURLConnection;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/okhttp/OkResponseCache;->update(Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    :cond_7
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponseBody()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->maybeCache()V

    :cond_8
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheRequest:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Lcom/android/okhttp/internal/http/Transport;->getTransferStream(Ljava/net/CacheRequest;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/HttpEngine;->initContentStream(Ljava/io/InputStream;)V

    goto/16 :goto_0
.end method

.method public receiveHeaders(Lcom/android/okhttp/internal/http/RawHeaders;)V
    .locals 3
    .param p1    # Lcom/android/okhttp/internal/http/RawHeaders;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v1, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->cookieHandler:Ljava/net/CookieHandler;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/android/okhttp/internal/http/RawHeaders;->toMultimap(Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public final release(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyIn:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyIn:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connectionReleased:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connectionReleased:Z

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseTransferIn:Ljava/io/InputStream;

    invoke-interface {v0, p1, v1, v2}, Lcom/android/okhttp/internal/http/Transport;->makeReusable(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-static {v0}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->automaticallyReleaseConnectionToPool:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connectionPool:Lcom/android/okhttp/ConnectionPool;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0, v1}, Lcom/android/okhttp/ConnectionPool;->recycle(Lcom/android/okhttp/Connection;)V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    goto :goto_0
.end method

.method public final sendRequest()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->prepareRawRequestHeaders()V

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->initResponseSource()V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    instance-of v1, v1, Lcom/android/okhttp/OkResponseCache;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    check-cast v1, Lcom/android/okhttp/OkResponseCache;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    invoke-interface {v1, v2}, Lcom/android/okhttp/OkResponseCache;->trackResponse(Lcom/android/okhttp/ResponseSource;)V

    :cond_2
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->isOnlyIfCached()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    invoke-virtual {v1}, Lcom/android/okhttp/ResponseSource;->requiresConnection()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    sget-object v2, Lcom/android/okhttp/ResponseSource;->CONDITIONAL_CACHE:Lcom/android/okhttp/ResponseSource;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cachedResponseBody:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    :cond_3
    sget-object v1, Lcom/android/okhttp/ResponseSource;->CACHE:Lcom/android/okhttp/ResponseSource;

    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    sget-object v1, Lcom/android/okhttp/internal/http/HttpEngine;->GATEWAY_TIMEOUT_RESPONSE:Ljava/net/CacheResponse;

    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Ljava/net/CacheResponse;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Ljava/net/CacheResponse;

    invoke-virtual {v1}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/okhttp/internal/http/RawHeaders;->fromMultimap(Ljava/util/Map;Z)Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v0

    new-instance v1, Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-direct {v1, v2, v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;-><init>(Ljava/net/URI;Lcom/android/okhttp/internal/http/RawHeaders;)V

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Ljava/net/CacheResponse;

    invoke-virtual {v2}, Ljava/net/CacheResponse;->getBody()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/okhttp/internal/http/HttpEngine;->setResponse(Lcom/android/okhttp/internal/http/ResponseHeaders;Ljava/io/InputStream;)V

    :cond_4
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    invoke-virtual {v1}, Lcom/android/okhttp/ResponseSource;->requiresConnection()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->sendSocketRequest()V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connectionPool:Lcom/android/okhttp/ConnectionPool;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v1, v2}, Lcom/android/okhttp/ConnectionPool;->recycle(Lcom/android/okhttp/Connection;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    goto :goto_0
.end method

.method public writingRequestHeaders()V
    .locals 4

    iget-wide v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    return-void
.end method
