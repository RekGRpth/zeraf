.class Lcom/android/okhttp/internal/spdy/SpdyReader$1;
.super Ljava/io/InputStream;
.source "SpdyReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/okhttp/internal/spdy/SpdyReader;->newNameValueBlockStream()Ljava/io/DataInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/okhttp/internal/spdy/SpdyReader;


# direct methods
.method constructor <init>(Lcom/android/okhttp/internal/spdy/SpdyReader;)V
    .locals 0

    iput-object p1, p0, Lcom/android/okhttp/internal/spdy/SpdyReader$1;->this$0:Lcom/android/okhttp/internal/spdy/SpdyReader;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyReader$1;->this$0:Lcom/android/okhttp/internal/spdy/SpdyReader;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyReader;->in:Ljava/io/DataInputStream;
    invoke-static {v0}, Lcom/android/okhttp/internal/spdy/SpdyReader;->access$100(Lcom/android/okhttp/internal/spdy/SpdyReader;)Ljava/io/DataInputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    return-void
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/android/okhttp/internal/Util;->readSingleByte(Ljava/io/InputStream;)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyReader$1;->this$0:Lcom/android/okhttp/internal/spdy/SpdyReader;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyReader;->compressedLimit:I
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyReader;->access$000(Lcom/android/okhttp/internal/spdy/SpdyReader;)I

    move-result v1

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result p3

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyReader$1;->this$0:Lcom/android/okhttp/internal/spdy/SpdyReader;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyReader;->in:Ljava/io/DataInputStream;
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyReader;->access$100(Lcom/android/okhttp/internal/spdy/SpdyReader;)Ljava/io/DataInputStream;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v0

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyReader$1;->this$0:Lcom/android/okhttp/internal/spdy/SpdyReader;

    # -= operator for: Lcom/android/okhttp/internal/spdy/SpdyReader;->compressedLimit:I
    invoke-static {v1, v0}, Lcom/android/okhttp/internal/spdy/SpdyReader;->access$020(Lcom/android/okhttp/internal/spdy/SpdyReader;I)I

    return v0
.end method
