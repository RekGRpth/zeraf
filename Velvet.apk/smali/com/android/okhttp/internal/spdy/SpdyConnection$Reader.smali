.class Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;
.super Ljava/lang/Object;
.source "SpdyConnection.java"

# interfaces
.implements Lcom/android/okhttp/internal/spdy/SpdyReader$Handler;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/spdy/SpdyConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Reader"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;


# direct methods
.method private constructor <init>(Lcom/android/okhttp/internal/spdy/SpdyConnection;)V
    .locals 0

    iput-object p1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/okhttp/internal/spdy/SpdyConnection;Lcom/android/okhttp/internal/spdy/SpdyConnection$1;)V
    .locals 0
    .param p1    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p2    # Lcom/android/okhttp/internal/spdy/SpdyConnection$1;

    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;-><init>(Lcom/android/okhttp/internal/spdy/SpdyConnection;)V

    return-void
.end method


# virtual methods
.method public data(IILjava/io/InputStream;I)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/io/InputStream;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->getStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;
    invoke-static {v1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$900(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    const/4 v2, 0x2

    invoke-virtual {v1, p2, v2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->writeSynResetLater(II)V

    int-to-long v1, p4

    invoke-static {p3, v1, v2}, Lcom/android/okhttp/internal/Util;->skipByReading(Ljava/io/InputStream;J)J

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p3, p4}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveData(Ljava/io/InputStream;I)V

    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveFin()V

    goto :goto_0
.end method

.method public goAway(III)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v4, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    const/4 v5, 0x1

    # setter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->shutdown:Z
    invoke-static {v3, v5}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1002(Lcom/android/okhttp/internal/spdy/SpdyConnection;Z)Z

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;
    invoke-static {v3}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1200(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, p2, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/spdy/SpdyStream;->isLocallyInitiated()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/okhttp/internal/spdy/SpdyStream;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveRstStream(I)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public headers(IILjava/util/List;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->getStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;
    invoke-static {v1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$900(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveHeaders(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public noop()V
    .locals 0

    return-void
.end method

.method public ping(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-boolean v2, v2, Lcom/android/okhttp/internal/spdy/SpdyConnection;->client:Z

    rem-int/lit8 v3, p2, 0x2

    if-ne v3, v1, :cond_1

    :goto_0
    if-eq v2, v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    const/4 v2, 0x0

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->writePingLater(ILcom/android/okhttp/internal/spdy/Ping;)V
    invoke-static {v1, p2, v2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1600(Lcom/android/okhttp/internal/spdy/SpdyConnection;ILcom/android/okhttp/internal/spdy/Ping;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->removePing(I)Lcom/android/okhttp/internal/spdy/Ping;
    invoke-static {v1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1700(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)Lcom/android/okhttp/internal/spdy/Ping;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/Ping;->receive()V

    goto :goto_1
.end method

.method public rstStream(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->removeStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveRstStream(I)V

    :cond_0
    return-void
.end method

.method public run()V
    .locals 5

    const/4 v2, 0x2

    const/4 v1, 0x6

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyReader:Lcom/android/okhttp/internal/spdy/SpdyReader;
    invoke-static {v3}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$700(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Lcom/android/okhttp/internal/spdy/SpdyReader;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/android/okhttp/internal/spdy/SpdyReader;->nextFrame(Lcom/android/okhttp/internal/spdy/SpdyReader$Handler;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    const/4 v1, 0x5

    :try_start_1
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->close(II)V
    invoke-static {v3, v2, v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$800(Lcom/android/okhttp/internal/spdy/SpdyConnection;II)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v2, 0x1

    const/4 v1, 0x1

    :try_start_2
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->close(II)V
    invoke-static {v3, v2, v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$800(Lcom/android/okhttp/internal/spdy/SpdyConnection;II)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_3
    iget-object v4, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->close(II)V
    invoke-static {v4, v2, v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$800(Lcom/android/okhttp/internal/spdy/SpdyConnection;II)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_1
    throw v3

    :catch_2
    move-exception v4

    goto :goto_1

    :catch_3
    move-exception v3

    goto :goto_0
.end method

.method public settings(ILcom/android/okhttp/internal/spdy/Settings;)V
    .locals 9
    .param p1    # I
    .param p2    # Lcom/android/okhttp/internal/spdy/Settings;

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v6, v6, Lcom/android/okhttp/internal/spdy/SpdyConnection;->settings:Lcom/android/okhttp/internal/spdy/Settings;

    if-eqz v6, :cond_0

    and-int/lit8 v6, p1, 0x1

    if-eqz v6, :cond_2

    :cond_0
    iget-object v6, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iput-object p2, v6, Lcom/android/okhttp/internal/spdy/SpdyConnection;->settings:Lcom/android/okhttp/internal/spdy/Settings;

    :goto_0
    iget-object v6, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;
    invoke-static {v6}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1200(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;
    invoke-static {v6}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1200(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    iget-object v8, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;
    invoke-static {v8}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1200(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    new-array v8, v8, [Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-interface {v6, v8}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, [Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-object v5, v0

    :cond_1
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_3

    move-object v1, v5

    array-length v3, v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v4, v1, v2

    monitor-enter v4

    :try_start_1
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v6, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v6, v6, Lcom/android/okhttp/internal/spdy/SpdyConnection;->settings:Lcom/android/okhttp/internal/spdy/Settings;

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveSettings(Lcom/android/okhttp/internal/spdy/Settings;)V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    :try_start_4
    iget-object v6, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v6, v6, Lcom/android/okhttp/internal/spdy/SpdyConnection;->settings:Lcom/android/okhttp/internal/spdy/Settings;

    invoke-virtual {v6, p2}, Lcom/android/okhttp/internal/spdy/Settings;->merge(Lcom/android/okhttp/internal/spdy/Settings;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v6

    :catchall_1
    move-exception v6

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v6

    :catchall_2
    move-exception v6

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v6

    :cond_3
    return-void
.end method

.method public synReply(IILjava/util/List;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->getStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;
    invoke-static {v1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$900(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    const/4 v2, 0x2

    invoke-virtual {v1, p2, v2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->writeSynResetLater(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p3}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveReply(Ljava/util/List;)V

    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveFin()V

    goto :goto_0
.end method

.method public synStream(IIIIILjava/util/List;)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v9, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    monitor-enter v9

    :try_start_0
    new-instance v0, Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v7, v1, Lcom/android/okhttp/internal/spdy/SpdyConnection;->settings:Lcom/android/okhttp/internal/spdy/Settings;

    move v1, p2

    move v3, p1

    move v4, p4

    move v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/android/okhttp/internal/spdy/SpdyStream;-><init>(ILcom/android/okhttp/internal/spdy/SpdyConnection;IIILjava/util/List;Lcom/android/okhttp/internal/spdy/Settings;)V

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->shutdown:Z
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1000(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Z

    move-result v1

    if-eqz v1, :cond_0

    monitor-exit v9

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # setter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->lastGoodStreamId:I
    invoke-static {v1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1102(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)I

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1200(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/okhttp/internal/spdy/SpdyStream;

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v8, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v8, v1}, Lcom/android/okhttp/internal/spdy/SpdyStream;->closeLater(I)V

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->removeStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_1
    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->executor:Ljava/util/concurrent/ExecutorService;
    invoke-static {}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1500()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader$1;

    const-string v3, "Callback %s stream %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection;->hostName:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$1300(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader$1;-><init>(Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;Ljava/lang/String;Lcom/android/okhttp/internal/spdy/SpdyStream;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public windowUpdate(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;->this$0:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    # invokes: Lcom/android/okhttp/internal/spdy/SpdyConnection;->getStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;
    invoke-static {v1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->access$900(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3}, Lcom/android/okhttp/internal/spdy/SpdyStream;->receiveWindowUpdate(I)V

    :cond_0
    return-void
.end method
