.class final Lcom/android/okhttp/internal/spdy/Settings;
.super Ljava/lang/Object;
.source "Settings.java"


# instance fields
.field private persistValue:I

.field private persisted:I

.field private set:I

.field private final values:[I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/Settings;->values:[I

    return-void
.end method


# virtual methods
.method flags(I)I
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/android/okhttp/internal/spdy/Settings;->isPersisted(I)Z

    move-result v1

    if-eqz v1, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/okhttp/internal/spdy/Settings;->persistValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x1

    :cond_1
    return v0
.end method

.method get(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/Settings;->values:[I

    aget v0, v0, p1

    return v0
.end method

.method getInitialWindowSize(I)I
    .locals 3
    .param p1    # I

    const/16 v0, 0x80

    iget v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->set:I

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->values:[I

    const/4 v2, 0x7

    aget p1, v1, v2

    :cond_0
    return p1
.end method

.method isPersisted(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    shl-int v0, v1, p1

    iget v2, p0, Lcom/android/okhttp/internal/spdy/Settings;->persisted:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method isSet(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    shl-int v0, v1, p1

    iget v2, p0, Lcom/android/okhttp/internal/spdy/Settings;->set:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method merge(Lcom/android/okhttp/internal/spdy/Settings;)V
    .locals 3
    .param p1    # Lcom/android/okhttp/internal/spdy/Settings;

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x9

    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Lcom/android/okhttp/internal/spdy/Settings;->isSet(I)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Lcom/android/okhttp/internal/spdy/Settings;->flags(I)I

    move-result v1

    invoke-virtual {p1, v0}, Lcom/android/okhttp/internal/spdy/Settings;->get(I)I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/okhttp/internal/spdy/Settings;->set(III)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method persistValue(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    shl-int v0, v1, p1

    iget v2, p0, Lcom/android/okhttp/internal/spdy/Settings;->persistValue:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method set(III)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->values:[I

    array-length v1, v1

    if-lt p1, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    shl-int v0, v1, p1

    iget v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->set:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->set:I

    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->persistValue:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->persistValue:I

    :goto_1
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->persisted:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->persisted:I

    :goto_2
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->values:[I

    aput p3, v1, p1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->persistValue:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->persistValue:I

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->persisted:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lcom/android/okhttp/internal/spdy/Settings;->persisted:I

    goto :goto_2
.end method
