.class public final Lcom/android/okhttp/internal/spdy/SpdyStream;
.super Ljava/lang/Object;
.source "SpdyStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/okhttp/internal/spdy/SpdyStream$1;,
        Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;,
        Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final STATUS_CODE_NAMES:[Ljava/lang/String;


# instance fields
.field private final connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

.field private final id:I

.field private final in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

.field private final out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

.field private final priority:I

.field private readTimeoutMillis:J

.field private final requestHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private responseHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rstStatusCode:I

.field private final slot:I

.field private writeWindowSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v3, v0, v2

    const-string v2, "PROTOCOL_ERROR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "INVALID_STREAM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "REFUSED_STREAM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "UNSUPPORTED_VERSION"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CANCEL"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "INTERNAL_ERROR"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "FLOW_CONTROL_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "STREAM_IN_USE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "STREAM_ALREADY_CLOSED"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "INVALID_CREDENTIALS"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "FRAME_TOO_LARGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/okhttp/internal/spdy/SpdyStream;->STATUS_CODE_NAMES:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method constructor <init>(ILcom/android/okhttp/internal/spdy/SpdyConnection;IIILjava/util/List;Lcom/android/okhttp/internal/spdy/Settings;)V
    .locals 6
    .param p1    # I
    .param p2    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p7    # Lcom/android/okhttp/internal/spdy/Settings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/android/okhttp/internal/spdy/SpdyConnection;",
            "III",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/okhttp/internal/spdy/Settings;",
            ")V"
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->readTimeoutMillis:J

    new-instance v0, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    invoke-direct {v0, p0, v5}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;-><init>(Lcom/android/okhttp/internal/spdy/SpdyStream;Lcom/android/okhttp/internal/spdy/SpdyStream$1;)V

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    new-instance v0, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    invoke-direct {v0, p0, v5}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;-><init>(Lcom/android/okhttp/internal/spdy/SpdyStream;Lcom/android/okhttp/internal/spdy/SpdyStream$1;)V

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connection == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p6, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "requestHeaders == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput p1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    iput-object p2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iput p4, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->priority:I

    iput p5, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->slot:I

    iput-object p6, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->requestHeaders:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->isLocallyInitiated()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    # setter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->finished:Z
    invoke-static {v3, v0}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->access$202(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;Z)Z

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    and-int/lit8 v3, p3, 0x1

    if-eqz v3, :cond_3

    :goto_1
    # setter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->finished:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->access$302(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;Z)Z

    :goto_2
    invoke-direct {p0, p7}, Lcom/android/okhttp/internal/spdy/SpdyStream;->setSettings(Lcom/android/okhttp/internal/spdy/Settings;)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    # setter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->finished:Z
    invoke-static {v3, v0}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->access$202(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;Z)Z

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    and-int/lit8 v3, p3, 0x2

    if-eqz v3, :cond_6

    :goto_4
    # setter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->finished:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->access$302(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;Z)Z

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4
.end method

.method static synthetic access$1000(Lcom/android/okhttp/internal/spdy/SpdyStream;)I
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    return v0
.end method

.method static synthetic access$1100(Lcom/android/okhttp/internal/spdy/SpdyStream;)V
    .locals 0
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->cancelStreamIfNecessary()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/okhttp/internal/spdy/SpdyStream;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-direct {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/okhttp/internal/spdy/SpdyStream;)I
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->writeWindowSize:I

    return v0
.end method

.method static synthetic access$700(Lcom/android/okhttp/internal/spdy/SpdyStream;)I
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    return v0
.end method

.method static synthetic access$800(Lcom/android/okhttp/internal/spdy/SpdyStream;)Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/okhttp/internal/spdy/SpdyStream;)J
    .locals 2
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget-wide v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->readTimeoutMillis:J

    return-wide v0
.end method

.method private cancelStreamIfNecessary()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-boolean v2, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->finished:Z
    invoke-static {v2}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->access$200(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->closed:Z
    invoke-static {v2}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->access$400(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->finished:Z
    invoke-static {v2}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->access$300(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->closed:Z
    invoke-static {v2}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->access$500(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->isOpen()Z

    move-result v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/android/okhttp/internal/spdy/SpdyStream;->close(I)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_4
    if-nez v1, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget v3, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->removeStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    goto :goto_1
.end method

.method private closeInternal(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    sget-boolean v1, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    monitor-exit p0

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->finished:Z
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->access$200(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->finished:Z
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->access$300(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;)Z

    move-result v1

    if-eqz v1, :cond_2

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iput p1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->removeStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private rstStatusString()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    sget-object v1, Lcom/android/okhttp/internal/spdy/SpdyStream;->STATUS_CODE_NAMES:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v0, Lcom/android/okhttp/internal/spdy/SpdyStream;->STATUS_CODE_NAMES:[Ljava/lang/String;

    iget v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setSettings(Lcom/android/okhttp/internal/spdy/Settings;)V
    .locals 2

    const/high16 v0, 0x10000

    sget-boolean v1, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Lcom/android/okhttp/internal/spdy/Settings;->getInitialWindowSize(I)I

    move-result v0

    :cond_1
    iput v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->writeWindowSize:I

    return-void
.end method


# virtual methods
.method public close(I)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/spdy/SpdyStream;->closeInternal(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    invoke-virtual {v0, v1, p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->writeSynReset(II)V

    goto :goto_0
.end method

.method public closeLater(I)V
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/spdy/SpdyStream;->closeInternal(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    invoke-virtual {v0, v1, p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->writeSynResetLater(II)V

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->isLocallyInitiated()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reply before requesting the output stream"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    return-object v0
.end method

.method public declared-synchronized getResponseHeaders()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;

    if-nez v2, :cond_0

    iget v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/io/InterruptedIOException;

    invoke-direct {v1}, Ljava/io/InterruptedIOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    :try_start_2
    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v2

    :cond_1
    :try_start_3
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stream was reset: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public isLocallyInitiated()Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v3, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    rem-int/lit8 v3, v3, 0x2

    if-ne v3, v1, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/spdy/SpdyConnection;->client:Z

    if-ne v3, v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public declared-synchronized isOpen()Z
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->finished:Z
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->access$200(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->closed:Z
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->access$400(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->finished:Z
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->access$300(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->closed:Z
    invoke-static {v1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->access$500(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method receiveData(Ljava/io/InputStream;I)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-boolean v0, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    invoke-virtual {v0, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->receive(Ljava/io/InputStream;I)V

    return-void
.end method

.method receiveFin()V
    .locals 3

    sget-boolean v1, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->in:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;

    const/4 v2, 0x1

    # setter for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->finished:Z
    invoke-static {v1, v2}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;->access$202(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataInputStream;Z)Z

    invoke-virtual {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->isOpen()Z

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->removeStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method receiveHeaders(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-boolean v2, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_0
    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;

    if-eqz v2, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/android/okhttp/internal/spdy/SpdyStream;->closeLater(I)V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method receiveReply(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-boolean v2, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_0
    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->isLocallyInitiated()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;

    if-nez v2, :cond_2

    iput-object p1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->responseHeaders:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->isOpen()Z

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/android/okhttp/internal/spdy/SpdyStream;->closeLater(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_3
    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->connection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget v3, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->id:I

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->removeStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    goto :goto_1
.end method

.method declared-synchronized receiveRstStream(I)V
    .locals 2
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iput p1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->rstStatusCode:I

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method receiveSettings(Lcom/android/okhttp/internal/spdy/Settings;)V
    .locals 1
    .param p1    # Lcom/android/okhttp/internal/spdy/Settings;

    sget-boolean v0, Lcom/android/okhttp/internal/spdy/SpdyStream;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/spdy/SpdyStream;->setSettings(Lcom/android/okhttp/internal/spdy/Settings;)V

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method

.method declared-synchronized receiveWindowUpdate(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->out:Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;

    # -= operator for: Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->unacknowledgedBytes:I
    invoke-static {v0, p1}, Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;->access$620(Lcom/android/okhttp/internal/spdy/SpdyStream$SpdyDataOutputStream;I)I

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setReadTimeout(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/okhttp/internal/spdy/SpdyStream;->readTimeoutMillis:J

    return-void
.end method
