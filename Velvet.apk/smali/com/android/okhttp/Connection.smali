.class public final Lcom/android/okhttp/Connection;
.super Ljava/lang/Object;
.source "Connection.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final HTTP_11:[B

.field private static final NPN_PROTOCOLS:[B

.field private static final SPDY3:[B


# instance fields
.field private final address:Lcom/android/okhttp/Address;

.field private connected:Z

.field private httpMinorVersion:I

.field private idleStartTimeNs:J

.field private in:Ljava/io/InputStream;

.field private final inetSocketAddress:Ljava/net/InetSocketAddress;

.field private final modernTls:Z

.field private out:Ljava/io/OutputStream;

.field private final proxy:Ljava/net/Proxy;

.field private socket:Ljava/net/Socket;

.field private spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/okhttp/Connection;->NPN_PROTOCOLS:[B

    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/okhttp/Connection;->SPDY3:[B

    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/okhttp/Connection;->HTTP_11:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x6t
        0x73t
        0x70t
        0x64t
        0x79t
        0x2ft
        0x33t
        0x8t
        0x68t
        0x74t
        0x74t
        0x70t
        0x2ft
        0x31t
        0x2et
        0x31t
    .end array-data

    :array_1
    .array-data 1
        0x73t
        0x70t
        0x64t
        0x79t
        0x2ft
        0x33t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x68t
        0x74t
        0x74t
        0x70t
        0x2ft
        0x31t
        0x2et
        0x31t
    .end array-data
.end method

.method public constructor <init>(Lcom/android/okhttp/Address;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Z)V
    .locals 2
    .param p1    # Lcom/android/okhttp/Address;
    .param p2    # Ljava/net/Proxy;
    .param p3    # Ljava/net/InetSocketAddress;
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/okhttp/Connection;->connected:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/okhttp/Connection;->httpMinorVersion:I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "address == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "proxy == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "inetSocketAddress == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iput-object p2, p0, Lcom/android/okhttp/Connection;->proxy:Ljava/net/Proxy;

    iput-object p3, p0, Lcom/android/okhttp/Connection;->inetSocketAddress:Ljava/net/InetSocketAddress;

    iput-boolean p4, p0, Lcom/android/okhttp/Connection;->modernTls:Z

    return-void
.end method

.method private makeTunnel(Lcom/android/okhttp/TunnelRequest;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/android/okhttp/TunnelRequest;->getRequestHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/android/okhttp/Connection;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RawHeaders;->toBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    iget-object v1, p0, Lcom/android/okhttp/Connection;->in:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/android/okhttp/internal/http/RawHeaders;->fromBytes(Ljava/io/InputStream;)Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/RawHeaders;->getResponseCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected response code for CONNECT: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/RawHeaders;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    new-instance v1, Lcom/android/okhttp/internal/http/RawHeaders;

    invoke-direct {v1, v0}, Lcom/android/okhttp/internal/http/RawHeaders;-><init>(Lcom/android/okhttp/internal/http/RawHeaders;)V

    new-instance v0, Ljava/net/URL;

    const-string v3, "https"

    iget-object v4, p1, Lcom/android/okhttp/TunnelRequest;->host:Ljava/lang/String;

    iget v5, p1, Lcom/android/okhttp/TunnelRequest;->port:I

    const-string v6, "/"

    invoke-direct {v0, v3, v4, v5, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    const/16 v3, 0x197

    iget-object v4, p0, Lcom/android/okhttp/Connection;->proxy:Ljava/net/Proxy;

    invoke-static {v3, v2, v1, v4, v0}, Lcom/android/okhttp/internal/http/HttpAuthenticator;->processAuthHeader(ILcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/internal/http/RawHeaders;Ljava/net/Proxy;Ljava/net/URL;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to authenticate with proxy"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x197 -> :sswitch_0
    .end sparse-switch
.end method

.method private upgradeToTls(Lcom/android/okhttp/TunnelRequest;)V
    .locals 8
    .param p1    # Lcom/android/okhttp/TunnelRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x1

    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->requiresTunnel()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1}, Lcom/android/okhttp/Connection;->makeTunnel(Lcom/android/okhttp/TunnelRequest;)V

    :cond_0
    iget-object v3, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget-object v3, v3, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v4, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    iget-object v5, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget-object v5, v5, Lcom/android/okhttp/Address;->uriHost:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget v6, v6, Lcom/android/okhttp/Address;->uriPort:I

    invoke-virtual {v3, v4, v5, v6, v7}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    check-cast v2, Ljavax/net/ssl/SSLSocket;

    iget-boolean v3, p0, Lcom/android/okhttp/Connection;->modernTls:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget-object v3, v3, Lcom/android/okhttp/Address;->uriHost:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/android/okhttp/internal/Platform;->enableTlsExtensions(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    :goto_0
    iget-boolean v3, p0, Lcom/android/okhttp/Connection;->modernTls:Z

    if-eqz v3, :cond_1

    sget-object v3, Lcom/android/okhttp/Connection;->NPN_PROTOCOLS:[B

    invoke-virtual {v0, v2, v3}, Lcom/android/okhttp/internal/Platform;->setNpnProtocols(Ljavax/net/ssl/SSLSocket;[B)V

    :cond_1
    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    iget-object v3, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget-object v3, v3, Lcom/android/okhttp/Address;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    iget-object v4, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget-object v4, v4, Lcom/android/okhttp/Address;->uriHost:Ljava/lang/String;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Hostname \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget-object v5, v5, Lcom/android/okhttp/Address;->uriHost:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' was not verified"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-virtual {v0, v2}, Lcom/android/okhttp/internal/Platform;->supportTlsIntolerantServer(Ljavax/net/ssl/SSLSocket;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/Connection;->out:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/Connection;->in:Ljava/io/InputStream;

    iget-boolean v3, p0, Lcom/android/okhttp/Connection;->modernTls:Z

    if-eqz v3, :cond_4

    invoke-virtual {v0, v2}, Lcom/android/okhttp/internal/Platform;->getNpnSelectedProtocol(Ljavax/net/ssl/SSLSocket;)[B

    move-result-object v1

    if-eqz v1, :cond_4

    sget-object v3, Lcom/android/okhttp/Connection;->SPDY3:[B

    invoke-static {v1, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    new-instance v3, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;

    iget-object v4, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    invoke-virtual {v4}, Lcom/android/okhttp/Address;->getUriHost()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/Connection;->in:Ljava/io/InputStream;

    iget-object v6, p0, Lcom/android/okhttp/Connection;->out:Ljava/io/OutputStream;

    invoke-direct {v3, v4, v7, v5, v6}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;-><init>(Ljava/lang/String;ZLjava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->build()Lcom/android/okhttp/internal/spdy/SpdyConnection;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    :cond_4
    return-void

    :cond_5
    sget-object v3, Lcom/android/okhttp/Connection;->HTTP_11:[B

    invoke-static {v1, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected NPN transport "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    const-string v6, "ISO-8859-1"

    invoke-direct {v5, v1, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    return-void
.end method

.method public connect(IILcom/android/okhttp/TunnelRequest;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/android/okhttp/TunnelRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v1, p0, Lcom/android/okhttp/Connection;->connected:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "already connected"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/okhttp/Connection;->connected:Z

    iget-object v1, p0, Lcom/android/okhttp/Connection;->proxy:Ljava/net/Proxy;

    invoke-virtual {v1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v1, v2, :cond_3

    new-instance v1, Ljava/net/Socket;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->proxy:Ljava/net/Proxy;

    invoke-direct {v1, v2}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    :goto_0
    iput-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->inetSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v1, v2, p1}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/android/okhttp/Connection;->in:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/android/okhttp/Connection;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget-object v1, v1, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_1

    invoke-direct {p0, p3}, Lcom/android/okhttp/Connection;->upgradeToTls(Lcom/android/okhttp/TunnelRequest;)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->isSpdy()Z

    move-result v1

    if-nez v1, :cond_2

    const/16 v0, 0x80

    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->in:Ljava/io/InputStream;

    invoke-direct {v1, v2, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v1, p0, Lcom/android/okhttp/Connection;->in:Ljava/io/InputStream;

    :cond_2
    return-void

    :cond_3
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1}, Ljava/net/Socket;-><init>()V

    goto :goto_0
.end method

.method public getAddress()Lcom/android/okhttp/Address;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    return-object v0
.end method

.method public getHttpMinorVersion()I
    .locals 1

    iget v0, p0, Lcom/android/okhttp/Connection;->httpMinorVersion:I

    return v0
.end method

.method public getIdleStartTimeNs()J
    .locals 2

    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/okhttp/Connection;->idleStartTimeNs:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->getIdleStartTimeNs()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getProxy()Ljava/net/Proxy;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->proxy:Ljava/net/Proxy;

    return-object v0
.end method

.method public getSocket()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/okhttp/Connection;->connected:Z

    return v0
.end method

.method public isExpired(J)Z
    .locals 4
    .param p1    # J

    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->getIdleStartTimeNs()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIdle()Z
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSpdy()Z
    .locals 1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newTransport(Lcom/android/okhttp/internal/http/HttpEngine;)Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/android/okhttp/internal/http/HttpEngine;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/okhttp/internal/http/SpdyTransport;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-direct {v0, p1, v1}, Lcom/android/okhttp/internal/http/SpdyTransport;-><init>(Lcom/android/okhttp/internal/http/HttpEngine;Lcom/android/okhttp/internal/spdy/SpdyConnection;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/okhttp/internal/http/HttpTransport;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->out:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->in:Ljava/io/InputStream;

    invoke-direct {v0, p1, v1, v2}, Lcom/android/okhttp/internal/http/HttpTransport;-><init>(Lcom/android/okhttp/internal/http/HttpEngine;Ljava/io/OutputStream;Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method public requiresTunnel()Z
    .locals 2

    iget-object v0, p0, Lcom/android/okhttp/Connection;->address:Lcom/android/okhttp/Address;

    iget-object v0, v0, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->proxy:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->proxy:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetIdleStartTime()V
    .locals 2

    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "spdyConnection != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/okhttp/Connection;->idleStartTimeNs:J

    return-void
.end method

.method public setHttpMinorVersion(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/okhttp/Connection;->httpMinorVersion:I

    return-void
.end method
