.class Lcom/android/inputmethodcommon/InputMethodSettingsImpl;
.super Ljava/lang/Object;
.source "InputMethodSettingsImpl.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mImi:Landroid/view/inputmethod/InputMethodInfo;

.field private mImm:Landroid/view/inputmethod/InputMethodManager;

.field private mInputMethodSettingsCategoryTitleRes:I

.field private mSubtypeEnablerIcon:Landroid/graphics/drawable/Drawable;

.field private mSubtypeEnablerIconRes:I

.field private mSubtypeEnablerPreference:Landroid/preference/Preference;

.field private mSubtypeEnablerTitle:Ljava/lang/CharSequence;

.field private mSubtypeEnablerTitleRes:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethodcommon/InputMethodSettingsImpl;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/android/inputmethodcommon/InputMethodSettingsImpl;
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->getSubtypeEnablerTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/inputmethodcommon/InputMethodSettingsImpl;)Landroid/view/inputmethod/InputMethodInfo;
    .locals 1
    .param p0    # Lcom/android/inputmethodcommon/InputMethodSettingsImpl;

    iget-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    return-object v0
.end method

.method private static getEnabledSubtypesLabel(Landroid/content/Context;Landroid/view/inputmethod/InputMethodManager;Landroid/view/inputmethod/InputMethodInfo;)Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/inputmethod/InputMethodManager;
    .param p2    # Landroid/view/inputmethod/InputMethodInfo;

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_1
    const/4 v5, 0x1

    invoke-virtual {p1, p2, v5}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_3

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v3, p0, v5, v6}, Landroid/view/inputmethod/InputMethodSubtype;->getDisplayName(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private static getMyImi(Landroid/content/Context;Landroid/view/inputmethod/InputMethodManager;)Landroid/view/inputmethod/InputMethodInfo;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getSubtypeEnablerTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerTitleRes:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerTitleRes:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerTitle:Ljava/lang/CharSequence;

    goto :goto_0
.end method


# virtual methods
.method public init(Landroid/content/Context;Landroid/preference/PreferenceScreen;)Z
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/preference/PreferenceScreen;

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mContext:Landroid/content/Context;

    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mImm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-static {p1, v0}, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->getMyImi(Landroid/content/Context;Landroid/view/inputmethod/InputMethodManager;)Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    iget-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    move-result v0

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    new-instance v2, Lcom/android/inputmethodcommon/InputMethodSettingsImpl$1;

    invoke-direct {v2, p0, p1}, Lcom/android/inputmethodcommon/InputMethodSettingsImpl$1;-><init>(Lcom/android/inputmethodcommon/InputMethodSettingsImpl;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->updateSubtypeEnabler()V

    move v0, v1

    goto :goto_0
.end method

.method public setInputMethodSettingsCategoryTitle(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mInputMethodSettingsCategoryTitleRes:I

    invoke-virtual {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->updateSubtypeEnabler()V

    return-void
.end method

.method public setSubtypeEnablerTitle(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerTitleRes:I

    invoke-virtual {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->updateSubtypeEnabler()V

    return-void
.end method

.method public updateSubtypeEnabler()V
    .locals 4

    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerTitleRes:I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    iget v2, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerTitleRes:I

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(I)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mImm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v3, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    invoke-static {v1, v2, v3}, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->getEnabledSubtypesLabel(Landroid/content/Context;Landroid/view/inputmethod/InputMethodManager;Landroid/view/inputmethod/InputMethodInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    iget v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerIconRes:I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    iget v2, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerIconRes:I

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIcon(I)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerTitle:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerTitle:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerPreference:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/android/inputmethodcommon/InputMethodSettingsImpl;->mSubtypeEnablerIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
