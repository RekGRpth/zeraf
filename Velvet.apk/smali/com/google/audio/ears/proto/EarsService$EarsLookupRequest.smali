.class public final Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EarsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/audio/ears/proto/EarsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EarsLookupRequest"
.end annotation


# instance fields
.field private cachedSize:I

.field private clientCountryCode_:Ljava/lang/String;

.field private clientId_:Ljava/lang/String;

.field private clientLocale_:Ljava/lang/String;

.field private clientName_:Ljava/lang/String;

.field private clientVersion_:Ljava/lang/String;

.field private debug_:Z

.field private desiredResultType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private hasClientCountryCode:Z

.field private hasClientId:Z

.field private hasClientLocale:Z

.field private hasClientName:Z

.field private hasClientVersion:Z

.field private hasDebug:Z

.field private hasMaxResults:Z

.field private hasSessionId:Z

.field private maxResults_:I

.field private sessionId_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->sessionId_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->desiredResultType_:Ljava/util/List;

    iput v1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->maxResults_:I

    iput-boolean v1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->debug_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientLocale_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientCountryCode_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDesiredResultType(I)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->desiredResultType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->desiredResultType_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->desiredResultType_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->cachedSize:I

    return v0
.end method

.method public getClientCountryCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientCountryCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientId_:Ljava/lang/String;

    return-object v0
.end method

.method public getClientLocale()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientLocale_:Ljava/lang/String;

    return-object v0
.end method

.method public getClientName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientName_:Ljava/lang/String;

    return-object v0
.end method

.method public getClientVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getDebug()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->debug_:Z

    return v0
.end method

.method public getDesiredResultTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->desiredResultType_:Ljava/util/List;

    return-object v0
.end method

.method public getMaxResults()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->maxResults_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasSessionId()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getSessionId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getDesiredResultTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_1
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getDesiredResultTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasDebug()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getDebug()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientLocale()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientLocale()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientName()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientVersion()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientCountryCode()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientCountryCode()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasMaxResults()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getMaxResults()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientId()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    iput v3, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->cachedSize:I

    return v3
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->sessionId_:Ljava/lang/String;

    return-object v0
.end method

.method public hasClientCountryCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientCountryCode:Z

    return v0
.end method

.method public hasClientId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientId:Z

    return v0
.end method

.method public hasClientLocale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientLocale:Z

    return v0
.end method

.method public hasClientName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientName:Z

    return v0
.end method

.method public hasClientVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientVersion:Z

    return v0
.end method

.method public hasDebug()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasDebug:Z

    return v0
.end method

.method public hasMaxResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasMaxResults:Z

    return v0
.end method

.method public hasSessionId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasSessionId:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setSessionId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->addDesiredResultType(I)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setDebug(Z)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setClientLocale(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setClientName(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setClientVersion(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setClientCountryCode(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setMaxResults(I)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->setClientId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    move-result-object v0

    return-object v0
.end method

.method public setClientCountryCode(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientCountryCode:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientCountryCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientId:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientId_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientLocale(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientLocale:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientLocale_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientName(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientName:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientName_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientVersion(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientVersion:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->clientVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setDebug(Z)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasDebug:Z

    iput-boolean p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->debug_:Z

    return-object p0
.end method

.method public setMaxResults(I)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasMaxResults:Z

    iput p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->maxResults_:I

    return-object p0
.end method

.method public setSessionId(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasSessionId:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->sessionId_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasSessionId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getDesiredResultTypeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasDebug()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getDebug()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientLocale()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientName()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientVersion()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientCountryCode()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasMaxResults()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getMaxResults()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->hasClientId()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    return-void
.end method
