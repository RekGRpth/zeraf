.class public final Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Synthesis.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/speech/s3/Synthesis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TtsServiceRequest"
.end annotation


# instance fields
.field private audioChunkSize_:I

.field private audioEncoding_:I

.field private cachedSize:I

.field private engineSpecificRequest_:Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;

.field private hasAudioChunkSize:Z

.field private hasAudioEncoding:Z

.field private hasEngineSpecificRequest:Z

.field private hasInputIsLoggable:Z

.field private hasSsml:Z

.field private hasSynthesisPitch:Z

.field private hasSynthesisSpeed:Z

.field private hasSynthesisText:Z

.field private hasSynthesisVolume:Z

.field private hasVoiceEngine:Z

.field private hasVoiceLanguage:Z

.field private hasVoiceName:Z

.field private hasVoiceSampleRate:Z

.field private inputIsLoggable_:Z

.field private ssml_:Ljava/lang/String;

.field private synthesisPitch_:D

.field private synthesisSpeed_:D

.field private synthesisText_:Ljava/lang/String;

.field private synthesisVolume_:D

.field private voiceEngine_:Ljava/lang/String;

.field private voiceLanguage_:Ljava/lang/String;

.field private voiceName_:Ljava/lang/String;

.field private voiceSampleRate_:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->ssml_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->engineSpecificRequest_:Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;

    iput-boolean v3, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->inputIsLoggable_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceName_:Ljava/lang/String;

    iput v3, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceSampleRate_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceEngine_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisSpeed_:D

    iput-wide v1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisPitch_:D

    iput-wide v1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisVolume_:D

    iput v3, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->audioEncoding_:I

    const/16 v0, 0x400

    iput v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->audioChunkSize_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAudioChunkSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->audioChunkSize_:I

    return v0
.end method

.method public getAudioEncoding()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->audioEncoding_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->cachedSize:I

    return v0
.end method

.method public getEngineSpecificRequest()Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->engineSpecificRequest_:Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;

    return-object v0
.end method

.method public getInputIsLoggable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->inputIsLoggable_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisText()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSynthesisText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSsml()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSsml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceLanguage()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getVoiceLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceName()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getVoiceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceSampleRate()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getVoiceSampleRate()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceEngine()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getVoiceEngine()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisSpeed()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSynthesisSpeed()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisPitch()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSynthesisPitch()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisVolume()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSynthesisVolume()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasAudioEncoding()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getAudioEncoding()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasAudioChunkSize()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getAudioChunkSize()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasInputIsLoggable()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getInputIsLoggable()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasEngineSpecificRequest()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getEngineSpecificRequest()Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iput v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->cachedSize:I

    return v0
.end method

.method public getSsml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->ssml_:Ljava/lang/String;

    return-object v0
.end method

.method public getSynthesisPitch()D
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisPitch_:D

    return-wide v0
.end method

.method public getSynthesisSpeed()D
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisSpeed_:D

    return-wide v0
.end method

.method public getSynthesisText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisText_:Ljava/lang/String;

    return-object v0
.end method

.method public getSynthesisVolume()D
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisVolume_:D

    return-wide v0
.end method

.method public getVoiceEngine()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceEngine_:Ljava/lang/String;

    return-object v0
.end method

.method public getVoiceLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getVoiceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceName_:Ljava/lang/String;

    return-object v0
.end method

.method public getVoiceSampleRate()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceSampleRate_:I

    return v0
.end method

.method public hasAudioChunkSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasAudioChunkSize:Z

    return v0
.end method

.method public hasAudioEncoding()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasAudioEncoding:Z

    return v0
.end method

.method public hasEngineSpecificRequest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasEngineSpecificRequest:Z

    return v0
.end method

.method public hasInputIsLoggable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasInputIsLoggable:Z

    return v0
.end method

.method public hasSsml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSsml:Z

    return v0
.end method

.method public hasSynthesisPitch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisPitch:Z

    return v0
.end method

.method public hasSynthesisSpeed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisSpeed:Z

    return v0
.end method

.method public hasSynthesisText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisText:Z

    return v0
.end method

.method public hasSynthesisVolume()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisVolume:Z

    return v0
.end method

.method public hasVoiceEngine()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceEngine:Z

    return v0
.end method

.method public hasVoiceLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceLanguage:Z

    return v0
.end method

.method public hasVoiceName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceName:Z

    return v0
.end method

.method public hasVoiceSampleRate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceSampleRate:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setSynthesisText(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setSsml(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setVoiceLanguage(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setVoiceName(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setVoiceSampleRate(I)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setVoiceEngine(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setSynthesisSpeed(D)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setSynthesisPitch(D)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setSynthesisVolume(D)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setAudioEncoding(I)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setAudioChunkSize(I)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setInputIsLoggable(Z)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;

    invoke-direct {v1}, Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->setEngineSpecificRequest(Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x39 -> :sswitch_7
        0x41 -> :sswitch_8
        0x49 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public setAudioChunkSize(I)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasAudioChunkSize:Z

    iput p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->audioChunkSize_:I

    return-object p0
.end method

.method public setAudioEncoding(I)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasAudioEncoding:Z

    iput p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->audioEncoding_:I

    return-object p0
.end method

.method public setEngineSpecificRequest(Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasEngineSpecificRequest:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->engineSpecificRequest_:Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;

    return-object p0
.end method

.method public setInputIsLoggable(Z)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasInputIsLoggable:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->inputIsLoggable_:Z

    return-object p0
.end method

.method public setSsml(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSsml:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->ssml_:Ljava/lang/String;

    return-object p0
.end method

.method public setSynthesisPitch(D)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisPitch:Z

    iput-wide p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisPitch_:D

    return-object p0
.end method

.method public setSynthesisSpeed(D)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisSpeed:Z

    iput-wide p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisSpeed_:D

    return-object p0
.end method

.method public setSynthesisText(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisText:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisText_:Ljava/lang/String;

    return-object p0
.end method

.method public setSynthesisVolume(D)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisVolume:Z

    iput-wide p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->synthesisVolume_:D

    return-object p0
.end method

.method public setVoiceEngine(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceEngine:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceEngine_:Ljava/lang/String;

    return-object p0
.end method

.method public setVoiceLanguage(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceLanguage:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setVoiceName(Ljava/lang/String;)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceName:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceName_:Ljava/lang/String;

    return-object p0
.end method

.method public setVoiceSampleRate(I)Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceSampleRate:Z

    iput p1, p0, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->voiceSampleRate_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisText()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSynthesisText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSsml()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSsml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceLanguage()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getVoiceLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceName()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getVoiceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceSampleRate()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getVoiceSampleRate()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasVoiceEngine()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getVoiceEngine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisSpeed()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSynthesisSpeed()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisPitch()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSynthesisPitch()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasSynthesisVolume()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getSynthesisVolume()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasAudioEncoding()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getAudioEncoding()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasAudioChunkSize()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getAudioChunkSize()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasInputIsLoggable()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getInputIsLoggable()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->hasEngineSpecificRequest()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;->getEngineSpecificRequest()Lcom/google/speech/synthesizer/EngineSpecific$SynthesisEngineSpecificRequest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    return-void
.end method
