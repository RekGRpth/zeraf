.class public final Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Majel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/speech/s3/Majel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MajelServiceEvent"
.end annotation


# instance fields
.field private cachedSize:I

.field private compressedMajelResponse_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private hasCompressedMajelResponse:Z

.field private hasMajel:Z

.field private majel_:Lcom/google/majel/proto/MajelProtos$MajelResponse;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->majel_:Lcom/google/majel/proto/MajelProtos$MajelResponse;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->compressedMajelResponse_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public clearCompressedMajelResponse()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasCompressedMajelResponse:Z

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->compressedMajelResponse_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->cachedSize:I

    return v0
.end method

.method public getCompressedMajelResponse()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->compressedMajelResponse_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getMajel()Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->majel_:Lcom/google/majel/proto/MajelProtos$MajelResponse;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasMajel()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->getMajel()Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasCompressedMajelResponse()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->getCompressedMajelResponse()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->cachedSize:I

    return v0
.end method

.method public hasCompressedMajelResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasCompressedMajelResponse:Z

    return v0
.end method

.method public hasMajel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasMajel:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-direct {v1}, Lcom/google/majel/proto/MajelProtos$MajelResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->setMajel(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->setCompressedMajelResponse(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public setCompressedMajelResponse(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasCompressedMajelResponse:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->compressedMajelResponse_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setMajel(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;
    .locals 1
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasMajel:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->majel_:Lcom/google/majel/proto/MajelProtos$MajelResponse;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasMajel()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->getMajel()Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasCompressedMajelResponse()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->getCompressedMajelResponse()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_1
    return-void
.end method
