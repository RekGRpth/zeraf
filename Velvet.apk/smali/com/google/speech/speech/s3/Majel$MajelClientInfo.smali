.class public final Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Majel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/speech/s3/Majel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MajelClientInfo"
.end annotation


# instance fields
.field private browserParams_:Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

.field private cachedSize:I

.field private capabilities_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private clientName_:Ljava/lang/String;

.field private client_:I

.field private context_:Lcom/google/majel/proto/ContextProtos$Context;

.field private gservicesCountryCode_:Ljava/lang/String;

.field private hasBrowserParams:Z

.field private hasClient:Z

.field private hasClientName:Z

.field private hasContext:Z

.field private hasGservicesCountryCode:Z

.field private hasPreviewParams:Z

.field private hasSafesearchLevel:Z

.field private hasScreenParams:Z

.field private hasSystemTimeMs:Z

.field private hasTimeZone:Z

.field private hasUseCompressedResponse:Z

.field private previewParams_:Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

.field private safesearchLevel_:I

.field private screenParams_:Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

.field private systemTimeMs_:J

.field private timeZone_:Ljava/lang/String;

.field private useCompressedResponse_:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v2, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->useCompressedResponse_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->capabilities_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->previewParams_:Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    iput-object v1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->browserParams_:Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    iput-object v1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->screenParams_:Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    iput-object v1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->context_:Lcom/google/majel/proto/ContextProtos$Context;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->safesearchLevel_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->systemTimeMs_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->timeZone_:Ljava/lang/String;

    iput v2, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->client_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->clientName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->gservicesCountryCode_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->capabilities_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->capabilities_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->capabilities_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBrowserParams()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->browserParams_:Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->cachedSize:I

    return v0
.end method

.method public getCapabilitiesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->capabilities_:Ljava/util/List;

    return-object v0
.end method

.method public getClient()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->client_:I

    return v0
.end method

.method public getClientName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->clientName_:Ljava/lang/String;

    return-object v0
.end method

.method public getContext()Lcom/google/majel/proto/ContextProtos$Context;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->context_:Lcom/google/majel/proto/ContextProtos$Context;

    return-object v0
.end method

.method public getGservicesCountryCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->gservicesCountryCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreviewParams()Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->previewParams_:Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    return-object v0
.end method

.method public getSafesearchLevel()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->safesearchLevel_:I

    return v0
.end method

.method public getScreenParams()Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->screenParams_:Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasUseCompressedResponse()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getUseCompressedResponse()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getCapabilitiesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_1
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getCapabilitiesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasContext()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getContext()Lcom/google/majel/proto/ContextProtos$Context;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasSafesearchLevel()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getSafesearchLevel()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasPreviewParams()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getPreviewParams()Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasBrowserParams()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getBrowserParams()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasSystemTimeMs()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getSystemTimeMs()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasTimeZone()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasScreenParams()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getScreenParams()Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasClient()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getClient()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasClientName()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getClientName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasGservicesCountryCode()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getGservicesCountryCode()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    iput v3, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->cachedSize:I

    return v3
.end method

.method public getSystemTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->systemTimeMs_:J

    return-wide v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->timeZone_:Ljava/lang/String;

    return-object v0
.end method

.method public getUseCompressedResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->useCompressedResponse_:Z

    return v0
.end method

.method public hasBrowserParams()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasBrowserParams:Z

    return v0
.end method

.method public hasClient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasClient:Z

    return v0
.end method

.method public hasClientName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasClientName:Z

    return v0
.end method

.method public hasContext()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasContext:Z

    return v0
.end method

.method public hasGservicesCountryCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasGservicesCountryCode:Z

    return v0
.end method

.method public hasPreviewParams()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasPreviewParams:Z

    return v0
.end method

.method public hasSafesearchLevel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasSafesearchLevel:Z

    return v0
.end method

.method public hasScreenParams()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasScreenParams:Z

    return v0
.end method

.method public hasSystemTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasSystemTimeMs:Z

    return v0
.end method

.method public hasTimeZone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasTimeZone:Z

    return v0
.end method

.method public hasUseCompressedResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasUseCompressedResponse:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setUseCompressedResponse(Z)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/ContextProtos$Context;

    invoke-direct {v1}, Lcom/google/majel/proto/ContextProtos$Context;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setContext(Lcom/google/majel/proto/ContextProtos$Context;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setSafesearchLevel(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    invoke-direct {v1}, Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setPreviewParams(Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-direct {v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setBrowserParams(Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setSystemTimeMs(J)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setTimeZone(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    invoke-direct {v1}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setScreenParams(Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setClient(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setClientName(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setGservicesCountryCode(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public setBrowserParams(Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasBrowserParams:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->browserParams_:Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    return-object p0
.end method

.method public setClient(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasClient:Z

    iput p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->client_:I

    return-object p0
.end method

.method public setClientName(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasClientName:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->clientName_:Ljava/lang/String;

    return-object p0
.end method

.method public setContext(Lcom/google/majel/proto/ContextProtos$Context;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ContextProtos$Context;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasContext:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->context_:Lcom/google/majel/proto/ContextProtos$Context;

    return-object p0
.end method

.method public setGservicesCountryCode(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasGservicesCountryCode:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->gservicesCountryCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setPreviewParams(Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasPreviewParams:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->previewParams_:Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    return-object p0
.end method

.method public setSafesearchLevel(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasSafesearchLevel:Z

    iput p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->safesearchLevel_:I

    return-object p0
.end method

.method public setScreenParams(Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasScreenParams:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->screenParams_:Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    return-object p0
.end method

.method public setSystemTimeMs(J)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasSystemTimeMs:Z

    iput-wide p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->systemTimeMs_:J

    return-object p0
.end method

.method public setTimeZone(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasTimeZone:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->timeZone_:Ljava/lang/String;

    return-object p0
.end method

.method public setUseCompressedResponse(Z)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasUseCompressedResponse:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->useCompressedResponse_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasUseCompressedResponse()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getUseCompressedResponse()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getCapabilitiesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasContext()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getContext()Lcom/google/majel/proto/ContextProtos$Context;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasSafesearchLevel()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getSafesearchLevel()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasPreviewParams()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getPreviewParams()Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasBrowserParams()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getBrowserParams()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasSystemTimeMs()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getSystemTimeMs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasTimeZone()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasScreenParams()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getScreenParams()Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasClient()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getClient()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasClientName()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getClientName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->hasGservicesCountryCode()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->getGservicesCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    return-void
.end method
