.class public final Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientLogRequestProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/ClientLogRequestProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientLogRequest"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasVoiceSearch:Z

.field private voiceSearch_:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->voiceSearch_:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->hasVoiceSearch()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->getVoiceSearch()Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iput v0, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->cachedSize:I

    return v0
.end method

.method public getVoiceSearch()Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->voiceSearch_:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    return-object v0
.end method

.method public hasVoiceSearch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->hasVoiceSearch:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->setVoiceSearch(Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;)Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public setVoiceSearch(Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;)Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->hasVoiceSearch:Z

    iput-object p1, p0, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->voiceSearch_:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->hasVoiceSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->getVoiceSearch()Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    return-void
.end method
