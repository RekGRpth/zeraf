.class public final Lcom/google/speech/s3/S3$S3ClientInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "S3.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/S3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "S3ClientInfo"
.end annotation


# instance fields
.field private applicationId_:Ljava/lang/String;

.field private applicationVersion_:Ljava/lang/String;

.field private cachedSize:I

.field private clientId_:Ljava/lang/String;

.field private deviceDisplayDensityDpi_:I

.field private deviceDisplayHeightPixels_:I

.field private deviceDisplayWidthPixels_:I

.field private deviceModel_:Ljava/lang/String;

.field private experimentId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasApplicationId:Z

.field private hasApplicationVersion:Z

.field private hasClientId:Z

.field private hasDeviceDisplayDensityDpi:Z

.field private hasDeviceDisplayHeightPixels:Z

.field private hasDeviceDisplayWidthPixels:Z

.field private hasDeviceModel:Z

.field private hasPlatformId:Z

.field private hasPlatformVersion:Z

.field private hasTriggerApplicationId:Z

.field private hasUserAgent:Z

.field private noiseSuppressorId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private platformId_:Ljava/lang/String;

.field private platformVersion_:Ljava/lang/String;

.field private triggerApplicationId_:Ljava/lang/String;

.field private userAgent_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->clientId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->userAgent_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->experimentId_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->platformId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->platformVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->applicationId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->applicationVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceModel_:Ljava/lang/String;

    iput v1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayWidthPixels_:I

    iput v1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayHeightPixels_:I

    iput v1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayDensityDpi_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->noiseSuppressorId_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->triggerApplicationId_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addExperimentId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->experimentId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->experimentId_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->experimentId_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addNoiseSuppressorId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->noiseSuppressorId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->noiseSuppressorId_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->noiseSuppressorId_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getApplicationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->applicationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getApplicationVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->applicationVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->cachedSize:I

    return v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->clientId_:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceDisplayDensityDpi()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayDensityDpi_:I

    return v0
.end method

.method public getDeviceDisplayHeightPixels()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayHeightPixels_:I

    return v0
.end method

.method public getDeviceDisplayWidthPixels()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayWidthPixels_:I

    return v0
.end method

.method public getDeviceModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceModel_:Ljava/lang/String;

    return-object v0
.end method

.method public getExperimentIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->experimentId_:Ljava/util/List;

    return-object v0
.end method

.method public getNoiseSuppressorIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->noiseSuppressorId_:Ljava/util/List;

    return-object v0
.end method

.method public getPlatformId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->platformId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPlatformVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->platformVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasClientId()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getClientId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasApplicationId()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getApplicationId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasUserAgent()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getUserAgent()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getExperimentIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_3
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getExperimentIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasPlatformId()Z

    move-result v4

    if-eqz v4, :cond_4

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getPlatformId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasPlatformVersion()Z

    move-result v4

    if-eqz v4, :cond_5

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getPlatformVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasApplicationVersion()Z

    move-result v4

    if-eqz v4, :cond_6

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getApplicationVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceModel()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getDeviceModel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayWidthPixels()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getDeviceDisplayWidthPixels()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayHeightPixels()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getDeviceDisplayHeightPixels()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayDensityDpi()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getDeviceDisplayDensityDpi()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getNoiseSuppressorIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_b
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getNoiseSuppressorIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasTriggerApplicationId()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getTriggerApplicationId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    iput v3, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->cachedSize:I

    return v3
.end method

.method public getTriggerApplicationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->triggerApplicationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->userAgent_:Ljava/lang/String;

    return-object v0
.end method

.method public hasApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasApplicationId:Z

    return v0
.end method

.method public hasApplicationVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasApplicationVersion:Z

    return v0
.end method

.method public hasClientId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasClientId:Z

    return v0
.end method

.method public hasDeviceDisplayDensityDpi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayDensityDpi:Z

    return v0
.end method

.method public hasDeviceDisplayHeightPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayHeightPixels:Z

    return v0
.end method

.method public hasDeviceDisplayWidthPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayWidthPixels:Z

    return v0
.end method

.method public hasDeviceModel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceModel:Z

    return v0
.end method

.method public hasPlatformId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasPlatformId:Z

    return v0
.end method

.method public hasPlatformVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasPlatformVersion:Z

    return v0
.end method

.method public hasTriggerApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasTriggerApplicationId:Z

    return v0
.end method

.method public hasUserAgent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasUserAgent:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/S3$S3ClientInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3ClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/S3$S3ClientInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setClientId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setApplicationId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setUserAgent(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->addExperimentId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setPlatformId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setPlatformVersion(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setApplicationVersion(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setDeviceModel(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setDeviceDisplayWidthPixels(I)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setDeviceDisplayHeightPixels(I)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setDeviceDisplayDensityDpi(I)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->addNoiseSuppressorId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3ClientInfo;->setTriggerApplicationId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x42 -> :sswitch_5
        0x4a -> :sswitch_6
        0x52 -> :sswitch_7
        0x5a -> :sswitch_8
        0x60 -> :sswitch_9
        0x68 -> :sswitch_a
        0x70 -> :sswitch_b
        0x7a -> :sswitch_c
        0x82 -> :sswitch_d
    .end sparse-switch
.end method

.method public setApplicationId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasApplicationId:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->applicationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setApplicationVersion(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasApplicationVersion:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->applicationVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasClientId:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->clientId_:Ljava/lang/String;

    return-object p0
.end method

.method public setDeviceDisplayDensityDpi(I)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayDensityDpi:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayDensityDpi_:I

    return-object p0
.end method

.method public setDeviceDisplayHeightPixels(I)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayHeightPixels:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayHeightPixels_:I

    return-object p0
.end method

.method public setDeviceDisplayWidthPixels(I)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayWidthPixels:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceDisplayWidthPixels_:I

    return-object p0
.end method

.method public setDeviceModel(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceModel:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->deviceModel_:Ljava/lang/String;

    return-object p0
.end method

.method public setPlatformId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasPlatformId:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->platformId_:Ljava/lang/String;

    return-object p0
.end method

.method public setPlatformVersion(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasPlatformVersion:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->platformVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setTriggerApplicationId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasTriggerApplicationId:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->triggerApplicationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setUserAgent(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->hasUserAgent:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3ClientInfo;->userAgent_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasClientId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasApplicationId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getApplicationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasUserAgent()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getUserAgent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getExperimentIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasPlatformId()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getPlatformId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasPlatformVersion()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getPlatformVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasApplicationVersion()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getApplicationVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceModel()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getDeviceModel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayWidthPixels()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getDeviceDisplayWidthPixels()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayHeightPixels()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getDeviceDisplayHeightPixels()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasDeviceDisplayDensityDpi()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getDeviceDisplayDensityDpi()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getNoiseSuppressorIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_1

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->hasTriggerApplicationId()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3ClientInfo;->getTriggerApplicationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    return-void
.end method
