.class public final Lcom/google/speech/s3/S3$S3UserInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "S3.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/S3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "S3UserInfo"
.end annotation


# instance fields
.field private adaptationId_:Ljava/lang/String;

.field private authToken_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/s3/S3$AuthToken;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private hasAdaptationId:Z

.field private hasInstallId:Z

.field private hasLatLong:Z

.field private hasSpokenLanguage:Z

.field private hasTrustedDescriptors:Z

.field private hasUsePreciseGeolocation:Z

.field private hasUserLocale:Z

.field private hasXGeoLocation:Z

.field private installId_:Ljava/lang/String;

.field private latLong_:Ljava/lang/String;

.field private spokenLanguage_:Lcom/google/speech/s3/S3$Locale;

.field private trustedDescriptors_:Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;

.field private usePreciseGeolocation_:Z

.field private userLocale_:Lcom/google/speech/s3/S3$Locale;

.field private xGeoLocation_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->authToken_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->spokenLanguage_:Lcom/google/speech/s3/S3$Locale;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->userLocale_:Lcom/google/speech/s3/S3$Locale;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->installId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->adaptationId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->latLong_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->xGeoLocation_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->trustedDescriptors_:Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->usePreciseGeolocation_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAuthToken(Lcom/google/speech/s3/S3$AuthToken;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$AuthToken;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->authToken_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->authToken_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->authToken_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public clearAuthToken()Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->authToken_:Ljava/util/List;

    return-object p0
.end method

.method public getAdaptationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->adaptationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthTokenList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/s3/S3$AuthToken;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->authToken_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->cachedSize:I

    return v0
.end method

.method public getInstallId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->installId_:Ljava/lang/String;

    return-object v0
.end method

.method public getLatLong()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->latLong_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasSpokenLanguage()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getSpokenLanguage()Lcom/google/speech/s3/S3$Locale;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasUserLocale()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getUserLocale()Lcom/google/speech/s3/S3$Locale;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasInstallId()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getInstallId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasLatLong()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getLatLong()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasXGeoLocation()Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getXGeoLocation()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getAuthTokenList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/s3/S3$AuthToken;

    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasTrustedDescriptors()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getTrustedDescriptors()Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasAdaptationId()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getAdaptationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasUsePreciseGeolocation()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getUsePreciseGeolocation()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    iput v2, p0, Lcom/google/speech/s3/S3$S3UserInfo;->cachedSize:I

    return v2
.end method

.method public getSpokenLanguage()Lcom/google/speech/s3/S3$Locale;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->spokenLanguage_:Lcom/google/speech/s3/S3$Locale;

    return-object v0
.end method

.method public getTrustedDescriptors()Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->trustedDescriptors_:Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;

    return-object v0
.end method

.method public getUsePreciseGeolocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->usePreciseGeolocation_:Z

    return v0
.end method

.method public getUserLocale()Lcom/google/speech/s3/S3$Locale;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->userLocale_:Lcom/google/speech/s3/S3$Locale;

    return-object v0
.end method

.method public getXGeoLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->xGeoLocation_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAdaptationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasAdaptationId:Z

    return v0
.end method

.method public hasInstallId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasInstallId:Z

    return v0
.end method

.method public hasLatLong()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasLatLong:Z

    return v0
.end method

.method public hasSpokenLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasSpokenLanguage:Z

    return v0
.end method

.method public hasTrustedDescriptors()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasTrustedDescriptors:Z

    return v0
.end method

.method public hasUsePreciseGeolocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasUsePreciseGeolocation:Z

    return v0
.end method

.method public hasUserLocale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasUserLocale:Z

    return v0
.end method

.method public hasXGeoLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasXGeoLocation:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/S3$S3UserInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3UserInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/S3$S3UserInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/speech/s3/S3$Locale;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$Locale;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3UserInfo;->setSpokenLanguage(Lcom/google/speech/s3/S3$Locale;)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/speech/s3/S3$Locale;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$Locale;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3UserInfo;->setUserLocale(Lcom/google/speech/s3/S3$Locale;)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3UserInfo;->setInstallId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3UserInfo;->setLatLong(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3UserInfo;->setXGeoLocation(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/speech/s3/S3$AuthToken;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$AuthToken;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3UserInfo;->addAuthToken(Lcom/google/speech/s3/S3$AuthToken;)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_7
    new-instance v1, Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;

    invoke-direct {v1}, Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3UserInfo;->setTrustedDescriptors(Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3UserInfo;->setAdaptationId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3UserInfo;->setUsePreciseGeolocation(Z)Lcom/google/speech/s3/S3$S3UserInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x42 -> :sswitch_5
        0x4a -> :sswitch_6
        0x5a -> :sswitch_7
        0x62 -> :sswitch_8
        0x68 -> :sswitch_9
    .end sparse-switch
.end method

.method public setAdaptationId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasAdaptationId:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->adaptationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setInstallId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasInstallId:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->installId_:Ljava/lang/String;

    return-object p0
.end method

.method public setLatLong(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasLatLong:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->latLong_:Ljava/lang/String;

    return-object p0
.end method

.method public setSpokenLanguage(Lcom/google/speech/s3/S3$Locale;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$Locale;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasSpokenLanguage:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->spokenLanguage_:Lcom/google/speech/s3/S3$Locale;

    return-object p0
.end method

.method public setTrustedDescriptors(Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasTrustedDescriptors:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->trustedDescriptors_:Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;

    return-object p0
.end method

.method public setUsePreciseGeolocation(Z)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasUsePreciseGeolocation:Z

    iput-boolean p1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->usePreciseGeolocation_:Z

    return-object p0
.end method

.method public setUserLocale(Lcom/google/speech/s3/S3$Locale;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$Locale;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasUserLocale:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->userLocale_:Lcom/google/speech/s3/S3$Locale;

    return-object p0
.end method

.method public setXGeoLocation(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3UserInfo;->hasXGeoLocation:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3UserInfo;->xGeoLocation_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasSpokenLanguage()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getSpokenLanguage()Lcom/google/speech/s3/S3$Locale;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasUserLocale()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getUserLocale()Lcom/google/speech/s3/S3$Locale;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasInstallId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getInstallId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasLatLong()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getLatLong()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasXGeoLocation()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getXGeoLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getAuthTokenList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/s3/S3$AuthToken;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasTrustedDescriptors()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getTrustedDescriptors()Llocation/unified/LocationDescriptorProto$LocationDescriptorSet;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasAdaptationId()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getAdaptationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->hasUsePreciseGeolocation()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3UserInfo;->getUsePreciseGeolocation()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    return-void
.end method
