.class public final Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "VoicesearchClientLogProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/logs/VoicesearchClientLogProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlternateCorrectionData"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasLength:Z

.field private hasNewText:Z

.field private hasOldText:Z

.field private hasRecognizerSegmentIndex:Z

.field private hasStart:Z

.field private hasUnit:Z

.field private length_:I

.field private newText_:Ljava/lang/String;

.field private oldText_:Ljava/lang/String;

.field private recognizerSegmentIndex_:I

.field private start_:I

.field private unit_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->recognizerSegmentIndex_:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->unit_:I

    iput v1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->start_:I

    iput v1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->length_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->oldText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->newText_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->cachedSize:I

    return v0
.end method

.method public getLength()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->length_:I

    return v0
.end method

.method public getNewText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->newText_:Ljava/lang/String;

    return-object v0
.end method

.method public getOldText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->oldText_:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognizerSegmentIndex()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->recognizerSegmentIndex_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasRecognizerSegmentIndex()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getRecognizerSegmentIndex()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasUnit()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getUnit()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasStart()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getStart()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasLength()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getLength()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasOldText()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getOldText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasNewText()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getNewText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->cachedSize:I

    return v0
.end method

.method public getStart()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->start_:I

    return v0
.end method

.method public getUnit()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->unit_:I

    return v0
.end method

.method public hasLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasLength:Z

    return v0
.end method

.method public hasNewText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasNewText:Z

    return v0
.end method

.method public hasOldText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasOldText:Z

    return v0
.end method

.method public hasRecognizerSegmentIndex()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasRecognizerSegmentIndex:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasStart:Z

    return v0
.end method

.method public hasUnit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasUnit:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setRecognizerSegmentIndex(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setUnit(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setStart(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setLength(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setOldText(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setNewText(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public setLength(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasLength:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->length_:I

    return-object p0
.end method

.method public setNewText(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasNewText:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->newText_:Ljava/lang/String;

    return-object p0
.end method

.method public setOldText(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasOldText:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->oldText_:Ljava/lang/String;

    return-object p0
.end method

.method public setRecognizerSegmentIndex(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasRecognizerSegmentIndex:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->recognizerSegmentIndex_:I

    return-object p0
.end method

.method public setStart(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasStart:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->start_:I

    return-object p0
.end method

.method public setUnit(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasUnit:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->unit_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasRecognizerSegmentIndex()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getRecognizerSegmentIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasUnit()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getUnit()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getStart()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasLength()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasOldText()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getOldText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->hasNewText()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->getNewText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    return-void
.end method
