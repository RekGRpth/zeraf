.class public final Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/api/RecognizerProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Hypothesis"
.end annotation


# instance fields
.field private accept_:Z

.field private alternates_:Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

.field private cachedSize:I

.field private confidence_:F

.field private hasAccept:Z

.field private hasAlternates:Z

.field private hasConfidence:Z

.field private hasPhoneAlign:Z

.field private hasPrenormText:Z

.field private hasScrubbedText:Z

.field private hasSemanticResult:Z

.field private hasStateAlign:Z

.field private hasText:Z

.field private hasWordAlign:Z

.field private phoneAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

.field private prenormText_:Ljava/lang/String;

.field private scrubbedText_:Ljava/lang/String;

.field private semanticResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

.field private stateAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

.field private text_:Ljava/lang/String;

.field private wordAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

.field private wordConfFeature_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->text_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->prenormText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->scrubbedText_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->semanticResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->confidence_:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->accept_:Z

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->stateAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->phoneAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->wordAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->wordConfFeature_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->alternates_:Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addWordConfFeature(Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->wordConfFeature_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->wordConfFeature_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->wordConfFeature_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAccept()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->accept_:Z

    return v0
.end method

.method public getAlternates()Lcom/google/speech/common/Alternates$RecognitionClientAlternates;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->alternates_:Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->cachedSize:I

    return v0
.end method

.method public getConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->confidence_:F

    return v0
.end method

.method public getPhoneAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->phoneAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object v0
.end method

.method public getPrenormText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->prenormText_:Ljava/lang/String;

    return-object v0
.end method

.method public getScrubbedText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->scrubbedText_:Ljava/lang/String;

    return-object v0
.end method

.method public getSemanticResult()Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->semanticResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasText()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasConfidence()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getConfidence()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasPhoneAlign()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getPhoneAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasWordAlign()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getWordAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getWordConfFeatureList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAlternates()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getAlternates()Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasSemanticResult()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getSemanticResult()Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasStateAlign()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getStateAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAccept()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getAccept()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasPrenormText()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getPrenormText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasScrubbedText()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getScrubbedText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    iput v2, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->cachedSize:I

    return v2
.end method

.method public getStateAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->stateAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public getWordAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->wordAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object v0
.end method

.method public getWordConfFeatureList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->wordConfFeature_:Ljava/util/List;

    return-object v0
.end method

.method public hasAccept()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAccept:Z

    return v0
.end method

.method public hasAlternates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAlternates:Z

    return v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasConfidence:Z

    return v0
.end method

.method public hasPhoneAlign()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasPhoneAlign:Z

    return v0
.end method

.method public hasPrenormText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasPrenormText:Z

    return v0
.end method

.method public hasScrubbedText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasScrubbedText:Z

    return v0
.end method

.method public hasSemanticResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasSemanticResult:Z

    return v0
.end method

.method public hasStateAlign()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasStateAlign:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasText:Z

    return v0
.end method

.method public hasWordAlign()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasWordAlign:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setConfidence(F)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    invoke-direct {v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setPhoneAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    invoke-direct {v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setWordAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    invoke-direct {v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->addWordConfFeature(Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    invoke-direct {v1}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setAlternates(Lcom/google/speech/common/Alternates$RecognitionClientAlternates;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setSemanticResult(Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    invoke-direct {v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setStateAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setAccept(Z)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setPrenormText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setScrubbedText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x52 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
    .end sparse-switch
.end method

.method public setAccept(Z)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAccept:Z

    iput-boolean p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->accept_:Z

    return-object p0
.end method

.method public setAlternates(Lcom/google/speech/common/Alternates$RecognitionClientAlternates;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAlternates:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->alternates_:Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    return-object p0
.end method

.method public setConfidence(F)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasConfidence:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->confidence_:F

    return-object p0
.end method

.method public setPhoneAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasPhoneAlign:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->phoneAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object p0
.end method

.method public setPrenormText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasPrenormText:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->prenormText_:Ljava/lang/String;

    return-object p0
.end method

.method public setScrubbedText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasScrubbedText:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->scrubbedText_:Ljava/lang/String;

    return-object p0
.end method

.method public setSemanticResult(Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasSemanticResult:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->semanticResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    return-object p0
.end method

.method public setStateAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasStateAlign:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->stateAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasText:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->text_:Ljava/lang/String;

    return-object p0
.end method

.method public setWordAlign(Lcom/google/speech/decoder/common/Alignment$AlignmentProto;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasWordAlign:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->wordAlign_:Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasText()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasConfidence()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getConfidence()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasPhoneAlign()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getPhoneAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasWordAlign()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getWordAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getWordConfFeatureList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAlternates()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getAlternates()Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasSemanticResult()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getSemanticResult()Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasStateAlign()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getStateAlign()Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAccept()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getAccept()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasPrenormText()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getPrenormText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasScrubbedText()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getScrubbedText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    return-void
.end method
