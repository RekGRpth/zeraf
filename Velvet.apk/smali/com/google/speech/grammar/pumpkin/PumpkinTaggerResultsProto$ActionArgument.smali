.class public final Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PumpkinTaggerResultsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActionArgument"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasName:Z

.field private hasScore:Z

.field private hasType:Z

.field private hasUnnormalizedValue:Z

.field private hasUserType:Z

.field private hasValue:Z

.field private name_:Ljava/lang/String;

.field private score_:F

.field private type_:I

.field private unnormalizedValue_:Ljava/lang/String;

.field private userType_:Ljava/lang/String;

.field private value_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->name_:Ljava/lang/String;

    iput v1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->type_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->userType_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->score_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->value_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->unnormalizedValue_:Ljava/lang/String;

    iput v1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->cachedSize:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->score_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasType()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasUserType()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getUserType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasScore()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getScore()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasUnnormalizedValue()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getUnnormalizedValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->cachedSize:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->type_:I

    return v0
.end method

.method public getUnnormalizedValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->unnormalizedValue_:Ljava/lang/String;

    return-object v0
.end method

.method public getUserType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->userType_:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->value_:Ljava/lang/String;

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasName:Z

    return v0
.end method

.method public hasScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasScore:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasType:Z

    return v0
.end method

.method public hasUnnormalizedValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasUnnormalizedValue:Z

    return v0
.end method

.method public hasUserType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasUserType:Z

    return v0
.end method

.method public hasValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasValue:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->setName(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->setType(I)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->setUserType(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->setScore(F)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->setValue(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->setUnnormalizedValue(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasName:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setScore(F)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasScore:Z

    iput p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->score_:F

    return-object p0
.end method

.method public setType(I)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasType:Z

    iput p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->type_:I

    return-object p0
.end method

.method public setUnnormalizedValue(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasUnnormalizedValue:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->unnormalizedValue_:Ljava/lang/String;

    return-object p0
.end method

.method public setUserType(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasUserType:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->userType_:Ljava/lang/String;

    return-object p0
.end method

.method public setValue(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasValue:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->value_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasType()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasUserType()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getUserType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasScore()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getScore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->hasUnnormalizedValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$ActionArgument;->getUnnormalizedValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    return-void
.end method
