.class public final Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PumpkinConfigProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PumpkinConfig"
.end annotation


# instance fields
.field private cachedSize:I

.field private canonicalizeToFullContacts_:Z

.field private decoderConfig_:Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

.field private hasCanonicalizeToFullContacts:Z

.field private hasDecoderConfig:Z

.field private hasInsErrScore:Z

.field private hasLanguage:Z

.field private hasValidatorManagerConfig:Z

.field private insErrScore_:F

.field private language_:Ljava/lang/String;

.field private validatorManagerConfig_:Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->language_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->validatorManagerConfig_:Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    iput-object v1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->decoderConfig_:Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->insErrScore_:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->canonicalizeToFullContacts_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->cachedSize:I

    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    invoke-direct {v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->cachedSize:I

    return v0
.end method

.method public getCanonicalizeToFullContacts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->canonicalizeToFullContacts_:Z

    return v0
.end method

.method public getDecoderConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->decoderConfig_:Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    return-object v0
.end method

.method public getInsErrScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->insErrScore_:F

    return v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->language_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasLanguage()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasValidatorManagerConfig()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getValidatorManagerConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasDecoderConfig()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getDecoderConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasInsErrScore()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getInsErrScore()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasCanonicalizeToFullContacts()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getCanonicalizeToFullContacts()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->cachedSize:I

    return v0
.end method

.method public getValidatorManagerConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->validatorManagerConfig_:Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    return-object v0
.end method

.method public hasCanonicalizeToFullContacts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasCanonicalizeToFullContacts:Z

    return v0
.end method

.method public hasDecoderConfig()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasDecoderConfig:Z

    return v0
.end method

.method public hasInsErrScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasInsErrScore:Z

    return v0
.end method

.method public hasLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasLanguage:Z

    return v0
.end method

.method public hasValidatorManagerConfig()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasValidatorManagerConfig:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->setLanguage(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    invoke-direct {v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->setValidatorManagerConfig(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    invoke-direct {v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->setDecoderConfig(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->setInsErrScore(F)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->setCanonicalizeToFullContacts(Z)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public setCanonicalizeToFullContacts(Z)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasCanonicalizeToFullContacts:Z

    iput-boolean p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->canonicalizeToFullContacts_:Z

    return-object p0
.end method

.method public setDecoderConfig(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .locals 1
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasDecoderConfig:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->decoderConfig_:Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    return-object p0
.end method

.method public setInsErrScore(F)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasInsErrScore:Z

    iput p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->insErrScore_:F

    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasLanguage:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->language_:Ljava/lang/String;

    return-object p0
.end method

.method public setValidatorManagerConfig(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .locals 1
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasValidatorManagerConfig:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->validatorManagerConfig_:Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasLanguage()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasValidatorManagerConfig()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getValidatorManagerConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasDecoderConfig()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getDecoderConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasInsErrScore()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getInsErrScore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->hasCanonicalizeToFullContacts()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getCanonicalizeToFullContacts()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_4
    return-void
.end method
