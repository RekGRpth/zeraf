.class public final Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PumpkinConfigProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ValidatorManagerConfig"
.end annotation


# instance fields
.field private atInEmails_:Ljava/lang/String;

.field private cachedSize:I

.field private contactKeyWords_:Ljava/lang/String;

.field private dotInEmails_:Ljava/lang/String;

.field private dotInUrls_:Ljava/lang/String;

.field private grammar_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;",
            ">;"
        }
    .end annotation
.end field

.field private hasAtInEmails:Z

.field private hasContactKeyWords:Z

.field private hasDotInEmails:Z

.field private hasDotInUrls:Z

.field private hasResourcePath:Z

.field private resourcePath_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->resourcePath_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->contactKeyWords_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->dotInEmails_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->atInEmails_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->dotInUrls_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->grammar_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addGrammar(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
    .locals 1
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->grammar_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->grammar_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->grammar_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAtInEmails()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->atInEmails_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->cachedSize:I

    return v0
.end method

.method public getContactKeyWords()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->contactKeyWords_:Ljava/lang/String;

    return-object v0
.end method

.method public getDotInEmails()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->dotInEmails_:Ljava/lang/String;

    return-object v0
.end method

.method public getDotInUrls()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->dotInUrls_:Ljava/lang/String;

    return-object v0
.end method

.method public getGrammarList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->grammar_:Ljava/util/List;

    return-object v0
.end method

.method public getResourcePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->resourcePath_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasResourcePath()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getResourcePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasContactKeyWords()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getContactKeyWords()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasDotInEmails()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getDotInEmails()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasAtInEmails()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getAtInEmails()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasDotInUrls()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getDotInUrls()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getGrammarList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_5
    iput v2, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->cachedSize:I

    return v2
.end method

.method public hasAtInEmails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasAtInEmails:Z

    return v0
.end method

.method public hasContactKeyWords()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasContactKeyWords:Z

    return v0
.end method

.method public hasDotInEmails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasDotInEmails:Z

    return v0
.end method

.method public hasDotInUrls()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasDotInUrls:Z

    return v0
.end method

.method public hasResourcePath()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasResourcePath:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->setResourcePath(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->setContactKeyWords(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->setDotInEmails(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->setAtInEmails(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->setDotInUrls(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    invoke-direct {v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->addGrammar(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public setAtInEmails(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasAtInEmails:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->atInEmails_:Ljava/lang/String;

    return-object p0
.end method

.method public setContactKeyWords(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasContactKeyWords:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->contactKeyWords_:Ljava/lang/String;

    return-object p0
.end method

.method public setDotInEmails(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasDotInEmails:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->dotInEmails_:Ljava/lang/String;

    return-object p0
.end method

.method public setDotInUrls(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasDotInUrls:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->dotInUrls_:Ljava/lang/String;

    return-object p0
.end method

.method public setResourcePath(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasResourcePath:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->resourcePath_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasResourcePath()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getResourcePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasContactKeyWords()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getContactKeyWords()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasDotInEmails()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getDotInEmails()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasAtInEmails()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getAtInEmails()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->hasDotInUrls()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getDotInUrls()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getGrammarList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_5
    return-void
.end method
