.class public final Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PumpkinConfigProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DecoderConfig"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasNumOfActiveStates:Z

.field private hasOnTheFlyComposition:Z

.field private hasRetainOnlyBestHypotheses:Z

.field private numOfActiveStates_:I

.field private onTheFlyComposition_:Z

.field private retainOnlyBestHypotheses_:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/16 v0, 0x32

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->numOfActiveStates_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->onTheFlyComposition_:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->retainOnlyBestHypotheses_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->cachedSize:I

    return v0
.end method

.method public getNumOfActiveStates()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->numOfActiveStates_:I

    return v0
.end method

.method public getOnTheFlyComposition()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->onTheFlyComposition_:Z

    return v0
.end method

.method public getRetainOnlyBestHypotheses()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->retainOnlyBestHypotheses_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasNumOfActiveStates()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->getNumOfActiveStates()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasOnTheFlyComposition()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->getOnTheFlyComposition()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasRetainOnlyBestHypotheses()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->getRetainOnlyBestHypotheses()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->cachedSize:I

    return v0
.end method

.method public hasNumOfActiveStates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasNumOfActiveStates:Z

    return v0
.end method

.method public hasOnTheFlyComposition()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasOnTheFlyComposition:Z

    return v0
.end method

.method public hasRetainOnlyBestHypotheses()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasRetainOnlyBestHypotheses:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->setNumOfActiveStates(I)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->setOnTheFlyComposition(Z)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->setRetainOnlyBestHypotheses(Z)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public setNumOfActiveStates(I)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasNumOfActiveStates:Z

    iput p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->numOfActiveStates_:I

    return-object p0
.end method

.method public setOnTheFlyComposition(Z)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasOnTheFlyComposition:Z

    iput-boolean p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->onTheFlyComposition_:Z

    return-object p0
.end method

.method public setRetainOnlyBestHypotheses(Z)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasRetainOnlyBestHypotheses:Z

    iput-boolean p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->retainOnlyBestHypotheses_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasNumOfActiveStates()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->getNumOfActiveStates()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasOnTheFlyComposition()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->getOnTheFlyComposition()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->hasRetainOnlyBestHypotheses()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$DecoderConfig;->getRetainOnlyBestHypotheses()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    return-void
.end method
