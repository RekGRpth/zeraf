.class public final Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Alignment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Segment"
.end annotation


# instance fields
.field private amCost_:F

.field private cachedSize:I

.field private confidence_:F

.field private cost_:F

.field private end_:J

.field private hasAmCost:Z

.field private hasConfidence:Z

.field private hasCost:Z

.field private hasEnd:Z

.field private hasIlabel:Z

.field private hasIlabelStr:Z

.field private hasLmCost:Z

.field private hasOlabel:Z

.field private hasOlabelStr:Z

.field private hasStart:Z

.field private ilabelStr_:Ljava/lang/String;

.field private ilabel_:J

.field private lmCost_:F

.field private olabelStr_:Ljava/lang/String;

.field private olabel_:J

.field private start_:J

.field private states_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v2, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->start_:J

    iput-wide v2, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->end_:J

    iput v1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->cost_:F

    iput v1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->amCost_:F

    iput v1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->lmCost_:F

    iput-wide v2, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->ilabel_:J

    iput-wide v2, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->olabel_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->ilabelStr_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->olabelStr_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->states_:Ljava/util/List;

    iput v1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->confidence_:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addStates(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->states_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->states_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->states_:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAmCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->amCost_:F

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->cachedSize:I

    return v0
.end method

.method public getConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->confidence_:F

    return v0
.end method

.method public getCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->cost_:F

    return v0
.end method

.method public getEnd()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->end_:J

    return-wide v0
.end method

.method public getIlabel()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->ilabel_:J

    return-wide v0
.end method

.method public getIlabelStr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->ilabelStr_:Ljava/lang/String;

    return-object v0
.end method

.method public getLmCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->lmCost_:F

    return v0
.end method

.method public getOlabel()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->olabel_:J

    return-wide v0
.end method

.method public getOlabelStr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->olabelStr_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasStart()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getStart()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasEnd()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getEnd()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasCost()Z

    move-result v5

    if-eqz v5, :cond_2

    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getCost()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v4, v5

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasAmCost()Z

    move-result v5

    if-eqz v5, :cond_3

    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getAmCost()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v4, v5

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasLmCost()Z

    move-result v5

    if-eqz v5, :cond_4

    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getLmCost()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v4, v5

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasIlabel()Z

    move-result v5

    if-eqz v5, :cond_5

    const/16 v5, 0xb

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getIlabel()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasOlabel()Z

    move-result v5

    if-eqz v5, :cond_6

    const/16 v5, 0xc

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getOlabel()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasIlabelStr()Z

    move-result v5

    if-eqz v5, :cond_7

    const/16 v5, 0xd

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getIlabelStr()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasOlabelStr()Z

    move-result v5

    if-eqz v5, :cond_8

    const/16 v5, 0xe

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getOlabelStr()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getStatesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64SizeNoTag(J)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_0

    :cond_9
    add-int/2addr v4, v0

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getStatesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasConfidence()Z

    move-result v5

    if-eqz v5, :cond_a

    const/16 v5, 0x10

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getConfidence()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v4, v5

    :cond_a
    iput v4, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->cachedSize:I

    return v4
.end method

.method public getStart()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->start_:J

    return-wide v0
.end method

.method public getStatesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->states_:Ljava/util/List;

    return-object v0
.end method

.method public hasAmCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasAmCost:Z

    return v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasConfidence:Z

    return v0
.end method

.method public hasCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasCost:Z

    return v0
.end method

.method public hasEnd()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasEnd:Z

    return v0
.end method

.method public hasIlabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasIlabel:Z

    return v0
.end method

.method public hasIlabelStr()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasIlabelStr:Z

    return v0
.end method

.method public hasLmCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasLmCost:Z

    return v0
.end method

.method public hasOlabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasOlabel:Z

    return v0
.end method

.method public hasOlabelStr()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasOlabelStr:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasStart:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setStart(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setEnd(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setAmCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setLmCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setIlabel(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setOlabel(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setIlabelStr(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setOlabelStr(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->addStates(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->setConfidence(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x30 -> :sswitch_1
        0x38 -> :sswitch_2
        0x45 -> :sswitch_3
        0x4d -> :sswitch_4
        0x55 -> :sswitch_5
        0x58 -> :sswitch_6
        0x60 -> :sswitch_7
        0x6a -> :sswitch_8
        0x72 -> :sswitch_9
        0x78 -> :sswitch_a
        0x85 -> :sswitch_b
    .end sparse-switch
.end method

.method public setAmCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasAmCost:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->amCost_:F

    return-object p0
.end method

.method public setConfidence(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasConfidence:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->confidence_:F

    return-object p0
.end method

.method public setCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasCost:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->cost_:F

    return-object p0
.end method

.method public setEnd(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasEnd:Z

    iput-wide p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->end_:J

    return-object p0
.end method

.method public setIlabel(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasIlabel:Z

    iput-wide p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->ilabel_:J

    return-object p0
.end method

.method public setIlabelStr(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasIlabelStr:Z

    iput-object p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->ilabelStr_:Ljava/lang/String;

    return-object p0
.end method

.method public setLmCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasLmCost:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->lmCost_:F

    return-object p0
.end method

.method public setOlabel(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasOlabel:Z

    iput-wide p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->olabel_:J

    return-object p0
.end method

.method public setOlabelStr(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasOlabelStr:Z

    iput-object p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->olabelStr_:Ljava/lang/String;

    return-object p0
.end method

.method public setStart(J)Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasStart:Z

    iput-wide p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->start_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 6
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasStart()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getStart()J

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasEnd()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getEnd()J

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasCost()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getCost()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasAmCost()Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getAmCost()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasLmCost()Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getLmCost()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasIlabel()Z

    move-result v3

    if-eqz v3, :cond_5

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getIlabel()J

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasOlabel()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getOlabel()J

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasIlabelStr()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getIlabelStr()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasOlabelStr()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getOlabelStr()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getStatesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/16 v3, 0xf

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->hasConfidence()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;->getConfidence()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_a
    return-void
.end method
