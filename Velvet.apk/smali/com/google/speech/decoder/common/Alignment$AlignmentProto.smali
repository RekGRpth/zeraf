.class public final Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Alignment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/decoder/common/Alignment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlignmentProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;
    }
.end annotation


# instance fields
.field private amCost_:F

.field private cachedSize:I

.field private frameDuration_:F

.field private hasAmCost:Z

.field private hasFrameDuration:Z

.field private hasItableChecksum:Z

.field private hasItableFilename:Z

.field private hasLmCost:Z

.field private hasOtableChecksum:Z

.field private hasOtableFilename:Z

.field private hasTotalCost:Z

.field private hasWordLabelPosition:Z

.field private itableChecksum_:Ljava/lang/String;

.field private itableFilename_:Ljava/lang/String;

.field private lmCost_:F

.field private otableChecksum_:Ljava/lang/String;

.field private otableFilename_:Ljava/lang/String;

.field private segment_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;",
            ">;"
        }
    .end annotation
.end field

.field private totalCost_:F

.field private wordLabelPosition_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->frameDuration_:F

    iput v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->totalCost_:F

    iput v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->amCost_:F

    iput v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->lmCost_:F

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->segment_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->itableChecksum_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->itableFilename_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->otableChecksum_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->otableFilename_:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->wordLabelPosition_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addSegment(Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->segment_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->segment_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->segment_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAmCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->amCost_:F

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->cachedSize:I

    return v0
.end method

.method public getFrameDuration()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->frameDuration_:F

    return v0
.end method

.method public getItableChecksum()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->itableChecksum_:Ljava/lang/String;

    return-object v0
.end method

.method public getItableFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->itableFilename_:Ljava/lang/String;

    return-object v0
.end method

.method public getLmCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->lmCost_:F

    return v0
.end method

.method public getOtableChecksum()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->otableChecksum_:Ljava/lang/String;

    return-object v0
.end method

.method public getOtableFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->otableFilename_:Ljava/lang/String;

    return-object v0
.end method

.method public getSegmentList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->segment_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasFrameDuration()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getFrameDuration()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasTotalCost()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getTotalCost()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasAmCost()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getAmCost()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasLmCost()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getLmCost()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getSegmentList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeGroupSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasItableChecksum()Z

    move-result v3

    if-eqz v3, :cond_5

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getItableChecksum()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasItableFilename()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getItableFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasOtableChecksum()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getOtableChecksum()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasOtableFilename()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getOtableFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasWordLabelPosition()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getWordLabelPosition()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    iput v2, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->cachedSize:I

    return v2
.end method

.method public getTotalCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->totalCost_:F

    return v0
.end method

.method public getWordLabelPosition()I
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->wordLabelPosition_:I

    return v0
.end method

.method public hasAmCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasAmCost:Z

    return v0
.end method

.method public hasFrameDuration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasFrameDuration:Z

    return v0
.end method

.method public hasItableChecksum()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasItableChecksum:Z

    return v0
.end method

.method public hasItableFilename()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasItableFilename:Z

    return v0
.end method

.method public hasLmCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasLmCost:Z

    return v0
.end method

.method public hasOtableChecksum()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasOtableChecksum:Z

    return v0
.end method

.method public hasOtableFilename()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasOtableFilename:Z

    return v0
.end method

.method public hasTotalCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasTotalCost:Z

    return v0
.end method

.method public hasWordLabelPosition()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasWordLabelPosition:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setFrameDuration(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setTotalCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setAmCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setLmCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    invoke-direct {v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;-><init>()V

    const/4 v2, 0x5

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readGroup(Lcom/google/protobuf/micro/MessageMicro;I)V

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->addSegment(Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setItableChecksum(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setItableFilename(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setOtableChecksum(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setOtableFilename(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->setWordLabelPosition(I)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2b -> :sswitch_5
        0x8a -> :sswitch_6
        0x92 -> :sswitch_7
        0x9a -> :sswitch_8
        0xa2 -> :sswitch_9
        0xa8 -> :sswitch_a
    .end sparse-switch
.end method

.method public setAmCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasAmCost:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->amCost_:F

    return-object p0
.end method

.method public setFrameDuration(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasFrameDuration:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->frameDuration_:F

    return-object p0
.end method

.method public setItableChecksum(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasItableChecksum:Z

    iput-object p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->itableChecksum_:Ljava/lang/String;

    return-object p0
.end method

.method public setItableFilename(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasItableFilename:Z

    iput-object p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->itableFilename_:Ljava/lang/String;

    return-object p0
.end method

.method public setLmCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasLmCost:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->lmCost_:F

    return-object p0
.end method

.method public setOtableChecksum(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasOtableChecksum:Z

    iput-object p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->otableChecksum_:Ljava/lang/String;

    return-object p0
.end method

.method public setOtableFilename(Ljava/lang/String;)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasOtableFilename:Z

    iput-object p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->otableFilename_:Ljava/lang/String;

    return-object p0
.end method

.method public setTotalCost(F)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasTotalCost:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->totalCost_:F

    return-object p0
.end method

.method public setWordLabelPosition(I)Lcom/google/speech/decoder/common/Alignment$AlignmentProto;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasWordLabelPosition:Z

    iput p1, p0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->wordLabelPosition_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasFrameDuration()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getFrameDuration()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasTotalCost()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getTotalCost()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasAmCost()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getAmCost()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasLmCost()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getLmCost()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getSegmentList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/decoder/common/Alignment$AlignmentProto$Segment;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeGroup(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasItableChecksum()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getItableChecksum()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasItableFilename()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getItableFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasOtableChecksum()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getOtableChecksum()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasOtableFilename()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getOtableFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->hasWordLabelPosition()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/speech/decoder/common/Alignment$AlignmentProto;->getWordLabelPosition()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    return-void
.end method
