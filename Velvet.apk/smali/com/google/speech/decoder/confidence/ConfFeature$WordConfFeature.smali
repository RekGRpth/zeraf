.class public final Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ConfFeature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/decoder/confidence/ConfFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WordConfFeature"
.end annotation


# instance fields
.field private amScore_:F

.field private ascoreBest_:F

.field private ascoreMean_:F

.field private ascoreMeandiff_:F

.field private ascoreStddev_:F

.field private ascoreWorst_:F

.field private cachedSize:I

.field private durScore_:F

.field private framePosterior_:F

.field private hasAmScore:Z

.field private hasAscoreBest:Z

.field private hasAscoreMean:Z

.field private hasAscoreMeandiff:Z

.field private hasAscoreStddev:Z

.field private hasAscoreWorst:Z

.field private hasDurScore:Z

.field private hasFramePosterior:Z

.field private hasLatPosterior:Z

.field private hasLmScore:Z

.field private hasNumPhones:Z

.field private hasPivotPosterior:Z

.field private hasWordDuration:Z

.field private latPosterior_:F

.field private lmScore_:F

.field private numPhones_:F

.field private pivotPosterior_:F

.field private wordDuration_:F


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->latPosterior_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->framePosterior_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->numPhones_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->wordDuration_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreMean_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreStddev_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreWorst_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreMeandiff_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreBest_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->lmScore_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->durScore_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->amScore_:F

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->pivotPosterior_:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAmScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->amScore_:F

    return v0
.end method

.method public getAscoreBest()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreBest_:F

    return v0
.end method

.method public getAscoreMean()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreMean_:F

    return v0
.end method

.method public getAscoreMeandiff()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreMeandiff_:F

    return v0
.end method

.method public getAscoreStddev()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreStddev_:F

    return v0
.end method

.method public getAscoreWorst()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreWorst_:F

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->cachedSize:I

    return v0
.end method

.method public getDurScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->durScore_:F

    return v0
.end method

.method public getFramePosterior()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->framePosterior_:F

    return v0
.end method

.method public getLatPosterior()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->latPosterior_:F

    return v0
.end method

.method public getLmScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->lmScore_:F

    return v0
.end method

.method public getNumPhones()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->numPhones_:F

    return v0
.end method

.method public getPivotPosterior()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->pivotPosterior_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasLatPosterior()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getLatPosterior()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasFramePosterior()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getFramePosterior()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasNumPhones()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getNumPhones()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasWordDuration()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getWordDuration()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreMean()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreMean()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreStddev()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreStddev()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreWorst()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreWorst()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreMeandiff()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreMeandiff()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreBest()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreBest()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasLmScore()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getLmScore()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasDurScore()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getDurScore()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAmScore()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAmScore()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasPivotPosterior()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getPivotPosterior()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iput v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->cachedSize:I

    return v0
.end method

.method public getWordDuration()F
    .locals 1

    iget v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->wordDuration_:F

    return v0
.end method

.method public hasAmScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAmScore:Z

    return v0
.end method

.method public hasAscoreBest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreBest:Z

    return v0
.end method

.method public hasAscoreMean()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreMean:Z

    return v0
.end method

.method public hasAscoreMeandiff()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreMeandiff:Z

    return v0
.end method

.method public hasAscoreStddev()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreStddev:Z

    return v0
.end method

.method public hasAscoreWorst()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreWorst:Z

    return v0
.end method

.method public hasDurScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasDurScore:Z

    return v0
.end method

.method public hasFramePosterior()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasFramePosterior:Z

    return v0
.end method

.method public hasLatPosterior()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasLatPosterior:Z

    return v0
.end method

.method public hasLmScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasLmScore:Z

    return v0
.end method

.method public hasNumPhones()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasNumPhones:Z

    return v0
.end method

.method public hasPivotPosterior()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasPivotPosterior:Z

    return v0
.end method

.method public hasWordDuration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasWordDuration:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setLatPosterior(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setFramePosterior(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setNumPhones(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setWordDuration(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setAscoreMean(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setAscoreStddev(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setAscoreWorst(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setAscoreMeandiff(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setAscoreBest(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setLmScore(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setDurScore(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setAmScore(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->setPivotPosterior(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x4d -> :sswitch_9
        0x55 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6d -> :sswitch_d
    .end sparse-switch
.end method

.method public setAmScore(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAmScore:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->amScore_:F

    return-object p0
.end method

.method public setAscoreBest(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreBest:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreBest_:F

    return-object p0
.end method

.method public setAscoreMean(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreMean:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreMean_:F

    return-object p0
.end method

.method public setAscoreMeandiff(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreMeandiff:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreMeandiff_:F

    return-object p0
.end method

.method public setAscoreStddev(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreStddev:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreStddev_:F

    return-object p0
.end method

.method public setAscoreWorst(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreWorst:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->ascoreWorst_:F

    return-object p0
.end method

.method public setDurScore(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasDurScore:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->durScore_:F

    return-object p0
.end method

.method public setFramePosterior(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasFramePosterior:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->framePosterior_:F

    return-object p0
.end method

.method public setLatPosterior(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasLatPosterior:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->latPosterior_:F

    return-object p0
.end method

.method public setLmScore(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasLmScore:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->lmScore_:F

    return-object p0
.end method

.method public setNumPhones(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasNumPhones:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->numPhones_:F

    return-object p0
.end method

.method public setPivotPosterior(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasPivotPosterior:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->pivotPosterior_:F

    return-object p0
.end method

.method public setWordDuration(F)Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasWordDuration:Z

    iput p1, p0, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->wordDuration_:F

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasLatPosterior()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getLatPosterior()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasFramePosterior()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getFramePosterior()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasNumPhones()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getNumPhones()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasWordDuration()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getWordDuration()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreMean()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreMean()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreStddev()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreStddev()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreWorst()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreWorst()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreMeandiff()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreMeandiff()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAscoreBest()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAscoreBest()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasLmScore()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getLmScore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasDurScore()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getDurScore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasAmScore()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getAmScore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->hasPivotPosterior()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/speech/decoder/confidence/ConfFeature$WordConfFeature;->getPivotPosterior()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_c
    return-void
.end method
