.class public final Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognitionContextProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/common/proto/RecognitionContextProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognitionContext"
.end annotation


# instance fields
.field private applicationName_:Ljava/lang/String;

.field private cachedSize:I

.field private canonicalLanguage_:Ljava/lang/String;

.field private clientApplicationId_:Ljava/lang/String;

.field private clientId_:Ljava/lang/String;

.field private enabledKeyboardLanguage_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fieldId_:Ljava/lang/String;

.field private fieldName_:Ljava/lang/String;

.field private hardware_:Ljava/lang/String;

.field private hasApplicationName:Z

.field private hasCanonicalLanguage:Z

.field private hasClientApplicationId:Z

.field private hasClientId:Z

.field private hasFieldId:Z

.field private hasFieldName:Z

.field private hasGrxmlGrammar_:Z

.field private hasHardware:Z

.field private hasHasGrxmlGrammar:Z

.field private hasHint:Z

.field private hasImeOptions:Z

.field private hasInputType:Z

.field private hasLabel:Z

.field private hasLanguage:Z

.field private hasLanguageModel:Z

.field private hasRawLanguage:Z

.field private hasReferer:Z

.field private hasSelectedKeyboardLanguage:Z

.field private hasSingleLine:Z

.field private hasUserAgent:Z

.field private hasVoiceSearchLanguage:Z

.field private hint_:Ljava/lang/String;

.field private imeOptions_:I

.field private inputType_:I

.field private label_:Ljava/lang/String;

.field private languageModel_:Ljava/lang/String;

.field private language_:Ljava/lang/String;

.field private rawLanguage_:Ljava/lang/String;

.field private referer_:Ljava/lang/String;

.field private selectedKeyboardLanguage_:Ljava/lang/String;

.field private singleLine_:Z

.field private userAgent_:Ljava/lang/String;

.field private voiceSearchLanguage_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->applicationName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->fieldName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->fieldId_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->singleLine_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->label_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hint_:Ljava/lang/String;

    iput v1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->inputType_:I

    iput v1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->imeOptions_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->languageModel_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->rawLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->canonicalLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->language_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->clientId_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasGrxmlGrammar_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->selectedKeyboardLanguage_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->enabledKeyboardLanguage_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->voiceSearchLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->referer_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->userAgent_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hardware_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->clientApplicationId_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addEnabledKeyboardLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->enabledKeyboardLanguage_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->enabledKeyboardLanguage_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->enabledKeyboardLanguage_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getApplicationName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->applicationName_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->cachedSize:I

    return v0
.end method

.method public getCanonicalLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->canonicalLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getClientApplicationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->clientApplicationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->clientId_:Ljava/lang/String;

    return-object v0
.end method

.method public getEnabledKeyboardLanguageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->enabledKeyboardLanguage_:Ljava/util/List;

    return-object v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->fieldId_:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->fieldName_:Ljava/lang/String;

    return-object v0
.end method

.method public getHardware()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hardware_:Ljava/lang/String;

    return-object v0
.end method

.method public getHasGrxmlGrammar()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasGrxmlGrammar_:Z

    return v0
.end method

.method public getHint()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hint_:Ljava/lang/String;

    return-object v0
.end method

.method public getImeOptions()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->imeOptions_:I

    return v0
.end method

.method public getInputType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->inputType_:I

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->label_:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->language_:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguageModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->languageModel_:Ljava/lang/String;

    return-object v0
.end method

.method public getRawLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->rawLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getReferer()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->referer_:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedKeyboardLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->selectedKeyboardLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasApplicationName()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasFieldName()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getFieldName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasFieldId()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getFieldId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasSingleLine()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getSingleLine()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLabel()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getLabel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHint()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getHint()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasInputType()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getInputType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasImeOptions()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getImeOptions()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLanguageModel()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getLanguageModel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLanguage()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasClientId()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getClientId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHasGrxmlGrammar()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getHasGrxmlGrammar()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasSelectedKeyboardLanguage()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getSelectedKeyboardLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getEnabledKeyboardLanguageList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_d
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getEnabledKeyboardLanguageList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasVoiceSearchLanguage()Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v4, 0xf

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getVoiceSearchLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasReferer()Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getReferer()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasUserAgent()Z

    move-result v4

    if-eqz v4, :cond_10

    const/16 v4, 0x11

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getUserAgent()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHardware()Z

    move-result v4

    if-eqz v4, :cond_11

    const/16 v4, 0x12

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getHardware()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasClientApplicationId()Z

    move-result v4

    if-eqz v4, :cond_12

    const/16 v4, 0x13

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getClientApplicationId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasRawLanguage()Z

    move-result v4

    if-eqz v4, :cond_13

    const/16 v4, 0x14

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getRawLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasCanonicalLanguage()Z

    move-result v4

    if-eqz v4, :cond_14

    const/16 v4, 0x15

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getCanonicalLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_14
    iput v3, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->cachedSize:I

    return v3
.end method

.method public getSingleLine()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->singleLine_:Z

    return v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->userAgent_:Ljava/lang/String;

    return-object v0
.end method

.method public getVoiceSearchLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->voiceSearchLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public hasApplicationName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasApplicationName:Z

    return v0
.end method

.method public hasCanonicalLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasCanonicalLanguage:Z

    return v0
.end method

.method public hasClientApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasClientApplicationId:Z

    return v0
.end method

.method public hasClientId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasClientId:Z

    return v0
.end method

.method public hasFieldId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasFieldId:Z

    return v0
.end method

.method public hasFieldName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasFieldName:Z

    return v0
.end method

.method public hasHardware()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHardware:Z

    return v0
.end method

.method public hasHasGrxmlGrammar()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHasGrxmlGrammar:Z

    return v0
.end method

.method public hasHint()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHint:Z

    return v0
.end method

.method public hasImeOptions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasImeOptions:Z

    return v0
.end method

.method public hasInputType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasInputType:Z

    return v0
.end method

.method public hasLabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLabel:Z

    return v0
.end method

.method public hasLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLanguage:Z

    return v0
.end method

.method public hasLanguageModel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLanguageModel:Z

    return v0
.end method

.method public hasRawLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasRawLanguage:Z

    return v0
.end method

.method public hasReferer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasReferer:Z

    return v0
.end method

.method public hasSelectedKeyboardLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasSelectedKeyboardLanguage:Z

    return v0
.end method

.method public hasSingleLine()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasSingleLine:Z

    return v0
.end method

.method public hasUserAgent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasUserAgent:Z

    return v0
.end method

.method public hasVoiceSearchLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasVoiceSearchLanguage:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setApplicationName(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setFieldName(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setFieldId(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setSingleLine(Z)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setLabel(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setHint(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setInputType(I)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setImeOptions(I)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setLanguageModel(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setClientId(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setHasGrxmlGrammar(Z)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setSelectedKeyboardLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->addEnabledKeyboardLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setVoiceSearchLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setReferer(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setUserAgent(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setHardware(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setClientApplicationId(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setRawLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setCanonicalLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
    .end sparse-switch
.end method

.method public setApplicationName(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasApplicationName:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->applicationName_:Ljava/lang/String;

    return-object p0
.end method

.method public setCanonicalLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasCanonicalLanguage:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->canonicalLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientApplicationId(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasClientApplicationId:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->clientApplicationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientId(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasClientId:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->clientId_:Ljava/lang/String;

    return-object p0
.end method

.method public setFieldId(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasFieldId:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->fieldId_:Ljava/lang/String;

    return-object p0
.end method

.method public setFieldName(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasFieldName:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->fieldName_:Ljava/lang/String;

    return-object p0
.end method

.method public setHardware(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHardware:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hardware_:Ljava/lang/String;

    return-object p0
.end method

.method public setHasGrxmlGrammar(Z)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHasGrxmlGrammar:Z

    iput-boolean p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasGrxmlGrammar_:Z

    return-object p0
.end method

.method public setHint(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHint:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hint_:Ljava/lang/String;

    return-object p0
.end method

.method public setImeOptions(I)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasImeOptions:Z

    iput p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->imeOptions_:I

    return-object p0
.end method

.method public setInputType(I)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasInputType:Z

    iput p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->inputType_:I

    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLabel:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->label_:Ljava/lang/String;

    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLanguage:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->language_:Ljava/lang/String;

    return-object p0
.end method

.method public setLanguageModel(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLanguageModel:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->languageModel_:Ljava/lang/String;

    return-object p0
.end method

.method public setRawLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasRawLanguage:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->rawLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setReferer(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasReferer:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->referer_:Ljava/lang/String;

    return-object p0
.end method

.method public setSelectedKeyboardLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasSelectedKeyboardLanguage:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->selectedKeyboardLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setSingleLine(Z)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasSingleLine:Z

    iput-boolean p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->singleLine_:Z

    return-object p0
.end method

.method public setUserAgent(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasUserAgent:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->userAgent_:Ljava/lang/String;

    return-object p0
.end method

.method public setVoiceSearchLanguage(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasVoiceSearchLanguage:Z

    iput-object p1, p0, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->voiceSearchLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasApplicationName()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getApplicationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasFieldName()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasFieldId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getFieldId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasSingleLine()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getSingleLine()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLabel()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHint()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getHint()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasInputType()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getInputType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasImeOptions()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getImeOptions()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLanguageModel()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getLanguageModel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasLanguage()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasClientId()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHasGrxmlGrammar()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getHasGrxmlGrammar()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasSelectedKeyboardLanguage()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getSelectedKeyboardLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getEnabledKeyboardLanguageList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasVoiceSearchLanguage()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getVoiceSearchLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasReferer()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getReferer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasUserAgent()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getUserAgent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasHardware()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getHardware()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasClientApplicationId()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getClientApplicationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasRawLanguage()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getRawLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->hasCanonicalLanguage()Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->getCanonicalLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_14
    return-void
.end method
