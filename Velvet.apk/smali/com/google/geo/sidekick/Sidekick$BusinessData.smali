.class public final Lcom/google/geo/sidekick/Sidekick$BusinessData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BusinessData"
.end annotation


# instance fields
.field private cachedSize:I

.field private cid_:J

.field private coverPhoto_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private featureId_:Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

.field private hasCid:Z

.field private hasCoverPhoto:Z

.field private hasFeatureId:Z

.field private hasLocation:Z

.field private hasName:Z

.field private hasNumRatingStarsE3:Z

.field private hasNumReviews:Z

.field private hasOpenHours:Z

.field private hasOpenHoursToday:Z

.field private hasOpenUntil:Z

.field private hasPhoneNumber:Z

.field private hasPriceLevel:Z

.field private hasReviewScore:Z

.field private hasStarRating:Z

.field private hasWebsite:Z

.field private hasZagatRated:Z

.field private knownForTerm_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private location_:Lcom/google/geo/sidekick/Sidekick$Location;

.field private name_:Ljava/lang/String;

.field private numRatingStarsE3_:I

.field private numReviews_:I

.field private openHoursToday_:Ljava/lang/String;

.field private openHours_:Ljava/lang/String;

.field private openUntil_:Ljava/lang/String;

.field private phoneNumber_:Ljava/lang/String;

.field private priceLevel_:Ljava/lang/String;

.field private reviewScore_:I

.field private starRating_:Ljava/lang/String;

.field private type_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private website_:Ljava/lang/String;

.field private zagatRated_:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->cid_:J

    iput-object v3, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openHours_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openHoursToday_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openUntil_:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->coverPhoto_:Lcom/google/geo/sidekick/Sidekick$Photo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->starRating_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->knownForTerm_:Ljava/util/List;

    iput-object v3, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->featureId_:Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->phoneNumber_:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->zagatRated_:Z

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->numReviews_:I

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->reviewScore_:I

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->numRatingStarsE3_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->priceLevel_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->website_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->type_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addKnownForTerm(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->knownForTerm_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->knownForTerm_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->knownForTerm_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addType(I)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->type_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->type_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->type_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->cachedSize:I

    return v0
.end method

.method public getCid()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->cid_:J

    return-wide v0
.end method

.method public getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->coverPhoto_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getFeatureId()Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->featureId_:Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    return-object v0
.end method

.method public getKnownForTermCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->knownForTerm_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getKnownForTermList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->knownForTerm_:Ljava/util/List;

    return-object v0
.end method

.method public getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumRatingStarsE3()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->numRatingStarsE3_:I

    return v0
.end method

.method public getNumReviews()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->numReviews_:I

    return v0
.end method

.method public getOpenHours()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openHours_:Ljava/lang/String;

    return-object v0
.end method

.method public getOpenHoursToday()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openHoursToday_:Ljava/lang/String;

    return-object v0
.end method

.method public getOpenUntil()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openUntil_:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->phoneNumber_:Ljava/lang/String;

    return-object v0
.end method

.method public getPriceLevel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->priceLevel_:Ljava/lang/String;

    return-object v0
.end method

.method public getReviewScore()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->reviewScore_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCid()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHours()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenHours()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasStarRating()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getStarRating()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getKnownForTermList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_3
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getKnownForTermList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasFeatureId()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getFeatureId()Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHoursToday()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenHoursToday()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCoverPhoto()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPhoneNumber()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasZagatRated()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getZagatRated()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumReviews()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getNumReviews()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasReviewScore()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getReviewScore()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPriceLevel()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getPriceLevel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasName()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasWebsite()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getWebsite()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenUntil()Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v4, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenUntil()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_e
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_f
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getTypeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumRatingStarsE3()Z

    move-result v4

    if-eqz v4, :cond_10

    const/16 v4, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getNumRatingStarsE3()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasLocation()Z

    move-result v4

    if-eqz v4, :cond_11

    const/16 v4, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_11
    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->cachedSize:I

    return v3
.end method

.method public getStarRating()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->starRating_:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->type_:Ljava/util/List;

    return-object v0
.end method

.method public getWebsite()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->website_:Ljava/lang/String;

    return-object v0
.end method

.method public getZagatRated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->zagatRated_:Z

    return v0
.end method

.method public hasCid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid:Z

    return v0
.end method

.method public hasCoverPhoto()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCoverPhoto:Z

    return v0
.end method

.method public hasFeatureId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasFeatureId:Z

    return v0
.end method

.method public hasLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasLocation:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasName:Z

    return v0
.end method

.method public hasNumRatingStarsE3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumRatingStarsE3:Z

    return v0
.end method

.method public hasNumReviews()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumReviews:Z

    return v0
.end method

.method public hasOpenHours()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHours:Z

    return v0
.end method

.method public hasOpenHoursToday()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHoursToday:Z

    return v0
.end method

.method public hasOpenUntil()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenUntil:Z

    return v0
.end method

.method public hasPhoneNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPhoneNumber:Z

    return v0
.end method

.method public hasPriceLevel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPriceLevel:Z

    return v0
.end method

.method public hasReviewScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasReviewScore:Z

    return v0
.end method

.method public hasStarRating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasStarRating:Z

    return v0
.end method

.method public hasWebsite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasWebsite:Z

    return v0
.end method

.method public hasZagatRated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasZagatRated:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setCid(J)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setOpenHours(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setStarRating(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->addKnownForTerm(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setFeatureId(Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setOpenHoursToday(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setCoverPhoto(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setPhoneNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setZagatRated(Z)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setNumReviews(I)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setReviewScore(I)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setPriceLevel(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setWebsite(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setOpenUntil(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->addType(I)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setNumRatingStarsE3(I)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto/16 :goto_0

    :sswitch_12
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v0

    return-object v0
.end method

.method public setCid(J)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->cid_:J

    return-object p0
.end method

.method public setCoverPhoto(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCoverPhoto:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->coverPhoto_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public setFeatureId(Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasFeatureId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->featureId_:Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    return-object p0
.end method

.method public setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasLocation:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setNumRatingStarsE3(I)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumRatingStarsE3:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->numRatingStarsE3_:I

    return-object p0
.end method

.method public setNumReviews(I)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumReviews:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->numReviews_:I

    return-object p0
.end method

.method public setOpenHours(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHours:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openHours_:Ljava/lang/String;

    return-object p0
.end method

.method public setOpenHoursToday(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHoursToday:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openHoursToday_:Ljava/lang/String;

    return-object p0
.end method

.method public setOpenUntil(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenUntil:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->openUntil_:Ljava/lang/String;

    return-object p0
.end method

.method public setPhoneNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPhoneNumber:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->phoneNumber_:Ljava/lang/String;

    return-object p0
.end method

.method public setPriceLevel(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPriceLevel:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->priceLevel_:Ljava/lang/String;

    return-object p0
.end method

.method public setReviewScore(I)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasReviewScore:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->reviewScore_:I

    return-object p0
.end method

.method public setStarRating(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasStarRating:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->starRating_:Ljava/lang/String;

    return-object p0
.end method

.method public setWebsite(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasWebsite:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->website_:Ljava/lang/String;

    return-object p0
.end method

.method public setZagatRated(Z)Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasZagatRated:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$BusinessData;->zagatRated_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCid()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHours()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenHours()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasStarRating()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getStarRating()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getKnownForTermList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasFeatureId()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getFeatureId()Lcom/google/geo/sidekick/Sidekick$GeostoreFeatureId;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHoursToday()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenHoursToday()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCoverPhoto()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPhoneNumber()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasZagatRated()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getZagatRated()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumReviews()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getNumReviews()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasReviewScore()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getReviewScore()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPriceLevel()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getPriceLevel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasName()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasWebsite()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getWebsite()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenUntil()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenUntil()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getTypeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/16 v2, 0x10

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_1

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumRatingStarsE3()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getNumRatingStarsE3()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_11
    return-void
.end method
