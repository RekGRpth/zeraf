.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Sports"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private hasNotificationPolicy:Z

.field private hasNotificationSetting:Z

.field private hasShowAfter:Z

.field private hasShowBefore:Z

.field private hasShowDuring:Z

.field private notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

.field private notificationSetting_:I

.field private showAfter_:Z

.field private showBefore_:Z

.field private showDuring_:Z

.field private sportTeamPlayer_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->notificationSetting_:I

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showBefore_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showDuring_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showAfter_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->sportTeamPlayer_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addSportTeamPlayer(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->sportTeamPlayer_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->sportTeamPlayer_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->sportTeamPlayer_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public clearSportTeamPlayer()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->sportTeamPlayer_:Ljava/util/List;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->cachedSize:I

    return v0
.end method

.method public getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object v0
.end method

.method public getNotificationSetting()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->notificationSetting_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowBefore()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getShowBefore()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowDuring()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getShowDuring()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowAfter()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getShowAfter()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getSportTeamPlayerList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasNotificationPolicy()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasNotificationSetting()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getNotificationSetting()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->cachedSize:I

    return v2
.end method

.method public getShowAfter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showAfter_:Z

    return v0
.end method

.method public getShowBefore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showBefore_:Z

    return v0
.end method

.method public getShowDuring()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showDuring_:Z

    return v0
.end method

.method public getSportTeamPlayerCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->sportTeamPlayer_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSportTeamPlayerList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->sportTeamPlayer_:Ljava/util/List;

    return-object v0
.end method

.method public hasNotificationPolicy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasNotificationPolicy:Z

    return v0
.end method

.method public hasNotificationSetting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasNotificationSetting:Z

    return v0
.end method

.method public hasShowAfter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowAfter:Z

    return v0
.end method

.method public hasShowBefore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowBefore:Z

    return v0
.end method

.method public hasShowDuring()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowDuring:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->setShowBefore(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->setShowDuring(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->setShowAfter(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->addSportTeamPlayer(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->setNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->setNotificationSetting(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    move-result-object v0

    return-object v0
.end method

.method public setNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasNotificationPolicy:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object p0
.end method

.method public setNotificationSetting(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasNotificationSetting:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->notificationSetting_:I

    return-object p0
.end method

.method public setShowAfter(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowAfter:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showAfter_:Z

    return-object p0
.end method

.method public setShowBefore(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowBefore:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showBefore_:Z

    return-object p0
.end method

.method public setShowDuring(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowDuring:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->showDuring_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowBefore()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getShowBefore()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowDuring()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getShowDuring()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasShowAfter()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getShowAfter()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getSportTeamPlayerList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasNotificationPolicy()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->hasNotificationSetting()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;->getNotificationSetting()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    return-void
.end method
