.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SidekickConfiguration"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;,
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    }
.end annotation


# instance fields
.field private birthday_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

.field private cachedSize:I

.field private cardNotificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

.field private commuteTravelMode_:I

.field private currency_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;

.field private defaultGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

.field private eventReservations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;

.field private flights_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;

.field private hasBirthday:Z

.field private hasCardNotificationPolicy:Z

.field private hasCommuteTravelMode:Z

.field private hasCurrency:Z

.field private hasDefaultGroup:Z

.field private hasEventReservations:Z

.field private hasFlights:Z

.field private hasHashId:Z

.field private hasHeavyTrafficGroup:Z

.field private hasLastModifiedTimeSeconds:Z

.field private hasLocationHistory:Z

.field private hasMovies:Z

.field private hasNews:Z

.field private hasNextMeeting:Z

.field private hasNotificationOverride:Z

.field private hasNowCardsDisabled:Z

.field private hasOtherTravelMode:Z

.field private hasPhotoSpot:Z

.field private hasPlaces:Z

.field private hasPublicAlerts:Z

.field private hasPublicAlertsGroup:Z

.field private hasRemindersGroup:Z

.field private hasResearchTopics:Z

.field private hasRestaurantReservations:Z

.field private hasShowCardMask:Z

.field private hasSports:Z

.field private hasStockQuotes:Z

.field private hasTicketsGroup:Z

.field private hasTraffic:Z

.field private hasTrafficCardSharing:Z

.field private hasTrafficDelayActiveAlertThresholdMinutes:Z

.field private hasTrafficQueryIntervalSeconds:Z

.field private hasTransitStations:Z

.field private hasTranslate:Z

.field private hasTravelClock:Z

.field private hasTravelMode:Z

.field private hasTravelTimeGroup:Z

.field private hasUnits:Z

.field private hasUseGmailData:Z

.field private hasWeather:Z

.field private hasWorkLabel:Z

.field private hashId_:Ljava/lang/String;

.field private heavyTrafficGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

.field private lastModifiedTimeSeconds_:J

.field private locationHistory_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;

.field private movies_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;

.field private news_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

.field private nextMeeting_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

.field private notificationOverride_:I

.field private nowCardsDisabled_:Z

.field private otherTravelMode_:I

.field private photoSpot_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;

.field private places_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

.field private publicAlertsGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

.field private publicAlerts_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

.field private remindersGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

.field private researchTopics_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;

.field private restaurantReservations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;

.field private showCardMask_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

.field private sports_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

.field private stockQuotes_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;

.field private ticketsGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

.field private trafficCardSharing_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;

.field private trafficDelayActiveAlertThresholdMinutes_:I

.field private trafficQueryIntervalSeconds_:I

.field private traffic_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;

.field private transitStations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;

.field private translate_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;

.field private travelClock_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;

.field private travelMode_:I

.field private travelTimeGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

.field private units_:I

.field private useGmailData_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;

.field private weather_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;

.field private workLabel_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hashId_:Ljava/lang/String;

    const/16 v0, 0x12c

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficQueryIntervalSeconds_:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficDelayActiveAlertThresholdMinutes_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->lastModifiedTimeSeconds_:J

    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelMode_:I

    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->commuteTravelMode_:I

    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->otherTravelMode_:I

    iput-boolean v3, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->nowCardsDisabled_:Z

    iput v4, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->units_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->workLabel_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->showCardMask_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    iput v4, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->notificationOverride_:I

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->publicAlertsGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->ticketsGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->heavyTrafficGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelTimeGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->remindersGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->defaultGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->weather_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->traffic_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->nextMeeting_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->flights_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->transitStations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->places_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->sports_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->translate_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->currency_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelClock_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->publicAlerts_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->movies_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->stockQuotes_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->news_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->photoSpot_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->useGmailData_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->birthday_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->researchTopics_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->locationHistory_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->restaurantReservations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->eventReservations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficCardSharing_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->cardNotificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBirthday()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->birthday_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->cachedSize:I

    return v0
.end method

.method public getCardNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->cardNotificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object v0
.end method

.method public getCommuteTravelMode()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->commuteTravelMode_:I

    return v0
.end method

.method public getCurrency()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->currency_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;

    return-object v0
.end method

.method public getDefaultGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->defaultGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object v0
.end method

.method public getEventReservations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->eventReservations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;

    return-object v0
.end method

.method public getFlights()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->flights_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;

    return-object v0
.end method

.method public getHashId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hashId_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeavyTrafficGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->heavyTrafficGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object v0
.end method

.method public getLastModifiedTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->lastModifiedTimeSeconds_:J

    return-wide v0
.end method

.method public getLocationHistory()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->locationHistory_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;

    return-object v0
.end method

.method public getMovies()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->movies_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;

    return-object v0
.end method

.method public getNews()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->news_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    return-object v0
.end method

.method public getNextMeeting()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->nextMeeting_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

    return-object v0
.end method

.method public getNotificationOverride()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->notificationOverride_:I

    return v0
.end method

.method public getNowCardsDisabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->nowCardsDisabled_:Z

    return v0
.end method

.method public getOtherTravelMode()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->otherTravelMode_:I

    return v0
.end method

.method public getPhotoSpot()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->photoSpot_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;

    return-object v0
.end method

.method public getPlaces()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->places_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    return-object v0
.end method

.method public getPublicAlerts()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->publicAlerts_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    return-object v0
.end method

.method public getPublicAlertsGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->publicAlertsGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object v0
.end method

.method public getRemindersGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->remindersGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object v0
.end method

.method public getResearchTopics()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->researchTopics_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;

    return-object v0
.end method

.method public getRestaurantReservations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->restaurantReservations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficQueryIntervalSeconds()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTrafficQueryIntervalSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficDelayActiveAlertThresholdMinutes()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTrafficDelayActiveAlertThresholdMinutes()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasLastModifiedTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getLastModifiedTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelMode()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTravelMode()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasUnits()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getUnits()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasShowCardMask()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getShowCardMask()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasWeather()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getWeather()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTraffic()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTraffic()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNextMeeting()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNextMeeting()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasFlights()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getFlights()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTransitStations()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTransitStations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPlaces()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getPlaces()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasSports()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getSports()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCardNotificationPolicy()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v1, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getCardNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTranslate()Z

    move-result v1

    if-eqz v1, :cond_e

    const/16 v1, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTranslate()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCurrency()Z

    move-result v1

    if-eqz v1, :cond_f

    const/16 v1, 0x13

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getCurrency()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelClock()Z

    move-result v1

    if-eqz v1, :cond_10

    const/16 v1, 0x14

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTravelClock()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasWorkLabel()Z

    move-result v1

    if-eqz v1, :cond_11

    const/16 v1, 0x15

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getWorkLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNowCardsDisabled()Z

    move-result v1

    if-eqz v1, :cond_12

    const/16 v1, 0x18

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNowCardsDisabled()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPublicAlerts()Z

    move-result v1

    if-eqz v1, :cond_13

    const/16 v1, 0x1a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getPublicAlerts()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasMovies()Z

    move-result v1

    if-eqz v1, :cond_14

    const/16 v1, 0x1b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getMovies()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasStockQuotes()Z

    move-result v1

    if-eqz v1, :cond_15

    const/16 v1, 0x1c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getStockQuotes()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNews()Z

    move-result v1

    if-eqz v1, :cond_16

    const/16 v1, 0x1d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNews()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPhotoSpot()Z

    move-result v1

    if-eqz v1, :cond_17

    const/16 v1, 0x1f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getPhotoSpot()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasUseGmailData()Z

    move-result v1

    if-eqz v1, :cond_18

    const/16 v1, 0x21

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getUseGmailData()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_18
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasBirthday()Z

    move-result v1

    if-eqz v1, :cond_19

    const/16 v1, 0x22

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getBirthday()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_19
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasHashId()Z

    move-result v1

    if-eqz v1, :cond_1a

    const/16 v1, 0x23

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getHashId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasResearchTopics()Z

    move-result v1

    if-eqz v1, :cond_1b

    const/16 v1, 0x24

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getResearchTopics()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasLocationHistory()Z

    move-result v1

    if-eqz v1, :cond_1c

    const/16 v1, 0x26

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getLocationHistory()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasRestaurantReservations()Z

    move-result v1

    if-eqz v1, :cond_1d

    const/16 v1, 0x27

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getRestaurantReservations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasEventReservations()Z

    move-result v1

    if-eqz v1, :cond_1e

    const/16 v1, 0x28

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getEventReservations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCommuteTravelMode()Z

    move-result v1

    if-eqz v1, :cond_1f

    const/16 v1, 0x29

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getCommuteTravelMode()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasOtherTravelMode()Z

    move-result v1

    if-eqz v1, :cond_20

    const/16 v1, 0x2a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getOtherTravelMode()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_20
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNotificationOverride()Z

    move-result v1

    if-eqz v1, :cond_21

    const/16 v1, 0x2b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNotificationOverride()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_21
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPublicAlertsGroup()Z

    move-result v1

    if-eqz v1, :cond_22

    const/16 v1, 0x2c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getPublicAlertsGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_22
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTicketsGroup()Z

    move-result v1

    if-eqz v1, :cond_23

    const/16 v1, 0x2d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTicketsGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_23
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasHeavyTrafficGroup()Z

    move-result v1

    if-eqz v1, :cond_24

    const/16 v1, 0x2e

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getHeavyTrafficGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_24
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelTimeGroup()Z

    move-result v1

    if-eqz v1, :cond_25

    const/16 v1, 0x2f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTravelTimeGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_25
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasRemindersGroup()Z

    move-result v1

    if-eqz v1, :cond_26

    const/16 v1, 0x30

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getRemindersGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_26
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasDefaultGroup()Z

    move-result v1

    if-eqz v1, :cond_27

    const/16 v1, 0x31

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getDefaultGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_27
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficCardSharing()Z

    move-result v1

    if-eqz v1, :cond_28

    const/16 v1, 0x33

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTrafficCardSharing()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_28
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->cachedSize:I

    return v0
.end method

.method public getShowCardMask()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->showCardMask_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    return-object v0
.end method

.method public getSports()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->sports_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    return-object v0
.end method

.method public getStockQuotes()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->stockQuotes_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;

    return-object v0
.end method

.method public getTicketsGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->ticketsGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object v0
.end method

.method public getTraffic()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->traffic_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;

    return-object v0
.end method

.method public getTrafficCardSharing()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficCardSharing_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;

    return-object v0
.end method

.method public getTrafficDelayActiveAlertThresholdMinutes()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficDelayActiveAlertThresholdMinutes_:I

    return v0
.end method

.method public getTrafficQueryIntervalSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficQueryIntervalSeconds_:I

    return v0
.end method

.method public getTransitStations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->transitStations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;

    return-object v0
.end method

.method public getTranslate()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->translate_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;

    return-object v0
.end method

.method public getTravelClock()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelClock_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;

    return-object v0
.end method

.method public getTravelMode()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelMode_:I

    return v0
.end method

.method public getTravelTimeGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelTimeGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object v0
.end method

.method public getUnits()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->units_:I

    return v0
.end method

.method public getUseGmailData()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->useGmailData_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;

    return-object v0
.end method

.method public getWeather()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->weather_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;

    return-object v0
.end method

.method public getWorkLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->workLabel_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBirthday()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasBirthday:Z

    return v0
.end method

.method public hasCardNotificationPolicy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCardNotificationPolicy:Z

    return v0
.end method

.method public hasCommuteTravelMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCommuteTravelMode:Z

    return v0
.end method

.method public hasCurrency()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCurrency:Z

    return v0
.end method

.method public hasDefaultGroup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasDefaultGroup:Z

    return v0
.end method

.method public hasEventReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasEventReservations:Z

    return v0
.end method

.method public hasFlights()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasFlights:Z

    return v0
.end method

.method public hasHashId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasHashId:Z

    return v0
.end method

.method public hasHeavyTrafficGroup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasHeavyTrafficGroup:Z

    return v0
.end method

.method public hasLastModifiedTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasLastModifiedTimeSeconds:Z

    return v0
.end method

.method public hasLocationHistory()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasLocationHistory:Z

    return v0
.end method

.method public hasMovies()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasMovies:Z

    return v0
.end method

.method public hasNews()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNews:Z

    return v0
.end method

.method public hasNextMeeting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNextMeeting:Z

    return v0
.end method

.method public hasNotificationOverride()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNotificationOverride:Z

    return v0
.end method

.method public hasNowCardsDisabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNowCardsDisabled:Z

    return v0
.end method

.method public hasOtherTravelMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasOtherTravelMode:Z

    return v0
.end method

.method public hasPhotoSpot()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPhotoSpot:Z

    return v0
.end method

.method public hasPlaces()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPlaces:Z

    return v0
.end method

.method public hasPublicAlerts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPublicAlerts:Z

    return v0
.end method

.method public hasPublicAlertsGroup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPublicAlertsGroup:Z

    return v0
.end method

.method public hasRemindersGroup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasRemindersGroup:Z

    return v0
.end method

.method public hasResearchTopics()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasResearchTopics:Z

    return v0
.end method

.method public hasRestaurantReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasRestaurantReservations:Z

    return v0
.end method

.method public hasShowCardMask()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasShowCardMask:Z

    return v0
.end method

.method public hasSports()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasSports:Z

    return v0
.end method

.method public hasStockQuotes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasStockQuotes:Z

    return v0
.end method

.method public hasTicketsGroup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTicketsGroup:Z

    return v0
.end method

.method public hasTraffic()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTraffic:Z

    return v0
.end method

.method public hasTrafficCardSharing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficCardSharing:Z

    return v0
.end method

.method public hasTrafficDelayActiveAlertThresholdMinutes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficDelayActiveAlertThresholdMinutes:Z

    return v0
.end method

.method public hasTrafficQueryIntervalSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficQueryIntervalSeconds:Z

    return v0
.end method

.method public hasTransitStations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTransitStations:Z

    return v0
.end method

.method public hasTranslate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTranslate:Z

    return v0
.end method

.method public hasTravelClock()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelClock:Z

    return v0
.end method

.method public hasTravelMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelMode:Z

    return v0
.end method

.method public hasTravelTimeGroup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelTimeGroup:Z

    return v0
.end method

.method public hasUnits()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasUnits:Z

    return v0
.end method

.method public hasUseGmailData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasUseGmailData:Z

    return v0
.end method

.method public hasWeather()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasWeather:Z

    return v0
.end method

.method public hasWorkLabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasWorkLabel:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTrafficQueryIntervalSeconds(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTrafficDelayActiveAlertThresholdMinutes(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setLastModifiedTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTravelMode(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setUnits(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setShowCardMask(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setWeather(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTraffic(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setNextMeeting(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setFlights(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTransitStations(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setPlaces(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setSports(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setCardNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTranslate(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setCurrency(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_11
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTravelClock(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setWorkLabel(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setNowCardsDisabled(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_14
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setPublicAlerts(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_15
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setMovies(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_16
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setStockQuotes(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_17
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setNews(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_18
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setPhotoSpot(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_19
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setUseGmailData(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_1a
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setBirthday(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setHashId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_1c
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setResearchTopics(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_1d
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setLocationHistory(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_1e
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setRestaurantReservations(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_1f
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setEventReservations(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_20
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setCommuteTravelMode(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_21
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setOtherTravelMode(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_22
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setNotificationOverride(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_23
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setPublicAlertsGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_24
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTicketsGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_25
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setHeavyTrafficGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_26
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTravelTimeGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_27
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setRemindersGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_28
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setDefaultGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_29
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->setTrafficCardSharing(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x52 -> :sswitch_7
        0x5a -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
        0x82 -> :sswitch_d
        0x8a -> :sswitch_e
        0x92 -> :sswitch_f
        0x9a -> :sswitch_10
        0xa2 -> :sswitch_11
        0xaa -> :sswitch_12
        0xc0 -> :sswitch_13
        0xd2 -> :sswitch_14
        0xda -> :sswitch_15
        0xe2 -> :sswitch_16
        0xea -> :sswitch_17
        0xfa -> :sswitch_18
        0x10a -> :sswitch_19
        0x112 -> :sswitch_1a
        0x11a -> :sswitch_1b
        0x122 -> :sswitch_1c
        0x132 -> :sswitch_1d
        0x13a -> :sswitch_1e
        0x142 -> :sswitch_1f
        0x148 -> :sswitch_20
        0x150 -> :sswitch_21
        0x158 -> :sswitch_22
        0x162 -> :sswitch_23
        0x16a -> :sswitch_24
        0x172 -> :sswitch_25
        0x17a -> :sswitch_26
        0x182 -> :sswitch_27
        0x18a -> :sswitch_28
        0x19a -> :sswitch_29
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public setBirthday(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasBirthday:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->birthday_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    return-object p0
.end method

.method public setCardNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCardNotificationPolicy:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->cardNotificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object p0
.end method

.method public setCommuteTravelMode(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCommuteTravelMode:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->commuteTravelMode_:I

    return-object p0
.end method

.method public setCurrency(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCurrency:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->currency_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;

    return-object p0
.end method

.method public setDefaultGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasDefaultGroup:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->defaultGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object p0
.end method

.method public setEventReservations(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasEventReservations:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->eventReservations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;

    return-object p0
.end method

.method public setFlights(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasFlights:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->flights_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;

    return-object p0
.end method

.method public setHashId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasHashId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hashId_:Ljava/lang/String;

    return-object p0
.end method

.method public setHeavyTrafficGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasHeavyTrafficGroup:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->heavyTrafficGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object p0
.end method

.method public setLastModifiedTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasLastModifiedTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->lastModifiedTimeSeconds_:J

    return-object p0
.end method

.method public setLocationHistory(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasLocationHistory:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->locationHistory_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;

    return-object p0
.end method

.method public setMovies(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasMovies:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->movies_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;

    return-object p0
.end method

.method public setNews(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNews:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->news_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    return-object p0
.end method

.method public setNextMeeting(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNextMeeting:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->nextMeeting_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

    return-object p0
.end method

.method public setNotificationOverride(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNotificationOverride:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->notificationOverride_:I

    return-object p0
.end method

.method public setNowCardsDisabled(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNowCardsDisabled:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->nowCardsDisabled_:Z

    return-object p0
.end method

.method public setOtherTravelMode(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasOtherTravelMode:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->otherTravelMode_:I

    return-object p0
.end method

.method public setPhotoSpot(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPhotoSpot:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->photoSpot_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;

    return-object p0
.end method

.method public setPlaces(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPlaces:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->places_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    return-object p0
.end method

.method public setPublicAlerts(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPublicAlerts:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->publicAlerts_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    return-object p0
.end method

.method public setPublicAlertsGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPublicAlertsGroup:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->publicAlertsGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object p0
.end method

.method public setRemindersGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasRemindersGroup:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->remindersGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object p0
.end method

.method public setResearchTopics(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasResearchTopics:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->researchTopics_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;

    return-object p0
.end method

.method public setRestaurantReservations(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasRestaurantReservations:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->restaurantReservations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;

    return-object p0
.end method

.method public setShowCardMask(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasShowCardMask:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->showCardMask_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    return-object p0
.end method

.method public setSports(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasSports:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->sports_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    return-object p0
.end method

.method public setStockQuotes(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasStockQuotes:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->stockQuotes_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;

    return-object p0
.end method

.method public setTicketsGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTicketsGroup:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->ticketsGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object p0
.end method

.method public setTraffic(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTraffic:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->traffic_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;

    return-object p0
.end method

.method public setTrafficCardSharing(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficCardSharing:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficCardSharing_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;

    return-object p0
.end method

.method public setTrafficDelayActiveAlertThresholdMinutes(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficDelayActiveAlertThresholdMinutes:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficDelayActiveAlertThresholdMinutes_:I

    return-object p0
.end method

.method public setTrafficQueryIntervalSeconds(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficQueryIntervalSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->trafficQueryIntervalSeconds_:I

    return-object p0
.end method

.method public setTransitStations(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTransitStations:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->transitStations_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;

    return-object p0
.end method

.method public setTranslate(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTranslate:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->translate_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;

    return-object p0
.end method

.method public setTravelClock(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelClock:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelClock_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;

    return-object p0
.end method

.method public setTravelMode(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelMode:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelMode_:I

    return-object p0
.end method

.method public setTravelTimeGroup(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelTimeGroup:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->travelTimeGroup_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    return-object p0
.end method

.method public setUnits(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasUnits:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->units_:I

    return-object p0
.end method

.method public setUseGmailData(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasUseGmailData:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->useGmailData_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;

    return-object p0
.end method

.method public setWeather(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasWeather:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->weather_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;

    return-object p0
.end method

.method public setWorkLabel(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasWorkLabel:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->workLabel_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficQueryIntervalSeconds()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTrafficQueryIntervalSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficDelayActiveAlertThresholdMinutes()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTrafficDelayActiveAlertThresholdMinutes()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasLastModifiedTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getLastModifiedTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelMode()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTravelMode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasUnits()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getUnits()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasShowCardMask()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getShowCardMask()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasWeather()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getWeather()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Weather;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTraffic()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTraffic()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Traffic;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNextMeeting()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNextMeeting()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasFlights()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getFlights()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Flights;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTransitStations()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTransitStations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TransitStations;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPlaces()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getPlaces()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Places;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasSports()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getSports()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCardNotificationPolicy()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getCardNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTranslate()Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTranslate()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Translate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCurrency()Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0x13

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getCurrency()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Currency;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelClock()Z

    move-result v0

    if-eqz v0, :cond_10

    const/16 v0, 0x14

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTravelClock()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TravelClock;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasWorkLabel()Z

    move-result v0

    if-eqz v0, :cond_11

    const/16 v0, 0x15

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getWorkLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNowCardsDisabled()Z

    move-result v0

    if-eqz v0, :cond_12

    const/16 v0, 0x18

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNowCardsDisabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPublicAlerts()Z

    move-result v0

    if-eqz v0, :cond_13

    const/16 v0, 0x1a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getPublicAlerts()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PublicAlerts;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasMovies()Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0x1b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getMovies()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Movies;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasStockQuotes()Z

    move-result v0

    if-eqz v0, :cond_15

    const/16 v0, 0x1c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getStockQuotes()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_15
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNews()Z

    move-result v0

    if-eqz v0, :cond_16

    const/16 v0, 0x1d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNews()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_16
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPhotoSpot()Z

    move-result v0

    if-eqz v0, :cond_17

    const/16 v0, 0x1f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getPhotoSpot()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$PhotoSpot;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_17
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasUseGmailData()Z

    move-result v0

    if-eqz v0, :cond_18

    const/16 v0, 0x21

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getUseGmailData()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$UseGmailData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_18
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasBirthday()Z

    move-result v0

    if-eqz v0, :cond_19

    const/16 v0, 0x22

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getBirthday()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_19
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasHashId()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/16 v0, 0x23

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getHashId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasResearchTopics()Z

    move-result v0

    if-eqz v0, :cond_1b

    const/16 v0, 0x24

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getResearchTopics()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ResearchTopics;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasLocationHistory()Z

    move-result v0

    if-eqz v0, :cond_1c

    const/16 v0, 0x26

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getLocationHistory()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$LocationHistory;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasRestaurantReservations()Z

    move-result v0

    if-eqz v0, :cond_1d

    const/16 v0, 0x27

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getRestaurantReservations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$RestaurantReservations;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasEventReservations()Z

    move-result v0

    if-eqz v0, :cond_1e

    const/16 v0, 0x28

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getEventReservations()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EventReservations;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasCommuteTravelMode()Z

    move-result v0

    if-eqz v0, :cond_1f

    const/16 v0, 0x29

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getCommuteTravelMode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasOtherTravelMode()Z

    move-result v0

    if-eqz v0, :cond_20

    const/16 v0, 0x2a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getOtherTravelMode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_20
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasNotificationOverride()Z

    move-result v0

    if-eqz v0, :cond_21

    const/16 v0, 0x2b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNotificationOverride()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_21
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasPublicAlertsGroup()Z

    move-result v0

    if-eqz v0, :cond_22

    const/16 v0, 0x2c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getPublicAlertsGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_22
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTicketsGroup()Z

    move-result v0

    if-eqz v0, :cond_23

    const/16 v0, 0x2d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTicketsGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_23
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasHeavyTrafficGroup()Z

    move-result v0

    if-eqz v0, :cond_24

    const/16 v0, 0x2e

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getHeavyTrafficGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_24
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTravelTimeGroup()Z

    move-result v0

    if-eqz v0, :cond_25

    const/16 v0, 0x2f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTravelTimeGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_25
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasRemindersGroup()Z

    move-result v0

    if-eqz v0, :cond_26

    const/16 v0, 0x30

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getRemindersGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_26
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasDefaultGroup()Z

    move-result v0

    if-eqz v0, :cond_27

    const/16 v0, 0x31

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getDefaultGroup()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$EntrySourceGroup;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_27
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->hasTrafficCardSharing()Z

    move-result v0

    if-eqz v0, :cond_28

    const/16 v0, 0x33

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getTrafficCardSharing()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_28
    return-void
.end method
