.class public final Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GenericCardEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private cardType_:Ljava/lang/String;

.field private hasCardType:Z

.field private hasImageHeight:Z

.field private hasImageUrl:Z

.field private hasImageWidth:Z

.field private hasText:Z

.field private hasTitle:Z

.field private imageHeight_:I

.field private imageUrl_:Ljava/lang/String;

.field private imageWidth_:I

.field private text_:Ljava/lang/String;

.field private title_:Ljava/lang/String;

.field private viewAction_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->cardType_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->text_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageUrl_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageWidth_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageHeight_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->viewAction_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addViewAction(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->viewAction_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->viewAction_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->viewAction_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->cachedSize:I

    return v0
.end method

.method public getCardType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->cardType_:Ljava/lang/String;

    return-object v0
.end method

.method public getImageHeight()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageHeight_:I

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getImageWidth()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageWidth_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasTitle()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasText()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageUrl()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getViewActionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasCardType()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getCardType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageWidth()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageWidth()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageHeight()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageHeight()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->cachedSize:I

    return v2
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getViewAction(I)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->viewAction_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    return-object v0
.end method

.method public getViewActionCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->viewAction_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getViewActionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->viewAction_:Ljava/util/List;

    return-object v0
.end method

.method public hasCardType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasCardType:Z

    return v0
.end method

.method public hasImageHeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageHeight:Z

    return v0
.end method

.method public hasImageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageUrl:Z

    return v0
.end method

.method public hasImageWidth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageWidth:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasText:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasTitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setImageUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->addViewAction(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setCardType(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setImageWidth(I)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setImageHeight(I)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v0

    return-object v0
.end method

.method public setCardType(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasCardType:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->cardType_:Ljava/lang/String;

    return-object p0
.end method

.method public setImageHeight(I)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageHeight:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageHeight_:I

    return-object p0
.end method

.method public setImageUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setImageWidth(I)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageWidth:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->imageWidth_:I

    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasText:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->text_:Ljava/lang/String;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasTitle:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasText()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageUrl()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getViewActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasCardType()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getCardType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageWidth()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageWidth()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageHeight()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageHeight()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    return-void
.end method
