.class public final Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$GmailEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GmailMessageDescription"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasAttachment_:Z

.field private hasHasAttachment:Z

.field private hasHasStar:Z

.field private hasIsImportant:Z

.field private hasIsUnread:Z

.field private hasNumMessagesInThread:Z

.field private hasNumThreadParticipants:Z

.field private hasSender:Z

.field private hasSnippet:Z

.field private hasStar_:Z

.field private hasSubject:Z

.field private hasThreadTimeSec:Z

.field private hasUrl:Z

.field private isImportant_:Z

.field private isUnread_:Z

.field private labels_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private numMessagesInThread_:I

.field private numThreadParticipants_:I

.field private participants_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private recipients_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private sender_:Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

.field private snippet_:Ljava/lang/String;

.field private subject_:Ljava/lang/String;

.field private threadTimeSec_:J

.field private url_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasAttachment_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasStar_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->isUnread_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->isImportant_:Z

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->numMessagesInThread_:I

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->numThreadParticipants_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->subject_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->snippet_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->url_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->labels_:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->threadTimeSec_:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->sender_:Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->recipients_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->participants_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addLabels(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->labels_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->labels_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->labels_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addParticipants(Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->participants_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->participants_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->participants_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addRecipients(Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->recipients_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->recipients_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->recipients_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->cachedSize:I

    return v0
.end method

.method public getHasAttachment()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasAttachment_:Z

    return v0
.end method

.method public getHasStar()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasStar_:Z

    return v0
.end method

.method public getIsImportant()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->isImportant_:Z

    return v0
.end method

.method public getIsUnread()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->isUnread_:Z

    return v0
.end method

.method public getLabelsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->labels_:Ljava/util/List;

    return-object v0
.end method

.method public getNumMessagesInThread()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->numMessagesInThread_:I

    return v0
.end method

.method public getNumThreadParticipants()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->numThreadParticipants_:I

    return v0
.end method

.method public getParticipantsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->participants_:Ljava/util/List;

    return-object v0
.end method

.method public getRecipientsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->recipients_:Ljava/util/List;

    return-object v0
.end method

.method public getSender()Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->sender_:Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasHasAttachment()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getHasAttachment()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasHasStar()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getHasStar()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasIsUnread()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getIsUnread()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSubject()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getSubject()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSnippet()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getSnippet()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasUrl()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getLabelsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_6
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getLabelsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasThreadTimeSec()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getThreadTimeSec()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSender()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getSender()Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getRecipientsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    const/16 v4, 0xa

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getParticipantsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    const/16 v4, 0xb

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_2

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasIsImportant()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getIsImportant()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasNumMessagesInThread()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getNumMessagesInThread()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasNumThreadParticipants()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getNumThreadParticipants()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->cachedSize:I

    return v3
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->snippet_:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->subject_:Ljava/lang/String;

    return-object v0
.end method

.method public getThreadTimeSec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->threadTimeSec_:J

    return-wide v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasHasAttachment()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasHasAttachment:Z

    return v0
.end method

.method public hasHasStar()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasHasStar:Z

    return v0
.end method

.method public hasIsImportant()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasIsImportant:Z

    return v0
.end method

.method public hasIsUnread()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasIsUnread:Z

    return v0
.end method

.method public hasNumMessagesInThread()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasNumMessagesInThread:Z

    return v0
.end method

.method public hasNumThreadParticipants()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasNumThreadParticipants:Z

    return v0
.end method

.method public hasSender()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSender:Z

    return v0
.end method

.method public hasSnippet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSnippet:Z

    return v0
.end method

.method public hasSubject()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSubject:Z

    return v0
.end method

.method public hasThreadTimeSec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasThreadTimeSec:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setHasAttachment(Z)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setHasStar(Z)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setIsUnread(Z)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setSubject(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setSnippet(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->addLabels(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setThreadTimeSec(J)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setSender(Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->addRecipients(Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->addParticipants(Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setIsImportant(Z)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setNumMessagesInThread(I)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->setNumThreadParticipants(I)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    move-result-object v0

    return-object v0
.end method

.method public setHasAttachment(Z)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasHasAttachment:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasAttachment_:Z

    return-object p0
.end method

.method public setHasStar(Z)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasHasStar:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasStar_:Z

    return-object p0
.end method

.method public setIsImportant(Z)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasIsImportant:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->isImportant_:Z

    return-object p0
.end method

.method public setIsUnread(Z)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasIsUnread:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->isUnread_:Z

    return-object p0
.end method

.method public setNumMessagesInThread(I)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasNumMessagesInThread:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->numMessagesInThread_:I

    return-object p0
.end method

.method public setNumThreadParticipants(I)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasNumThreadParticipants:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->numThreadParticipants_:I

    return-object p0
.end method

.method public setSender(Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSender:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->sender_:Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    return-object p0
.end method

.method public setSnippet(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSnippet:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->snippet_:Ljava/lang/String;

    return-object p0
.end method

.method public setSubject(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSubject:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->subject_:Ljava/lang/String;

    return-object p0
.end method

.method public setThreadTimeSec(J)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasThreadTimeSec:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->threadTimeSec_:J

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->url_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasHasAttachment()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getHasAttachment()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasHasStar()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getHasStar()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasIsUnread()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getIsUnread()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSubject()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSnippet()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getSnippet()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getLabelsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasThreadTimeSec()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getThreadTimeSec()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasSender()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getSender()Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getRecipientsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getParticipantsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasIsImportant()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getIsImportant()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasNumMessagesInThread()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getNumMessagesInThread()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt32(II)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->hasNumThreadParticipants()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;->getNumThreadParticipants()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt32(II)V

    :cond_d
    return-void
.end method
