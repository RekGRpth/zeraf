.class public final Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CalendarEntry"
.end annotation


# instance fields
.field private cachedSize:I

.field private endTimeSeconds_:J

.field private endTimeZoneOffsetSeconds_:I

.field private endTimeZone_:Ljava/lang/String;

.field private eventUrl_:Ljava/lang/String;

.field private hasEndTimeSeconds:Z

.field private hasEndTimeZone:Z

.field private hasEndTimeZoneOffsetSeconds:Z

.field private hasEventUrl:Z

.field private hasHash:Z

.field private hasLocation:Z

.field private hasParticipationResponse:Z

.field private hasRoute:Z

.field private hasStartTimeSeconds:Z

.field private hasStartTimeZone:Z

.field private hasStartTimeZoneOffsetSeconds:Z

.field private hasTitle:Z

.field private hasTravelTimeSeconds:Z

.field private hash_:Ljava/lang/String;

.field private location_:Lcom/google/geo/sidekick/Sidekick$Location;

.field private participationResponse_:I

.field private route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

.field private startTimeSeconds_:J

.field private startTimeZoneOffsetSeconds_:I

.field private startTimeZone_:Ljava/lang/String;

.field private title_:Ljava/lang/String;

.field private travelTimeSeconds_:I


# direct methods
.method public constructor <init>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hash_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->travelTimeSeconds_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->title_:Ljava/lang/String;

    iput-wide v3, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeSeconds_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeZone_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeZoneOffsetSeconds_:I

    iput-wide v3, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeSeconds_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeZone_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeZoneOffsetSeconds_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->eventUrl_:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->participationResponse_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->cachedSize:I

    return v0
.end method

.method public getEndTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeSeconds_:J

    return-wide v0
.end method

.method public getEndTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeZone_:Ljava/lang/String;

    return-object v0
.end method

.method public getEndTimeZoneOffsetSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeZoneOffsetSeconds_:I

    return v0
.end method

.method public getEventUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->eventUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getHash()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hash_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getParticipationResponse()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->participationResponse_:I

    return v0
.end method

.method public getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasHash()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getHash()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasLocation()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTravelTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getTravelTimeSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTitle()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getStartTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeZone()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getStartTimeZone()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeZoneOffsetSeconds()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getStartTimeZoneOffsetSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getEndTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeZone()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getEndTimeZone()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeZoneOffsetSeconds()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getEndTimeZoneOffsetSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEventUrl()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getEventUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasParticipationResponse()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getParticipationResponse()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->cachedSize:I

    return v0
.end method

.method public getStartTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeSeconds_:J

    return-wide v0
.end method

.method public getStartTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeZone_:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTimeZoneOffsetSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeZoneOffsetSeconds_:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getTravelTimeSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->travelTimeSeconds_:I

    return v0
.end method

.method public hasEndTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeSeconds:Z

    return v0
.end method

.method public hasEndTimeZone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeZone:Z

    return v0
.end method

.method public hasEndTimeZoneOffsetSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeZoneOffsetSeconds:Z

    return v0
.end method

.method public hasEventUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEventUrl:Z

    return v0
.end method

.method public hasHash()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasHash:Z

    return v0
.end method

.method public hasLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasLocation:Z

    return v0
.end method

.method public hasParticipationResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasParticipationResponse:Z

    return v0
.end method

.method public hasRoute()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute:Z

    return v0
.end method

.method public hasStartTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeSeconds:Z

    return v0
.end method

.method public hasStartTimeZone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeZone:Z

    return v0
.end method

.method public hasStartTimeZoneOffsetSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeZoneOffsetSeconds:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTitle:Z

    return v0
.end method

.method public hasTravelTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTravelTimeSeconds:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setHash(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setTravelTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setStartTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setStartTimeZone(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setStartTimeZoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setEndTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setEndTimeZone(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setEndTimeZoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setEventUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setParticipationResponse(I)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    move-result-object v0

    return-object v0
.end method

.method public setEndTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeSeconds_:J

    return-object p0
.end method

.method public setEndTimeZone(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeZone:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeZone_:Ljava/lang/String;

    return-object p0
.end method

.method public setEndTimeZoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeZoneOffsetSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->endTimeZoneOffsetSeconds_:I

    return-object p0
.end method

.method public setEventUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEventUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->eventUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setHash(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasHash:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hash_:Ljava/lang/String;

    return-object p0
.end method

.method public setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasLocation:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public setParticipationResponse(I)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasParticipationResponse:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->participationResponse_:I

    return-object p0
.end method

.method public setRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object p0
.end method

.method public setStartTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeSeconds_:J

    return-object p0
.end method

.method public setStartTimeZone(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeZone:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeZone_:Ljava/lang/String;

    return-object p0
.end method

.method public setStartTimeZoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeZoneOffsetSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->startTimeZoneOffsetSeconds_:I

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTitle:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public setTravelTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTravelTimeSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->travelTimeSeconds_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasHash()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getHash()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTravelTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getTravelTimeSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getStartTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeZone()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getStartTimeZone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasStartTimeZoneOffsetSeconds()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getStartTimeZoneOffsetSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getEndTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeZone()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getEndTimeZone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEndTimeZoneOffsetSeconds()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getEndTimeZoneOffsetSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasEventUrl()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getEventUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasParticipationResponse()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getParticipationResponse()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_c
    return-void
.end method
