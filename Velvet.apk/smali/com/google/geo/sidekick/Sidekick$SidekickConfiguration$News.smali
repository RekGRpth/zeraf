.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "News"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private excludedQueries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;",
            ">;"
        }
    .end annotation
.end field

.field private hasInterestLevel:Z

.field private hasNotificationPolicy:Z

.field private interestLevel_:I

.field private notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->interestLevel_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->excludedQueries_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addExcludedQueries(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->excludedQueries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->excludedQueries_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->excludedQueries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->cachedSize:I

    return v0
.end method

.method public getExcludedQueriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->excludedQueries_:Ljava/util/List;

    return-object v0
.end method

.method public getInterestLevel()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->interestLevel_:I

    return v0
.end method

.method public getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->hasNotificationPolicy()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->hasInterestLevel()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->getInterestLevel()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->getExcludedQueriesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->cachedSize:I

    return v2
.end method

.method public hasInterestLevel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->hasInterestLevel:Z

    return v0
.end method

.method public hasNotificationPolicy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->hasNotificationPolicy:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->setNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->setInterestLevel(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->addExcludedQueries(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;

    move-result-object v0

    return-object v0
.end method

.method public setInterestLevel(I)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->hasInterestLevel:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->interestLevel_:I

    return-object p0
.end method

.method public setNotificationPolicy(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->hasNotificationPolicy:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->notificationPolicy_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->hasNotificationPolicy()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->getNotificationPolicy()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NotificationPolicy;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->hasInterestLevel()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->getInterestLevel()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News;->getExcludedQueriesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    return-void
.end method
