.class public final Lcom/google/geo/sidekick/Sidekick$ClientDescription;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientDescription"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasOsType:Z

.field private hasOsVersion:Z

.field private hasSessionId:Z

.field private hasSessionIdExpirationSeconds:Z

.field private hasSidekickAppVersion:Z

.field private osType_:I

.field private osVersion_:Ljava/lang/String;

.field private sessionIdExpirationSeconds_:J

.field private sessionId_:Ljava/lang/String;

.field private sidekickAppVersion_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->osType_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->osVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sidekickAppVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sessionId_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sessionIdExpirationSeconds_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->cachedSize:I

    return v0
.end method

.method public getOsType()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->osType_:I

    return v0
.end method

.method public getOsVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->osVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasOsType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getOsType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasOsVersion()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getOsVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSidekickAppVersion()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getSidekickAppVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSessionId()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSessionIdExpirationSeconds()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getSessionIdExpirationSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->cachedSize:I

    return v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sessionId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionIdExpirationSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sessionIdExpirationSeconds_:J

    return-wide v0
.end method

.method public getSidekickAppVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sidekickAppVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public hasOsType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasOsType:Z

    return v0
.end method

.method public hasOsVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasOsVersion:Z

    return v0
.end method

.method public hasSessionId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSessionId:Z

    return v0
.end method

.method public hasSessionIdExpirationSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSessionIdExpirationSeconds:Z

    return v0
.end method

.method public hasSidekickAppVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSidekickAppVersion:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setOsType(I)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setOsVersion(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setSidekickAppVersion(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setSessionId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setSessionIdExpirationSeconds(J)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    move-result-object v0

    return-object v0
.end method

.method public setOsType(I)Lcom/google/geo/sidekick/Sidekick$ClientDescription;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasOsType:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->osType_:I

    return-object p0
.end method

.method public setOsVersion(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasOsVersion:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->osVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setSessionId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSessionId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sessionId_:Ljava/lang/String;

    return-object p0
.end method

.method public setSessionIdExpirationSeconds(J)Lcom/google/geo/sidekick/Sidekick$ClientDescription;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSessionIdExpirationSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sessionIdExpirationSeconds_:J

    return-object p0
.end method

.method public setSidekickAppVersion(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSidekickAppVersion:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->sidekickAppVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasOsType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getOsType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasOsVersion()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getOsVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSidekickAppVersion()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getSidekickAppVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSessionId()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->hasSessionIdExpirationSeconds()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->getSessionIdExpirationSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    return-void
.end method
