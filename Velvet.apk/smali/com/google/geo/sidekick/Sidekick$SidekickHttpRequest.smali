.class public final Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SidekickHttpRequest"
.end annotation


# instance fields
.field private cachedSize:I

.field private client_:Lcom/google/geo/sidekick/Sidekick$ClientDescription;

.field private hasClient:Z

.field private hasPayload:Z

.field private hasTimeoutSeconds:Z

.field private hasTimestampSeconds:Z

.field private payload_:Lcom/google/geo/sidekick/Sidekick$RequestPayload;

.field private timeoutSeconds_:I

.field private timestampSeconds_:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->timestampSeconds_:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->timeoutSeconds_:I

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->client_:Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->payload_:Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->cachedSize:I

    return v0
.end method

.method public getClient()Lcom/google/geo/sidekick/Sidekick$ClientDescription;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->client_:Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    return-object v0
.end method

.method public getPayload()Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->payload_:Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasTimestampSeconds()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getTimestampSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasTimeoutSeconds()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getTimeoutSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasClient()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getClient()Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasPayload()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getPayload()Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->cachedSize:I

    return v0
.end method

.method public getTimeoutSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->timeoutSeconds_:I

    return v0
.end method

.method public getTimestampSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->timestampSeconds_:J

    return-wide v0
.end method

.method public hasClient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasClient:Z

    return v0
.end method

.method public hasPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasPayload:Z

    return v0
.end method

.method public hasTimeoutSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasTimeoutSeconds:Z

    return v0
.end method

.method public hasTimestampSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasTimestampSeconds:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->setTimeoutSeconds(I)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->setClient(Lcom/google/geo/sidekick/Sidekick$ClientDescription;)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->setPayload(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public setClient(Lcom/google/geo/sidekick/Sidekick$ClientDescription;)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasClient:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->client_:Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    return-object p0
.end method

.method public setPayload(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasPayload:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->payload_:Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    return-object p0
.end method

.method public setTimeoutSeconds(I)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasTimeoutSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->timeoutSeconds_:I

    return-object p0
.end method

.method public setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasTimestampSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->timestampSeconds_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasTimestampSeconds()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getTimestampSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasTimeoutSeconds()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getTimeoutSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasClient()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getClient()Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->hasPayload()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->getPayload()Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    return-void
.end method
