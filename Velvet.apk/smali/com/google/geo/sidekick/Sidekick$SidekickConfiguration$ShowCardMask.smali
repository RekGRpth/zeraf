.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowCardMask"
.end annotation


# instance fields
.field private barcodes_:Z

.field private birthday_:Z

.field private cachedSize:I

.field private eventReservations_:Z

.field private events_:Z

.field private flightReservations_:Z

.field private flight_:Z

.field private gmail_:Z

.field private hasBarcodes:Z

.field private hasBirthday:Z

.field private hasEventReservations:Z

.field private hasEvents:Z

.field private hasFlight:Z

.field private hasFlightReservations:Z

.field private hasGmail:Z

.field private hasHotelReservations:Z

.field private hasLocationHistory:Z

.field private hasMovies:Z

.field private hasNews:Z

.field private hasNextMeeting:Z

.field private hasPackageTracking:Z

.field private hasPhotoSpot:Z

.field private hasPlaces:Z

.field private hasPublicAlerts:Z

.field private hasRealEstate:Z

.field private hasReminders:Z

.field private hasResearchTopics:Z

.field private hasRestaurantReservations:Z

.field private hasSports:Z

.field private hasStockQuotes:Z

.field private hasTraffic:Z

.field private hasTransit:Z

.field private hasTravel:Z

.field private hasTravelAttractions:Z

.field private hasTravelCurrency:Z

.field private hasTravelHomeTime:Z

.field private hasTravelTranslate:Z

.field private hasWeather:Z

.field private hasWebsiteUpdate:Z

.field private hotelReservations_:Z

.field private locationHistory_:Z

.field private movies_:Z

.field private news_:Z

.field private nextMeeting_:Z

.field private packageTracking_:Z

.field private photoSpot_:Z

.field private places_:Z

.field private publicAlerts_:Z

.field private realEstate_:Z

.field private reminders_:Z

.field private researchTopics_:Z

.field private restaurantReservations_:Z

.field private sports_:Z

.field private stockQuotes_:Z

.field private traffic_:Z

.field private transit_:Z

.field private travelAttractions_:Z

.field private travelCurrency_:Z

.field private travelHomeTime_:Z

.field private travelTranslate_:Z

.field private travel_:Z

.field private weather_:Z

.field private websiteUpdate_:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->weather_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->traffic_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->nextMeeting_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travel_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelCurrency_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelHomeTime_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelTranslate_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->flight_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->transit_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->places_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->sports_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->publicAlerts_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->movies_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->stockQuotes_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelAttractions_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->news_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->photoSpot_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->packageTracking_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->restaurantReservations_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->eventReservations_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hotelReservations_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->flightReservations_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->birthday_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->researchTopics_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->events_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->locationHistory_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->barcodes_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->gmail_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->realEstate_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->websiteUpdate_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->reminders_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBarcodes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->barcodes_:Z

    return v0
.end method

.method public getBirthday()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->birthday_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->cachedSize:I

    return v0
.end method

.method public getEventReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->eventReservations_:Z

    return v0
.end method

.method public getEvents()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->events_:Z

    return v0
.end method

.method public getFlight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->flight_:Z

    return v0
.end method

.method public getFlightReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->flightReservations_:Z

    return v0
.end method

.method public getGmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->gmail_:Z

    return v0
.end method

.method public getHotelReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hotelReservations_:Z

    return v0
.end method

.method public getLocationHistory()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->locationHistory_:Z

    return v0
.end method

.method public getMovies()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->movies_:Z

    return v0
.end method

.method public getNews()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->news_:Z

    return v0
.end method

.method public getNextMeeting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->nextMeeting_:Z

    return v0
.end method

.method public getPackageTracking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->packageTracking_:Z

    return v0
.end method

.method public getPhotoSpot()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->photoSpot_:Z

    return v0
.end method

.method public getPlaces()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->places_:Z

    return v0
.end method

.method public getPublicAlerts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->publicAlerts_:Z

    return v0
.end method

.method public getRealEstate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->realEstate_:Z

    return v0
.end method

.method public getReminders()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->reminders_:Z

    return v0
.end method

.method public getResearchTopics()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->researchTopics_:Z

    return v0
.end method

.method public getRestaurantReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->restaurantReservations_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasWeather()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getWeather()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTraffic()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTraffic()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasNextMeeting()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getNextMeeting()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelCurrency()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravelCurrency()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelHomeTime()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravelHomeTime()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelTranslate()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravelTranslate()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasFlight()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getFlight()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTransit()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTransit()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPlaces()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getPlaces()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasSports()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getSports()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravel()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravel()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPublicAlerts()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getPublicAlerts()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasMovies()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getMovies()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasStockQuotes()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getStockQuotes()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelAttractions()Z

    move-result v1

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravelAttractions()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasNews()Z

    move-result v1

    if-eqz v1, :cond_f

    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getNews()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPhotoSpot()Z

    move-result v1

    if-eqz v1, :cond_10

    const/16 v1, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getPhotoSpot()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPackageTracking()Z

    move-result v1

    if-eqz v1, :cond_11

    const/16 v1, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getPackageTracking()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasRestaurantReservations()Z

    move-result v1

    if-eqz v1, :cond_12

    const/16 v1, 0x14

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getRestaurantReservations()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasEventReservations()Z

    move-result v1

    if-eqz v1, :cond_13

    const/16 v1, 0x15

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getEventReservations()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasHotelReservations()Z

    move-result v1

    if-eqz v1, :cond_14

    const/16 v1, 0x16

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getHotelReservations()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasFlightReservations()Z

    move-result v1

    if-eqz v1, :cond_15

    const/16 v1, 0x17

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getFlightReservations()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasBirthday()Z

    move-result v1

    if-eqz v1, :cond_16

    const/16 v1, 0x18

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getBirthday()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasResearchTopics()Z

    move-result v1

    if-eqz v1, :cond_17

    const/16 v1, 0x19

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getResearchTopics()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasEvents()Z

    move-result v1

    if-eqz v1, :cond_18

    const/16 v1, 0x1a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getEvents()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_18
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasLocationHistory()Z

    move-result v1

    if-eqz v1, :cond_19

    const/16 v1, 0x1b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getLocationHistory()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_19
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasBarcodes()Z

    move-result v1

    if-eqz v1, :cond_1a

    const/16 v1, 0x1c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getBarcodes()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasGmail()Z

    move-result v1

    if-eqz v1, :cond_1b

    const/16 v1, 0x1d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getGmail()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasRealEstate()Z

    move-result v1

    if-eqz v1, :cond_1c

    const/16 v1, 0x1e

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getRealEstate()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasWebsiteUpdate()Z

    move-result v1

    if-eqz v1, :cond_1d

    const/16 v1, 0x1f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getWebsiteUpdate()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasReminders()Z

    move-result v1

    if-eqz v1, :cond_1e

    const/16 v1, 0x20

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getReminders()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1e
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->cachedSize:I

    return v0
.end method

.method public getSports()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->sports_:Z

    return v0
.end method

.method public getStockQuotes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->stockQuotes_:Z

    return v0
.end method

.method public getTraffic()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->traffic_:Z

    return v0
.end method

.method public getTransit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->transit_:Z

    return v0
.end method

.method public getTravel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travel_:Z

    return v0
.end method

.method public getTravelAttractions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelAttractions_:Z

    return v0
.end method

.method public getTravelCurrency()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelCurrency_:Z

    return v0
.end method

.method public getTravelHomeTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelHomeTime_:Z

    return v0
.end method

.method public getTravelTranslate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelTranslate_:Z

    return v0
.end method

.method public getWeather()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->weather_:Z

    return v0
.end method

.method public getWebsiteUpdate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->websiteUpdate_:Z

    return v0
.end method

.method public hasBarcodes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasBarcodes:Z

    return v0
.end method

.method public hasBirthday()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasBirthday:Z

    return v0
.end method

.method public hasEventReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasEventReservations:Z

    return v0
.end method

.method public hasEvents()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasEvents:Z

    return v0
.end method

.method public hasFlight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasFlight:Z

    return v0
.end method

.method public hasFlightReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasFlightReservations:Z

    return v0
.end method

.method public hasGmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasGmail:Z

    return v0
.end method

.method public hasHotelReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasHotelReservations:Z

    return v0
.end method

.method public hasLocationHistory()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasLocationHistory:Z

    return v0
.end method

.method public hasMovies()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasMovies:Z

    return v0
.end method

.method public hasNews()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasNews:Z

    return v0
.end method

.method public hasNextMeeting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasNextMeeting:Z

    return v0
.end method

.method public hasPackageTracking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPackageTracking:Z

    return v0
.end method

.method public hasPhotoSpot()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPhotoSpot:Z

    return v0
.end method

.method public hasPlaces()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPlaces:Z

    return v0
.end method

.method public hasPublicAlerts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPublicAlerts:Z

    return v0
.end method

.method public hasRealEstate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasRealEstate:Z

    return v0
.end method

.method public hasReminders()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasReminders:Z

    return v0
.end method

.method public hasResearchTopics()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasResearchTopics:Z

    return v0
.end method

.method public hasRestaurantReservations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasRestaurantReservations:Z

    return v0
.end method

.method public hasSports()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasSports:Z

    return v0
.end method

.method public hasStockQuotes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasStockQuotes:Z

    return v0
.end method

.method public hasTraffic()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTraffic:Z

    return v0
.end method

.method public hasTransit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTransit:Z

    return v0
.end method

.method public hasTravel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravel:Z

    return v0
.end method

.method public hasTravelAttractions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelAttractions:Z

    return v0
.end method

.method public hasTravelCurrency()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelCurrency:Z

    return v0
.end method

.method public hasTravelHomeTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelHomeTime:Z

    return v0
.end method

.method public hasTravelTranslate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelTranslate:Z

    return v0
.end method

.method public hasWeather()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasWeather:Z

    return v0
.end method

.method public hasWebsiteUpdate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasWebsiteUpdate:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setWeather(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setTraffic(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setNextMeeting(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setTravelCurrency(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setTravelHomeTime(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setTravelTranslate(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setFlight(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setTransit(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setPlaces(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setSports(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setTravel(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setPublicAlerts(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setMovies(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setStockQuotes(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setTravelAttractions(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setNews(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setPhotoSpot(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setPackageTracking(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setRestaurantReservations(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setEventReservations(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setHotelReservations(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setFlightReservations(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setBirthday(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setResearchTopics(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setEvents(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setLocationHistory(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setBarcodes(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setGmail(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setRealEstate(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setWebsiteUpdate(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->setReminders(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0xa0 -> :sswitch_13
        0xa8 -> :sswitch_14
        0xb0 -> :sswitch_15
        0xb8 -> :sswitch_16
        0xc0 -> :sswitch_17
        0xc8 -> :sswitch_18
        0xd0 -> :sswitch_19
        0xd8 -> :sswitch_1a
        0xe0 -> :sswitch_1b
        0xe8 -> :sswitch_1c
        0xf0 -> :sswitch_1d
        0xf8 -> :sswitch_1e
        0x100 -> :sswitch_1f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;

    move-result-object v0

    return-object v0
.end method

.method public setBarcodes(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasBarcodes:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->barcodes_:Z

    return-object p0
.end method

.method public setBirthday(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasBirthday:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->birthday_:Z

    return-object p0
.end method

.method public setEventReservations(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasEventReservations:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->eventReservations_:Z

    return-object p0
.end method

.method public setEvents(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasEvents:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->events_:Z

    return-object p0
.end method

.method public setFlight(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasFlight:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->flight_:Z

    return-object p0
.end method

.method public setFlightReservations(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasFlightReservations:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->flightReservations_:Z

    return-object p0
.end method

.method public setGmail(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasGmail:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->gmail_:Z

    return-object p0
.end method

.method public setHotelReservations(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasHotelReservations:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hotelReservations_:Z

    return-object p0
.end method

.method public setLocationHistory(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasLocationHistory:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->locationHistory_:Z

    return-object p0
.end method

.method public setMovies(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasMovies:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->movies_:Z

    return-object p0
.end method

.method public setNews(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasNews:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->news_:Z

    return-object p0
.end method

.method public setNextMeeting(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasNextMeeting:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->nextMeeting_:Z

    return-object p0
.end method

.method public setPackageTracking(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPackageTracking:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->packageTracking_:Z

    return-object p0
.end method

.method public setPhotoSpot(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPhotoSpot:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->photoSpot_:Z

    return-object p0
.end method

.method public setPlaces(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPlaces:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->places_:Z

    return-object p0
.end method

.method public setPublicAlerts(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPublicAlerts:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->publicAlerts_:Z

    return-object p0
.end method

.method public setRealEstate(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasRealEstate:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->realEstate_:Z

    return-object p0
.end method

.method public setReminders(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasReminders:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->reminders_:Z

    return-object p0
.end method

.method public setResearchTopics(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasResearchTopics:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->researchTopics_:Z

    return-object p0
.end method

.method public setRestaurantReservations(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasRestaurantReservations:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->restaurantReservations_:Z

    return-object p0
.end method

.method public setSports(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasSports:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->sports_:Z

    return-object p0
.end method

.method public setStockQuotes(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasStockQuotes:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->stockQuotes_:Z

    return-object p0
.end method

.method public setTraffic(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTraffic:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->traffic_:Z

    return-object p0
.end method

.method public setTransit(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTransit:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->transit_:Z

    return-object p0
.end method

.method public setTravel(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravel:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travel_:Z

    return-object p0
.end method

.method public setTravelAttractions(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelAttractions:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelAttractions_:Z

    return-object p0
.end method

.method public setTravelCurrency(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelCurrency:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelCurrency_:Z

    return-object p0
.end method

.method public setTravelHomeTime(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelHomeTime:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelHomeTime_:Z

    return-object p0
.end method

.method public setTravelTranslate(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelTranslate:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->travelTranslate_:Z

    return-object p0
.end method

.method public setWeather(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasWeather:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->weather_:Z

    return-object p0
.end method

.method public setWebsiteUpdate(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasWebsiteUpdate:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->websiteUpdate_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasWeather()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getWeather()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTraffic()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTraffic()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasNextMeeting()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getNextMeeting()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelCurrency()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravelCurrency()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelHomeTime()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravelHomeTime()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelTranslate()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravelTranslate()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasFlight()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getFlight()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTransit()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTransit()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPlaces()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getPlaces()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasSports()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getSports()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravel()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravel()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPublicAlerts()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getPublicAlerts()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasMovies()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getMovies()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasStockQuotes()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getStockQuotes()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasTravelAttractions()Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getTravelAttractions()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasNews()Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getNews()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPhotoSpot()Z

    move-result v0

    if-eqz v0, :cond_10

    const/16 v0, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getPhotoSpot()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasPackageTracking()Z

    move-result v0

    if-eqz v0, :cond_11

    const/16 v0, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getPackageTracking()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasRestaurantReservations()Z

    move-result v0

    if-eqz v0, :cond_12

    const/16 v0, 0x14

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getRestaurantReservations()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasEventReservations()Z

    move-result v0

    if-eqz v0, :cond_13

    const/16 v0, 0x15

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getEventReservations()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasHotelReservations()Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0x16

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getHotelReservations()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasFlightReservations()Z

    move-result v0

    if-eqz v0, :cond_15

    const/16 v0, 0x17

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getFlightReservations()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_15
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasBirthday()Z

    move-result v0

    if-eqz v0, :cond_16

    const/16 v0, 0x18

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getBirthday()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_16
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasResearchTopics()Z

    move-result v0

    if-eqz v0, :cond_17

    const/16 v0, 0x19

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getResearchTopics()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_17
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasEvents()Z

    move-result v0

    if-eqz v0, :cond_18

    const/16 v0, 0x1a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getEvents()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_18
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasLocationHistory()Z

    move-result v0

    if-eqz v0, :cond_19

    const/16 v0, 0x1b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getLocationHistory()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_19
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasBarcodes()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/16 v0, 0x1c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getBarcodes()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasGmail()Z

    move-result v0

    if-eqz v0, :cond_1b

    const/16 v0, 0x1d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getGmail()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasRealEstate()Z

    move-result v0

    if-eqz v0, :cond_1c

    const/16 v0, 0x1e

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getRealEstate()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasWebsiteUpdate()Z

    move-result v0

    if-eqz v0, :cond_1d

    const/16 v0, 0x1f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getWebsiteUpdate()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->hasReminders()Z

    move-result v0

    if-eqz v0, :cond_1e

    const/16 v0, 0x20

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$ShowCardMask;->getReminders()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1e
    return-void
.end method
