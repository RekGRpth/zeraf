.class public final Lcom/google/geo/sidekick/Sidekick$StockQuote;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StockQuote"
.end annotation


# instance fields
.field private cachedSize:I

.field private chartUrl_:Ljava/lang/String;

.field private exchange_:Ljava/lang/String;

.field private hasChartUrl:Z

.field private hasExchange:Z

.field private hasLastPrice:Z

.field private hasLastUpdateSeconds:Z

.field private hasName:Z

.field private hasPriceVariation:Z

.field private hasPriceVariationPercent:Z

.field private hasPrimaryKey:Z

.field private hasQuoteserverData:Z

.field private hasSource:Z

.field private hasSymbol:Z

.field private hasWebUrl:Z

.field private lastPrice_:F

.field private lastUpdateSeconds_:J

.field private name_:Ljava/lang/String;

.field private priceVariationPercent_:F

.field private priceVariation_:F

.field private primaryKey_:Ljava/lang/String;

.field private quoteserverData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

.field private source_:I

.field private symbol_:Ljava/lang/String;

.field private webUrl_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->exchange_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->symbol_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->lastUpdateSeconds_:J

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->lastPrice_:F

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->priceVariation_:F

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->priceVariationPercent_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->chartUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->webUrl_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->quoteserverData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->primaryKey_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->name_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->source_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->cachedSize:I

    return v0
.end method

.method public getChartUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->chartUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getExchange()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->exchange_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastPrice()F
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->lastPrice_:F

    return v0
.end method

.method public getLastUpdateSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->lastUpdateSeconds_:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPriceVariation()F
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->priceVariation_:F

    return v0
.end method

.method public getPriceVariationPercent()F
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->priceVariationPercent_:F

    return v0
.end method

.method public getPrimaryKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->primaryKey_:Ljava/lang/String;

    return-object v0
.end method

.method public getQuoteserverData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->quoteserverData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasExchange()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getExchange()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasSymbol()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastUpdateSeconds()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastUpdateSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastPrice()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastPrice()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariation()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariation()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariationPercent()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariationPercent()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasChartUrl()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getChartUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasWebUrl()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getWebUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasQuoteserverData()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getQuoteserverData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPrimaryKey()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPrimaryKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasName()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasSource()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSource()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->cachedSize:I

    return v0
.end method

.method public getSource()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->source_:I

    return v0
.end method

.method public getSymbol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->symbol_:Ljava/lang/String;

    return-object v0
.end method

.method public getWebUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->webUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public hasChartUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasChartUrl:Z

    return v0
.end method

.method public hasExchange()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasExchange:Z

    return v0
.end method

.method public hasLastPrice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastPrice:Z

    return v0
.end method

.method public hasLastUpdateSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastUpdateSeconds:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasName:Z

    return v0
.end method

.method public hasPriceVariation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariation:Z

    return v0
.end method

.method public hasPriceVariationPercent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariationPercent:Z

    return v0
.end method

.method public hasPrimaryKey()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPrimaryKey:Z

    return v0
.end method

.method public hasQuoteserverData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasQuoteserverData:Z

    return v0
.end method

.method public hasSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasSource:Z

    return v0
.end method

.method public hasSymbol()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasSymbol:Z

    return v0
.end method

.method public hasWebUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasWebUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setExchange(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setSymbol(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setLastUpdateSeconds(J)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setLastPrice(F)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setPriceVariation(F)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setPriceVariationPercent(F)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setChartUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setWebUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setQuoteserverData(Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setPrimaryKey(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->setSource(I)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    move-result-object v0

    return-object v0
.end method

.method public setChartUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasChartUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->chartUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setExchange(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasExchange:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->exchange_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastPrice(F)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastPrice:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->lastPrice_:F

    return-object p0
.end method

.method public setLastUpdateSeconds(J)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastUpdateSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->lastUpdateSeconds_:J

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setPriceVariation(F)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariation:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->priceVariation_:F

    return-object p0
.end method

.method public setPriceVariationPercent(F)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariationPercent:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->priceVariationPercent_:F

    return-object p0
.end method

.method public setPrimaryKey(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPrimaryKey:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->primaryKey_:Ljava/lang/String;

    return-object p0
.end method

.method public setQuoteserverData(Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasQuoteserverData:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->quoteserverData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    return-object p0
.end method

.method public setSource(I)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasSource:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->source_:I

    return-object p0
.end method

.method public setSymbol(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasSymbol:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->symbol_:Ljava/lang/String;

    return-object p0
.end method

.method public setWebUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasWebUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$StockQuote;->webUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasExchange()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getExchange()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasSymbol()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastUpdateSeconds()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastUpdateSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastPrice()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastPrice()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariation()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariation()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariationPercent()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariationPercent()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasChartUrl()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getChartUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasWebUrl()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getWebUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasQuoteserverData()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getQuoteserverData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPrimaryKey()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPrimaryKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasName()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSource()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_b
    return-void
.end method
