.class public final Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GogglesProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/bionics/goggles/api2/GogglesProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BoundingBox"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasHeight:Z

.field private hasWidth:Z

.field private hasX:Z

.field private hasY:Z

.field private height_:I

.field private width_:I

.field private x_:I

.field private y_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->x_:I

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->y_:I

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->width_:I

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->height_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->cachedSize:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->height_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasX()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getX()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasY()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getY()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasWidth()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getWidth()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasHeight()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->cachedSize:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->width_:I

    return v0
.end method

.method public getX()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->x_:I

    return v0
.end method

.method public getY()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->y_:I

    return v0
.end method

.method public hasHeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasHeight:Z

    return v0
.end method

.method public hasWidth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasWidth:Z

    return v0
.end method

.method public hasX()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasX:Z

    return v0
.end method

.method public hasY()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasY:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->setX(I)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->setY(I)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->setWidth(I)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->setHeight(I)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public setHeight(I)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasHeight:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->height_:I

    return-object p0
.end method

.method public setWidth(I)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasWidth:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->width_:I

    return-object p0
.end method

.method public setX(I)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasX:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->x_:I

    return-object p0
.end method

.method public setY(I)Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasY:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->y_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasX()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getX()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasY()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasWidth()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getWidth()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$BoundingBox;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    return-void
.end method
