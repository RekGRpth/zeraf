.class public Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;
.super Ljava/lang/Object;
.source "VoiceRecognitionHandler.java"


# static fields
.field private static DEBUG:Z


# instance fields
.field private mDictationListener:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

.field private final mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

.field private mRecParams:Lcom/google/android/speech/params/RecognizerParams;

.field private mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

.field private mStarted:Z

.field private final mUiThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;Ljava/util/concurrent/Executor;Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    return-void
.end method

.method private createRecognitionContext()Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v1}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    invoke-direct {v1}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;-><init>()V

    iget-object v2, v0, Landroid/view/inputmethod/EditorInfo;->label:Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/google/android/voicesearch/util/TextUtil;->safeToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setLabel(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    iget-object v2, v0, Landroid/view/inputmethod/EditorInfo;->hintText:Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/google/android/voicesearch/util/TextUtil;->safeToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setHint(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    iget-object v2, v0, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/voicesearch/util/TextUtil;->safeToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setApplicationName(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    iget v2, v0, Landroid/view/inputmethod/EditorInfo;->fieldId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/voicesearch/util/TextUtil;->safeToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setFieldId(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    iget-object v2, v0, Landroid/view/inputmethod/EditorInfo;->fieldName:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/voicesearch/util/TextUtil;->safeToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setFieldName(Ljava/lang/String;)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    iget v2, v0, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-virtual {v1, v2}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setInputType(I)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    iget v2, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-virtual {v1, v2}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setImeOptions(I)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->isSingleLine()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;->setSingleLine(Z)Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    goto :goto_0
.end method

.method private isSingleLine()Z
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v3}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    new-instance v3, Landroid/view/inputmethod/ExtractedTextRequest;

    invoke-direct {v3}, Landroid/view/inputmethod/ExtractedTextRequest;-><init>()V

    invoke-interface {v1, v3, v2}, Landroid/view/inputmethod/InputConnection;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->flags:I

    and-int/lit8 v3, v3, 0x1

    if-lez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isSuggestionEnabled()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v1}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, v0, Landroid/view/inputmethod/EditorInfo;->inputType:I

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startListening(Lcom/google/android/speech/params/RecognizerParams;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mRecParams:Lcom/google/android/speech/params/RecognizerParams;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v0}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mRecParams:Lcom/google/android/speech/params/RecognizerParams;

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mDictationListener:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/speech/Recognizer;->startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mStarted:Z

    return-void
.end method


# virtual methods
.method public cancelRecognition()V
    .locals 2

    sget-boolean v0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceRecognitionHandler"

    const-string v1, "#cancelRecognition"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mStarted:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v0}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/speech/Recognizer;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mDictationListener:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->invalidate()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mStarted:Z

    :cond_1
    return-void
.end method

.method public createRecognitionParams(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParams;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->newBuilder()Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSpokenBcp47Locale(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->createRecognitionContext()Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setRecognitionContext(Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->DICTATION:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setMode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setGreco3Mode(Lcom/google/android/speech/embedded/Greco3Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->isSuggestionEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSuggestionEnabled(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    return-object v0
.end method

.method public getRecognitionParameters()Lcom/google/android/speech/params/RecognizerParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mRecParams:Lcom/google/android/speech/params/RecognizerParams;

    return-object v0
.end method

.method public isWaitingForResults()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mDictationListener:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mDictationListener:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startRecognizer(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;
    .param p2    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    sget-boolean v0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceRecognitionHandler"

    const-string v1, "startRecognizer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mDictationListener:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mDictationListener:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->invalidate()V

    :cond_1
    iput-object p2, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mDictationListener:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->startListening(Lcom/google/android/speech/params/RecognizerParams;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v0}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/speech/Recognizer;->stopListening()V

    :cond_0
    return-void
.end method
