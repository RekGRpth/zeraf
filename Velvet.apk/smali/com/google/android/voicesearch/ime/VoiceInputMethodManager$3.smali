.class final Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$3;
.super Ljava/lang/Object;
.source "VoiceInputMethodManager.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->create(Landroid/inputmethodservice/InputMethodService;)Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/android/speech/Recognizer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$3;->val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/android/speech/Recognizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$3;->val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$3;->get()Lcom/google/android/speech/Recognizer;

    move-result-object v0

    return-object v0
.end method
