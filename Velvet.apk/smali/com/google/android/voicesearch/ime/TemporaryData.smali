.class public Lcom/google/android/voicesearch/ime/TemporaryData;
.super Ljava/lang/Object;
.source "TemporaryData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DATA:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mData:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDATA;"
        }
    .end annotation
.end field

.field private mExpiredTimestamp:J

.field private final mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mData:Ljava/lang/Object;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mExpiredTimestamp:J

    return-void
.end method


# virtual methods
.method public extend()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x1f4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mExpiredTimestamp:J

    return-void
.end method

.method public forceExpire()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mExpiredTimestamp:J

    return-void
.end method

.method public getData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDATA;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mData:Ljava/lang/Object;

    return-object v0
.end method

.method public isExpired()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mExpiredTimestamp:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDATA;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/TemporaryData;->mData:Ljava/lang/Object;

    return-void
.end method
