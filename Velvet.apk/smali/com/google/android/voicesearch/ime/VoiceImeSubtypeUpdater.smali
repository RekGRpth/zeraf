.class public Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;
.super Ljava/lang/Object;
.source "VoiceImeSubtypeUpdater.java"

# interfaces
.implements Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mPackageName:Ljava/lang/String;

.field private mScheduledUpdate:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->mPackageName:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->mExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->updateVoiceImeSubtypes(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    return-void
.end method

.method private createInputMethodSubtype(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;Z)Landroid/view/inputmethod/InputMethodSubtype;
    .locals 9
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;
    .param p2    # Z

    const/4 v2, 0x0

    const-string v4, "voice"

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v0, 0x78

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "UntranslatableReplacementStringInSubtypeName="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p2, :cond_0

    const-string v0, ",requireNetworkConnectivity"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v0, ",bcp47="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getBcp47Locale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x1

    new-instance v0, Landroid/view/inputmethod/InputMethodSubtype;

    const v1, 0x7f0d0447

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getMainJavaLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v7, v2

    invoke-direct/range {v0 .. v7}, Landroid/view/inputmethod/InputMethodSubtype;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method private maybeScheduleUpdate(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Z)V
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .param p2    # Z

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->mScheduledUpdate:Z

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->mScheduledUpdate:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater$1;-><init>(Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private updateVoiceImeSubtypes(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 11
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    :try_start_0
    invoke-static {p1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getVoiceImeDialects(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Ljava/util/ArrayList;

    move-result-object v8

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    invoke-static {p1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getEmbeddedBcp47(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    invoke-virtual {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getBcp47Locale()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-direct {p0, v7, v1}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->createInputMethodSubtype(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;Z)Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v9, "VoiceImeSubtypeUpdater"

    const-string v10, "Error updating IME subtype: "

    invoke-static {v9, v10, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_1
    iget-object v9, p0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->mContext:Landroid/content/Context;

    const-string v10, "input_method"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Landroid/view/inputmethod/InputMethodSubtype;

    invoke-interface {v5, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v4, v10, v9}, Landroid/view/inputmethod/InputMethodManager;->setAdditionalInputMethodSubtypes(Ljava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public maybeScheduleUpdate(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->maybeScheduleUpdate(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Z)V

    return-void
.end method

.method public onChange(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->maybeScheduleUpdate(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Z)V

    return-void
.end method
