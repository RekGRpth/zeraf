.class public Lcom/google/android/voicesearch/ime/ImePreferences$Settings;
.super Lcom/android/inputmethodcommon/InputMethodSettingsFragment;
.source "ImePreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/ime/ImePreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# instance fields
.field private mController:Lcom/google/android/searchcommon/preferences/PreferenceController;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f0d03d5

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->setInputMethodSettingsCategoryTitle(I)V

    const v1, 0x7f0d03d6

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->setSubtypeEnablerTitle(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/VelvetApplication;->createPreferenceController(Landroid/app/Activity;)Lcom/google/android/searchcommon/preferences/PreferenceController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->useMainPreferences(Landroid/preference/PreferenceManager;)V

    const v1, 0x7f07000a

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->addPreferencesFromResource(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/preferences/PreferenceController;->handlePreference(Landroid/preference/Preference;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onCreateComplete(Landroid/os/Bundle;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onDestroy()V

    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onPause()V

    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onStop()V

    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onStop()V

    return-void
.end method
