.class final Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;
.super Ljava/lang/Object;
.source "VoiceInputMethodManager.java"

# interfaces
.implements Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->create(Landroid/inputmethodservice/InputMethodService;)Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

.field final synthetic val$powerManager:Landroid/os/PowerManager;

.field final synthetic val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method constructor <init>(Landroid/inputmethodservice/InputMethodService;Landroid/os/PowerManager;Lcom/google/android/voicesearch/VoiceSearchServices;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$powerManager:Landroid/os/PowerManager;

    iput-object p3, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

    return-object v0
.end method

.method public getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    return-object v0
.end method

.method public getRecognizer()Lcom/google/android/speech/Recognizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public isScreenOn()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$powerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    return v0
.end method

.method public scheduleSendEvents()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->scheduleSendEvents(Landroid/content/Context;)V

    return-void
.end method

.method public switchToLastInputMethod()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/inputmethodservice/InputMethodService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;->val$inputMethodService:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v2}, Landroid/inputmethodservice/InputMethodService;->getWindow()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget-object v1, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->switchToLastInputMethod(Landroid/os/IBinder;)Z

    return-void
.end method
