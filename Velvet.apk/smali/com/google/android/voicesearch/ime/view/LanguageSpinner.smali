.class public Lcom/google/android/voicesearch/ime/view/LanguageSpinner;
.super Landroid/widget/Spinner;
.source "LanguageSpinner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;

.field private mCurrentDialect:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

.field private mDialects:[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mCallback:Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mCurrentDialect:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mDialects:[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    return-object v0
.end method

.method private getCurrentDialect(Ljava/lang/String;[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-object v0, p2

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getBcp47Locale()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    aget-object v1, p2, v4

    goto :goto_1
.end method


# virtual methods
.method public onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->onDetachedFromWindow()V

    return-void
.end method

.method public performClick()Z
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x43

    invoke-static {v6}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v6, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mCallback:Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;

    invoke-interface {v6}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;->onDisplayDialectSelectionPopup()V

    iget-object v6, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mDialects:[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    invoke-static {v6}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDisplayNames([Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)[Ljava/lang/CharSequence;

    move-result-object v1

    array-length v6, v1

    add-int/lit8 v6, v6, 0x1

    new-array v4, v6, [Ljava/lang/CharSequence;

    array-length v6, v1

    invoke-static {v1, v7, v4, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v6, v1

    if-ne v6, v8, :cond_0

    const v3, 0x7f0d042f

    :goto_0
    array-length v6, v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    move-object v1, v4

    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v7, Lcom/google/android/voicesearch/ime/view/LanguageSpinner$2;

    invoke-direct {v7, p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner$2;-><init>(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)V

    invoke-virtual {v6, v1, v7}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/google/android/voicesearch/ime/view/LanguageSpinner$1;

    invoke-direct {v7, p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner$1;-><init>(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0d042e

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mDialog:Landroid/app/AlertDialog;

    iget-object v6, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getWindowToken()Landroid/os/IBinder;

    move-result-object v6

    iput-object v6, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    const/16 v6, 0x3eb

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    invoke-virtual {v5, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const/high16 v6, 0x20000

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    iget-object v6, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    return v8

    :cond_0
    const v3, 0x7f0d0430

    goto :goto_0
.end method

.method public setCallback(Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mCallback:Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;

    return-void
.end method

.method public setLanguages(Ljava/lang/String;[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    const/4 v1, 0x1

    const/4 v2, 0x0

    array-length v0, p2

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getCurrentDialect(Ljava/lang/String;[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mCurrentDialect:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mDialects:[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f04005e

    new-array v1, v1, [Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->mCurrentDialect:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    invoke-virtual {v5}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-direct {v0, v3, v4, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method
