.class public interface abstract Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;
.super Ljava/lang/Object;
.source "VoiceInputViewHandler.java"

# interfaces
.implements Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract forceClose()V
.end method

.method public abstract startRecognition()V
.end method

.method public abstract stopRecognition()V
.end method
