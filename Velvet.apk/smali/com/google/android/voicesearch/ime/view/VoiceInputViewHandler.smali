.class public Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
.super Ljava/lang/Object;
.source "VoiceInputViewHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;,
        Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;
    }
.end annotation


# instance fields
.field private mClickables:[Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private mImageIndicator:Landroid/widget/ImageView;

.field private mImeStateView:Landroid/widget/TextView;

.field private mInputView:Landroid/view/View;

.field private mLanguageView:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

.field private mPrevImeView:Landroid/widget/ImageView;

.field private mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

.field private final mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

.field private mStartRecognitionListener:Landroid/view/View$OnClickListener;

.field private mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

.field private mStopButton:Landroid/widget/Button;

.field private mStopRecognitionListener:Landroid/view/View$OnClickListener;

.field private mTitleView:Landroid/widget/TextView;

.field private final mViewBuilder:Lcom/google/android/voicesearch/ime/view/ViewBuilder;

.field private mViews:[Landroid/view/View;

.field private mWaitingForResultBar:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/speech/SpeechLevelSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-static {p1}, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->create(Landroid/content/Context;)Lcom/google/android/voicesearch/ime/view/ViewBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mViewBuilder:Lcom/google/android/voicesearch/ime/view/ViewBuilder;

    return-void
.end method

.method private setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 4
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mClickables:[Landroid/view/View;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    if-eqz v3, :cond_0

    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private showView(Landroid/view/View;Ljava/util/HashSet;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/HashSet",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private varargs showViews([Landroid/view/View;)V
    .locals 5
    .param p1    # [Landroid/view/View;

    invoke-static {p1}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mViews:[Landroid/view/View;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-direct {p0, v3, v4}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->showView(Landroid/view/View;Ljava/util/HashSet;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public displayAudioNotInitialized()V
    .locals 3

    const/4 v1, 0x1

    sget-object v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->AUDIO_NOT_INITIALIZED:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setKeepScreenOn(Z)V

    new-array v0, v1, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->showViews([Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    const v1, 0x7f0200bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public displayError(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->ERROR:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setKeepScreenOn(Z)V

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mPrevImeView:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->showViews([Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public displayListening()V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->LISTENING:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mLanguageView:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->showViews([Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020133

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStopRecognitionListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    const v1, 0x7f0d03ca

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public displayPause(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->PAUSED:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mLanguageView:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mPrevImeView:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->showViews([Landroid/view/View;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mWaitingForResultBar:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020130

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStartRecognitionListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    const v1, 0x7f0d0429

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public displayRecording()V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->RECORDING:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mLanguageView:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->showViews([Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020136

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStopRecognitionListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    const v1, 0x7f0d042a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public displayWorking()V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->WORKING:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mTitleView:Landroid/widget/TextView;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mWaitingForResultBar:Landroid/view/View;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStopButton:Landroid/widget/Button;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->showViews([Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mTitleView:Landroid/widget/TextView;

    const v1, 0x7f0d042b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public getView(Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;)Landroid/view/View;
    .locals 8
    .param p1    # Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mViewBuilder:Lcom/google/android/voicesearch/ime/view/ViewBuilder;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->createView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    new-instance v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$1;-><init>(Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStartRecognitionListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$2;-><init>(Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStopRecognitionListener:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v1, 0x7f100059

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mTitleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v1, 0x7f10013f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->setLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v1, 0x7f100141

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v1, 0x7f100137

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mLanguageView:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mLanguageView:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->setCallback(Lcom/google/android/voicesearch/ime/view/LanguageSpinner$Callback;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v1, 0x7f100139

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mPrevImeView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mPrevImeView:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$3;-><init>(Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v1, 0x7f10013c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v1, 0x7f100140

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mWaitingForResultBar:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v1, 0x7f10013d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStopButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStopButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$4;-><init>(Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v1, v7, [Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v2, 0x7f10013b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v3

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v2, 0x7f10013a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v4

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v2, 0x7f10013c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v5

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    const v2, 0x7f100138

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    aput-object v0, v1, v6

    iput-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mClickables:[Landroid/view/View;

    const/16 v0, 0x8

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mTitleView:Landroid/widget/TextView;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mSoundLevels:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImageIndicator:Landroid/widget/ImageView;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mLanguageView:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mPrevImeView:Landroid/widget/ImageView;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mImeStateView:Landroid/widget/TextView;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mStopButton:Landroid/widget/Button;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mWaitingForResultBar:Landroid/view/View;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mViews:[Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mInputView:Landroid/view/View;

    return-object v0
.end method

.method public hideWaitingForResults()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mWaitingForResultBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public isListening()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    sget-object v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->LISTENING:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    sget-object v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->PAUSED:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecording()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    sget-object v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->RECORDING:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreState()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    sget-object v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->RECORDING:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayRecording()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    sget-object v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->LISTENING:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayListening()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    sget-object v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->WORKING:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayWorking()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    sget-object v1, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;->AUDIO_NOT_INITIALIZED:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayAudioNotInitialized()V

    goto :goto_0

    :cond_3
    const-string v0, "VoiceInputViewHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restored into unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mState:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayPause(Z)V

    goto :goto_0
.end method

.method public setLanguages(Ljava/lang/String;[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->mLanguageView:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->setLanguages(Ljava/lang/String;[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)V

    return-void
.end method
