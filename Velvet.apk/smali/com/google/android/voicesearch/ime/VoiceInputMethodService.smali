.class public Lcom/google/android/voicesearch/ime/VoiceInputMethodService;
.super Landroid/inputmethodservice/InputMethodService;
.source "VoiceInputMethodService.java"


# instance fields
.field private mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;-><init>()V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3}, Landroid/inputmethodservice/InputMethodService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public hideWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleHideWindow()V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->hideWindow()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->create(Landroid/inputmethodservice/InputMethodService;)Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    return-void
.end method

.method public onCreateInputView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleCreateInputView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleDestroy()V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onDestroy()V

    return-void
.end method

.method public onEvaluateFullscreenMode()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->isMaybeForceFullScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateFullscreenMode()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFinishInput()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleFinishInput()V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onFinishInput()V

    return-void
.end method

.method public onFinishInputView(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleFinishInputView(Z)V

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onFinishInputView(Z)V

    return-void
.end method

.method public onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 1
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->cancelSendEvents(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method public onUpdateSelection(IIIIII)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-super/range {p0 .. p6}, Landroid/inputmethodservice/InputMethodService;->onUpdateSelection(IIIIII)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleUpdateSelection(IIIIII)V

    return-void
.end method

.method public onViewClicked(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleViewClicked(Z)V

    return-void
.end method

.method public showWindow(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->mVoiceInputManager:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleShowWindow(Z)V

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->showWindow(Z)V

    return-void
.end method
