.class public interface abstract Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;
.super Ljava/lang/Object;
.source "VoiceImeInputMethodService.java"


# virtual methods
.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;
.end method

.method public abstract getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;
.end method

.method public abstract getRecognizer()Lcom/google/android/speech/Recognizer;
.end method

.method public abstract getResources()Landroid/content/res/Resources;
.end method

.method public abstract isScreenOn()Z
.end method

.method public abstract scheduleSendEvents()V
.end method

.method public abstract switchToLastInputMethod()V
.end method
