.class Lcom/google/android/voicesearch/util/LocalTtsManager$5;
.super Ljava/lang/Object;
.source "LocalTtsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/LocalTtsManager;->onUtteranceCompleted(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field final synthetic val$s:Ljava/lang/String;

.field final synthetic val$utterance:Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$5;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$5;->val$s:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$5;->val$utterance:Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$5;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbackLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$100(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$5;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbacksMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$200(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$5;->val$s:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$5;->val$utterance:Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;

    if-eq v0, v2, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$5;->val$utterance:Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;

    iget-object v0, v0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->completionCallback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
