.class public Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;
.super Ljava/lang/Object;
.source "CalendarHelper.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/util/CalendarHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Reminder"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mMethod:I

.field private final mMinutesInAdvance:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->mMinutesInAdvance:I

    iput p2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->mMethod:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getMethod()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->mMethod:I

    return v0
.end method

.method public getMinutesInAdvance()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->mMinutesInAdvance:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->mMinutesInAdvance:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->mMethod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
