.class Lcom/google/android/voicesearch/util/LocalTtsManager$1;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "LocalTtsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/util/LocalTtsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/LocalTtsManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$1;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$1;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # invokes: Lcom/google/android/voicesearch/util/LocalTtsManager;->onUtteranceCompleted(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$000(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;)V

    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$1;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbackLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$100(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$1;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbacksMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$200(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;

    if-eqz v7, :cond_0

    iget v0, v7, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->networkTtsFlag:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$1;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbacksMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$200(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v7

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$1;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iget-object v1, v6, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->utterance:Ljava/lang/String;

    iget-object v2, v6, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->utteranceId:Ljava/lang/String;

    iget v3, v6, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->audioStream:I

    iget-object v4, v6, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->completionCallback:Ljava/lang/Runnable;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V

    :goto_1
    return-void

    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$1;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # invokes: Lcom/google/android/voicesearch/util/LocalTtsManager;->onUtteranceCompleted(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$000(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method
