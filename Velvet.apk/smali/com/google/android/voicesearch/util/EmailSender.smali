.class public Lcom/google/android/voicesearch/util/EmailSender;
.super Ljava/lang/Object;
.source "EmailSender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/util/EmailSender$Email;
    }
.end annotation


# instance fields
.field private final mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/helper/AccountHelper;Landroid/content/pm/PackageManager;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/helper/AccountHelper;
    .param p2    # Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/util/EmailSender;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/EmailSender;->mPackageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/util/EmailSender;Lcom/google/android/voicesearch/util/EmailSender$Email;ZLjava/lang/String;ZLcom/google/android/searchcommon/util/IntentStarter;)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/EmailSender;
    .param p1    # Lcom/google/android/voicesearch/util/EmailSender$Email;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Lcom/google/android/searchcommon/util/IntentStarter;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/voicesearch/util/EmailSender;->sendEmail(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLjava/lang/String;ZLcom/google/android/searchcommon/util/IntentStarter;)Z

    move-result v0

    return v0
.end method

.method private addCommonExtras(Lcom/google/android/voicesearch/util/EmailSender$Email;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/util/EmailSender$Email;
    .param p2    # Landroid/content/Intent;

    iget-object v0, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->to:[Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "android.intent.extra.EMAIL"

    iget-object v1, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->to:[Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    iget-object v0, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->cc:[Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "android.intent.extra.CC"

    iget-object v1, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->cc:[Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v0, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->bcc:[Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, "android.intent.extra.BCC"

    iget-object v1, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->bcc:[Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-object v0, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->subject:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    const-string v0, "android.intent.extra.SUBJECT"

    iget-object v1, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->subject:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    iget-object v0, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->body:Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    const-string v0, "android.intent.extra.TEXT"

    iget-object v1, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->body:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_4
    iget-object v0, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->attachment:Landroid/net/Uri;

    if-eqz v0, :cond_5

    const-string v0, "android.intent.extra.STREAM"

    iget-object v1, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->attachment:Landroid/net/Uri;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_5
    return-void
.end method

.method private createGMailIntent(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLjava/lang/String;Z)Landroid/content/Intent;
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/util/EmailSender$Email;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    new-instance v0, Landroid/content/Intent;

    if-eqz p2, :cond_1

    const-string v1, "com.google.android.gm.action.AUTO_SEND"

    :goto_0
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p4, :cond_3

    invoke-direct {p0}, Lcom/google/android/voicesearch/util/EmailSender;->installedGmailSupportsNoteCategory()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.google.android.gm"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    :goto_1
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gm.extra.ACCOUNT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/util/EmailSender;->addCommonExtras(Lcom/google/android/voicesearch/util/EmailSender$Email;Landroid/content/Intent;)V

    return-object v0

    :cond_1
    const-string v1, "android.intent.action.SEND"

    goto :goto_0

    :cond_2
    if-nez p2, :cond_0

    const-string v1, "com.google.android.voicesearch.SELF_NOTE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_3
    const-string v1, "com.google.android.gm"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private installedGmailSupportsNoteCategory()Z
    .locals 3

    const/4 v1, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "text/plain"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.voicesearch.SELF_NOTE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.gm"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/voicesearch/util/EmailSender;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private sendEmail(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLjava/lang/String;ZLcom/google/android/searchcommon/util/IntentStarter;)Z
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/util/EmailSender$Email;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Lcom/google/android/searchcommon/util/IntentStarter;

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    iget-object v1, p1, Lcom/google/android/voicesearch/util/EmailSender$Email;->to:[Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/voicesearch/util/EmailSender;->createGMailIntent(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {p5, v1}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p3, p4}, Lcom/google/android/voicesearch/util/EmailSender;->createGMailIntent(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {p5, v1}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/util/EmailSender;->createEmailIntent(Lcom/google/android/voicesearch/util/EmailSender$Email;)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {p5, v0}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public createEmailIntent(Lcom/google/android/voicesearch/util/EmailSender$Email;)Landroid/content/Intent;
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/util/EmailSender$Email;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "mailto"

    const-string v2, ""

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/util/EmailSender;->addCommonExtras(Lcom/google/android/voicesearch/util/EmailSender$Email;Landroid/content/Intent;)V

    return-object v0
.end method

.method public sendEmail(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/util/EmailSender$Email;
    .param p2    # Z
    .param p3    # Lcom/google/android/searchcommon/util/IntentStarter;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/EmailSender;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    new-instance v1, Lcom/google/android/voicesearch/util/EmailSender$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/voicesearch/util/EmailSender$1;-><init>(Lcom/google/android/voicesearch/util/EmailSender;Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V

    invoke-interface {v0, v1}, Lcom/google/android/speech/helper/AccountHelper;->getMainGmailAccount(Lcom/google/android/speech/callback/SimpleCallback;)V

    return-void
.end method

.method public sendEmailToSelf(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/util/EmailSender$Email;
    .param p2    # Z
    .param p3    # Lcom/google/android/searchcommon/util/IntentStarter;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/EmailSender;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    new-instance v1, Lcom/google/android/voicesearch/util/EmailSender$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/voicesearch/util/EmailSender$2;-><init>(Lcom/google/android/voicesearch/util/EmailSender;Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V

    invoke-interface {v0, v1}, Lcom/google/android/speech/helper/AccountHelper;->getMainGmailAccount(Lcom/google/android/speech/callback/SimpleCallback;)V

    return-void
.end method
