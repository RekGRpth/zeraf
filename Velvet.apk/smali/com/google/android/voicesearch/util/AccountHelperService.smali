.class public Lcom/google/android/voicesearch/util/AccountHelperService;
.super Landroid/app/IntentService;
.source "AccountHelperService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "AccountHelperService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static start(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/voicesearch/util/AccountHelperService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v5

    invoke-virtual {v8}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;

    move-result-object v0

    invoke-virtual {v5}, Lcom/google/android/voicesearch/settings/Settings;->getAuthTokenLastRefreshTimestamp()J

    move-result-wide v1

    invoke-virtual {v5}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getAuth()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;->getAuthTokenProactivelyInvalidatePeriodMsec()I

    move-result v9

    int-to-long v6, v9

    add-long v3, v1, v6

    const-wide/16 v9, 0x0

    cmp-long v9, v1, v9

    if-eqz v9, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    cmp-long v9, v9, v3

    if-lez v9, :cond_0

    invoke-interface {v0}, Lcom/google/android/speech/helper/AccountHelper;->invalidateAuthTokens()V

    const-wide/16 v9, 0x1

    sget-object v11, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v9, v10, v11}, Lcom/google/android/speech/helper/AccountHelper;->blockingGetAuthTokens(JLjava/util/concurrent/TimeUnit;)Ljava/util/List;

    :cond_0
    return-void
.end method
