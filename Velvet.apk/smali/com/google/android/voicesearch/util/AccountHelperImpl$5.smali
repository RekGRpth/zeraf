.class Lcom/google/android/voicesearch/util/AccountHelperImpl$5;
.super Ljava/lang/Object;
.source "AccountHelperImpl.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/AccountHelperImpl;->getMainGmailAccount(Lcom/google/android/speech/callback/SimpleCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<[",
        "Landroid/accounts/Account;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

.field final synthetic val$callback:Lcom/google/android/speech/callback/SimpleCallback;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/AccountHelperImpl;Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$5;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$5;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [Landroid/accounts/Account;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/util/AccountHelperImpl$5;->onResult([Landroid/accounts/Account;)V

    return-void
.end method

.method public onResult([Landroid/accounts/Account;)V
    .locals 2
    .param p1    # [Landroid/accounts/Account;

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$5;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-interface {v0, v1}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$5;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    goto :goto_0
.end method
