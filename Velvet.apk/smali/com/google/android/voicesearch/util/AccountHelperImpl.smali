.class public Lcom/google/android/voicesearch/util/AccountHelperImpl;
.super Ljava/lang/Object;
.source "AccountHelperImpl.java"

# interfaces
.implements Lcom/google/android/speech/helper/AccountHelper;


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mAuthTokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchSettings;)V
    .locals 1
    .param p1    # Landroid/accounts/AccountManager;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAccountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/util/AccountHelperImpl;)[Landroid/accounts/Account;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/AccountHelperImpl;

    invoke-direct {p0}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/util/AccountHelperImpl;)Landroid/accounts/AccountManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/AccountHelperImpl;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAccountManager:Landroid/accounts/AccountManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/util/AccountHelperImpl;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/AccountHelperImpl;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/util/AccountHelperImpl;[Landroid/accounts/Account;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/util/AccountHelperImpl;
    .param p1    # [Landroid/accounts/Account;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->maybeReorderAccounts([Landroid/accounts/Account;)V

    return-void
.end method

.method private static asFuture(Landroid/accounts/AccountManagerFuture;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/accounts/AccountManagerFuture",
            "<TT;>;)",
            "Ljava/util/concurrent/Future",
            "<TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/util/AccountHelperImpl$3;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/util/AccountHelperImpl$3;-><init>(Landroid/accounts/AccountManagerFuture;)V

    return-object v0
.end method

.method private getAuthTokenFuture(Landroid/accounts/Account;Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;
    .locals 7
    .param p1    # Landroid/accounts/Account;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            ")",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAccountManager:Landroid/accounts/AccountManager;

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method private getAuthTokenFuture(Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<",
            "Landroid/os/Bundle;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/util/AccountHelperImpl$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/util/AccountHelperImpl$2;-><init>(Lcom/google/android/voicesearch/util/AccountHelperImpl;)V

    invoke-static {p1, v0}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->transform(Ljava/util/concurrent/Future;Lcom/google/common/base/Function;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method private getGoogleAccounts()[Landroid/accounts/Account;
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->maybeReorderAccounts([Landroid/accounts/Account;)V

    return-object v0
.end method

.method private maybeReorderAccounts([Landroid/accounts/Account;)V
    .locals 6
    .param p1    # [Landroid/accounts/Account;

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v3}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    array-length v3, p1

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    aget-object v3, p1, v5

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_0

    aget-object v3, p1, v1

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    aget-object v2, p1, v1

    aget-object v3, p1, v5

    aput-object v3, p1, v1

    aput-object v2, p1, v5

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public blockingGetAuthTokens(JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
    .locals 23
    .param p1    # J
    .param p3    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/voicesearch/settings/Settings;->getAuthTokenLastRefreshTimestamp()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getAuth()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Authentication;->getAuthTokenInvalidateBeforeUsePeriodMsec()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v20, v6, v20

    if-lez v20, :cond_0

    add-long v20, v6, v18

    cmp-long v20, v20, v15

    if-gez v20, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->invalidateAuthTokens()V

    :cond_0
    const-wide/16 v20, 0x0

    cmp-long v20, v6, v20

    if-nez v20, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/voicesearch/settings/Settings;->updateAuthTokenLastRefreshTimestamp()V

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;

    move-object/from16 v21, v0

    monitor-enter v21

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->clear()V

    monitor-exit v21
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v5

    array-length v14, v5

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v14, :cond_3

    aget-object v4, v5, v12

    const-string v20, "speech"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v4, v1}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->getAuthTokenFuture(Landroid/accounts/Account;Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-static {v10}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->asFuture(Landroid/accounts/AccountManagerFuture;)Ljava/util/concurrent/Future;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->getAuthTokenFuture(Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :catchall_0
    move-exception v20

    :try_start_1
    monitor-exit v21
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v20

    :cond_3
    invoke-static {v11}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->successfulAsList(Ljava/util/List;)Ljava/util/concurrent/Future;

    move-result-object v17

    const/4 v8, 0x0

    :try_start_2
    move-object/from16 v0, v17

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    invoke-interface {v0, v1, v2, v3}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Ljava/util/List;

    move-object v8, v0

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_4
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    if-nez v20, :cond_4

    const-string v20, "AccountHelperImpl"

    const-string v21, "Found null auth in list."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v13}, Ljava/util/Iterator;->remove()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_0
    move-exception v9

    const-string v20, "AccountHelperImpl"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Interrupted waiting for auth: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v9}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v20, 0x0

    :goto_2
    return-object v20

    :catch_1
    move-exception v9

    const-string v20, "AccountHelperImpl"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error fetching auth: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v9}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v20, 0x0

    goto :goto_2

    :catch_2
    move-exception v9

    const-string v20, "AccountHelperImpl"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Timed out fetching auth: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v9}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v20, 0x0

    goto :goto_2

    :cond_5
    move-object/from16 v20, v8

    goto :goto_2
.end method

.method public getGmailAccounts(Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<[",
            "Landroid/accounts/Account;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "mail"

    invoke-static {v4}, Lcom/google/android/gsf/GoogleLoginServiceConstants;->featureForService(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Lcom/google/android/voicesearch/util/AccountHelperImpl$4;

    invoke-direct {v3, p0, p1}, Lcom/google/android/voicesearch/util/AccountHelperImpl$4;-><init>(Lcom/google/android/voicesearch/util/AccountHelperImpl;Lcom/google/android/speech/callback/SimpleCallback;)V

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method public getMainGmailAccount(Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Landroid/accounts/Account;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/util/AccountHelperImpl$5;

    invoke-direct {v0, p0, p1}, Lcom/google/android/voicesearch/util/AccountHelperImpl$5;-><init>(Lcom/google/android/voicesearch/util/AccountHelperImpl;Lcom/google/android/speech/callback/SimpleCallback;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->getGmailAccounts(Lcom/google/android/speech/callback/SimpleCallback;)V

    return-void
.end method

.method public getServiceName()Ljava/lang/String;
    .locals 1

    const-string v0, "speech"

    return-object v0
.end method

.method public hasGoogleAccount()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invalidateAuthTokens()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAccountManager:Landroid/accounts/AccountManager;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v4, "com.google"

    invoke-virtual {v2, v4, v0}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->resetAuthTokenLastRefreshTimestamp()V

    goto :goto_0
.end method

.method public promptForPermissions(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    new-instance v0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;-><init>(Lcom/google/android/voicesearch/util/AccountHelperImpl;Landroid/app/Activity;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
