.class Lcom/google/android/voicesearch/EffectOnWebResults$Creator;
.super Ljava/lang/Object;
.source "EffectOnWebResults.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/EffectOnWebResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Creator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/voicesearch/EffectOnWebResults;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/EffectOnWebResults$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/EffectOnWebResults$1;

    invoke-direct {p0}, Lcom/google/android/voicesearch/EffectOnWebResults$Creator;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/voicesearch/EffectOnWebResults;
    .locals 4
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    new-instance v2, Lcom/google/android/voicesearch/EffectOnWebResults;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/voicesearch/EffectOnWebResults;-><init>(ILjava/lang/String;Lcom/google/android/voicesearch/EffectOnWebResults$1;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/EffectOnWebResults$Creator;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/voicesearch/EffectOnWebResults;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/google/android/voicesearch/EffectOnWebResults;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/EffectOnWebResults$Creator;->newArray(I)[Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v0

    return-object v0
.end method
