.class public Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;
.super Landroid/os/AsyncTask;
.source "ActionEditorSetContactPictureTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

.field private final mContactPictureView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Lcom/google/android/voicesearch/ui/ActionEditorView;)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mContactPictureView:Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Long;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1    # [Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mContactPictureView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/speech/contacts/ContactLookup;->fetchPhotoBitmap(Landroid/content/ContentResolver;JZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->doInBackground([Ljava/lang/Long;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onContactPictureSet()V
    .locals 0

    return-void
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mContactPictureView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mContactPictureView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->onContactPictureSet()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->showPanelContent(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mContactPictureView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->showPanelContent(Z)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
