.class public Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;
.super Landroid/view/View;
.source "DrawSoundLevelsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;
    }
.end annotation


# instance fields
.field private mAnimator:Landroid/animation/TimeAnimator;

.field private mCurrentVolume:I

.field private mDisableBackgroundColor:I

.field private mEnableBackgroundColor:I

.field private final mLevelSize:I

.field private mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

.field private mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v2, Lcom/google/android/googlequicksearchbox/R$styleable;->DrawSoundLevelView:[I

    invoke-virtual {p1, p2, v2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const-string v2, "#66FFFFFF"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mEnableBackgroundColor:I

    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mDisableBackgroundColor:I

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getBaseLevelSize(Landroid/content/res/TypedArray;)I

    move-result v2

    iput v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mLevelSize:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v2, Lcom/google/android/speech/SpeechLevelSource;

    invoke-direct {v2}, Lcom/google/android/speech/SpeechLevelSource;-><init>()V

    iput-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v2, v4}, Lcom/google/android/speech/SpeechLevelSource;->setSpeechLevel(I)V

    new-instance v2, Landroid/animation/TimeAnimator;

    invoke-direct {v2}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v2, v3}, Landroid/animation/TimeAnimator;->setRepeatCount(I)V

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v2, v3, v4}, Landroid/animation/TimeAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    new-instance v3, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;

    invoke-direct {v3, p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;-><init>(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)V

    invoke-virtual {v2, v3}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->startAnimator()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)Lcom/google/android/speech/SpeechLevelSource;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)I
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mCurrentVolume:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;I)I
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mCurrentVolume:I

    return p1
.end method

.method private drawLevel(Landroid/graphics/Canvas;I)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    int-to-float v2, p2

    iget-object v3, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private getBaseLevelSize(Landroid/content/res/TypedArray;)I
    .locals 4
    .param p1    # Landroid/content/res/TypedArray;

    const/4 v2, 0x3

    const v3, 0x7f020131

    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    return v2
.end method

.method private startAnimator()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->startAnimator()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mEnableBackgroundColor:I

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mLevelSize:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mCurrentVolume:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mLevelSize:I

    add-int v0, v1, v2

    div-int/lit8 v1, v0, 0x2

    invoke-direct {p0, p1, v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->drawLevel(Landroid/graphics/Canvas;I)V

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mDisableBackgroundColor:I

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    instance-of v1, p1, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;

    if-nez v1, :cond_1

    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v1, v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;->mAnimationStarted:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->startAnimator()V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->isStarted()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;->mAnimationStarted:Z

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->startAnimator()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    goto :goto_0
.end method

.method public setLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    return-void
.end method
