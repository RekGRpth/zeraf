.class Lcom/google/android/voicesearch/ui/RecognizerView$1;
.super Ljava/lang/Object;
.source "RecognizerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/ui/RecognizerView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/ui/RecognizerView;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/ui/RecognizerView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/RecognizerView$1;->this$0:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    sget-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$2;->$SwitchMap$com$google$android$voicesearch$ui$RecognizerView$State:[I

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView$1;->this$0:Lcom/google/android/voicesearch/ui/RecognizerView;

    # getter for: Lcom/google/android/voicesearch/ui/RecognizerView;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;
    invoke-static {v1}, Lcom/google/android/voicesearch/ui/RecognizerView;->access$000(Lcom/google/android/voicesearch/ui/RecognizerView;)Lcom/google/android/voicesearch/ui/RecognizerView$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ui/RecognizerView$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView$1;->this$0:Lcom/google/android/voicesearch/ui/RecognizerView;

    # getter for: Lcom/google/android/voicesearch/ui/RecognizerView;->mCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;
    invoke-static {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->access$100(Lcom/google/android/voicesearch/ui/RecognizerView;)Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/ui/RecognizerView$Callback;->onCancelRecordingClicked()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView$1;->this$0:Lcom/google/android/voicesearch/ui/RecognizerView;

    # getter for: Lcom/google/android/voicesearch/ui/RecognizerView;->mCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;
    invoke-static {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->access$100(Lcom/google/android/voicesearch/ui/RecognizerView;)Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/ui/RecognizerView$Callback;->onStopRecordingClicked()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView$1;->this$0:Lcom/google/android/voicesearch/ui/RecognizerView;

    # getter for: Lcom/google/android/voicesearch/ui/RecognizerView;->mCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;
    invoke-static {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->access$100(Lcom/google/android/voicesearch/ui/RecognizerView;)Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/ui/RecognizerView$Callback;->onCancelRecordingClicked()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView$1;->this$0:Lcom/google/android/voicesearch/ui/RecognizerView;

    # getter for: Lcom/google/android/voicesearch/ui/RecognizerView;->mCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;
    invoke-static {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->access$100(Lcom/google/android/voicesearch/ui/RecognizerView;)Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/ui/RecognizerView$Callback;->onStartRecordingClicked()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
