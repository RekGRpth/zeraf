.class public Lcom/google/android/voicesearch/ui/TravelModeSpinner;
.super Landroid/widget/Spinner;
.source "TravelModeSpinner.java"


# instance fields
.field private mActionType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private getArrayLabels(I)[Ljava/lang/String;
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x3

    if-ne v1, p1, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0f0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getArrayValues(I)[I
    .locals 2
    .param p1    # I

    const/4 v0, 0x3

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method private setTransportationMethod(I)V
    .locals 4
    .param p1    # I

    iget v3, p0, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->mActionType:I

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getArrayValues(I)[I

    move-result-object v2

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget v3, v2, v0

    if-ne v3, p1, :cond_1

    move v1, v0

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setSelection(I)V

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getSelectedTransportationMethod()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->inSupportedActionType()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getSelectedItemPosition()I

    move-result v0

    iget v2, p0, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->mActionType:I

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getArrayValues(I)[I

    move-result-object v1

    aget v2, v1, v0

    goto :goto_0
.end method

.method public inSupportedActionType()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->mActionType:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->mActionType:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method public setActionType(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->mActionType:I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->inSupportedActionType()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0400ce

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->getArrayLabels(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setTransportationMethod(I)V

    goto :goto_0
.end method
