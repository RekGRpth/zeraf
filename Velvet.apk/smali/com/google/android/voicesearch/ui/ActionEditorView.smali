.class public Lcom/google/android/voicesearch/ui/ActionEditorView;
.super Landroid/widget/LinearLayout;
.source "ActionEditorView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;
    }
.end annotation


# instance fields
.field private mActionEditorListener:Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

.field private mCancelButton:Landroid/view/View;

.field private mClickCatcher:Landroid/view/View;

.field private mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

.field private mMainContent:Landroid/view/ViewGroup;

.field private mPanelContent:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x7f0e0047

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object v2, Lcom/google/android/googlequicksearchbox/R$styleable;->ResultCard:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ui/ActionEditorView;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->handleContentClicked()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/ui/ActionEditorView;)Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mActionEditorListener:Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

    return-object v0
.end method

.method private handleContentClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mActionEditorListener:Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mActionEditorListener:Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

    invoke-interface {v0}, Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;->onBailOut()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->cancelCountDownAnimation()V

    return-void
.end method


# virtual methods
.method public cancelCountDownAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->cancelCountDownAnimation()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    const/4 v3, 0x1

    const v1, 0x7f10001b

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mMainContent:Landroid/view/ViewGroup;

    const v1, 0x7f100019

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mPanelContent:Landroid/view/ViewGroup;

    const v1, 0x7f10007c

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/ui/CountDownView;

    iput-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    const v1, 0x7f10001c

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mClickCatcher:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mClickCatcher:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mClickCatcher:Landroid/view/View;

    new-instance v2, Lcom/google/android/voicesearch/ui/ActionEditorView$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/ui/ActionEditorView$1;-><init>(Lcom/google/android/voicesearch/ui/ActionEditorView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f10001a

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    new-instance v1, Lcom/google/android/voicesearch/ui/ActionEditorView$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/ui/ActionEditorView$2;-><init>(Lcom/google/android/voicesearch/ui/ActionEditorView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f100112

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCancelButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCancelButton:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCancelButton:Landroid/view/View;

    new-instance v2, Lcom/google/android/voicesearch/ui/ActionEditorView$3;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/ui/ActionEditorView$3;-><init>(Lcom/google/android/voicesearch/ui/ActionEditorView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setActionEditorListener(Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mActionEditorListener:Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->setActionEditorListener(Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;)V

    return-void
.end method

.method public setBailOutEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mMainContent:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mMainContent:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    return-void
.end method

.method public setConfirmIcon(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->setConfirmIcon(I)V

    return-void
.end method

.method public setConfirmIcon(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->setConfirmIcon(Landroid/view/View;)V

    return-void
.end method

.method public setConfirmText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->setConfirmText(I)V

    return-void
.end method

.method public setConfirmText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->setConfirmText(Ljava/lang/String;)V

    return-void
.end method

.method public setConfirmationEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->setConfirmationEnabled(Z)V

    return-void
.end method

.method public setContentClickable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mClickCatcher:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mClickCatcher:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public setMainContent(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mMainContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mMainContent:Landroid/view/ViewGroup;

    invoke-static {v0, p1, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public setNoConfirmIcon()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->setNoConfirmIcon()V

    return-void
.end method

.method public setPanelContent(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mPanelContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mPanelContent:Landroid/view/ViewGroup;

    invoke-static {v0, p1, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f100018

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showCountDownView(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showPanelContent(Z)V
    .locals 2
    .param p1    # Z

    const v0, 0x7f100018

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public startCountDownAnimation(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionEditorView;->mCountDownView:Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ui/CountDownView;->startCountDownAnimation(J)V

    return-void
.end method
