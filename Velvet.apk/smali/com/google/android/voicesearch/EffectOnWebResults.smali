.class public Lcom/google/android/voicesearch/EffectOnWebResults;
.super Ljava/lang/Object;
.source "EffectOnWebResults.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/EffectOnWebResults$1;,
        Lcom/google/android/voicesearch/EffectOnWebResults$Creator;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/voicesearch/EffectOnWebResults;",
            ">;"
        }
    .end annotation
.end field

.field public static final NO_EFFECT:Lcom/google/android/voicesearch/EffectOnWebResults;

.field public static final NO_WEB_RESULTS:Lcom/google/android/voicesearch/EffectOnWebResults;

.field public static final SUPPRESSED:Lcom/google/android/voicesearch/EffectOnWebResults;


# instance fields
.field private final mResultsEffect:I

.field private final mWebResultTypeToHide:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/voicesearch/EffectOnWebResults;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/EffectOnWebResults;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/EffectOnWebResults;->NO_EFFECT:Lcom/google/android/voicesearch/EffectOnWebResults;

    new-instance v0, Lcom/google/android/voicesearch/EffectOnWebResults;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/EffectOnWebResults;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/EffectOnWebResults;->SUPPRESSED:Lcom/google/android/voicesearch/EffectOnWebResults;

    new-instance v0, Lcom/google/android/voicesearch/EffectOnWebResults;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/EffectOnWebResults;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/EffectOnWebResults;->NO_WEB_RESULTS:Lcom/google/android/voicesearch/EffectOnWebResults;

    new-instance v0, Lcom/google/android/voicesearch/EffectOnWebResults$Creator;

    invoke-direct {v0, v2}, Lcom/google/android/voicesearch/EffectOnWebResults$Creator;-><init>(Lcom/google/android/voicesearch/EffectOnWebResults$1;)V

    sput-object v0, Lcom/google/android/voicesearch/EffectOnWebResults;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mResultsEffect:I

    iput-object p2, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mWebResultTypeToHide:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(ILjava/lang/String;Lcom/google/android/voicesearch/EffectOnWebResults$1;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/voicesearch/EffectOnWebResults$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/EffectOnWebResults;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method public static getPeanutEffect(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/android/voicesearch/EffectOnWebResults;
    .locals 3
    .param p0    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    new-instance v2, Lcom/google/android/voicesearch/EffectOnWebResults;

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getSearchResultsUnnecessary()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasWebSearchType()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getWebSearchType()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-direct {v2, v0, v1}, Lcom/google/android/voicesearch/EffectOnWebResults;-><init>(ILjava/lang/String;)V

    return-object v2

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getPumpkinEffect(Lcom/google/android/speech/embedded/TaggerResult;)Lcom/google/android/voicesearch/EffectOnWebResults;
    .locals 1
    .param p0    # Lcom/google/android/speech/embedded/TaggerResult;

    sget-object v0, Lcom/google/android/voicesearch/EffectOnWebResults;->SUPPRESSED:Lcom/google/android/voicesearch/EffectOnWebResults;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getWebResultTypeToHide()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mWebResultTypeToHide:Ljava/lang/String;

    return-object v0
.end method

.method public shouldShowNoWebResults()Z
    .locals 2

    iget v0, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mResultsEffect:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldSuppressWebResults()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mResultsEffect:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[FX:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mResultsEffect:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mWebResultTypeToHide:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mResultsEffect:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mWebResultTypeToHide:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/EffectOnWebResults;->mWebResultTypeToHide:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
