.class Lcom/google/android/voicesearch/handsfree/SpeakNowController;
.super Ljava/lang/Object;
.source "SpeakNowController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/SpeakNowController$AsyncContactRetrieverListener;,
        Lcom/google/android/voicesearch/handsfree/SpeakNowController$ActionListener;,
        Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;,
        Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;
    }
.end annotation


# instance fields
.field private final mActionProcessor:Lcom/google/android/voicesearch/handsfree/ActionProcessor;

.field private final mAsyncContactRetriever:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;

.field private final mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field private mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

.field private final mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

.field private mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

.field private final mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p2    # Lcom/google/android/voicesearch/util/LocalTtsManager;
    .param p3    # Lcom/google/android/voicesearch/handsfree/ViewDisplayer;
    .param p4    # Lcom/google/android/voicesearch/settings/Settings;
    .param p5    # Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;
    .param p6    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    invoke-static {p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mAsyncContactRetriever:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;

    new-instance v0, Lcom/google/android/voicesearch/handsfree/ActionProcessor;

    new-instance v1, Lcom/google/android/voicesearch/handsfree/SpeakNowController$ActionListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$ActionListener;-><init>(Lcom/google/android/voicesearch/handsfree/SpeakNowController;Lcom/google/android/voicesearch/handsfree/SpeakNowController$1;)V

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/ActionProcessor;-><init>(Lcom/google/android/voicesearch/handsfree/ActionProcessor$ActionListener;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mActionProcessor:Lcom/google/android/voicesearch/handsfree/ActionProcessor;

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object p6, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/ActionProcessor;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mActionProcessor:Lcom/google/android/voicesearch/handsfree/ActionProcessor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->handleNoMatch()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/MainController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mAsyncContactRetriever:Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;

    return-object v0
.end method

.method private getStartUpTts()I
    .locals 1

    const v0, 0x7f0d0380

    return v0
.end method

.method private handleNoMatch()V
    .locals 2

    const v1, 0x7f0d0464

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/voicesearch/handsfree/MainController;->showError(II)V

    return-void
.end method


# virtual methods
.method public setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/MainController;

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-void
.end method

.method public start()V
    .locals 5

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->showSpeakNow()Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    invoke-interface {v1, p0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;->setController(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    iget-object v3, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/handsfree/MainController;->getSpokenLanguageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;->setLanguage(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    invoke-interface {v1}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;->showStart()V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    new-instance v3, Lcom/google/android/voicesearch/handsfree/RecognizerUiAdapter;

    iget-object v4, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/handsfree/RecognizerUiAdapter;-><init>(Lcom/google/android/voicesearch/handsfree/RecognizerUi;)V

    invoke-virtual {v1, v3}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->attachUi(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;)V

    new-instance v0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$1;-><init>(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-static {v1, v0}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->getStartUpTts()I

    move-result v3

    const-string v4, "speak-now-tts"

    invoke-virtual {v1, v3, v4, v2, v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(ILjava/lang/String;ILjava/lang/Runnable;)V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method
