.class Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;
.super Ljava/lang/Object;
.source "PhoneCallDisambigContactController.java"

# interfaces
.implements Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InterpretationProcessorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;
    .param p2    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->cancelByVoice()V

    return-void
.end method

.method public onConfirm()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->handleVoiceErrror()V

    return-void
.end method

.method public onSelect(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-lt p1, v0, :cond_1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    add-int/lit8 p1, p1, -0x1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->access$400(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->handleVoiceErrror()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$InterpretationProcessorListener;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->mContacts:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->access$400(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->pickContactByVoice(Lcom/google/android/speech/contacts/Contact;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
