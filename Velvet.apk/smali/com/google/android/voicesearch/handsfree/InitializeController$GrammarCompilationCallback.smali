.class Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;
.super Ljava/lang/Object;
.source "InitializeController.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/InitializeController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GrammarCompilationCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/InitializeController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/handsfree/InitializeController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;->this$0:Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/handsfree/InitializeController;Lcom/google/android/voicesearch/handsfree/InitializeController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/InitializeController;
    .param p2    # Lcom/google/android/voicesearch/handsfree/InitializeController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;-><init>(Lcom/google/android/voicesearch/handsfree/InitializeController;)V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;->this$0:Lcom/google/android/voicesearch/handsfree/InitializeController;

    # invokes: Lcom/google/android/voicesearch/handsfree/InitializeController;->handleGrammarCompilationSuccess()V
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->access$400(Lcom/google/android/voicesearch/handsfree/InitializeController;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;->this$0:Lcom/google/android/voicesearch/handsfree/InitializeController;

    # invokes: Lcom/google/android/voicesearch/handsfree/InitializeController;->handleGrammarCompilationError()V
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->access$500(Lcom/google/android/voicesearch/handsfree/InitializeController;)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/handsfree/InitializeController$GrammarCompilationCallback;->onResult(Ljava/lang/Integer;)V

    return-void
.end method
