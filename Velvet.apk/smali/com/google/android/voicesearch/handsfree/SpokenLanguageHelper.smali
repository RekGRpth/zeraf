.class Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;
.super Ljava/lang/Object;
.source "SpokenLanguageHelper.java"


# instance fields
.field private final mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private mSpokenDisplayLanguage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method


# virtual methods
.method public getSpokenBcp47Locale()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v1}, Lcom/google/android/speech/embedded/Greco3DataManager;->waitForInitialization()V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v1, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasResourcesForCompilation(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "en-US"

    :cond_0
    return-object v0
.end method

.method public declared-synchronized getSpokenDisplayLanguageName()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mSpokenDisplayLanguage:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->getSpokenBcp47Locale()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mSpokenDisplayLanguage:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mSpokenDisplayLanguage:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hasResources()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->getSpokenBcp47Locale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasResourcesForCompilation(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
