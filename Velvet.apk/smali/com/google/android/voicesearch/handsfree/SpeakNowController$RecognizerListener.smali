.class final Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;
.super Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.source "SpeakNowController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/SpeakNowController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RecognizerListener"
.end annotation


# instance fields
.field private mActionProcessed:Z

.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    invoke-direct {p0}, Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/handsfree/SpeakNowController;Lcom/google/android/voicesearch/handsfree/SpeakNowController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;
    .param p2    # Lcom/google/android/voicesearch/handsfree/SpeakNowController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;-><init>(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    return-void
.end method


# virtual methods
.method public onDone()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # invokes: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->handleNoMatch()V
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$400(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    :cond_0
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # getter for: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$500(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;->showNotListening()V

    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # invokes: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->handleNoMatch()V
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$400(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    :cond_0
    return-void
.end method

.method public onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # getter for: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mActionProcessor:Lcom/google/android/voicesearch/handsfree/ActionProcessor;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$300(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/ActionProcessor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/ActionProcessor;->process(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # invokes: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->handleNoMatch()V
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$400(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    :cond_0
    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # invokes: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->handleNoMatch()V
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$400(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    :cond_0
    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # getter for: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mUi:Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$500(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$Ui;->showListening()V

    return-void
.end method

.method public onRecognitionCancelled()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->mActionProcessed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$RecognizerListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # getter for: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$600(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/MainController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    :cond_0
    return-void
.end method
