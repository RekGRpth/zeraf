.class public Lcom/google/android/voicesearch/handsfree/MainController;
.super Ljava/lang/Object;
.source "MainController.java"


# instance fields
.field private final mActivityCallback:Lcom/google/android/voicesearch/handsfree/ActivityCallback;

.field private final mErrorController:Lcom/google/android/voicesearch/handsfree/ErrorController;

.field private final mHandsFreeAudioRouter:Lcom/google/android/voicesearch/handsfree/AudioRouter;

.field private final mHandsFreeSpokenLanguageHelper:Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

.field private final mInitializeController:Lcom/google/android/voicesearch/handsfree/InitializeController;

.field private final mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field private final mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mPhoneCallContactController:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

.field private final mPhoneCallDisambigContactController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

.field private final mSpeakNowController:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

.field private final mUberRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/handsfree/ActivityCallback;Lcom/google/android/voicesearch/handsfree/InitializeController;Lcom/google/android/voicesearch/handsfree/SpeakNowController;Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;Lcom/google/android/voicesearch/handsfree/ErrorController;Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/handsfree/AudioRouter;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/ActivityCallback;
    .param p2    # Lcom/google/android/voicesearch/handsfree/InitializeController;
    .param p3    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;
    .param p4    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;
    .param p5    # Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;
    .param p6    # Lcom/google/android/voicesearch/handsfree/ErrorController;
    .param p7    # Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;
    .param p8    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p9    # Lcom/google/android/voicesearch/util/LocalTtsManager;
    .param p10    # Lcom/google/android/voicesearch/handsfree/AudioRouter;
    .param p11    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mActivityCallback:Lcom/google/android/voicesearch/handsfree/ActivityCallback;

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mInitializeController:Lcom/google/android/voicesearch/handsfree/InitializeController;

    iput-object p3, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mSpeakNowController:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    iput-object p4, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mPhoneCallDisambigContactController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    iput-object p5, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mPhoneCallContactController:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    iput-object p6, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mErrorController:Lcom/google/android/voicesearch/handsfree/ErrorController;

    iput-object p7, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mHandsFreeSpokenLanguageHelper:Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

    iput-object p8, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mUberRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iput-object p9, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object p10, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mHandsFreeAudioRouter:Lcom/google/android/voicesearch/handsfree/AudioRouter;

    iput-object p11, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-void
.end method


# virtual methods
.method public callContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mPhoneCallContactController:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->start(Lcom/google/android/speech/contacts/Contact;)V

    return-void
.end method

.method public callContacts(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/handsfree/MainController;->callContact(Lcom/google/android/speech/contacts/Contact;)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mPhoneCallDisambigContactController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->start(Ljava/util/List;)V

    goto :goto_1
.end method

.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mHandsFreeAudioRouter:Lcom/google/android/voicesearch/handsfree/AudioRouter;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/AudioRouter;->destroy()V

    return-void
.end method

.method public exit()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->stop()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mActivityCallback:Lcom/google/android/voicesearch/handsfree/ActivityCallback;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/ActivityCallback;->finish()V

    return-void
.end method

.method public getSpokenLanguageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mHandsFreeSpokenLanguageHelper:Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->getSpokenDisplayLanguageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public init()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mInitializeController:Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mSpeakNowController:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mPhoneCallDisambigContactController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mPhoneCallContactController:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mErrorController:Lcom/google/android/voicesearch/handsfree/ErrorController;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/handsfree/ErrorController;->setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V

    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mUberRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->stop()V

    return-void
.end method

.method public scheduleExit()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v1, Lcom/google/android/voicesearch/handsfree/MainController$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/handsfree/MainController$1;-><init>(Lcom/google/android/voicesearch/handsfree/MainController;)V

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public showError(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mErrorController:Lcom/google/android/voicesearch/handsfree/ErrorController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/handsfree/ErrorController;->start(II)V

    return-void
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mInitializeController:Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->start()V

    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mActivityCallback:Lcom/google/android/voicesearch/handsfree/ActivityCallback;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/ActivityCallback;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public startSpeakNow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/MainController;->mSpeakNowController:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->start()V

    return-void
.end method
