.class Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$2;
.super Ljava/lang/Object;
.source "PhoneCallDisambigContactView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->setContacts(Ljava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;

.field final synthetic val$contact:Lcom/google/android/speech/contacts/Contact;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;Lcom/google/android/speech/contacts/Contact;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$2;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$2;->val$contact:Lcom/google/android/speech/contacts/Contact;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/16 v0, 0x2c

    const/high16 v1, 0x1000000

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$2;->this$0:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;

    # getter for: Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->mController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->access$000(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;)Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$2;->val$contact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->pickContactByTouch(Lcom/google/android/speech/contacts/Contact;)V

    return-void
.end method
