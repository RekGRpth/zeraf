.class Lcom/google/android/voicesearch/handsfree/AudioRouter$1;
.super Ljava/lang/Object;
.source "AudioRouter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/handsfree/AudioRouter;->establishRoute(Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/AudioRouter;

.field final synthetic val$rescheduledListener:Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/handsfree/AudioRouter;Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;->this$0:Lcom/google/android/voicesearch/handsfree/AudioRouter;

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;->val$rescheduledListener:Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;->this$0:Lcom/google/android/voicesearch/handsfree/AudioRouter;

    # getter for: Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/AudioRouter;->access$000(Lcom/google/android/voicesearch/handsfree/AudioRouter;)Lcom/google/android/voicesearch/AudioRecorderRouter;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/AudioRecorderRouter;->start()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;->val$rescheduledListener:Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;->onAudioRouteFailed()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;->this$0:Lcom/google/android/voicesearch/handsfree/AudioRouter;

    # getter for: Lcom/google/android/voicesearch/handsfree/AudioRouter;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/AudioRouter;->access$100(Lcom/google/android/voicesearch/handsfree/AudioRouter;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->setAudioStream(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;->this$0:Lcom/google/android/voicesearch/handsfree/AudioRouter;

    # getter for: Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/AudioRouter;->access$200(Lcom/google/android/voicesearch/handsfree/AudioRouter;)Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;->val$rescheduledListener:Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;->onAudioRouteEstablished()V

    return-void
.end method
