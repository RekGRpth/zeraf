.class Lcom/google/android/voicesearch/handsfree/RecognizerUiAdapter;
.super Ljava/lang/Object;
.source "RecognizerUiAdapter.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;


# instance fields
.field private final mRecognizerUi:Lcom/google/android/voicesearch/handsfree/RecognizerUi;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/handsfree/RecognizerUi;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/RecognizerUi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/RecognizerUiAdapter;->mRecognizerUi:Lcom/google/android/voicesearch/handsfree/RecognizerUi;

    return-void
.end method


# virtual methods
.method public setFinalRecognizedText(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerUiAdapter;->mRecognizerUi:Lcom/google/android/voicesearch/handsfree/RecognizerUi;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/handsfree/RecognizerUi;->setLanguage(Ljava/lang/String;)V

    return-void
.end method

.method public setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    return-void
.end method

.method public showInitializing()V
    .locals 0

    return-void
.end method

.method public showInitializingMic()V
    .locals 0

    return-void
.end method

.method public showListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerUiAdapter;->mRecognizerUi:Lcom/google/android/voicesearch/handsfree/RecognizerUi;

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/RecognizerUi;->showListening()V

    return-void
.end method

.method public showNotListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/RecognizerUiAdapter;->mRecognizerUi:Lcom/google/android/voicesearch/handsfree/RecognizerUi;

    invoke-interface {v0}, Lcom/google/android/voicesearch/handsfree/RecognizerUi;->showNotListening()V

    return-void
.end method

.method public showRecognizing()V
    .locals 0

    return-void
.end method

.method public showRecording()V
    .locals 0

    return-void
.end method

.method public showSoundSearchPromotedQuery()V
    .locals 0

    return-void
.end method

.method public showTapToSpeak()V
    .locals 0

    return-void
.end method

.method public updateRecognizedText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method
