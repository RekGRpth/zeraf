.class public Lcom/google/android/voicesearch/handsfree/AudioRouter;
.super Ljava/lang/Object;
.source "AudioRouter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;
    }
.end annotation


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;

.field private final mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/media/AudioManager;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/voicesearch/AudioRecorderRouter;)V
    .locals 0
    .param p1    # Landroid/media/AudioManager;
    .param p2    # Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    .param p3    # Ljava/util/concurrent/Executor;
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
    .param p5    # Lcom/google/android/voicesearch/AudioRecorderRouter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioManager:Landroid/media/AudioManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    iput-object p5, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;

    iput-object p4, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p3, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mUiExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/handsfree/AudioRouter;)Lcom/google/android/voicesearch/AudioRecorderRouter;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/AudioRouter;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/handsfree/AudioRouter;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/AudioRouter;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/handsfree/AudioRouter;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/AudioRouter;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;

    invoke-interface {v0}, Lcom/google/android/voicesearch/AudioRecorderRouter;->requestStop()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->setAudioStream(I)V

    return-void
.end method

.method public establishRoute(Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mUiExecutor:Ljava/util/concurrent/Executor;

    invoke-static {v1, p1}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/AudioRouter;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/voicesearch/handsfree/AudioRouter$1;-><init>(Lcom/google/android/voicesearch/handsfree/AudioRouter;Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
