.class public Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;
.super Ljava/lang/Object;
.source "VelvetCardController.java"

# interfaces
.implements Lcom/google/android/voicesearch/speechservice/ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/VelvetCardController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ActionListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/VelvetCardController;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/VelvetCardController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddCalendarEventAction(Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/CalendarEventController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/CalendarEventController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->start(Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;)V

    return-void
.end method

.method public onAtHomePowerControlAction(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/AtHomeController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AtHomeController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/fragments/AtHomeController;->start(Ljava/lang/String;I)V

    return-void
.end method

.method public onDictionaryResults(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/DictionaryResultController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->start(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)V

    return-void
.end method

.method public onEmailAction(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/EmailController;->start(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)V

    return-void
.end method

.method public onFlightResults(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/FlightResultController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/FlightResultController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/FlightResultController;->start(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)V

    return-void
.end method

.method public onGogglesAction()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$200(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$200(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isGogglesEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$500(Lcom/google/android/voicesearch/VelvetCardController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/goggles/GogglesUtils;->deviceSupportsGoggles(Lcom/google/android/velvet/VelvetApplication;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/TraceTracker;->newSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventStartSearch(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$400(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/voicesearch/VelvetCardController;->access$300(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/Query;->goggles(I)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->setModifiedQuery(Lcom/google/android/velvet/Query;)V

    :cond_0
    return-void
.end method

.method public onGogglesGenericResult(Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/GogglesGenericController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->start(Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;)V

    return-void
.end method

.method public onHelpAction(Lcom/google/majel/proto/ActionV2Protos$HelpAction;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$200(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$200(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isActionDiscoveryEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;->getTitle()Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;->getIntroduction()Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;->start(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction;->getFeatureList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v1}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v1

    const-class v3, Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-virtual {v1, v3}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/fragments/HelpController;->start(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onHighConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;->start(Lcom/google/android/voicesearch/speechservice/AnswerData;)V

    return-void
.end method

.method public onHtmlAnswer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;->start(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onImageResults(Lcom/google/android/voicesearch/speechservice/ImageResultData;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/ImageResultController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ImageResultController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/ImageResultController;->start(Lcom/google/android/voicesearch/speechservice/ImageResultData;)V

    return-void
.end method

.method public onMediumConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;->start(Lcom/google/android/voicesearch/speechservice/AnswerData;)V

    return-void
.end method

.method public onMultipleLocalResults(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->start(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V

    return-void
.end method

.method public onOpenAppPlayMediaAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;->start(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    return-void
.end method

.method public onOpenApplicationAction(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/OpenAppController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenAppController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/OpenAppController;->start(Ljava/lang/String;)V

    return-void
.end method

.method public onOpenBookAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/OpenBookController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenBookController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/OpenBookController;->start(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    return-void
.end method

.method public onOpenURLAction(Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/OpenUrlController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenUrlController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->start(Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;)V

    return-void
.end method

.method public onPhoneAction(Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$100(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->isTelephoneCapable()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0d0391

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(I)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->onPuntAction(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->start(Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;)V

    goto :goto_0
.end method

.method public onPlayMovieAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v1}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v1

    const-class v2, Lcom/google/android/voicesearch/fragments/PlayMovieController;

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PlayMovieController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/PlayMovieController;->start(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    return-void
.end method

.method public onPlayMusicAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/PlayMusicController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PlayMusicController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->start(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    return-void
.end method

.method public onPuntAction(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/PuntController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PuntController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/PuntController;->start(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V

    return-void
.end method

.method public onQueryCalendarAction()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->start()V

    return-void
.end method

.method public onRecognizedContact(Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/RecognizedContactController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->start(Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;)V

    return-void
.end method

.method public onSearchResults(Lcom/google/majel/proto/PeanutProtos$Url;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Url;

    return-void
.end method

.method public onSelfNoteAction(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/SelfNoteController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SelfNoteController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->start(Ljava/lang/String;)V

    return-void
.end method

.method public onSetAlarmAction(Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/SetAlarmController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SetAlarmController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->start(Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;)V

    return-void
.end method

.method public onSetReminderAction(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$200(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$200(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isMarinerEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/SetReminderController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SetReminderController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/SetReminderController;->start(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/PuntController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PuntController;

    const v1, 0x7f0d03f5

    const v2, 0x7f0d03f6

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    const-class v5, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v4, v5}, Lcom/google/android/voicesearch/VelvetCardController;->getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(IIILandroid/content/Intent;)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/PuntController;->start(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V

    goto :goto_0
.end method

.method public onShowContactInformationAction(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;I)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->start(Ljava/util/List;I)V

    return-void
.end method

.method public onSingleLocalResult(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->start(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V

    return-void
.end method

.method public onSmsAction(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$100(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->isTelephoneCapable()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0d0391

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(I)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->onPuntAction(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/MessageEditorController;->start(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)V

    goto :goto_0
.end method

.method public onSoundSearchAction()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mAction:Lcom/google/android/velvet/presenter/Action;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$400(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/voicesearch/VelvetCardController;->access$300(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->soundSearchFromAction()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->setModifiedQuery(Lcom/google/android/velvet/Query;)V

    return-void
.end method

.method public onSportsResult(Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/SportsResultController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SportsResultController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/SportsResultController;->start(Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V

    return-void
.end method

.method public onUnsupportedAction(Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;)V
    .locals 2
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;->getExplanation()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->onPuntAction(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V

    return-void
.end method

.method public onUpdateSocialNetwork(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->start(Ljava/lang/String;I)V

    return-void
.end method

.method public onWeatherResults(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VelvetCardController$ActionListenerImpl;->this$0:Lcom/google/android/voicesearch/VelvetCardController;

    # getter for: Lcom/google/android/voicesearch/VelvetCardController;->mControllerFactory:Lcom/google/android/voicesearch/fragments/ControllerFactory;
    invoke-static {v0}, Lcom/google/android/voicesearch/VelvetCardController;->access$000(Lcom/google/android/voicesearch/VelvetCardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;

    move-result-object v0

    const-class v1, Lcom/google/android/voicesearch/fragments/WeatherResultController;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/WeatherResultController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/WeatherResultController;->start(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)V

    return-void
.end method
