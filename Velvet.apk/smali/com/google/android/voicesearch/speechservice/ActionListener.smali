.class public interface abstract Lcom/google/android/voicesearch/speechservice/ActionListener;
.super Ljava/lang/Object;
.source "ActionListener.java"


# virtual methods
.method public abstract onAddCalendarEventAction(Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;)V
.end method

.method public abstract onAtHomePowerControlAction(Ljava/lang/String;I)V
.end method

.method public abstract onDictionaryResults(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)V
.end method

.method public abstract onEmailAction(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)V
.end method

.method public abstract onFlightResults(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)V
.end method

.method public abstract onGogglesAction()V
.end method

.method public abstract onGogglesGenericResult(Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;)V
.end method

.method public abstract onHelpAction(Lcom/google/majel/proto/ActionV2Protos$HelpAction;)V
.end method

.method public abstract onHighConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
.end method

.method public abstract onHtmlAnswer(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onImageResults(Lcom/google/android/voicesearch/speechservice/ImageResultData;)V
.end method

.method public abstract onMediumConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
.end method

.method public abstract onMultipleLocalResults(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V
.end method

.method public abstract onOpenAppPlayMediaAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
.end method

.method public abstract onOpenApplicationAction(Ljava/lang/String;)V
.end method

.method public abstract onOpenBookAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
.end method

.method public abstract onOpenURLAction(Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;)V
.end method

.method public abstract onPhoneAction(Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;)V
.end method

.method public abstract onPlayMovieAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
.end method

.method public abstract onPlayMusicAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
.end method

.method public abstract onPuntAction(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V
.end method

.method public abstract onQueryCalendarAction()V
.end method

.method public abstract onRecognizedContact(Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;)V
.end method

.method public abstract onSearchResults(Lcom/google/majel/proto/PeanutProtos$Url;)V
.end method

.method public abstract onSelfNoteAction(Ljava/lang/String;)V
.end method

.method public abstract onSetAlarmAction(Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;)V
.end method

.method public abstract onSetReminderAction(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)V
.end method

.method public abstract onShowContactInformationAction(Ljava/util/List;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract onSingleLocalResult(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V
.end method

.method public abstract onSmsAction(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)V
.end method

.method public abstract onSoundSearchAction()V
.end method

.method public abstract onSportsResult(Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V
.end method

.method public abstract onUnsupportedAction(Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;)V
.end method

.method public abstract onUpdateSocialNetwork(Ljava/lang/String;I)V
.end method

.method public abstract onWeatherResults(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)V
.end method
