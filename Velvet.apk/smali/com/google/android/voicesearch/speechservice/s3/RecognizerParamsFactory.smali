.class public Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;
.super Ljava/lang/Object;
.source "RecognizerParamsFactory.java"


# instance fields
.field private final mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/helper/AccountHelper;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/CoreSearchServices;)V
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;
    .param p3    # Lcom/google/android/speech/helper/AccountHelper;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Lcom/google/android/searchcommon/CoreSearchServices;

    new-instance v1, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;

    invoke-interface/range {p5 .. p5}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;-><init>(Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/apps/sidekick/inject/LocationOracle;)V

    invoke-interface/range {p5 .. p5}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    invoke-interface/range {p5 .. p5}, Lcom/google/android/searchcommon/CoreSearchServices;->getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getVersionCodeString()Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {p5 .. p5}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v6

    invoke-interface/range {p5 .. p5}, Lcom/google/android/searchcommon/CoreSearchServices;->getPinholeParamsBuilder()Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    move-result-object v8

    invoke-interface/range {p5 .. p5}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v9

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/WindowManager;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    new-instance v12, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-direct {v12, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    invoke-interface/range {p5 .. p5}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v13

    move-object/from16 v0, p3

    move-object/from16 v2, p2

    move-object/from16 v7, p4

    invoke-static/range {v0 .. v13}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->createRecognizerBuilderData(Lcom/google/android/speech/helper/AccountHelper;Lcom/google/android/speech/helper/SpeechLocationHelper;Lcom/google/android/speech/SpeechSettings;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/speech/utils/NetworkInformation;Ljava/lang/String;Lcom/google/android/searchcommon/SearchConfig;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;Lcom/google/android/searchcommon/UserAgentHelper;Landroid/view/WindowManager;Landroid/content/res/Resources;Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;-><init>(Lcom/google/android/speech/params/RecognizerBuilderData;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/speech/params/RecognizerBuilderData;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    return-void
.end method

.method private static createRecognizerBuilderData(Lcom/google/android/speech/helper/AccountHelper;Lcom/google/android/speech/helper/SpeechLocationHelper;Lcom/google/android/speech/SpeechSettings;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/speech/utils/NetworkInformation;Ljava/lang/String;Lcom/google/android/searchcommon/SearchConfig;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;Lcom/google/android/searchcommon/UserAgentHelper;Landroid/view/WindowManager;Landroid/content/res/Resources;Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/speech/params/RecognizerBuilderData;
    .locals 17
    .param p0    # Lcom/google/android/speech/helper/AccountHelper;
    .param p1    # Lcom/google/android/speech/helper/SpeechLocationHelper;
    .param p2    # Lcom/google/android/speech/SpeechSettings;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;
    .param p4    # Lcom/google/android/speech/utils/NetworkInformation;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/searchcommon/SearchConfig;
    .param p7    # Ljava/util/concurrent/ExecutorService;
    .param p8    # Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;
    .param p9    # Lcom/google/android/searchcommon/UserAgentHelper;
    .param p10    # Landroid/view/WindowManager;
    .param p11    # Landroid/content/res/Resources;
    .param p12    # Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;
    .param p13    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    new-instance v4, Lcom/google/android/speech/params/DeviceParamsImpl;

    new-instance v6, Lcom/google/android/speech/network/request/BrowserParamsSupplier;

    move-object/from16 v0, p12

    move-object/from16 v1, p13

    move-object/from16 v2, p9

    move-object/from16 v3, p10

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/google/android/speech/network/request/BrowserParamsSupplier;-><init>(Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/common/base/Supplier;Landroid/view/WindowManager;)V

    new-instance v7, Lcom/google/android/speech/network/request/PreviewParamsSupplier;

    move-object/from16 v0, p11

    invoke-direct {v7, v0}, Lcom/google/android/speech/network/request/PreviewParamsSupplier;-><init>(Landroid/content/res/Resources;)V

    invoke-static {v7}, Lcom/google/common/base/Suppliers;->memoize(Lcom/google/common/base/Supplier;)Lcom/google/common/base/Supplier;

    move-result-object v7

    new-instance v8, Lcom/google/android/speech/network/request/ScreenParamsSupplier;

    move-object/from16 v0, p10

    invoke-direct {v8, v0}, Lcom/google/android/speech/network/request/ScreenParamsSupplier;-><init>(Landroid/view/WindowManager;)V

    invoke-static {v8}, Lcom/google/common/base/Suppliers;->memoize(Lcom/google/common/base/Supplier;)Lcom/google/common/base/Supplier;

    move-result-object v8

    move-object/from16 v5, p5

    move-object/from16 v9, p9

    move-object/from16 v10, p6

    move-object/from16 v11, p3

    invoke-direct/range {v4 .. v11}, Lcom/google/android/speech/params/DeviceParamsImpl;-><init>(Ljava/lang/String;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;)V

    new-instance v5, Lcom/google/android/speech/params/RecognizerBuilderData;

    sget-object v11, Lcom/google/android/speech/params/RequestIdGenerator;->INSTANCE:Lcom/google/android/speech/params/RequestIdGenerator;

    move-object/from16 v6, p1

    move-object/from16 v7, p0

    move-object/from16 v8, p7

    move-object/from16 v9, p2

    move-object/from16 v10, p8

    move-object/from16 v12, p4

    move-object/from16 v13, p10

    move-object/from16 v14, p6

    move-object/from16 v15, p3

    move-object/from16 v16, v4

    invoke-direct/range {v5 .. v16}, Lcom/google/android/speech/params/RecognizerBuilderData;-><init>(Lcom/google/android/speech/helper/SpeechLocationHelper;Lcom/google/android/speech/helper/AccountHelper;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/SpeechSettings;Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;Lcom/google/android/speech/params/RequestIdGenerator;Lcom/google/android/speech/utils/NetworkInformation;Landroid/view/WindowManager;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/speech/params/DeviceParams;)V

    return-object v5
.end method


# virtual methods
.method public newBuilder()Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 2

    new-instance v0, Lcom/google/android/speech/params/RecognizerParamsBuilder;

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-direct {v0, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;-><init>(Lcom/google/android/speech/params/RecognizerBuilderData;)V

    return-object v0
.end method
