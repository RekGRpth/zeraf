.class public Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;
.super Ljava/lang/Object;
.source "TextRecognizer.java"


# instance fields
.field private final mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

.field private mCurrentCallback:Lcom/google/android/speech/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/majel/proto/MajelProtos$MajelResponse;",
            ">;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingRecognition:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mRecognitionExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

.field private final mSameThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

.field private final mServerInfo:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
    .param p2    # Lcom/google/android/speech/network/ConnectionFactory;
    .param p3    # Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/speech/network/ConnectionFactory;",
            "Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mRecognitionExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    iput-object p3, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    iput-object p4, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;

    iput-object p5, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mServerInfo:Lcom/google/common/base/Supplier;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mSameThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/common/base/Supplier;)V
    .locals 6
    .param p1    # Ljava/util/concurrent/ExecutorService;
    .param p2    # Lcom/google/android/speech/network/ConnectionFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/speech/network/ConnectionFactory;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;Lcom/google/android/velvet/Query;Lcom/google/android/speech/params/RecognizerParams;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/speech/params/RecognizerParams;
    .param p3    # Ljava/util/concurrent/Executor;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->getResponseAndDispatchCallback(Lcom/google/android/velvet/Query;Lcom/google/android/speech/params/RecognizerParams;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;)Lcom/google/android/speech/callback/Callback;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mCurrentCallback:Lcom/google/android/speech/callback/Callback;

    return-object v0
.end method

.method private createBaseActionRequest(Lcom/google/android/speech/params/RecognizerParams;)Lcom/google/speech/s3/S3$S3Request;
    .locals 8
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createBaseS3Request()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getService()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/speech/s3/S3$S3Request;->setService(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->waitForS3ClientInfo()Lcom/google/speech/s3/S3$S3ClientInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v0}, Lcom/google/speech/s3/S3$S3Request;->setS3ClientInfoExtension(Lcom/google/speech/s3/S3$S3ClientInfo;)Lcom/google/speech/s3/S3$S3Request;

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->waitForMajelClientInfo()Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v3, v5

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    move-object v3, v5

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v1}, Lcom/google/speech/s3/S3$S3Request;->setMajelClientInfoExtension(Lcom/google/speech/speech/s3/Majel$MajelClientInfo;)Lcom/google/speech/s3/S3$S3Request;

    new-instance v5, Lcom/google/speech/s3/S3$S3SessionInfo;

    invoke-direct {v5}, Lcom/google/speech/s3/S3$S3SessionInfo;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getRequestId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/speech/s3/S3$S3SessionInfo;->setSessionId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3SessionInfo;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/speech/s3/S3$S3Request;->setS3SessionInfoExtension(Lcom/google/speech/s3/S3$S3SessionInfo;)Lcom/google/speech/s3/S3$S3Request;

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->waitForS3UserInfo()Lcom/google/speech/s3/S3$S3UserInfo;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->waitForMobileUserInfo()Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    move-result-object v2

    if-eqz v4, :cond_3

    invoke-virtual {v3, v4}, Lcom/google/speech/s3/S3$S3Request;->setS3UserInfoExtension(Lcom/google/speech/s3/S3$S3UserInfo;)Lcom/google/speech/s3/S3$S3Request;

    :cond_3
    if-eqz v2, :cond_0

    invoke-virtual {v3, v2}, Lcom/google/speech/s3/S3$S3Request;->setMobileUserInfoExtension(Lcom/google/speech/s3/MobileUser$MobileUserInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0
.end method

.method private createMajelRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    invoke-direct {v0}, Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;->setQuery(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;->setOriginalQuery(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    :cond_0
    return-object v0
.end method

.method private createMajelTextActionRequest(Lcom/google/android/speech/params/RecognizerParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;
    .locals 3
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createBaseS3Request()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getService()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/s3/S3$S3Request;->setService(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v1

    invoke-direct {p0, p2, p3}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->createMajelRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/s3/S3$S3Request;->setMajelServiceRequestExtension(Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    return-object v0
.end method

.method private createTextActionRequest(Lcom/google/android/speech/params/RecognizerParams;Ljava/lang/String;Ljava/lang/String;)[Lcom/google/speech/s3/S3$S3Request;
    .locals 3
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/speech/s3/S3$S3Request;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->createBaseActionRequest(Lcom/google/android/speech/params/RecognizerParams;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->createMajelTextActionRequest(Lcom/google/android/speech/params/RecognizerParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createEndOfData()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method private doGetMajelResponse(Ljava/lang/String;Lcom/google/android/speech/params/RecognizerParams;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/params/RecognizerParams;

    const/4 v7, 0x0

    invoke-direct {p0, p2, p1, v7}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->createTextActionRequest(Lcom/google/android/speech/params/RecognizerParams;Ljava/lang/String;Ljava/lang/String;)[Lcom/google/speech/s3/S3$S3Request;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mServerInfo:Lcom/google/common/base/Supplier;

    invoke-interface {v8}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getUrl()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    invoke-interface {v8, v3, v6}, Lcom/google/android/speech/network/ConnectionFactory;->openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/android/speech/network/IoUtils;->addHttpHeaders(Ljava/net/HttpURLConnection;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)V

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    :try_start_1
    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->writeRequests(Ljava/net/HttpURLConnection;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;[Lcom/google/speech/s3/S3$S3Request;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    if-eqz v8, :cond_2

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    :cond_2
    const/16 v8, 0x18

    :try_start_2
    invoke-static {v8}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    const/16 v8, 0xc8

    if-eq v5, v8, :cond_3

    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unexpected response code: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v2

    :try_start_3
    const-string v8, "VS.TextRecognizer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Malformed URL: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    :cond_3
    :try_start_4
    invoke-virtual {v6}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v9

    invoke-virtual {v9}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v8

    if-nez v8, :cond_4

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    :cond_4
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_5
    .catch Ljava/net/MalformedURLException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v8

    if-eqz v8, :cond_5

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    :cond_5
    :try_start_6
    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->parseMajelServiceResponse(Ljava/net/HttpURLConnection;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    :try_end_6
    .catch Ljava/net/MalformedURLException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v7

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    :catch_1
    move-exception v2

    const/16 v8, 0x9

    :try_start_7
    invoke-static {v8}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    :catchall_0
    move-exception v7

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_6
    throw v7
.end method

.method private getResponseAndDispatchCallback(Lcom/google/android/velvet/Query;Lcom/google/android/speech/params/RecognizerParams;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/speech/params/RecognizerParams;
    .param p3    # Ljava/util/concurrent/Executor;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->doGetMajelResponse(Ljava/lang/String;Lcom/google/android/speech/params/RecognizerParams;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;-><init>(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;Lcom/google/majel/proto/MajelProtos$MajelResponse;Lcom/google/android/velvet/Query;)V

    invoke-interface {p3, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private parseMajelServiceResponse(Ljava/net/HttpURLConnection;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 5
    .param p1    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->createS3ResponseStream(Ljava/net/HttpURLConnection;)Lcom/google/android/speech/message/S3ResponseStream;

    move-result-object v2

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/speech/message/S3ResponseStream;->read()Lcom/google/speech/s3/S3$S3Response;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/s3/S3$S3Response;->hasMajelServiceEventExtension()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/speech/s3/S3$S3Response;->getMajelServiceEventExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    move-result-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    const/16 v3, 0x9

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    if-eqz v0, :cond_3

    const/16 v3, 0xc

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-static {v0}, Lcom/google/android/speech/message/S3ResponseProcessor;->processMajelServiceEvent(Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/16 v3, 0xa

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v3

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private writeRequests(Ljava/net/HttpURLConnection;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;[Lcom/google/speech/s3/S3$S3Request;)V
    .locals 8
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .param p3    # [Lcom/google/speech/s3/S3$S3Request;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p3

    :try_start_0
    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    if-nez v4, :cond_0

    new-instance v6, Ljava/io/IOException;

    const-string v7, "Found null S3 request"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v6

    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v6

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->createS3RequestStream(Ljava/net/HttpURLConnection;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Lcom/google/android/speech/message/S3RequestStream;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v6, p3, v6

    invoke-virtual {v5, v6}, Lcom/google/android/speech/message/S3RequestStream;->writeHeader(Lcom/google/speech/s3/S3$S3Request;)V

    const/4 v1, 0x1

    :goto_1
    array-length v6, p3

    if-ge v1, v6, :cond_2

    aget-object v6, p3, v1

    invoke-virtual {v5, v6}, Lcom/google/android/speech/message/S3RequestStream;->write(Lcom/google/speech/s3/S3$S3Request;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/speech/message/S3RequestStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    return-void
.end method


# virtual methods
.method protected buildRecognizerParams(Landroid/location/Location;)Lcom/google/android/speech/params/RecognizerParams;
    .locals 4
    .param p1    # Landroid/location/Location;

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->newBuilder()Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSpokenBcp47Locale(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v2, v3}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setMode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setLocationOverride(Landroid/location/Location;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v2

    return-object v2
.end method

.method public cancel()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mSameThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mPendingRecognition:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mPendingRecognition:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    iput-object v2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mPendingRecognition:Ljava/util/concurrent/Future;

    iput-object v2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mCurrentCallback:Lcom/google/android/speech/callback/Callback;

    :cond_0
    return-void
.end method

.method protected createS3RequestStream(Ljava/net/HttpURLConnection;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Lcom/google/android/speech/message/S3RequestStream;
    .locals 4
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/message/S3RequestStream;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getHeader()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/speech/message/S3RequestStream;-><init>(Ljava/io/OutputStream;Ljava/lang/String;Z)V

    return-object v0
.end method

.method protected createS3ResponseStream(Ljava/net/HttpURLConnection;)Lcom/google/android/speech/message/S3ResponseStream;
    .locals 2
    .param p1    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/message/S3ResponseStream;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/speech/message/S3ResponseStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public start(Lcom/google/android/velvet/Query;Lcom/google/android/speech/callback/Callback;Ljava/util/concurrent/Executor;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/majel/proto/MajelProtos$MajelResponse;",
            ">;",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mSameThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mSameThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1, p3}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check(Ljava/util/concurrent/Executor;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->cancel()V

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getLocationOverride()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->buildRecognizerParams(Landroid/location/Location;)Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParams;->getRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->logTextSearchStart(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mCurrentCallback:Lcom/google/android/speech/callback/Callback;

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mRecognitionExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;

    invoke-direct {v2, p0, p1, v0, p3}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;-><init>(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;Lcom/google/android/velvet/Query;Lcom/google/android/speech/params/RecognizerParams;Ljava/util/concurrent/Executor;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mPendingRecognition:Ljava/util/concurrent/Future;

    return-void
.end method
