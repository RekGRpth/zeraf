.class Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;
.super Ljava/lang/Object;
.source "TextRecognizer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->getResponseAndDispatchCallback(Lcom/google/android/velvet/Query;Lcom/google/android/speech/params/RecognizerParams;Ljava/util/concurrent/Executor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

.field final synthetic val$query:Lcom/google/android/velvet/Query;

.field final synthetic val$response:Lcom/google/majel/proto/MajelProtos$MajelResponse;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;Lcom/google/majel/proto/MajelProtos$MajelResponse;Lcom/google/android/velvet/Query;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->this$0:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->val$response:Lcom/google/majel/proto/MajelProtos$MajelResponse;

    iput-object p3, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->val$query:Lcom/google/android/velvet/Query;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->this$0:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    # getter for: Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mCurrentCallback:Lcom/google/android/speech/callback/Callback;
    invoke-static {v0}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->access$100(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;)Lcom/google/android/speech/callback/Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->val$response:Lcom/google/majel/proto/MajelProtos$MajelResponse;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->this$0:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    # getter for: Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mCurrentCallback:Lcom/google/android/speech/callback/Callback;
    invoke-static {v0}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->access$100(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;)Lcom/google/android/speech/callback/Callback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->val$query:Lcom/google/android/velvet/Query;

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->val$response:Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$2;->this$0:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    # getter for: Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->mCurrentCallback:Lcom/google/android/speech/callback/Callback;
    invoke-static {v0}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->access$100(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;)Lcom/google/android/speech/callback/Callback;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/speech/callback/Callback;->onError(Ljava/lang/Object;)V

    goto :goto_0
.end method
