.class public Lcom/google/android/voicesearch/speechservice/BaseballResultData;
.super Ljava/lang/Object;
.source "BaseballResultData.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/voicesearch/speechservice/BaseballResultData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mFirstTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

.field private mFirstTeamScores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSecondTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

.field private mSecondTeamScores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/speechservice/BaseballResultData$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1    # Landroid/os/Parcel;

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;-><init>(III)V

    iput-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    new-instance v1, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;-><init>(III)V

    iput-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeamScores:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    invoke-virtual {p1, v1, v5}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeamScores:Ljava/util/List;

    invoke-virtual {p1, v1, v5}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/voicesearch/speechservice/BaseballResultData$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/voicesearch/speechservice/BaseballResultData$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;I)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;
    .param p2    # Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeamScores:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeamScores:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getFirstTeam()Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    return-object v0
.end method

.method public getFirstTeamScores()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    return-object v0
.end method

.method public getInningsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSecondTeam()Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    return-object v0
.end method

.method public getSecondTeamScores()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeamScores:Ljava/util/List;

    return-object v0
.end method

.method public setFirstTeamScore(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkPositionIndex(II)I

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setSecondTeamScore(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeamScores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkPositionIndex(II)I

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeamScores:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getHits()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getRuns()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getErrors()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getHits()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getRuns()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getErrors()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mFirstTeamScores:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->mSecondTeamScores:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    return-void
.end method
