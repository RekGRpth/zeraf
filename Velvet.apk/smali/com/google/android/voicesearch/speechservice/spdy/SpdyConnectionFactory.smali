.class public Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;
.super Ljava/lang/Object;
.source "SpdyConnectionFactory.java"

# interfaces
.implements Lcom/google/android/speech/network/ConnectionFactory;


# static fields
.field private static final NPN_PROTOCOLS:[[B


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mOkHttpClient:Lcom/android/okhttp/OkHttpClient;

.field private mSpdySocketFactory:Landroid/net/SSLCertificateSocketFactory;

.field private final mUserAgentSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [[B

    const/4 v1, 0x0

    const/4 v2, 0x6

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x8

    new-array v2, v2, [B

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->NPN_PROTOCOLS:[[B

    return-void

    nop

    :array_0
    .array-data 1
        0x73t
        0x70t
        0x64t
        0x79t
        0x2ft
        0x33t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x68t
        0x74t
        0x74t
        0x70t
        0x2ft
        0x31t
        0x2et
        0x31t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mUserAgentSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method private declared-synchronized maybeInitializeSocketFactory(I)V
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mSpdySocketFactory:Landroid/net/SSLCertificateSocketFactory;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-static {p1, v1}, Landroid/net/SSLCertificateSocketFactory;->getDefault(ILandroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    check-cast v1, Landroid/net/SSLCertificateSocketFactory;

    iput-object v1, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mSpdySocketFactory:Landroid/net/SSLCertificateSocketFactory;

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mSpdySocketFactory:Landroid/net/SSLCertificateSocketFactory;

    sget-object v2, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->NPN_PROTOCOLS:[[B

    invoke-virtual {v1, v2}, Landroid/net/SSLCertificateSocketFactory;->setNpnProtocols([[B)V

    new-instance v0, Lcom/android/okhttp/OkHttpClient;

    invoke-direct {v0}, Lcom/android/okhttp/OkHttpClient;-><init>()V

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mSpdySocketFactory:Landroid/net/SSLCertificateSocketFactory;

    invoke-virtual {v0, v1}, Lcom/android/okhttp/OkHttpClient;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)Lcom/android/okhttp/OkHttpClient;

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mOkHttpClient:Lcom/android/okhttp/OkHttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v1, Landroid/net/SSLSessionCache;

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/net/SSLSessionCache;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Ljava/net/HttpURLConnection;
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/net/URL;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method public openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 4
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .param p2    # Ljava/net/URL;

    const-string v2, "https"

    invoke-virtual {p2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "SpdyConnectionFactory only supports HTTPS connections"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getConnectionTimeoutMsec()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->maybeInitializeSocketFactory(I)V

    new-instance v0, Lcom/android/okhttp/OkHttpClient;

    invoke-direct {v0}, Lcom/android/okhttp/OkHttpClient;-><init>()V

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mSpdySocketFactory:Landroid/net/SSLCertificateSocketFactory;

    invoke-virtual {v0, v2}, Lcom/android/okhttp/OkHttpClient;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)Lcom/android/okhttp/OkHttpClient;

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mOkHttpClient:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v2, p2}, Lcom/android/okhttp/OkHttpClient;->open(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getReadTimeoutMsec()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getConnectionTimeoutMsec()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const-string v3, "User-Agent"

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;->mUserAgentSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->hasChunkSize()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getChunkSize()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    :goto_0
    return-object v1

    :cond_1
    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    goto :goto_0
.end method

.method public openSocket(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;)Ljava/net/Socket;
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SpdyConnectionFactory can only open HTTP connections."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
