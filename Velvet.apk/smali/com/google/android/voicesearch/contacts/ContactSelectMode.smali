.class public final enum Lcom/google/android/voicesearch/contacts/ContactSelectMode;
.super Ljava/lang/Enum;
.source "ContactSelectMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/voicesearch/contacts/ContactSelectMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/voicesearch/contacts/ContactSelectMode;

.field public static final enum CALENDAR:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

.field public static final enum CALL_CONTACT:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

.field public static final enum CALL_NUMBER:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

.field public static final enum EMAIL:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

.field public static final enum SHOW_CONTACT_INFO:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

.field public static final enum SMS:Lcom/google/android/voicesearch/contacts/ContactSelectMode;


# instance fields
.field private final mActionIconResourceId:I

.field private final mBailOutIconResourceId:I

.field private final mBailOutStringResourceId:I

.field private final mContactLookupMode:Lcom/google/android/speech/contacts/ContactLookup$Mode;

.field private final mLayoutResourceId:I

.field private final mParentActionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const-string v1, "EMAIL"

    const/4 v2, 0x0

    const v3, 0x7f04001f

    const v4, 0x7f020054

    const v5, 0x7f020051

    const v6, 0x7f0d044a

    const/4 v7, 0x2

    sget-object v8, Lcom/google/android/speech/contacts/ContactLookup$Mode;->EMAIL:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;-><init>(Ljava/lang/String;IIIIIILcom/google/android/speech/contacts/ContactLookup$Mode;)V

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->EMAIL:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const-string v1, "CALL_CONTACT"

    const/4 v2, 0x1

    const v3, 0x7f04001e

    const v4, 0x7f02004b

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xa

    sget-object v8, Lcom/google/android/speech/contacts/ContactLookup$Mode;->PHONE_NUMBER:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;-><init>(Ljava/lang/String;IIIIIILcom/google/android/speech/contacts/ContactLookup$Mode;)V

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->CALL_CONTACT:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const-string v1, "CALL_NUMBER"

    const/4 v2, 0x2

    const v3, 0x7f04001e

    const v4, 0x7f02004b

    const v5, 0x7f02004b

    const v6, 0x7f0d0441

    const/16 v7, 0x1c

    sget-object v8, Lcom/google/android/speech/contacts/ContactLookup$Mode;->PHONE_NUMBER:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;-><init>(Ljava/lang/String;IIIIIILcom/google/android/speech/contacts/ContactLookup$Mode;)V

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->CALL_NUMBER:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const-string v1, "SHOW_CONTACT_INFO"

    const/4 v2, 0x3

    const v3, 0x7f04001e

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x21

    sget-object v8, Lcom/google/android/speech/contacts/ContactLookup$Mode;->CONTACT:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;-><init>(Ljava/lang/String;IIIIIILcom/google/android/speech/contacts/ContactLookup$Mode;)V

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->SHOW_CONTACT_INFO:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const-string v1, "SMS"

    const/4 v2, 0x4

    const v3, 0x7f04001e

    const v4, 0x7f020080

    const v5, 0x7f020051

    const v6, 0x7f0d0448

    const/4 v7, 0x1

    sget-object v8, Lcom/google/android/speech/contacts/ContactLookup$Mode;->PHONE_NUMBER:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;-><init>(Ljava/lang/String;IIIIIILcom/google/android/speech/contacts/ContactLookup$Mode;)V

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->SMS:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const-string v1, "CALENDAR"

    const/4 v2, 0x5

    const v3, 0x7f04001f

    const/4 v4, 0x0

    const v5, 0x7f020057

    const v6, 0x7f0d0410

    const/16 v7, 0xc

    sget-object v8, Lcom/google/android/speech/contacts/ContactLookup$Mode;->EMAIL:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;-><init>(Ljava/lang/String;IIIIIILcom/google/android/speech/contacts/ContactLookup$Mode;)V

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->CALENDAR:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->EMAIL:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->CALL_CONTACT:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->CALL_NUMBER:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->SHOW_CONTACT_INFO:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->SMS:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->CALENDAR:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->$VALUES:[Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIILcom/google/android/speech/contacts/ContactLookup$Mode;)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # Lcom/google/android/speech/contacts/ContactLookup$Mode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Lcom/google/android/speech/contacts/ContactLookup$Mode;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mLayoutResourceId:I

    iput p4, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mActionIconResourceId:I

    iput p5, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mBailOutIconResourceId:I

    iput p6, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mBailOutStringResourceId:I

    iput p7, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mParentActionType:I

    iput-object p8, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mContactLookupMode:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/contacts/ContactSelectMode;
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/voicesearch/contacts/ContactSelectMode;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->$VALUES:[Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    invoke-virtual {v0}, [Lcom/google/android/voicesearch/contacts/ContactSelectMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    return-object v0
.end method


# virtual methods
.method public getActionIconResourceId()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mActionIconResourceId:I

    return v0
.end method

.method public getBailOutIconResourceId()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mBailOutIconResourceId:I

    return v0
.end method

.method public getBailOutStringResourceId()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mBailOutStringResourceId:I

    return v0
.end method

.method public getContactLookupMode()Lcom/google/android/speech/contacts/ContactLookup$Mode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mContactLookupMode:Lcom/google/android/speech/contacts/ContactLookup$Mode;

    return-object v0
.end method

.method public getLayoutResourceId()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mLayoutResourceId:I

    return v0
.end method

.method public getParentActionType()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->mParentActionType:I

    return v0
.end method
