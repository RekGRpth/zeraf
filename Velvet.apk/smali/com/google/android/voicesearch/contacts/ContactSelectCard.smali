.class public Lcom/google/android/voicesearch/contacts/ContactSelectCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "ContactSelectCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/contacts/ContactSelectController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/contacts/ContactSelectController;",
        ">;",
        "Lcom/google/android/voicesearch/contacts/ContactSelectController$Ui;"
    }
.end annotation


# instance fields
.field private mBailOutView:Landroid/widget/TextView;

.field private mContactListView:Lcom/google/android/voicesearch/contacts/ContactListView;

.field private mContacts:[Lcom/google/android/speech/contacts/Contact;

.field private mDidYouMeanText:Ljava/lang/String;

.field private mRecognizedView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private setBailOutIcon(I)V
    .locals 3
    .param p1    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/4 v2, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mBailOutView:Landroid/widget/TextView;

    invoke-virtual {v0, p1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mBailOutView:Landroid/widget/TextView;

    invoke-virtual {v0, p1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f04001d

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100064

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mRecognizedView:Landroid/widget/TextView;

    const v1, 0x7f100067

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/contacts/ContactListView;

    iput-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mContactListView:Lcom/google/android/voicesearch/contacts/ContactListView;

    const v1, 0x7f100068

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mBailOutView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mContactListView:Lcom/google/android/voicesearch/contacts/ContactListView;

    new-instance v2, Lcom/google/android/voicesearch/contacts/ContactSelectCard$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/contacts/ContactSelectCard$1;-><init>(Lcom/google/android/voicesearch/contacts/ContactSelectCard;)V

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/contacts/ContactListView;->setContactSelectedListener(Lcom/google/android/voicesearch/contacts/ContactListView$Listener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mBailOutView:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/voicesearch/contacts/ContactSelectCard$2;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/contacts/ContactSelectCard$2;-><init>(Lcom/google/android/voicesearch/contacts/ContactSelectCard;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public setBailOutAction(Lcom/google/android/voicesearch/contacts/ContactSelectMode;)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->getBailOutIconResourceId()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p1}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->getBailOutStringResourceId()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p1}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->getBailOutIconResourceId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->setBailOutIcon(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mBailOutView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->getBailOutStringResourceId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public setContacts(Ljava/lang/String;[Lcom/google/android/speech/contacts/Contact;Lcom/google/android/voicesearch/contacts/ContactSelectMode;I)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/google/android/speech/contacts/Contact;
    .param p3    # Lcom/google/android/voicesearch/contacts/ContactSelectMode;
    .param p4    # I

    const/4 v1, 0x1

    const/4 v5, 0x0

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, p2

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d043d

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v5

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mDidYouMeanText:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mContacts:[Lcom/google/android/speech/contacts/Contact;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mRecognizedView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mRecognizedView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mDidYouMeanText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mContactListView:Lcom/google/android/voicesearch/contacts/ContactListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mContactListView:Lcom/google/android/voicesearch/contacts/ContactListView;

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mContacts:[Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p3}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->getLayoutResourceId()I

    move-result v2

    invoke-virtual {p3}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->getActionIconResourceId()I

    move-result v3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/contacts/ContactListView;->setContacts([Lcom/google/android/speech/contacts/Contact;IIIZ)V

    :cond_1
    return-void

    :cond_2
    move v0, v5

    goto :goto_0
.end method

.method public showFindContactAction()V
    .locals 2

    const v0, 0x7f020068

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->setBailOutIcon(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectCard;->mBailOutView:Landroid/widget/TextView;

    const v1, 0x7f0d0441

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method
