.class public Lcom/google/android/voicesearch/SendGoogleFeedback;
.super Landroid/app/Activity;
.source "SendGoogleFeedback.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Landroid/view/View;

    invoke-static {p0}, Lcom/google/android/voicesearch/SendGoogleFeedback;->getCurrentScreenshot(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static getCurrentScreenshot(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static launchGoogleFeedback(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/View;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.BUG_REPORT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/voicesearch/SendGoogleFeedback$1;

    invoke-direct {v0, p1, p0}, Lcom/google/android/voicesearch/SendGoogleFeedback$1;-><init>(Landroid/view/View;Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method


# virtual methods
.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendGoogleFeedback;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendGoogleFeedback;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/SendGoogleFeedback;->launchGoogleFeedback(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendGoogleFeedback;->finish()V

    return-void
.end method
