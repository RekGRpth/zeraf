.class public Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;
.super Ljava/lang/Object;
.source "PersonalizationDialogHelper.java"

# interfaces
.implements Lcom/google/android/voicesearch/ui/URLObservableSpan$URLSpanListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$Callbacks;,
        Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$DisableButtonListener;,
        Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$EnableButtonListener;
    }
.end annotation


# instance fields
.field private mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

.field private mContext:Landroid/content/Context;

.field private mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

.field private mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/speech/helper/AccountHelper;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/speech/helper/AccountHelper;
    .param p3    # Lcom/google/android/voicesearch/settings/Settings;
    .param p4    # Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iput-object p3, p0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p4, p0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;)Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;

    iget-object v0, p0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;)Lcom/google/android/speech/helper/AccountHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;

    iget-object v0, p0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    return-object v0
.end method


# virtual methods
.method public createDialog(ILcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$Callbacks;)Landroid/app/Dialog;
    .locals 17
    .param p1    # I
    .param p2    # Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$Callbacks;

    new-instance v3, Landroid/view/ContextThemeWrapper;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mContext:Landroid/content/Context;

    const v15, 0x103006f

    invoke-direct {v3, v14, v15}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v14, 0x7f0d03af

    invoke-virtual {v2, v14}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const-string v14, "layout_inflater"

    invoke-virtual {v3, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    const/4 v10, 0x0

    const/4 v11, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v14, "PersonalizationOptInActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "unknown dialog "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :pswitch_0
    const v14, 0x7f0d03b4

    invoke-virtual {v3, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v15}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getHelp()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;->getPrivacyUrl()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/google/android/voicesearch/util/TextUtil;->replaceLink(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const/4 v10, 0x1

    const/4 v11, 0x1

    const v14, 0x7f0d03b7

    new-instance v15, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$EnableButtonListener;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v15, v0, v3, v1}, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$EnableButtonListener;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;Landroid/content/Context;I)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v14, 0x7f0d03b8

    new-instance v15, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$DisableButtonListener;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v15, v0, v1}, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$DisableButtonListener;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;I)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_1
    const v14, 0x7f040088

    const/4 v15, 0x0

    invoke-virtual {v6, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    if-nez v10, :cond_0

    const v14, 0x7f1001ad

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const v14, 0x7f100118

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v14, v0}, Lcom/google/android/voicesearch/ui/URLObservableSpan;->replace(Landroid/text/Spanned;Lcom/google/android/voicesearch/ui/URLObservableSpan$URLSpanListener;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v14

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v14, 0x7f1001ae

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    if-eqz v11, :cond_1

    const v14, 0x7f0d03b5

    invoke-virtual {v3, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v14}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->getMoreInfoUrl()Ljava/lang/String;

    move-result-object v14

    invoke-static {v8, v14}, Lcom/google/android/voicesearch/util/TextUtil;->createLinkTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v14, v0}, Lcom/google/android/voicesearch/ui/URLObservableSpan;->replace(Landroid/text/Spanned;Lcom/google/android/voicesearch/ui/URLObservableSpan$URLSpanListener;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :goto_2
    invoke-virtual {v2, v13}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_0

    :pswitch_1
    const v14, 0x7f0d03b4

    invoke-virtual {v3, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v15}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getHelp()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Help;->getPrivacyUrl()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/google/android/voicesearch/util/TextUtil;->replaceLink(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const/4 v11, 0x1

    const v14, 0x104000a

    new-instance v15, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$EnableButtonListener;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v15, v0, v3, v1}, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$EnableButtonListener;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;Landroid/content/Context;I)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v14, 0x1040000

    new-instance v15, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$DisableButtonListener;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v15, v0, v1}, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$DisableButtonListener;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;I)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    :pswitch_2
    const v14, 0x7f0d03b6

    invoke-virtual {v3, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v15}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->getDashboardUrl()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/google/android/voicesearch/util/TextUtil;->replaceLink(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const v14, 0x104000a

    new-instance v15, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$DisableButtonListener;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v15, v0, v1}, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$DisableButtonListener;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;I)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v14, 0x1040000

    new-instance v15, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$EnableButtonListener;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v15, v0, v3, v1}, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$EnableButtonListener;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;Landroid/content/Context;I)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    :cond_1
    const/16 v14, 0x8

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->getMoreInfoUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x2e

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->getDashboardUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x2f

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    goto :goto_0
.end method
