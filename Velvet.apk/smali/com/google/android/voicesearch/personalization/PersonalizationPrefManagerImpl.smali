.class public Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;
.super Ljava/lang/Object;
.source "PersonalizationPrefManagerImpl.java"

# interfaces
.implements Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;


# instance fields
.field private final mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

.field private final mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private final mPrefs:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/helper/AccountHelper;Lcom/google/android/speech/utils/NetworkInformation;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;
    .param p3    # Lcom/google/android/speech/helper/AccountHelper;
    .param p4    # Lcom/google/android/speech/utils/NetworkInformation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl$1;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;)V

    invoke-static {v0}, Lcom/google/common/base/Suppliers;->memoize(Lcom/google/common/base/Supplier;)Lcom/google/common/base/Supplier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mPrefs:Lcom/google/common/base/Supplier;

    iput-object p1, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object p2, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p3, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iput-object p4, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;)Lcom/google/android/searchcommon/GsaPreferenceController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;

    iget-object v0, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

    return-object v0
.end method

.method private getPersonalizationValue()I
    .locals 4

    iget-object v2, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mPrefs:Lcom/google/common/base/Supplier;

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    const-string v2, "pref-voice-personalization-status"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->isPersonalizationAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x5

    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "pref-voice-personalization-status"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public isEnabled()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->getPersonalizationValue()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPersonalizationAvailable()Z
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    invoke-interface {v3}, Lcom/google/android/speech/helper/AccountHelper;->hasGoogleAccount()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v3}, Lcom/google/android/speech/utils/NetworkInformation;->getSimMcc()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->getMccCountryCodesList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setEnabled(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mPrefs:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    if-eqz p1, :cond_1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "pref-voice-personalization-status"

    const/4 v3, 0x4

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_0
    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    if-eqz p1, :cond_2

    const/16 v1, 0x30

    :goto_1
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "pref-voice-personalization-status"

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_2
    const/16 v1, 0x31

    goto :goto_1
.end method
