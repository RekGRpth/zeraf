.class public Lcom/google/android/voicesearch/intentapi/IntentApiActivity;
.super Landroid/app/Activity;
.source "IntentApiActivity.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;


# instance fields
.field private mController:Lcom/google/android/voicesearch/fragments/IntentApiController;

.field private mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-direct {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setIntentApiViewAndAttachCallbacks()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;)Lcom/google/android/voicesearch/fragments/IntentApiController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lcom/google/android/voicesearch/fragments/IntentApiController;

    return-object v0
.end method

.method private setIntentApiViewAndAttachCallbacks()V
    .locals 2

    const v0, 0x7f040063

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setContentView(I)V

    new-instance v0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    const v1, 0x7f100117

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    new-instance v1, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$4;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$4;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->setCallback(Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/voicesearch/VoiceSearchServices;->getHeadsetAudioRecorderRouter()Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$1;

    invoke-direct {v1, p0, v3}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$1;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;Lcom/google/android/voicesearch/VoiceSearchServices;)V

    new-instance v4, Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAudioStore()Lcom/google/android/speech/audio/AudioStore;

    move-result-object v6

    invoke-static {v3, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->create(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/common/base/Supplier;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v7

    invoke-direct {v4, p0, v5, v6, v7}, Lcom/google/android/voicesearch/fragments/IntentApiController;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/audio/AudioStore;Lcom/google/android/voicesearch/fragments/UberRecognizerController;)V

    iput-object v4, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setIntentApiViewAndAttachCallbacks()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->cancel()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x3e

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->scheduleSendEvents(Landroid/content/Context;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;-><init>(Landroid/content/Intent;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    :cond_0
    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->cancelSendEvents(Landroid/content/Context;)V

    const/16 v1, 0x3d

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->onResume(Lcom/google/android/voicesearch/intentapi/IntentApiParams;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->attachUi(Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->detachUi(Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public setFinalRecognizedText(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->setLanguage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPromptText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    const v1, 0x7f0d03ca

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    return-void
.end method

.method public showError(IIZ)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const v1, 0x7f040064

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setContentView(I)V

    const v1, 0x7f10011b

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    const v1, 0x7f10014b

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$2;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const v1, 0x7f10014c

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$3;

    invoke-direct {v2, p0, p2}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$3;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public showInitializing()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->showInitializing()V

    return-void
.end method

.method public showInitializingMic()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->showInitializing()V

    return-void
.end method

.method public showListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->showListening()V

    return-void
.end method

.method public showNotListening()V
    .locals 0

    return-void
.end method

.method public showReadyForSpeech()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->showListening()V

    return-void
.end method

.method public showRecognizing()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->showRecognizing()V

    return-void
.end method

.method public showRecording()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mViewHelper:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->showRecording()V

    return-void
.end method

.method public showSoundSearchPromotedQuery()V
    .locals 0

    return-void
.end method

.method public showTapToSpeak()V
    .locals 0

    return-void
.end method

.method public updateRecognizedText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method
