.class public Lcom/google/android/voicesearch/intentapi/IntentApiParams;
.super Ljava/lang/Object;
.source "IntentApiParams.java"


# instance fields
.field private final mAutoScript:Z

.field private final mCallingPackage:Ljava/lang/String;

.field private final mMaxResults:I

.field private final mPendingBundleIntent:Landroid/os/Bundle;

.field private final mPendingIntent:Landroid/app/PendingIntent;

.field private final mPrompt:Ljava/lang/String;

.field private final mReturnAudio:Z


# direct methods
.method public constructor <init>(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "EXPERIMENTAL_AUTO_SCRIPT"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mAutoScript:Z

    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mPendingIntent:Landroid/app/PendingIntent;

    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mPendingBundleIntent:Landroid/os/Bundle;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getReturnAudio(Landroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mReturnAudio:Z

    const-string v0, "android.speech.extra.PROMPT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mPrompt:Ljava/lang/String;

    const-string v0, "android.speech.extra.MAX_RESULTS"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mMaxResults:I

    iget-boolean v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mAutoScript:Z

    if-eqz v0, :cond_0

    const-string v0, "auto-script"

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mCallingPackage:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getCallingPackage(Landroid/content/Intent;Landroid/app/PendingIntent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mCallingPackage:Ljava/lang/String;

    goto :goto_0
.end method

.method private getCallingPackage(Landroid/content/Intent;Landroid/app/PendingIntent;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # Ljava/lang/String;

    if-nez p3, :cond_0

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object p3

    :cond_0
    const-string v0, "android"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "calling_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "calling_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    :cond_1
    invoke-static {p3}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const-string v0, "IntentApiParams"

    const-string v1, "ACTION_RECOGNIZE_SPEECH intent called incorrectly. Maybe you called startActivity, but you should have called startActivityForResult (or otherwise included a pending intent)."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ""

    goto :goto_0
.end method

.method private getReturnAudio(Landroid/content/Intent;)Z
    .locals 4
    .param p1    # Landroid/content/Intent;

    const-string v1, "android.speech.extra.GET_AUDIO_FORMAT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/audio/AudioUtils$Encoding;->AMR:Lcom/google/android/speech/audio/AudioUtils$Encoding;

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioUtils$Encoding;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const-string v1, "IntentApiParams"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The audio format is not supported [requested="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " supported="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/speech/audio/AudioUtils$Encoding;->AMR:Lcom/google/android/speech/audio/AudioUtils$Encoding;

    invoke-virtual {v3}, Lcom/google/android/speech/audio/AudioUtils$Encoding;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCallingPackage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mCallingPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxResults()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mMaxResults:I

    return v0
.end method

.method public getPendingBundleIntent()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mPendingBundleIntent:Landroid/os/Bundle;

    return-object v0
.end method

.method public getPendingIntent()Landroid/app/PendingIntent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mPendingIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public getPrompt()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mPrompt:Ljava/lang/String;

    return-object v0
.end method

.method public isAutoScript()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mAutoScript:Z

    return v0
.end method

.method public isReturnAudio()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->mReturnAudio:Z

    return v0
.end method
