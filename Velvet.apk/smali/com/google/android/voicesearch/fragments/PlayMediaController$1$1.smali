.class Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;
.super Ljava/lang/Object;
.source "PlayMediaController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/PlayMediaController$1;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->clearCard()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-boolean v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->val$isRestore:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    # getter for: Lcom/google/android/voicesearch/fragments/PlayMediaController;->mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$400(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->isPlayStoreLink(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    # getter for: Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$300(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getIsFromSoundSearch()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->disableCountDown()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->showCard()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;->this$1:Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->showCardAndPlayTts()V

    goto :goto_0
.end method
