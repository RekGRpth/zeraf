.class public Lcom/google/android/voicesearch/fragments/OpenUrlController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "OpenUrlController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;,
        Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAction:Lcom/google/majel/proto/PeanutProtos$Url;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    return-void
.end method

.method public static createData(Lcom/google/majel/proto/PeanutProtos$Url;)Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;
    .locals 1
    .param p0    # Lcom/google/majel/proto/PeanutProtos$Url;

    new-instance v0, Lcom/google/android/voicesearch/fragments/OpenUrlController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/OpenUrlController$1;-><init>(Lcom/google/majel/proto/PeanutProtos$Url;)V

    return-object v0
.end method


# virtual methods
.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenUrlController;->mAction:Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {v0}, Lcom/google/majel/proto/PeanutProtos$Url;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0xd

    return v0
.end method

.method public initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlController;->mAction:Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Url;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;->setName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlController;->mAction:Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Url;->getDisplayLink()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;->setDisplayUrl(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlController;->mAction:Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Url;->getRenderedLink()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;->setPreviewUrl(Ljava/lang/String;)V

    return-void
.end method

.method protected internalBailOut()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->internalExecuteAction()V

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlController;->mAction:Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Url;->getLink()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-direct {v0}, Lcom/google/majel/proto/PeanutProtos$Url;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenUrlController;->mAction:Lcom/google/majel/proto/PeanutProtos$Url;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenUrlController;->mAction:Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/PeanutProtos$Url;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    return-void
.end method

.method public start(Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;->getUrl()Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Url;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenUrlController;->mAction:Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->showCardAndPlayTts()V

    return-void
.end method
