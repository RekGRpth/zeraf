.class public Lcom/google/android/voicesearch/fragments/RecognizedContactController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "RecognizedContactController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;,
        Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;",
        ">;"
    }
.end annotation


# static fields
.field private static final EMAIL_CONSTANTS:[Ljava/lang/String;

.field private static final PHONE_CONSTANTS:[Ljava/lang/String;


# instance fields
.field private mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "email"

    aput-object v1, v0, v2

    const-string v1, "secondary_email"

    aput-object v1, v0, v3

    const-string v1, "tertiary_email"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->EMAIL_CONSTANTS:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "phone"

    aput-object v1, v0, v2

    const-string v1, "secondary_phone"

    aput-object v1, v0, v3

    const-string v1, "tertiary_phone"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->PHONE_CONSTANTS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method

.method public static createData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;",
            ">;)",
            "Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAddToContactsIntent()Landroid/content/Intent;
    .locals 11

    new-instance v0, Landroid/content/Intent;

    const-string v9, "android.intent.action.INSERT"

    sget-object v10, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v9, "name"

    iget-object v10, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v10}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v9, "company"

    iget-object v10, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v10}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getCompany()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v9, "job_title"

    iget-object v10, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v10}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v9, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v9}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getEmails()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    :goto_0
    sget-object v9, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->EMAIL_CONSTANTS:[Ljava/lang/String;

    array-length v9, v9

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    if-ge v3, v9, :cond_0

    sget-object v9, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->EMAIL_CONSTANTS:[Ljava/lang/String;

    aget-object v10, v9, v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;

    invoke-virtual {v9}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget-object v9, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v9}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getPhoneNumbers()Ljava/util/List;

    move-result-object v5

    const/4 v3, 0x0

    :goto_1
    sget-object v9, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->PHONE_CONSTANTS:[Ljava/lang/String;

    array-length v9, v9

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    if-ge v3, v9, :cond_1

    sget-object v9, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->PHONE_CONSTANTS:[Ljava/lang/String;

    aget-object v10, v9, v3

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;

    invoke-virtual {v9}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;->getFull()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    iget-object v9, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v9}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getPostalAddresses()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_2

    const-string v10, "postal"

    const/4 v9, 0x0

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;

    invoke-virtual {v9}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;->getFull()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v9, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v9}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getUrls()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "mimetype"

    const-string v10, "vnd.android.cursor.item/website"

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "data1"

    invoke-virtual {v7, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    const-string v9, "data"

    invoke-virtual {v0, v9, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object v0
.end method

.method public getDataString()Ljava/lang/String;
    .locals 7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getCompany()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getCompany()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getEmails()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getPhoneNumbers()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;->getFull()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getPostalAddresses()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;->getFull()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getUrls()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public initUi()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;->setName(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;->setTitle(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getCompany()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;->setCompany(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getUrls()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getEmails()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v3}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getPhoneNumbers()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-interface {v4}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;->getPostalAddresses()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;->setListData(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public start(Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->mData:Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->showCard()V

    return-void
.end method
