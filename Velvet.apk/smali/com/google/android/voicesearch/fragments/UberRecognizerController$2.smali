.class final Lcom/google/android/voicesearch/fragments/UberRecognizerController$2;
.super Ljava/lang/Object;
.source "UberRecognizerController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/UberRecognizerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone()V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/String;Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/exception/RecognizeException;

    return-void
.end method

.method public onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    return-void
.end method

.method public onNoMajelResult()V
    .locals 0

    return-void
.end method

.method public onNoMatch(Lcom/google/android/speech/exception/NoMatchRecognizeException;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/exception/NoMatchRecognizeException;

    return-void
.end method

.method public onNoSoundSearchMatch(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/exception/SoundSearchRecognizeException;

    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 0

    return-void
.end method

.method public onRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/velvet/prefetch/SearchResultPage;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/velvet/prefetch/SearchResultPage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/prefetch/SearchResultPage;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public onSoundSearchError(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/exception/SoundSearchRecognizeException;

    return-void
.end method

.method public onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V
    .locals 0
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    return-void
.end method

.method public onTtsAvailable()V
    .locals 0

    return-void
.end method
