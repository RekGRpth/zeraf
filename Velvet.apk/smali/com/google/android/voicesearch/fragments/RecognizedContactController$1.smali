.class final Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;
.super Ljava/lang/Object;
.source "RecognizedContactController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/RecognizedContactController;->createData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$company:Ljava/lang/String;

.field final synthetic val$emails:Ljava/util/List;

.field final synthetic val$name:Ljava/lang/String;

.field final synthetic val$phoneNumbers:Ljava/util/List;

.field final synthetic val$postalAddresses:Ljava/util/List;

.field final synthetic val$title:Ljava/lang/String;

.field final synthetic val$urls:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$name:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$title:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$company:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$urls:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$emails:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$phoneNumbers:Ljava/util/List;

    iput-object p7, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$postalAddresses:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCompany()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$company:Ljava/lang/String;

    return-object v0
.end method

.method public getEmails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$emails:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$name:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumbers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$phoneNumbers:Ljava/util/List;

    return-object v0
.end method

.method public getPostalAddresses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$postalAddresses:Ljava/util/List;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$title:Ljava/lang/String;

    return-object v0
.end method

.method public getUrls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/RecognizedContactController$1;->val$urls:Ljava/util/List;

    return-object v0
.end method
