.class Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;
.super Landroid/os/AsyncTask;
.source "ReminderSaver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->saveReminder(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;Lcom/google/android/speech/callback/SimpleCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

.field final synthetic val$action:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

.field final synthetic val$callback:Lcom/google/android/speech/callback/SimpleCallback;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->this$0:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->val$action:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 14
    .param p1    # [Ljava/lang/Void;

    const/16 v12, 0xb

    const/4 v13, 0x0

    new-instance v10, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v10}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    iget-object v11, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->val$action:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v10, v11}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setAddReminderActionExtension(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    iget-object v10, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->val$action:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v10}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getConfirmationUrlPath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    new-instance v10, Landroid/net/Uri$Builder;

    invoke-direct {v10}, Landroid/net/Uri$Builder;-><init>()V

    iget-object v11, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->this$0:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;
    invoke-static {v11}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->access$000(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->this$0:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;
    invoke-static {v11}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->access$000(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v11

    invoke-virtual {v11, v13}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v10, "pinfo"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v7, v6}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v1, v6, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_1
    const-string v10, "pinfo"

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->toByteArray()[B

    move-result-object v11

    invoke-static {v11, v12}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->this$0:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    # invokes: Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->getCookieHeader(Ljava/lang/String;)Ljava/util/Map;
    invoke-static {v10, v8}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->access$100(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    :try_start_0
    iget-object v10, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->this$0:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;
    invoke-static {v10}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->access$200(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v10

    new-instance v11, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-direct {v11, v8, v3}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    const/16 v12, 0xb

    invoke-interface {v10, v11, v12}, Lcom/google/android/searchcommon/util/HttpHelper;->get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    :goto_1
    return-object v10

    :catch_0
    move-exception v2

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->this$0:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->access$300(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->this$0:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->access$300(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
