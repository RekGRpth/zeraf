.class public Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "HighConfidenceAnswerCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;"
    }
.end annotation


# instance fields
.field private mAnswerDescriptionView:Landroid/widget/TextView;

.field private mAnswerView:Landroid/widget/TextView;

.field private mAttributionTitleView:Landroid/widget/TextView;

.field private mAttributionUrlView:Landroid/widget/TextView;

.field private mImageAttributionView:Landroid/widget/TextView;

.field private mImageFrame:Landroid/view/View;

.field private mImageView:Lcom/google/android/velvet/ui/WebImageView;

.field private mTextLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private attributionIsFromGoogle(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "google.com"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private getDisplayString(Lcom/google/majel/proto/AttributionProtos$Attribution;I)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/majel/proto/AttributionProtos$Attribution;
    .param p2    # I

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getDisplayText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageDomain()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private removeHtmlTags(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f040057

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAnswerView:Landroid/widget/TextView;

    const v1, 0x7f10012d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAnswerDescriptionView:Landroid/widget/TextView;

    const v1, 0x7f10012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAttributionTitleView:Landroid/widget/TextView;

    const v1, 0x7f10012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAttributionUrlView:Landroid/widget/TextView;

    const v1, 0x7f100128

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageFrame:Landroid/view/View;

    const v1, 0x7f100129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    const v1, 0x7f10012a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageAttributionView:Landroid/widget/TextView;

    const v1, 0x7f10012b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mTextLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAttributionUrlView:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard$1;-><init>(Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageFrame:Landroid/view/View;

    new-instance v2, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard$2;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard$2;-><init>(Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public setAnswer(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAnswerView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setAnswerDescription(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAnswerDescriptionView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAnswerDescriptionView:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->removeHtmlTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setImageData(Lcom/google/majel/proto/PeanutProtos$Image;Lcom/google/majel/proto/AttributionProtos$Attribution;)V
    .locals 8
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Image;
    .param p2    # Lcom/google/majel/proto/AttributionProtos$Attribution;

    const/16 v5, 0x8

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageFrame:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mTextLayout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c003d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mTextLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Image;->hasData()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Image;->getData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v2

    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v2, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_2
    if-eqz v1, :cond_4

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v3, v1}, Lcom/google/android/velvet/ui/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    const v3, 0x7f0d0388

    invoke-direct {p0, p2, v3}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->getDisplayString(Lcom/google/majel/proto/AttributionProtos$Attribution;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->attributionIsFromGoogle(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_3
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageAttributionView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mImageAttributionView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setSource(Lcom/google/majel/proto/AttributionProtos$Attribution;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/AttributionProtos$Attribution;

    const/16 v2, 0x8

    const v1, 0x7f0d0387

    invoke-direct {p0, p1, v1}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->getDisplayString(Lcom/google/majel/proto/AttributionProtos$Attribution;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAttributionTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAttributionUrlView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->mAttributionTitleView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;->removeHtmlTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
