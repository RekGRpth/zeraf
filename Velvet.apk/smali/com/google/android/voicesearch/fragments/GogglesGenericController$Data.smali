.class public interface abstract Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;
.super Ljava/lang/Object;
.source "GogglesGenericController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/GogglesGenericController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Data"
.end annotation


# virtual methods
.method public abstract getActions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/goggles/GogglesGenericAction;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFifeImageUrl()Ljava/lang/String;
.end method

.method public abstract getSessionid()Ljava/lang/String;
.end method

.method public abstract getSubtitle()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method
