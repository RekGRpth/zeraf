.class public Lcom/google/android/voicesearch/fragments/HelpController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "HelpController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;,
        Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;,
        Lcom/google/android/voicesearch/fragments/HelpController$Contact;,
        Lcom/google/android/voicesearch/fragments/HelpController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/HelpController$Ui;",
        ">;"
    }
.end annotation


# static fields
.field private static final COLUMNS:[Ljava/lang/String;

.field private static final DAY_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final DAY_OF_WEEK_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final GET_FIRST_NAME_COLUMNS:[Ljava/lang/String;

.field private static final HOUR_12_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final HOUR_24_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final MINUTE_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final MONTH_FORMAT:Ljava/text/SimpleDateFormat;


# instance fields
.field private exampleIndex:I

.field private final mAppVersion:I

.field private final mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;

.field private final mContactsForExamples:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/voicesearch/fragments/HelpController$Contact;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mDeviceCapabilities:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private mExamples:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;",
            ">;"
        }
    .end annotation
.end field

.field private final mExamplesWithContact:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFeature:Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

.field private final mHourFormat:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEEE"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->DAY_OF_WEEK_FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->MONTH_FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->DAY_FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->HOUR_24_FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "KK"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->HOUR_12_FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->MINUTE_FORMAT:Ljava/text/SimpleDateFormat;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->COLUMNS:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "data2"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->GET_FIRST_NAME_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Landroid/content/ContentResolver;Lcom/google/android/searchcommon/DeviceCapabilityManager;IZ)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Landroid/content/ContentResolver;
    .param p6    # Lcom/google/android/searchcommon/DeviceCapabilityManager;
    .param p7    # I
    .param p8    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mContactsForExamples:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->exampleIndex:I

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/speech/contacts/ContactRetriever;

    invoke-direct {v0, p5}, Lcom/google/android/speech/contacts/ContactRetriever;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mDeviceCapabilities:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    iput p7, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mAppVersion:I

    if-eqz p8, :cond_0

    sget-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->HOUR_24_FORMAT:Ljava/text/SimpleDateFormat;

    :goto_0
    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mHourFormat:Ljava/text/SimpleDateFormat;

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->HOUR_12_FORMAT:Ljava/text/SimpleDateFormat;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/HelpController;J)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpController;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/HelpController;->getFirstName(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/fragments/HelpController;->COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/fragments/HelpController;)Lcom/google/android/speech/contacts/ContactRetriever;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mContactsForExamples:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;

    return-object v0
.end method

.method private formatQuery(Ljava/lang/String;Ljava/util/List;Ljava/util/Calendar;Lcom/google/android/voicesearch/fragments/HelpController$Contact;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/util/Calendar;
    .param p4    # Lcom/google/android/voicesearch/fragments/HelpController$Contact;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Calendar;",
            "Lcom/google/android/voicesearch/fragments/HelpController$Contact;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    move-object v2, p1

    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    if-eqz p3, :cond_0

    sget-object v4, Lcom/google/android/voicesearch/fragments/HelpController;->DAY_OF_WEEK_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v4}, Lcom/google/android/voicesearch/fragments/HelpController;->substitutePlaceholder(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_1
    if-eqz p3, :cond_0

    sget-object v4, Lcom/google/android/voicesearch/fragments/HelpController;->DAY_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v4}, Lcom/google/android/voicesearch/fragments/HelpController;->substitutePlaceholder(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_2
    if-eqz p3, :cond_0

    sget-object v4, Lcom/google/android/voicesearch/fragments/HelpController;->MONTH_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v4}, Lcom/google/android/voicesearch/fragments/HelpController;->substitutePlaceholder(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_3
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/google/android/voicesearch/fragments/HelpController$Contact;->getFirstName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v4}, Lcom/google/android/voicesearch/fragments/HelpController;->substitutePlaceholder(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v4, "%%"

    const-string v5, "%"

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private getFirstName(J)Ljava/lang/String;
    .locals 10
    .param p1    # J

    const/4 v6, 0x0

    const/4 v2, 0x1

    new-instance v7, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;

    invoke-direct {v7, p0, v6}, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;-><init>(Lcom/google/android/voicesearch/fragments/HelpController;Lcom/google/android/voicesearch/fragments/HelpController$1;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/voicesearch/fragments/HelpController;->GET_FIRST_NAME_COLUMNS:[Ljava/lang/String;

    const-string v4, "contact_id = ? AND mimetype = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    const-string v8, "vnd.android.cursor.item/name"

    aput-object v8, v5, v2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/speech/contacts/ContactRetriever;->getContacts(Landroid/net/Uri;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;)V

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;->firstName:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;->access$200(Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRequiredContactContentUri(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Landroid/net/Uri;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getSubstitutionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private internalStart()V
    .locals 5

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mFeature:Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->getExampleCount()I

    move-result v4

    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mFeature:Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->getExampleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/HelpController;->isAvailable(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/HelpController;->getRequiredContactContentUri(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_5

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    if-lez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/voicesearch/fragments/HelpController;->lookupContactsForExamples(Ljava/util/Set;)V

    :goto_1
    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HelpController;->showCard()V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HelpController;->showNoCard()V

    goto :goto_1
.end method

.method private isAvailable(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Z
    .locals 6
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasQuery()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasMinVersion()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mAppVersion:I

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getMinVersion()I

    move-result v5

    if-lt v2, v5, :cond_2

    :cond_1
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRetireVersion()Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mAppVersion:I

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRetireVersion()I

    move-result v5

    if-lt v2, v5, :cond_3

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRequiredCapabilityList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mDeviceCapabilities:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-interface {v2}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->isTelephoneCapable()Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    goto :goto_0

    :cond_5
    if-ne v0, v4, :cond_6

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mDeviceCapabilities:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-interface {v2}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->hasRearFacingCamera()Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    goto :goto_0

    :cond_6
    move v2, v3

    goto :goto_0

    :cond_7
    move v2, v4

    goto :goto_0
.end method

.method private lookupContactsForExamples(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/fragments/HelpController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/HelpController$1;-><init>(Lcom/google/android/voicesearch/fragments/HelpController;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    new-array v2, v2, [Landroid/net/Uri;

    invoke-interface {p1, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/HelpController$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private selectContactFor(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/android/voicesearch/fragments/HelpController$Contact;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mContactsForExamples:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/HelpController$Contact;

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private showExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)V
    .locals 11
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    const/4 v10, 0x5

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HelpController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v7

    check-cast v7, Lcom/google/android/voicesearch/fragments/HelpController$Ui;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getQuery()Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x0

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    const/4 v1, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/HelpController;->selectContactFor(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/android/voicesearch/fragments/HelpController$Contact;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasImageUrl()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getImageUrl()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/google/android/voicesearch/fragments/HelpController$Ui;->setPreviewUrl(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getSubstitutionCount()I

    move-result v8

    if-lez v8, :cond_8

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getSubstitutionList()Ljava/util/List;

    move-result-object v8

    invoke-direct {p0, v4, v8, v0, v1}, Lcom/google/android/voicesearch/fragments/HelpController;->formatQuery(Ljava/lang/String;Ljava/util/List;Ljava/util/Calendar;Lcom/google/android/voicesearch/fragments/HelpController$Contact;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/google/android/voicesearch/fragments/HelpController$Ui;->setExampleQuery(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v7, v1}, Lcom/google/android/voicesearch/fragments/HelpController$Ui;->setPreviewContact(Lcom/google/android/voicesearch/fragments/HelpController$Contact;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeDays()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeSeconds()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasTime()Z

    move-result v8

    if-eqz v8, :cond_0

    :cond_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeSeconds()Z

    move-result v8

    if-eqz v8, :cond_6

    const/16 v8, 0xd

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRelativeSeconds()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->add(II)V

    :cond_4
    :goto_2
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasTime()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v6

    const/16 v8, 0xb

    invoke-virtual {v6}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getHour()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->set(II)V

    const/16 v8, 0xc

    invoke-virtual {v6}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getMinute()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeDays()Z

    move-result v8

    if-nez v8, :cond_5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, 0x1

    invoke-virtual {v0, v10, v8}, Ljava/util/Calendar;->add(II)V

    :cond_5
    if-eqz v5, :cond_7

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    sget-object v8, Lcom/google/android/voicesearch/fragments/HelpController;->DAY_OF_WEEK_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/google/android/voicesearch/fragments/HelpController;->DAY_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {v9, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/google/android/voicesearch/fragments/HelpController;->MONTH_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {v10, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v8, v9, v10}, Lcom/google/android/voicesearch/fragments/HelpController$Ui;->setPreviewDate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->hasRelativeDays()Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;->getRelativeDays()I

    move-result v8

    invoke-virtual {v0, v10, v8}, Ljava/util/Calendar;->add(II)V

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mHourFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/google/android/voicesearch/fragments/HelpController;->MINUTE_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {v9, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/google/android/voicesearch/fragments/HelpController$Ui;->setPreviewTime(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-interface {v7, v4}, Lcom/google/android/voicesearch/fragments/HelpController$Ui;->setExampleQuery(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method static substitutePlaceholder(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(?<!%)%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(?![0-9])"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mFeature:Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x25

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x2a

    return v0
.end method

.method public initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HelpController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/HelpController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mFeature:Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->getHeadline()Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/HelpController$Ui;->setHeadline(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->exampleIndex:I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;

    iget v1, p0, Lcom/google/android/voicesearch/fragments/HelpController;->exampleIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/HelpController;->showExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)V

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->parseFrom([B)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mFeature:Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    return-void
.end method

.method public refreshExample()V
    .locals 2

    iget v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->exampleIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->exampleIndex:I

    iget v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->exampleIndex:I

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->exampleIndex:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;

    iget v1, p0, Lcom/google/android/voicesearch/fragments/HelpController;->exampleIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/HelpController;->showExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)V

    return-void
.end method

.method public restoreStart()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/HelpController;->internalStart()V

    return-void
.end method

.method public start(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HelpController;->mFeature:Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/HelpController;->internalStart()V

    return-void
.end method
