.class Lcom/google/android/voicesearch/fragments/PlayMediaController$1;
.super Ljava/lang/Object;
.source "PlayMediaController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/PlayMediaController;->setAppSelector(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

.field final synthetic val$isRestore:Z


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/PlayMediaController;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iput-boolean p2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->val$isRestore:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    # invokes: Lcom/google/android/voicesearch/fragments/PlayMediaController;->getMediaPreviewIntent()Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$000(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    # getter for: Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$200(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/android/voicesearch/util/AppSelectionHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->isSupported(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    # setter for: Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPreviewEnabled:Z
    invoke-static {v2, v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$102(Lcom/google/android/voicesearch/fragments/PlayMediaController;Z)Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    # getter for: Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$300(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getPlayStoreLinks(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    # getter for: Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$300(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getLocalApps(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    # invokes: Lcom/google/android/voicesearch/fragments/PlayMediaController;->getDefaultApp()Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$500(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    move-result-object v2

    # setter for: Lcom/google/android/voicesearch/fragments/PlayMediaController;->mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    invoke-static {v1, v2}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->access$402(Lcom/google/android/voicesearch/fragments/PlayMediaController;Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;->this$0:Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getMainThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v1

    new-instance v2, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController$1$1;-><init>(Lcom/google/android/voicesearch/fragments/PlayMediaController$1;)V

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
