.class final enum Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;
.super Ljava/lang/Enum;
.source "SearchPlateFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/SearchPlateFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "InputMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

.field public static final enum GOGGLES_CAPTURE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

.field public static final enum GOGGLES_RESPONSE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

.field public static final enum SOUND_SEARCH:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

.field public static final enum TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

.field public static final enum TEXT_FROM_VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

.field public static final enum VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    new-instance v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    const-string v1, "TEXT_FROM_VOICE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT_FROM_VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    new-instance v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    const-string v1, "VOICE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    new-instance v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    const-string v1, "GOGGLES_CAPTURE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_CAPTURE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    new-instance v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    const-string v1, "GOGGLES_RESPONSE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_RESPONSE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    new-instance v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    const-string v1, "SOUND_SEARCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->SOUND_SEARCH:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT_FROM_VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_CAPTURE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_RESPONSE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->SOUND_SEARCH:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->$VALUES:[Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->$VALUES:[Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-virtual {v0}, [Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    return-object v0
.end method
