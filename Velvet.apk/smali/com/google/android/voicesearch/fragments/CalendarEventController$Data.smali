.class public interface abstract Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;
.super Ljava/lang/Object;
.source "CalendarEventController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/CalendarEventController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Data"
.end annotation


# virtual methods
.method public abstract getAttendees()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEndTimeMs()J
.end method

.method public abstract getLocation()Ljava/lang/String;
.end method

.method public abstract getReminders()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getStartTimeMs()J
.end method

.method public abstract getSummary()Ljava/lang/String;
.end method
