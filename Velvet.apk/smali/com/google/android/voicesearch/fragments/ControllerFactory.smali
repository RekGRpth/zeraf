.class public Lcom/google/android/voicesearch/fragments/ControllerFactory;
.super Ljava/lang/Object;
.source "ControllerFactory.java"


# instance fields
.field private final mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

.field private mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

.field private mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

.field private final mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

.field private final mContext:Landroid/content/Context;

.field private final mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field private final mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mPresenter:Lcom/google/android/voicesearch/CardController;

.field private mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

.field private final mResources:Landroid/content/res/Resources;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private final mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

.field private final mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/speech/contacts/ContactLookup;Landroid/content/Context;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/DeviceCapabilityManager;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/helper/AccountHelper;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/VelvetFactory;
    .param p2    # Lcom/google/android/voicesearch/CardController;
    .param p3    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p4    # Lcom/google/android/speech/contacts/ContactLookup;
    .param p5    # Landroid/content/Context;
    .param p6    # Lcom/google/android/voicesearch/util/LocalTtsManager;
    .param p7    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p8    # Ljava/util/concurrent/ExecutorService;
    .param p9    # Lcom/google/android/searchcommon/DeviceCapabilityManager;
    .param p10    # Lcom/google/android/voicesearch/settings/Settings;
    .param p11    # Lcom/google/android/speech/helper/AccountHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object p7, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p8, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    iput-object p9, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    iput-object p10, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p11, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/ControllerFactory;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ControllerFactory;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private createAccountNameSupplier()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/fragments/ControllerFactory$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory$1;-><init>(Lcom/google/android/voicesearch/fragments/ControllerFactory;)V

    return-object v0
.end method

.method private createControllerFor(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;"
        }
    .end annotation

    const-string v0, "AtHomeController"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/fragments/AtHomeController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/AtHomeController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "CalendarEventController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/voicesearch/fragments/CalendarEventController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getCalendarHelper()Lcom/google/android/voicesearch/util/CalendarHelper;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getCalendarTextHelper()Lcom/google/android/voicesearch/util/CalendarTextHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/CalendarEventController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/voicesearch/util/CalendarTextHelper;Landroid/content/res/Resources;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    goto :goto_0

    :cond_1
    const-string v0, "DictionaryResultController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/DictionaryResultController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto :goto_0

    :cond_2
    const-string v0, "EmailController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/voicesearch/fragments/EmailController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getEmailSender()Lcom/google/android/voicesearch/util/EmailSender;

    move-result-object v4

    const-class v5, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-direct {p0, v5}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getFallbackController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v5

    check-cast v5, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/EmailController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/util/EmailSender;Lcom/google/android/voicesearch/contacts/ContactSelectController;)V

    goto :goto_0

    :cond_3
    const-string v0, "FlightResultController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/voicesearch/fragments/FlightResultController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/FlightResultController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto :goto_0

    :cond_4
    const-string v0, "HighConfidenceAnswerController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto :goto_0

    :cond_5
    const-string v0, "HtmlAnswerController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    new-instance v0, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v1

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;-><init>(Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    goto/16 :goto_0

    :cond_6
    const-string v0, "ImageResultController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    new-instance v0, Lcom/google/android/voicesearch/fragments/ImageResultController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/voicesearch/fragments/ImageResultController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Ljava/lang/String;Lcom/google/android/searchcommon/CoreSearchServices;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "SingleImageResultController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    new-instance v0, Lcom/google/android/voicesearch/fragments/SingleImageResultController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/voicesearch/fragments/SingleImageResultController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Ljava/lang/String;Lcom/google/android/searchcommon/CoreSearchServices;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "MediumConfidenceAnswerController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "MessageEditorController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v1, Lcom/google/android/voicesearch/fragments/MessageEditorController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    const-class v0, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getFallbackController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/voicesearch/fragments/MessageEditorController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/contacts/ContactSelectController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_a
    const-string v0, "MultipleLocalResultsController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    new-instance v0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto/16 :goto_0

    :cond_b
    const-string v0, "OpenAppController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Lcom/google/android/voicesearch/fragments/OpenAppController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v4, Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->createPreferredApplicationsManager()Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/voicesearch/util/AppSelectionHelper;-><init>(Lcom/google/android/voicesearch/util/PreferredApplicationsManager;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/OpenAppController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V

    goto/16 :goto_0

    :cond_c
    const-string v0, "OpenUrlController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v0, Lcom/google/android/voicesearch/fragments/OpenUrlController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/voicesearch/fragments/OpenUrlController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    goto/16 :goto_0

    :cond_d
    const-string v0, "PhoneCallController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    new-instance v1, Lcom/google/android/voicesearch/fragments/PhoneCallController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    const-class v0, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getFallbackController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/voicesearch/fragments/PhoneCallController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/contacts/ContactSelectController;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_e
    const-string v0, "PlayMusicController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    new-instance v0, Lcom/google/android/voicesearch/fragments/PlayMusicController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->createPreferredApplicationsManager()Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/voicesearch/util/AppSelectionHelper;-><init>(Lcom/google/android/voicesearch/util/PreferredApplicationsManager;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/PlayMusicController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V

    goto/16 :goto_0

    :cond_f
    const-string v0, "PlayMovieController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    new-instance v0, Lcom/google/android/voicesearch/fragments/PlayMovieController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->createPreferredApplicationsManager()Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/voicesearch/util/AppSelectionHelper;-><init>(Lcom/google/android/voicesearch/util/PreferredApplicationsManager;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->createAccountNameSupplier()Lcom/google/common/base/Supplier;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/PlayMovieController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;Lcom/google/common/base/Supplier;)V

    goto/16 :goto_0

    :cond_10
    const-string v0, "OpenBookController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    new-instance v0, Lcom/google/android/voicesearch/fragments/OpenBookController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->createPreferredApplicationsManager()Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/voicesearch/util/AppSelectionHelper;-><init>(Lcom/google/android/voicesearch/util/PreferredApplicationsManager;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->createAccountNameSupplier()Lcom/google/common/base/Supplier;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/OpenBookController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;Lcom/google/common/base/Supplier;)V

    goto/16 :goto_0

    :cond_11
    const-string v0, "OpenAppPlayMediaController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    new-instance v0, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->createPreferredApplicationsManager()Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/voicesearch/util/AppSelectionHelper;-><init>(Lcom/google/android/voicesearch/util/PreferredApplicationsManager;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V

    goto/16 :goto_0

    :cond_12
    const-string v0, "PuntController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    new-instance v0, Lcom/google/android/voicesearch/fragments/PuntController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/PuntController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto/16 :goto_0

    :cond_13
    const-string v0, "QueryCalendarController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    new-instance v0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getCalendarHelper()Lcom/google/android/voicesearch/util/CalendarHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getCalendarTextHelper()Lcom/google/android/voicesearch/util/CalendarTextHelper;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/util/CalendarTextHelper;)V

    goto/16 :goto_0

    :cond_14
    const-string v0, "SelfNoteController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    new-instance v0, Lcom/google/android/voicesearch/fragments/SelfNoteController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getEmailSender()Lcom/google/android/voicesearch/util/EmailSender;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f0d040d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/SelfNoteController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/util/EmailSender;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V

    goto/16 :goto_0

    :cond_15
    const-string v0, "SetAlarmController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    new-instance v0, Lcom/google/android/voicesearch/fragments/SetAlarmController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/SetAlarmController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/text/DateFormat;)V

    goto/16 :goto_0

    :cond_16
    const-string v0, "SetReminderController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    new-instance v0, Lcom/google/android/voicesearch/fragments/SetReminderController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getCalendarTextHelper()Lcom/google/android/voicesearch/util/CalendarTextHelper;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getReminderSaver()Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/SetReminderController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/util/CalendarTextHelper;Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)V

    goto/16 :goto_0

    :cond_17
    const-string v0, "ShowContactInformationController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    new-instance v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    const-class v3, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getFallbackController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getEmailSender()Lcom/google/android/voicesearch/util/EmailSender;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0d045a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    const-string v9, "clipboard"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/ClipboardManager;

    iget-object v9, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/contacts/ContactSelectController;Lcom/google/android/speech/contacts/ContactLookup;Lcom/google/android/voicesearch/util/EmailSender;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Landroid/content/ClipboardManager;Lcom/google/android/searchcommon/DeviceCapabilityManager;)V

    goto/16 :goto_0

    :cond_18
    const-string v0, "SingleLocalResultController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    new-instance v0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/DeviceCapabilityManager;)V

    goto/16 :goto_0

    :cond_19
    const-string v0, "SportsResultController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    new-instance v0, Lcom/google/android/voicesearch/fragments/SportsResultController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/SportsResultController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto/16 :goto_0

    :cond_1a
    const-string v0, "WeatherResultController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    new-instance v0, Lcom/google/android/voicesearch/fragments/WeatherResultController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/WeatherResultController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto/16 :goto_0

    :cond_1b
    const-string v0, "ContactSelectController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/voicesearch/contacts/ContactSelectController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/speech/contacts/ContactLookup;Lcom/google/android/voicesearch/settings/Settings;)V

    goto/16 :goto_0

    :cond_1c
    const-string v0, "SocialUpdateController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    new-instance v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-class v5, Lcom/google/android/voicesearch/fragments/PuntController;

    invoke-direct {p0, v5}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getFallbackController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v5

    check-cast v5, Lcom/google/android/voicesearch/fragments/PuntController;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Landroid/content/pm/PackageManager;Lcom/google/android/voicesearch/fragments/PuntController;Ljava/util/concurrent/ExecutorService;)V

    goto/16 :goto_0

    :cond_1d
    const-string v0, "GogglesGenericController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    new-instance v0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto/16 :goto_0

    :cond_1e
    const-string v0, "RecognizedContactController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    new-instance v0, Lcom/google/android/voicesearch/fragments/RecognizedContactController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/RecognizedContactController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto/16 :goto_0

    :cond_1f
    const-string v0, "HelpController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    new-instance v0, Lcom/google/android/voicesearch/fragments/HelpController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-static {v8}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/fragments/HelpController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Landroid/content/ContentResolver;Lcom/google/android/searchcommon/DeviceCapabilityManager;IZ)V

    goto/16 :goto_0

    :cond_20
    const-string v0, "PlainTitleAndTextController"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    new-instance v0, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mPresenter:Lcom/google/android/voicesearch/CardController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mTts:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    goto/16 :goto_0

    :cond_21
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown controller name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private createPreferredApplicationsManager()Lcom/google/android/voicesearch/util/PreferredApplicationsManager;
    .locals 2

    new-instance v0, Lcom/google/android/voicesearch/util/PreferredApplicationsManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/util/PreferredApplicationsManager;-><init>(Landroid/content/SharedPreferences;)V

    return-object v0
.end method

.method private getCalendarHelper()Lcom/google/android/voicesearch/util/CalendarHelper;
    .locals 6

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/util/CalendarHelper;-><init>(Lcom/google/android/speech/helper/AccountHelper;Landroid/content/ContentResolver;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Z)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    return-object v0
.end method

.method private getCalendarTextHelper()Lcom/google/android/voicesearch/util/CalendarTextHelper;
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/util/CalendarTextHelper;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/util/CalendarTextHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    return-object v0
.end method

.method private getEmailSender()Lcom/google/android/voicesearch/util/EmailSender;
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/util/EmailSender;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/util/EmailSender;-><init>(Lcom/google/android/speech/helper/AccountHelper;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    return-object v0
.end method

.method private getFallbackController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;>(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->setFallbackController()V

    return-object v0
.end method

.method private getReminderSaver()Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetFactory;->createReminderSaver()Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory;->mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    return-object v0
.end method


# virtual methods
.method public getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;>(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.google.android.voicesearch.fragments"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "ContactSelectController"

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v4

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->createControllerFor(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v2, p1, :cond_2

    :goto_1
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    return-object v0

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    move v4, v3

    goto :goto_1
.end method

.method public getController(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;>(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/Class;)Lcom/google/android/voicesearch/fragments/AbstractCardController;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method
