.class public interface abstract Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;
.super Ljava/lang/Object;
.source "PlayMediaController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;
.implements Lcom/google/android/voicesearch/fragments/CountDownUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/PlayMediaController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract getImageDimensions()Landroid/view/ViewGroup$LayoutParams;
.end method

.method public abstract setAppLabel(I)V
.end method

.method public abstract setAppLabel(Ljava/lang/String;)V
.end method

.method public abstract setPrice(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setSelectorApps(Ljava/util/List;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract showImageBitmap(Landroid/graphics/Bitmap;)V
.end method

.method public abstract showImageDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract showImageUri(Landroid/net/Uri;)V
.end method

.method public abstract showOwnedMode(Z)V
.end method

.method public abstract showPlayStoreRating(D)V
.end method

.method public abstract showPreview(Z)V
.end method
