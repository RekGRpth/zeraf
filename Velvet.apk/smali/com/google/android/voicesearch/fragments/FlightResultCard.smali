.class public Lcom/google/android/voicesearch/fragments/FlightResultCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "FlightResultCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/FlightResultController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/FlightResultController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/FlightResultController$Ui;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCard:Lcom/google/android/velvet/cards/FlightCard;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/fragments/FlightResultCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/voicesearch/fragments/FlightResultCard;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private static getTime(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object v4, v6

    :goto_0
    return-object v4

    :cond_0
    :try_start_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0, p0}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "UTC"

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    :cond_1
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    if-eqz p1, :cond_2

    invoke-static {p1}, Lcom/google/android/velvet/cards/FlightCard;->fixTimeZone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, v5}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v7}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Landroid/util/TimeFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v7, Lcom/google/android/voicesearch/fragments/FlightResultCard;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Received unrecognizable time value \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    goto :goto_0

    :cond_2
    :try_start_1
    const-string p1, "UTC"
    :try_end_1
    .catch Landroid/util/TimeFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    new-instance v0, Lcom/google/android/velvet/cards/FlightCard;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/cards/FlightCard;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/FlightResultCard;->mCard:Lcom/google/android/velvet/cards/FlightCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/FlightResultCard;->mCard:Lcom/google/android/velvet/cards/FlightCard;

    return-object v0
.end method

.method public setData(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)V
    .locals 9
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/velvet/cards/FlightCard$Builder;

    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;->getAirlineName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;->getNumber()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v7, v8}, Lcom/google/android/velvet/cards/FlightCard$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;->getFlightList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->hasStatusCode()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->hasDepartureTimeScheduled()Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    sget-object v7, Lcom/google/android/voicesearch/fragments/FlightResultCard;->TAG:Ljava/lang/String;

    const-string v8, "Flight segment data was missing"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getDepartureTimeZone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getDepartureTimeScheduled()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/google/android/voicesearch/fragments/FlightResultCard;->getTime(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/FlightCard$Builder;->addSegment()Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getStatusCode()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->setStatus(I)Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;

    invoke-virtual {v6}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->departure()Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getDepartureAirportName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setAirportName(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getDepartureAirportCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setAirportCode(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setScheduled(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getDepartureTimeActual()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v2}, Lcom/google/android/voicesearch/fragments/FlightResultCard;->getTime(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setActual(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getDepartureTerminal()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setTerminal(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getDepartureGate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setGate(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getArrivalTimeZone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->arrival()Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getArrivalAirportName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setAirportName(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getArrivalAirportCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setAirportCode(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getArrivalTimeScheduled()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v0}, Lcom/google/android/voicesearch/fragments/FlightResultCard;->getTime(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setScheduled(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getArrivalTimeActual()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v0}, Lcom/google/android/voicesearch/fragments/FlightResultCard;->getTime(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setActual(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getArrivalTerminal()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setTerminal(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightData;->getArrivalGate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setGate(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    goto/16 :goto_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/FlightResultCard;->mCard:Lcom/google/android/velvet/cards/FlightCard;

    invoke-virtual {v1, v7}, Lcom/google/android/velvet/cards/FlightCard$Builder;->update(Lcom/google/android/velvet/cards/FlightCard;)V

    return-void
.end method
