.class public Lcom/google/android/voicesearch/fragments/SocialUpdateCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "SocialUpdateCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/SocialUpdateController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;"
    }
.end annotation


# instance fields
.field private mUpdateText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private setConfirmTextWithNetwork(I)V
    .locals 4
    .param p1    # I

    const v0, 0x7f0d0420

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->setConfirmText(I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f0400b7

    invoke-virtual {p0, p2, p3, v1}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmIcon(I)V

    const v1, 0x7f10022b

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->mUpdateText:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/TextView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->mUpdateText:Landroid/widget/TextView;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->clearTextViews([Landroid/widget/TextView;)V

    return-object v0
.end method

.method public setMessageBody(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->mUpdateText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->mUpdateText:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->uiReady()V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public showEmptyView()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->mUpdateText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->mUpdateText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0396

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showNewUpdate(I)V
    .locals 1
    .param p1    # I

    const v0, 0x7f020051

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->setConfirmIcon(I)V

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->setConfirmTextWithNetwork(I)V

    return-void
.end method

.method public showPostUpdate(I)V
    .locals 1
    .param p1    # I

    const v0, 0x7f020051

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->setConfirmIcon(I)V

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;->setConfirmTextWithNetwork(I)V

    return-void
.end method
