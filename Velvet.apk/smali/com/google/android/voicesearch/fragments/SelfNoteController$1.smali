.class Lcom/google/android/voicesearch/fragments/SelfNoteController$1;
.super Ljava/lang/Object;
.source "SelfNoteController.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/SelfNoteController;->start(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/SelfNoteController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/SelfNoteController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController$1;->this$0:Lcom/google/android/voicesearch/fragments/SelfNoteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Landroid/net/Uri;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SelfNoteController$1;->this$0:Lcom/google/android/voicesearch/fragments/SelfNoteController;

    sget-object v1, Lcom/google/android/speech/audio/AudioUtils$Encoding;->AMR:Lcom/google/android/speech/audio/AudioUtils$Encoding;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->getLastAudioUri(Lcom/google/android/speech/audio/AudioUtils$Encoding;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteController$1;->call()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
