.class public Lcom/google/android/voicesearch/fragments/SetReminderController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "SetReminderController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

.field private final mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

.field private final mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/util/CalendarTextHelper;Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Lcom/google/android/voicesearch/util/CalendarTextHelper;
    .param p5    # Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    return-void
.end method

.method private editReminder(I)V
    .locals 3

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const-class v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "action"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    if-eqz p1, :cond_1

    const-string v1, "preferredTriggerType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->startActivity(Landroid/content/Intent;)Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->actionComplete()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public detach()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->detach()V

    return-void
.end method

.method public editWithDefaultLocation()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->editReminder(I)V

    return-void
.end method

.method public editWithDefaultTime()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->editReminder(I)V

    return-void
.end method

.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x22

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x26

    return v0
.end method

.method public initUi()V
    .locals 10

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v5

    check-cast v5, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v6}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLabel()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->setLabel(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->getTriggerType(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v6}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLabel()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-interface {v5, v6}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->setNoTrigger(Z)V

    :goto_1
    return-void

    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    :pswitch_0
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v6}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getAbsoluteTimeTrigger()Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;->getTimeMs()J

    move-result-wide v8

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;->hasSymbolicTime()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;->getSymbolicTime()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->fromActionV2Symbol(I)Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    move-result-object v6

    :goto_2
    invoke-virtual {v7, v8, v9, v6}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->formatSymbolicTime(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->setAbsoluteTimeTrigger(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {v6}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLocationTrigger()Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;->getLocation()Lcom/google/majel/proto/ActionV2Protos$Location;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getAlias()I

    move-result v1

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;->getType()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    :pswitch_2
    goto :goto_1

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAlias()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5, v1, v2}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->setArrivingTrigger(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-interface {v5, v2}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->setArrivingTrigger(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAlias()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5, v1, v2}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->setLeavingTrigger(ILjava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-interface {v5, v2}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->setLeavingTrigger(Ljava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected internalBailOut()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->editReminder(I)V

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    new-instance v2, Lcom/google/android/voicesearch/fragments/SetReminderController$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/SetReminderController$1;-><init>(Lcom/google/android/voicesearch/fragments/SetReminderController;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->saveReminder(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;Lcom/google/android/speech/callback/SimpleCallback;)V

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->parseFrom([B)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    return-void
.end method

.method public restoreStart()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->showCard()V

    return-void
.end method

.method public start(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SetReminderController;->mAction:Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->showCardAndPlayTts()V

    return-void
.end method
