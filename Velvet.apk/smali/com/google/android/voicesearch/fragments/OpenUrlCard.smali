.class public Lcom/google/android/voicesearch/fragments/OpenUrlCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "OpenUrlCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/OpenUrlController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/OpenUrlController$Ui;"
    }
.end annotation


# instance fields
.field private mDisplayUrlView:Landroid/widget/TextView;

.field private mNameView:Landroid/widget/TextView;

.field private mPreviewView:Lcom/google/android/velvet/ui/WebImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f040085

    invoke-virtual {p0, p2, p3, v1}, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    const v1, 0x7f10019f

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->mNameView:Landroid/widget/TextView;

    const v1, 0x7f1001a0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->mDisplayUrlView:Landroid/widget/TextView;

    const v1, 0x7f10019e

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->mPreviewView:Lcom/google/android/velvet/ui/WebImageView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->mPreviewView:Lcom/google/android/velvet/ui/WebImageView;

    new-instance v2, Lcom/google/android/voicesearch/fragments/OpenUrlCard$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/OpenUrlCard$1;-><init>(Lcom/google/android/voicesearch/fragments/OpenUrlCard;)V

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/WebImageView;->setOnDownloadListener(Lcom/google/android/velvet/ui/WebImageView$Listener;)V

    const v1, 0x7f0d03bd

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmText(I)V

    const v1, 0x7f020089

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmIcon(I)V

    return-object v0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDisplayUrl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->mDisplayUrlView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->mNameView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPreviewUrl(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->mPreviewView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenUrlCard;->mPreviewView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
