.class Lcom/google/android/voicesearch/fragments/AbstractCardView$1;
.super Ljava/lang/Object;
.source "AbstractCardView.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/AbstractCardView;->onCreateView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/AbstractCardView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$1;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$1;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleAttach()V

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$1;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    # getter for: Lcom/google/android/voicesearch/fragments/AbstractCardView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->access$000(Lcom/google/android/voicesearch/fragments/AbstractCardView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "#onViewDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$1;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleDetach()V

    return-void
.end method
