.class final Lcom/google/android/voicesearch/fragments/CalendarEventController$3;
.super Ljava/lang/Object;
.source "CalendarEventController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/CalendarEventController;->createData(Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$attendees:Ljava/util/List;

.field final synthetic val$endTimeMs:J

.field final synthetic val$location:Ljava/lang/String;

.field final synthetic val$reminders:Ljava/util/List;

.field final synthetic val$startTimeMs:J

.field final synthetic val$summary:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$summary:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$location:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$startTimeMs:J

    iput-wide p5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$endTimeMs:J

    iput-object p7, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$attendees:Ljava/util/List;

    iput-object p8, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$reminders:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttendees()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$attendees:Ljava/util/List;

    return-object v0
.end method

.method public getEndTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$endTimeMs:J

    return-wide v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$location:Ljava/lang/String;

    return-object v0
.end method

.method public getReminders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$reminders:Ljava/util/List;

    return-object v0
.end method

.method public getStartTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$startTimeMs:J

    return-wide v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;->val$summary:Ljava/lang/String;

    return-object v0
.end method
