.class public Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "MultipleLocalResultsController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mActionType:I

.field private mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

.field private mTransportationMethod:I


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;)I
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    iget v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mActionType:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;)I
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    iget v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mTransportationMethod:I

    return v0
.end method


# virtual methods
.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mActionType:I

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->getActionTypeLog(I)I

    move-result v0

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "transportation_method"

    iget v2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mTransportationMethod:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x19

    return v0
.end method

.method public initUi()V
    .locals 15

    const/4 v14, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getOriginCount()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1, v14}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResult(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    move-result-object v13

    :goto_0
    iget v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mActionType:I

    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->getActionIconImageResource(I)I

    move-result v4

    new-instance v12, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$1;

    invoke-direct {v12, p0}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$1;-><init>(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;

    const/4 v11, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->hasPreviewImage()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getPreviewImage()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v8

    array-length v1, v8

    invoke-static {v8, v14, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-interface {v0, v7, v12}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;->setMapImageBitmap(Landroid/graphics/Bitmap;Landroid/view/View$OnClickListener;)V

    const/4 v11, 0x1

    :cond_0
    if-nez v11, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getPreviewImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v12}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;->setMapImageUrl(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    :cond_1
    iget v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mActionType:I

    iget v2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mTransportationMethod:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;->setActionTypeAndTransportationMethod(II)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getActionType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    const/4 v14, 0x1

    :cond_2
    const/4 v10, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResultList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v10, v1, :cond_6

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1, v10}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResult(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    move-result-object v9

    new-instance v5, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$2;

    invoke-direct {v5, p0, v9}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$2;-><init>(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;)V

    new-instance v6, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;

    invoke-direct {v6, p0, v13, v9}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;-><init>(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;)V

    if-eqz v14, :cond_5

    invoke-virtual {v9}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v9}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;->addLocalResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResultList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v10, v1, :cond_3

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;->addLocalResultDivider()V

    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_0

    :cond_5
    const-string v3, ""

    goto :goto_2

    :cond_6
    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-direct {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "transportation_method"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mTransportationMethod:I

    return-void
.end method

.method public setTransportationMethod(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mTransportationMethod:I

    return-void
.end method

.method public start(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getActionType()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mActionType:I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getTransportationMethod()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mTransportationMethod:I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->showCardAndPlayTts()V

    return-void
.end method
