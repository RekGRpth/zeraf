.class public Lcom/google/android/voicesearch/fragments/HelpCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "HelpCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/HelpController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/HelpController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/HelpController$Ui;"
    }
.end annotation


# instance fields
.field private mDatePreview:Landroid/view/View;

.field private mDatePreviewDay:Landroid/widget/TextView;

.field private mDatePreviewDayOfWeek:Landroid/widget/TextView;

.field private mDatePreviewMonth:Landroid/widget/TextView;

.field private mExample:Landroid/widget/TextView;

.field private mHeadline:Landroid/widget/TextView;

.field private mImagePreviewBack:Lcom/google/android/velvet/ui/WebImageView;

.field private mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;

.field private mLastShownPreview:Landroid/view/View;

.field private mPlaceholderAvatar:Landroid/graphics/drawable/Drawable;

.field private mRefreshExampleButton:Landroid/widget/TextView;

.field private mTimePreview:Landroid/view/View;

.field private mTimePreviewHour:Landroid/widget/TextView;

.field private mTimePreviewMinute:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/HelpCard;)Lcom/google/android/velvet/ui/WebImageView;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/HelpCard;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mPlaceholderAvatar:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/voicesearch/fragments/HelpCard;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpCard;
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mPlaceholderAvatar:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/HelpCard;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpCard;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/HelpCard;->pushImagePreviewFrontToBack()V

    return-void
.end method

.method private pushImagePreviewFrontToBack()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewBack:Lcom/google/android/velvet/ui/WebImageView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewBack:Lcom/google/android/velvet/ui/WebImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/HelpCard;->showPreview(Landroid/view/View;)V

    return-void
.end method

.method private showPreview(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const-wide/16 v2, 0x1f4

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mLastShownPreview:Landroid/view/View;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mLastShownPreview:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mLastShownPreview:Landroid/view/View;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    invoke-static {p1}, Lcom/google/android/velvet/ui/util/Animations;->showAndFadeIn(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    :goto_0
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mLastShownPreview:Landroid/view/View;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f040056

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10011c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mHeadline:Landroid/widget/TextView;

    const v1, 0x7f10011d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mExample:Landroid/widget/TextView;

    const v1, 0x7f100127

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mRefreshExampleButton:Landroid/widget/TextView;

    const v1, 0x7f100125

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;

    const v1, 0x7f100126

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewBack:Lcom/google/android/velvet/ui/WebImageView;

    const v1, 0x7f10011e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreview:Landroid/view/View;

    const v1, 0x7f100120

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewDay:Landroid/widget/TextView;

    const v1, 0x7f100121

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewMonth:Landroid/widget/TextView;

    const v1, 0x7f10011f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewDayOfWeek:Landroid/widget/TextView;

    const v1, 0x7f100122

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mTimePreview:Landroid/view/View;

    const v1, 0x7f100123

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mTimePreviewHour:Landroid/widget/TextView;

    const v1, 0x7f100124

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mTimePreviewMinute:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mRefreshExampleButton:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/voicesearch/fragments/HelpCard$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/HelpCard$1;-><init>(Lcom/google/android/voicesearch/fragments/HelpCard;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public setExampleQuery(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HelpCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d03a2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mLastShownPreview:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mExample:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/velvet/ui/util/Animations;->fadeUpdateText(Landroid/widget/TextView;Ljava/lang/String;)Landroid/view/ViewPropertyAnimator;

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mExample:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setHeadline(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mHeadline:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPreviewContact(Lcom/google/android/voicesearch/fragments/HelpController$Contact;)V
    .locals 5
    .param p1    # Lcom/google/android/voicesearch/fragments/HelpController$Contact;

    new-instance v0, Lcom/google/android/voicesearch/fragments/HelpCard$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/HelpCard$2;-><init>(Lcom/google/android/voicesearch/fragments/HelpCard;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/fragments/HelpController$Contact;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/HelpCard$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public setPreviewDate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/high16 v6, 0x3f400000

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mLastShownPreview:Landroid/view/View;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x64

    const/high16 v2, 0x3f000000

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/ViewPropertyAnimator;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewDayOfWeek:Landroid/widget/TextView;

    invoke-static {v5, p1, v6}, Lcom/google/android/velvet/ui/util/Animations;->fadeScaleUpdateText(Landroid/widget/TextView;Ljava/lang/String;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewDay:Landroid/widget/TextView;

    invoke-static {v5, p2, v6}, Lcom/google/android/velvet/ui/util/Animations;->fadeScaleUpdateText(Landroid/widget/TextView;Ljava/lang/String;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewMonth:Landroid/widget/TextView;

    invoke-static {v5, p3, v6}, Lcom/google/android/velvet/ui/util/Animations;->fadeScaleUpdateText(Landroid/widget/TextView;Ljava/lang/String;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/velvet/ui/util/Animations;->stagger(JF[Landroid/view/ViewPropertyAnimator;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreview:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/HelpCard;->showPreview(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewDayOfWeek:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewDay:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mDatePreviewMonth:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setPreviewTime(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3f400000

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HelpCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d03a3

    new-array v3, v8, [Ljava/lang/Object;

    aput-object p2, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mLastShownPreview:Landroid/view/View;

    if-eqz v1, :cond_0

    const-wide/16 v1, 0x64

    const/high16 v3, 0x3f000000

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/view/ViewPropertyAnimator;

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mTimePreviewHour:Landroid/widget/TextView;

    invoke-static {v5, p1, v6}, Lcom/google/android/velvet/ui/util/Animations;->fadeScaleUpdateText(Landroid/widget/TextView;Ljava/lang/String;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mTimePreviewMinute:Landroid/widget/TextView;

    invoke-static {v5, v0, v6}, Lcom/google/android/velvet/ui/util/Animations;->fadeScaleUpdateText(Landroid/widget/TextView;Ljava/lang/String;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/velvet/ui/util/Animations;->stagger(JF[Landroid/view/ViewPropertyAnimator;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mTimePreview:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/HelpCard;->showPreview(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mTimePreviewHour:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mTimePreviewMinute:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setPreviewUrl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/HelpCard;->pushImagePreviewFrontToBack()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    return-void
.end method
