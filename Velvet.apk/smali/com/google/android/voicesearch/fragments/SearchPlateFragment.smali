.class public Lcom/google/android/voicesearch/fragments/SearchPlateFragment;
.super Lcom/google/android/velvet/ui/VelvetFragment;
.source "SearchPlateFragment.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/SearchPlateUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/SearchPlateFragment$7;,
        Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;,
        Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/velvet/ui/VelvetFragment",
        "<",
        "Lcom/google/android/velvet/presenter/SearchPlatePresenter;",
        ">;",
        "Lcom/google/android/velvet/presenter/SearchPlateUi;"
    }
.end annotation


# instance fields
.field private mContent:Landroid/view/ViewGroup;

.field private mCurrentNontextUi:Landroid/view/View;

.field private mDisplayText:Landroid/widget/TextView;

.field private mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

.field private mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

.field private mLastQueryString:Ljava/lang/CharSequence;

.field private mMicGuide:Landroid/view/View;

.field private mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

.field private mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

.field private mSoundSearchPromotedQuery:Landroid/view/View;

.field private mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

.field private mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

.field private mSpeechUi:Landroid/view/View;

.field private mStreamingTextView:Lcom/google/android/voicesearch/ui/StreamingTextView;

.field private mVoiceState:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetFragment;-><init>()V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->INITIALIZING:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mVoiceState:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/goggles/ui/GogglesPlate;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/velvet/ui/widget/SearchPlate;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    return-object v0
.end method

.method private ensureGogglesUi()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->isGogglesUiInflated()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    const v2, 0x7f1000f8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/goggles/ui/GogglesPlate;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    check-cast v1, Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iget-object v3, v1, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->gogglesCaptureCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    check-cast v1, Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iget-object v1, v1, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->gogglesResponseCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->setCallbacks(Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;)V

    goto :goto_0
.end method

.method private ensureSoundSearchUi()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->isSoundSearchUiInflated()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    const v2, 0x7f100234

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    new-instance v2, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$3;-><init>(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->setListener(Lcom/google/android/ears/ui/SoundSearchRecognizerView$Listener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v1, v2}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    goto :goto_0
.end method

.method private ensureSpeechUi()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    const v2, 0x7f100233

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    const v2, 0x7f100238

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    const v2, 0x7f10023a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/ui/StreamingTextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mStreamingTextView:Lcom/google/android/voicesearch/ui/StreamingTextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    const v2, 0x7f100239

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    new-instance v2, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$2;-><init>(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    const v2, 0x7f1001fc

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mMicGuide:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    const v2, 0x7f100119

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/ui/RecognizerView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/ui/RecognizerView;->setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    check-cast v1, Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iget-object v1, v1, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->recognizerCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    invoke-virtual {v2, v1}, Lcom/google/android/voicesearch/ui/RecognizerView;->setCallback(Lcom/google/android/voicesearch/ui/RecognizerView$Callback;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mVoiceState:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    sget-object v2, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->INITIALIZING:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->showInitializingInternal()V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private hideMicGuide()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mMicGuide:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mMicGuide:Landroid/view/View;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isGogglesUiInflated()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSoundSearchUiInflated()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSpeechUiInflated()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeHideSoundSearchPromotedQuery(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method private setInputMode(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$7;->$SwitchMap$com$google$android$voicesearch$fragments$SearchPlateFragment$InputMode:[I

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$7;->$SwitchMap$com$google$android$voicesearch$fragments$SearchPlateFragment$InputMode:[I

    invoke-virtual {p1}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    :goto_1
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->stopCamera()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->startCamera()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method private setVoiceState(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    const/4 v2, 0x4

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->isSpeechUiInflated()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mVoiceState:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mVoiceState:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$7;->$SwitchMap$com$google$android$voicesearch$fragments$SearchPlateFragment$VoiceState:[I

    invoke-virtual {p1}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->hideMicGuide()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->showInitializingInternal()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0d039f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->hideMicGuide()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mStreamingTextView:Lcom/google/android/voicesearch/ui/StreamingTextView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/StreamingTextView;->reset()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0d0432

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0d03ca

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/velvet/ui/util/Animations;->showAndFadeIn(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mStreamingTextView:Lcom/google/android/voicesearch/ui/StreamingTextView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/StreamingTextView;->reset()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->showMicGuide()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0d0446

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->hideMicGuide()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private showInitializingInternal()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0d0433

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mStreamingTextView:Lcom/google/android/voicesearch/ui/StreamingTextView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/StreamingTextView;->reset()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->hideMicGuide()V

    return-void
.end method

.method private showMicGuide()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mMicGuide:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mMicGuide:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/velvet/ui/util/Animations;->showAndFadeIn(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showNontextInputView(ZLandroid/view/View;)V
    .locals 3
    .param p1    # Z
    .param p2    # Landroid/view/View;

    const/high16 v2, 0x3f800000

    const/16 v1, 0x8

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->maybeHideSoundSearchPromotedQuery(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    if-ne p2, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->exitViaVoice()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    :goto_1
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_4

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$5;

    invoke-direct {v1, p0, p2}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$5;-><init>(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$4;

    invoke-direct {v1, p0, p2}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$4;-><init>(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method


# virtual methods
.method public doFinalFocus(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ui/GogglesPlate;->doFinalFocus(Ljava/lang/Runnable;)V

    return-void
.end method

.method public focusSearchBox()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->focusSearchBox()V

    return-void
.end method

.method public getGogglesQueryImage()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->getLastPreviewFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideFocus()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideFocus()V

    return-void
.end method

.method public hideKeyboard()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideFocus()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideKeyboard()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method

.method public hideLogo(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideLogo(Z)V

    return-void
.end method

.method public hideProgress()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideProgress()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->isGogglesUiInflated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->stopSpinner()V

    :cond_0
    return-void
.end method

.method public isGogglesCapturing()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_CAPTURE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTorchOn()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->isTorchOn()Z

    move-result v0

    return v0
.end method

.method public notifyGogglesQuerySent()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->notifyGogglesQuerySent()V

    return-void
.end method

.method public notifyGogglesResult()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->notifyGogglesResult()V

    return-void
.end method

.method public notifyNoGogglesResult()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->notifyNoGogglesResult()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v0, 0x7f0400b9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    const v1, 0x7f10020a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/widget/SearchPlate;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    new-instance v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$1;-><init>(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_CAPTURE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->stopCamera()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/VelvetFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mVoiceState:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "SearchPlateFragment.gogglesQueryText"

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mLastQueryString:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string v0, "SearchPlateFragment.inputMode"

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "SearchPlateFragment.musicActionVisible"

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2}, Lcom/google/android/velvet/ui/VelvetFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    check-cast v0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->setPresenter(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V

    return-void
.end method

.method public pauseUi()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->pauseUi()V

    return-void
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    const-string v2, "SearchPlateFragment.musicActionVisible"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->INITIALIZING:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setVoiceState(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->getPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->getGogglesQueryImage()Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v2, "SearchPlateFragment.gogglesQueryText"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v2, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->setQueryImage(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mLastQueryString:Ljava/lang/CharSequence;

    :cond_1
    const-string v2, "SearchPlateFragment.inputMode"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "SearchPlateFragment.inputMode"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v3, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->SOUND_SEARCH:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v2, v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSoundSearchUi()V

    :cond_2
    return-void
.end method

.method public setFinalRecognizedText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mStreamingTextView:Lcom/google/android/voicesearch/ui/StreamingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/StreamingTextView;->setFinalRecognizedText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setHintText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->setHintText(I)V

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public setQuery(Lcom/google/android/velvet/Query;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getGogglesQueryImage()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->setQueryText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->setQueryImage(Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->setQuery(Lcom/google/android/velvet/Query;)V

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mLastQueryString:Ljava/lang/CharSequence;

    return-void
.end method

.method public setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/RecognizerView;->setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    invoke-virtual {v0, p1}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    :cond_1
    return-void
.end method

.method public setTextQueryCorrections(Landroid/text/Spanned;)V
    .locals 1
    .param p1    # Landroid/text/Spanned;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->setTextQueryCorrections(Landroid/text/Spanned;)V

    return-void
.end method

.method public setTorch(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ui/GogglesPlate;->setTorch(Z)V

    return-void
.end method

.method public showClearButton(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showClearButton(Z)V

    return-void
.end method

.method public showGogglesInput(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->enterDirectly()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->showNontextInputView(ZLandroid/view/View;)V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_CAPTURE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setInputMode(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->enterViaTextToCaptureMode(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT_FROM_VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->enterViaVoiceToCaptureMode(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_RESPONSE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->transitionBackToCaptureMode()V

    goto :goto_0
.end method

.method public showGogglesResponse(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mLastQueryString:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->setQueryText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/goggles/ui/GogglesPlate;->enterViaTextToResponseMode(II)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->showNontextInputView(ZLandroid/view/View;)V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->GOGGLES_RESPONSE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setInputMode(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT_FROM_VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/goggles/ui/GogglesPlate;->enterViaVoiceToResponseMode(II)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0, p2}, Lcom/google/android/goggles/ui/GogglesPlate;->goToResponseMode(I)V

    goto :goto_0
.end method

.method public showInitializing()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->INITIALIZING:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setVoiceState(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->showNotListening()V

    return-void
.end method

.method public showInitializingMic()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->INITIALIZING_MIC:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setVoiceState(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->showInitializingMic()V

    return-void
.end method

.method public showKeyboard()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showKeyboard()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method

.method public showListening()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->RECORDING:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setVoiceState(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->showListening()V

    return-void
.end method

.method public showLogo()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showLogo()V

    return-void
.end method

.method public showNotListening()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->SOUND_SEARCH:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSoundSearchUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    invoke-virtual {v0}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->showNotListening()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->READY:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setVoiceState(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->showNotListening()V

    goto :goto_0
.end method

.method public showProgress()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showProgress()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->isGogglesUiInflated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->startSpinner()V

    :cond_0
    return-void
.end method

.method public showRecognizing()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->RECOGNIZING:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setVoiceState(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->showRecognizing()V

    return-void
.end method

.method public showRecording()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->showRecording()V

    return-void
.end method

.method public showSoundSearchInput(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSoundSearchUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->showNontextInputView(ZLandroid/view/View;)V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->SOUND_SEARCH:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setInputMode(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    invoke-virtual {v0}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->showListening()V

    return-void
.end method

.method public showSoundSearchListening()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSoundSearchUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchUi:Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    invoke-virtual {v0}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->showListening()V

    return-void
.end method

.method public showSoundSearchPromotedQuery()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSoundSearchPromotedQuery:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method public showSpeechInput(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSpeechUi:Landroid/view/View;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->showNontextInputView(ZLandroid/view/View;)V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setInputMode(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;)V

    return-void
.end method

.method public showTapToSpeak()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;->READY:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setVoiceState(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$VoiceState;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mRecognizerView:Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->showNotListening()V

    return-void
.end method

.method public showTextInput(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x4

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    sget-object v2, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->maybeHideSoundSearchPromotedQuery(Z)V

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    :cond_2
    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setInputMode(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;)V

    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->isSpeechUiInflated()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->isGogglesUiInflated()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/widget/SearchPlate;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/widget/SearchPlate;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    if-ne v1, v2, :cond_7

    const-wide/16 v1, 0xfa

    :goto_1
    invoke-virtual {v3, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;

    invoke-direct {v2, p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;-><init>(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v1}, Lcom/google/android/goggles/ui/GogglesPlate;->exitViaText()V

    :cond_5
    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mCurrentNontextUi:Landroid/view/View;

    :cond_6
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mContent:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    goto/16 :goto_0

    :cond_7
    const-wide/16 v1, 0x0

    goto :goto_1
.end method

.method public showTextQueryMode()V
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setInputMode(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showTextQueryMode()V

    return-void
.end method

.method public showVoiceQueryMode()V
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT_FROM_VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->setInputMode(Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showVoiceQueryMode()V

    return-void
.end method

.method public startCamera()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->startCamera()V

    return-void
.end method

.method public stopCamera()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->stopCamera()V

    return-void
.end method

.method public stopSpinner()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureGogglesUi()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mGogglesUi:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->stopSpinner()V

    return-void
.end method

.method public updateRecognizedText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->ensureSpeechUi()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mDisplayText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mStreamingTextView:Lcom/google/android/voicesearch/ui/StreamingTextView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ui/StreamingTextView;->updateRecognizedText(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
