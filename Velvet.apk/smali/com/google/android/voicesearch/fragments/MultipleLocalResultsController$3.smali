.class Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;
.super Ljava/lang/Object;
.source "MultipleLocalResultsController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->initUi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

.field final synthetic val$dest:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

.field final synthetic val$origin:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;->this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;->val$origin:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;->val$dest:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;->this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;->this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    # getter for: Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mActionType:I
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->access$100(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;->val$origin:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;->val$dest:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$3;->this$0:Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    # getter for: Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->mTransportationMethod:I
    invoke-static {v4}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->access$200(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;)I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->createIntentForAction(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method
