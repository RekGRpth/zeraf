.class public Lcom/google/android/voicesearch/fragments/PlayMovieCard;
.super Lcom/google/android/voicesearch/fragments/PlayMediaCard;
.source "PlayMovieCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/PlayMovieController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/PlayMediaCard",
        "<",
        "Lcom/google/android/voicesearch/fragments/PlayMovieController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/PlayMovieController$Ui;"
    }
.end annotation


# instance fields
.field private mExpiryTime:Landroid/widget/TextView;

.field private mGenreView:Landroid/widget/TextView;

.field private mReleaseAndRuntimeInfo:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaCard;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v5, 0x7f040092

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->createMediaActionEditor(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    const v0, 0x7f1001cb

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mTitleView:Landroid/widget/TextView;

    const v0, 0x7f1001cf

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mGenreView:Landroid/widget/TextView;

    const v0, 0x7f1001cd

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mReleaseAndRuntimeInfo:Landroid/widget/TextView;

    const v0, 0x7f1001ce

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mExpiryTime:Landroid/widget/TextView;

    return-object v6
.end method

.method public setGenre(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mGenreView:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->showTextIfNonEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setReleaseAndRuntimeInfo(III)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-lez p3, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mExpiryTime:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mExpiryTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d03c6

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p3, v5}, Lcom/google/android/apps/sidekick/TimeUtilities;->getEtaString(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mReleaseAndRuntimeInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mReleaseAndRuntimeInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d03c7

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMovieCard;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
