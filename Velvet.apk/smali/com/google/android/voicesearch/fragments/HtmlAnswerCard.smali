.class public Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "HtmlAnswerCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/HtmlAnswerController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/HtmlAnswerController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/HtmlAnswerController$Ui;"
    }
.end annotation


# instance fields
.field private mHtmlView:Landroid/webkit/WebView;

.field private final mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$1;-><init>(Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;)Landroid/webkit/WebView;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    return-object v0
.end method


# virtual methods
.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public getPendingLayoutView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    return-object v0
.end method

.method public handleAttach()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleAttach()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/VelvetApplication;->addActivityLifecycleObserver(Lcom/google/android/velvet/ActivityLifecycleObserver;)V

    return-void
.end method

.method public handleDetach()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/VelvetApplication;->removeActivityLifecycleObserver(Lcom/google/android/velvet/ActivityLifecycleObserver;)V

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleDetach()V

    return-void
.end method

.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const/4 v5, 0x1

    const v4, 0x7f040059

    invoke-virtual {p2, v4, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f100132

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/webkit/WebView;

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    const-string v4, "webview_geolocation"

    invoke-virtual {p1, v4, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/webkit/WebSettings;->setGeolocationDatabasePath(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    new-instance v5, Lcom/google/android/velvet/webview/GsaWebChromeClient;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/velvet/webview/GsaWebChromeClient;-><init>(Landroid/webkit/WebView;Lcom/google/android/searchcommon/google/LocationSettings;)V

    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    new-instance v5, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$2;

    invoke-direct {v5, p0}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard$2;-><init>(Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;)V

    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    return-object v3
.end method

.method public setHtml(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;->mHtmlView:Landroid/webkit/WebView;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
