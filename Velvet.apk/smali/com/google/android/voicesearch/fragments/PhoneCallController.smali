.class public Lcom/google/android/voicesearch/fragments/PhoneCallController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "PhoneCallController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;,
        Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;,
        Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

.field private final mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

.field private mEmbeddedAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

.field private mSelectedContact:Lcom/google/android/speech/contacts/Contact;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/contacts/ContactSelectController;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/voicesearch/contacts/ContactSelectController;
    .param p4    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/PhoneCallController;)Lcom/google/android/speech/contacts/Contact;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/PhoneCallController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/voicesearch/fragments/PhoneCallController;Lcom/google/android/speech/contacts/Contact;)Lcom/google/android/speech/contacts/Contact;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/PhoneCallController;
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    return-object p1
.end method

.method public static createData(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;
    .locals 2
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasActionV2Extension()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getActionV2Extension()Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasPhoneActionExtension()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getActionV2Extension()Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getPhoneActionExtension()Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v0

    :goto_0
    new-instance v1, Lcom/google/android/voicesearch/fragments/PhoneCallController$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController$1;-><init>(Lcom/google/majel/proto/ActionV2Protos$ActionV2;Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private internalStart()V
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mEmbeddedAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mEmbeddedAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->mergePhoneAction(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->isSpokenPhoneNumber(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getSpokenNumber(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/speech/contacts/Contact;->newPhoneNumberOnlyContact(Ljava/lang/String;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->showCard()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactCount()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->requireShowCard()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->isNumberOnlyContact()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->CALL_NUMBER:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    :goto_1
    const/4 v3, 0x0

    new-instance v5, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;

    invoke-direct {v5, p0, v4}, Lcom/google/android/voicesearch/fragments/PhoneCallController$ContactSelectedCallbackImpl;-><init>(Lcom/google/android/voicesearch/fragments/PhoneCallController;Lcom/google/android/voicesearch/fragments/PhoneCallController$1;)V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->pickContact(Ljava/util/List;Lcom/google/android/voicesearch/contacts/ContactSelectMode;ZLjava/lang/String;Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->CALL_CONTACT:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->showCard()V

    goto :goto_0
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public destroyUi()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->destroyUi()V

    return-void
.end method

.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->isNumberOnlyContact()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getActionTypeLog(Z)I

    move-result v0

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->isNumberOnlyContact()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getScreenTypeLog(Z)I

    move-result v0

    return v0
.end method

.method public initUi()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;->setToContact(Lcom/google/android/speech/contacts/Contact;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;->showContactNotFound()V

    goto :goto_0
.end method

.method protected internalBailOut()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getShowContactIntent(Lcom/google/android/speech/contacts/Contact;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getCallIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->startActivity(Landroid/content/Intent;)Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNumberOnlyContact()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    return-void
.end method

.method public restoreStart()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->internalStart()V

    return-void
.end method

.method public start(Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;->getAction()Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;->getEmbeddedAction()Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mEmbeddedAction:Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->internalStart()V

    return-void
.end method
