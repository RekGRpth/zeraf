.class public Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "MultipleLocalResultsCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;"
    }
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private final mLocalItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mMapClickCatcher:Landroid/view/View;

.field private mMapImage:Lcom/google/android/velvet/ui/WebImageView;

.field private mMarkerIndex:I

.field private mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

.field private mViewContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMarkerIndex:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mLocalItems:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;)Lcom/google/android/voicesearch/ui/TravelModeSpinner;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    return-object v0
.end method


# virtual methods
.method public addLocalResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0037

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iget v5, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMarkerIndex:I

    array-length v6, v2

    if-lt v5, v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f04007a

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mViewContainer:Landroid/view/ViewGroup;

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f100188

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f100189

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f10018a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    const v5, 0x7f100186

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v5, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMarkerIndex:I

    aget-object v5, v2, v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, p5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v5, 0x7f10018b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    const v5, 0x7f100187

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mLocalItems:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v5, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMarkerIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMarkerIndex:I

    goto :goto_0

    :cond_2
    invoke-virtual {v3, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public addLocalResultDivider()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f040079

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mViewContainer:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mLocalItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public handleDetach()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleDetach()V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mLocalItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mLocalItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMarkerIndex:I

    return-void
.end method

.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f040078

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100182

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMapImage:Lcom/google/android/velvet/ui/WebImageView;

    const v1, 0x7f100183

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMapClickCatcher:Landroid/view/View;

    const v1, 0x7f100181

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mViewContainer:Landroid/view/ViewGroup;

    const v1, 0x7f100184

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    new-instance v2, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard$1;-><init>(Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;)V

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public setActionTypeAndTransportationMethod(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setActionType(II)V

    return-void
.end method

.method public setMapImageBitmap(Landroid/graphics/Bitmap;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMapImage:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMapClickCatcher:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setMapImageUrl(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMapImage:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;->mMapClickCatcher:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
