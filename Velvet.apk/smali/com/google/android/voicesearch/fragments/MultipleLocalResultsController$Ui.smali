.class public interface abstract Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController$Ui;
.super Ljava/lang/Object;
.source "MultipleLocalResultsController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract addLocalResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
.end method

.method public abstract addLocalResultDivider()V
.end method

.method public abstract setActionTypeAndTransportationMethod(II)V
.end method

.method public abstract setMapImageBitmap(Landroid/graphics/Bitmap;Landroid/view/View$OnClickListener;)V
.end method

.method public abstract setMapImageUrl(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
.end method
