.class public interface abstract Lcom/google/android/voicesearch/fragments/HelpController$Ui;
.super Ljava/lang/Object;
.source "HelpController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/HelpController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setExampleQuery(Ljava/lang/String;)V
.end method

.method public abstract setHeadline(Ljava/lang/String;)V
.end method

.method public abstract setPreviewContact(Lcom/google/android/voicesearch/fragments/HelpController$Contact;)V
.end method

.method public abstract setPreviewDate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setPreviewTime(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setPreviewUrl(Ljava/lang/String;)V
.end method
