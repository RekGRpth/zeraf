.class public Lcom/google/android/voicesearch/fragments/WeatherResultController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "WeatherResultController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/WeatherResultController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/WeatherResultController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mData:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method


# virtual methods
.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/WeatherResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/WeatherResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->toByteArray()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x17

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x17

    return v0
.end method

.method public initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/WeatherResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/WeatherResultController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/WeatherResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/WeatherResultController$Ui;->setData(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)V

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;->parseFrom([B)Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/WeatherResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    :cond_0
    return-void
.end method

.method public start(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/WeatherResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/WeatherResultController;->showCardAndPlayTts()V

    return-void
.end method
