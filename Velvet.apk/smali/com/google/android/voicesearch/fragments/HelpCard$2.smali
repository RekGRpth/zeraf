.class Lcom/google/android/voicesearch/fragments/HelpCard$2;
.super Landroid/os/AsyncTask;
.source "HelpCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/HelpCard;->setPreviewContact(Lcom/google/android/voicesearch/fragments/HelpController$Contact;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/HelpCard;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/HelpCard;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Long;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # [Ljava/lang/Long;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/HelpCard;->access$000(Lcom/google/android/voicesearch/fragments/HelpCard;)Lcom/google/android/velvet/ui/WebImageView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/WebImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/speech/contacts/ContactLookup;->fetchPhotoBitmap(Landroid/content/ContentResolver;JZ)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpCard;->mPlaceholderAvatar:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/HelpCard;->access$100(Lcom/google/android/voicesearch/fragments/HelpCard;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/HelpCard;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    # setter for: Lcom/google/android/voicesearch/fragments/HelpCard;->mPlaceholderAvatar:Landroid/graphics/drawable/Drawable;
    invoke-static {v1, v2}, Lcom/google/android/voicesearch/fragments/HelpCard;->access$102(Lcom/google/android/voicesearch/fragments/HelpCard;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/HelpCard$2;->doInBackground([Ljava/lang/Long;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    # invokes: Lcom/google/android/voicesearch/fragments/HelpCard;->pushImagePreviewFrontToBack()V
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/HelpCard;->access$200(Lcom/google/android/voicesearch/fragments/HelpCard;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/HelpCard;->access$000(Lcom/google/android/voicesearch/fragments/HelpCard;)Lcom/google/android/velvet/ui/WebImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpCard;->mImagePreviewFront:Lcom/google/android/velvet/ui/WebImageView;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/HelpCard;->access$000(Lcom/google/android/voicesearch/fragments/HelpCard;)Lcom/google/android/velvet/ui/WebImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/HelpCard$2;->this$0:Lcom/google/android/voicesearch/fragments/HelpCard;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpCard;->mPlaceholderAvatar:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/HelpCard;->access$100(Lcom/google/android/voicesearch/fragments/HelpCard;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/HelpCard$2;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
