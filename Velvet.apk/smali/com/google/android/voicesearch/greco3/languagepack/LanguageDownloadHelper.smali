.class public Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;
.super Ljava/lang/Object;
.source "LanguageDownloadHelper.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;,
        Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;
    }
.end annotation


# instance fields
.field private mActiveDownloads:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDownloadManager:Landroid/app/DownloadManager;

.field private final mListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreferenceStore:Lcom/google/android/speech/embedded/Greco3Preferences;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method private constructor <init>(Landroid/app/DownloadManager;Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3Preferences;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 1
    .param p1    # Landroid/app/DownloadManager;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Preferences;
    .param p4    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mPreferenceStore:Lcom/google/android/speech/embedded/Greco3Preferences;

    iput-object p4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mPreferenceStore:Lcom/google/android/speech/embedded/Greco3Preferences;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3Preferences;->getActiveDownloads()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    return-void
.end method

.method private buildDownloadRequest(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Landroid/app/DownloadManager$Request;
    .locals 11
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const/4 v10, 0x1

    const/4 v9, 0x0

    :try_start_0
    new-instance v3, Landroid/app/DownloadManager$Request;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getDownloadUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v3, v9}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    invoke-virtual {v3, v9}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f0d0434

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f0d0435

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v7}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    :try_start_1
    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mContext:Landroid/content/Context;

    const-string v5, "download_cache"

    invoke-static {p1}, Lcom/google/android/speech/embedded/LanguagePackUtils;->buildDownloadFilename(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/DownloadManager$Request;->setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_0
    invoke-virtual {v3, v10}, Landroid/app/DownloadManager$Request;->setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;

    :goto_1
    return-object v3

    :catch_0
    move-exception v0

    const-string v4, "VS.VoiceDownloadHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid download URI in metadata: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getDownloadUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto :goto_1

    :catch_1
    move-exception v2

    const-string v4, "VS.VoiceDownloadHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error from #setDestinationInExternalFilesDir :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v4, "VS.VoiceDownloadHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error from #setDestinationInExternalFilesDir :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static create(Landroid/app/DownloadManager;Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3Preferences;Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;
    .locals 1
    .param p0    # Landroid/app/DownloadManager;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Preferences;
    .param p3    # Lcom/google/android/voicesearch/settings/Settings;

    new-instance v0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;-><init>(Landroid/app/DownloadManager;Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3Preferences;Lcom/google/android/voicesearch/settings/Settings;)V

    invoke-virtual {p2, v0}, Lcom/google/android/speech/embedded/Greco3Preferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-object v0
.end method

.method private dispatchChange()V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;

    invoke-interface {v2, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;->onDownloadSetChanged(Ljava/util/Map;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_0
    return-void
.end method

.method private getLanguagePackForActiveDownloadLocked(J)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;
    .locals 6
    .param p1    # J

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v2, v1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognitionResourcesList()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/speech/embedded/LanguagePackUtils;->findById(Ljava/lang/String;Ljava/util/List;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_2
    const-string v3, "VS.VoiceDownloadHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Download ID not found in active set :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized cancelDownload(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 8
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const/4 v7, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v1

    const/4 v3, 0x0

    :try_start_2
    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    const/4 v5, 0x1

    new-array v5, v5, [J

    const/4 v6, 0x0

    aput-wide v1, v5, v6

    invoke-virtual {v4, v5}, Landroid/app/DownloadManager;->remove([J)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    :goto_1
    if-eq v3, v7, :cond_1

    :try_start_3
    const-string v4, "VS.VoiceDownloadHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(DownloadManager) Unexpected number of removals: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mPreferenceStore:Lcom/google/android/speech/embedded/Greco3Preferences;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/speech/embedded/Greco3Preferences;->removeActiveDownload(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :catch_0
    move-exception v0

    :try_start_4
    const-string v4, "VS.VoiceDownloadHelper"

    const-string v5, "Exception from DownloadManager "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized enqueueDownload(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)I
    .locals 7
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getCompatibleEngineVersionsList()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    monitor-exit p0

    return v4

    :cond_0
    const-wide/16 v0, 0x0

    const/4 v3, 0x0

    :try_start_1
    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->buildDownloadRequest(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Landroid/app/DownloadManager$Request;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_1

    :try_start_2
    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mPreferenceStore:Lcom/google/android/speech/embedded/Greco3Preferences;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/speech/embedded/Greco3Preferences;->addActiveDownload(Ljava/lang/String;J)V

    const/4 v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v4, "VS.VoiceDownloadHelper"

    const-string v5, "Exception from DownloadManager"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_1
    const/4 v4, -0x1

    goto :goto_0
.end method

.method public getDownloadInfo(J)Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;
    .locals 8
    .param p1    # J

    const/4 v3, 0x1

    new-instance v7, Landroid/app/DownloadManager$Query;

    invoke-direct {v7}, Landroid/app/DownloadManager$Query;-><init>()V

    new-array v1, v3, [J

    const/4 v2, 0x0

    aput-wide p1, v1, v2

    invoke-virtual {v7, v1}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    const/4 v6, 0x0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v1, v7}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eq v1, v3, :cond_1

    const-string v1, "VS.VoiceDownloadHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Querying download manager failed for ID :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;

    const-string v1, "local_filename"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->getLanguagePackForActiveDownloadLocked(J)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    move-result-object v2

    const-string v3, "status"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;-><init>(Ljava/lang/String;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;IJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public declared-synchronized isActiveDownload(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Z
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized markCompleted(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mActiveDownloads:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mPreferenceStore:Lcom/google/android/speech/embedded/Greco3Preferences;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getLanguagePackId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/speech/embedded/Greco3Preferences;->removeActiveDownload(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const-string v0, "g3_active_downloads"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->dispatchChange()V

    :cond_0
    return-void
.end method

.method public registerDownloadSetChangeListener(Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public unregisterDownloadSetChangeListener(Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->mListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
