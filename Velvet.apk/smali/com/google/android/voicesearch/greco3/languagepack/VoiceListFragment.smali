.class public Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;
.super Landroid/app/ListFragment;
.source "VoiceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;
    }
.end annotation


# instance fields
.field private final mDataManagerInitializationCallback:Lcom/google/android/speech/callback/SimpleCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceClass:I

.field private mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

.field private final mDownloadSetListener:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;

.field private mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field private mLanguageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;",
            ">;"
        }
    .end annotation
.end field

.field private mListAdapter:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;

.field private mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private mType:I

.field private mUiThread:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$1;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadSetListener:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;

    new-instance v0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$2;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDataManagerInitializationCallback:Lcom/google/android/speech/callback/SimpleCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    invoke-direct {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->refreshLanguagePackList()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mUiThread:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Lcom/google/android/voicesearch/settings/Settings;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->doDownload(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->doDelete(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mLanguageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->populateView(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    return-void
.end method

.method private doDelete(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 3
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$4;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$4;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/speech/embedded/Greco3DataManager;->deleteLanguage(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Ljava/util/concurrent/Executor;Ljava/lang/Runnable;)V

    return-void
.end method

.method private doDownload(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 4
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->enqueueDownload(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0d037d

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method private initViewForDelete(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    new-instance v0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$6;

    invoke-direct {v0, p0, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$6;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private initViewForDownload(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    new-instance v0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$5;

    invoke-direct {v0, p0, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$5;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private initViewForUpdateAndDelete(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;II)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;
    .param p3    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-virtual {p0, p4}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0, p5}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;

    invoke-direct {v1, p0, p2, v0, p3}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;[Ljava/lang/CharSequence;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private isInstalledInSystemPartition(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInstalledInSystemPartition(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isUsingDownloadedData(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->isUsingDownloadedData(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private populateView(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const v1, 0x7f10014e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->populateViewForInstalledPack(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getInstalledLanguages()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->populateViewForInstalledPack(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->populateViewForInstallablePack(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    goto :goto_0
.end method

.method private populateViewForDownloadingLanguagePack(Landroid/view/View;Landroid/widget/TextView;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/widget/TextView;
    .param p3    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const v0, 0x7f0d046e

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    new-instance v0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$7;

    invoke-direct {v0, p0, p3}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$7;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private populateViewForInstallablePack(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const v3, 0x7f10014f

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f100150

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    invoke-virtual {v3, p2}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->isActiveDownload(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->populateViewForDownloadingLanguagePack(Landroid/view/View;Landroid/widget/TextView;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getSizeKb()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    int-to-long v4, v4

    invoke-static {v3, v4, v5}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0d046d

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->initViewForDownload(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    goto :goto_0
.end method

.method private populateViewForInstalledPack(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getInstallableLanguages()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    iget v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDeviceClass:I

    invoke-static {p2, v0, v1, v3}, Lcom/google/android/speech/embedded/LanguagePackUtils;->isUpgradeable(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Ljava/util/List;II)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->isInstalledInSystemPartition(Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->isUsingDownloadedData(Ljava/lang/String;)Z

    move-result v10

    const v0, 0x7f10014f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v0, 0x7f100150

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getSizeKb()I

    move-result v1

    mul-int/lit16 v1, v1, 0x400

    int-to-long v3, v1

    invoke-static {v0, v3, v4}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    if-eqz v9, :cond_3

    if-eqz v10, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->initViewForDelete(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    if-eqz v2, :cond_0

    const v0, 0x7f0d0468

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0d0466

    const v5, 0x7f0d0467

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->initViewForUpdateAndDelete(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;II)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0d046a

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->initViewForDelete(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    const-string v0, ""

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    :cond_2
    const v0, 0x7f0d046b

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0, p1, v2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->initViewForDownload(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_4

    const v0, 0x7f0d0469

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0d0466

    const v5, 0x7f0d0467

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->initViewForUpdateAndDelete(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;II)V

    goto :goto_0

    :cond_4
    const v0, 0x7f0d046c

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->initViewForDelete(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    goto :goto_0
.end method

.method private refreshLanguagePackList()V
    .locals 5

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getInstalledLanguages()Ljava/util/Map;

    move-result-object v0

    iget v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mType:I

    if-ne v2, v4, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mLanguageList:Ljava/util/ArrayList;

    :goto_0
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mListAdapter:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;->notifyDataSetChanged()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getInstallableLanguages()Ljava/util/List;

    move-result-object v2

    iget v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDeviceClass:I

    invoke-static {v2, v4, v3, v0}, Lcom/google/android/speech/embedded/LanguagePackUtils;->getInstallableLanguagePacks(Ljava/util/List;IILjava/util/Map;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mLanguageList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mLanguageList:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method


# virtual methods
.method getInstallableLanguages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognitionResourcesList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getInstalledLanguages()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->getInstalledLanguages()Ljava/util/HashMap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mListAdapter:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mType:I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mUiThread:Ljava/util/concurrent/Executor;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getLangugageDownloadHelper()Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDataManagerInitializationCallback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-virtual {v2, v3}, Lcom/google/android/speech/embedded/Greco3DataManager;->maybeInitialize(Lcom/google/android/speech/callback/SimpleCallback;)Z

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3Container;->getDeviceClassSupplier()Lcom/google/common/base/Supplier;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDeviceClass:I

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    new-instance v2, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mListAdapter:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mLanguageList:Ljava/util/ArrayList;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v2, 0x7f0400d3

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const v2, 0x7f0d0438

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-object v1

    :cond_0
    const v2, 0x7f0d0437

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadSetListener:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->unregisterDownloadSetChangeListener(Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;)V

    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->refreshLanguagePackList()V

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadSetListener:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->registerDownloadSetChangeListener(Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$Listener;)V

    return-void
.end method
