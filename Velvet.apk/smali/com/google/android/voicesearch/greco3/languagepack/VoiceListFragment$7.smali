.class Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$7;
.super Ljava/lang/Object;
.source "VoiceListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->populateViewForDownloadingLanguagePack(Landroid/view/View;Landroid/widget/TextView;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

.field final synthetic val$languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$7;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    iput-object p2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$7;->val$languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$7;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    # getter for: Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;
    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->access$500(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$7;->val$languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->cancelDownload(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    return-void
.end method
