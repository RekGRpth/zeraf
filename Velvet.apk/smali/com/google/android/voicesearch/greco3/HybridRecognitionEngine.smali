.class public Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;
.super Ljava/lang/Object;
.source "HybridRecognitionEngine.java"

# interfaces
.implements Lcom/google/android/speech/engine/RecognitionEngine;


# static fields
.field private static final EMPTY_CALLBACK:Lcom/google/android/speech/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mLocalExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mLocalRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

.field private final mMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

.field private final mMusicExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mNetworkExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mNetworkRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

.field private final mOriginalMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

.field private final mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

.field private final mTimeoutExecutor:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->EMPTY_CALLBACK:Lcom/google/android/speech/callback/Callback;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/speech/engine/RecognitionEngine;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/engine/RecognitionEngine;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/engine/RecognitionEngine;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/utils/NetworkInformation;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/speech/SpeechSettings;Lcom/google/common/base/Supplier;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/engine/RecognitionEngine;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/speech/engine/RecognitionEngine;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/speech/engine/RecognitionEngine;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/speech/utils/NetworkInformation;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/google/android/speech/SpeechSettings;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p4, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mNetworkExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mLocalExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p6, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mMusicExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mLocalExecutor:Ljava/util/concurrent/ExecutorService;

    const-class v1, Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-static {v0, v1, p1}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/engine/RecognitionEngine;

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mLocalRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mNetworkExecutor:Ljava/util/concurrent/ExecutorService;

    const-class v1, Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-static {v0, v1, p3}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/engine/RecognitionEngine;

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mNetworkRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    iput-object p5, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mOriginalMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mMusicExecutor:Ljava/util/concurrent/ExecutorService;

    const-class v1, Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-static {v0, v1, p5}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/engine/RecognitionEngine;

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    iput-object p7, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    iput-object p8, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mTimeoutExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p9, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    iput-object p10, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;

    invoke-direct {p0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->shouldStopMusicDetectorOnStartOfSpeech()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;)Lcom/google/android/speech/engine/RecognitionEngine;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mOriginalMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    return-object v0
.end method

.method private static callbackWithSource(ILcom/google/android/voicesearch/greco3/ResultsMerger;)Lcom/google/android/speech/callback/Callback;
    .locals 1
    .param p0    # I
    .param p1    # Lcom/google/android/voicesearch/greco3/ResultsMerger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/voicesearch/greco3/ResultsMerger;",
            ")",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;

    invoke-direct {v0, p1, p0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;-><init>(Lcom/google/android/voicesearch/greco3/ResultsMerger;I)V

    return-object v0
.end method

.method private isMusicDetectorEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;->getEnableMusicDetector()Z

    move-result v0

    goto :goto_0
.end method

.method private isMusicDetectorEnabledForHotword()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;->getEnableMusicHotworder()Z

    move-result v0

    goto :goto_0
.end method

.method private shouldStopMusicDetectorOnStartOfSpeech()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;->hasStopMusicDetectionOnStartOfSpeech()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;->getStopMusicDetectionOnStartOfSpeech()Z

    move-result v0

    goto :goto_0
.end method

.method private startNetworkRecognizerOnly(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .param p3    # Lcom/google/android/speech/params/RecognizerParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/audio/AudioInputStreamFactory;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/params/RecognizerParams;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mNetworkRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/google/android/speech/engine/RecognitionEngine;->startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    return-void
.end method

.method private startRecognitionForHotwordInternal(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .param p3    # Lcom/google/android/speech/params/RecognizerParams;
    .param p4    # Lcom/google/android/speech/audio/EndpointerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/audio/AudioInputStreamFactory;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/params/RecognizerParams;",
            "Lcom/google/android/speech/audio/EndpointerListener;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mLocalRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/speech/engine/RecognitionEngine;->startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->isMusicDetectorEnabledForHotword()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/speech/engine/RecognitionEngine;->startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    :cond_0
    return-void
.end method

.method private startRecognitionInternal(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V
    .locals 11
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .param p3    # Lcom/google/android/speech/params/RecognizerParams;
    .param p4    # Lcom/google/android/speech/audio/EndpointerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/audio/AudioInputStreamFactory;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/params/RecognizerParams;",
            "Lcom/google/android/speech/audio/EndpointerListener;",
            ")V"
        }
    .end annotation

    sget-object v7, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->EMPTY_CALLBACK:Lcom/google/android/speech/callback/Callback;

    move-object v8, p2

    move-object v10, p4

    const/4 v9, 0x1

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getGreco3Mode()Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v6

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    if-eq v6, v1, :cond_0

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne v6, v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v1}, Lcom/google/android/speech/utils/NetworkInformation;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getResultsMergerStrategy()Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    move-result-object v1

    sget-object v2, Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;->EMBEDDED_ONLY:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    if-ne v1, v2, :cond_5

    :cond_1
    const/16 v1, 0x46

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    const/4 v9, 0x0

    move-object v7, p2

    const/4 v1, 0x0

    invoke-direct {p0, p4, v1}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->wrapWithMergerCallbacks(Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/android/voicesearch/greco3/ResultsMerger;)Lcom/google/android/speech/audio/EndpointerListener;

    move-result-object v10

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mLocalRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v1, p1, v7, p3, v10}, Lcom/google/android/speech/engine/RecognitionEngine;->startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    if-eqz v9, :cond_3

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mNetworkRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v8, p3, v2}, Lcom/google/android/speech/engine/RecognitionEngine;->startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->isMusicDetectorEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v1

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v1, p1, v8, p3, v10}, Lcom/google/android/speech/engine/RecognitionEngine;->startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    :cond_4
    return-void

    :cond_5
    new-instance v0, Lcom/google/android/voicesearch/greco3/ResultsMerger;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mTimeoutExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getEmbeddedRecognizerFallbackTimeout()J

    move-result-wide v3

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getResultsMergerStrategy()Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    move-result-object v5

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/greco3/ResultsMerger;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/speech/callback/Callback;JLcom/google/android/voicesearch/greco3/ResultsMergerStrategy;)V

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->callbackWithSource(ILcom/google/android/voicesearch/greco3/ResultsMerger;)Lcom/google/android/speech/callback/Callback;

    move-result-object v7

    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->callbackWithSource(ILcom/google/android/voicesearch/greco3/ResultsMerger;)Lcom/google/android/speech/callback/Callback;

    move-result-object v8

    invoke-direct {p0, p4, v0}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->wrapWithMergerCallbacks(Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/android/voicesearch/greco3/ResultsMerger;)Lcom/google/android/speech/audio/EndpointerListener;

    move-result-object v10

    goto :goto_0
.end method

.method private wrapWithMergerCallbacks(Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/android/voicesearch/greco3/ResultsMerger;)Lcom/google/android/speech/audio/EndpointerListener;
    .locals 1
    .param p1    # Lcom/google/android/speech/audio/EndpointerListener;
    .param p2    # Lcom/google/android/voicesearch/greco3/ResultsMerger;

    new-instance v0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$3;-><init>(Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/android/voicesearch/greco3/ResultsMerger;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mLocalRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v0}, Lcom/google/android/speech/engine/RecognitionEngine;->close()V

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mNetworkRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v0}, Lcom/google/android/speech/engine/RecognitionEngine;->close()V

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->mMusicDetectorRecognitionEngine:Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v0}, Lcom/google/android/speech/engine/RecognitionEngine;->close()V

    return-void
.end method

.method public startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .param p3    # Lcom/google/android/speech/params/RecognizerParams;
    .param p4    # Lcom/google/android/speech/audio/EndpointerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/audio/AudioInputStreamFactory;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/params/RecognizerParams;",
            "Lcom/google/android/speech/audio/EndpointerListener;",
            ")V"
        }
    .end annotation

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->HOTWORD:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->startRecognitionForHotwordInternal(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->isRecordedAudio()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->startNetworkRecognizerOnly(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->startRecognitionInternal(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    goto :goto_0
.end method
