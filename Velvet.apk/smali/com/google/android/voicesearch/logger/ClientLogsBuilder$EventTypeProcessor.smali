.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventTypeProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventTypeProcessor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 0
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventTypeProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    return-void
.end method


# virtual methods
.method public internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 5
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v3

    const/high16 v4, 0x10000000

    if-ne v3, v4, :cond_2

    invoke-virtual {p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventTypeProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v3, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    const/4 v3, 0x1

    :goto_1
    return v3

    :sswitch_0
    new-instance v4, Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    invoke-direct {v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;-><init>()V

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_2
    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;->setBugNumber(I)Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setBugReport(Lcom/google/speech/logs/VoicesearchClientLogProto$BugReport;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :cond_1
    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_2

    :sswitch_1
    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;

    invoke-virtual {v1}, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;->getProto()Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setAlternateCorrection(Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-virtual {v1}, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;->getRequestId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :sswitch_2
    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v3, "com.google.android.speech.REQUEST_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const-string v3, "com.google.android.speech.SEGMENT_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const-string v3, "com.google.android.speech.REQUEST_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    new-instance v2, Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    invoke-direct {v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;-><init>()V

    const-string v3, "com.google.android.speech.SEGMENT_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;->setRecognizerSegmentIndex(I)Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;

    invoke-virtual {p2, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setTypingCorrection(Lcom/google/speech/logs/VoicesearchClientLogProto$TypingCorrection;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0xf -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_0
        0x22 -> :sswitch_2
    .end sparse-switch
.end method
