.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestDataProcessor"
.end annotation


# instance fields
.field private final mAddRequestIds:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mCacheEventRequestDatas:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mOverrideEventRequestIds:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 5
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    const/16 v3, 0x28

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mOverrideEventRequestIds:Ljava/util/HashMap;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mCacheEventRequestDatas:Ljava/util/HashMap;

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2, v3}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mAddRequestIds:Landroid/util/SparseArray;

    const/16 v2, 0x5d

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x5e

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x4c

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x4a

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x26

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x29

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x2a

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x24

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x27

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x4b

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x23

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x60

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x61

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x62

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x46

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x47

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x5a

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x6c

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x55

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x56

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x67

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x68

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x33

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0xa

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x9

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/4 v2, 0x7

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0xc

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x1b

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x1c

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x20

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x1f

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x21

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0xb

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x1a

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x19

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x17

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x16

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x18

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x1e

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x32

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0xe

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x48

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0xd

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x12

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x2c

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x2d

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x25

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x3f

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x11

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    const/16 v2, 0x69

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->addClientEvent(I)V

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mOverrideEventRequestIds:Ljava/util/HashMap;

    const v3, 0x10000034

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x10000033

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mOverrideEventRequestIds:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mCacheEventRequestDatas:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->EMPTY_REQUEST_DATA:[Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1100()[Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
    .param p3    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    return-void
.end method

.method private addClientEvent(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mAddRequestIds:Landroid/util/SparseArray;

    const/high16 v1, 0x10000000

    or-int/2addr v1, p1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 7
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x0

    const/high16 v4, 0x20000000

    const/4 v2, 0x1

    invoke-virtual {p1, v4, v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;
    invoke-static {v3, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1202(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;)Ljava/lang/String;

    move v1, v2

    :goto_0
    return v1

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p1, v4, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # setter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I
    invoke-static {v1, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1302(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;I)I

    move v1, v2

    goto :goto_0

    :cond_1
    const/16 v1, 0x9

    invoke-virtual {p1, v4, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    const/4 v3, 0x6

    # setter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I
    invoke-static {v1, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1302(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;I)I

    move v1, v2

    goto :goto_0

    :cond_2
    const/16 v1, 0xa

    invoke-virtual {p1, v4, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    const/4 v3, 0x5

    # setter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I
    invoke-static {v1, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1302(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;I)I

    move v1, v2

    goto :goto_0

    :cond_3
    const/16 v1, 0xb

    invoke-virtual {p1, v4, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # setter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I
    invoke-static {v1, v6}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1302(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;I)I

    move v1, v2

    goto :goto_0

    :cond_4
    const/16 v1, 0xc

    invoke-virtual {p1, v4, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # setter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I
    invoke-static {v1, v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1302(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;I)I

    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1200(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mAddRequestIds:Landroid/util/SparseArray;

    invoke-virtual {p1, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->in(Landroid/util/SparseArray;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1200(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1300(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_6
    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1200(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mCacheEventRequestDatas:Ljava/util/HashMap;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v4

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v5

    or-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mCacheEventRequestDatas:Ljava/util/HashMap;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v4

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v5

    or-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1200(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    iget-object v6, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I
    invoke-static {v6}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1300(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mOverrideEventRequestIds:Ljava/util/HashMap;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v4

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v5

    or-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mCacheEventRequestDatas:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;->mOverrideEventRequestIds:Ljava/util/HashMap;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v5

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v6

    or-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->EMPTY_REQUEST_DATA:[Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1100()[Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_8

    aget-object v1, v0, v3

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p2, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    aget-object v1, v0, v2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_8
    move v1, v3

    goto/16 :goto_0
.end method
