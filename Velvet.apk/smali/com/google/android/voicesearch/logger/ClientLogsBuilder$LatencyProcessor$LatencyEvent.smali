.class public Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;
.super Ljava/lang/Object;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LatencyEvent"
.end annotation


# instance fields
.field final mBreakdownEvents:[I

.field final mEventType:I

.field final mFromEvent:I

.field final mToEvent:I

.field final synthetic this$1:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;II)V
    .locals 0
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3, p3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III)V
    .locals 6
    .param p2    # I
    .param p3    # I
    .param p4    # I

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->EMPTY_BREAKDOWN_EVENTS:[I
    invoke-static {}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1600()[I

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III[I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III[I)V
    .locals 4
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # [I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->this$1:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/voicesearch/logger/EventUtils;->getGroup(I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-static {p3}, Lcom/google/android/voicesearch/logger/EventUtils;->getGroup(I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-static {p4}, Lcom/google/android/voicesearch/logger/EventUtils;->getGroup(I)I

    move-result v0

    const/high16 v3, 0x10000000

    if-ne v0, v3, :cond_2

    :goto_2
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput p2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mFromEvent:I

    iput p3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mToEvent:I

    iput p4, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mEventType:I

    invoke-static {p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mBreakdownEvents:[I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method static synthetic access$1400(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;I)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->hasBreakDownEvent(I)Z

    move-result v0

    return v0
.end method

.method private hasBreakDownEvent(I)Z
    .locals 7
    .param p1    # I

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/google/android/voicesearch/logger/EventUtils;->getId(I)I

    move-result v4

    if-ne v4, p1, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mBreakdownEvents:[I

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget v1, v0, v2

    if-ne p1, v1, :cond_1

    :goto_2
    return v5

    :cond_0
    move v4, v6

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v5, v6

    goto :goto_2
.end method
