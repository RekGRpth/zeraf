.class public final Lcom/google/android/voicesearch/logger/store/EventLoggerStores;
.super Ljava/lang/Object;
.source "EventLoggerStores.java"


# static fields
.field private static final sMainEventLoggerStore:Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;

    invoke-direct {v0}, Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/logger/store/EventLoggerStores;->sMainEventLoggerStore:Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createEventStore()Lcom/google/android/searchcommon/EventLoggerStore;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/logger/store/EventLoggerStores;->sMainEventLoggerStore:Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;

    return-object v0
.end method

.method public static getMainEventLoggerStore()Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/logger/store/EventLoggerStores;->sMainEventLoggerStore:Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;

    return-object v0
.end method
