.class Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HeadsetAudioRecorderRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScoBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;->this$0:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;
    .param p2    # Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;-><init>(Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;->this$0:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    const-string v1, "android.media.extra.SCO_AUDIO_STATE"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    # invokes: Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->updateScoState(I)V
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->access$100(Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;I)V

    :cond_0
    return-void
.end method
