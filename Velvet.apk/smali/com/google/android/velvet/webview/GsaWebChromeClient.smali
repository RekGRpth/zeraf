.class public Lcom/google/android/velvet/webview/GsaWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "GsaWebChromeClient.java"


# instance fields
.field private final mClientWebView:Landroid/webkit/WebView;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;Lcom/google/android/searchcommon/google/LocationSettings;)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebChromeClient;->mClientWebView:Landroid/webkit/WebView;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/google/LocationSettings;

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebChromeClient;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    return-void
.end method


# virtual methods
.method public onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 1
    .param p1    # Landroid/webkit/ConsoleMessage;

    const/4 v0, 0x1

    return v0
.end method

.method public onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/webkit/GeolocationPermissions$Callback;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebChromeClient;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForSearch()Z

    move-result v0

    const/4 v1, 0x0

    invoke-interface {p2, p1, v0, v1}, Landroid/webkit/GeolocationPermissions$Callback;->invoke(Ljava/lang/String;ZZ)V

    return-void
.end method

.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/webkit/JsResult;

    const/4 v0, 0x0

    return v0
.end method

.method public onJsConfirm(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/webkit/JsResult;

    const/4 v0, 0x0

    return v0
.end method

.method public onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/webkit/JsPromptResult;

    const/4 v0, 0x0

    return v0
.end method
