.class Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$JsBridge;
.super Ljava/lang/Object;
.source "GsaCommunicationJsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "JsBridge"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$JsBridge;->this$0:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;
    .param p2    # Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$JsBridge;-><init>(Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;)V

    return-void
.end method


# virtual methods
.method public onJsEvents(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$JsBridge;->this$0:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    # getter for: Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mJsEventController:Lcom/google/android/velvet/presenter/JsEventController;
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->access$200(Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;)Lcom/google/android/velvet/presenter/JsEventController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$JsBridge;->this$0:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    # invokes: Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->read(Ljava/lang/String;)Ljava/util/Map;
    invoke-static {v1, p1}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->access$100(Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/velvet/presenter/JsEventController;->dispatchEvents(Ljava/util/Map;Ljava/lang/String;)V

    return-void
.end method
