.class Lcom/google/android/velvet/webview/GsaWebViewController$5;
.super Ljava/lang/Object;
.source "GsaWebViewController.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/webview/GsaWebViewController;->initWebView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/webview/GsaWebViewController;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$5;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleEvents(Ljava/util/Map;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$5;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$700(Lcom/google/android/velvet/webview/GsaWebViewController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$5;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->handleAgsaEvents(Ljava/util/Map;Ljava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$800(Lcom/google/android/velvet/webview/GsaWebViewController;Ljava/util/Map;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
