.class Lcom/google/android/velvet/VelvetFactory$1;
.super Ljava/lang/Object;
.source "VelvetFactory.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/VelvetFactory;->createBackgroundTask(Ljava/lang/String;Z)Ljava/util/concurrent/Callable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/VelvetFactory;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/VelvetFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/VelvetFactory$1;->this$0:Lcom/google/android/velvet/VelvetFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetFactory$1;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 3

    const-string v0, "Velvet.VelvetFactory"

    const-string v1, "refreshing search history."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory$1;->this$0:Lcom/google/android/velvet/VelvetFactory;

    # getter for: Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;
    invoke-static {v0}, Lcom/google/android/velvet/VelvetFactory;->access$000(Lcom/google/android/velvet/VelvetFactory;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    new-instance v2, Lcom/google/android/searchcommon/util/NoOpConsumer;

    invoke-direct {v2}, Lcom/google/android/searchcommon/util/NoOpConsumer;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/google/WebSuggestSource;->getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    const/4 v0, 0x0

    return-object v0
.end method
