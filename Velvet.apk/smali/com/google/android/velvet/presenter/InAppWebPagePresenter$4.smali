.class Lcom/google/android/velvet/presenter/InAppWebPagePresenter$4;
.super Ljava/lang/Object;
.source "InAppWebPagePresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->initializeLoadingIndicatorTimer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$4;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$4;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$100(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$4;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->showLoadingIndicator()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$4;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mLoadingIndicatorTimer:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$700(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
