.class Lcom/google/android/velvet/presenter/QueryState$LoadState;
.super Ljava/lang/Object;
.source "QueryState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/QueryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LoadState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mCurrentQuery:Lcom/google/android/velvet/Query;

.field private mError:Lcom/google/android/velvet/presenter/SearchError;

.field private mLoadedData:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mNewlyCommitted:Z

.field private mNewlyLoaded:Z

.field protected mState:I

.field private final mType:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mCurrentQuery:Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mType:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mError:Lcom/google/android/velvet/presenter/SearchError;

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/velvet/presenter/QueryState$LoadState;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$LoadState;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public getError()Lcom/google/android/velvet/presenter/SearchError;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mError:Lcom/google/android/velvet/presenter/SearchError;

    return-object v0
.end method

.method getLoadedData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;

    return-object v0
.end method

.method getQuery()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mCurrentQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method hasDataFor(Lcom/google/android/velvet/Query;)Z
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getCommitId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getCommitId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-static {v0, p1}, Lcom/google/android/velvet/Query;->isSameQueryType(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method hasError()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isFinished()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isLoaded()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isLoadingOrLoaded()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method loadComplete(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->setState(I)V

    return-void
.end method

.method loadError(Lcom/google/android/velvet/presenter/SearchError;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/SearchError;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mError:Lcom/google/android/velvet/presenter/SearchError;

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->setState(I)V

    return-void
.end method

.method queryCommitted(Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mCurrentQuery:Lcom/google/android/velvet/Query;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->setState(I)V

    return-void
.end method

.method reset()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->setState(I)V

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mCurrentQuery:Lcom/google/android/velvet/Query;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;

    return-void
.end method

.method protected setState(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    const/4 v3, 0x3

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    if-eq v2, v0, :cond_1

    if-ne p1, v0, :cond_1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyCommitted:Z

    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    if-eq v2, v3, :cond_2

    if-ne p1, v3, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyLoaded:Z

    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mError:Lcom/google/android/velvet/presenter/SearchError;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/SearchError;

    :goto_2
    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mError:Lcom/google/android/velvet/presenter/SearchError;

    iput p1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    return-void

    :cond_1
    if-eq p1, v0, :cond_0

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    if-eq p1, v3, :cond_0

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyCommitted:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method takeNewlyCommitted()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyCommitted:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyCommitted:Z

    return v0
.end method

.method takeNewlyLoaded()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyLoaded:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyLoaded:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mState:I

    # invokes: Lcom/google/android/velvet/presenter/QueryState;->stateString(I)Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/QueryState;->access$700(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NC="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyCommitted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NL="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mNewlyLoaded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " D="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
