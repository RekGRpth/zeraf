.class Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;
.super Ljava/lang/Object;
.source "WebResultHider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "JsBridge"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;->this$1:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;Lcom/google/android/velvet/presenter/WebResultHider$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;
    .param p2    # Lcom/google/android/velvet/presenter/WebResultHider$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;-><init>(Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;)V

    return-void
.end method


# virtual methods
.method public onHideFailed()V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;->this$1:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->this$0:Lcom/google/android/velvet/presenter/WebResultHider;

    # getter for: Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I
    invoke-static {v0}, Lcom/google/android/velvet/presenter/WebResultHider;->access$600(Lcom/google/android/velvet/presenter/WebResultHider;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;->this$1:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->this$0:Lcom/google/android/velvet/presenter/WebResultHider;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;->this$1:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

    iget-object v1, v1, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->this$0:Lcom/google/android/velvet/presenter/WebResultHider;

    # getter for: Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I
    invoke-static {v1}, Lcom/google/android/velvet/presenter/WebResultHider;->access$600(Lcom/google/android/velvet/presenter/WebResultHider;)I

    move-result v1

    # invokes: Lcom/google/android/velvet/presenter/WebResultHider;->onJsCallback(IZI)V
    invoke-static {v0, v1, v2, v2}, Lcom/google/android/velvet/presenter/WebResultHider;->access$500(Lcom/google/android/velvet/presenter/WebResultHider;IZI)V

    :cond_0
    return-void
.end method

.method public onHideResults(ZII)V
    .locals 2
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;->this$1:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->this$0:Lcom/google/android/velvet/presenter/WebResultHider;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/velvet/presenter/WebResultHider;->onJsCallback(IZI)V
    invoke-static {v0, p3, v1, p2}, Lcom/google/android/velvet/presenter/WebResultHider;->access$500(Lcom/google/android/velvet/presenter/WebResultHider;IZI)V

    return-void
.end method

.method public onInit()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    return-void
.end method
