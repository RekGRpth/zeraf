.class public Lcom/google/android/velvet/presenter/VelvetFragments;
.super Ljava/lang/Object;
.source "VelvetFragments.java"


# instance fields
.field private final mFragments:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final mUi:Lcom/google/android/velvet/presenter/VelvetUi;

.field private final mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

.field private final mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/VelvetUi;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetUi;
    .param p2    # Lcom/google/android/velvet/VelvetFactory;
    .param p3    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-void
.end method


# virtual methods
.method createNewFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/VelvetFactory;->createFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method getFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/VelvetFragment;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v1, p1}, Lcom/google/android/velvet/presenter/VelvetUi;->findFragmentByTag(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/presenter/VelvetFragments;->createNewFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public onFragmentCreated(Lcom/google/android/velvet/ui/VelvetFragment;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "All VelvetFragments must have a tag"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duplicate fragment \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'; currently have: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; creating : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onFragmentDestroyed(Lcom/google/android/velvet/ui/VelvetFragment;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "All VelvetFragments must have a tag"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fragment \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'; requested destroy without create"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mFragments:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onFragmentViewCreated(Lcom/google/android/velvet/ui/VelvetFragment;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;",
            ">(",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<TP;>;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/VelvetFactory;->createFragmentPresenter(Lcom/google/android/velvet/ui/VelvetFragment;)Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/ui/VelvetFragment;->setPresenter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onAttach(Lcom/google/android/velvet/presenter/VelvetPresenter;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onFragmentPresenterAttached(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V

    return-void
.end method

.method public onFragmentViewDestroyed(Lcom/google/android/velvet/ui/VelvetFragment;Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;",
            ">(",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<TP;>;TP;)V"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onDetach()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragments;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onFragmentPresenterDetached(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/ui/VelvetFragment;->setPresenter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V

    return-void
.end method
