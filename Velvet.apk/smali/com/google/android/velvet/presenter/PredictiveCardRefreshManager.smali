.class public Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
.super Ljava/lang/Object;
.source "PredictiveCardRefreshManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;,
        Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;,
        Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;,
        Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;
    }
.end annotation


# static fields
.field static final REFRESH_TIMEOUT_MILLIS:J = 0x4e20L


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntryInvalidator:Lcom/google/android/apps/sidekick/inject/EntryInvalidator;

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mErrorReceiver:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;

.field private final mLocalBroadcastManager:Lvedroid/support/v4/content/LocalBroadcastManager;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mObserver:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

.field private mPredictiveCardsListenersRegistered:Z

.field private mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

.field private mRefreshTimeOfLastPopulate:J

.field private mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mWaitBeforeNextPopulate:Z


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryInvalidator;Lvedroid/support/v4/content/LocalBroadcastManager;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/util/Clock;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p4    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p5    # Lcom/google/android/apps/sidekick/inject/EntryInvalidator;
    .param p6    # Lvedroid/support/v4/content/LocalBroadcastManager;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

    invoke-direct {v0, p0, v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mObserver:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

    new-instance v0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;

    invoke-direct {v0, p0, v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mErrorReceiver:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPredictiveCardsListenersRegistered:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mWaitBeforeNextPopulate:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeOfLastPopulate:J

    iput-object p1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryInvalidator:Lcom/google/android/apps/sidekick/inject/EntryInvalidator;

    iput-object p6, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mLocalBroadcastManager:Lvedroid/support/v4/content/LocalBroadcastManager;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
    .param p1    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->handleRefreshTimeout()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->cancelRefreshTimeoutHandler()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)J
    .locals 2
    .param p0    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    iget-wide v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeOfLastPopulate:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method private cancelRefreshTimeoutHandler()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    :cond_0
    return-void
.end method

.method private handleRefreshTimeout()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->populateView()V

    return-void
.end method


# virtual methods
.method public buildView()V
    .locals 8

    const-wide/16 v5, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v4}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->isAttached()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->isInitializedFromStorage()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v4}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->resetView()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getLastRefreshTimeMillis()J

    move-result-wide v0

    cmp-long v4, v0, v5

    if-nez v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->doInitialRefresh()V

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryInvalidator:Lcom/google/android/apps/sidekick/inject/EntryInvalidator;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/EntryInvalidator;->invalidateIfNecessary()V

    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getLastRefreshTimeMillis()J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v5

    if-nez v4, :cond_4

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->doInitialRefresh()V

    goto :goto_0

    :cond_4
    sub-long v4, v2, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getStalenessTimeoutMs()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->doInitialRefresh()V

    goto :goto_0

    :cond_5
    iget-wide v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeOfLastPopulate:J

    cmp-long v4, v4, v0

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mWaitBeforeNextPopulate:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v5, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;

    invoke-direct {v5, p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    const-wide/16 v6, 0x32

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v4}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->populateView()V

    goto :goto_0
.end method

.method public doInitialRefresh()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->isNetworkAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->showError()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->populateView()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->resetView()V

    new-instance v0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    const-wide/16 v2, 0x4e20

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->startInitialRefresh()V

    goto :goto_0
.end method

.method public getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-object v0
.end method

.method public refreshCards(ZZZLcom/google/geo/sidekick/Sidekick$Interest;)V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->entriesIncludeMore()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string v1, "com.google.android.apps.sidekick.MORE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    if-eqz p3, :cond_2

    const-string v1, "com.google.android.apps.sidekick.INCREMENTAL"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    if-eqz p1, :cond_3

    const-string v1, "com.google.android.apps.sidekick.USER_REFRESH"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    if-eqz p4, :cond_4

    const-string v1, "com.google.android.apps.sidekick.INTEREST"

    invoke-virtual {p4}, Lcom/google/geo/sidekick/Sidekick$Interest;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_4
    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public registerPredictiveCardsListeners()V
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPredictiveCardsListenersRegistered:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mObserver:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

    invoke-interface {v1, v2}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->registerEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.android.apps.sidekick.STARTING_REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.google.android.apps.sidekick.REFRESH_SERVER_UNREACHABLE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mLocalBroadcastManager:Lvedroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mErrorReceiver:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;

    invoke-virtual {v1, v2, v0}, Lvedroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPredictiveCardsListenersRegistered:Z

    :cond_0
    return-void
.end method

.method public resetTimeOfLastPopulate()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeOfLastPopulate:J

    return-void
.end method

.method public setPresenter(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    return-void
.end method

.method setRefreshTimeOfLastPopulate(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeOfLastPopulate:J

    return-void
.end method

.method public setWaitBeforeNextPopulate(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mWaitBeforeNextPopulate:Z

    return-void
.end method

.method public unregisterPredictiveCardsListeners()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPredictiveCardsListenersRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mObserver:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->unregisterEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mLocalBroadcastManager:Lvedroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mErrorReceiver:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPredictiveCardsListenersRegistered:Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->cancelRefreshTimeoutHandler()V

    return-void
.end method

.method public updateTimeOfLastPopulate()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getLastRefreshTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeOfLastPopulate:J

    return-void
.end method
