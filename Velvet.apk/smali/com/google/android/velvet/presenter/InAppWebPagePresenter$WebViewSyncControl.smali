.class Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;
.super Ljava/lang/Object;
.source "InAppWebPagePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WebViewSyncControl"
.end annotation


# instance fields
.field private mDelayedPageLoad:Z

.field private mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

.field private mPageLoadState:I

.field private mPageShown:Z


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mDelayedPageLoad:Z

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageShown:Z

    new-instance v0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;-><init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;-><init>()V

    return-void
.end method

.method private showPageOnce()Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageShown:Z

    if-nez v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageShown:Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    const-string v2, "[Showing Page]"

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->add(Ljava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized delayedPageLoad()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mDelayedPageLoad:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    const-string v1, "delayedPageLoad"

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->add(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized dumpEvents(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1    # Ljava/io/PrintWriter;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized isPageLoading()Z
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized pageLoadFinished(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mDelayedPageLoad:Z

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    new-instance v1, Lcom/google/android/searchcommon/util/LazyString;

    const-string v2, "pageLoadFinished %s -> %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/google/android/searchcommon/util/LazyString;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->add(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->showPageOnce()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    new-instance v2, Lcom/google/android/searchcommon/util/LazyString;

    const-string v3, "pageLoadFinished %s -> %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v2, v3, v4}, Lcom/google/android/searchcommon/util/LazyString;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->add(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized pageLoadStarted(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    new-instance v1, Lcom/google/android/searchcommon/util/LazyString;

    const-string v2, "pageLoadStarted %s -> %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/google/android/searchcommon/util/LazyString;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->add(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized pageReady()Z
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mDelayedPageLoad:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    const-string v1, "pageReady"

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->add(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->showPageOnce()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageLoadState:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mDelayedPageLoad:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mPageShown:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->mLog:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;

    const-string v1, "reset"

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->add(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
