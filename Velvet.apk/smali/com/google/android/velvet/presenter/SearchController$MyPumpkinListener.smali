.class Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyPumpkinListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchController;
    .param p2    # Lcom/google/android/velvet/presenter/SearchController$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;-><init>(Lcom/google/android/velvet/presenter/SearchController;)V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;)V
    .locals 5
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    invoke-virtual {p1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->getHypothesisCount()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, "Velvet.SearchController"

    const-string v3, "Pumpkin returned a match."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->getHypothesis(I)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;

    move-result-object v0

    new-instance v1, Lcom/google/android/speech/embedded/TaggerResult;

    invoke-direct {v1, v0}, Lcom/google/android/speech/embedded/TaggerResult;-><init>(Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-static {v1}, Lcom/google/android/velvet/presenter/Action;->fromPumpkinTaggerResult(Lcom/google/android/speech/embedded/TaggerResult;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedPumpkinResult(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v3

    sget-object v4, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedPumpkinResult(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    const/16 v2, 0x62

    invoke-static {v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;->onResult(Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;)V

    return-void
.end method
