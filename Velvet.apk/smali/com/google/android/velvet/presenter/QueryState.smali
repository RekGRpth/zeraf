.class public Lcom/google/android/velvet/presenter/QueryState;
.super Ljava/lang/Object;
.source "QueryState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/QueryState$CombinedError;,
        Lcom/google/android/velvet/presenter/QueryState$Observer;,
        Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;,
        Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;,
        Lcom/google/android/velvet/presenter/QueryState$LoadState;
    }
.end annotation


# instance fields
.field private mActionBackStack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/presenter/Action;",
            ">;"
        }
    .end annotation
.end field

.field private final mActionObserver:Landroid/database/DataSetObserver;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mCommittedQuery:Lcom/google/android/velvet/Query;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mCorpora:Lcom/google/android/velvet/Corpora;

.field private mCurrentAction:Lcom/google/android/velvet/presenter/Action;

.field private mHotwordActive:Z

.field private mHotwordAllowed:Z

.field private final mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

.field private final mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

.field private mMusicLastDetectedAt:J

.field private final mObservers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/velvet/presenter/QueryState$Observer;",
            ">;"
        }
    .end annotation
.end field

.field private final mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/velvet/presenter/QueryState$LoadState",
            "<",
            "Lcom/google/android/velvet/presenter/Action;",
            ">;"
        }
    .end annotation
.end field

.field private mQuery:Lcom/google/android/velvet/Query;

.field private mQueryBackStack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/Query;",
            ">;"
        }
    .end annotation
.end field

.field private final mRandom:Ljava/util/Random;

.field private mResolvingAdClickUrl:Z

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private mWasEditingQuery:Ljava/lang/Boolean;

.field private mWebCorpus:Lcom/google/android/velvet/WebCorpus;

.field private final mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/velvet/Corpora;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Ljava/util/Random;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/velvet/Corpora;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;
    .param p5    # Ljava/util/Random;
    .param p6    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/velvet/presenter/QueryState$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/QueryState$1;-><init>(Lcom/google/android/velvet/presenter/QueryState;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionObserver:Landroid/database/DataSetObserver;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCorpora:Lcom/google/android/velvet/Corpora;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/QueryState;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/QueryState;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mObservers:Ljava/util/Set;

    new-instance v0, Lcom/google/android/searchcommon/util/LatencyTracker;

    const-string v1, "Velvet.QueryState"

    invoke-direct {v0, v1, p4}, Lcom/google/android/searchcommon/util/LatencyTracker;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/Clock;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/Query;->sentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->committed()Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->text()Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    new-instance v0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-direct {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    new-instance v0, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    const-string v1, "majel-action"

    invoke-direct {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    new-instance v0, Lcom/google/android/velvet/presenter/QueryState$LoadState;

    const-string v1, "pumpkin-action"

    invoke-direct {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/QueryState;->mRandom:Ljava/util/Random;

    iput-object p6, p0, Lcom/google/android/velvet/presenter/QueryState;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionBackStack:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/QueryState;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->onActionChanged()V

    return-void
.end method

.method static synthetic access$700(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/velvet/presenter/QueryState;->stateString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private canPumpkinHandleQuery(Lcom/google/android/velvet/Query;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isTextSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getAnswersEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->shouldShowCards()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private editQueryIfNothingToShow()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->resultsLoadedAndNothingToShow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->startQueryEdit()V

    :cond_0
    return-void
.end method

.method private getCurrentError(Lcom/google/android/velvet/presenter/QueryState$LoadState;)Lcom/google/android/velvet/presenter/SearchError;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/presenter/QueryState$LoadState",
            "<*>;)",
            "Lcom/google/android/velvet/presenter/SearchError;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->getError()Lcom/google/android/velvet/presenter/SearchError;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNumCards()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->getNumCards(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)I

    move-result v0

    return v0
.end method

.method private static getNumCards(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;
    .param p1    # Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->shouldShowCards()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/Action;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/Action;->getNumCardsToShow()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private isCurrentCommit(Lcom/google/android/velvet/Query;)Z
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getCommitId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->getCommitId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEditingSummonsQuery()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->isSummons()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->isSummons()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSingleRequestArchitectureEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    return v0
.end method

.method private logLatencyEvents()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->haveCommit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    const/16 v1, 0x55

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LatencyTracker;->logLatencyEvents(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/LatencyTracker;->reset()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isTextSearch()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    const/16 v1, 0x56

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LatencyTracker;->logLatencyEvents(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LatencyTracker;->logLatencyEvents(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LatencyTracker;->logLatencyEvents(I)V

    goto :goto_0
.end method

.method private maybeGoBackOnCardActionComplete()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/Query;

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->goBack()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private maybePushQuery()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingSummonsQuery()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionBackStack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setCurrentAction(Lcom/google/android/velvet/presenter/Action;)V

    goto :goto_0
.end method

.method private maybeSelectAction()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->isLoaded()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->access$500(Lcom/google/android/velvet/presenter/QueryState$LoadState;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$LoadState;->mLoadedData:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->access$500(Lcom/google/android/velvet/presenter/QueryState$LoadState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/Action;

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eq v0, v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v1, v2}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$102(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z
    invoke-static {v1, v2}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$202(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    :cond_1
    const/16 v1, 0x25

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setCurrentAction(Lcom/google/android/velvet/presenter/Action;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->editQueryIfNothingToShow()V

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->isLoaded()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getLoadedData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/Action;

    goto :goto_0
.end method

.method private needToHideResults()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/Action;->getEffectOnWebResults()Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/EffectOnWebResults;->getWebResultTypeToHide()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private needsGogglesDisambiguation()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->needDisambiguation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyStateChange()V
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mObservers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/QueryState$Observer;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/QueryState$Observer;->onQueryStateChanged()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private onActionChanged()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/Action;->isReady()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/Action;->getNumCardsToShow()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->takeTtsAvailable()Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/Action;->takeSuppressCorpora()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v1, v3}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$102(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z
    invoke-static {v1, v2}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$202(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$100(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/Action;->haveCards()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v1, v2}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$102(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z
    invoke-static {v1, v3}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$202(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    :cond_2
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/Action;->isCardActionComplete()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->maybeGoBackOnCardActionComplete()Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_0
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/presenter/Action;->getModifiedCommit(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    if-eq v0, v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->switchQuery(Lcom/google/android/velvet/Query;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->editQueryIfNothingToShow()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/Action;->getModifiedQuery(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    :cond_5
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    goto :goto_0
.end method

.method private setCommittedQuery(Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->shouldEditOnCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->text()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->clearCommit()Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    goto :goto_0
.end method

.method private setCurrentAction(Lcom/google/android/velvet/presenter/Action;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/Action;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->unregisterObserver(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->cancelCardCountDown()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->registerObserver(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private setQuery(Lcom/google/android/velvet/Query;)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    if-eq p1, v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mResolvingAdClickUrl:Z

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private shouldEditOnCommit(Lcom/google/android/velvet/Query;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getSentinelMode()Lcom/google/android/velvet/presenter/UiMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->isSuggestMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldResetQueryOnStopEdit()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->shouldEditOnCommit(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingSummonsQuery()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isSecondarySearchQuery()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private shouldShowNoWebResults()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->getEffectOnWebResults()Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/EffectOnWebResults;->shouldShowNoWebResults()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static stateString(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INVALID("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "IDLE"

    goto :goto_0

    :pswitch_1
    const-string v0, "COMMITTED"

    goto :goto_0

    :pswitch_2
    const-string v0, "LOADING"

    goto :goto_0

    :pswitch_3
    const-string v0, "LOADED"

    goto :goto_0

    :pswitch_4
    const-string v0, "ERROR"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private stopQueryEditInternal()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->shouldResetQueryOnStopEdit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query;->withCommitIdFrom(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    goto :goto_0
.end method

.method private takeQueryToCommit()Lcom/google/android/velvet/Query;
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->equivalentForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method adClickInProgress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mResolvingAdClickUrl:Z

    return v0
.end method

.method public addObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/QueryState$Observer;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mObservers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/QueryState$Observer;->onQueryStateChanged()V

    return-void
.end method

.method canForceReload()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->haveCommit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method cardLoadError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/SearchError;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->loadError(Lcom/google/android/velvet/presenter/SearchError;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$102(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$202(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method clearQuery()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->isSummons()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "web"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/google/android/velvet/Corpus;->getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/Query;->withCorpus(Lcom/google/android/velvet/Corpus;)Lcom/google/android/velvet/Query;

    move-result-object v1

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_1
    return-void
.end method

.method public commit()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->canCommit()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->maybePushQuery()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->haveCommit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isTextSearch()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$102(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$202(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mRandom:Ljava/util/Random;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getCardSuppressionPercentage()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->withSuppressedAnswers()Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/LatencyTracker;->reset()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->committed()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setCommittedQuery(Lcom/google/android/velvet/Query;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_3
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "QueryState:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mQuery: "

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mCommittedQuery: "

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mCurrentAction: "

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mWebviewState: "

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mMajelActionState: "

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mPumpkinActionState: "

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mWebCorpus: "

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebCorpus:Lcom/google/android/velvet/WebCorpus;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Backstack"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionBackStack:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    :goto_1
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    const-string v0, "[no action]"

    goto :goto_1

    :cond_1
    return-void
.end method

.method public forceReloadIfPossible()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->canForceReload()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->reset()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->reset()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    sget-object v1, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->loadComplete(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->reset()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setCurrentAction(Lcom/google/android/velvet/presenter/Action;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method forceReportEditingQueryChanged()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWasEditingQuery:Ljava/lang/Boolean;

    return-void
.end method

.method public get()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method public getAction()Lcom/google/android/velvet/presenter/Action;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    return-object v0
.end method

.method public getCommittedQuery()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method getError()Lcom/google/android/velvet/presenter/SearchError;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowError()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-direct {p0, v2}, Lcom/google/android/velvet/presenter/QueryState;->getCurrentError(Lcom/google/android/velvet/presenter/QueryState$LoadState;)Lcom/google/android/velvet/presenter/SearchError;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-direct {p0, v2}, Lcom/google/android/velvet/presenter/QueryState;->getCurrentError(Lcom/google/android/velvet/presenter/QueryState$LoadState;)Lcom/google/android/velvet/presenter/SearchError;

    move-result-object v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    if-eq v0, v1, :cond_0

    new-instance v2, Lcom/google/android/velvet/presenter/QueryState$CombinedError;

    invoke-direct {v2, v1, v0}, Lcom/google/android/velvet/presenter/QueryState$CombinedError;-><init>(Lcom/google/android/velvet/presenter/SearchError;Lcom/google/android/velvet/presenter/SearchError;)V

    move-object v0, v2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method public goBack()Z
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->resultsLoadedAndNothingToShow()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->stopQueryEditInternal()V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_1
    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/Action;->reambiguate()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/Action;->isCardActionComplete()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v4, 0x2000

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/presenter/Action;->setGwsLoggableEvent(I)V

    :cond_3
    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    if-gez v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->reset()V

    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/Query;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionBackStack:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/Action;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    invoke-direct {p0, v2}, Lcom/google/android/velvet/presenter/QueryState;->setCommittedQuery(Lcom/google/android/velvet/Query;)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->reset()V

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v3, v2}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v3, v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->loadComplete(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v3, v2}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v3, v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->loadComplete(Ljava/lang/Object;)V

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setCurrentAction(Lcom/google/android/velvet/presenter/Action;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/presenter/Action;->getModifiedQuery(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->reset()V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->reset()V

    goto :goto_2
.end method

.method public hasGogglesInQueryStack()Z
    .locals 4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v3}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/Query;

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method haveCommit()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEditingQuery()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isHotwordActive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mHotwordActive:Z

    return v0
.end method

.method isMusicDetected()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mMusicLastDetectedAt:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getMusicDetectionTimeoutMs()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isZeroQuery()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public newQueryFromWebView(Lcom/google/android/velvet/Query;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->equivalentForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/LatencyTracker;->reset()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->maybePushQuery()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->committed()Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    goto :goto_0
.end method

.method onAdClickComplete()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mResolvingAdClickUrl:Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    return-void
.end method

.method onAdClickStart()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mResolvingAdClickUrl:Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    return-void
.end method

.method onGogglesQueryImageReady(Lcom/google/android/velvet/Query;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/Query;->withGogglesQueryImage(Landroid/graphics/Bitmap;)Lcom/google/android/velvet/Query;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method onListeningForHotwordChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mHotwordActive:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mHotwordActive:Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method onMusicDetected()V
    .locals 7

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mMusicLastDetectedAt:J

    sub-long v3, v1, v3

    iget-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->getMusicDetectionTimeoutMs()I

    move-result v5

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-wide v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMusicLastDetectedAt:J

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/Action;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->loadComplete(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->maybeSelectAction()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method public onReceivedPumpkinResult(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/Action;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LatencyTracker;->reportLatencyEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->loadComplete(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->maybeSelectAction()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method public onReceivedTts(Lcom/google/android/velvet/Query;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getLoadedData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/Action;

    invoke-static {v2, v1}, Lcom/google/android/velvet/presenter/QueryState;->getNumCards(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->ttsAvailable()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_1
    return-void
.end method

.method onRecognitionPaused(Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->reset()V

    :cond_0
    return-void
.end method

.method onTextRecognizedWithProxiedSrp(Lcom/google/android/velvet/Query;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/velvet/prefetch/SearchResultPage;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/velvet/prefetch/SearchResultPage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/prefetch/SearchResultPage;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/velvet/Query;->withRecognizedText(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object p1

    if-eqz p5, :cond_0

    invoke-interface {p5, p1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->updateQuery(Lcom/google/android/velvet/Query;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_1
    return-void
.end method

.method public pintoModuleLoaded()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPintoModuleLoaded:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$402(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    return-void
.end method

.method public removeObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/QueryState$Observer;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mObservers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public reportLatencyEvent(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/util/LatencyTracker;->reportLatencyEvent(I)V

    const/16 v0, 0xe

    if-eq p1, v0, :cond_0

    const/16 v0, 0x27

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->logLatencyEvents()V

    :cond_1
    return-void
.end method

.method reset()V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mResolvingAdClickUrl:Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->reset()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->reset()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->reset()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionBackStack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-direct {p0, v3}, Lcom/google/android/velvet/presenter/QueryState;->setCurrentAction(Lcom/google/android/velvet/presenter/Action;)V

    sget-object v1, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/Query;->sentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->committed()Lcom/google/android/velvet/Query;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebCorpus:Lcom/google/android/velvet/WebCorpus;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebCorpus:Lcom/google/android/velvet/WebCorpus;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query;->withCorpus(Lcom/google/android/velvet/Corpus;)Lcom/google/android/velvet/Query;

    move-result-object v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    return-void
.end method

.method public restoreInstanceState(Landroid/os/Bundle;Lcom/google/android/voicesearch/VelvetCardController;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
    .param p2    # Lcom/google/android/voicesearch/VelvetCardController;

    const-string v5, "velvet:query_state:query"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->reset()V

    const-string v5, "velvet:query_state:query"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/velvet/Query;

    iput-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    const-string v5, "velvet:query_state:committed_query"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/velvet/Query;

    iput-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    const-string v5, "velvet:query_state:action"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    iget-object v6, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    iget-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v5, v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->loadComplete(Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    iget-object v6, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    iget-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v5, v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->loadComplete(Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {p2, v5, v0}, Lcom/google/android/voicesearch/VelvetCardController;->restoreActionControllers(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setCurrentAction(Lcom/google/android/velvet/presenter/Action;)V

    :cond_0
    const-string v5, "velvet:query_state:query_back_stack"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    const-string v5, "velvet:query_state:action_back_stack"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    array-length v5, v4

    array-length v6, v1

    if-ne v5, v6, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v2, 0x0

    :goto_1
    array-length v5, v4

    if-ge v2, v5, :cond_3

    aget-object v3, v4, v2

    check-cast v3, Lcom/google/android/velvet/Query;

    aget-object v0, v1, v2

    check-cast v0, Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_1

    invoke-virtual {p2, v3, v0}, Lcom/google/android/voicesearch/VelvetCardController;->restoreActionControllers(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    :cond_1
    iget-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionBackStack:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    :cond_3
    return-void
.end method

.method resultHidingComplete()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I
    invoke-static {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$300(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->resultHidingComplete()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method resultsLoadedAndNothingToShow()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->haveCommit()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->getNumCards()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowNoWebResults()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->needsGogglesDisambiguation()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resultsPageEnd()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->loadComplete(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    return-void
.end method

.method public resultsPageError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/SearchError;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->loadError(Lcom/google/android/velvet/presenter/SearchError;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->expectActionFromGws()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/velvet/presenter/QueryState;->cardLoadError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_1
    return-void
.end method

.method public resultsPageStart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->loadStarted()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    return-void
.end method

.method public retry(Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->reset()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->reset()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->reset()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "velvet:query_state:query"

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "velvet:query_state:committed_query"

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "velvet:query_state:action"

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "velvet:query_state:query_back_stack"

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/velvet/Query;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v1, "velvet:query_state:action_back_stack"

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionBackStack:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mActionBackStack:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/velvet/presenter/Action;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    return-void
.end method

.method selectCorpus(Lcom/google/android/velvet/Corpus;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpus;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query;->withCorpus(Lcom/google/android/velvet/Corpus;)Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-object p0
.end method

.method setHotwordAllowed(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mHotwordAllowed:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mHotwordAllowed:Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method public shouldCancel(Lcom/google/android/velvet/Query;)Z
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method shouldListenForHotword()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mHotwordAllowed:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->haveCommit()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getSentinelMode()Lcom/google/android/velvet/presenter/UiMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->isHotwordSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method shouldShowCards()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mResolvingAdClickUrl:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->getNumCards()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    goto :goto_0
.end method

.method shouldShowError()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0, v3}, Lcom/google/android/velvet/presenter/QueryState;->canPumpkinHandleQuery(Lcom/google/android/velvet/Query;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->isLoaded()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->getNumCards()I

    move-result v3

    if-gtz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-direct {p0, v3}, Lcom/google/android/velvet/presenter/QueryState;->getCurrentError(Lcom/google/android/velvet/presenter/QueryState$LoadState;)Lcom/google/android/velvet/presenter/SearchError;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-direct {p0, v3}, Lcom/google/android/velvet/presenter/QueryState;->getCurrentError(Lcom/google/android/velvet/presenter/QueryState$LoadState;)Lcom/google/android/velvet/presenter/SearchError;

    move-result-object v2

    if-eqz v0, :cond_2

    if-nez v2, :cond_5

    :cond_2
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->isLoadingOrLoaded()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_3
    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->getNumCards()I

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v3}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    const/4 v1, 0x1

    :cond_6
    goto :goto_0
.end method

.method shouldShowSpinner()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->haveCommit()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->getNumCards()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mResolvingAdClickUrl:Z

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowError()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    :cond_4
    return v0
.end method

.method shouldShowWebView()Z
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mResolvingAdClickUrl:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mReadyToShow:Z
    invoke-static {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$600(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z

    move-result v1

    and-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowNoWebResults()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getAnswersEnabled()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->shouldShowCards()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->getNumCards()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->needToHideResults()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->isWebResultHidden()Z

    move-result v0

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->isLoadingOrLoaded()Z

    move-result v0

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->isLoadingOrLoaded()Z

    move-result v0

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->hasError()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->isLoadingOrLoaded()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v0, 0x1

    :goto_1
    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method

.method shouldSuppressCorpora()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$100(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z

    move-result v0

    return v0
.end method

.method public startQueryEdit()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->shouldResetQueryOnStopEdit()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->text()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->clearCommit()Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->text()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->clearCommit()Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    goto :goto_0
.end method

.method public stopQueryEdit()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->stopQueryEditInternal()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method

.method switchQuery(Lcom/google/android/velvet/Query;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LatencyTracker;->reportLatencyEvent(I)V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/velvet/Query;->isSameQueryType(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->setCurrentAction(Lcom/google/android/velvet/presenter/Action;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_1
    return-void
.end method

.method public takeActionToHandle()Lcom/google/android/velvet/presenter/Action;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->takeStartHandling()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LatencyTracker;->reportLatencyEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method takeEditingQueryChanged()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWasEditingQuery:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWasEditingQuery:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWasEditingQuery:Ljava/lang/Boolean;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method takeNewlyCommittedWebQuery()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->takeNewlyCommitted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method takeNewlyLoadedWebQuery()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->takeNewlyLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method takePlayTtsWithoutCard()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    sget-object v1, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->canPlayTts()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getLoadedData()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->haveCards()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->takeTtsAvailable()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowWebView()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->takeTtsAvailable()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public takeQueryToCommitToMajel()Lcom/google/android/velvet/Query;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/velvet/Query;->isSameQueryType(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isTextSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getAnswersEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->shouldShowCards()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    :goto_0
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public takeQueryToCommitToPumpkin()Lcom/google/android/velvet/Query;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->canPumpkinHandleQuery(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState;->isCurrentCommit(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mLatencyTracker:Lcom/google/android/searchcommon/util/LatencyTracker;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LatencyTracker;->reportLatencyEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public takeQueryToCommitToWebView()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->isSingleRequestArchitectureEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->takeQueryToCommit()Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public takeQueryToLoadViaGws()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->isSingleRequestArchitectureEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->takeQueryToCommit()Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->expectActionFromGws()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-nez v1, :cond_1

    const/16 v1, 0x23

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->queryCommitted(Lcom/google/android/velvet/Query;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public takeResultTypeToHide()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I
    invoke-static {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$300(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPintoModuleLoaded:Z
    invoke-static {v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$400(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/Action;->getEffectOnWebResults()Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/EffectOnWebResults;->getWebResultTypeToHide()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v2, 0x2

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I
    invoke-static {v1, v2}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$302(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;I)I

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I
    invoke-static {v1, v2}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$302(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;I)I

    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    goto :goto_0
.end method

.method takeShowCorporaRequest()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$200(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$100(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z
    invoke-static {v0, v2}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$202(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public takeUnusedActionToLog()Lcom/google/android/velvet/presenter/Action;
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->hasDataFor(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->hasDataFor(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->getLoadedData()Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->getLoadedData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/Action;

    move-object v0, v1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->takeStartHandling()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mPumpkinActionState:Lcom/google/android/velvet/presenter/QueryState$LoadState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->getLoadedData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/Action;

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QS[Q:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " CQ:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mMajelActionState:Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WVS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsupressCorpora()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->setGwsLoggableEvent(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$100(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$102(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_1
    return-void
.end method

.method updateCorpora(Lcom/google/android/velvet/Corpora;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Corpora;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/Query;->withUpdatedCorpora(Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->setQuery(Lcom/google/android/velvet/Query;)Z

    invoke-virtual {p1}, Lcom/google/android/velvet/Corpora;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebCorpus:Lcom/google/android/velvet/WebCorpus;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState;->haveCommit()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/Query;->withUpdatedCorpora(Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mCommittedQuery:Lcom/google/android/velvet/Query;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/QueryState;->mQueryBackStack:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/Query;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/Query;->withUpdatedCorpora(Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    return-void
.end method

.method public webViewReadyToShowChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # getter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mReadyToShow:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$600(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState;->mWebviewState:Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    # setter for: Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mReadyToShow:Z
    invoke-static {v0, p1}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->access$602(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/QueryState;->notifyStateChange()V

    :cond_0
    return-void
.end method
