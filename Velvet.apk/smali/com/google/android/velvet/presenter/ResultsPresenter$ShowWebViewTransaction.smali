.class Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "ResultsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/ResultsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowWebViewTransaction"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/ResultsPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/ResultsPresenter;Lcom/google/android/velvet/presenter/ResultsPresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/ResultsPresenter;
    .param p2    # Lcom/google/android/velvet/presenter/ResultsPresenter$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;-><init>(Lcom/google/android/velvet/presenter/ResultsPresenter;)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # invokes: Lcom/google/android/velvet/presenter/ResultsPresenter;->setupWebView()V
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$700(Lcom/google/android/velvet/presenter/ResultsPresenter;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewVisible:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$602(Lcom/google/android/velvet/presenter/ResultsPresenter;Z)Z

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Lcom/google/android/velvet/presenter/MainContentUi;->setWhiteBackgroundState(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # invokes: Lcom/google/android/velvet/presenter/ResultsPresenter;->updateWebViewLayout()V
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$800(Lcom/google/android/velvet/presenter/ResultsPresenter;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "ShowWebViewTransaction[]"

    return-object v0
.end method
