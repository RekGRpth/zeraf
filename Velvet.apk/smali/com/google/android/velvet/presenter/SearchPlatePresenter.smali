.class public Lcom/google/android/velvet/presenter/SearchPlatePresenter;
.super Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
.source "SearchPlatePresenter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionsUi;


# instance fields
.field public final gogglesCaptureCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

.field public final gogglesResponseCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

.field private mCorrectionPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

.field private mGogglesController:Lcom/google/android/goggles/GogglesController;

.field private mQueryEmpty:Z

.field private mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

.field private final mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

.field public final recognizerCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/SearchPlateUi;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/SearchPlateUi;

    const-string v0, "searchplate"

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;-><init>(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->recognizerCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    new-instance v0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;-><init>(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->gogglesCaptureCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    new-instance v0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;-><init>(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->gogglesResponseCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mQueryEmpty:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/goggles/GogglesController;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    return-object v0
.end method


# virtual methods
.method public enterGogglesMode(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showGogglesInput(Z)V

    return-void
.end method

.method public enterGogglesResponseMode(ZI)V
    .locals 1
    .param p1    # Z
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showGogglesResponse(ZI)V

    return-void
.end method

.method public enterQueryEditMode()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->focusSearchBox()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showKeyboard()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v1, v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->hideLogo(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mQueryEmpty:Z

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v1, v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showClearButton(Z)V

    return-void
.end method

.method public enterSoundSearchMode(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showSoundSearchInput(Z)V

    return-void
.end method

.method public enterTextMode(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showTextInput(Z)V

    return-void
.end method

.method public enterVoiceSearchMode(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showSpeechInput(Z)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->isMusicDetected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isTriggeredFromHotword()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showSoundSearchPromotedQuery()V

    :cond_0
    return-void
.end method

.method public getGogglesQueryImage()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->getGogglesQueryImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public hideKeyboard()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->hideKeyboard()V

    return-void
.end method

.method public hideProgress()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->hideProgress()V

    return-void
.end method

.method public onCameraSearchButtonClick()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getGogglesDisclosedCapability()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/velvet/Query;->goggles(I)Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    :cond_0
    return-void
.end method

.method public onClearButtonClick()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->clearQuery()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->resetSearchPlate()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterQueryEditMode()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isChangingConfigurations()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->pause(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->isGogglesCapturing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/GogglesController;->pause(Z)V

    :cond_0
    invoke-super {p0}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onPause()V

    return-void
.end method

.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSearchController()Lcom/google/android/velvet/presenter/SearchController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchController;->getRecognizerController()Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getGogglesController()Lcom/google/android/goggles/GogglesController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->attachUi(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/GogglesController;->attachUi(Lcom/google/android/goggles/GogglesController$GogglesUi;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionsCachingPromoter()Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mCorrectionPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mCorrectionPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetFactory;->createCorrectionPromoter()Lcom/google/android/searchcommon/suggest/Promoter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->setPromoter(Lcom/google/android/searchcommon/suggest/Promoter;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->CORRECTION_SUGGESTIONS:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mCorrectionPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    invoke-interface {v0, v1, v2, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->addSuggestionsView(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)V

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v1, p1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->restoreInstanceState(Landroid/os/Bundle;)V

    :cond_2
    return-void
.end method

.method protected onPreDetach()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->detachUi(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/GogglesController;->detachUi(Lcom/google/android/goggles/GogglesController$GogglesUi;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->CORRECTION_SUGGESTIONS:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->removeSuggestionsView(Ljava/lang/Object;)V

    return-void
.end method

.method public onPromotedSoundSearchClick()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->soundSearchFromPromotedQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->switchQuery(Lcom/google/android/velvet/Query;)V

    return-void
.end method

.method public onQueryTextChanged(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->startQueryEdit()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onQueryTextChanged(Ljava/lang/String;)V

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mQueryEmpty:Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mQueryEmpty:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showClearButton(Z)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->isGogglesCapturing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesController;->resume()V

    :cond_0
    return-void
.end method

.method public onSearchBoxEditorAction(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSearchBoxTouched()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancel()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onSearchBoxTouched()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterQueryEditMode()V

    return-void
.end method

.method public onSearchButtonClick()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    :cond_0
    return-void
.end method

.method public onStopListening()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->stopListening()V

    :cond_0
    return-void
.end method

.method public onVoiceSearchButtonClick()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->voiceSearch()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    :cond_0
    return-void
.end method

.method public resetSearchPlate()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, v1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showTextInput(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showTextQueryMode()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, v1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->hideLogo(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->hideFocus()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showClearButton(Z)V

    return-void
.end method

.method public setHintTextResource(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->setHintText(I)V

    return-void
.end method

.method public setQuery(Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mQueryEmpty:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/SearchPlateUi;->setQuery(Lcom/google/android/velvet/Query;)V

    return-void
.end method

.method public showProgress()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showProgress()V

    return-void
.end method

.method public showQueryComitted()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showLogo()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showTextQueryMode()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mQueryEmpty:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showClearButton(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->hideFocus()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showSoundSearchListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showSoundSearchListening()V

    return-void
.end method

.method public showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p3    # I
    .param p4    # Z
    .param p5    # Z

    if-lez p3, :cond_0

    const/4 v2, 0x0

    invoke-interface {p2, v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v2, v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->setTextQueryCorrections(Landroid/text/Spanned;)V

    :cond_0
    return-void
.end method

.method public showVoiceQueryComitted()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showLogo()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showVoiceQueryMode()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mQueryEmpty:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->showClearButton(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mUi:Lcom/google/android/velvet/presenter/SearchPlateUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/SearchPlateUi;->hideFocus()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
