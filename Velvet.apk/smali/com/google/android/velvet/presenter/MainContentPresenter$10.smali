.class Lcom/google/android/velvet/presenter/MainContentPresenter$10;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "MainContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/MainContentPresenter;->postSetVerticalItemMargin(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

.field final synthetic val$margin:I


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;II)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$10;->this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

    iput p4, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$10;->val$margin:I

    invoke-direct {p0, p2, p3}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$10;->val$margin:I

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setVerticalItemMargin(I)V

    return-void
.end method
