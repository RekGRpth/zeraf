.class Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;
.super Ljava/lang/Object;
.source "CardDismissalHandler.java"

# interfaces
.implements Lcom/google/android/velvet/tg/DismissTrailFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/CardDismissalHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TgDismissTrailFactory"
.end annotation


# instance fields
.field private final mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

.field final synthetic this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/CardDismissalHandler;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 0
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/CardDismissalHandler;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/velvet/presenter/CardDismissalHandler$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/CardDismissalHandler;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p3    # Lcom/google/android/velvet/presenter/CardDismissalHandler$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;-><init>(Lcom/google/android/velvet/presenter/CardDismissalHandler;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    return-void
.end method


# virtual methods
.method public createDismissTrail()Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->shouldShowDismissTrail()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    # getter for: Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPresenter:Lcom/google/android/velvet/presenter/TgPresenter;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->access$100(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/TgPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    # getter for: Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPresenter:Lcom/google/android/velvet/presenter/TgPresenter;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->access$100(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/VelvetFactory;->createDismissTrail(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;)Lcom/google/android/apps/sidekick/DismissTrail;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/DismissTrail;->setCardDismissalHandler(Lcom/google/android/velvet/presenter/CardDismissalHandler;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    # getter for: Lcom/google/android/velvet/presenter/CardDismissalHandler;->mShowedDismissTrailForEntryTypes:Landroid/util/SparseBooleanArray;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->access$200(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Landroid/util/SparseBooleanArray;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    :cond_0
    return-object v0
.end method

.method public shouldShowDismissTrail()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    # getter for: Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPresenter:Lcom/google/android/velvet/presenter/TgPresenter;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->access$100(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    # getter for: Lcom/google/android/velvet/presenter/CardDismissalHandler;->mShowedDismissTrailForEntryTypes:Landroid/util/SparseBooleanArray;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->access$200(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;->this$0:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    # invokes: Lcom/google/android/velvet/presenter/CardDismissalHandler;->getDismissedEntryTypes()Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->access$300(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
