.class Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;
.super Ljava/lang/Object;
.source "ContextHeaderPresenter.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->maybeUpdateContextImage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

.field final synthetic val$backgroundImage:Lcom/google/android/apps/sidekick/inject/BackgroundImage;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;Lcom/google/android/apps/sidekick/inject/BackgroundImage;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;->val$backgroundImage:Lcom/google/android/apps/sidekick/inject/BackgroundImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Landroid/graphics/drawable/Drawable;)Z
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mSetDefaultImageRunnable:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->access$200(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->setCanceled(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEnabled:Z
    invoke-static {v1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->access$300(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->isAttached()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUi:Lcom/google/android/velvet/presenter/ContextHeaderUi;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->access$400(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Lcom/google/android/velvet/presenter/ContextHeaderUi;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;->val$backgroundImage:Lcom/google/android/apps/sidekick/inject/BackgroundImage;

    invoke-interface {v1, p1, v2}, Lcom/google/android/velvet/presenter/ContextHeaderUi;->setContextImageDrawable(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/sidekick/inject/BackgroundImage;)V

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;->consume(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method
