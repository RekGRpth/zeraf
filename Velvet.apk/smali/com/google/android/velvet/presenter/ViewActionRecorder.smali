.class public Lcom/google/android/velvet/presenter/ViewActionRecorder;
.super Ljava/lang/Object;
.source "ViewActionRecorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;
    }
.end annotation


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

.field private mPresenter:Lcom/google/android/velvet/presenter/MainContentPresenter;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/util/Clock;
    .param p2    # Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    return-void
.end method

.method public static addListCardTags(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 5
    .param p0    # Landroid/view/View;
    .param p1    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f100015

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const v2, 0x7f100016

    invoke-virtual {p0, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    new-instance v2, Lcom/google/android/velvet/presenter/ViewActionRecorder$1;

    invoke-direct {v2, p0}, Lcom/google/android/velvet/presenter/ViewActionRecorder$1;-><init>(Landroid/view/View;)V

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    return-void
.end method

.method private getEntryViewHierarchies()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;",
            ">;"
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mPresenter:Lcom/google/android/velvet/presenter/MainContentPresenter;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/MainContentPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const v5, 0x7f100011

    invoke-virtual {v1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    if-eqz v0, :cond_0

    new-instance v5, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;

    invoke-direct {v5, v1, v0}, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;-><init>(Landroid/view/View;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private getVisibleEntryViews()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v9, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mPresenter:Lcom/google/android/velvet/presenter/MainContentPresenter;

    invoke-virtual {v9}, Lcom/google/android/velvet/presenter/MainContentPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v4

    iget-object v9, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mPresenter:Lcom/google/android/velvet/presenter/MainContentPresenter;

    invoke-virtual {v9}, Lcom/google/android/velvet/presenter/MainContentPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v5

    check-cast v5, Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-interface {v4}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v7

    invoke-interface {v4}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getViewportHeight()I

    move-result v9

    add-int v6, v7, v9

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->getEntryViewHierarchies()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;

    iget-object v9, v1, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mRootView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    if-lez v9, :cond_0

    iget-object v9, v1, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mRootView:Landroid/view/View;

    invoke-virtual {v5, v9}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->isViewLocallyVisible(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->isExpanded()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->getEntryViews()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v4, v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getDescendantTop(Landroid/view/View;)I

    move-result v9

    invoke-static {v0, v9, v7, v6}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->isCardVisible(Landroid/view/View;III)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v8
.end method

.method private static isCardVisible(Landroid/view/View;III)Z
    .locals 6
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x0

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int v0, p1, v4

    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v4, v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    if-ge v4, v5, :cond_2

    if-gt p1, p2, :cond_0

    if-lt v0, p3, :cond_0

    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method recordViewEndTimes()V
    .locals 15

    const v14, 0x7f100014

    iget-object v12, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v12}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->getEntryViewHierarchies()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->getEntryViews()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4, v14}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    if-eqz v9, :cond_1

    const v12, 0x7f100015

    invoke-virtual {v4, v12}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v12

    const/16 v13, 0x15

    if-ne v12, v13, :cond_2

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    sub-long v10, v1, v12

    const-wide/16 v12, 0x1f4

    cmp-long v12, v10, v12

    if-ltz v12, :cond_3

    iget-object v12, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    invoke-interface {v12, v3, v0, v10, v11}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;->saveAction(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;J)V

    :cond_3
    const/4 v12, 0x0

    invoke-virtual {v4, v14, v12}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method recordViewStartTimes()V
    .locals 6

    const v5, 0x7f100014

    iget-object v4, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->getVisibleEntryViews()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setPresenter(Lcom/google/android/velvet/presenter/MainContentPresenter;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/MainContentPresenter;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/MainContentPresenter;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder;->mPresenter:Lcom/google/android/velvet/presenter/MainContentPresenter;

    return-void
.end method
