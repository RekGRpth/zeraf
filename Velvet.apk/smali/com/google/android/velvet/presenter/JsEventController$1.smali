.class Lcom/google/android/velvet/presenter/JsEventController$1;
.super Ljava/lang/Object;
.source "JsEventController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/JsEventController;->runHandlers(Ljava/util/Map;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/JsEventController;

.field final synthetic val$immutableEvents:Ljava/util/Map;

.field final synthetic val$urlString:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/JsEventController;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/JsEventController$1;->this$0:Lcom/google/android/velvet/presenter/JsEventController;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/JsEventController$1;->val$immutableEvents:Ljava/util/Map;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/JsEventController$1;->val$urlString:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/velvet/presenter/JsEventController$1;->this$0:Lcom/google/android/velvet/presenter/JsEventController;

    # getter for: Lcom/google/android/velvet/presenter/JsEventController;->mHandlers:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/JsEventController;->access$000(Lcom/google/android/velvet/presenter/JsEventController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/JsEventController$1;->val$immutableEvents:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/JsEventController$1;->val$urlString:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;->handleEvents(Ljava/util/Map;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method
