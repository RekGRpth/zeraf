.class public Lcom/google/android/velvet/presenter/JsEventController;
.super Ljava/lang/Object;
.source "JsEventController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;
    }
.end annotation


# instance fields
.field private mHandlers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/JsEventController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/JsEventController;->mHandlers:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/JsEventController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/JsEventController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/JsEventController;->mHandlers:Ljava/util/List;

    return-object v0
.end method

.method private declared-synchronized runHandlers(Ljava/util/Map;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/JsEventController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v2, Lcom/google/android/velvet/presenter/JsEventController$1;

    invoke-direct {v2, p0, v0, p2}, Lcom/google/android/velvet/presenter/JsEventController$1;-><init>(Lcom/google/android/velvet/presenter/JsEventController;Ljava/util/Map;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public dispatchEvents(Ljava/util/Map;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/presenter/JsEventController;->runHandlers(Ljava/util/Map;Ljava/lang/String;)V

    return-void
.end method

.method public dispose()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/JsEventController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/JsEventController;->mHandlers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public registerHandler(Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/JsEventController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/JsEventController;->mHandlers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/JsEventController;->mHandlers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
