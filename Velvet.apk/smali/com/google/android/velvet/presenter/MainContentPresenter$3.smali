.class Lcom/google/android/velvet/presenter/MainContentPresenter$3;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "MainContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/MainContentPresenter;->postAddViews(ILjava/lang/Iterable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

.field final synthetic val$index:I

.field final synthetic val$views:Ljava/lang/Iterable;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;IILjava/lang/Iterable;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;
    .param p4    # I

    iput-object p1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$3;->this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

    iput p5, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$3;->val$index:I

    iput-object p6, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$3;->val$views:Ljava/lang/Iterable;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>(Ljava/lang/String;Ljava/lang/Object;I)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    iget v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$3;->val$index:I

    iget-object v3, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$3;->val$views:Ljava/lang/Iterable;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
