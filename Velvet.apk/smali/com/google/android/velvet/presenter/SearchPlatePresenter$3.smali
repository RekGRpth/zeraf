.class Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;
.super Ljava/lang/Object;
.source "SearchPlatePresenter.java"

# interfaces
.implements Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchPlatePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraButtonClicked()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    # getter for: Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->access$100(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/goggles/GogglesController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesController;->onCameraButtonClicked()V

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/TraceTracker;->newSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventStartSearch(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->onCameraSearchButtonClick()V

    return-void
.end method

.method public onClearButtonClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->onClearButtonClick()V

    return-void
.end method

.method public onMicrophoneButtonClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->onVoiceSearchButtonClick()V

    return-void
.end method

.method public onTextEdited(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Z)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->hideKeyboard()V

    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    # getter for: Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->access$100(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/goggles/GogglesController;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/goggles/GogglesController;->onTextEdited(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$3;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->switchQuery(Lcom/google/android/velvet/Query;)V

    goto :goto_0
.end method
