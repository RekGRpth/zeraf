.class Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "InAppWebPageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/InAppWebPageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InAppWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;Lcom/google/android/velvet/ui/InAppWebPageActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/ui/InAppWebPageActivity;
    .param p2    # Lcom/google/android/velvet/ui/InAppWebPageActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    # getter for: Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->pageLoadFinished(Ljava/lang/String;)V

    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    # getter for: Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->pageLoadStarted(Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    # getter for: Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v0

    invoke-virtual {v0, p4, p3, p2}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->pageLoadError(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/HttpAuthHandler;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    # getter for: Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "Received HTTP Auth Request"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->pageLoadError(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p2}, Landroid/webkit/HttpAuthHandler;->cancel()V

    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 4
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/SslErrorHandler;
    .param p3    # Landroid/net/http/SslError;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    # getter for: Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p3}, Landroid/net/http/SslError;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->pageLoadError(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->cancel()V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    # getter for: Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v0

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->shouldOverrideUrlLoading(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
