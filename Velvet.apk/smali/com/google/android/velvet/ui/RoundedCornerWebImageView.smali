.class public Lcom/google/android/velvet/ui/RoundedCornerWebImageView;
.super Lcom/google/android/velvet/ui/WebImageView;
.source "RoundedCornerWebImageView.java"


# instance fields
.field private final mPaint:Landroid/graphics/Paint;

.field private final mRoundedRectangle:Landroid/graphics/drawable/shapes/Shape;

.field private mShader:Landroid/graphics/BitmapShader;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/ui/WebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    if-eqz p2, :cond_5

    sget-object v4, Lcom/google/android/googlequicksearchbox/R$styleable;->RoundedCornerWebImageView:[I

    invoke-virtual {p1, p2, v4, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    cmpl-float v4, v3, v8

    if-lez v4, :cond_4

    if-lez v1, :cond_4

    const/16 v4, 0x8

    new-array v2, v4, [F

    and-int/lit8 v4, v1, 0x1

    if-eqz v4, :cond_0

    aput v3, v2, v6

    aput v3, v2, v7

    :cond_0
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    aput v3, v2, v4

    const/4 v4, 0x3

    aput v3, v2, v4

    :cond_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    const/4 v4, 0x4

    aput v3, v2, v4

    const/4 v4, 0x5

    aput v3, v2, v4

    :cond_2
    and-int/lit8 v4, v1, 0x8

    if-eqz v4, :cond_3

    const/4 v4, 0x6

    aput v3, v2, v4

    const/4 v4, 0x7

    aput v3, v2, v4

    :cond_3
    new-instance v4, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v4, v2, v5, v5}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    iput-object v4, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mRoundedRectangle:Landroid/graphics/drawable/shapes/Shape;

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :goto_0
    return-void

    :cond_4
    iput-object v5, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mRoundedRectangle:Landroid/graphics/drawable/shapes/Shape;

    iput-object v5, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mPaint:Landroid/graphics/Paint;

    goto :goto_0

    :cond_5
    iput-object v5, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mRoundedRectangle:Landroid/graphics/drawable/shapes/Shape;

    iput-object v5, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mPaint:Landroid/graphics/Paint;

    goto :goto_0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mShader:Landroid/graphics/BitmapShader;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_1

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/WebImageView;->onDraw(Landroid/graphics/Canvas;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mShader:Landroid/graphics/BitmapShader;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mShader:Landroid/graphics/BitmapShader;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v1, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mRoundedRectangle:Landroid/graphics/drawable/shapes/Shape;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/shapes/Shape;->resize(FF)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mRoundedRectangle:Landroid/graphics/drawable/shapes/Shape;

    iget-object v2, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p1, v2}, Landroid/graphics/drawable/shapes/Shape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mRoundedRectangle:Landroid/graphics/drawable/shapes/Shape;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    new-instance v1, Landroid/graphics/BitmapShader;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, v0, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v1, p0, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->mShader:Landroid/graphics/BitmapShader;

    :cond_0
    return-void
.end method
