.class public Lcom/google/android/velvet/ui/CardUtils;
.super Ljava/lang/Object;
.source "CardUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static examplify(Landroid/view/View;)V
    .locals 5
    .param p0    # Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Landroid/view/View;->setEnabled(Z)V

    instance-of v3, p0, Landroid/widget/EditText;

    if-eqz v3, :cond_0

    move-object v3, p0

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setInputType(I)V

    :cond_0
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-nez v3, :cond_2

    :cond_1
    return-void

    :cond_2
    move-object v2, p0

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/velvet/ui/CardUtils;->examplify(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
