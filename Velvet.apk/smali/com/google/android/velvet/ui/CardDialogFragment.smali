.class public Lcom/google/android/velvet/ui/CardDialogFragment;
.super Landroid/app/DialogFragment;
.source "CardDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Z)Lcom/google/android/velvet/ui/CardDialogFragment;
    .locals 4
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Z

    new-instance v1, Lcom/google/android/velvet/ui/CardDialogFragment;

    invoke-direct {v1}, Lcom/google/android/velvet/ui/CardDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "entry_key"

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "is_sample_key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/CardDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 17
    .param p1    # Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "entry_key"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v9

    new-instance v8, Landroid/app/Dialog;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    const v15, 0x7f0e0018

    invoke-direct {v8, v14, v15}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const/4 v14, 0x1

    invoke-virtual {v8, v14}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    const v14, 0x7f040014

    invoke-virtual {v8, v14}, Landroid/app/Dialog;->setContentView(I)V

    if-eqz v9, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v13

    invoke-interface {v13}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-interface {v14, v9, v15, v0}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "is_sample_key"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->examplify()V

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v2, v1, v14, v15}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    invoke-interface {v2, v1, v5}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "is_sample_key"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-static {v5}, Lcom/google/android/velvet/ui/CardUtils;->examplify(Landroid/view/View;)V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v14

    iget v12, v14, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0022

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0070

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    mul-int/lit8 v14, v7, 0x2

    sub-int v14, v12, v14

    invoke-static {v6, v14}, Ljava/lang/Math;->min(II)I

    move-result v6

    const v14, 0x7f100050

    invoke-virtual {v8, v14}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    new-instance v14, Landroid/view/ViewGroup$LayoutParams;

    const/4 v15, -0x2

    invoke-direct {v14, v6, v15}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v14}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v8}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v14

    const v15, 0x3f4ccccd

    iput v15, v14, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/CardDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "is_sample_key"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    const v14, 0x7f100051

    invoke-virtual {v4, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v14, 0x7f0d00da

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/velvet/ui/CardDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v14, 0x0

    invoke-virtual {v3, v14}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v3}, Landroid/widget/TextView;->bringToFront()V

    :cond_2
    return-object v8
.end method
