.class public Lcom/google/android/velvet/ui/CardAnimator;
.super Landroid/animation/ValueAnimator;
.source "CardAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/CardAnimator$3;
    }
.end annotation


# instance fields
.field private fromRight:Z

.field private mAnimationIndex:I

.field private mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

.field private final mDisplayHeight:I

.field private final mIsAppear:Z

.field private mTargetView:Landroid/view/View;


# direct methods
.method public constructor <init>(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    const/4 v1, 0x2

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    iput v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationIndex:I

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->fromRight:Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    iput p2, p0, Lcom/google/android/velvet/ui/CardAnimator;->mDisplayHeight:I

    new-instance v0, Lcom/google/android/velvet/ui/CardAnimator$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/CardAnimator$1;-><init>(Lcom/google/android/velvet/ui/CardAnimator;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/CardAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v0, Lcom/google/android/velvet/ui/CardAnimator$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/CardAnimator$2;-><init>(Lcom/google/android/velvet/ui/CardAnimator;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/CardAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v0, :cond_0

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/CardAnimator;->setFloatValues([F)V

    :goto_0
    return-void

    :cond_0
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/CardAnimator;->setFloatValues([F)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/velvet/ui/CardAnimator;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/CardAnimator;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/CardAnimator;->onEnd()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/ui/CardAnimator;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/CardAnimator;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/CardAnimator;->doUpdate()V

    return-void
.end method

.method private configureTimings()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationIndex:I

    mul-int/lit8 v0, v0, 0x64

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/ui/CardAnimator;->setStartDelay(J)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_UP:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_DOWN:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    if-ne v0, v1, :cond_3

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x190

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/ui/CardAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    :goto_2
    return-void

    :cond_1
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/ui/CardAnimator;->setStartDelay(J)V

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0xc8

    goto :goto_1

    :cond_3
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/ui/CardAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_2
.end method

.method private doUpdate()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/CardAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sget-object v1, Lcom/google/android/velvet/ui/CardAnimator$3;->$SwitchMap$com$google$android$velvet$tg$SuggestionGridLayout$LayoutParams$AnimationType:[I

    iget-object v2, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-virtual {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/CardAnimator;->cancel()V

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/google/android/velvet/ui/CardAnimator;->updateDealAnimation(Landroid/view/View;F)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/google/android/velvet/ui/CardAnimator;->updateFadeAnimation(Landroid/view/View;F)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/velvet/ui/CardAnimator;->updateSlideAnimation(Landroid/view/View;FZ)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/velvet/ui/CardAnimator;->updateSlideAnimation(Landroid/view/View;FZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private onEnd()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setRotation(F)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v2, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/velvet/ui/CardAnimator;->mDisplayHeight:I

    int-to-float v0, v0

    goto :goto_0
.end method

.method private updateDealAnimation(Landroid/view/View;F)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # F

    const/high16 v6, -0x40800000

    const/high16 v5, 0x3f800000

    iget v3, p0, Lcom/google/android/velvet/ui/CardAnimator;->mDisplayHeight:I

    int-to-float v3, v3

    sub-float v4, v5, p2

    mul-float v2, v3, v4

    const/high16 v3, -0x3d380000

    sub-float v4, v5, p2

    mul-float v1, v3, v4

    iget-boolean v3, p0, Lcom/google/android/velvet/ui/CardAnimator;->fromRight:Z

    if-eqz v3, :cond_0

    mul-float/2addr v1, v6

    :cond_0
    const/high16 v3, 0x41a00000

    sub-float v4, v5, p2

    mul-float v0, v3, v4

    iget-boolean v3, p0, Lcom/google/android/velvet/ui/CardAnimator;->fromRight:Z

    if-eqz v3, :cond_1

    mul-float/2addr v0, v6

    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v5}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method private updateFadeAnimation(Landroid/view/View;F)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # F

    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method private updateSlideAnimation(Landroid/view/View;FZ)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # F
    .param p3    # Z

    const/high16 v5, 0x3f800000

    const/4 v3, 0x0

    mul-float v4, v5, p2

    add-float v0, v3, v4

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v3

    neg-int v3, v3

    int-to-float v1, v3

    :goto_0
    sub-float v3, v5, p2

    mul-float v2, v1, v3

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    return-void

    :cond_0
    iget v3, p0, Lcom/google/android/velvet/ui/CardAnimator;->mDisplayHeight:I

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v1, v3

    goto :goto_0
.end method

.method public static usesAlpha(Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_DOWN:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_UP:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setTarget(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v2, 0x1

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    :goto_0
    iput-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    :goto_1
    iput-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    :cond_0
    iget v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->animationIndex:I

    rem-int/lit8 v1, v1, 0x2

    if-ne v1, v2, :cond_5

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->fromRight:Z

    iget v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->animationIndex:I

    iput v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationIndex:I

    :goto_3
    iget-boolean v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    if-eq v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mTargetView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    :cond_2
    return-void

    :cond_3
    iget-object v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->disappearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    :cond_6
    iget-boolean v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_7

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    :goto_4
    iput-object v1, p0, Lcom/google/android/velvet/ui/CardAnimator;->mAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_3

    :cond_7
    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_4
.end method

.method public start()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/CardAnimator;->configureTimings()V

    invoke-super {p0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method
