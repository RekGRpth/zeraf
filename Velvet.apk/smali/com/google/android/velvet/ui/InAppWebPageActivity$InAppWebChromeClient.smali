.class Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "InAppWebPageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/InAppWebPageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InAppWebChromeClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebChromeClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;Lcom/google/android/velvet/ui/InAppWebPageActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/ui/InAppWebPageActivity;
    .param p2    # Lcom/google/android/velvet/ui/InAppWebPageActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebChromeClient;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V

    return-void
.end method


# virtual methods
.method public onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebChromeClient;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    # getter for: Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->setTitle(Ljava/lang/String;)V

    return-void
.end method
