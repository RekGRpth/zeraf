.class final Lcom/google/android/velvet/ui/util/Animations$1;
.super Ljava/lang/Object;
.source "Animations.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/util/Animations;->fadeScaleUpdateText(Landroid/widget/TextView;Ljava/lang/String;F)Landroid/view/ViewPropertyAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$newText:Ljava/lang/String;

.field final synthetic val$scaleFrom:F

.field final synthetic val$view:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;Ljava/lang/String;F)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$view:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$newText:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$scaleFrom:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/high16 v2, 0x3f800000

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$view:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$newText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$view:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$scaleFrom:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setScaleX(F)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$view:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$scaleFrom:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setScaleY(F)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/Animations$1;->val$view:Landroid/widget/TextView;

    # invokes: Lcom/google/android/velvet/ui/util/Animations;->getAnimator(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    invoke-static {v0}, Lcom/google/android/velvet/ui/util/Animations;->access$000(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    return-void
.end method
