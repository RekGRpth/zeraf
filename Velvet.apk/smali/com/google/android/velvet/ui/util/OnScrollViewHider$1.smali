.class Lcom/google/android/velvet/ui/util/OnScrollViewHider$1;
.super Ljava/lang/Object;
.source "OnScrollViewHider.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/util/OnScrollViewHider;-><init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/util/OnScrollViewHider;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/util/OnScrollViewHider;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider$1;->this$0:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I

    sub-int v0, p5, p3

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider$1;->this$0:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    # getter for: Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I
    invoke-static {v1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->access$000(Lcom/google/android/velvet/ui/util/OnScrollViewHider;)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider$1;->this$0:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->onViewHeightChanged(I)V

    :cond_0
    return-void
.end method
