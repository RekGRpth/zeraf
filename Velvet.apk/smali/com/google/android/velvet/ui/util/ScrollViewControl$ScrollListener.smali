.class public interface abstract Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;
.super Ljava/lang/Object;
.source "ScrollViewControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/util/ScrollViewControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ScrollListener"
.end annotation


# virtual methods
.method public abstract onScrollAnimationFinished()V
.end method

.method public abstract onScrollChanged(II)V
.end method

.method public abstract onScrollFinished()V
.end method

.method public abstract onScrollMarginConsumed(Landroid/view/View;II)V
.end method
