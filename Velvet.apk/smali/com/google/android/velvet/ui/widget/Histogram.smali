.class public Lcom/google/android/velvet/ui/widget/Histogram;
.super Landroid/view/View;
.source "Histogram.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/widget/Histogram$Bar;
    }
.end annotation


# instance fields
.field private mBarPadding:I

.field private mBarSize:I

.field private mBars:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ui/widget/Histogram$Bar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    sget-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->Histogram:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/widget/Histogram;->parseAttrs(Landroid/content/res/TypedArray;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    sget-object v0, Lcom/google/android/googlequicksearchbox/R$styleable;->Histogram:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/widget/Histogram;->parseAttrs(Landroid/content/res/TypedArray;)V

    return-void
.end method

.method private parseAttrs(Landroid/content/res/TypedArray;)V
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBarSize:I

    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBarPadding:I

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public addBar(FI)V
    .locals 3
    .param p1    # F
    .param p2    # I

    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    const/high16 v1, 0x3f800000

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    new-instance v2, Lcom/google/android/velvet/ui/widget/Histogram$Bar;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/velvet/ui/widget/Histogram$Bar;-><init>(Lcom/google/android/velvet/ui/widget/Histogram;FLandroid/graphics/Paint;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/Histogram;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/Histogram;->getWidth()I

    move-result v8

    const/4 v7, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/velvet/ui/widget/Histogram$Bar;

    iget v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBarSize:I

    iget v1, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBarPadding:I

    add-int/2addr v0, v1

    mul-int/2addr v0, v7

    int-to-float v2, v0

    iget v0, v6, Lcom/google/android/velvet/ui/widget/Histogram$Bar;->length:F

    int-to-float v1, v8

    mul-float v3, v0, v1

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBarSize:I

    int-to-float v0, v0

    add-float v4, v2, v0

    iget-object v5, v6, Lcom/google/android/velvet/ui/widget/Histogram$Bar;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBarSize:I

    mul-int v0, v1, v2

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBars:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/android/velvet/ui/widget/Histogram;->mBarPadding:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/Histogram;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/Histogram;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-static {v1, p1}, Lcom/google/android/velvet/ui/widget/Histogram;->getDefaultSize(II)I

    move-result v1

    invoke-static {v0, p2}, Lcom/google/android/velvet/ui/widget/Histogram;->getDefaultSize(II)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/velvet/ui/widget/Histogram;->setMeasuredDimension(II)V

    return-void
.end method
