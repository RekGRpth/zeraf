.class Lcom/google/android/velvet/Corpora$2;
.super Ljava/lang/Object;
.source "Corpora.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/Corpora;->maybeFetchNewCorpora()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/Corpora;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/Corpora;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/Corpora$2;->this$0:Lcom/google/android/velvet/Corpora;

    iput-object p2, p0, Lcom/google/android/velvet/Corpora$2;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$2;->val$url:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$2;->this$0:Lcom/google/android/velvet/Corpora;

    # getter for: Lcom/google/android/velvet/Corpora;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v3}, Lcom/google/android/velvet/Corpora;->access$000(Lcom/google/android/velvet/Corpora;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/SearchSettings;->setWebCorpora([B)V

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$2;->this$0:Lcom/google/android/velvet/Corpora;

    # getter for: Lcom/google/android/velvet/Corpora;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v3}, Lcom/google/android/velvet/Corpora;->access$000(Lcom/google/android/velvet/Corpora;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/SearchSettings;->setWebCorporaConfigUrl(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/google/android/velvet/Corpora$2;->this$0:Lcom/google/android/velvet/Corpora;

    # getter for: Lcom/google/android/velvet/Corpora;->mLoader:Lcom/google/android/searchcommon/util/UriLoader;
    invoke-static {v3}, Lcom/google/android/velvet/Corpora;->access$300(Lcom/google/android/velvet/Corpora;)Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/Corpora$2;->val$url:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    if-eqz v1, :cond_0

    array-length v3, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$2;->this$0:Lcom/google/android/velvet/Corpora;

    # getter for: Lcom/google/android/velvet/Corpora;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v3}, Lcom/google/android/velvet/Corpora;->access$000(Lcom/google/android/velvet/Corpora;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/Corpora$2;->val$url:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/SearchSettings;->setWebCorporaConfigUrl(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;->parseFrom([B)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$2;->this$0:Lcom/google/android/velvet/Corpora;

    # invokes: Lcom/google/android/velvet/Corpora;->buildWebCorpora(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;)V
    invoke-static {v3, v0}, Lcom/google/android/velvet/Corpora;->access$200(Lcom/google/android/velvet/Corpora;Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;)V

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$2;->this$0:Lcom/google/android/velvet/Corpora;

    # getter for: Lcom/google/android/velvet/Corpora;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v3}, Lcom/google/android/velvet/Corpora;->access$000(Lcom/google/android/velvet/Corpora;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/google/android/searchcommon/SearchSettings;->setWebCorpora([B)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "Velvet.Corpora"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed parsing corpora config at URL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/Corpora$2;->val$url:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v3, "Velvet.Corpora"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid corpora config at URL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/Corpora$2;->val$url:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
