.class Lcom/google/android/velvet/gallery/ImageMetadataController$1;
.super Ljava/lang/Object;
.source "ImageMetadataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/gallery/ImageMetadataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/gallery/ImageMetadataController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$1;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$1;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$1;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/velvet/gallery/ImageMetadataController;->access$002(Lcom/google/android/velvet/gallery/ImageMetadataController;Ljava/lang/String;)Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$1;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    # getter for: Lcom/google/android/velvet/gallery/ImageMetadataController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->access$100(Lcom/google/android/velvet/gallery/ImageMetadataController;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$1;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    # getter for: Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/velvet/gallery/ImageMetadataController;->access$200(Lcom/google/android/velvet/gallery/ImageMetadataController;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/velvet/gallery/ImageProvider;->reportMoreImagesAvailable(Landroid/content/Context;I)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
