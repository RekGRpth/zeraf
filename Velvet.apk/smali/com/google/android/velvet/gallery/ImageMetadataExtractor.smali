.class public Lcom/google/android/velvet/gallery/ImageMetadataExtractor;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "ImageMetadataExtractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;
    }
.end annotation


# instance fields
.field private final mCharactersBuilder:Ljava/lang/StringBuilder;

.field private final mResultHandler:Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;

.field private mWantCharacters:Z


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mWantCharacters:Z

    iput-object p1, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mResultHandler:Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mCharactersBuilder:Ljava/lang/StringBuilder;

    return-void
.end method

.method private getCharacters()Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mWantCharacters:Z

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-boolean v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mWantCharacters:Z

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mCharactersBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mCharactersBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    return-object v0
.end method

.method private static maybeLogExceptionDetails(Lorg/xml/sax/SAXParseException;Ljava/lang/String;)V
    .locals 10
    .param p0    # Lorg/xml/sax/SAXParseException;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/xml/sax/SAXParseException;->getLineNumber()I

    move-result v7

    add-int/lit8 v4, v7, -0x1

    invoke-virtual {p0}, Lorg/xml/sax/SAXParseException;->getColumnNumber()I

    move-result v0

    :try_start_0
    const-string v7, "\\n"

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    aget-object v3, v7, v4
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v5, v0, -0x32

    if-gez v5, :cond_0

    const/4 v5, 0x0

    :cond_0
    add-int/lit8 v1, v0, 0x32

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-le v1, v7, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    :cond_1
    invoke-virtual {v3, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v7, "ImageMetadataExtractor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not find text which caused the exception on line "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startGettingCharacters()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mWantCharacters:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-boolean v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mWantCharacters:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    iget-boolean v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mWantCharacters:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mCharactersBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mWantCharacters:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->getCharacters()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mResultHandler:Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;

    invoke-interface {v1, v0}, Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;->onMetadataSectionExtracted(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public declared-synchronized parseXml(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    const/4 v3, 0x0

    :try_start_0
    iput-boolean v3, p0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->mWantCharacters:Z

    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v2

    invoke-interface {v2, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    new-instance v3, Lorg/xml/sax/InputSource;

    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v2, v3}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v3, "ImageMetadataExtractor"

    const-string v4, "IOException fetching image metadata"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :catch_1
    move-exception v0

    :try_start_2
    const-string v3, "ImageMetadataExtractor"

    const-string v4, "SAX Parsing Exception fetching image metadata"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v0, p1}, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->maybeLogExceptionDetails(Lorg/xml/sax/SAXParseException;Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v3, "ImageMetadataExtractor"

    const-string v4, "SAXException fetching image metadata"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_3
    move-exception v0

    const-string v3, "ImageMetadataExtractor"

    const-string v4, "ParserConfigurationException fetching image metadata"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_4
    move-exception v1

    const-string v3, "ImageMetadataExtractor"

    const-string v4, "IllegalStateException fetching image metadata"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/Attributes;

    const-string v0, "gsamd"

    const-string v1, "id"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->startGettingCharacters()V

    :cond_0
    return-void
.end method
