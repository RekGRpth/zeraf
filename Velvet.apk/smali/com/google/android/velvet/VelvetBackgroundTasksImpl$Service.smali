.class public Lcom/google/android/velvet/VelvetBackgroundTasksImpl$Service;
.super Landroid/app/IntentService;
.source "VelvetBackgroundTasksImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/VelvetBackgroundTasksImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Service"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Velvet.VelvetBackgroundTasksImpl"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$Service;->setIntentRedelivery(Z)V

    return-void
.end method


# virtual methods
.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # invokes: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->serviceTasks()V
    invoke-static {v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$1200(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)V

    return-void
.end method
