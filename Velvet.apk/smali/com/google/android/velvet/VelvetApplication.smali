.class public Lcom/google/android/velvet/VelvetApplication;
.super Landroid/app/Application;
.source "VelvetApplication.java"

# interfaces
.implements Lcom/google/android/velvet/ActivityLifecycleNotifier;
.implements Lcom/google/android/velvet/ActivityLifecycleObserver;


# static fields
.field private static sThisPackageInfo:Landroid/content/pm/PackageInfo;


# instance fields
.field private mActivityLifecycleObservers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/velvet/ActivityLifecycleObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

.field private final mBackgroundInitLock:Ljava/lang/Object;

.field private mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field private mFactory:Lcom/google/android/velvet/VelvetFactory;

.field private mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

.field private mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

.field private mImageLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

.field private mLocationOracleLockForMariner:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

.field private mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private mReady:Z

.field private mSidekickAlarmsRegistered:Z

.field private mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

.field private mSidekickServicesStarted:Z

.field private mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mBackgroundInitLock:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/VelvetApplication;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/VelvetApplication;

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetApplication;->registerSidekickAlarms()V

    return-void
.end method

.method private createBackgroundLoader(Lcom/google/android/searchcommon/util/SynchronousLoader;)Lcom/google/android/searchcommon/util/UriLoader;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/searchcommon/util/SynchronousLoader",
            "<TA;>;)",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<TA;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/AsyncServices;->getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/util/BackgroundLoader;

    invoke-direct {v1, p1, v0}, Lcom/google/android/searchcommon/util/BackgroundLoader;-><init>(Lcom/google/android/searchcommon/util/SynchronousLoader;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    return-object v1
.end method

.method private createImageLoader()Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    .locals 10

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v1

    new-instance v5, Lcom/google/android/searchcommon/imageloader/NetworkImageLoader;

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v1, v6}, Lcom/google/android/searchcommon/imageloader/NetworkImageLoader;-><init>(Lcom/google/android/searchcommon/util/HttpHelper;Landroid/content/res/Resources;)V

    invoke-direct {p0, v5}, Lcom/google/android/velvet/VelvetApplication;->createUiThreadPostingBackgroundLoader(Lcom/google/android/searchcommon/util/SynchronousLoader;)Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v4

    new-instance v5, Lcom/google/android/searchcommon/imageloader/DataUriImageLoader;

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/searchcommon/imageloader/DataUriImageLoader;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {p0, v5}, Lcom/google/android/velvet/VelvetApplication;->createUiThreadPostingBackgroundLoader(Lcom/google/android/searchcommon/util/SynchronousLoader;)Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v0

    new-instance v5, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/searchcommon/AsyncServices;->getExclusiveBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v9

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    invoke-direct {p0, v5}, Lcom/google/android/velvet/VelvetApplication;->createUiThreadPostingLoader(Lcom/google/android/searchcommon/util/UriLoader;)Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v3

    new-instance v2, Lcom/google/android/searchcommon/imageloader/ImageLoader;

    invoke-direct {v2, v4, v0, v3}, Lcom/google/android/searchcommon/imageloader/ImageLoader;-><init>(Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/searchcommon/util/UriLoader;)V

    new-instance v5, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {v5, v2}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;-><init>(Lcom/google/android/searchcommon/util/UriLoader;)V

    return-object v5
.end method

.method private createUiThreadPostingBackgroundLoader(Lcom/google/android/searchcommon/util/SynchronousLoader;)Lcom/google/android/searchcommon/util/UriLoader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/searchcommon/util/SynchronousLoader",
            "<TA;>;)",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<TA;>;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/velvet/VelvetApplication;->createBackgroundLoader(Lcom/google/android/searchcommon/util/SynchronousLoader;)Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/VelvetApplication;->createUiThreadPostingLoader(Lcom/google/android/searchcommon/util/UriLoader;)Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v0

    return-object v0
.end method

.method private createUiThreadPostingLoader(Lcom/google/android/searchcommon/util/UriLoader;)Lcom/google/android/searchcommon/util/UriLoader;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<TA;>;)",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<TA;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/util/PostToExecutorLoader;

    invoke-direct {v1, v0, p1}, Lcom/google/android/searchcommon/util/PostToExecutorLoader;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/UriLoader;)V

    return-object v1
.end method

.method public static fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/VelvetApplication;

    return-object v0
.end method

.method private static declared-synchronized getPkgInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;
    .locals 6
    .param p0    # Landroid/content/Context;

    const-class v4, Lcom/google/android/velvet/VelvetApplication;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/google/android/velvet/VelvetApplication;->sThisPackageInfo:Landroid/content/pm/PackageInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    sput-object v1, Lcom/google/android/velvet/VelvetApplication;->sThisPackageInfo:Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    sget-object v3, Lcom/google/android/velvet/VelvetApplication;->sThisPackageInfo:Landroid/content/pm/PackageInfo;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v4

    return-object v3

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static getVersionCode(Landroid/content/Context;)I
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->getPkgInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    return v0
.end method

.method public static getVersionName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->getPkgInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "2.4.10.eclipse"

    :cond_0
    return-object v0
.end method

.method private initializeSidekick()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/inject/DefaultSidekickInjector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    return-void
.end method

.method private registerSidekickAlarms()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mBackgroundInitLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickAlarmsRegistered:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isTheGoogleDeployed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isUserOptedIn()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickAlarmsRegistered:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getPendingIntentFactory()Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->ensureScheduled(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getAlarmUtils()Lcom/google/android/apps/sidekick/AlarmUtils;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->registerRefreshAlarm(Landroid/content/Context;Lcom/google/android/apps/sidekick/AlarmUtils;Z)V

    invoke-static {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->startUpdateAlarm(Landroid/content/Context;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public addActivityLifecycleObserver(Lcom/google/android/velvet/ActivityLifecycleObserver;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/ActivityLifecycleObserver;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mActivityLifecycleObservers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public declared-synchronized awaitReadiness()V
    .locals 4

    monitor-enter p0

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/velvet/VelvetApplication;->mReady:Z

    if-nez v2, :cond_1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public createPreferenceController(Landroid/app/Activity;)Lcom/google/android/searchcommon/preferences/PreferenceController;
    .locals 20
    .param p1    # Landroid/app/Activity;

    new-instance v1, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/velvet/VelvetApplication;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v4}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/velvet/VelvetApplication;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v5}, Lcom/google/android/voicesearch/VoiceSearchServices;->getPersonalizationPrefManager()Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v8}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/speech/embedded/Greco3Container;->getDeviceClassSupplier()Lcom/google/common/base/Supplier;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v8}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v8}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSearchHistoryHelper()Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v19

    move-object/from16 v8, p1

    invoke-direct/range {v1 .. v19}, Lcom/google/android/searchcommon/preferences/SearchPreferenceControllerFactory;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/app/Activity;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/searchcommon/GlobalSearchServices;Landroid/database/DataSetObservable;Lcom/google/common/base/Supplier;Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/velvet/VelvetBackgroundTasks;Lcom/google/android/searchcommon/history/SearchHistoryHelper;Lcom/google/android/speech/utils/NetworkInformation;Lcom/google/android/searchcommon/MarinerOptInSettings;)V

    return-object v1
.end method

.method public getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    return-object v0
.end method

.method public getFactory()Lcom/google/android/velvet/VelvetFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    return-object v0
.end method

.method public getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    return-object v0
.end method

.method public getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/goggles/GogglesSettings;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/GogglesSettings;-><init>(Lcom/google/android/velvet/VelvetApplication;)V

    iput-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    return-object v0
.end method

.method public getImageLoader()Lcom/google/android/searchcommon/util/UriLoader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mImageLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    return-object v0
.end method

.method public getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v0

    return-object v0
.end method

.method public getMainActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/google/android/googlequicksearchbox/SearchActivity;

    return-object v0
.end method

.method public getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    return-object v0
.end method

.method public getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    return-object v0
.end method

.method public getVersionCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getVersionCodeString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    return-object v0
.end method

.method public maybeRegisterSidekickAlarms()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/AsyncServices;->getPooledBackgroundExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/VelvetApplication$2;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/VelvetApplication$2;-><init>(Lcom/google/android/velvet/VelvetApplication;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected declared-synchronized notifyApplicationReady()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/velvet/VelvetApplication;->mReady:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onActivityStart()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mActivityLifecycleObservers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ActivityLifecycleObserver;

    invoke-interface {v0}, Lcom/google/android/velvet/ActivityLifecycleObserver;->onActivityStart()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onActivityStop()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mActivityLifecycleObservers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ActivityLifecycleObserver;

    invoke-interface {v0}, Lcom/google/android/velvet/ActivityLifecycleObserver;->onActivityStop()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 6

    invoke-static {}, Lcom/google/android/velvet/VelvetStrictMode;->init()V

    invoke-static {}, Lcom/google/android/voicesearch/logger/EventLogger;->init()V

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    const/16 v2, 0x10

    invoke-static {v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mActivityLifecycleObservers:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-direct {v2, v0}, Lcom/google/android/searchcommon/GsaPreferenceController;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    new-instance v2, Lcom/google/android/searchcommon/AsyncServicesImpl;

    invoke-direct {v2}, Lcom/google/android/searchcommon/AsyncServicesImpl;-><init>()V

    iput-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    new-instance v2, Lcom/google/android/searchcommon/CoreSearchServicesImpl;

    invoke-direct {v2, v0, p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;-><init>(Landroid/content/Context;Lcom/google/android/velvet/VelvetApplication;)V

    iput-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->getDebugFeaturesLevel()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/searchcommon/debug/DebugFeatures;->setDebugLevel(I)V

    new-instance v2, Lcom/google/android/voicesearch/VoiceSearchServices;

    iget-object v3, p0, Lcom/google/android/velvet/VelvetApplication;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/VelvetApplication;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iget-object v5, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/google/android/voicesearch/VoiceSearchServices;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/CoreSearchServices;)V

    iput-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->init()V

    new-instance v1, Lcom/google/android/velvet/VelvetApplication$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/VelvetApplication$1;-><init>(Lcom/google/android/velvet/VelvetApplication;)V

    new-instance v2, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;

    iget-object v3, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iget-object v4, p0, Lcom/google/android/velvet/VelvetApplication;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-direct {v2, p0, v3, v4, v1}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/AsyncServices;Lcom/google/common/base/Supplier;)V

    iput-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetApplication;->createImageLoader()Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mImageLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetApplication;->initializeSidekick()V

    new-instance v2, Lcom/google/android/velvet/VelvetFactory;

    invoke-direct {v2, p0}, Lcom/google/android/velvet/VelvetFactory;-><init>(Lcom/google/android/velvet/VelvetApplication;)V

    iput-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->isTheGoogleDeployed()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->startTgServices()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->notifyApplicationReady()V

    const/16 v2, 0x11

    invoke-static {v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/android/velvet/VelvetStrictMode;->onStartupPoint(I)V

    return-void
.end method

.method public onTerminate()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->destroy()V

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->dispose()V

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    const/16 v0, 0xf

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3c

    if-le p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mImageLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->purge()V

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/GlobalSearchServices;->onTrimMemory(I)V

    :cond_1
    return-void
.end method

.method public removeActivityLifecycleObserver(Lcom/google/android/velvet/ActivityLifecycleObserver;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/ActivityLifecycleObserver;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->mActivityLifecycleObservers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public startTgServices()V
    .locals 4

    const/4 v2, 0x1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v1, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickServicesStarted:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->isTheGoogleDeployed()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isUserOptedIn()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mLocationOracleLockForMariner:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v1

    const-string v3, "mariner"

    invoke-interface {v1, v3}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->newRunningLock(Ljava/lang/String;)Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mLocationOracleLockForMariner:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mLocationOracleLockForMariner:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;->acquire()V

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getCalendarController()Lcom/google/android/apps/sidekick/calendar/CalendarController;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarController;->startCalendar(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->initializeFromStorage()V

    iput-boolean v2, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickServicesStarted:Z

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public stopTgServices()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.sidekick.TrafficIntentService.SHUTDOWN_ACTION"

    const-class v3, Lcom/google/android/apps/sidekick/TrafficIntentService;

    invoke-direct {v1, v2, v4, v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/VelvetApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.sidekick.notifications.SHUTDOWN"

    const-class v3, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v1, v2, v4, v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/VelvetApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mLocationOracleLockForMariner:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mLocationOracleLockForMariner:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;->release()V

    iput-object v4, p0, Lcom/google/android/velvet/VelvetApplication;->mLocationOracleLockForMariner:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getCalendarController()Lcom/google/android/apps/sidekick/calendar/CalendarController;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarController;->stopCalendar(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getAlarmUtils()Lcom/google/android/apps/sidekick/AlarmUtils;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->unregisterRefreshAlarm(Landroid/content/Context;Lcom/google/android/apps/sidekick/AlarmUtils;)V

    iput-boolean v5, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickServicesStarted:Z

    iget-object v1, p0, Lcom/google/android/velvet/VelvetApplication;->mBackgroundInitLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/velvet/VelvetApplication;->mSidekickAlarmsRegistered:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
