.class public Lcom/google/android/velvet/cards/WeatherCard$Builder;
.super Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;
.source "WeatherCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/WeatherCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mChanceOfPrecipitation:Ljava/lang/Integer;

.field private mConditions:Ljava/lang/String;

.field private mIcon:Landroid/net/Uri;

.field private mTemperature:Ljava/lang/Integer;

.field private mWindSpeed:Ljava/lang/Integer;

.field private mWindSpeedUnit:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mTemperature:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public addForecast(Ljava/lang/CharSequence;Landroid/net/Uri;II)Lcom/google/android/velvet/cards/WeatherCard$Builder;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/net/Uri;
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->addForecast(Ljava/lang/CharSequence;Landroid/net/Uri;II)Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;

    return-object p0
.end method

.method public bridge synthetic addForecast(Ljava/lang/CharSequence;Landroid/net/Uri;II)Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/net/Uri;
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/cards/WeatherCard$Builder;->addForecast(Ljava/lang/CharSequence;Landroid/net/Uri;II)Lcom/google/android/velvet/cards/WeatherCard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setChanceOfPrecipitation(I)Lcom/google/android/velvet/cards/WeatherCard$Builder;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mChanceOfPrecipitation:Ljava/lang/Integer;

    return-object p0
.end method

.method public setConditions(Ljava/lang/String;)Lcom/google/android/velvet/cards/WeatherCard$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mConditions:Ljava/lang/String;

    return-object p0
.end method

.method public setIcon(Landroid/net/Uri;)Lcom/google/android/velvet/cards/WeatherCard$Builder;
    .locals 0
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mIcon:Landroid/net/Uri;

    return-object p0
.end method

.method public setWindSpeed(ILjava/lang/String;)Lcom/google/android/velvet/cards/WeatherCard$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mWindSpeed:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mWindSpeedUnit:Ljava/lang/String;

    return-object p0
.end method

.method public update(Lcom/google/android/velvet/cards/WeatherCard;)V
    .locals 7
    .param p1    # Lcom/google/android/velvet/cards/WeatherCard;

    invoke-super {p0, p1}, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->update(Lcom/google/android/velvet/cards/WeatherCard;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mTemperature:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mIcon:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mConditions:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mChanceOfPrecipitation:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mWindSpeed:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/google/android/velvet/cards/WeatherCard$Builder;->mWindSpeedUnit:Ljava/lang/String;

    move-object v0, p1

    # invokes: Lcom/google/android/velvet/cards/WeatherCard;->setCurrentWeather(ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/velvet/cards/WeatherCard;->access$000(Lcom/google/android/velvet/cards/WeatherCard;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/velvet/cards/WeatherCard;->invalidate()V

    return-void
.end method
