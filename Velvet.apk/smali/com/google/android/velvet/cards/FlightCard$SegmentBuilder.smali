.class public Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;
.super Ljava/lang/Object;
.source "FlightCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/FlightCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SegmentBuilder"
.end annotation


# instance fields
.field private final mArrival:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

.field private final mDeparture:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

.field private mStatusCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mStatusCode:I

    new-instance v0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    invoke-direct {v0}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mDeparture:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    new-instance v0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    invoke-direct {v0}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mArrival:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;Lcom/google/android/velvet/cards/FlightCard;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;
    .param p1    # Lcom/google/android/velvet/cards/FlightCard;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->addTo(Lcom/google/android/velvet/cards/FlightCard;)V

    return-void
.end method

.method private addTo(Lcom/google/android/velvet/cards/FlightCard;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/cards/FlightCard;

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mDeparture:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    # getter for: Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mActual:Ljava/util/Calendar;
    invoke-static {v1}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->access$300(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;)Ljava/util/Calendar;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mDeparture:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    # getter for: Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mActual:Ljava/util/Calendar;
    invoke-static {v1}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->access$300(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;)Ljava/util/Calendar;

    move-result-object v0

    :goto_0
    iget v1, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mStatusCode:I

    iget-object v2, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mDeparture:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    # getter for: Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mAirportName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->access$500(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mArrival:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    # getter for: Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mAirportName:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->access$500(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/velvet/cards/FlightCard;->addSegment(ILjava/util/Calendar;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {p1, v1, v0, v2, v3}, Lcom/google/android/velvet/cards/FlightCard;->access$600(Lcom/google/android/velvet/cards/FlightCard;ILjava/util/Calendar;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mDeparture:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    # invokes: Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->addTo(Lcom/google/android/velvet/cards/FlightCard;Ljava/util/Calendar;)V
    invoke-static {v1, p1, v0}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->access$700(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;Lcom/google/android/velvet/cards/FlightCard;Ljava/util/Calendar;)V

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mArrival:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    # invokes: Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->addTo(Lcom/google/android/velvet/cards/FlightCard;Ljava/util/Calendar;)V
    invoke-static {v1, p1, v0}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->access$700(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;Lcom/google/android/velvet/cards/FlightCard;Ljava/util/Calendar;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mDeparture:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    # getter for: Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mScheduled:Ljava/util/Calendar;
    invoke-static {v1}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->access$400(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;)Ljava/util/Calendar;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public arrival()Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mArrival:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    return-object v0
.end method

.method public departure()Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mDeparture:Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    return-object v0
.end method

.method public setStatus(I)Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->mStatusCode:I

    return-object p0
.end method
