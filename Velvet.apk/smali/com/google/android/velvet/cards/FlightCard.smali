.class public Lcom/google/android/velvet/cards/FlightCard;
.super Landroid/widget/LinearLayout;
.source "FlightCard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/cards/FlightCard$StopBuilder;,
        Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;,
        Lcom/google/android/velvet/cards/FlightCard$Builder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

.field private mLabel:Landroid/widget/TextView;

.field private mNavigateButton:Landroid/widget/Button;

.field private mSegmentsTable:Landroid/widget/TableLayout;

.field private mUnusedStops:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/velvet/cards/FlightCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/velvet/cards/FlightCard;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/FlightCard;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/FlightCard;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/FlightCard;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/cards/FlightCard;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/FlightCard;
    .param p1    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/util/List;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/velvet/cards/FlightCard;->setFlightInfo(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/cards/FlightCard;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/FlightCard;

    invoke-direct {p0}, Lcom/google/android/velvet/cards/FlightCard;->removeSegments()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/velvet/cards/FlightCard;ILjava/util/Calendar;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/FlightCard;
    .param p1    # I
    .param p2    # Ljava/util/Calendar;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/cards/FlightCard;->addSegment(ILjava/util/Calendar;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/velvet/cards/FlightCard;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/FlightCard;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/util/Calendar;
    .param p6    # Ljava/util/Calendar;
    .param p7    # Ljava/util/Calendar;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/velvet/cards/FlightCard;->populateStop(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V

    return-void
.end method

.method private addSegment(ILjava/util/Calendar;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1    # I
    .param p2    # Ljava/util/Calendar;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    invoke-virtual {v8}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v8, 0x7f04003e

    iget-object v9, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    const/4 v10, 0x1

    invoke-virtual {v3, v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    add-int/lit8 v9, v2, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

    invoke-virtual {v8, p1, p2}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->getFlightStatus(ILjava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    add-int/lit8 v9, v2, 0x2

    invoke-virtual {v8, v9}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0d0121

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p3, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    add-int/lit8 v9, v2, 0x3

    invoke-virtual {v8, v9}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mUnusedStops:Ljava/util/Queue;

    invoke-interface {v8, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    add-int/lit8 v9, v2, 0x5

    invoke-virtual {v8, v9}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0d0122

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p4, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    add-int/lit8 v9, v2, 0x6

    invoke-virtual {v8, v9}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/google/android/velvet/cards/FlightCard;->mUnusedStops:Ljava/util/Queue;

    invoke-interface {v8, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static fixTimeZone(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0    # Ljava/lang/String;

    const/high16 v6, 0x42700000

    if-eqz p0, :cond_1

    const-string v5, "GMT+"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "GMT-"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/16 v5, 0x2e

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-lez v5, :cond_1

    const/4 v5, 0x3

    :try_start_0
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v5, 0x41c00000

    rem-float/2addr v3, v5

    float-to-int v1, v3

    mul-float v5, v3, v6

    rem-float/2addr v5, v6

    float-to-int v2, v5

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "GMT%c%d:%d"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    :cond_1
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    sget-object v5, Lcom/google/android/velvet/cards/FlightCard;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid time zone: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private init()V
    .locals 4

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/FlightCard;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v2, 0x7f02000b

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/FlightCard;->setBackgroundResource(I)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/FlightCard;->setOrientation(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f04003c

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    new-instance v2, Lcom/google/android/velvet/cards/FlightStatusFormatter;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/velvet/cards/FlightStatusFormatter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/velvet/cards/FlightCard;->mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

    const v2, 0x7f1000d0

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/FlightCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/velvet/cards/FlightCard;->mLabel:Landroid/widget/TextView;

    const v2, 0x7f1000d1

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/FlightCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TableLayout;

    iput-object v2, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    const v2, 0x7f1000d2

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/FlightCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/velvet/cards/FlightCard;->mNavigateButton:Landroid/widget/Button;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/velvet/cards/FlightCard;->mUnusedStops:Ljava/util/Queue;

    return-void
.end method

.method private static isDifferentDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 4
    .param p0    # Ljava/util/Calendar;
    .param p1    # Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x2

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private populateStop(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 25
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/util/Calendar;
    .param p6    # Ljava/util/Calendar;
    .param p7    # Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/cards/FlightCard;->mUnusedStops:Ljava/util/Queue;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/Queue;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_3

    const/16 v21, 0x1

    :goto_0
    invoke-static/range {v21 .. v21}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/cards/FlightCard;->mUnusedStops:Ljava/util/Queue;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup;

    const v21, 0x7f1000df

    invoke-virtual/range {v20 .. v21}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v21, 0x7f1000e1

    invoke-virtual/range {v20 .. v21}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v21, 0x7f1000e2

    invoke-virtual/range {v20 .. v21}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    if-eqz p6, :cond_6

    invoke-static/range {p6 .. p7}, Lcom/google/android/velvet/cards/FlightCard;->isDifferentDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v21

    if-eqz v21, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/cards/FlightCard;->mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

    move-object/from16 v21, v0

    const v22, 0x8001b

    move-object/from16 v0, v21

    move-object/from16 v1, p6

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->formatDateTime(Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    if-eqz p5, :cond_0

    invoke-virtual/range {p6 .. p6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual/range {p5 .. p5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    sub-long v9, v3, v16

    invoke-static {v9, v10}, Ljava/lang/Math;->abs(J)J

    move-result-wide v21

    const-wide/32 v23, 0xea60

    cmp-long v21, v21, v23

    if-lez v21, :cond_0

    move-object/from16 v0, p6

    move-object/from16 v1, p5

    invoke-static {v0, v1}, Lcom/google/android/velvet/cards/FlightCard;->isDifferentDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v21

    if-eqz v21, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/cards/FlightCard;->mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

    move-object/from16 v21, v0

    const v22, 0x8001b

    move-object/from16 v0, v21

    move-object/from16 v1, p5

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->formatDateTime(Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v11

    :goto_2
    const v21, 0x7f0d0129

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v11, v22, v23

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_3
    invoke-virtual {v15}, Landroid/widget/TextView;->getVisibility()I

    move-result v21

    if-nez v21, :cond_1

    const/16 v19, 0x0

    new-instance v14, Landroid/graphics/Paint;

    invoke-direct {v14}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v7}, Landroid/widget/TextView;->getTextSize()F

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v21

    add-float v19, v19, v21

    invoke-virtual {v5}, Landroid/widget/TextView;->getTextSize()F

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v21

    add-float v19, v19, v21

    invoke-virtual {v15}, Landroid/widget/TextView;->getTextSize()F

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v21

    add-float v19, v19, v21

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0c0083

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    add-float v19, v19, v21

    invoke-static {v8}, Lcom/google/android/velvet/util/LayoutUtils;->getCardWidth(Landroid/content/Context;)I

    move-result v6

    int-to-float v0, v6

    move/from16 v21, v0

    cmpl-float v21, v19, v21

    if-lez v21, :cond_1

    const v21, 0x7f1000e0

    invoke-virtual/range {v20 .. v21}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/LinearLayout;

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/widget/LinearLayout;->setOrientation(I)V

    :cond_1
    const v21, 0x7f1000e3

    invoke-virtual/range {v20 .. v21}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_8

    const/4 v13, 0x1

    :goto_4
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_9

    const/4 v12, 0x1

    :goto_5
    if-eqz v13, :cond_a

    if-eqz v12, :cond_a

    const v21, 0x7f0d0128

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p3, v22, v23

    const/16 v23, 0x1

    aput-object p4, v22, v23

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_6
    return-void

    :cond_3
    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/cards/FlightCard;->mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->formatTime(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/cards/FlightCard;->mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->formatTime(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_2

    :cond_6
    if-eqz p5, :cond_0

    move-object/from16 v0, p5

    move-object/from16 v1, p7

    invoke-static {v0, v1}, Lcom/google/android/velvet/cards/FlightCard;->isDifferentDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v21

    if-eqz v21, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/cards/FlightCard;->mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

    move-object/from16 v21, v0

    const v22, 0x8001b

    move-object/from16 v0, v21

    move-object/from16 v1, p5

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->formatDateTime(Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/cards/FlightCard;->mFormatter:Lcom/google/android/velvet/cards/FlightStatusFormatter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->formatTime(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_5

    :cond_a
    if-eqz v13, :cond_b

    const v21, 0x7f0d0126

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p3, v22, v23

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_b
    if-eqz v12, :cond_2

    const v21, 0x7f0d0127

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p4, v22, v23

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6
.end method

.method private removeSegments()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightCard;->mSegmentsTable:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    return-void
.end method

.method private setFlightInfo(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 14
    .param p1    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard;->mLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d011b

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p4, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard;->mNavigateButton:Landroid/widget/Button;

    invoke-static/range {p4 .. p4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard;->mNavigateButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    if-eqz p5, :cond_0

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f10002f

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/FlightCard;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {v1, v12, v0}, Lcom/google/android/apps/sidekick/MoonshineUtilities;->getEffectiveGmailReferenceAndSetText(Landroid/content/Context;Landroid/widget/Button;Ljava/util/List;)Lcom/google/geo/sidekick/Sidekick$GmailReference;

    move-result-object v13

    if-eqz v13, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v11

    new-instance v1, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v13}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->getEmailUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/cards/FlightCard;->mLabel:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v7, "FLIGHT_EMAIL"

    const-string v8, "mail"

    sget-object v9, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->GMAIL_URL_PREFIXES:[Ljava/lang/String;

    const/4 v10, 0x0

    move-object v6, p1

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    invoke-virtual {v12, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard;->mNavigateButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public setOnNavigateListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightCard;->mNavigateButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
