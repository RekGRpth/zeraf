.class public Lcom/google/android/velvet/prefetch/SearchResultPageCache;
.super Ljava/lang/Object;
.source "SearchResultPageCache.java"


# instance fields
.field private final mCache:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage;",
            ">;"
        }
    .end annotation
.end field

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mDownloading:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage;",
            ">;"
        }
    .end annotation
.end field

.field private mLastPrefetchTime:J

.field private final mLock:Ljava/lang/Object;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLock:Ljava/lang/Object;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLastPrefetchTime:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    iput-object p1, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mDownloading:Ljava/util/Queue;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/velvet/prefetch/SearchResultPage;)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/prefetch/SearchResultPage;

    :goto_0
    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getPrefetchCacheEntries()I

    move-result v4

    if-lt v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v3, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mDownloading:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/prefetch/SearchResultPage;

    invoke-interface {v2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->isComplete()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {v2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->isFailed()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_3
    invoke-interface {p1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->isComplete()Z

    move-result v3

    if-nez v3, :cond_6

    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mDownloading:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getPrefetchSimultaneousDownloads()I

    move-result v4

    if-lt v3, v4, :cond_5

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mDownloading:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/prefetch/SearchResultPage;

    if-eqz v1, :cond_4

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->isComplete()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->cancel()V

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mDownloading:Ljava/util/Queue;

    invoke-interface {v3, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :cond_6
    return-void
.end method

.method public clear()V
    .locals 4

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x0

    :try_start_0
    iput-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/prefetch/SearchResultPage;

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->isComplete()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->cancel()V

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mDownloading:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-array v2, v10, [Ljava/lang/Object;

    aput-object p1, v2, v8

    const-string v3, "SearchResultPageCache state:"

    aput-object v3, v2, v9

    invoke-static {p2, v2}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v2, v10, [Ljava/lang/Object;

    aput-object p1, v2, v8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "max cache entries: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getPrefetchCacheEntries()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {p2, v2}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x2

    :try_start_0
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mLastPrefetchTime: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLastPrefetchTime:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {p2, v2}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    const/4 v4, 0x1

    const-string v5, "mWaitingPrefetch:"

    aput-object v5, v2, v4

    invoke-static {p2, v2}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    if-nez v2, :cond_0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    const/4 v4, 0x1

    const-string v5, "  null"

    aput-object v5, v2, v4

    invoke-static {p2, v2}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v2, v12, [Ljava/lang/Object;

    aput-object p1, v2, v8

    const-string v3, "mDownloading, "

    aput-object v3, v2, v9

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mDownloading:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    const-string v3, " items, these should also be in mCache:"

    aput-object v3, v2, v11

    invoke-static {p2, v2}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mDownloading:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/prefetch/SearchResultPage;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    goto :goto_1

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, p2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_1
    new-array v2, v12, [Ljava/lang/Object;

    aput-object p1, v2, v8

    const-string v3, "mCache, "

    aput-object v3, v2, v9

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    const-string v3, " items:"

    aput-object v3, v2, v11

    invoke-static {p2, v2}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/prefetch/SearchResultPage;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    goto :goto_2

    :cond_2
    return-void
.end method

.method get(Lcom/google/android/velvet/Query;JZ)Lcom/google/android/velvet/prefetch/SearchResultPage;
    .locals 11
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # J
    .param p4    # Z

    if-eqz p4, :cond_0

    iget-object v8, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLock:Ljava/lang/Object;

    monitor-enter v8

    const/4 v7, 0x0

    :try_start_0
    iput-object v7, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v3, 0x0

    iget-object v7, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v7}, Lcom/google/android/searchcommon/SearchConfig;->getPrefetchTtlMillis()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/prefetch/SearchResultPage;

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->getFetchTimeMillis()J

    move-result-wide v4

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v2

    sub-long v7, p2, v4

    int-to-long v9, v6

    cmp-long v7, v7, v9

    if-lez v7, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :cond_2
    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->isFailed()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v8

    invoke-virtual {v7, v8, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->equivalentForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v7

    if-eqz v7, :cond_4

    move-object v3, v1

    goto :goto_0

    :cond_4
    if-eqz p4, :cond_1

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->isComplete()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    invoke-interface {v1}, Lcom/google/android/velvet/prefetch/SearchResultPage;->cancel()V

    goto :goto_0

    :cond_5
    return-object v3
.end method

.method prefetchWaitingPage(J)V
    .locals 3
    .param p1    # J

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    iput-wide p1, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLastPrefetchTime:J

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->add(Lcom/google/android/velvet/prefetch/SearchResultPage;)V

    invoke-interface {v0}, Lcom/google/android/velvet/prefetch/SearchResultPage;->fetch()V

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method setWaitingPage(Lcom/google/android/velvet/prefetch/SearchResultPage;J)J
    .locals 12
    .param p1    # Lcom/google/android/velvet/prefetch/SearchResultPage;
    .param p2    # J

    const-wide/16 v10, 0x0

    iget-object v7, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-wide v8, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLastPrefetchTime:J

    cmp-long v6, v8, v10

    if-lez v6, :cond_3

    cmp-long v6, p2, v10

    if-ltz v6, :cond_0

    iget-wide v8, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mLastPrefetchTime:J

    sub-long v4, p2, v8

    iget-object v6, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v6}, Lcom/google/android/searchcommon/SearchConfig;->getPrefetchThrottlePeriodMillis()I

    move-result v6

    int-to-long v8, v6

    sub-long v0, v8, v4

    :goto_0
    cmp-long v6, v0, v10

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    if-nez v6, :cond_1

    move-wide v2, v0

    :goto_1
    iput-object p1, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mWaitingPrefetch:Lcom/google/android/velvet/prefetch/SearchResultPage;

    monitor-exit v7

    return-wide v2

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_1

    :cond_2
    const-wide/16 v2, 0x0

    goto :goto_1

    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "{"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->mCache:Ljava/util/Queue;

    invoke-interface {v5}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    add-int/lit8 v1, v0, 0x1

    if-lez v0, :cond_0

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/velvet/prefetch/SearchResultPage;

    invoke-interface {v3}, Lcom/google/android/velvet/prefetch/SearchResultPage;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v5, "}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
