.class final Lcom/google/android/velvet/Query$1;
.super Ljava/lang/Object;
.source "Query.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/velvet/Query;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p1    # Landroid/os/Parcel;

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    # invokes: Lcom/google/android/velvet/Query;->fromParcel(Landroid/os/Parcel;)Lcom/google/android/velvet/Query;
    invoke-static {v0, p1}, Lcom/google/android/velvet/Query;->access$000(Lcom/google/android/velvet/Query;Landroid/os/Parcel;)Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/Query$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/velvet/Query;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/Query$1;->newArray(I)[Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method
