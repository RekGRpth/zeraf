.class public Lcom/google/android/velvet/Cookies;
.super Ljava/lang/Object;
.source "Cookies.java"


# instance fields
.field private final mCookieMan:Landroid/webkit/CookieManager;

.field private final mSyncMan:Landroid/webkit/CookieSyncManager;


# direct methods
.method public constructor <init>(Landroid/webkit/CookieSyncManager;Landroid/webkit/CookieManager;)V
    .locals 0
    .param p1    # Landroid/webkit/CookieSyncManager;
    .param p2    # Landroid/webkit/CookieManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/Cookies;->mSyncMan:Landroid/webkit/CookieSyncManager;

    iput-object p2, p0, Lcom/google/android/velvet/Cookies;->mCookieMan:Landroid/webkit/CookieManager;

    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/google/android/velvet/Cookies;
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    new-instance v0, Lcom/google/android/velvet/Cookies;

    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v1

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/Cookies;-><init>(Landroid/webkit/CookieSyncManager;Landroid/webkit/CookieManager;)V

    return-object v0
.end method


# virtual methods
.method public getCookie(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/Cookies;->mCookieMan:Landroid/webkit/CookieManager;

    invoke-virtual {v0, p1}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public removeAllCookies()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Cookies;->mCookieMan:Landroid/webkit/CookieManager;

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    return-void
.end method

.method public setCookiesFromHeaders(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "Set-Cookie"

    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/velvet/Cookies;->mCookieMan:Landroid/webkit/CookieManager;

    invoke-virtual {v3, p1, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public stopSync()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Cookies;->mSyncMan:Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->stopSync()V

    return-void
.end method

.method public sync()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Cookies;->mSyncMan:Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    return-void
.end method
