.class public final enum Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;
.super Ljava/lang/Enum;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnimationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

.field public static final enum DEAL:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

.field public static final enum FADE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

.field public static final enum NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

.field public static final enum SLIDE_DOWN:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

.field public static final enum SLIDE_UP:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    const-string v1, "DEAL"

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    const-string v1, "SLIDE_UP"

    invoke-direct {v0, v1, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_UP:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    const-string v1, "SLIDE_DOWN"

    invoke-direct {v0, v1, v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_DOWN:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    const-string v1, "FADE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->FADE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_UP:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_DOWN:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->FADE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->$VALUES:[Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;
    .locals 1

    const-class v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;
    .locals 1

    sget-object v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->$VALUES:[Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-virtual {v0}, [Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    return-object v0
.end method
