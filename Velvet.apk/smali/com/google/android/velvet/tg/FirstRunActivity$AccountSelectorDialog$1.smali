.class Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$1;
.super Landroid/widget/ArrayAdapter;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$1;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-super {p0, p1, v2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$1;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mEnabled:Z
    invoke-static {v2}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->access$000(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Z

    move-result v2

    if-nez v2, :cond_0

    const v2, 0x1020014

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x3eb33333

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    new-instance v2, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$1$1;

    invoke-direct {v2, p0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$1$1;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$1;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object v1
.end method
