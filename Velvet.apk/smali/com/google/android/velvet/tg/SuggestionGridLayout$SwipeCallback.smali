.class Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/velvet/tg/SwipeHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SwipeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;Lcom/google/android/velvet/tg/SuggestionGridLayout$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p2    # Lcom/google/android/velvet/tg/SuggestionGridLayout$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;-><init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;)V

    return-void
.end method


# virtual methods
.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v2

    if-ne v2, p1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # invokes: Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;
    invoke-static {v2, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$400(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    goto :goto_0
.end method

.method dragEnd(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v1

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->enableAnimation(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->enableAnimation(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$800(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # setter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v1, v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$502(Lcom/google/android/velvet/tg/SuggestionGridLayout;Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # setter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v1, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$202(Lcom/google/android/velvet/tg/SuggestionGridLayout;Z)Z

    invoke-virtual {p1, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->invalidate()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # invokes: Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$400(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$800(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    const/4 v10, 0x0

    const/high16 v11, 0x3f000000

    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v9}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$200(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Z

    move-result v9

    if-eqz v9, :cond_1

    move-object v5, v10

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    add-float/2addr v9, v11

    float-to-int v7, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    add-float/2addr v9, v11

    float-to-int v8, v9

    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v9}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-ltz v1, :cond_5

    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v9, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v9

    if-eqz v9, :cond_3

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$300(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$300(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v9

    if-eqz v9, :cond_2

    instance-of v9, v5, Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;

    if-eqz v9, :cond_4

    move-object v9, v5

    check-cast v9, Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;

    iget-object v11, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-interface {v9, v11, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;->isDismissableViewAtPosition(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/MotionEvent;)Z

    move-result v9

    if-nez v9, :cond_2

    :cond_4
    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # invokes: Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;
    invoke-static {v9, v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$400(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v9

    iget-boolean v9, v9, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDrag:Z

    if-eqz v9, :cond_2

    instance-of v9, v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;

    if-nez v9, :cond_0

    move-object v4, v2

    check-cast v4, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isExpanded()Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v9, v3, -0x1

    if-eq v6, v9, :cond_0

    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # setter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v9, v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$502(Lcom/google/android/velvet/tg/SuggestionGridLayout;Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # invokes: Lcom/google/android/velvet/tg/SuggestionGridLayout;->setupDragImage(Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)V
    invoke-static {v9, v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$600(Lcom/google/android/velvet/tg/SuggestionGridLayout;Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)V

    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v5

    goto/16 :goto_0

    :cond_5
    move-object v5, v10

    goto/16 :goto_0
.end method

.method public onBeginDrag(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$800(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # setter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v1, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$202(Lcom/google/android/velvet/tg/SuggestionGridLayout;Z)Z

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->invalidate()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # invokes: Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$400(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$800(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v5

    if-ne p1, v5, :cond_3

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$800(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    const/4 v6, 0x0

    # setter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    invoke-static {v5, v6}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$502(Lcom/google/android/velvet/tg/SuggestionGridLayout;Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v3, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # invokes: Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    invoke-static {v5, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$901(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1000(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1000(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;

    move-result-object v5

    invoke-interface {v5, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;->onViewsDismissed(Ljava/util/ArrayList;)V

    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    const/4 v6, 0x0

    # setter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v5, v6}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$202(Lcom/google/android/velvet/tg/SuggestionGridLayout;Z)Z

    if-eqz v3, :cond_2

    invoke-interface {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->shouldShowDismissTrail()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->showDismissTrail()V

    :cond_2
    :goto_2
    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->invalidate()V

    return-void

    :cond_3
    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # invokes: Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;
    invoke-static {v5, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$400(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$800(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    if-eqz v5, :cond_4

    invoke-interface {v3, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # invokes: Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    invoke-static {v5, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1101(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)V

    :cond_4
    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1000(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1000(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;

    move-result-object v5

    invoke-interface {v5, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;->onViewsDismissed(Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_5
    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1200(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public onDragCancelled(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public onSnapBackCompleted(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;->dragEnd(Landroid/view/View;)V

    return-void
.end method
