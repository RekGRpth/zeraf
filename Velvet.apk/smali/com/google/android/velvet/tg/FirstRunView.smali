.class public Lcom/google/android/velvet/tg/FirstRunView;
.super Landroid/widget/FrameLayout;
.source "FirstRunView.java"


# instance fields
.field private container:Landroid/view/ViewGroup;

.field private footer:Landroid/view/View;

.field private footerPadding:I

.field private scrollView:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f1000c4

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunView;->footer:Landroid/view/View;

    const v0, 0x7f1000b6

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunView;->scrollView:Landroid/view/ViewGroup;

    const v0, 0x7f1000b9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunView;->container:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/tg/FirstRunView;->footerPadding:I

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunView;->scrollView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusable(Z)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 21
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->footer:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-eqz v17, :cond_1

    const v17, 0x7f1000c5

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x4

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->container:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v12

    const/4 v6, 0x0

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v12, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->container:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-nez v17, :cond_3

    move-object v6, v11

    :cond_2
    if-eqz v6, :cond_0

    const/4 v13, 0x0

    const v17, 0x7f1000ce

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_4

    const v17, 0x7f1000cc

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v14, Landroid/graphics/Rect;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14}, Lcom/google/android/velvet/tg/FirstRunView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v18

    add-int v13, v17, v18

    :goto_2
    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->footer:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getHeight()I

    move-result v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->footerPadding:I

    move/from16 v17, v0

    add-int v15, v13, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/FirstRunView;->getHeight()I

    move-result v16

    add-int v17, v15, v7

    move/from16 v0, v17

    move/from16 v1, v16

    if-ge v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->footer:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/FirstRunView;->getWidth()I

    move-result v19

    add-int v20, v15, v7

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/view/View;->layout(IIII)V

    const v17, 0x7f1000c5

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x4

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->scrollView:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/FirstRunView;->getWidth()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v13}, Landroid/view/ViewGroup;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->scrollView:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/FirstRunView;->getWidth()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v13}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0

    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    :cond_4
    const v17, 0x7f1000ca

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_5

    new-instance v14, Landroid/graphics/Rect;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/google/android/velvet/tg/FirstRunView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v18

    add-int v13, v17, v18

    goto/16 :goto_2

    :cond_5
    new-instance v14, Landroid/graphics/Rect;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v14}, Lcom/google/android/velvet/tg/FirstRunView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v18

    add-int v13, v17, v18

    goto/16 :goto_2

    :cond_6
    const v17, 0x7f1000c5

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    if-lez v7, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->scrollView:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, v9, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v7, v0, :cond_0

    iput v7, v9, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunView;->scrollView:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v17, Lcom/google/android/velvet/tg/FirstRunView$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/velvet/tg/FirstRunView$1;-><init>(Lcom/google/android/velvet/tg/FirstRunView;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method
