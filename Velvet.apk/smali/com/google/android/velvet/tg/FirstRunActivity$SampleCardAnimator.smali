.class Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;
.super Ljava/lang/Object;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SampleCardAnimator"
.end annotation


# instance fields
.field private final mView:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->mView:Landroid/view/View;

    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/google/android/velvet/tg/FirstRunActivity$1;)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/velvet/tg/FirstRunActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;Ljava/lang/Runnable;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->dismissCard(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;Ljava/lang/Runnable;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->dealSoon(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;Ljava/lang/Runnable;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->dealCard(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->mView:Landroid/view/View;

    return-object v0
.end method

.method private dealCard(Ljava/lang/Runnable;)V
    .locals 4
    .param p1    # Ljava/lang/Runnable;

    new-instance v1, Lcom/google/android/velvet/ui/CardAnimator;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/velvet/ui/CardAnimator;-><init>(ZI)V

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->mView:Landroid/view/View;

    const v3, 0x7f1000ce

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    new-instance v2, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator$2;

    invoke-direct {v2, p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator$2;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    return-void
.end method

.method private dealSoon(Ljava/lang/Runnable;)V
    .locals 4
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->mView:Landroid/view/View;

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator$1;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private dismissCard(Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x7f050000

    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setStartDelay(J)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->mView:Landroid/view/View;

    const v2, 0x7f1000ce

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator$3;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    return-void
.end method
