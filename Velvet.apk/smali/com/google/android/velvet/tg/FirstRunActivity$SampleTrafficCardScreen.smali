.class Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;
.super Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SampleTrafficCardScreen"
.end annotation


# instance fields
.field private mPopulated:Z

.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;II)V
    .locals 8
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v0, 0x7f0d01c1

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f0d01bd

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f040006

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;ILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;->mPopulated:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/velvet/tg/FirstRunActivity$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;II)V

    return-void
.end method


# virtual methods
.method setup(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;->setup(Landroid/view/View;)V

    const v1, 0x7f1000ce

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;->mPopulated:Z

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->populateSampleCard(Landroid/view/View;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;->mPopulated:Z

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
