.class Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->bringToFrontAndCollapse(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iput-object p2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->val$v:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->val$v:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->val$v:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget-boolean v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->updateItemBackgrounds()V

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnStackChangeListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;
    invoke-static {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1600(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnStackChangeListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;
    invoke-static {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1600(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget-object v1, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;->onStackViewOrderChanged(Ljava/util/List;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setDisableClipping(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;->this$1:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->requestLayout()V

    goto :goto_0
.end method
