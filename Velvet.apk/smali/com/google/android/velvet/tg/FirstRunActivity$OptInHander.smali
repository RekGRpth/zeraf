.class Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;
.super Landroid/os/Handler;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OptInHander"
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;Landroid/accounts/Account;)V
    .locals 0
    .param p2    # Landroid/accounts/Account;

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;->mAccount:Landroid/accounts/Account;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V
    invoke-static {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$900(Lcom/google/android/velvet/tg/FirstRunActivity;Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->completeOptIn(Landroid/accounts/Account;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
