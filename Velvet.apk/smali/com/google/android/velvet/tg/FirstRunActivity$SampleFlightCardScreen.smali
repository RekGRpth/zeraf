.class Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;
.super Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SampleFlightCardScreen"
.end annotation


# instance fields
.field private mPopulated:Z

.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;II)V
    .locals 8
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v0, 0x7f0d01c2

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f0d01bd

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f04003b

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;ILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;->mPopulated:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/velvet/tg/FirstRunActivity$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;II)V

    return-void
.end method


# virtual methods
.method setup(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;->setup(Landroid/view/View;)V

    const v1, 0x7f1000ce

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/cards/FlightCard;

    iget-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;->mPopulated:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$1100(Lcom/google/android/velvet/tg/FirstRunActivity;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->populateSampleCard(Lcom/google/android/velvet/cards/FlightCard;Landroid/view/LayoutInflater;Lcom/google/android/searchcommon/util/Clock;Z)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;->mPopulated:Z

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/cards/FlightCard;->setVisibility(I)V

    return-void
.end method
