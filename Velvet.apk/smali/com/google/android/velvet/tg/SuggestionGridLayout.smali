.class public Lcom/google/android/velvet/tg/SuggestionGridLayout;
.super Landroid/view/ViewGroup;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/tg/SuggestionGridLayout$CardWrapper;,
        Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;,
        Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;,
        Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;,
        Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;,
        Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;,
        Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;,
        Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;,
        Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;
    }
.end annotation


# instance fields
.field private final mAnimatingViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mAppearTransitionCount:I

.field private mColCount:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field private mColWidth:I

.field private mContentWidth:I

.field private mDealingIndex:I

.field private mDragImageView:Landroid/widget/ImageView;

.field private mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

.field private final mGridItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;",
            ">;"
        }
    .end annotation
.end field

.field private mHitRect:Landroid/graphics/Rect;

.field private mHorizontalItemMargin:I

.field private mIsDragging:Z

.field private mItemBottoms:[I

.field private mLayoutTransition:Landroid/animation/LayoutTransition;

.field private mMaxColumnWidth:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field private mOnDismissListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;

.field private mOnStackChangeListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;

.field mReinitStackBitmap:Z

.field private mResources:Landroid/content/res/Resources;

.field private mStackBitmap:Landroid/graphics/Bitmap;

.field private mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

.field private mVerticalItemMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iput-boolean v8, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mIsDragging:Z

    iput-boolean v8, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mReinitStackBitmap:Z

    iput-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    iput v8, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDealingIndex:I

    iput v8, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAppearTransitionCount:I

    sget-object v5, Lcom/google/android/googlequicksearchbox/R$styleable;->SuggestionGridLayout:[I

    invoke-virtual {p1, p2, v5, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const v5, 0x7fffffff

    invoke-virtual {v0, v6, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mMaxColumnWidth:I

    invoke-virtual {v0, v8, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    iget v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    const/4 v5, 0x3

    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mVerticalItemMargin:I

    const/4 v5, 0x2

    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHorizontalItemMargin:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    invoke-direct {p0, v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->configureTransition(Landroid/animation/LayoutTransition;)V

    :cond_0
    invoke-virtual {p0, v8}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setClipToPadding(Z)V

    invoke-virtual {p0, v8}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setClipChildren(Z)V

    invoke-virtual {p0, v6}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setChildrenDrawingOrderEnabled(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v3

    new-instance v5, Lcom/google/android/velvet/tg/SwipeHelper;

    new-instance v6, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;

    invoke-direct {v6, p0, v7}, Lcom/google/android/velvet/tg/SuggestionGridLayout$SwipeCallback;-><init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;Lcom/google/android/velvet/tg/SuggestionGridLayout$1;)V

    int-to-float v7, v3

    invoke-direct {v5, v8, v6, v1, v7}, Lcom/google/android/velvet/tg/SwipeHelper;-><init>(ILcom/google/android/velvet/tg/SwipeHelper$Callback;FF)V

    iput-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mResources:Landroid/content/res/Resources;

    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    sget-object v5, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v5, v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    const/4 v6, -0x1

    invoke-super {p0, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;

    return-object v0
.end method

.method static synthetic access$108(Lcom/google/android/velvet/tg/SuggestionGridLayout;)I
    .locals 2
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAppearTransitionCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAppearTransitionCount:I

    return v0
.end method

.method static synthetic access$110(Lcom/google/android/velvet/tg/SuggestionGridLayout;)I
    .locals 2
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAppearTransitionCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAppearTransitionCount:I

    return v0
.end method

.method static synthetic access$1101(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;I)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;I)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1401(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/ViewGroup$LayoutParams;

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/content/res/Resources;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnStackChangeListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;

    return-object v0
.end method

.method static synthetic access$1701(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/ViewGroup$LayoutParams;

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->fadeIn(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mIsDragging:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/velvet/tg/SuggestionGridLayout;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mIsDragging:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/graphics/Rect;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/velvet/tg/SuggestionGridLayout;Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iput-object p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragStack:Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/velvet/tg/SuggestionGridLayout;Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setupDragImage(Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$901(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method private addNewCardsToDeal(Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    invoke-interface {p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    iget-object v3, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-static {v3}, Lcom/google/android/velvet/ui/CardAnimator;->usesAlpha(Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    iget v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDealingIndex:I

    iput v3, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->animationIndex:I

    iget v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDealingIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDealingIndex:I

    goto :goto_0

    :cond_1
    return-void
.end method

.method private configureTransition(Landroid/animation/LayoutTransition;)V
    .locals 7
    .param p1    # Landroid/animation/LayoutTransition;

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    const-wide/16 v3, 0x12c

    invoke-virtual {p1, v3, v4}, Landroid/animation/LayoutTransition;->setDuration(J)V

    invoke-virtual {p1, v5}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    invoke-virtual {p1, v6}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v0, Lcom/google/android/velvet/ui/CardAnimator;

    const/4 v3, 0x1

    invoke-direct {v0, v3, v2}, Lcom/google/android/velvet/ui/CardAnimator;-><init>(ZI)V

    invoke-virtual {p1, v5, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v4, 0x40200000

    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {p1, v5, v3}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    new-instance v1, Lcom/google/android/velvet/ui/CardAnimator;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/google/android/velvet/ui/CardAnimator;-><init>(ZI)V

    invoke-virtual {p1, v6, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v4, 0x3fc00000

    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {p1, v6, v3}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    new-instance v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$1;

    invoke-direct {v3, p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$1;-><init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;)V

    invoke-virtual {p1, v3}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    return-void
.end method

.method private fadeIn(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    invoke-interface {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private setupDragImage(Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)V
    .locals 8
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gt v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->initStackBitmap()V

    :cond_1
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->draw(Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/ImageView;->measure(II)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    iget v2, p1, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->left:I

    iget v3, p1, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->top:I

    iget v4, p1, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->left:I

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, p1, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->top:I

    iget-object v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setTranslationX(F)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setTranslationY(F)V

    return-void
.end method

.method private setupLayoutParams(Landroid/view/View;I)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I

    iget v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    if-lt p2, v2, :cond_0

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Column exceeds column count."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v1, v0

    :goto_0
    check-cast v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    const/4 v2, -0x2

    if-eq p2, v2, :cond_2

    iput p2, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    :cond_2
    iget-boolean v2, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    if-eqz v2, :cond_3

    iget v2, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    const-string v2, "SuggestionGridLayout"

    const-string v3, "only spanAllColumns views can have no padding"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v1

    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    goto :goto_0
.end method

.method private toggleStackExpansion(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1    # Landroid/view/MotionEvent;

    const/4 v8, 0x0

    const/high16 v10, 0x3f000000

    const/4 v7, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    if-ne v9, v7, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    add-float/2addr v9, v10

    float-to-int v5, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    add-float/2addr v9, v10

    float-to-int v6, v9

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_4

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v9}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    iget-object v9, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v9, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v2

    if-eqz v2, :cond_0

    instance-of v9, v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    if-eqz v9, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    iget v9, v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mCollapsedCardsTop:I

    if-le v6, v9, :cond_3

    iget v9, v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mCollapsedCardsBottom:I

    if-ge v6, v9, :cond_3

    invoke-virtual {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isExpanded()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->bringToFrontAndCollapse(Landroid/view/View;)V

    :goto_1
    return v7

    :cond_2
    invoke-virtual {v3, v7}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isCollapsible()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isExpanded()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v3, v8}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    goto :goto_1

    :cond_4
    move v7, v8

    goto :goto_1
.end method


# virtual methods
.method public addStackToColumn(Ljava/util/ArrayList;ILcom/google/android/velvet/tg/DismissTrailFactory;)V
    .locals 9
    .param p2    # I
    .param p3    # Lcom/google/android/velvet/tg/DismissTrailFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I",
            "Lcom/google/android/velvet/tg/DismissTrailFactory;",
            ")V"
        }
    .end annotation

    iget v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    if-lt p2, v1, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Column exceeds column count."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;-><init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/content/Context;Ljava/util/ArrayList;ILcom/google/android/velvet/tg/DismissTrailFactory;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    const/4 v1, -0x1

    invoke-super {p0, v8, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mReinitStackBitmap:Z

    return-void
.end method

.method public addView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v0, -0x2

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addViewWithIndexAndColumn(Landroid/view/View;II)V

    return-void
.end method

.method public addViewToColumn(Landroid/view/View;ILcom/google/android/velvet/tg/DismissTrailFactory;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Lcom/google/android/velvet/tg/DismissTrailFactory;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;I)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;

    invoke-direct {v0, p0, p1, p3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;-><init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;Lcom/google/android/velvet/tg/DismissTrailFactory;)V

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;)V

    invoke-super {p0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public addViewWithIndexAndColumn(Landroid/view/View;II)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;I)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;-><init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;Lcom/google/android/velvet/tg/DismissTrailFactory;)V

    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;)V

    invoke-super {p0, p1, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/view/View;
    .param p3    # J

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v0

    instance-of v4, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mIsDragging:Z

    if-nez v4, :cond_0

    iget v4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAppearTransitionCount:I

    if-nez v4, :cond_0

    move-object v3, v0

    check-cast v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->clippingDisabled()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/4 v1, 0x1

    invoke-virtual {v3, p2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->getChildClipRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v2

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    return v2
.end method

.method public generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 4

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 7
    .param p1    # I
    .param p2    # I

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return p2

    :cond_0
    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v1, p1, v0

    if-lt p2, v1, :cond_1

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    sub-int v6, p2, v1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result p2

    goto :goto_0

    :cond_1
    move v3, p2

    const/4 v2, 0x0

    :goto_1
    if-gt v2, v3, :cond_3

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v3, v3, 0x1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move p2, v3

    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    return v0
.end method

.method public getSwipeControlForSelfSwipingChild()Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

    return-object v0
.end method

.method initStackBitmap()V
    .locals 8

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    instance-of v7, v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    if-eqz v7, :cond_1

    move-object v7, v3

    check-cast v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v7}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isCollapsible()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getMeasuredWidth()I

    move-result v6

    invoke-interface {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v1

    if-le v6, v5, :cond_0

    move v5, v6

    :cond_0
    if-le v1, v4, :cond_1

    move v4, v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-lez v5, :cond_4

    if-lez v4, :cond_4

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-gt v5, v7, :cond_3

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    if-le v4, v7, :cond_4

    :cond_3
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    :cond_4
    return-void
.end method

.method public isViewLocallyVisible(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    instance-of v2, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    if-eqz v2, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isTopCard(Landroid/view/View;)Z

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/velvet/tg/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->toggleStackExpansion(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_1
    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    :goto_0
    return v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingTop()I

    move-result v11

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([II)V

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getMeasuredWidth()I

    move-result v10

    iget v11, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mContentWidth:I

    sub-int/2addr v10, v11

    div-int/lit8 v0, v10, 0x2

    iget-object v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_4

    iget-object v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    invoke-interface {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->isGone()Z

    move-result v10

    if-eqz v10, :cond_0

    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v7

    iget v10, v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_1

    const/4 v4, 0x0

    :goto_2
    iget-boolean v10, v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    if-eqz v10, :cond_2

    const/4 v6, 0x0

    :goto_3
    iget-object v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    aget v10, v10, v4

    iget v11, v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->topMargin:I

    add-int v9, v10, v11

    invoke-interface {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getMeasuredWidth()I

    move-result v10

    add-int v8, v6, v10

    invoke-interface {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v10

    add-int v1, v9, v10

    invoke-interface {v2, v6, v9, v8, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->gridLayout(IIII)V

    iget v10, v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_3

    iget-object v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    iget v11, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mVerticalItemMargin:I

    add-int/2addr v11, v1

    iget v12, v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v11, v12

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([II)V

    goto :goto_1

    :cond_1
    iget v4, v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    goto :goto_2

    :cond_2
    iget v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColWidth:I

    iget v11, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHorizontalItemMargin:I

    add-int/2addr v10, v11

    mul-int/2addr v10, v4

    add-int v6, v0, v10

    goto :goto_3

    :cond_3
    iget-object v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    iget v11, v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    iget v12, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mVerticalItemMargin:I

    add-int/2addr v12, v1

    iget v13, v7, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v12, v13

    aput v12, v10, v11

    goto :goto_1

    :cond_4
    iget-boolean v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mReinitStackBitmap:Z

    if-eqz v10, :cond_5

    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mReinitStackBitmap:Z

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->initStackBitmap()V

    :cond_5
    const/4 v10, 0x0

    iput v10, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDealingIndex:I

    return-void
.end method

.method protected onMeasure(II)V
    .locals 30
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v24

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v25

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    const/16 v23, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mHorizontalItemMargin:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    move/from16 v27, v0

    add-int/lit8 v27, v27, -0x1

    mul-int v18, v26, v27

    sparse-switch v24, :sswitch_data_0

    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    move/from16 v26, v0

    mul-int v26, v26, v8

    add-int v26, v26, v18

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mContentWidth:I

    move-object/from16 v0, p0

    iput v8, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColWidth:I

    const/high16 v26, 0x40000000

    move/from16 v0, v26

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mContentWidth:I

    move/from16 v26, v0

    const/high16 v27, 0x40000000

    invoke-static/range {v26 .. v27}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v26, 0x40000000

    move/from16 v0, v23

    move/from16 v1, v26

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Ljava/util/Arrays;->fill([II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v12, 0x0

    const/16 v16, 0x0

    :goto_1
    move/from16 v0, v16

    if-ge v0, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    invoke-interface {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->isGone()Z

    move-result v26

    if-eqz v26, :cond_1

    :cond_0
    :goto_2
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    :sswitch_0
    move/from16 v23, v25

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingLeft()I

    move-result v26

    sub-int v26, v23, v26

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingRight()I

    move-result v27

    sub-int v20, v26, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mMaxColumnWidth:I

    move/from16 v26, v0

    sub-int v27, v20, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    move/from16 v28, v0

    div-int v27, v27, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->min(II)I

    move-result v8

    goto/16 :goto_0

    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingLeft()I

    move-result v26

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingRight()I

    move-result v27

    add-int v21, v26, v27

    sub-int v20, v25, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mMaxColumnWidth:I

    move/from16 v26, v0

    sub-int v27, v20, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    move/from16 v28, v0

    div-int v27, v27, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->min(II)I

    move-result v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    move/from16 v26, v0

    mul-int v26, v26, v8

    add-int v26, v26, v18

    add-int v23, v26, v21

    goto/16 :goto_0

    :sswitch_2
    new-instance v26, Ljava/lang/IllegalArgumentException;

    const-string v27, "Cannot measure SuggestionGridLayout with mode UNSPECIFIED"

    invoke-direct/range {v26 .. v27}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v26

    :cond_1
    invoke-interface {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v17

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move-object/from16 v0, v17

    iget v11, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    const/16 v26, -0x1

    move/from16 v0, v26

    if-ne v11, v0, :cond_5

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    move/from16 v26, v0

    if-eqz v26, :cond_4

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->inheritPadding:Z

    move/from16 v26, v0

    if-eqz v26, :cond_2

    invoke-interface {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->inheritPadding()V

    :cond_2
    move v9, v2

    :goto_3
    const/4 v11, 0x0

    :goto_4
    add-int/lit8 v26, v5, -0x1

    move/from16 v0, v16

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->fillViewport:Z

    move/from16 v26, v0

    if-eqz v26, :cond_3

    move-object v12, v4

    :cond_3
    invoke-interface {v4, v9, v6}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->gridMeasure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v26, v0

    aget v27, v26, v11

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->topMargin:I

    move/from16 v28, v0

    invoke-interface {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v29

    add-int v28, v28, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    move/from16 v29, v0

    add-int v28, v28, v29

    add-int v27, v27, v28

    aput v27, v26, v11

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    move/from16 v26, v0

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v27, v0

    aget v27, v27, v11

    invoke-static/range {v26 .. v27}, Ljava/util/Arrays;->fill([II)V

    goto/16 :goto_2

    :cond_4
    move v9, v3

    goto :goto_3

    :cond_5
    move/from16 v9, v22

    goto :goto_4

    :cond_6
    move v13, v15

    const/high16 v26, 0x40000000

    move/from16 v0, v26

    if-eq v14, v0, :cond_9

    const/16 v19, 0x0

    const/16 v16, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mColCount:I

    move/from16 v26, v0

    move/from16 v0, v16

    move/from16 v1, v26

    if-ge v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v26, v0

    aget v26, v26, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v27, v0

    sub-int v10, v26, v27

    move/from16 v0, v19

    if-le v10, v0, :cond_7

    move/from16 v19, v10

    :cond_7
    add-int/lit8 v16, v16, 0x1

    goto :goto_5

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingTop()I

    move-result v26

    add-int v26, v26, v19

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingBottom()I

    move-result v27

    add-int v13, v26, v27

    :cond_9
    if-eqz v12, :cond_a

    invoke-interface {v12}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    move/from16 v26, v0

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_b

    const/4 v11, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v26, v0

    aget v26, v26, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    invoke-interface {v12}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v27

    sub-int v7, v26, v27

    invoke-interface {v12}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getMeasuredWidth()I

    move-result v26

    const/high16 v27, 0x40000000

    invoke-static/range {v26 .. v27}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    sub-int v27, v13, v7

    const/high16 v28, 0x40000000

    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-interface {v12, v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->gridMeasure(II)V

    :cond_a
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1, v13}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setMeasuredDimension(II)V

    return-void

    :cond_b
    move-object/from16 v0, v17

    iget v11, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    goto :goto_6

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/tg/SwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public releaseStackBitmap()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mReinitStackBitmap:Z

    return-void
.end method

.method public removeAllViews()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public removeGridItem(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mSwiper:Lcom/google/android/velvet/tg/SwipeHelper;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SwipeHelper;->cancelOngoingDrag()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    invoke-interface {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    if-eq p1, v1, :cond_2

    const-string v1, "SuggestionGridLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeGridItem with non-grid item "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    return-void
.end method

.method public resetChildDismissState(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    const/high16 v0, 0x3f800000

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method public setLayoutTransitionStartDelay(IJ)V
    .locals 3
    .param p1    # I
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    :cond_0
    return-void
.end method

.method public setLayoutTransitionsEnabled(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnDismissListener(Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;

    iput-object p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;

    return-void
.end method

.method public setOnStackChangeListener(Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;

    iput-object p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mOnStackChangeListener:Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;

    return-void
.end method

.method public setVerticalItemMargin(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mVerticalItemMargin:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout;->mVerticalItemMargin:I

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->requestLayout()V

    :cond_0
    return-void
.end method
