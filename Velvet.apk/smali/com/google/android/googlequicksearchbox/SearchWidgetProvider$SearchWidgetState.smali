.class Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;
.super Ljava/lang/Object;
.source "SearchWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchWidgetState"
.end annotation


# instance fields
.field private final mAppWidgetId:I

.field private mQueryTextViewIntent:Landroid/content/Intent;

.field private mVoiceSearchIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->mAppWidgetId:I

    return-void
.end method

.method private setOnClickActivityIntent(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/RemoteViews;
    .param p3    # I
    .param p4    # Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-static {p1, v1, p4, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method public setQueryTextViewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->mQueryTextViewIntent:Landroid/content/Intent;

    return-void
.end method

.method public setVoiceSearchIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->mVoiceSearchIntent:Landroid/content/Intent;

    return-void
.end method

.method public updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetManager;

    const v3, 0x7f10020f

    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0400ae

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v1, 0x7f10020e

    iget-object v2, p0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->mQueryTextViewIntent:Landroid/content/Intent;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->setOnClickActivityIntent(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->mVoiceSearchIntent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    const v1, 0x7f100210

    iget-object v2, p0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->mVoiceSearchIntent:Landroid/content/Intent;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->setOnClickActivityIntent(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/content/Intent;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_0
    iget v1, p0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider$SearchWidgetState;->mAppWidgetId:I

    invoke-virtual {p2, v1, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method
