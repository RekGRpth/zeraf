.class public Lcom/google/android/ears/EarsContentProviderHelper;
.super Ljava/lang/Object;
.source "EarsContentProviderHelper.java"


# static fields
.field private static final CONTENT_URI:Landroid/net/Uri;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "heard"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.ears.heard.EarsContentProvider"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/ears/EarsContentProviderHelper;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/pm/PackageManager;)V
    .locals 0
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/ears/EarsContentProviderHelper;->mContentResolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/google/android/ears/EarsContentProviderHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method private createContentValues(JLcom/google/audio/ears/proto/EarsService$EarsResult;ZZLjava/lang/String;)Landroid/content/ContentValues;
    .locals 6
    .param p1    # J
    .param p3    # Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Ljava/lang/String;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p3}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getMusicResult()Lcom/google/audio/ears/proto/EarsService$MusicResult;

    move-result-object v1

    const-string v3, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "resultType"

    invoke-static {p3}, Lcom/google/android/ears/EarsResultParser;->getResultTypeInt(Lcom/google/audio/ears/proto/EarsService$EarsResult;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "deleted"

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "synced"

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "refId"

    invoke-virtual {p3}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getReferenceId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "album"

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "albumArtUrl"

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbumArtUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "signedInAlbumArtUrl"

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getSignedInAlbumArtUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "artist"

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtist()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtistId()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "artistId"

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtistId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v3, "track"

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getTrack()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/ears/EarsResultParser;->getGoogleMusicProductOffer(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v3, "productId"

    invoke-virtual {v0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "productParentId"

    invoke-virtual {v0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getParentIdentifier()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/ears/EarsContentProviderHelper;->shouldIncludeCountry()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "countryCode"

    invoke-virtual {v2, v3, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-object v2
.end method


# virtual methods
.method public insertHeardMatch(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)J
    .locals 7
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v5, 0x3e8

    mul-long v2, v0, v5

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ears/EarsContentProviderHelper;->insertHeardMatch(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;JZZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public insertHeardMatch(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;JZZ)J
    .locals 9
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
    .param p2    # J
    .param p4    # Z
    .param p5    # Z

    invoke-virtual {p1}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getResultList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/ears/EarsResultParser;->getFirstEarsResultWithMusic(Ljava/util/List;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasMusicResult()Z

    move-result v0

    if-nez v0, :cond_1

    const-wide/16 p2, 0x0

    :cond_0
    :goto_0
    return-wide p2

    :cond_1
    invoke-virtual {p1}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getDetectedCountryCode()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-wide v1, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ears/EarsContentProviderHelper;->createContentValues(JLcom/google/audio/ears/proto/EarsService$EarsResult;ZZLjava/lang/String;)Landroid/content/ContentValues;

    move-result-object v8

    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/ears/EarsContentProviderHelper;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/ears/EarsContentProviderHelper;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    :goto_1
    if-nez v7, :cond_0

    const-wide/16 p2, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method shouldIncludeCountry()Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/ears/EarsContentProviderHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v4, "com.google.android.ears"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v4, 0xa

    if-lt v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    :goto_0
    return v2

    :catch_0
    move-exception v0

    goto :goto_0
.end method
