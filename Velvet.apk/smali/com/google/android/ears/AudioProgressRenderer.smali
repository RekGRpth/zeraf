.class public Lcom/google/android/ears/AudioProgressRenderer;
.super Landroid/view/View;
.source "AudioProgressRenderer.java"


# instance fields
.field private mAnimationStartTimeInMs:J

.field private final mAnimator:Landroid/animation/TimeAnimator;

.field private mBarCount:I

.field private mBarDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/drawable/ClipDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mBarFullDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mBlueBar:Landroid/graphics/drawable/Drawable;

.field private mCurrentMicReading:I

.field private final mEmptyBar:Landroid/graphics/drawable/Drawable;

.field private mInitialized:Z

.field private mLevelArray:[I

.field private mMicReadings:[I

.field private mScaledBarWidth:I

.field private mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ears/AudioProgressRenderer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/ears/AudioProgressRenderer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mBarFullDrawables:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mBarDrawables:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mInitialized:Z

    invoke-virtual {p0}, Lcom/google/android/ears/AudioProgressRenderer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02015e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mEmptyBar:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/ears/AudioProgressRenderer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02015d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mBlueBar:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x12c

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mMicReadings:[I

    new-instance v0, Landroid/animation/TimeAnimator;

    invoke-direct {v0}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/animation/TimeAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    return-void
.end method

.method private getBarHeightPixels()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mEmptyBar:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method private getBarLevelFromVolume(I)I
    .locals 2
    .param p1    # I

    const/16 v0, 0x457

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v1, 0x2710

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    div-int/lit16 v0, p1, 0x457

    mul-int/lit16 v0, v0, 0x457

    return v0
.end method

.method private getBarWidthPixels()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mEmptyBar:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method private getMeanVolumeFromReadings(I)I
    .locals 8
    .param p1    # I

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/ears/AudioProgressRenderer;->mMicReadings:[I

    array-length v6, v6

    iget v7, p0, Lcom/google/android/ears/AudioProgressRenderer;->mBarCount:I

    div-int v2, v6, v7

    const/4 v3, 0x0

    add-int/lit8 v6, p1, -0x1

    mul-int/2addr v6, v2

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v6, p0, Lcom/google/android/ears/AudioProgressRenderer;->mMicReadings:[I

    array-length v6, v6

    mul-int v7, p1, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    iget v6, p0, Lcom/google/android/ears/AudioProgressRenderer;->mCurrentMicReading:I

    if-ge v0, v6, :cond_0

    iget-object v6, p0, Lcom/google/android/ears/AudioProgressRenderer;->mMicReadings:[I

    aget v6, v6, v0

    add-int/2addr v3, v6

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sub-int v6, v4, v1

    if-nez v6, :cond_2

    :goto_1
    return v5

    :cond_2
    sub-int v5, v4, v1

    div-int v5, v3, v5

    goto :goto_1
.end method

.method private getVolume()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v0}, Lcom/google/android/speech/SpeechLevelSource;->getSpeechLevel()I

    move-result v0

    mul-int/lit8 v0, v0, 0x64

    goto :goto_0
.end method

.method private setSize(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const-wide/high16 v6, 0x3ff0000000000000L

    int-to-double v2, p2

    mul-double/2addr v2, v6

    invoke-direct {p0}, Lcom/google/android/ears/AudioProgressRenderer;->getBarHeightPixels()I

    move-result v4

    int-to-double v4, v4

    div-double v0, v2, v4

    invoke-direct {p0}, Lcom/google/android/ears/AudioProgressRenderer;->getBarWidthPixels()I

    move-result v2

    iput v2, p0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    int-to-double v2, p1

    mul-double/2addr v2, v6

    iget v4, p0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/ears/AudioProgressRenderer;->mBarCount:I

    iget v2, p0, Lcom/google/android/ears/AudioProgressRenderer;->mBarCount:I

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/google/android/ears/AudioProgressRenderer;->mLevelArray:[I

    return-void
.end method

.method private startAnimator()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    :cond_0
    return-void
.end method

.method private stopAnimator()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    return-void
.end method


# virtual methods
.method public drawCurrentAnimation(Landroid/graphics/Canvas;)V
    .locals 26
    .param p1    # Landroid/graphics/Canvas;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    const/16 v20, 0x3a98

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarCount:I

    move/from16 v21, v0

    div-int v14, v20, v21

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimationStartTimeInMs:J

    move-wide/from16 v20, v0

    sub-long v20, v6, v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mMicReadings:[I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    div-int/lit8 v21, v17, 0x32

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->min(II)I

    move-result v13

    invoke-direct/range {p0 .. p0}, Lcom/google/android/ears/AudioProgressRenderer;->getVolume()I

    move-result v19

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/ears/AudioProgressRenderer;->mCurrentMicReading:I

    :goto_0
    if-gt v11, v13, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mMicReadings:[I

    move-object/from16 v20, v0

    aput v19, v20, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v20, v13, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/ears/AudioProgressRenderer;->mCurrentMicReading:I

    div-int v5, v17, v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v5, v0, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarCount:I

    move/from16 v20, v0

    add-int/lit8 v5, v20, -0x1

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarDrawables:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v11

    :goto_1
    if-gt v11, v5, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/ears/AudioProgressRenderer;->getMeanVolumeFromReadings(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBlueBar:Landroid/graphics/drawable/Drawable;

    new-instance v4, Landroid/graphics/drawable/ClipDrawable;

    const/16 v20, 0x50

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v4, v9, v0, v1}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mLevelArray:[I

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/ears/AudioProgressRenderer;->getBarLevelFromVolume(I)I

    move-result v21

    aput v21, v20, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarDrawables:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarFullDrawables:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_2
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    const/4 v11, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v11, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mEmptyBar:Landroid/graphics/drawable/Drawable;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v21, v0

    mul-int v21, v21, v11

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v23, v0

    mul-int v23, v23, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v24, v0

    add-int v23, v23, v24

    invoke-direct/range {p0 .. p0}, Lcom/google/android/ears/AudioProgressRenderer;->getBarHeightPixels()I

    move-result v24

    invoke-virtual/range {v20 .. v24}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mEmptyBar:Landroid/graphics/drawable/Drawable;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_3
    const/4 v11, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarDrawables:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v11, v0, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarFullDrawables:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v20, v0

    mul-int v20, v20, v11

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v22, v0

    mul-int v22, v22, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v23, v0

    add-int v22, v22, v23

    invoke-direct/range {p0 .. p0}, Lcom/google/android/ears/AudioProgressRenderer;->getBarHeightPixels()I

    move-result v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mBarDrawables:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/ClipDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mLevelArray:[I

    move-object/from16 v20, v0

    aget v12, v20, v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimationStartTimeInMs:J

    move-wide/from16 v20, v0

    mul-int v22, v11, v14

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v15, v20, v22

    sub-long v20, v6, v15

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v18, v0

    const/16 v20, 0x12c

    move/from16 v0, v18

    move/from16 v1, v20

    if-ge v0, v1, :cond_5

    mul-int v20, v12, v18

    move/from16 v0, v20

    div-int/lit16 v12, v0, 0x12c

    :cond_4
    :goto_4
    invoke-virtual {v4, v12}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v20, v0

    mul-int v20, v20, v11

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v22, v0

    mul-int v22, v22, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ears/AudioProgressRenderer;->mScaledBarWidth:I

    move/from16 v23, v0

    add-int v22, v22, v23

    invoke-direct/range {p0 .. p0}, Lcom/google/android/ears/AudioProgressRenderer;->getBarHeightPixels()I

    move-result v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/drawable/ClipDrawable;->setBounds(IIII)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/ClipDrawable;->draw(Landroid/graphics/Canvas;)V

    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    :cond_5
    const/16 v20, 0x14b4

    move/from16 v0, v18

    move/from16 v1, v20

    if-ge v0, v1, :cond_4

    move/from16 v0, v18

    add-int/lit16 v0, v0, -0x12c

    move/from16 v18, v0

    const-wide v20, 0x408f400000000000L

    const-wide/high16 v22, 0x4000000000000000L

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    const-wide/high16 v24, 0x4000000000000000L

    mul-double v22, v22, v24

    const-wide v24, 0x400921fb54442d18L

    mul-double v22, v22, v24

    const-wide v24, 0x408f400000000000L

    div-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    mul-double v20, v20, v22

    const-wide v22, 0x3fe6666666666666L

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    const-wide v24, 0x408f400000000000L

    div-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->exp(D)D

    move-result-wide v22

    div-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->round(D)J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v20, v0

    add-int v12, v12, v20

    goto/16 :goto_4

    :cond_6
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    new-instance v1, Lcom/google/android/ears/AudioProgressRenderer$1;

    invoke-direct {v1, p0}, Lcom/google/android/ears/AudioProgressRenderer$1;-><init>(Lcom/google/android/ears/AudioProgressRenderer;)V

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimator:Landroid/animation/TimeAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mInitialized:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/ears/AudioProgressRenderer;->drawCurrentAnimation(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;

    instance-of v1, p1, Landroid/os/Bundle;

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "AudioProgressRenderer.animationStartTimeMs"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimationStartTimeInMs:J

    const-string v1, "AudioProgressRenderer.micReadingsArray"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/ears/AudioProgressRenderer;->mMicReadings:[I

    const-string v1, "AudioProgressRenderer.currentMicReading"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/ears/AudioProgressRenderer;->mCurrentMicReading:I

    const-string v1, "parentState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "parentState"

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "AudioProgressRenderer.animationStartTimeMs"

    iget-wide v2, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimationStartTimeInMs:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "AudioProgressRenderer.micReadingsArray"

    iget-object v2, p0, Lcom/google/android/ears/AudioProgressRenderer;->mMicReadings:[I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string v1, "AudioProgressRenderer.currentMicReading"

    iget v2, p0, Lcom/google/android/ears/AudioProgressRenderer;->mCurrentMicReading:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/ears/AudioProgressRenderer;->setSize(II)V

    return-void
.end method

.method public setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    iput-object p1, p0, Lcom/google/android/ears/AudioProgressRenderer;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    return-void
.end method

.method public startAnimation()V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimationStartTimeInMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimationStartTimeInMs:J

    :cond_0
    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mBarFullDrawables:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mBarDrawables:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/google/android/ears/AudioProgressRenderer;->startAnimator()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mInitialized:Z

    return-void
.end method

.method public stopAnimation()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mInitialized:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/ears/AudioProgressRenderer;->stopAnimator()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mInitialized:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/ears/AudioProgressRenderer;->mAnimationStartTimeInMs:J

    goto :goto_0
.end method
