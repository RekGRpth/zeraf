.class public Lcom/google/android/goggles/ResultRanker;
.super Ljava/lang/Object;
.source "ResultRanker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/ResultRanker$ResultListener;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mLastSequenceNumberSent:I

.field private final mLatestResults:Lcom/google/android/goggles/ResultSet;

.field private final mResultListener:Lcom/google/android/goggles/ResultRanker$ResultListener;

.field private mSceneChangeAt:I

.field private mStopped:Z

.field private mWaitingForCompletion:Z


# direct methods
.method public constructor <init>(Lcom/google/android/goggles/ResultRanker$ResultListener;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/ResultRanker$ResultListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/goggles/ResultRanker$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/goggles/ResultRanker$1;-><init>(Lcom/google/android/goggles/ResultRanker;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/goggles/ResultSet;

    invoke-direct {v0}, Lcom/google/android/goggles/ResultSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    iput-object p1, p0, Lcom/google/android/goggles/ResultRanker;->mResultListener:Lcom/google/android/goggles/ResultRanker$ResultListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/ResultRanker;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/ResultRanker;

    invoke-direct {p0}, Lcom/google/android/goggles/ResultRanker;->notifyResults()V

    return-void
.end method

.method static filterResults(Ljava/util/List;Ljava/util/List;I)V
    .locals 7
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;I)V"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->clear()V

    sget-object v2, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    invoke-virtual {v2}, Lcom/google/android/goggles/ui/DebugView$ResultStats;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getHighestSequenceNumberMatched()I

    move-result v2

    if-lt v2, p2, :cond_0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getHighestSequenceNumberMatched()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getResultId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getTitle()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/goggles/ui/DebugView$ResultStats;->add(ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getHighestSequenceNumberMatched()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getResultId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getTitle()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/goggles/ui/DebugView$ResultStats;->add(ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    iput v3, v2, Lcom/google/android/goggles/ui/DebugView$ResultStats;->lastResultCount:I

    sget-object v2, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    iput v3, v2, Lcom/google/android/goggles/ui/DebugView$ResultStats;->sceneResultCount:I

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    return-void
.end method

.method private declared-synchronized notifyResults()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->hasSceneResults()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mResultListener:Lcom/google/android/goggles/ResultRanker$ResultListener;

    iget-object v1, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v1}, Lcom/google/android/goggles/ResultSet;->copy()Lcom/google/android/goggles/ResultSet;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/goggles/ResultRanker$ResultListener;->onResultReady(Lcom/google/android/goggles/ResultSet;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mResultListener:Lcom/google/android/goggles/ResultRanker$ResultListener;

    invoke-interface {v0}, Lcom/google/android/goggles/ResultRanker$ResultListener;->onNoResult()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private receivedAllResults(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)Z
    .locals 2
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    iget v0, p0, Lcom/google/android/goggles/ResultRanker;->mLastSequenceNumberSent:I

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberComplete()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized cancelWait()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mWaitingForCompletion:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasSceneResults()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->hasSceneResults()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onNewScene(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mStopped:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput p1, p0, Lcom/google/android/goggles/ResultRanker;->mSceneChangeAt:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onNewSession(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mStopped:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ResultSet;->setSessionId(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onQuerySent(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mStopped:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput p1, p0, Lcom/google/android/goggles/ResultRanker;->mLastSequenceNumberSent:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V
    .locals 4
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    iget-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mStopped:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getResultSetNumber()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getResultsList()Ljava/util/List;

    move-result-object v2

    iget v3, p0, Lcom/google/android/goggles/ResultRanker;->mSceneChangeAt:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/goggles/ResultSet;->update(ILjava/util/List;I)V

    invoke-direct {p0, p1}, Lcom/google/android/goggles/ResultRanker;->receivedAllResults(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->hasSceneResults()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mWaitingForCompletion:Z

    if-eqz v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mWaitingForCompletion:Z

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/goggles/ResultRanker;->cancelWait()V

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mWaitingForCompletion:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/goggles/ResultRanker;->mSceneChangeAt:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/goggles/ResultRanker;->mLastSequenceNumberSent:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mStopped:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mStopped:Z

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized waitForResults(J)V
    .locals 3
    .param p1    # J

    const-wide/16 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mStopped:Z

    if-nez v0, :cond_0

    cmp-long v0, p1, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mLatestResults:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->hasSceneResults()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    cmp-long v0, p1, v1

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/goggles/ResultRanker;->mWaitingForCompletion:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/goggles/ResultRanker;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
