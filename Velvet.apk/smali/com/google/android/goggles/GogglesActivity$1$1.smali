.class Lcom/google/android/goggles/GogglesActivity$1$1;
.super Lcom/google/android/goggles/camera/FrameProcessor;
.source "GogglesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/GogglesActivity$1;->onCameraCreated(Lcom/google/android/goggles/camera/CameraManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/goggles/GogglesActivity$1;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/GogglesActivity$1;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/goggles/GogglesActivity$1$1;->this$1:Lcom/google/android/goggles/GogglesActivity$1;

    invoke-direct {p0, p2}, Lcom/google/android/goggles/camera/FrameProcessor;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onBackgroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V
    .locals 3
    .param p1    # Lcom/google/android/goggles/camera/PreviewFrame;

    iget-object v1, p1, Lcom/google/android/goggles/camera/PreviewFrame;->newBarcode:Lcom/google/android/goggles/Barcode;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "SCAN_RESULT"

    iget-object v2, p1, Lcom/google/android/goggles/camera/PreviewFrame;->newBarcode:Lcom/google/android/goggles/Barcode;

    iget-object v2, v2, Lcom/google/android/goggles/Barcode;->info:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesActivity$1$1;->this$1:Lcom/google/android/goggles/GogglesActivity$1;

    iget-object v1, v1, Lcom/google/android/goggles/GogglesActivity$1;->this$0:Lcom/google/android/goggles/GogglesActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/goggles/GogglesActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/goggles/GogglesActivity$1$1;->this$1:Lcom/google/android/goggles/GogglesActivity$1;

    iget-object v1, v1, Lcom/google/android/goggles/GogglesActivity$1;->this$0:Lcom/google/android/goggles/GogglesActivity;

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesActivity;->finish()V

    :cond_0
    return-void
.end method
