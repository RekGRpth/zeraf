.class Lcom/google/android/goggles/GogglesGenericCard$1;
.super Ljava/lang/Object;
.source "GogglesGenericCard.java"

# interfaces
.implements Lcom/google/android/velvet/ui/WebImageView$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/GogglesGenericCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/GogglesGenericCard;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/GogglesGenericCard;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/GogglesGenericCard$1;->this$0:Lcom/google/android/goggles/GogglesGenericCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImageDownloaded(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard$1;->this$0:Lcom/google/android/goggles/GogglesGenericCard;

    # getter for: Lcom/google/android/goggles/GogglesGenericCard;->mSessionId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/goggles/GogglesGenericCard;->access$100(Lcom/google/android/goggles/GogglesGenericCard;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/TraceTracker;->getSession(Ljava/lang/String;)Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesGenericCard$1;->this$0:Lcom/google/android/goggles/GogglesGenericCard;

    # getter for: Lcom/google/android/goggles/GogglesGenericCard;->mFifeImageUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesGenericCard;->access$000(Lcom/google/android/goggles/GogglesGenericCard;)Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Lcom/google/android/goggles/TraceTracker$Session;->addClientEventThumbnailGet(Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
