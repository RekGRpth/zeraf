.class Lcom/google/android/goggles/ui/GogglesPlate$1;
.super Ljava/lang/Object;
.source "GogglesPlate.java"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/ui/GogglesPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/ui/GogglesPlate;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/ui/GogglesPlate;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/ui/GogglesPlate$1;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/animation/LayoutTransition;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/View;
    .param p4    # I

    const/4 v0, 0x4

    if-ne p4, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$1;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$000(Lcom/google/android/goggles/ui/GogglesPlate;)I

    move-result v0

    # invokes: Lcom/google/android/goggles/ui/GogglesPlate;->isCaptureMode(I)Z
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$100(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$1;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # invokes: Lcom/google/android/goggles/ui/GogglesPlate;->maybeShowCameraPreview()V
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$200(Lcom/google/android/goggles/ui/GogglesPlate;)V

    :cond_0
    return-void
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0
    .param p1    # Landroid/animation/LayoutTransition;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/View;
    .param p4    # I

    return-void
.end method
