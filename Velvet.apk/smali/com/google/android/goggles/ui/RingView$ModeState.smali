.class Lcom/google/android/goggles/ui/RingView$ModeState;
.super Ljava/lang/Object;
.source "RingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/RingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModeState"
.end annotation


# instance fields
.field public animated:Z

.field public dashFraction:F

.field public dashVisibility:F

.field public discVisibility:F

.field public dotVisibility:F

.field public gogglesPlateMode:I

.field public rotatingSpeed:F

.field final synthetic this$0:Lcom/google/android/goggles/ui/RingView;


# direct methods
.method public constructor <init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V
    .locals 0
    .param p2    # I
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F
    .param p7    # F
    .param p8    # Z

    iput-object p1, p0, Lcom/google/android/goggles/ui/RingView$ModeState;->this$0:Lcom/google/android/goggles/ui/RingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/goggles/ui/RingView$ModeState;->gogglesPlateMode:I

    iput p3, p0, Lcom/google/android/goggles/ui/RingView$ModeState;->dashFraction:F

    iput p4, p0, Lcom/google/android/goggles/ui/RingView$ModeState;->dashVisibility:F

    iput p5, p0, Lcom/google/android/goggles/ui/RingView$ModeState;->dotVisibility:F

    iput p6, p0, Lcom/google/android/goggles/ui/RingView$ModeState;->discVisibility:F

    iput p7, p0, Lcom/google/android/goggles/ui/RingView$ModeState;->rotatingSpeed:F

    iput-boolean p8, p0, Lcom/google/android/goggles/ui/RingView$ModeState;->animated:Z

    return-void
.end method
