.class public Lcom/google/android/goggles/ui/DebugView$ResultStats;
.super Ljava/lang/Object;
.source "DebugView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/DebugView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResultStats"
.end annotation


# instance fields
.field public volatile lastResultCount:I

.field private final results:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/text/SpannableString;",
            ">;"
        }
    .end annotation
.end field

.field public volatile sceneResultCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/ui/DebugView$ResultStats;->results:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/goggles/ui/DebugView$ResultStats;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/DebugView$ResultStats;

    iget-object v0, p0, Lcom/google/android/goggles/ui/DebugView$ResultStats;->results:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method


# virtual methods
.method public add(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const/16 v4, 0x21

    const/4 v3, 0x1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/goggles/ui/DebugView;->isVisible()Z
    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->access$000()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, p2, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView$ResultStats;->results:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    const/4 v1, 0x0

    # invokes: Lcom/google/android/goggles/ui/DebugView;->isVisible()Z
    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->access$000()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v1, p0, Lcom/google/android/goggles/ui/DebugView$ResultStats;->lastResultCount:I

    iput v1, p0, Lcom/google/android/goggles/ui/DebugView$ResultStats;->sceneResultCount:I

    iget-object v0, p0, Lcom/google/android/goggles/ui/DebugView$ResultStats;->results:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    goto :goto_0
.end method
