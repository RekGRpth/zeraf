.class Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;
.super Ljava/lang/Object;
.source "GogglesPlateLayoutManager.java"


# instance fields
.field private mCameraPreview:Landroid/view/View;

.field private mCaptureHeightPx:I

.field private mDescribeHeightPx:I

.field private mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

.field private mQueryImage:Landroid/view/View;

.field private mResultHeightPx:I

.field private mRingView:Landroid/view/View;

.field private mTextEndpointHeightPx:I

.field private mVisibilityMatrix:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private mVoiceEndpointHeightPx:I


# direct methods
.method constructor <init>(Lcom/google/android/goggles/ui/GogglesPlate;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {p1}, Lcom/google/android/goggles/ui/GogglesPlate;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mResultHeightPx:I

    const v1, 0x7f0c00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mDescribeHeightPx:I

    return-void
.end method

.method private performFrameLayout(Landroid/view/View;ZIIII)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Z
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    sub-int v6, p5, p3

    sub-int v5, p6, p4

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v9

    const/16 v10, 0x8

    if-eq v9, v10, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v9, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v3, v9, 0x7

    iget v9, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v7, v9, 0x70

    and-int/lit8 v9, v3, 0x7

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    sub-int v9, v6, v8

    div-int/lit8 v9, v9, 0x2

    iget v10, v4, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v9, v10

    iget v10, v4, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int v0, v9, v10

    :goto_0
    sparse-switch v7, :sswitch_data_0

    sub-int v9, v5, v2

    div-int/lit8 v9, v9, 0x2

    iget v10, v4, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v9, v10

    iget v10, v4, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int v1, v9, v10

    :goto_1
    add-int v9, v0, v8

    add-int v10, v1, v2

    invoke-virtual {p1, v0, v1, v9, v10}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void

    :pswitch_1
    iget v0, v4, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    :pswitch_2
    sub-int v9, v6, v8

    iget v10, v4, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int v0, v9, v10

    goto :goto_0

    :sswitch_0
    iget v1, v4, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_1

    :sswitch_1
    sub-int v9, v5, v2

    iget v10, v4, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int v1, v9, v10

    goto :goto_1

    :sswitch_2
    sub-int v9, v5, v2

    div-int/lit8 v1, v9, 0x2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_2
        0x30 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method configureVisibilityMatrix(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v1, 0x7f100104

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCameraPreview:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v1, 0x7f100106

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mRingView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v1, 0x7f100105

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mQueryImage:Landroid/view/View;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCameraPreview:Landroid/view/View;

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mRingView:Landroid/view/View;

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mQueryImage:Landroid/view/View;

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v2, 0x7f100107

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v2, 0x7f10010c

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v2, 0x7f10010d

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v2, 0x7f10010e

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v2, 0x7f10010f

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v2, 0x7f100110

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1, v5}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->transitionElements(IZ)V

    return-void
.end method

.method determineHeight(I)I
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3fa00000

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v1}, Lcom/google/android/goggles/ui/GogglesPlate;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCameraPreview:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mRingView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mQueryImage:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_0
    if-nez p1, :cond_1

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mTextEndpointHeightPx:I

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVoiceEndpointHeightPx:I

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    if-ne p1, v0, :cond_3

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mDescribeHeightPx:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x7

    if-eq p1, v0, :cond_4

    const/16 v0, 0x9

    if-ne p1, v0, :cond_5

    :cond_4
    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mResultHeightPx:I

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    goto :goto_0
.end method

.method disableFlash()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    const v2, 0x7f10010d

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method getCaptureHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    return v0
.end method

.method onLayout(ZIIII)V
    .locals 11
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sub-int v0, p5, p3

    div-int/lit8 v7, v0, 0x2

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    div-int/lit8 v0, v0, 0x2

    sub-int v10, v7, v0

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCaptureHeightPx:I

    div-int/lit8 v0, v0, 0x2

    add-int v9, v7, v0

    const/4 v8, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->getChildCount()I

    move-result v0

    if-ge v8, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0, v8}, Lcom/google/android/goggles/ui/GogglesPlate;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mCameraPreview:Landroid/view/View;

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mRingView:Landroid/view/View;

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mQueryImage:Landroid/view/View;

    if-ne v1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    sub-int v2, p4, p2

    invoke-virtual {v1, v0, v10, v2, v9}, Landroid/view/View;->layout(IIII)V

    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->performFrameLayout(Landroid/view/View;ZIIII)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method setTextEndpointHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mTextEndpointHeightPx:I

    return-void
.end method

.method setVoiceEndpointHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVoiceEndpointHeightPx:I

    return-void
.end method

.method transitionElements(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    iget-object v2, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz p2, :cond_3

    iget-object v2, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v2

    const/high16 v3, 0x3f800000

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_1
    invoke-static {v1}, Lcom/google/android/velvet/ui/util/Animations;->showAndFadeIn(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v2, p0, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->mVisibilityMatrix:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const/4 v2, 0x4

    goto :goto_1

    :cond_5
    return-void
.end method
