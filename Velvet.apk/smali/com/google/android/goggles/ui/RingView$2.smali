.class final Lcom/google/android/goggles/ui/RingView$2;
.super Landroid/util/Property;
.source "RingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/RingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Lcom/google/android/goggles/ui/RingView;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Lcom/google/android/goggles/ui/RingView;)Ljava/lang/Integer;
    .locals 1
    .param p1    # Lcom/google/android/goggles/ui/RingView;

    # getter for: Lcom/google/android/goggles/ui/RingView;->mBgColor:I
    invoke-static {p1}, Lcom/google/android/goggles/ui/RingView;->access$200(Lcom/google/android/goggles/ui/RingView;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/goggles/ui/RingView;

    invoke-virtual {p0, p1}, Lcom/google/android/goggles/ui/RingView$2;->get(Lcom/google/android/goggles/ui/RingView;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/google/android/goggles/ui/RingView;Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/ui/RingView;
    .param p2    # Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    # setter for: Lcom/google/android/goggles/ui/RingView;->mBgColor:I
    invoke-static {p1, v0}, Lcom/google/android/goggles/ui/RingView;->access$202(Lcom/google/android/goggles/ui/RingView;I)I

    invoke-virtual {p1}, Lcom/google/android/goggles/ui/RingView;->invalidate()V

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/goggles/ui/RingView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/goggles/ui/RingView$2;->set(Lcom/google/android/goggles/ui/RingView;Ljava/lang/Integer;)V

    return-void
.end method
