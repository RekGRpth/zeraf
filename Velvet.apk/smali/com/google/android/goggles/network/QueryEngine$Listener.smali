.class public interface abstract Lcom/google/android/goggles/network/QueryEngine$Listener;
.super Ljava/lang/Object;
.source "QueryEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/network/QueryEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onAuthError()V
.end method

.method public abstract onError()V
.end method

.method public abstract onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V
.end method
