.class Lcom/google/android/goggles/network/QueryManagerImpl$1;
.super Ljava/lang/Object;
.source "QueryManagerImpl.java"

# interfaces
.implements Lcom/google/android/goggles/network/QueryEngine$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/network/QueryManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/network/QueryManagerImpl;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/network/QueryManagerImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/network/QueryManagerImpl$1;->this$0:Lcom/google/android/goggles/network/QueryManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl$1;->this$0:Lcom/google/android/goggles/network/QueryManagerImpl;

    iget-object v0, v0, Lcom/google/android/goggles/network/QueryManagerImpl;->mRetryPolicy:Lcom/google/android/goggles/network/RetryPolicy;

    invoke-virtual {v0}, Lcom/google/android/goggles/network/RetryPolicy;->canRetry()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl$1;->this$0:Lcom/google/android/goggles/network/QueryManagerImpl;

    # invokes: Lcom/google/android/goggles/network/QueryManagerImpl;->retry()V
    invoke-static {v0}, Lcom/google/android/goggles/network/QueryManagerImpl;->access$400(Lcom/google/android/goggles/network/QueryManagerImpl;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/goggles/network/QueryManagerImpl$1;->onError()V

    goto :goto_0
.end method

.method public onError()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl$1;->this$0:Lcom/google/android/goggles/network/QueryManagerImpl;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl$1;->this$0:Lcom/google/android/goggles/network/QueryManagerImpl;

    # getter for: Lcom/google/android/goggles/network/QueryManagerImpl;->mPostDisconnectedError:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->access$200(Lcom/google/android/goggles/network/QueryManagerImpl;)Ljava/lang/Runnable;

    move-result-object v1

    # invokes: Lcom/google/android/goggles/network/QueryManagerImpl;->closeInner(Ljava/lang/Runnable;)V
    invoke-static {v0, v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->access$300(Lcom/google/android/goggles/network/QueryManagerImpl;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V
    .locals 3
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl$1;->this$0:Lcom/google/android/goggles/network/QueryManagerImpl;

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberReceived()I

    move-result v2

    # setter for: Lcom/google/android/goggles/network/QueryManagerImpl;->mAckedSequenceNumber:I
    invoke-static {v1, v2}, Lcom/google/android/goggles/network/QueryManagerImpl;->access$002(Lcom/google/android/goggles/network/QueryManagerImpl;I)I

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/TraceTracker;->getLatestSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getResultSetNumber()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/goggles/TraceTracker$Session;->addClientEventRecvResponse(I)V

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->hasSessionMetadata()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getSessionMetadata()Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$SessionMetadata;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/TraceTracker;->getLatestSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/goggles/TraceTracker$Session;->setSessionId(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl$1;->this$0:Lcom/google/android/goggles/network/QueryManagerImpl;

    # getter for: Lcom/google/android/goggles/network/QueryManagerImpl;->mCallback:Lcom/google/android/goggles/network/QueryManager$Listener;
    invoke-static {v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->access$100(Lcom/google/android/goggles/network/QueryManagerImpl;)Lcom/google/android/goggles/network/QueryManager$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/goggles/network/QueryManager$Listener;->onNewSession(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl$1;->this$0:Lcom/google/android/goggles/network/QueryManagerImpl;

    # getter for: Lcom/google/android/goggles/network/QueryManagerImpl;->mCallback:Lcom/google/android/goggles/network/QueryManager$Listener;
    invoke-static {v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->access$100(Lcom/google/android/goggles/network/QueryManagerImpl;)Lcom/google/android/goggles/network/QueryManager$Listener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/goggles/network/QueryManager$Listener;->onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V

    return-void
.end method
