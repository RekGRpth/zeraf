.class public interface abstract Lcom/google/android/goggles/network/QueryManager$Listener;
.super Ljava/lang/Object;
.source "QueryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/network/QueryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onConnected()V
.end method

.method public abstract onConnectionError()V
.end method

.method public abstract onDisconnected()V
.end method

.method public abstract onError()V
.end method

.method public abstract onNewSession(Ljava/lang/String;)V
.end method

.method public abstract onQuerySent(I)V
.end method

.method public abstract onReconnected()V
.end method

.method public abstract onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V
.end method
