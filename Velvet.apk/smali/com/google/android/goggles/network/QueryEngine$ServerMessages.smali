.class public final Lcom/google/android/goggles/network/QueryEngine$ServerMessages;
.super Ljava/lang/Object;
.source "QueryEngine.java"

# interfaces
.implements Lcom/google/android/speech/callback/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/network/QueryEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "ServerMessages"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/Callback",
        "<",
        "Lcom/google/speech/s3/S3$S3Response;",
        "Lcom/google/android/speech/exception/RecognizeException;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/network/QueryEngine;


# direct methods
.method protected constructor <init>(Lcom/google/android/goggles/network/QueryEngine;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;->this$0:Lcom/google/android/goggles/network/QueryEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    const-string v0, "goggles.ServerMessages"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;->this$0:Lcom/google/android/goggles/network/QueryEngine;

    # getter for: Lcom/google/android/goggles/network/QueryEngine;->mListener:Lcom/google/android/goggles/network/QueryEngine$Listener;
    invoke-static {v0}, Lcom/google/android/goggles/network/QueryEngine;->access$000(Lcom/google/android/goggles/network/QueryEngine;)Lcom/google/android/goggles/network/QueryEngine$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/network/QueryEngine$Listener;->onError()V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/speech/exception/RecognizeException;

    invoke-virtual {p0, p1}, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    return-void
.end method

.method public onResult(Lcom/google/speech/s3/S3$S3Response;)V
    .locals 4
    .param p1    # Lcom/google/speech/s3/S3$S3Response;

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const-string v1, "goggles.ServerMessages"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Frontend error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/goggles/ui/DebugView;->lastError:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getErrorCode()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/speech/exception/AuthFailureException;->isAuthErrorCode(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/google/android/goggles/ui/DebugView;->authErrorCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/goggles/ui/DebugView;->authErrorCount:I

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    const-string v1, "goggles.ServerMessages"

    const-string v2, "Authentication error."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;->this$0:Lcom/google/android/goggles/network/QueryEngine;

    # getter for: Lcom/google/android/goggles/network/QueryEngine;->mListener:Lcom/google/android/goggles/network/QueryEngine$Listener;
    invoke-static {v1}, Lcom/google/android/goggles/network/QueryEngine;->access$000(Lcom/google/android/goggles/network/QueryEngine;)Lcom/google/android/goggles/network/QueryEngine$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/goggles/network/QueryEngine$Listener;->onAuthError()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;->this$0:Lcom/google/android/goggles/network/QueryEngine;

    # getter for: Lcom/google/android/goggles/network/QueryEngine;->mListener:Lcom/google/android/goggles/network/QueryEngine$Listener;
    invoke-static {v1}, Lcom/google/android/goggles/network/QueryEngine;->access$000(Lcom/google/android/goggles/network/QueryEngine;)Lcom/google/android/goggles/network/QueryEngine$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/goggles/network/QueryEngine$Listener;->onError()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getGogglesStreamResponseExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;->this$0:Lcom/google/android/goggles/network/QueryEngine;

    # getter for: Lcom/google/android/goggles/network/QueryEngine;->mListener:Lcom/google/android/goggles/network/QueryEngine$Listener;
    invoke-static {v1}, Lcom/google/android/goggles/network/QueryEngine;->access$000(Lcom/google/android/goggles/network/QueryEngine;)Lcom/google/android/goggles/network/QueryEngine$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/goggles/network/QueryEngine$Listener;->onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/speech/s3/S3$S3Response;

    invoke-virtual {p0, p1}, Lcom/google/android/goggles/network/QueryEngine$ServerMessages;->onResult(Lcom/google/speech/s3/S3$S3Response;)V

    return-void
.end method
