.class public Lcom/google/android/goggles/network/RetryPolicy;
.super Ljava/lang/Object;
.source "RetryPolicy.java"


# instance fields
.field private mRetries:I

.field private mStartMillis:J

.field private volatile mUnretryable:Z

.field private final mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/goggles/network/RetryPolicy;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method


# virtual methods
.method public canRetry()Z
    .locals 5

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/goggles/network/RetryPolicy;->mUnretryable:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/goggles/network/RetryPolicy;->mRetries:I

    invoke-virtual {p0}, Lcom/google/android/goggles/network/RetryPolicy;->getMaxRetries()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/goggles/network/RetryPolicy;->mStartMillis:J

    sub-long/2addr v1, v3

    invoke-virtual {p0}, Lcom/google/android/goggles/network/RetryPolicy;->getRetryTimeoutMillis()I

    move-result v3

    int-to-long v3, v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    iget v0, p0, Lcom/google/android/goggles/network/RetryPolicy;->mRetries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/goggles/network/RetryPolicy;->mRetries:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected getMaxRetries()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/network/RetryPolicy;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getNetworkRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;->getMaxRetries()I

    move-result v0

    return v0
.end method

.method protected getRetryTimeoutMillis()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/network/RetryPolicy;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getNetworkRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;->getMaxRetryTimeoutMsec()I

    move-result v0

    return v0
.end method

.method public markAsUnretryable()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/goggles/network/RetryPolicy;->mUnretryable:Z

    return-void
.end method

.method public final reset()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/goggles/network/RetryPolicy;->mRetries:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/goggles/network/RetryPolicy;->mStartMillis:J

    iput-boolean v2, p0, Lcom/google/android/goggles/network/RetryPolicy;->mUnretryable:Z

    return-void
.end method
