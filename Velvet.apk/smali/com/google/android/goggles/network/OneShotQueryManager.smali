.class public Lcom/google/android/goggles/network/OneShotQueryManager;
.super Ljava/lang/Object;
.source "OneShotQueryManager.java"


# instance fields
.field private final mCallback:Lcom/google/android/goggles/GogglesController$Callback;

.field private final mCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

.field private mJpegData:[B

.field private final mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

.field private final mQueryListener:Lcom/google/android/goggles/network/QueryManager$Listener;

.field private final mQueryManager:Lcom/google/android/goggles/network/QueryManager;

.field private final mResultRanker:Lcom/google/android/goggles/ResultRanker;

.field private mTextRestrict:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/goggles/GogglesController$Callback;Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 8
    .param p1    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p2    # Lcom/google/android/velvet/VelvetFactory;
    .param p3    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p4    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p5    # Lcom/google/android/goggles/GogglesSettings;
    .param p6    # Lcom/google/android/goggles/GogglesController$Callback;
    .param p7    # Lcom/google/android/velvet/presenter/QueryState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

    new-instance v0, Lcom/google/android/goggles/network/OneShotQueryManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/network/OneShotQueryManager$1;-><init>(Lcom/google/android/goggles/network/OneShotQueryManager;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mQueryListener:Lcom/google/android/goggles/network/QueryManager$Listener;

    new-instance v0, Lcom/google/android/goggles/network/OneShotQueryManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/network/OneShotQueryManager$2;-><init>(Lcom/google/android/goggles/network/OneShotQueryManager;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    new-instance v0, Lcom/google/android/goggles/network/QueryManagerImpl;

    iget-object v5, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mQueryListener:Lcom/google/android/goggles/network/QueryManager$Listener;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/goggles/network/QueryManagerImpl;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/network/QueryManager$Listener;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    iput-object p6, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mCallback:Lcom/google/android/goggles/GogglesController$Callback;

    new-instance v0, Lcom/google/android/goggles/ResultRanker;

    iget-object v1, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    invoke-direct {v0, v1}, Lcom/google/android/goggles/ResultRanker;-><init>(Lcom/google/android/goggles/ResultRanker$ResultListener;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/ResultRanker;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/OneShotQueryManager;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/GogglesController$Callback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/OneShotQueryManager;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/goggles/network/OneShotQueryManager;)[B
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/OneShotQueryManager;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegData:[B

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/goggles/network/OneShotQueryManager;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/OneShotQueryManager;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mTextRestrict:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/network/QueryManager;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/OneShotQueryManager;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/goggles/network/OneShotQueryManager;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/network/OneShotQueryManager;

    invoke-direct {p0}, Lcom/google/android/goggles/network/OneShotQueryManager;->closeSession()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/GogglesController$Callback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/OneShotQueryManager;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mCallback:Lcom/google/android/goggles/GogglesController$Callback;

    return-object v0
.end method

.method private closeSession()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    invoke-interface {v0}, Lcom/google/android/goggles/network/QueryManager;->close()V

    iput-object v1, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegData:[B

    iput-object v1, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mTextRestrict:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultRanker;->cancelWait()V

    invoke-direct {p0}, Lcom/google/android/goggles/network/OneShotQueryManager;->closeSession()V

    return-void
.end method

.method public query(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultRanker;->reset()V

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x5f

    iget-object v2, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p2, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "goggles.OneShotQueryManager"

    const-string v1, "Failed to convert bitmap into jpeg data."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$Callback;->onError()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegData:[B

    iput-object p3, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mTextRestrict:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    invoke-interface {v0, p1}, Lcom/google/android/goggles/network/QueryManager;->open(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mQueryManager:Lcom/google/android/goggles/network/QueryManager;

    iget-object v1, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mJpegData:[B

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, p3, v3}, Lcom/google/android/goggles/network/QueryManager;->query([BILjava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/goggles/ResultRanker;->waitForResults(J)V

    goto :goto_0
.end method
