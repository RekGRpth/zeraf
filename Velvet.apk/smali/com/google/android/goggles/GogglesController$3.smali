.class Lcom/google/android/goggles/GogglesController$3;
.super Ljava/lang/Object;
.source "GogglesController.java"

# interfaces
.implements Lcom/google/android/goggles/QueryFrameProcessor$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/GogglesController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/GogglesController;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/GogglesController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 0

    return-void
.end method

.method public onConnectionError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mResultCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$600(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/GogglesController$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$Callback;->onError()V

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onError()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$700(Lcom/google/android/goggles/GogglesController;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/goggles/GogglesController$3$1;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/GogglesController$3$1;-><init>(Lcom/google/android/goggles/GogglesController$3;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onImageFetchingDone()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$700(Lcom/google/android/goggles/GogglesController;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mPauseCameraUi:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/goggles/GogglesController;->access$900(Lcom/google/android/goggles/GogglesController;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onNewScene(I)V
    .locals 4
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mSceneChangeDelayMillis:I
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$800(Lcom/google/android/goggles/GogglesController;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$300(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ResultRanker;->onNewScene(I)V

    :goto_0
    sput p1, Lcom/google/android/goggles/ui/DebugView;->sceneChangeAt:I

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$700(Lcom/google/android/goggles/GogglesController;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/goggles/GogglesController$3$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/goggles/GogglesController$3$2;-><init>(Lcom/google/android/goggles/GogglesController$3;I)V

    iget-object v2, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mSceneChangeDelayMillis:I
    invoke-static {v2}, Lcom/google/android/goggles/GogglesController;->access$800(Lcom/google/android/goggles/GogglesController;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onNewSession(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "goggles.GogglesController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "New session: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$300(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ResultRanker;->onNewSession(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/goggles/ui/DebugView;->pushSessionId(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    return-void
.end method

.method public onQuerySent(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$200(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/GogglesController$GogglesUi;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$GogglesUi;->notifyGogglesQuerySent()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$300(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ResultRanker;->onQuerySent(I)V

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    iput p1, v0, Lcom/google/android/goggles/ui/DebugView$QueryStats;->sequenceNumberSent:I

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    return-void
.end method

.method public onReconnected()V
    .locals 0

    return-void
.end method

.method public onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V
    .locals 2
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$300(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ResultRanker;->onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$300(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/ResultRanker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultRanker;->hasSceneResults()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mCaptureButtonClicked:Z
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$400(Lcom/google/android/goggles/GogglesController;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$200(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/GogglesController$GogglesUi;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$GogglesUi;->notifyGogglesResult()V

    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getResultSetNumber()I

    move-result v1

    iput v1, v0, Lcom/google/android/goggles/ui/DebugView$QueryStats;->resultSetNumberRecv:I

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberReceived()I

    move-result v1

    iput v1, v0, Lcom/google/android/goggles/ui/DebugView$QueryStats;->highestSeqNumberRecv:I

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberComplete()I

    move-result v1

    iput v1, v0, Lcom/google/android/goggles/ui/DebugView$QueryStats;->highestSeqNumberCompleted:I

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # invokes: Lcom/google/android/goggles/GogglesController;->stopCapturingAndNotifyResults()V
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$500(Lcom/google/android/goggles/GogglesController;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mCaptureButtonClicked:Z
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$400(Lcom/google/android/goggles/GogglesController;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$3;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$200(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/GogglesController$GogglesUi;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$GogglesUi;->notifyNoGogglesResult()V

    goto :goto_0
.end method
