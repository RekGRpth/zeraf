.class Lcom/google/android/goggles/GogglesController$2;
.super Ljava/lang/Object;
.source "GogglesController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/GogglesController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/GogglesController;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/GogglesController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/GogglesController$2;->this$0:Lcom/google/android/goggles/GogglesController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v0, "goggles.GogglesController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Session max time ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/GogglesController$2;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mSessionTimeoutMillis:I
    invoke-static {v2}, Lcom/google/android/goggles/GogglesController;->access$000(Lcom/google/android/goggles/GogglesController;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") reached, abort."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController$2;->this$0:Lcom/google/android/goggles/GogglesController;

    # getter for: Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesController;->access$100(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/QueryFrameProcessor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/QueryFrameProcessor;->requestDoneStreamingImages(I)V

    return-void
.end method
