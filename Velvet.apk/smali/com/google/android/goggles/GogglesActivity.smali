.class public Lcom/google/android/goggles/GogglesActivity;
.super Landroid/app/Activity;
.source "GogglesActivity.java"


# static fields
.field public static final ZXING_DATA:Landroid/net/Uri;


# instance fields
.field private mApp:Lcom/google/android/velvet/VelvetApplication;

.field private final mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

.field private mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;

.field private mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://zxing.appspot.com/scan"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/goggles/GogglesActivity;->ZXING_DATA:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/google/android/goggles/GogglesActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/GogglesActivity$1;-><init>(Lcom/google/android/goggles/GogglesActivity;)V

    iput-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/GogglesActivity;)Lcom/google/android/goggles/GogglesSettings;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesActivity;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/goggles/GogglesActivity;)Lcom/google/android/goggles/ui/GogglesPlate;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesActivity;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/goggles/GogglesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.googlequicksearchbox.GOGGLES"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v1, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/GogglesActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/goggles/GogglesActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    const v0, 0x7f040044

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/GogglesActivity;->setContentView(I)V

    const v0, 0x7f1000f8

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/GogglesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/ui/GogglesPlate;

    iput-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesActivity;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->setCallbacks(Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;)V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->enterOffline()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->stopCamera()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->startCamera()V

    return-void
.end method
