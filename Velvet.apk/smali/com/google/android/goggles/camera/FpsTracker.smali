.class public Lcom/google/android/goggles/camera/FpsTracker;
.super Ljava/lang/Object;
.source "FpsTracker.java"


# instance fields
.field private final mCircularQueue:[I

.field private mCurrIndex:I

.field private final mFpsBuckets:[I

.field private volatile mFpsString:Ljava/lang/String;

.field private mLastFrameTimestamp:J

.field private mNumFrames:I

.field private mTotalTime:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x28

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FpsTracker;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x29

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsBuckets:[I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mLastFrameTimestamp:J

    iput v2, p0, Lcom/google/android/goggles/camera/FpsTracker;->mNumFrames:I

    iput v2, p0, Lcom/google/android/goggles/camera/FpsTracker;->mTotalTime:I

    iput v2, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCurrIndex:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsString:Ljava/lang/String;

    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCircularQueue:[I

    return-void
.end method

.method private handleDelta(I)V
    .locals 7
    .param p1    # I

    const/16 v6, 0x3e8

    const/16 v5, 0x28

    if-gtz p1, :cond_0

    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mNumFrames:I

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mNumFrames:I

    iget-object v4, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCircularQueue:[I

    array-length v4, v4

    if-le v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCircularQueue:[I

    iget v4, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCurrIndex:I

    aget v2, v3, v4

    div-int v1, v6, v2

    if-gt v1, v5, :cond_2

    iget-object v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsBuckets:[I

    aget v4, v3, v1

    add-int/lit8 v4, v4, -0x1

    aput v4, v3, v1

    :goto_1
    iget v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mTotalTime:I

    sub-int/2addr v3, v2

    iput v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mTotalTime:I

    :cond_1
    iget-object v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCircularQueue:[I

    iget v4, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCurrIndex:I

    aput p1, v3, v4

    div-int v0, v6, p1

    if-gt v0, v5, :cond_3

    iget-object v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsBuckets:[I

    aget v4, v3, v0

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, v0

    :goto_2
    iget v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mTotalTime:I

    add-int/2addr v3, p1

    iput v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mTotalTime:I

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsString:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsBuckets:[I

    aget v4, v3, v5

    add-int/lit8 v4, v4, -0x1

    aput v4, v3, v5

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsBuckets:[I

    aget v4, v3, v5

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, v5

    goto :goto_2
.end method


# virtual methods
.method public getFpsString()Ljava/lang/String;
    .locals 10

    iget-object v6, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsString:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsString:Ljava/lang/String;

    :goto_0
    return-object v6

    :cond_0
    iget v6, p0, Lcom/google/android/goggles/camera/FpsTracker;->mNumFrames:I

    iget-object v7, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCircularQueue:[I

    array-length v7, v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    if-nez v3, :cond_1

    const-string v6, ""

    goto :goto_0

    :cond_1
    const/4 v6, 0x5

    new-array v5, v6, [Ljava/lang/Integer;

    const/4 v2, 0x0

    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_2

    const/4 v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_2
    const/16 v6, 0x28

    if-gt v2, v6, :cond_5

    iget-object v6, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsBuckets:[I

    aget v6, v6, v2

    if-nez v6, :cond_4

    :cond_3
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iget-object v6, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsBuckets:[I

    aget v6, v6, v2

    add-int/2addr v0, v6

    mul-int v6, v4, v3

    div-int/lit8 v6, v6, 0x4

    if-lt v0, v6, :cond_3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_5
    const/high16 v6, 0x447a0000

    iget v7, p0, Lcom/google/android/goggles/camera/FpsTracker;->mTotalTime:I

    int-to-float v7, v7

    int-to-float v8, v3

    div-float/2addr v7, v8

    div-float v1, v6, v7

    const-string v6, "%2.1f  [%s] %d/%d"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, ","

    invoke-static {v9, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget v9, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCurrIndex:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    iget-object v9, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCircularQueue:[I

    array-length v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsString:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/goggles/camera/FpsTracker;->mFpsString:Ljava/lang/String;

    goto :goto_0
.end method

.method public tick()Z
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/goggles/camera/FpsTracker;->tick(J)Z

    move-result v0

    return v0
.end method

.method public tick(J)Z
    .locals 2
    .param p1    # J

    iget v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mNumFrames:I

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCircularQueue:[I

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCurrIndex:I

    iget v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mNumFrames:I

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mLastFrameTimestamp:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FpsTracker;->handleDelta(I)V

    :cond_0
    iget v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mNumFrames:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mNumFrames:I

    iput-wide p1, p0, Lcom/google/android/goggles/camera/FpsTracker;->mLastFrameTimestamp:J

    iget v0, p0, Lcom/google/android/goggles/camera/FpsTracker;->mCurrIndex:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
