.class Lcom/google/android/goggles/camera/CameraManager$3;
.super Ljava/lang/Object;
.source "CameraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraManager;->startPreviewWhenReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraManager;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/CameraManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraManager$3;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    const-string v0, "startPreviewCallback()"

    # invokes: Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {p0, v0}, Lcom/google/android/goggles/camera/CameraManager;->access$600(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$3;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # getter for: Lcom/google/android/goggles/camera/CameraManager;->mFocusFrameProcessor:Lcom/google/android/goggles/camera/FocusFrameProcessor;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$700(Lcom/google/android/goggles/camera/CameraManager;)Lcom/google/android/goggles/camera/FocusFrameProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->start()V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$3;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # getter for: Lcom/google/android/goggles/camera/CameraManager;->mCameraListener:Lcom/google/android/goggles/camera/CameraManager$Listener;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$300(Lcom/google/android/goggles/camera/CameraManager;)Lcom/google/android/goggles/camera/CameraManager$Listener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraManager$Listener;->onPreviewStarted()V

    return-void
.end method
