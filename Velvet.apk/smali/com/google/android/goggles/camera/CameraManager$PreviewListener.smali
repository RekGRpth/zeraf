.class Lcom/google/android/goggles/camera/CameraManager$PreviewListener;
.super Ljava/lang/Object;
.source "CameraManager.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/CameraManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreviewListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraManager;


# direct methods
.method private constructor <init>(Lcom/google/android/goggles/camera/CameraManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraManager$PreviewListener;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/goggles/camera/CameraManager;Lcom/google/android/goggles/camera/CameraManager$1;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/camera/CameraManager;
    .param p2    # Lcom/google/android/goggles/camera/CameraManager$1;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/camera/CameraManager$PreviewListener;-><init>(Lcom/google/android/goggles/camera/CameraManager;)V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    const-string v0, "onSurfaceTextureAvailable()"

    # invokes: Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {p1, v0}, Lcom/google/android/goggles/camera/CameraManager;->access$600(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$PreviewListener;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # setter for: Lcom/google/android/goggles/camera/CameraManager;->mPreviewSurface:Landroid/graphics/SurfaceTexture;
    invoke-static {v0, p1}, Lcom/google/android/goggles/camera/CameraManager;->access$802(Lcom/google/android/goggles/camera/CameraManager;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$PreviewListener;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # invokes: Lcom/google/android/goggles/camera/CameraManager;->setupCameraWhenReady()V
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$400(Lcom/google/android/goggles/camera/CameraManager;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$PreviewListener;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # invokes: Lcom/google/android/goggles/camera/CameraManager;->startPreviewWhenReady()V
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$500(Lcom/google/android/goggles/camera/CameraManager;)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2
    .param p1    # Landroid/graphics/SurfaceTexture;

    const-string v0, "onSurfaceTextureDestroyed()"

    # invokes: Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {p1, v0}, Lcom/google/android/goggles/camera/CameraManager;->access$600(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$PreviewListener;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/goggles/camera/CameraManager;->mPreviewSurface:Landroid/graphics/SurfaceTexture;
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/CameraManager;->access$802(Lcom/google/android/goggles/camera/CameraManager;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;

    const/4 v0, 0x0

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1    # Landroid/graphics/SurfaceTexture;

    return-void
.end method
