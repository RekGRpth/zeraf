.class Lcom/google/android/goggles/camera/FocusFrameProcessor$2;
.super Ljava/lang/Object;
.source "FocusFrameProcessor.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/FocusFrameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 4
    .param p1    # Z
    .param p2    # Landroid/hardware/Camera;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/goggles/camera/FocusFrameProcessor;->onFocusMoving(Z)V
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$000(Lcom/google/android/goggles/camera/FocusFrameProcessor;Z)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mPendingRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$300(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mPendingRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$300(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mPendingRunnable:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$302(Lcom/google/android/goggles/camera/FocusFrameProcessor;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$500(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$400(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
