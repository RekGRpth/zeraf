.class public Lcom/google/android/goggles/camera/CameraManager$Listener;
.super Ljava/lang/Object;
.source "CameraManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/CameraManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Listener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraError()V
    .locals 0

    return-void
.end method

.method public onCameraOpened(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onCameraReleased()V
    .locals 0

    return-void
.end method

.method public onPreviewStarted()V
    .locals 0

    return-void
.end method
