.class public Lcom/google/android/goggles/camera/FrameProcessor;
.super Ljava/lang/Object;
.source "FrameProcessor.java"


# instance fields
.field protected final mName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/goggles/camera/FrameProcessor;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onBackgroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/camera/PreviewFrame;

    return-void
.end method

.method protected onForegroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/camera/PreviewFrame;

    return-void
.end method
