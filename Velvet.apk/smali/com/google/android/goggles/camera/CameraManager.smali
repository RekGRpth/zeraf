.class public Lcom/google/android/goggles/camera/CameraManager;
.super Ljava/lang/Object;
.source "CameraManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/camera/CameraManager$Listener;,
        Lcom/google/android/goggles/camera/CameraManager$PreviewListener;,
        Lcom/google/android/goggles/camera/CameraManager$Size;
    }
.end annotation


# static fields
.field private static cameraService:Lcom/google/android/goggles/camera/CameraService;


# instance fields
.field private volatile mCameraActive:Z

.field private final mCameraListener:Lcom/google/android/goggles/camera/CameraManager$Listener;

.field private mCameraOpenCancelled:Z

.field private final mFocusFrameProcessor:Lcom/google/android/goggles/camera/FocusFrameProcessor;

.field private final mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

.field private mPreviewSurface:Landroid/graphics/SurfaceTexture;

.field private final mPreviewTexture:Landroid/view/TextureView;

.field private volatile mStartPreviewRequested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/goggles/camera/CameraService;

    invoke-direct {v0}, Lcom/google/android/goggles/camera/CameraService;-><init>()V

    sput-object v0, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    return-void
.end method

.method public constructor <init>(Landroid/view/TextureView;Lcom/google/android/goggles/camera/CameraManager$Listener;)V
    .locals 4
    .param p1    # Landroid/view/TextureView;
    .param p2    # Lcom/google/android/goggles/camera/CameraManager$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewTexture:Landroid/view/TextureView;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewTexture:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewSurface:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewTexture:Landroid/view/TextureView;

    new-instance v2, Lcom/google/android/goggles/camera/CameraManager$PreviewListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/goggles/camera/CameraManager$PreviewListener;-><init>(Lcom/google/android/goggles/camera/CameraManager;Lcom/google/android/goggles/camera/CameraManager$1;)V

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iput-object p2, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraListener:Lcom/google/android/goggles/camera/CameraManager$Listener;

    new-instance v1, Lcom/google/android/goggles/camera/PreviewLooper;

    invoke-direct {v1}, Lcom/google/android/goggles/camera/PreviewLooper;-><init>()V

    iput-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    invoke-virtual {p1}, Landroid/view/TextureView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->shouldForceFakeFocus()Z

    move-result v0

    new-instance v1, Lcom/google/android/goggles/camera/FocusFrameProcessor;

    sget-object v2, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-direct {v1, v2, v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;-><init>(Lcom/google/android/goggles/camera/CameraService;Z)V

    iput-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mFocusFrameProcessor:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mFocusFrameProcessor:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-virtual {p0, v1}, Lcom/google/android/goggles/camera/CameraManager;->addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V

    const-string v1, "constructed CameraManager"

    invoke-static {p0, v1}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    sput-object v1, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->previewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/goggles/camera/CameraManager;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/CameraManager;

    iget-boolean v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraOpenCancelled:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/goggles/camera/CameraManager;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraActive:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/goggles/camera/CameraManager;)Lcom/google/android/goggles/camera/CameraManager$Listener;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/CameraManager;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraListener:Lcom/google/android/goggles/camera/CameraManager$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/goggles/camera/CameraManager;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraManager;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraManager;->setupCameraWhenReady()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/goggles/camera/CameraManager;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraManager;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraManager;->startPreviewWhenReady()V

    return-void
.end method

.method static synthetic access$600(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/goggles/camera/CameraManager;)Lcom/google/android/goggles/camera/FocusFrameProcessor;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/CameraManager;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mFocusFrameProcessor:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/goggles/camera/CameraManager;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraManager;
    .param p1    # Landroid/graphics/SurfaceTexture;

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewSurface:Landroid/graphics/SurfaceTexture;

    return-object p1
.end method

.method private static cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public static generateUndistortTransformer(Lcom/google/android/goggles/camera/CameraManager$Size;Lcom/google/android/goggles/camera/CameraManager$Size;)Landroid/graphics/Matrix;
    .locals 5
    .param p0    # Lcom/google/android/goggles/camera/CameraManager$Size;
    .param p1    # Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    int-to-float v3, v3

    iget v4, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    int-to-float v4, v4

    div-float v1, v3, v4

    iget v3, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    int-to-float v3, v3

    iget v4, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    int-to-float v4, v4

    div-float v2, v3, v4

    cmpg-float v3, v1, v2

    if-gez v3, :cond_0

    div-float/2addr v2, v1

    const/high16 v1, 0x3f800000

    :goto_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iget v3, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    return-object v0

    :cond_0
    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000

    goto :goto_0
.end method

.method private getBestPictureSize(Lcom/google/android/goggles/camera/CameraManager$Size;Ljava/util/List;)Lcom/google/android/goggles/camera/CameraManager$Size;
    .locals 8
    .param p1    # Lcom/google/android/goggles/camera/CameraManager$Size;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/goggles/camera/CameraManager$Size;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/goggles/camera/CameraManager$Size;",
            ">;)",
            "Lcom/google/android/goggles/camera/CameraManager$Size;"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v6, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    int-to-float v6, v6

    iget v7, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    int-to-float v7, v7

    div-float v4, v6, v7

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v6, v5, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    int-to-float v6, v6

    iget v7, v5, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    int-to-float v7, v7

    div-float v0, v6, v7

    sub-float v6, v4, v0

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    sub-float v7, v4, v1

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_0

    move v1, v0

    move-object v2, v5

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private getCameraDisplayOrientation()I
    .locals 6

    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    const/4 v4, 0x0

    invoke-static {v4, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget-object v4, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewTexture:Landroid/view/TextureView;

    invoke-virtual {v4}, Landroid/view/TextureView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v3

    const/4 v0, 0x0

    packed-switch v3, :pswitch_data_0

    :goto_0
    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v4, v0

    rem-int/lit16 v2, v4, 0x168

    rsub-int v4, v2, 0x168

    rem-int/lit16 v2, v4, 0x168

    :goto_1
    return v2

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    :cond_0
    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v4, v0

    add-int/lit16 v4, v4, 0x168

    rem-int/lit16 v2, v4, 0x168

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private isInitialized()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraActive:Z

    if-nez v1, :cond_0

    const-string v1, "isInitialized() failed: !mCameraActive"

    invoke-static {p0, v1}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewSurface:Landroid/graphics/SurfaceTexture;

    if-nez v1, :cond_1

    const-string v1, "isInitialized() failed: mPreviewSurface == null"

    invoke-static {p0, v1}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setupCameraWhenReady()V
    .locals 10

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraManager;->isInitialized()Z

    move-result v8

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v8, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v8}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v8

    iget-object v4, v8, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/camera/CameraManager$Size;

    new-instance v5, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget-object v8, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewTexture:Landroid/view/TextureView;

    invoke-virtual {v8}, Landroid/view/TextureView;->getWidth()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewTexture:Landroid/view/TextureView;

    invoke-virtual {v9}, Landroid/view/TextureView;->getHeight()I

    move-result v9

    invoke-direct {v5, v8, v9}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v8, v7, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget v9, v7, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    mul-int/2addr v8, v9

    const v9, 0x30d40

    if-le v8, v9, :cond_2

    :cond_1
    sget-object v8, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v8}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v2

    iput-object v0, v2, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraManager;->getCameraDisplayOrientation()I

    move-result v8

    iput v8, v2, Lcom/google/android/goggles/camera/CameraParameters;->cameraDisplayOrientation:I

    iget-boolean v8, v2, Lcom/google/android/goggles/camera/CameraParameters;->continuousPictureFocusCapable:Z

    iput-boolean v8, v2, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    sget-object v8, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v8}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v8

    iget-object v3, v8, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    invoke-direct {p0, v0, v3}, Lcom/google/android/goggles/camera/CameraManager;->getBestPictureSize(Lcom/google/android/goggles/camera/CameraManager$Size;Ljava/util/List;)Lcom/google/android/goggles/camera/CameraManager$Size;

    move-result-object v8

    iput-object v8, v2, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    sget-object v8, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v8, v2}, Lcom/google/android/goggles/camera/CameraService;->setParameters(Lcom/google/android/goggles/camera/CameraParameters;)V

    iget v8, v2, Lcom/google/android/goggles/camera/CameraParameters;->cameraDisplayOrientation:I

    rem-int/lit16 v8, v8, 0xb4

    if-nez v8, :cond_3

    move-object v6, v0

    :goto_2
    iget-object v8, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewTexture:Landroid/view/TextureView;

    invoke-static {v6, v5}, Lcom/google/android/goggles/camera/CameraManager;->generateUndistortTransformer(Lcom/google/android/goggles/camera/CameraManager$Size;Lcom/google/android/goggles/camera/CameraManager$Size;)Landroid/graphics/Matrix;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_2
    move-object v0, v7

    goto :goto_1

    :cond_3
    new-instance v6, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v8, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    iget v9, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    invoke-direct {v6, v8, v9}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    goto :goto_2
.end method

.method private startPreviewWhenReady()V
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mStartPreviewRequested:Z

    if-nez v0, :cond_1

    const-string v0, "startPreviewWhenReady() skipped: !startPreviewRequested"

    invoke-static {p0, v0}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "startPreviewWhenReady() running"

    invoke-static {p0, v0}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraService;->installPreviewLooper(Lcom/google/android/goggles/camera/PreviewLooper;)V

    sget-object v0, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewSurface:Landroid/graphics/SurfaceTexture;

    new-instance v2, Lcom/google/android/goggles/camera/CameraManager$3;

    invoke-direct {v2, p0}, Lcom/google/android/goggles/camera/CameraManager$3;-><init>(Lcom/google/android/goggles/camera/CameraManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/goggles/camera/CameraService;->startPreview(Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/camera/FrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/camera/PreviewLooper;->addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V

    return-void
.end method

.method public canEnableTorch()Z
    .locals 3

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraActive:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v2}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v2, v0, Lcom/google/android/goggles/camera/CameraParameters;->torchCapable:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isTorchOn()Z
    .locals 2

    sget-object v1, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v1}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/android/goggles/camera/CameraParameters;->enableTorch:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public lockFocus(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mFocusFrameProcessor:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->autoFocusAndStop(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public openCamera()V
    .locals 2

    const-string v0, "openCamera()"

    invoke-static {p0, v0}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraOpenCancelled:Z

    sget-object v0, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    new-instance v1, Lcom/google/android/goggles/camera/CameraManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/camera/CameraManager$1;-><init>(Lcom/google/android/goggles/camera/CameraManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraService;->openCamera(Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;)V

    return-void
.end method

.method public releaseCamera()V
    .locals 2

    const-string v0, "releaseCamera()"

    invoke-static {p0, v0}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraOpenCancelled:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraActive:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->previewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mFocusFrameProcessor:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->stop()V

    invoke-static {}, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->onReleaseCamera()V

    sget-object v0, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    new-instance v1, Lcom/google/android/goggles/camera/CameraManager$2;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/camera/CameraManager$2;-><init>(Lcom/google/android/goggles/camera/CameraManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraService;->releaseCamera(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestStartPreview()V
    .locals 1

    const-string v0, "requestStartPreview()"

    invoke-static {p0, v0}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mStartPreviewRequested:Z

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraManager;->startPreviewWhenReady()V

    return-void
.end method

.method public setTorch(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v1, p0, Lcom/google/android/goggles/camera/CameraManager;->mCameraActive:Z

    if-nez v1, :cond_0

    const-string v1, "goggles.CameraManager"

    const-string v2, "Camera isn\'t active, can\'t set torch."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "setTorch()"

    invoke-static {p0, v1}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v1}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v0

    iput-boolean p1, v0, Lcom/google/android/goggles/camera/CameraParameters;->enableTorch:Z

    sget-object v1, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/camera/CameraService;->setParameters(Lcom/google/android/goggles/camera/CameraParameters;)V

    goto :goto_0
.end method

.method public stopPreview()V
    .locals 1

    const-string v0, "stopPreview()"

    invoke-static {p0, v0}, Lcom/google/android/goggles/camera/CameraManager;->cameraDebugMessage(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraManager;->mStartPreviewRequested:Z

    sget-object v0, Lcom/google/android/goggles/camera/CameraManager;->cameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraService;->stopPreview()V

    return-void
.end method
