.class public Lcom/google/android/goggles/camera/CameraManagerTestUtils;
.super Ljava/lang/Object;
.source "CameraManagerTestUtils.java"


# static fields
.field private static fakePreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

.field static previewLooper:Lcom/google/android/goggles/camera/PreviewLooper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/goggles/camera/PreviewFrame;)Lcom/google/android/goggles/camera/PreviewFrame;
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/PreviewFrame;

    sput-object p0, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->fakePreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    return-object p0
.end method

.method public static getFakePreviewFrame()Lcom/google/android/goggles/camera/PreviewFrame;
    .locals 1

    sget-object v0, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->fakePreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    return-object v0
.end method

.method public static insertFakeTriggers(Landroid/app/Activity;Landroid/view/Menu;)V
    .locals 7
    .param p0    # Landroid/app/Activity;
    .param p1    # Landroid/view/Menu;

    sget-object v3, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->previewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Lcom/google/android/goggles/camera/CameraManagerTestUtils$1;

    invoke-direct {v2, p0}, Lcom/google/android/goggles/camera/CameraManagerTestUtils$1;-><init>(Landroid/app/Activity;)V

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const-string v4, "Enter file in /data/local/tmp/ via adb"

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setInputType(I)V

    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Load image"

    new-instance v6, Lcom/google/android/goggles/camera/CameraManagerTestUtils$2;

    invoke-direct {v6, v2, v1}, Lcom/google/android/goggles/camera/CameraManagerTestUtils$2;-><init>(Landroid/os/AsyncTask;Landroid/widget/EditText;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const-string v4, "Load file for image"

    invoke-interface {p1, v4}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, Lcom/google/android/goggles/camera/CameraManagerTestUtils$3;

    invoke-direct {v5, v0}, Lcom/google/android/goggles/camera/CameraManagerTestUtils$3;-><init>(Landroid/app/AlertDialog;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static onReleaseCamera()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->previewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    sput-object v0, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->fakePreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    return-void
.end method
