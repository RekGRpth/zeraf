.class final Lcom/google/android/goggles/camera/CameraManagerTestUtils$1;
.super Landroid/os/AsyncTask;
.source "CameraManagerTestUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraManagerTestUtils;->insertFakeTriggers(Landroid/app/Activity;Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraManagerTestUtils$1;->val$context:Landroid/app/Activity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/goggles/camera/CameraManagerTestUtils$1;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const-string v1, ""

    aget-object v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    # setter for: Lcom/google/android/goggles/camera/CameraManagerTestUtils;->fakePreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;
    invoke-static {v4}, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->access$002(Lcom/google/android/goggles/camera/PreviewFrame;)Lcom/google/android/goggles/camera/PreviewFrame;

    const-string v1, "Reset preview image"

    :goto_0
    return-object v1

    :cond_0
    aget-object v1, p1, v3

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    # setter for: Lcom/google/android/goggles/camera/CameraManagerTestUtils;->fakePreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;
    invoke-static {v4}, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->access$002(Lcom/google/android/goggles/camera/PreviewFrame;)Lcom/google/android/goggles/camera/PreviewFrame;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/goggles/camera/PreviewFrame;

    invoke-direct {v1, v0}, Lcom/google/android/goggles/camera/PreviewFrame;-><init>(Landroid/graphics/Bitmap;)V

    # setter for: Lcom/google/android/goggles/camera/CameraManagerTestUtils;->fakePreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;
    invoke-static {v1}, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->access$002(Lcom/google/android/goggles/camera/PreviewFrame;)Lcom/google/android/goggles/camera/PreviewFrame;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loaded file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/goggles/camera/CameraManagerTestUtils$1;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManagerTestUtils$1;->val$context:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
