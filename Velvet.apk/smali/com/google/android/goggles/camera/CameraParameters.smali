.class public Lcom/google/android/goggles/camera/CameraParameters;
.super Ljava/lang/Object;
.source "CameraParameters.java"


# static fields
.field private static DEBUG:Z


# instance fields
.field public cameraDisplayOrientation:I

.field public continuousAutofocus:Z

.field public final continuousPictureFocusCapable:Z

.field public enableTorch:Z

.field public pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

.field public previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

.field public final supportedPictureSizes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/goggles/camera/CameraManager$Size;",
            ">;"
        }
    .end annotation
.end field

.field public final supportedPreviewSizes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/goggles/camera/CameraManager$Size;",
            ">;"
        }
    .end annotation
.end field

.field public final torchCapable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/goggles/camera/CameraParameters;->DEBUG:Z

    return-void
.end method

.method constructor <init>()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-direct {v0, v1, v1}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    new-instance v0, Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-direct {v0, v1, v1}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iput v2, p0, Lcom/google/android/goggles/camera/CameraParameters;->cameraDisplayOrientation:I

    iput-boolean v2, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    iput-boolean v2, p0, Lcom/google/android/goggles/camera/CameraParameters;->enableTorch:Z

    iput-boolean v2, p0, Lcom/google/android/goggles/camera/CameraParameters;->torchCapable:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-boolean v2, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousPictureFocusCapable:Z

    return-void
.end method

.method constructor <init>(Landroid/hardware/Camera$Parameters;)V
    .locals 11
    .param p1    # Landroid/hardware/Camera$Parameters;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    new-instance v5, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v8, v2, Landroid/hardware/Camera$Size;->width:I

    iget v9, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v5, v8, v9}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    iput-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    new-instance v5, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v8, v2, Landroid/hardware/Camera$Size;->width:I

    iget v9, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v5, v8, v9}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    iput-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    const-string v5, "continuous-picture"

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v5

    const-string v8, "torch"

    if-ne v5, v8, :cond_0

    move v5, v6

    :goto_0
    iput-boolean v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->enableTorch:Z

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v5, "torch"

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v6

    :goto_1
    iput-boolean v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->torchCapable:Z

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    iget-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    new-instance v8, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v9, v1, Landroid/hardware/Camera$Size;->width:I

    iget v10, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v8, v9, v10}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_0
    move v5, v7

    goto :goto_0

    :cond_1
    move v5, v7

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    iget-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    new-instance v8, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v9, v1, Landroid/hardware/Camera$Size;->width:I

    iget v10, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v8, v9, v10}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    iget-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_4

    const-string v5, "continuous-picture"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :goto_4
    iput-boolean v6, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousPictureFocusCapable:Z

    return-void

    :cond_4
    move v6, v7

    goto :goto_4
.end method

.method constructor <init>(Lcom/google/android/goggles/camera/CameraParameters;)V
    .locals 3
    .param p1    # Lcom/google/android/goggles/camera/CameraParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget-object v1, p1, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v1, v1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v2, p1, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v2, v2, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    new-instance v0, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget-object v1, p1, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v1, v1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v2, p1, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v2, v2, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v0, p1, Lcom/google/android/goggles/camera/CameraParameters;->cameraDisplayOrientation:I

    iput v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->cameraDisplayOrientation:I

    iget-boolean v0, p1, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    iget-boolean v0, p1, Lcom/google/android/goggles/camera/CameraParameters;->enableTorch:Z

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->enableTorch:Z

    iget-boolean v0, p1, Lcom/google/android/goggles/camera/CameraParameters;->torchCapable:Z

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->torchCapable:Z

    iget-object v0, p1, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    iget-object v0, p1, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    iget-boolean v0, p1, Lcom/google/android/goggles/camera/CameraParameters;->continuousPictureFocusCapable:Z

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousPictureFocusCapable:Z

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "torchCapable: %b | previewSize: %s |  pictureSize: %s | continuousAutofocus: %b | enableTorch: %b | cameraDisplayOrientation: %d | supportedPreviewSizes: %s |  supportedPictureSizes: %s | continuousPictureFocusCapable: %b"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->torchCapable:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {v3}, Lcom/google/android/goggles/camera/CameraManager$Size;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {v3}, Lcom/google/android/goggles/camera/CameraManager$Size;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->enableTorch:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->cameraDisplayOrientation:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPreviewSizes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->supportedPictureSizes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousPictureFocusCapable:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeTo(Landroid/hardware/Camera$Parameters;)V
    .locals 6
    .param p1    # Landroid/hardware/Camera$Parameters;

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    if-eqz v3, :cond_1

    sget-boolean v3, Lcom/google/android/goggles/camera/CameraParameters;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "goggles.CameraParameters"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting preview size to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v5, v5, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v5, v5, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v4, p0, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v4, v4, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-virtual {p1, v3, v4}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    if-eqz v3, :cond_3

    sget-boolean v3, Lcom/google/android/goggles/camera/CameraParameters;->DEBUG:Z

    if-eqz v3, :cond_2

    const-string v3, "goggles.CameraParameters"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting picture size to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v5, v5, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v5, v5, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v4, p0, Lcom/google/android/goggles/camera/CameraParameters;->pictureSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v4, v4, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-virtual {p1, v3, v4}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    if-eqz v3, :cond_6

    const-string v2, "continuous-picture"

    :goto_0
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    sget-boolean v3, Lcom/google/android/goggles/camera/CameraParameters;->DEBUG:Z

    if-eqz v3, :cond_4

    const-string v3, "goggles.CameraParameters"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting focus mode to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {p1, v2}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    sput-object v2, Lcom/google/android/goggles/ui/DebugView;->focusMode:Ljava/lang/String;

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/goggles/camera/CameraParameters;->enableTorch:Z

    if-eqz v3, :cond_8

    const-string v0, "torch"

    :goto_2
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    sget-boolean v3, Lcom/google/android/goggles/camera/CameraParameters;->DEBUG:Z

    if-eqz v3, :cond_5

    const-string v3, "goggles.CameraParameters"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting flash mode to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    :goto_3
    return-void

    :cond_6
    const-string v2, "auto"

    goto :goto_0

    :cond_7
    const-string v3, "error"

    sput-object v3, Lcom/google/android/goggles/ui/DebugView;->focusMode:Ljava/lang/String;

    const-string v3, "goggles.CameraParameters"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Focus mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not available on this device."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_8
    const-string v0, "off"

    goto :goto_2

    :cond_9
    const-string v3, "goggles.CameraParameters"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Flash mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not available on this device."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
