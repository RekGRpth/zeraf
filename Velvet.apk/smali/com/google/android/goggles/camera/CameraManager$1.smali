.class Lcom/google/android/goggles/camera/CameraManager$1;
.super Ljava/lang/Object;
.source "CameraManager.java"

# interfaces
.implements Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraManager;->openCamera()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraManager;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/CameraManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraManager$1;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$1;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # getter for: Lcom/google/android/goggles/camera/CameraManager;->mCameraListener:Lcom/google/android/goggles/camera/CameraManager$Listener;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$300(Lcom/google/android/goggles/camera/CameraManager;)Lcom/google/android/goggles/camera/CameraManager$Listener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraManager$Listener;->onCameraError()V

    return-void
.end method

.method public onCameraOpened(Z)V
    .locals 2
    .param p1    # Z

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$1;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # getter for: Lcom/google/android/goggles/camera/CameraManager;->mCameraOpenCancelled:Z
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$100(Lcom/google/android/goggles/camera/CameraManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$1;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/goggles/camera/CameraManager;->mCameraActive:Z
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/CameraManager;->access$202(Lcom/google/android/goggles/camera/CameraManager;Z)Z

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$1;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # getter for: Lcom/google/android/goggles/camera/CameraManager;->mCameraListener:Lcom/google/android/goggles/camera/CameraManager$Listener;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$300(Lcom/google/android/goggles/camera/CameraManager;)Lcom/google/android/goggles/camera/CameraManager$Listener;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/camera/CameraManager$Listener;->onCameraOpened(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$1;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # invokes: Lcom/google/android/goggles/camera/CameraManager;->setupCameraWhenReady()V
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$400(Lcom/google/android/goggles/camera/CameraManager;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraManager$1;->this$0:Lcom/google/android/goggles/camera/CameraManager;

    # invokes: Lcom/google/android/goggles/camera/CameraManager;->startPreviewWhenReady()V
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraManager;->access$500(Lcom/google/android/goggles/camera/CameraManager;)V

    goto :goto_0
.end method
