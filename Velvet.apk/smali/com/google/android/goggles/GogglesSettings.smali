.class public Lcom/google/android/goggles/GogglesSettings;
.super Ljava/lang/Object;
.source "GogglesSettings.java"


# instance fields
.field private final mApp:Lcom/google/android/velvet/VelvetApplication;

.field private mLoaded:Z

.field private mLogServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

.field private mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

.field private final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/VelvetApplication;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/VelvetApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/goggles/GogglesSettings;->mApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method private checkLoaded()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/goggles/GogglesSettings;->mLoaded:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/goggles/GogglesSettings;->load()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/goggles/GogglesSettings;->mLoaded:Z

    :cond_0
    return-void
.end method

.method private findServer(Ljava/util/List;Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;
    .locals 3
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->hasPairHttpServerInfo()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->hasSingleHttpServerInfo()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getDefaultServer()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method private load()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "goggles.frontend"

    invoke-direct {p0}, Lcom/google/android/goggles/GogglesSettings;->getDefaultServer()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/goggles/GogglesSettings;->loadServerInfo(Ljava/lang/String;)V

    return-void
.end method

.method private loadServerInfo(Ljava/lang/String;)V
    .locals 12
    .param p1    # Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/goggles/GogglesSettings;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/goggles/GogglesSettings;->mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/goggles/GogglesSettings;->mLogServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDebug()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;->getDebugServerList()Ljava/util/List;

    move-result-object v6

    invoke-direct {p0, v6, p1}, Lcom/google/android/goggles/GogglesSettings;->findServer(Ljava/util/List;Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;->getGogglesDebugServerList()Ljava/util/List;

    move-result-object v6

    invoke-direct {p0, v6, p1}, Lcom/google/android/goggles/GogglesSettings;->findServer(Ljava/util/List;Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    move-result-object v5

    :cond_1
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v4

    :try_start_0
    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;->toByteArray()[B

    move-result-object v6

    invoke-static {v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;->parseFrom([B)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;->getUp()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v6

    const v7, 0xc800

    invoke-virtual {v6, v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->setChunkSize(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    iput-object v3, p0, Lcom/google/android/goggles/GogglesSettings;->mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    invoke-virtual {v5}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/goggles/GogglesSettings;->mLogServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    :cond_2
    :try_start_1
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    new-instance v6, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    invoke-direct {v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;-><init>()V

    new-instance v7, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    invoke-direct {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;-><init>()V

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%s/down?pair="

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->setUrl(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;->setDown(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v6

    new-instance v7, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    invoke-direct {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;-><init>()V

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%s/up?pair="

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->setUrl(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v7

    const-string v8, "c548_232a_f5c8_05ff"

    invoke-virtual {v7, v8}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->setHeader(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v7

    const v8, 0xc800

    invoke-virtual {v7, v8}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->setChunkSize(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;->setUp(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/goggles/GogglesSettings;->mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    new-instance v6, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    invoke-direct {v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;-><init>()V

    const-string v7, "%s/fallback"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->setUrl(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/goggles/GogglesSettings;->mLogServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v2

    const-string v6, "goggles.GogglesSettings"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Malformed custom url: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/goggles/GogglesSettings;->mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/goggles/GogglesSettings;->mLogServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    goto/16 :goto_0
.end method


# virtual methods
.method public forceFakeFocus(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "goggles.fakeFocus"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public getAllServers()[Ljava/lang/CharSequence;
    .locals 9

    iget-object v7, p0, Lcom/google/android/goggles/GogglesSettings;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDebug()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;->getDebugServerList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;->getGogglesDebugServerList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v7, v8

    new-array v2, v7, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    invoke-virtual {v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getLabel()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    invoke-virtual {v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getLabel()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    new-array v2, v7, [Ljava/lang/CharSequence;

    :cond_2
    return-object v2
.end method

.method public getAnnotation()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "goggles.annotation"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentServer()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "goggles.frontend"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLogServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/goggles/GogglesSettings;->checkLoaded()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mLogServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    return-object v0
.end method

.method public getServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/goggles/GogglesSettings;->checkLoaded()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    return-object v0
.end method

.method public isTorchOn()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "goggles.torch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setAnnotation(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "goggles.annotation"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setServerInfo(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/GogglesSettings;->loadServerInfo(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "goggles.frontend"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setTorch(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "goggles.torch"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public shouldForceFakeFocus()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "goggles.fakeFocus"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public shouldShowDebugView()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "goggles.showDebugView"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public showDebugView(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/goggles/GogglesSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "goggles.showDebugView"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
