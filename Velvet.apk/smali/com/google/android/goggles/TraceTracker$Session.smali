.class public interface abstract Lcom/google/android/goggles/TraceTracker$Session;
.super Ljava/lang/Object;
.source "TraceTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/TraceTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Session"
.end annotation


# virtual methods
.method public abstract addClientEventLocalBarcodeDetected()V
.end method

.method public abstract addClientEventRecvResponse(I)V
.end method

.method public abstract addClientEventSceneChangeDetected()V
.end method

.method public abstract addClientEventSendRequest(I)V
.end method

.method public abstract addClientEventThumbnailGet(Ljava/lang/String;Z)V
.end method

.method public abstract addImpressionDisambiguation(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;
.end method

.method public abstract addImpressionSingleResult(ILjava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;
.end method

.method public abstract addUserEventDisambigClick(ILjava/lang/String;)V
.end method

.method public abstract addUserEventRequestResults()V
.end method

.method public abstract addUserEventStartSearch(I)V
.end method

.method public abstract addUserEventTextRefinement()V
.end method

.method public abstract build()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;
.end method

.method public abstract end()V
.end method

.method public abstract getSessionId()Ljava/lang/String;
.end method

.method public abstract setSessionId(Ljava/lang/String;)V
.end method
