.class public interface abstract Lcom/google/android/searchcommon/CoreSearchServices;
.super Ljava/lang/Object;
.source "CoreSearchServices.java"


# virtual methods
.method public abstract getAlarmManager()Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;
.end method

.method public abstract getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;
.end method

.method public abstract getBeamHelper()Lcom/google/android/velvet/util/BeamHelper;
.end method

.method public abstract getClock()Lcom/google/android/searchcommon/util/Clock;
.end method

.method public abstract getConfig()Lcom/google/android/searchcommon/SearchConfig;
.end method

.method public abstract getCookies()Lcom/google/android/velvet/Cookies;
.end method

.method public abstract getCorpora()Lcom/google/android/velvet/Corpora;
.end method

.method public abstract getDeviceCapabilityManager()Lcom/google/android/searchcommon/DeviceCapabilityManager;
.end method

.method public abstract getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;
.end method

.method public abstract getImageMetadataController()Lcom/google/android/velvet/gallery/ImageMetadataController;
.end method

.method public abstract getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;
.end method

.method public abstract getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;
.end method

.method public abstract getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;
.end method

.method public abstract getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;
.end method

.method public abstract getPendingIntentFactory()Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;
.end method

.method public abstract getPinholeParamsBuilder()Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;
.end method

.method public abstract getRlzHelper()Lcom/google/android/searchcommon/google/RlzHelper;
.end method

.method public abstract getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;
.end method

.method public abstract getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;
.end method

.method public abstract getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;
.end method

.method public abstract getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;
.end method

.method public abstract getUriRewriter()Lcom/google/android/searchcommon/google/UriRewriter;
.end method

.method public abstract getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;
.end method

.method public abstract getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;
.end method

.method public abstract getVoiceSettings()Lcom/google/android/voicesearch/settings/Settings;
.end method

.method public abstract getWebViewGoogleCookiesLock()Lcom/google/android/searchcommon/util/ForceableLock;
.end method
