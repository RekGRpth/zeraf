.class public Lcom/google/android/searchcommon/SearchSettingsImpl;
.super Ljava/lang/Object;
.source "SearchSettingsImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/SearchSettings;


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object p3, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-void
.end method

.method private getDebugFeatures()Lcom/google/android/searchcommon/debug/DebugFeatures;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    return-object v0
.end method

.method private static getSourceClickStatsPreference(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "source_stats_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSourceEnabledPreference(Lcom/google/android/searchcommon/summons/Source;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/android/searchcommon/summons/Source;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enable_corpus_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setSearchDomain(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "search_domain_scheme"

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "search_domain"

    invoke-interface {v0, v1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "search_domain_country_code"

    invoke-interface {v0, v1, p3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "search_language"

    invoke-interface {v0, v1, p4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "search_domain_apply_time"

    invoke-interface {v0, v1, p5, p6}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public addMenuItems(Landroid/view/Menu;Z)V
    .locals 3
    .param p1    # Landroid/view/Menu;
    .param p2    # Z

    new-instance v0, Landroid/view/MenuInflater;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    const v2, 0x7f120005

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v2, 0x7f1002b2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getSearchSettingsIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    return-void
.end method

.method public broadcastSettingsChanged()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.search.action.SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method protected declared-synchronized createAndSetInstallId()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "install-id"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getBackgroundTaskEarliestNextRun(Ljava/lang/String;)J
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "background_task_earliest_next_run_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getBackgroundTaskForcedRun(Ljava/lang/String;)J
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "background_task_forced_run_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getCachedUserAgentBase()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "user_agent"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCachedZeroQueryWebResults()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "zero_query_web_results"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDebugFeaturesLevel()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "debug_features_level"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getDebugSearchDomainOverride()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "debug_search_domain_override"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDebugSearchSchemeOverride()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "debug_search_scheme_override"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGoogleAccountToUse()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "google_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastApplicationLaunch()J
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "last_launch"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastRunVersion()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "last_run_version"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method public getPersonalizedSearchEnabled()Z
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "personalized_search_bool"

    iget-object v2, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRefreshWebViewCookiesAt()J
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "refresh_webview_cookies_at"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSafeSearch()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "safe_search_settings"

    const-string v2, "images"

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchDomain()Ljava/lang/String;
    .locals 4

    invoke-direct {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getDebugFeatures()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/debug/DebugFeatures;->dogfoodDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getDebugSearchDomainOverride()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    const-string v2, "search_domain"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSearchDomainCountryCode()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "search_domain_country_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchDomainLanguage()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "search_language"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchDomainScheme()Ljava/lang/String;
    .locals 4

    invoke-direct {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getDebugFeatures()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/debug/DebugFeatures;->dogfoodDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getDebugSearchSchemeOverride()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    const-string v2, "search_domain_scheme"

    const-string v3, "http"

    invoke-interface {v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSearchSettingsIntent()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.search.action.SEARCH_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getSignedOut()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "signed_out"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSourceClickStats(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getSourceClickStatsPreference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getVoiceSearchInstallId()Ljava/lang/String;
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    const-string v2, "install-id"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->createAndSetInstallId()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getWebCorpora()[B
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "web_corpora_config"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBytes(Ljava/lang/String;[B)[B

    move-result-object v0

    return-object v0
.end method

.method public getWebCorporaConfigUrl()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "web_corpora_config_url"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebViewLoggedInAccount()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "webview_logged_in_account"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebViewLoggedInDomain()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "webview_logged_in_domain"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSingleRequestArchitectureEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    return v0
.end method

.method public isSourceEnabled(Lcom/google/android/searchcommon/summons/Source;)Z
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->isEnabledByDefault()Z

    move-result v0

    invoke-static {p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getSourceEnabledPreference(Lcom/google/android/searchcommon/summons/Source;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    return v2
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/GsaPreferenceController;->registerChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public setBackgroundTaskEarliestNextRun(Ljava/lang/String;J)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # J

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "background_task_earliest_next_run_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeLongInMainPrefs(Ljava/lang/String;J)V

    return-void
.end method

.method public setBackgroundTaskForcedRun(Ljava/lang/String;J)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # J

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "background_task_forced_run_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeLongInMainPrefs(Ljava/lang/String;J)V

    return-void
.end method

.method public setCachedUserAgentBase(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "user_agent"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCachedZeroQueryWebResults(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "zero_query_web_results"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setDebugFeaturesLevel(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "debug_features_level"

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method public setGoogleAccountToUse(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    if-nez p1, :cond_0

    const-string v1, "google_account"

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    :goto_0
    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void

    :cond_0
    const-string v1, "google_account"

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0
.end method

.method public setGservicesOverridesJson(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "gservices_overrides"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setLastApplicationLaunch(J)V
    .locals 1
    .param p1    # J

    const-string v0, "last_launch"

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeLongInMainPrefs(Ljava/lang/String;J)V

    return-void
.end method

.method public setLastRunVersion(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "last_run_version"

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method public setRefreshWebViewCookiesAt(J)V
    .locals 1
    .param p1    # J

    const-string v0, "refresh_webview_cookies_at"

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeLongInMainPrefs(Ljava/lang/String;J)V

    return-void
.end method

.method public setSearchDomain(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/searchcommon/SearchSettingsImpl;->setSearchDomain(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public setSignedOut(Z)V
    .locals 2
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "signed_out"

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method public setSourceClickStats(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getSourceClickStatsPreference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setUseGoogleCom(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "use_google_com"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeBooleanInMainPrefs(Ljava/lang/String;Z)V

    return-void
.end method

.method public setWebCorpora([B)V
    .locals 1
    .param p1    # [B

    const-string v0, "web_corpora_config"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeBytesInMainPrefs(Ljava/lang/String;[B)V

    return-void
.end method

.method public setWebCorporaConfigUrl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "web_corpora_config_url"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setWebViewLoggedInAccount(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "webview_logged_in_account"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setWebViewLoggedInDomain(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "webview_logged_in_domain"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/SearchSettingsImpl;->storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public shouldForceHandsFreeDebugFlag()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "debug_force_hands_free"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public shouldUseGoogleCom()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "use_google_com"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected storeBooleanInMainPrefs(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method protected storeBytesInMainPrefs(Ljava/lang/String;[B)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method protected storeLongInMainPrefs(Ljava/lang/String;J)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method protected storeStringInMainPrefs(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchSettingsImpl;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method public unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/GsaPreferenceController;->unregisterChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method
