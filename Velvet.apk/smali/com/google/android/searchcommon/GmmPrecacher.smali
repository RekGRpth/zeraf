.class public Lcom/google/android/searchcommon/GmmPrecacher;
.super Ljava/lang/Object;
.source "GmmPrecacher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/GmmPrecacher$1;,
        Lcom/google/android/searchcommon/GmmPrecacher$ServiceHandler;
    }
.end annotation


# static fields
.field private static final RESULT_MESSAGES:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mIdentifierIntent:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/searchcommon/GmmPrecacher;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/GmmPrecacher;->TAG:Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Success"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Invalid request"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Package or certificate invalid"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Invalid locations"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/searchcommon/GmmPrecacher;->RESULT_MESSAGES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/GmmPrecacher;)Landroid/app/PendingIntent;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/GmmPrecacher;

    iget-object v0, p0, Lcom/google/android/searchcommon/GmmPrecacher;->mIdentifierIntent:Landroid/app/PendingIntent;

    return-object v0
.end method


# virtual methods
.method public precache(Landroid/content/Context;Ljava/util/List;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;)V"
        }
    .end annotation

    const/4 v10, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/location/Location;

    if-nez v1, :cond_0

    const-string v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/searchcommon/GmmPrecacher;->mIdentifierIntent:Landroid/app/PendingIntent;

    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.google.android.apps.maps.PREFETCH"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v7, 0x10000000

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v7, "messenger"

    new-instance v8, Landroid/os/Messenger;

    new-instance v9, Lcom/google/android/searchcommon/GmmPrecacher$ServiceHandler;

    const/4 v10, 0x0

    invoke-direct {v9, p0, v10}, Lcom/google/android/searchcommon/GmmPrecacher$ServiceHandler;-><init>(Lcom/google/android/searchcommon/GmmPrecacher;Lcom/google/android/searchcommon/GmmPrecacher$1;)V

    invoke-direct {v8, v9}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v7, "sender"

    iget-object v8, p0, Lcom/google/android/searchcommon/GmmPrecacher;->mIdentifierIntent:Landroid/app/PendingIntent;

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v7, "locations"

    invoke-virtual {v4, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v7

    goto :goto_1
.end method
