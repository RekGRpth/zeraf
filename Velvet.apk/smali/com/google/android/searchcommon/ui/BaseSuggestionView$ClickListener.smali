.class Lcom/google/android/searchcommon/ui/BaseSuggestionView$ClickListener;
.super Ljava/lang/Object;
.source "BaseSuggestionView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/ui/BaseSuggestionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/ui/BaseSuggestionView;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/ui/BaseSuggestionView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView$ClickListener;->this$0:Lcom/google/android/searchcommon/ui/BaseSuggestionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/ui/BaseSuggestionView;Lcom/google/android/searchcommon/ui/BaseSuggestionView$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/ui/BaseSuggestionView;
    .param p2    # Lcom/google/android/searchcommon/ui/BaseSuggestionView$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/ui/BaseSuggestionView$ClickListener;-><init>(Lcom/google/android/searchcommon/ui/BaseSuggestionView;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/BaseSuggestionView$ClickListener;->this$0:Lcom/google/android/searchcommon/ui/BaseSuggestionView;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->onSuggestionClicked()V

    return-void
.end method
