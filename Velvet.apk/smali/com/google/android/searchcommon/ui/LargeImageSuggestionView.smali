.class public Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;
.super Lcom/google/android/searchcommon/ui/DefaultSuggestionView;
.source "LargeImageSuggestionView.java"


# instance fields
.field private mAsyncIcon2:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

.field private mBackgroundImage:Landroid/widget/ImageView;

.field private mDividerBase:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mAsyncIcon2:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionSource()Lcom/google/android/searchcommon/summons/Source;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIconLarge()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->set(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->onFinishInflate()V

    const v0, 0x7f100152

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mBackgroundImage:Landroid/widget/ImageView;

    new-instance v0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mBackgroundImage:Landroid/widget/ImageView;

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;-><init>(Landroid/widget/ImageView;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mAsyncIcon2:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    const v0, 0x7f100154

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mDividerBase:Landroid/view/View;

    return-void
.end method

.method public setDividerVisibility(I)V
    .locals 1
    .param p1    # I

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mDividerBase:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->setDividerVisibility(I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->setEnabled(Z)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mBackgroundImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mBackgroundImage:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mBackgroundImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/LargeImageSuggestionView;->mBackgroundImage:Landroid/widget/ImageView;

    const/high16 v1, 0x3f000000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method
