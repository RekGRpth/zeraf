.class public Lcom/google/android/searchcommon/util/InputStreamChunkProducer;
.super Lcom/google/android/searchcommon/util/ChunkProducer;
.source "InputStreamChunkProducer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;
    }
.end annotation


# instance fields
.field private mInputStreamSupplier:Lcom/google/common/io/InputSupplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/io/InputSupplier",
            "<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mSourceStream:Ljava/io/InputStream;

.field private mStopWasCalled:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/util/ChunkProducer;-><init>(Ljava/util/concurrent/ExecutorService;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mLock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mSourceStream:Ljava/io/InputStream;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mStopWasCalled:Z

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$1;-><init>(Lcom/google/android/searchcommon/util/InputStreamChunkProducer;Ljava/io/InputStream;)V

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->setInputStreamSupplier(Lcom/google/common/io/InputSupplier;)V

    return-void
.end method

.method protected constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/ChunkProducer;-><init>(Ljava/util/concurrent/ExecutorService;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mLock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mSourceStream:Ljava/io/InputStream;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mStopWasCalled:Z

    return-void
.end method


# virtual methods
.method protected bufferAllData(Ljava/io/InputStream;Lcom/google/android/searchcommon/util/Consumer;)Z
    .locals 8
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;
        }
    .end annotation

    const/16 v7, 0x400

    const/4 v5, 0x0

    :cond_0
    new-array v0, v7, [B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v7, :cond_1

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->throwIOExceptionIfStopped(Ljava/lang/Exception;)V

    const/4 v2, 0x0

    rsub-int v6, v1, 0x400

    :try_start_0
    invoke-virtual {p1, v0, v1, v6}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-gez v2, :cond_2

    :cond_1
    if-nez v1, :cond_3

    const/4 v6, 0x1

    return v6

    :catch_0
    move-exception v4

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->throwIOExceptionIfStopped(Ljava/lang/Exception;)V

    throw v4

    :cond_2
    add-int/2addr v1, v2

    goto :goto_0

    :cond_3
    if-ne v1, v7, :cond_4

    new-instance v3, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;

    invoke-direct {v3, v0}, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;-><init>([B)V

    :goto_1
    invoke-interface {p2, v3}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    const/high16 v6, 0x400000

    if-le v5, v6, :cond_5

    new-instance v6, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;

    invoke-direct {v6}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;-><init>()V

    throw v6

    :cond_4
    new-instance v3, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;

    invoke-direct {v3, v0, v1}, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;-><init>([BI)V

    goto :goto_1

    :cond_5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/InterruptedException;

    invoke-direct {v6}, Ljava/lang/InterruptedException;-><init>()V

    throw v6
.end method

.method public close()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mStopWasCalled:Z

    iget-object v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mSourceStream:Ljava/io/InputStream;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$2;-><init>(Lcom/google/android/searchcommon/util/InputStreamChunkProducer;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->cancelAndInterruptBufferTask()V

    goto :goto_0
.end method

.method protected closeSource(Ljava/io/InputStream;Z)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .param p2    # Z

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :cond_0
    return-void
.end method

.method protected runBufferTask(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;)V"
        }
    .end annotation

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mInputStreamSupplier:Lcom/google/common/io/InputSupplier;

    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mLock:Ljava/lang/Object;

    monitor-enter v9
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    iget-boolean v6, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mStopWasCalled:Z

    if-eqz v6, :cond_1

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v6}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    invoke-virtual {p0, v4, v5}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->closeSource(Ljava/io/InputStream;Z)V

    if-eqz v5, :cond_0

    move v6, v7

    :goto_0
    invoke-virtual {p0, v6}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->setState(I)V

    :goto_1
    return-void

    :cond_0
    move v6, v8

    goto :goto_0

    :cond_1
    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v6, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mInputStreamSupplier:Lcom/google/common/io/InputSupplier;

    invoke-interface {v6}, Lcom/google/common/io/InputSupplier;->getInput()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/io/InputStream;

    move-object v4, v0

    if-eqz v4, :cond_6

    iget-object v9, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mLock:Ljava/lang/Object;

    monitor-enter v9
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    iget-boolean v6, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mStopWasCalled:Z

    if-eqz v6, :cond_5

    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v6}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    invoke-virtual {p0, v4, v5}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->closeSource(Ljava/io/InputStream;Z)V

    if-eqz v5, :cond_4

    :goto_2
    invoke-virtual {p0, v7}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->setState(I)V

    goto :goto_1

    :catchall_0
    move-exception v6

    :try_start_5
    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v6
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catch_0
    move-exception v1

    :try_start_7
    const-string v6, "Search.InputStreamChunkProducer"

    const-string v9, "Buffering thread was interrupted"

    invoke-static {v6, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    iget-object v9, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mLock:Ljava/lang/Object;

    monitor-enter v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :try_start_8
    iget-boolean v6, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mStopWasCalled:Z

    if-nez v6, :cond_2

    new-instance v3, Ljava/io/IOException;

    const-string v6, "Buffering thread was interrupted"

    invoke-direct {v3, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    :cond_2
    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    if-eqz v2, :cond_3

    :try_start_9
    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    invoke-direct {v6, v2}, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;-><init>(Ljava/io/IOException;)V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :cond_3
    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v6}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    invoke-virtual {p0, v4, v5}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->closeSource(Ljava/io/InputStream;Z)V

    if-eqz v5, :cond_8

    :goto_3
    invoke-virtual {p0, v7}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->setState(I)V

    goto :goto_1

    :cond_4
    move v7, v8

    goto :goto_2

    :cond_5
    :try_start_a
    iput-object v4, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mSourceStream:Ljava/io/InputStream;

    monitor-exit v9
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    invoke-virtual {p0, v4, p1}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->bufferAllData(Ljava/io/InputStream;Lcom/google/android/searchcommon/util/Consumer;)Z
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    move-result v5

    if-eqz v5, :cond_6

    :cond_6
    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v6}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    invoke-virtual {p0, v4, v5}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->closeSource(Ljava/io/InputStream;Z)V

    if-eqz v5, :cond_7

    :goto_4
    invoke-virtual {p0, v7}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->setState(I)V

    goto :goto_1

    :catchall_1
    move-exception v6

    :try_start_c
    monitor-exit v9
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :try_start_d
    throw v6
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0
    .catch Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :catch_1
    move-exception v1

    :try_start_e
    const-string v6, "Search.InputStreamChunkProducer"

    const-string v9, "Buffered data exceeded maximum size. Closing stream."

    invoke-static {v6, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/io/IOException;

    const-string v6, "Buffered data exceeded maximum size"

    invoke-direct {v2, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    invoke-direct {v6, v2}, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;-><init>(Ljava/io/IOException;)V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v6}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    invoke-virtual {p0, v4, v5}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->closeSource(Ljava/io/InputStream;Z)V

    if-eqz v5, :cond_9

    :goto_5
    invoke-virtual {p0, v7}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->setState(I)V

    goto/16 :goto_1

    :cond_7
    move v7, v8

    goto :goto_4

    :catchall_2
    move-exception v6

    :try_start_f
    monitor-exit v9
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    :try_start_10
    throw v6
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :catchall_3
    move-exception v6

    new-instance v9, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v9}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-interface {p1, v9}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    invoke-virtual {p0, v4, v5}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->closeSource(Ljava/io/InputStream;Z)V

    if-eqz v5, :cond_d

    :goto_6
    invoke-virtual {p0, v7}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->setState(I)V

    throw v6

    :cond_8
    move v7, v8

    goto :goto_3

    :cond_9
    move v7, v8

    goto :goto_5

    :catch_2
    move-exception v1

    const/4 v2, 0x0

    :try_start_11
    iget-object v9, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mLock:Ljava/lang/Object;

    monitor-enter v9
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    :try_start_12
    iget-boolean v6, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mStopWasCalled:Z

    if-eqz v6, :cond_b

    const-string v6, "Search.InputStreamChunkProducer"

    const-string v10, "IOException from source stream. This may simply be due to the stream being closed."

    invoke-static {v6, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_7
    monitor-exit v9
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    if-eqz v2, :cond_a

    :try_start_13
    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    invoke-direct {v6, v1}, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;-><init>(Ljava/io/IOException;)V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    :cond_a
    new-instance v6, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v6}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-interface {p1, v6}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    invoke-virtual {p0, v4, v5}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->closeSource(Ljava/io/InputStream;Z)V

    if-eqz v5, :cond_c

    :goto_8
    invoke-virtual {p0, v7}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->setState(I)V

    goto/16 :goto_1

    :cond_b
    :try_start_14
    const-string v6, "Search.InputStreamChunkProducer"

    const-string v10, "Exception while buffering stream"

    invoke-static {v6, v10, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v3, Ljava/io/IOException;

    const-string v6, "Exception while reading source"

    invoke-direct {v3, v6, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v3

    goto :goto_7

    :catchall_4
    move-exception v6

    monitor-exit v9
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :try_start_15
    throw v6
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    :cond_c
    move v7, v8

    goto :goto_8

    :cond_d
    move v7, v8

    goto :goto_6
.end method

.method protected setInputStreamSupplier(Lcom/google/common/io/InputSupplier;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/io/InputSupplier",
            "<",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mInputStreamSupplier:Lcom/google/common/io/InputSupplier;

    return-void
.end method

.method public start(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mInputStreamSupplier:Lcom/google/common/io/InputSupplier;

    const-string v1, "Should have called setInputStreamSupplier by now."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/util/ChunkProducer;->start(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public throwIOExceptionIfStopped(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->mStopWasCalled:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Source stream was closed"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Source stream was closed"

    invoke-direct {v0, v2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
