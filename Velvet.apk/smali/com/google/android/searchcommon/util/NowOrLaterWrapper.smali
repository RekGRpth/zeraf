.class public abstract Lcom/google/android/searchcommon/util/NowOrLaterWrapper;
.super Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;
.source "NowOrLaterWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumerWithProgress;,
        Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase",
        "<TA;TB;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/NowOrLater;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/NowOrLater",
            "<+TA;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;-><init>(Lcom/google/android/searchcommon/util/NowOrLater;)V

    return-void
.end method


# virtual methods
.method protected createConsumer(Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;)",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<TA;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumer;-><init>(Lcom/google/android/searchcommon/util/NowOrLaterWrapper;Lcom/google/android/searchcommon/util/Consumer;)V

    return-object v0
.end method

.method protected createConsumerWithProgress(Lcom/google/android/searchcommon/util/ConsumerWithProgress;)Lcom/google/android/searchcommon/util/ConsumerWithProgress;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;)",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<TA;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumerWithProgress;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumerWithProgress;-><init>(Lcom/google/android/searchcommon/util/NowOrLaterWrapper;Lcom/google/android/searchcommon/util/ConsumerWithProgress;)V

    return-object v0
.end method

.method public abstract get(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)TB;"
        }
    .end annotation
.end method

.method public getNow()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TB;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/NowOrLaterWrapper;->mWrapped:Lcom/google/android/searchcommon/util/NowOrLater;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/util/NowOrLaterWrapper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
