.class public Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException;
.super Lcom/google/android/searchcommon/util/HttpHelper$HttpException;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/HttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpRedirectException"
.end annotation


# instance fields
.field private final mLocation:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/util/HttpHelper$HttpException;-><init>(ILjava/lang/String;)V

    iput-object p3, p0, Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException;->mLocation:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getRedirectLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException;->mLocation:Ljava/lang/String;

    return-object v0
.end method
