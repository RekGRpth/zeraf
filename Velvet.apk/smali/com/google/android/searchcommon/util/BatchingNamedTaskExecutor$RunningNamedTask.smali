.class Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;
.super Ljava/lang/Object;
.source "BatchingNamedTaskExecutor.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/NamedTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RunningNamedTask"
.end annotation


# instance fields
.field private final mTask:Lcom/google/android/searchcommon/util/NamedTask;

.field final synthetic this$0:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;Lcom/google/android/searchcommon/util/NamedTask;)V
    .locals 0
    .param p2    # Lcom/google/android/searchcommon/util/NamedTask;

    iput-object p1, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;->this$0:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;->mTask:Lcom/google/android/searchcommon/util/NamedTask;

    return-void
.end method

.method private removeFromRunningTasks()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;->this$0:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    # getter for: Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->access$000(Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;)Ljava/util/Set;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;->this$0:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    # getter for: Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->mRunningTasks:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->access$000(Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public cancelExecution()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;->mTask:Lcom/google/android/searchcommon/util/NamedTask;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NamedTask;->cancelExecution()V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;->mTask:Lcom/google/android/searchcommon/util/NamedTask;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NamedTask;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;->mTask:Lcom/google/android/searchcommon/util/NamedTask;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NamedTask;->run()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor$RunningNamedTask;->removeFromRunningTasks()V

    return-void
.end method
