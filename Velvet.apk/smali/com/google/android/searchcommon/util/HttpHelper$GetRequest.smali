.class public Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;
.super Ljava/lang/Object;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/HttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GetRequest"
.end annotation


# instance fields
.field private mFollowRedirects:Z

.field private mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxStaleSecs:I

.field private mUrl:Ljava/lang/String;

.field private mUseCaches:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mFollowRedirects:Z

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mUseCaches:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mFollowRedirects:Z

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mUseCaches:Z

    iput-object p1, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mUrl:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mHeaders:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mHeaders:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mHeaders:Ljava/util/Map;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public getFollowRedirects()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mFollowRedirects:Z

    return v0
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mHeaders:Ljava/util/Map;

    return-object v0
.end method

.method public getMaxStaleSecs()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mMaxStaleSecs:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUseCaches()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mUseCaches:Z

    return v0
.end method

.method public setFollowRedirects(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mFollowRedirects:Z

    return-void
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mHeaders:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mHeaders:Ljava/util/Map;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mHeaders:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setMaxStaleSecs(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mMaxStaleSecs:I

    return-void
.end method

.method public setUseCaches(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->mUseCaches:Z

    return-void
.end method
