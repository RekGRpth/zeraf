.class public interface abstract Lcom/google/android/searchcommon/util/ConsumerWithProgress;
.super Ljava/lang/Object;
.source "ConsumerWithProgress.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<TA;>;"
    }
.end annotation


# virtual methods
.method public abstract consumePartial(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)Z"
        }
    .end annotation
.end method

.method public abstract progressChanged(I)V
.end method
