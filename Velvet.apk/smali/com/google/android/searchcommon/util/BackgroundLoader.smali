.class public Lcom/google/android/searchcommon/util/BackgroundLoader;
.super Ljava/lang/Object;
.source "BackgroundLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/UriLoader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/UriLoader",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private final mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private final mLoader:Lcom/google/android/searchcommon/util/SynchronousLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/SynchronousLoader",
            "<+TA;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/SynchronousLoader;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V
    .locals 0
    .param p2    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/SynchronousLoader",
            "<+TA;>;",
            "Lcom/google/android/searchcommon/util/NamedTaskExecutor;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/BackgroundLoader;->mLoader:Lcom/google/android/searchcommon/util/SynchronousLoader;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/BackgroundLoader;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    return-void
.end method


# virtual methods
.method public load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<TA;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/LaterTask;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/BackgroundLoader;->mLoader:Lcom/google/android/searchcommon/util/SynchronousLoader;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/BackgroundLoader;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/searchcommon/util/LaterTask;-><init>(Landroid/net/Uri;Lcom/google/android/searchcommon/util/SynchronousLoader;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    return-object v0
.end method
