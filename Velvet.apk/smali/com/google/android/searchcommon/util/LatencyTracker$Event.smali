.class public Lcom/google/android/searchcommon/util/LatencyTracker$Event;
.super Ljava/lang/Object;
.source "LatencyTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/LatencyTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Event"
.end annotation


# instance fields
.field public final latency:I

.field final synthetic this$0:Lcom/google/android/searchcommon/util/LatencyTracker;

.field public final type:I


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/util/LatencyTracker;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/searchcommon/util/LatencyTracker$Event;->this$0:Lcom/google/android/searchcommon/util/LatencyTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/searchcommon/util/LatencyTracker$Event;->type:I

    invoke-virtual {p1}, Lcom/google/android/searchcommon/util/LatencyTracker;->getLatency()I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/util/LatencyTracker$Event;->latency:I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
