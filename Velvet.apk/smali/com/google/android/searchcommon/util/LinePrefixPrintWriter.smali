.class public Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;
.super Ljava/io/PrintWriter;
.source "LinePrefixPrintWriter.java"


# instance fields
.field private final mLinePrefixWriter:Lcom/google/android/searchcommon/util/LinePrefixWriter;


# direct methods
.method public constructor <init>(Ljava/io/Writer;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/Writer;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/util/LinePrefixWriter;

    invoke-direct {v0, p1, p2}, Lcom/google/android/searchcommon/util/LinePrefixWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->out:Ljava/io/Writer;

    check-cast v0, Lcom/google/android/searchcommon/util/LinePrefixWriter;

    iput-object v0, p0, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->mLinePrefixWriter:Lcom/google/android/searchcommon/util/LinePrefixWriter;

    return-void
.end method


# virtual methods
.method public addToPrefix(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->flush()V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->mLinePrefixWriter:Lcom/google/android/searchcommon/util/LinePrefixWriter;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/util/LinePrefixWriter;->addToPrefix(Ljava/lang/String;)V

    return-void
.end method
