.class Lcom/google/android/searchcommon/util/SystemClockImpl$1;
.super Ljava/lang/Object;
.source "SystemClockImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/util/SystemClockImpl;->registerTimeResetListener(Lcom/google/android/searchcommon/util/Clock$TimeResetListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher",
        "<",
        "Lcom/google/android/searchcommon/util/Clock$TimeResetListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/SystemClockImpl;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/util/SystemClockImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/util/SystemClockImpl$1;->this$0:Lcom/google/android/searchcommon/util/SystemClockImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatch(Lcom/google/android/searchcommon/util/Clock$TimeResetListener;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/util/Clock$TimeResetListener;

    invoke-interface {p1}, Lcom/google/android/searchcommon/util/Clock$TimeResetListener;->onTimeReset()V

    return-void
.end method

.method public bridge synthetic dispatch(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/util/Clock$TimeResetListener;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/util/SystemClockImpl$1;->dispatch(Lcom/google/android/searchcommon/util/Clock$TimeResetListener;)V

    return-void
.end method
