.class public Lcom/google/android/searchcommon/util/UriDiff;
.super Ljava/lang/Object;
.source "UriDiff.java"


# static fields
.field public static final SAME:Lcom/google/android/searchcommon/util/UriDiff;


# instance fields
.field private final mAuthority:Z

.field private final mFragmentParams:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPath:Z

.field private final mQueryParams:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mScheme:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/searchcommon/util/UriDiff;

    invoke-direct {v0}, Lcom/google/android/searchcommon/util/UriDiff;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/util/UriDiff;->SAME:Lcom/google/android/searchcommon/util/UriDiff;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mScheme:Z

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mAuthority:Z

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mPath:Z

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mQueryParams:Ljava/util/Set;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mFragmentParams:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(ZZZLjava/util/Set;Ljava/util/Set;)V
    .locals 1
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/searchcommon/util/UriDiff;->mScheme:Z

    iput-boolean p2, p0, Lcom/google/android/searchcommon/util/UriDiff;->mAuthority:Z

    iput-boolean p3, p0, Lcom/google/android/searchcommon/util/UriDiff;->mPath:Z

    invoke-static {p4}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mQueryParams:Ljava/util/Set;

    invoke-static {p5}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mFragmentParams:Ljava/util/Set;

    return-void
.end method

.method public static diff(Landroid/net/Uri;Landroid/net/Uri;)Lcom/google/android/searchcommon/util/UriDiff;
    .locals 7
    .param p0    # Landroid/net/Uri;
    .param p1    # Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/searchcommon/util/UriDiff;->SAME:Lcom/google/android/searchcommon/util/UriDiff;

    :goto_0
    return-object v0

    :cond_0
    if-nez p0, :cond_1

    new-instance v0, Lcom/google/android/searchcommon/util/UriDiff;

    invoke-virtual {p1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/searchcommon/util/UriDiff;->fragmentAsQuery(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v5

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/util/UriDiff;-><init>(ZZZLjava/util/Set;Ljava/util/Set;)V

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Lcom/google/android/searchcommon/util/UriDiff;

    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v4

    invoke-static {p0}, Lcom/google/android/searchcommon/util/UriDiff;->fragmentAsQuery(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v5

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/util/UriDiff;-><init>(ZZZLjava/util/Set;Ljava/util/Set;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/searchcommon/util/UriDiff;

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move v6, v1

    :goto_1
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v1

    :goto_2
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    move v3, v1

    :cond_3
    invoke-static {p0, p1}, Lcom/google/android/searchcommon/util/UriDiff;->diffQueryParams(Landroid/net/Uri;Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v4

    invoke-static {p0}, Lcom/google/android/searchcommon/util/UriDiff;->fragmentAsQuery(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/searchcommon/util/UriDiff;->fragmentAsQuery(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/google/android/searchcommon/util/UriDiff;->diffQueryParams(Landroid/net/Uri;Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v5

    move v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/util/UriDiff;-><init>(ZZZLjava/util/Set;Ljava/util/Set;)V

    goto :goto_0

    :cond_4
    move v6, v3

    goto :goto_1

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method private static diffQueryParams(Landroid/net/Uri;Landroid/net/Uri;)Ljava/util/Set;
    .locals 8
    .param p0    # Landroid/net/Uri;
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v3

    new-instance v5, Ljava/util/HashSet;

    invoke-static {v0, v3}, Lcom/google/common/collect/Sets;->union(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0, v6}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-object v5
.end method

.method private static fragmentAsQuery(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p0    # Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public authorityDifferent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mAuthority:Z

    return v0
.end method

.method public fragmentDiffs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mFragmentParams:Ljava/util/Set;

    return-object v0
.end method

.method public pathDifferent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mPath:Z

    return v0
.end method

.method public queryDiffs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mQueryParams:Ljava/util/Set;

    return-object v0
.end method

.method public schemeDifferent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/UriDiff;->mScheme:Z

    return v0
.end method
