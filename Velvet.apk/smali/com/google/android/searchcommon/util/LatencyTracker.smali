.class public Lcom/google/android/searchcommon/util/LatencyTracker;
.super Lcom/google/android/searchcommon/util/Latency;
.source "LatencyTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/LatencyTracker$1;,
        Lcom/google/android/searchcommon/util/LatencyTracker$Event;,
        Lcom/google/android/searchcommon/util/LatencyTracker$EventList;
    }
.end annotation


# instance fields
.field private mEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/util/LatencyTracker$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/util/Latency;-><init>(Lcom/google/android/searchcommon/util/Clock;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/LatencyTracker;->mTag:Ljava/lang/String;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/LatencyTracker;->mEvents:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getLatencyEvents()Ljava/lang/String;
    .locals 4

    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/LatencyTracker;->mEvents:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/LatencyTracker$Event;

    iget v3, v0, Lcom/google/android/searchcommon/util/LatencyTracker$Event;->type:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget v3, v0, Lcom/google/android/searchcommon/util/LatencyTracker$Event;->latency:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public logLatencyEvents(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LATENCY["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/util/LatencyTracker;->mTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/LatencyTracker;->getLatencyEvents()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/LatencyTracker;->getStartTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/searchcommon/util/LatencyTracker;->mEvents:Ljava/util/List;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;-><init>(JLjava/util/List;Lcom/google/android/searchcommon/util/LatencyTracker$1;)V

    invoke-static {p1, v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/LatencyTracker;->mEvents:Ljava/util/List;

    return-void
.end method

.method public reportLatencyEvent(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Lcom/google/android/searchcommon/util/LatencyTracker$Event;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/LatencyTracker$Event;-><init>(Lcom/google/android/searchcommon/util/LatencyTracker;I)V

    iget-object v1, p0, Lcom/google/android/searchcommon/util/LatencyTracker;->mEvents:Ljava/util/List;

    new-instance v2, Lcom/google/android/searchcommon/util/LatencyTracker$Event;

    invoke-direct {v2, p0, p1}, Lcom/google/android/searchcommon/util/LatencyTracker$Event;-><init>(Lcom/google/android/searchcommon/util/LatencyTracker;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public reset()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/searchcommon/util/Latency;->reset()V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LatencyTracker;->mEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
