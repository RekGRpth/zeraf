.class public Lcom/google/android/searchcommon/util/PostToExecutorLoader;
.super Ljava/lang/Object;
.source "PostToExecutorLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/UriLoader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/UriLoader",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mWrapped:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<TA;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/UriLoader;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<TA;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/PostToExecutorLoader;->mExecutor:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/PostToExecutorLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    return-void
.end method


# virtual methods
.method public getWrapped()Lcom/google/android/searchcommon/util/UriLoader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<TA;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/PostToExecutorLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    return-object v0
.end method

.method public load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<TA;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/PostToExecutorLater;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/PostToExecutorLoader;->mExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/PostToExecutorLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-interface {v2, p1}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/util/PostToExecutorLater;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/NowOrLater;)V

    return-object v0
.end method
