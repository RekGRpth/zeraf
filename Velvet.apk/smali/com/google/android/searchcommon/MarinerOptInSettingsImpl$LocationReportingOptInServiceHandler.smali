.class Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;
.super Landroid/os/Handler;
.source "MarinerOptInSettingsImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationReportingOptInServiceHandler"
.end annotation


# instance fields
.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field final synthetic this$0:Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->this$0:Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    # getter for: Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->access$000(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "location_reporting_opt_in_handler"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v2, 0x3afc

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->this$0:Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    # getter for: Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mIdentifierIntent:Landroid/app/PendingIntent;
    invoke-static {v0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->access$100(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)Landroid/app/PendingIntent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->this$0:Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    # getter for: Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mIdentifierIntent:Landroid/app/PendingIntent;
    invoke-static {v0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->access$100(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->this$0:Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    # getter for: Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mServiceHandlers:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->access$200(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget v0, p1, Landroid/os/Message;->what:I

    if-ltz v0, :cond_5

    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->RESULT_MESSAGES:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->access$300()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_5

    :cond_3
    :goto_1
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->this$0:Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    # invokes: Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->cancelLatitudeOptInAlarm()V
    invoke-static {v0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->access$400(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_5
    :try_start_2
    iget v0, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->RESULT_MESSAGES:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->access$300()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget v0, p1, Landroid/os/Message;->what:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_6
    throw v0
.end method
