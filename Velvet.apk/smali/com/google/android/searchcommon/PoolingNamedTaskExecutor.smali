.class public Lcom/google/android/searchcommon/PoolingNamedTaskExecutor;
.super Ljava/lang/Object;
.source "PoolingNamedTaskExecutor.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;


# instance fields
.field private final mPool:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(ILjava/util/concurrent/ThreadFactory;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/util/concurrent/ThreadFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1, p2}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->createSafeScheduledExecutorService(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/PoolingNamedTaskExecutor;->mPool:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method


# virtual methods
.method public cancelPendingTasks()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/PoolingNamedTaskExecutor;->mPool:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    return-void
.end method

.method public execute(Lcom/google/android/searchcommon/util/NamedTask;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/NamedTask;

    iget-object v0, p0, Lcom/google/android/searchcommon/PoolingNamedTaskExecutor;->mPool:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public executeDelayed(Lcom/google/android/searchcommon/util/NamedTask;J)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/NamedTask;
    .param p2    # J

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Delay must not be negative."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/PoolingNamedTaskExecutor;->mPool:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p1, p2, p3, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public submit(Lcom/google/android/searchcommon/util/NamedTask;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/NamedTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/NamedTask;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/PoolingNamedTaskExecutor;->mPool:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method
