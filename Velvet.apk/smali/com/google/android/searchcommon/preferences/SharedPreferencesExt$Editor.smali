.class public interface abstract Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.super Ljava/lang/Object;
.source "SharedPreferencesExt.java"

# interfaces
.implements Landroid/content/SharedPreferences$Editor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Editor"
.end annotation


# virtual methods
.method public abstract clear()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method

.method public abstract putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method

.method public abstract putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method

.method public abstract putFloat(Ljava/lang/String;F)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method

.method public abstract putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method

.method public abstract putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method

.method public abstract putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method

.method public abstract putStringSet(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;"
        }
    .end annotation
.end method

.method public abstract remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method
