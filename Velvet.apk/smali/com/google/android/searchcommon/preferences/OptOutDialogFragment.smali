.class public Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;
.super Landroid/app/DialogFragment;
.source "OptOutDialogFragment.java"


# instance fields
.field private mTarget:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;)Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->mTarget:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;Z)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->optOut(Z)V

    return-void
.end method

.method static newInstance(Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;)Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;
    .locals 2
    .param p0    # Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

    new-instance v0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    invoke-direct {v0}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;-><init>()V

    if-eqz p0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    :cond_0
    return-object v0
.end method

.method private optOut(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->mTarget:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->updatePredictiveCardsEnabledSwitch()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->mTarget:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->startOptOutTask(Z)V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->mTarget:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->updatePredictiveCardsEnabledSwitch()V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->findOptOutHandler(Landroid/app/Fragment;)Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->mTarget:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040086

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f1001a1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    const v3, 0x7f0d01f3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0d01f2

    new-instance v5, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$2;

    invoke-direct {v5, p0, v1}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$2;-><init>(Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    new-instance v5, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$1;-><init>(Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method
