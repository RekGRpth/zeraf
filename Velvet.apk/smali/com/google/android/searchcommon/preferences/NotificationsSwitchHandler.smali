.class public Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;
.super Ljava/lang/Object;
.source "NotificationsSwitchHandler.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final mPreferencesKey:Ljava/lang/String;

.field private final mSharedPreferences:Landroid/content/SharedPreferences;

.field private mSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSharedPreferences:Landroid/content/SharedPreferences;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mPreferencesKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public hasSwitch()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isOverallPreferenceEnabled()Z
    .locals 4

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mPreferencesKey:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mPreferencesKey:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public setSwitch(Landroid/widget/Switch;)V
    .locals 2
    .param p1    # Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->isOverallPreferenceEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method public updateSwitchState()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->isOverallPreferenceEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_0
    return-void
.end method
