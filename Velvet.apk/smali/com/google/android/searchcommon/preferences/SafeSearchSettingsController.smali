.class public Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "SafeSearchSettingsController.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private mPreference:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    return-void
.end method

.method private updateSafeSearchSummary(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public handlePreference(Landroid/preference/Preference;)V
    .locals 1
    .param p1    # Landroid/preference/Preference;

    check-cast p1, Landroid/preference/ListPreference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/SearchSettings;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->updateSafeSearchSummary(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/SearchSettings;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->onDestroy()V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/SafeSearchSettingsController;->updateSafeSearchSummary(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
