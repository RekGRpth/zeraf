.class Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;
.super Landroid/os/AsyncTask;
.source "MyStocksSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StockSymbolSuggestTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final symbol:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->this$0:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->symbol:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    return-void
.end method

.method private getStockData(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 12
    .param p1    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    :try_start_0
    const-string v10, "matches"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v1, v10, :cond_2

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    invoke-virtual {v6}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v5

    new-instance v8, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-direct {v8}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;-><init>()V

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v2, v10, :cond_5

    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "id"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setGin(J)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const-string v10, "t"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v8, v9}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setSymbol(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    goto :goto_2

    :catch_0
    move-exception v0

    :cond_2
    return-object v7

    :cond_3
    const-string v10, "e"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v8, v9}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setExchange(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    goto :goto_2

    :cond_4
    const-string v10, "n"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v8, v9}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setDescription(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    goto :goto_2

    :cond_5
    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasGin()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasDescription()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasExchange()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->hasSymbol()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private sendRequest(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 12
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v5, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "http://www.google.com/finance/match?q="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;)V

    :try_start_0
    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    const/16 v10, 0x8

    invoke-interface {v9, v5, v10}, Lcom/google/android/searchcommon/util/HttpHelper;->rawGet(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)[B

    move-result-object v6

    if-nez v6, :cond_0

    move-object v7, v8

    :goto_0
    return-object v7

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v3

    :try_start_1
    new-instance v7, Lorg/json/JSONObject;

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v6}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-wide v8

    sub-long v1, v8, v3

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v7, v8

    goto :goto_0

    :catch_1
    move-exception v0

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->access$100()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Network error: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v8

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 2
    .param p1    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->symbol:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->sendRequest(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->getStockData(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    new-instance v3, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getSymbol()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;-><init>(Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->this$0:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->symbol:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->updateSymbolList(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method
