.class Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;
.super Landroid/os/AsyncTask;
.source "MyPlacesSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FetchLocationsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/geo/sidekick/Sidekick$EntryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;->this$0:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;
    .param p2    # Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;-><init>(Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Lcom/google/geo/sidekick/Sidekick$Interest;->setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/google/geo/sidekick/Sidekick$Interest;->addEntryTypeRestrict(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;->addInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setEntryQuery(Lcom/google/geo/sidekick/Sidekick$EntryQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;->this$0:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v2

    invoke-interface {v2, v3}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithoutLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v4

    if-nez v4, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v5

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;->doInBackground([Ljava/lang/Void;)Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;->this$0:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;

    # invokes: Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->handleLocationResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V
    invoke-static {v0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->access$100(Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V

    return-void
.end method
