.class final Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;
.super Landroid/widget/ArrayAdapter;
.source "MyStocksSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "StockDataListAdpater"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;",
        ">;"
    }
.end annotation


# instance fields
.field private final mMatchStyle:Landroid/text/style/CharacterStyle;

.field private final mPostMatchStyle:Landroid/text/style/CharacterStyle;

.field private final mPreMatchStyle:Landroid/text/style/CharacterStyle;

.field private mQuerySymbol:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;",
            ">;)V"
        }
    .end annotation

    const v2, 0x7f0e0048

    const v0, 0x7f0400bb

    const v1, 0x1020016

    invoke-direct {p0, p1, v0, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v0, p1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mPreMatchStyle:Landroid/text/style/CharacterStyle;

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v0, p1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mPostMatchStyle:Landroid/text/style/CharacterStyle;

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e0049

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mMatchStyle:Landroid/text/style/CharacterStyle;

    return-void
.end method

.method private hilightQuery(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v3, -0x1

    if-le v2, v3, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    add-int v0, v2, v3

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mPreMatchStyle:Landroid/text/style/CharacterStyle;

    invoke-virtual {v1, v3, v5, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mMatchStyle:Landroid/text/style/CharacterStyle;

    invoke-virtual {v1, v3, v2, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mPostMatchStyle:Landroid/text/style/CharacterStyle;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_0
    return-object v1

    :cond_0
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mPreMatchStyle:Landroid/text/style/CharacterStyle;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v1, v3, v5, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mQuerySymbol:Ljava/lang/String;

    if-eqz v4, :cond_0

    const v4, 0x1020016

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mQuerySymbol:Ljava/lang/String;

    invoke-direct {p0, v2, v4}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->hilightQuery(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v3
.end method

.method setQuery(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->mQuerySymbol:Ljava/lang/String;

    return-void
.end method
