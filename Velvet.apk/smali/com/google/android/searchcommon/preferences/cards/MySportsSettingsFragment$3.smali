.class final Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$3;
.super Ljava/lang/Object;
.source "MySportsSettingsFragment.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getSportEntitiesFromPreferences(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Z
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;->getHide()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$3;->apply(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Z

    move-result v0

    return v0
.end method
