.class public Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;
.source "MyPlacesSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$1;,
        Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;,
        Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mFetchLocationsTask:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;

.field private mHomeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

.field private mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->handleLocationResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V

    return-void
.end method

.method private getAddress(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasAddress()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getAddress()Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getLocationName(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private final getRenameOrEditAction(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    const/16 v3, 0x11

    if-eq v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleLocationResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V
    .locals 12
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    const/4 v11, 0x0

    const/4 v9, 0x0

    iput-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mFetchLocationsTask:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const-string v10, "Error getting locations"

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v9

    if-lez v9, :cond_4

    invoke-virtual {p1, v11}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v3

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getRenameOrEditAction(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v9

    const/16 v10, 0x11

    if-ne v9, v10, :cond_3

    iput-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v9

    const/16 v10, 0x12

    if-ne v9, v10, :cond_2

    iput-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_1

    :cond_4
    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    if-eqz v9, :cond_6

    new-instance v9, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    const v10, 0x7f0d01e3

    invoke-virtual {v9, v10}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setDialogTitle(I)V

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    const v10, 0x7f0d00d5

    invoke-virtual {p0, v10}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setName(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v9, v11}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setNameEditable(Z)V

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v9}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getAddress(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v9, v4}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setAddress(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v9, p0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_6
    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    if-eqz v9, :cond_0

    new-instance v9, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    const v10, 0x7f0d01e7

    invoke-virtual {v9, v10}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setDialogTitle(I)V

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v9}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getLocationName(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_8

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v9, v6}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setName(Ljava/lang/String;)V

    :goto_2
    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setNameEditable(Z)V

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v9}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getAddress(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v9, v8}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setAddress(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v9, p0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_0

    :cond_8
    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    const v10, 0x7f0d00ea

    invoke-virtual {p0, v10}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setName(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private startEditPlaceTask(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "editPlaceWorkerFragment"

    invoke-virtual {v6, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;)Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;

    move-result-object v7

    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "editPlaceWorkerFragment"

    invoke-virtual {v0, v7, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method


# virtual methods
.method editPlaceTaskFinished(ZLcom/google/geo/sidekick/Sidekick$Action;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Z
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    if-nez p1, :cond_0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v0

    const/16 v1, 0x11

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v0, p4}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setAddress(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v0, p3}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    invoke-virtual {v0, p4}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setAddress(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getPreferenceResourceId()I
    .locals 1

    const v0, 0x7f07000c

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;-><init>(Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mFetchLocationsTask:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mFetchLocationsTask:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mFetchLocationsTask:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mFetchLocationsTask:Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$FetchLocationsTask;->cancel(Z)Z

    :cond_0
    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v8, 0x1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomePreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    if-ne p1, v0, :cond_0

    move-object v3, p2

    check-cast v3, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Location;->getAddress()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getAddress(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v5, v6}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mHomeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getRenameOrEditAction(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v2

    const/4 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->startEditPlaceTask(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkPreference:Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;

    if-ne p1, v0, :cond_3

    move-object v3, p2

    check-cast v3, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Location;->getAddress()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getAddress(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getLocationName(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v5, v6}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v4, v7}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->mWorkEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->getRenameOrEditAction(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->startEditPlaceTask(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
