.class Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment$1;
.super Ljava/lang/Object;
.source "NotificationsSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->configureRingtonePreference(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;

.field final synthetic val$appContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment$1;->this$0:Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment$1;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment$1;->val$appContext:Landroid/content/Context;

    invoke-static {v2, p1, v0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updateRingtoneSummary(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V

    const/4 v2, 0x1

    return v2
.end method
