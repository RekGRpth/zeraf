.class public Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;
.source "MySportsSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;,
        Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$RemoveTeamDialogFragment;,
        Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;,
        Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;,
        Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;
    }
.end annotation


# static fields
.field private static final SPORT_ENUM_TO_ICON_ID:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

.field private mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mSportTeamPlayerPreferenceKey:Ljava/lang/String;

.field private mSportsEntities:Lcom/google/geo/sidekick/Sidekick$SportsTeams;

.field private mTeamsCategory:Landroid/preference/PreferenceCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f02011d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020123

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020126

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020129

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->SPORT_ENUM_TO_ICON_ID:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;Lcom/google/geo/sidekick/Sidekick$SportsTeams;)Lcom/google/geo/sidekick/Sidekick$SportsTeams;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportsEntities:Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->addTeam(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)V

    return-void
.end method

.method static synthetic access$500()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->SPORT_ENUM_TO_ICON_ID:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->removeTeam(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    return-object v0
.end method

.method private addTeam(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)V
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->keyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mTeamsCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->createTeamPreference(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Landroid/preference/Preference;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mTeamsCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->editConfiguration()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportTeamPlayerPreferenceKey:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->addMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->apply()V

    goto :goto_0
.end method

.method private createTeamPreference(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Landroid/preference/Preference;
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    new-instance v2, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->sportTeamToLabel(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->keyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setPersistent(Z)V

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {v2}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "team_preference_key"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const v3, 0x7f0400bb

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setLayoutResource(I)V

    sget-object v3, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->SPORT_ENUM_TO_ICON_ID:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;->getSport()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(I)V

    :cond_0
    return-object v2
.end method

.method static getSportEntitiesFromPreferences(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p0    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getMessages(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$3;

    invoke-direct {v2}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$3;-><init>()V

    invoke-static {v1, v2}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$4;

    invoke-direct {v1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$4;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private keyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    sget-object v0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->REPEATED_MESSAGE_INFO:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    invoke-interface {v0, p1}, Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;->primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadSportsFromLocalStorage()V
    .locals 4

    new-instance v0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$1;-><init>(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    const-string v2, "static_entities"

    new-instance v3, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$2;-><init>(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;Landroid/os/Handler;)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;->readFromFile(Ljava/lang/String;Lcom/google/common/base/Function;)V

    return-void
.end method

.method private removeTeam(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mTeamsCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, p1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mTeamsCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportTeamPlayerPreferenceKey:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;->setHide(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->editConfiguration()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportTeamPlayerPreferenceKey:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->updateMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->apply()V

    :cond_0
    return-void
.end method

.method private declared-synchronized showAddTeamDialog()Z
    .locals 4

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportsEntities:Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportsEntities:Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportsTeams;->getSportTeamPlayerCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;

    invoke-direct {v1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "sports_entries_extra"

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportsEntities:Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportsTeams;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "add_team_dialog_tag"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private showRemoveTeamDialog(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$RemoveTeamDialogFragment;->newInstance(Landroid/app/Fragment;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$RemoveTeamDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "remove_team_dialog_tag"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public static sportTeamToLabel(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Ljava/lang/String;
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;->hasTeam()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;->getTeam()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getPreferenceResourceId()I
    .locals 1

    const v0, 0x7f07000d

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getAsyncFileStorage()Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    const v6, 0x7f0d0082

    invoke-virtual {p0, v6}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportTeamPlayerPreferenceKey:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    const-string v7, "sports_teams_key"

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/PreferenceCategory;

    iput-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mTeamsCategory:Landroid/preference/PreferenceCategory;

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mTeamsCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v6, v8}, Landroid/preference/PreferenceCategory;->setOrderingAsAdded(Z)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportTeamPlayerPreferenceKey:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getSportEntitiesFromPreferences(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->createTeamPreference(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;)Landroid/preference/Preference;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mTeamsCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v6, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0d0205

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setTitle(I)V

    const-string v6, "add_team_preference_key"

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Landroid/preference/Preference;->setPersistent(Z)V

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    invoke-virtual {v0, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    const v6, 0x7f0d035e

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setSummary(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->loadSportsFromLocalStorage()V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2
    .param p1    # Landroid/preference/Preference;

    const-string v0, "add_team_preference_key"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->showAddTeamDialog()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "team_preference_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->showRemoveTeamDialog(Ljava/lang/CharSequence;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateSportsEntities()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    const-string v3, "add_team_preference_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "add_team_dialog_tag"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportsEntities:Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    invoke-virtual {v0, v2}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$AddTeamDialogFragment;->updateSportsEntities(Lcom/google/geo/sidekick/Sidekick$SportsTeams;)V

    goto :goto_0
.end method
