.class public Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;
.super Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;
.source "MyPlacesSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PrefsWorkerFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;)Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;

    new-instance v1, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;

    invoke-direct {v1}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;-><init>()V

    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;->buildArguments(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;->setArguments(Landroid/os/Bundle;)V

    const/4 v2, 0x0

    invoke-virtual {v1, p5, v2}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    return-object v1
.end method


# virtual methods
.method protected editPlaceTaskFinished(ZLcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Z
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment$PrefsWorkerFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;

    invoke-virtual {v0, p1, p3, p5, p6}, Lcom/google/android/searchcommon/preferences/cards/MyPlacesSettingsFragment;->editPlaceTaskFinished(ZLcom/google/geo/sidekick/Sidekick$Action;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
