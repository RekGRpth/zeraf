.class public abstract Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;
.super Landroid/preference/PreferenceFragment;
.source "SettingsFragmentBase.java"


# instance fields
.field private mController:Lcom/google/android/searchcommon/preferences/PreferenceController;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getController()Lcom/google/android/searchcommon/preferences/PreferenceController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    return-object v0
.end method

.method protected abstract getPreferencesResourceId()I
.end method

.method protected handlePreferenceGroup(Landroid/preference/PreferenceGroup;)V
    .locals 1
    .param p1    # Landroid/preference/PreferenceGroup;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->handlePreference(Landroid/preference/Preference;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/VelvetApplication;->createPreferenceController(Landroid/app/Activity;)Lcom/google/android/searchcommon/preferences/PreferenceController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->useMainPreferences(Landroid/preference/PreferenceManager;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->getPreferencesResourceId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->handlePreferenceGroup(Landroid/preference/PreferenceGroup;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onCreateComplete(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onDestroy()V

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onPause()V

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->mController:Lcom/google/android/searchcommon/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/PreferenceController;->onStop()V

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStop()V

    return-void
.end method
