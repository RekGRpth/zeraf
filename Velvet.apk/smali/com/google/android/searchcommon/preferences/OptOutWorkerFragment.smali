.class public Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;
.super Landroid/app/Fragment;
.source "OptOutWorkerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mOptOutTask:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

.field private mTurnOffHistory:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static newInstance(Landroid/accounts/Account;ZLandroid/app/DialogFragment;Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;)Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;
    .locals 3
    .param p0    # Landroid/accounts/Account;
    .param p1    # Z
    .param p2    # Landroid/app/DialogFragment;
    .param p3    # Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

    new-instance v1, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;

    invoke-direct {v1}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "turn_off_history_key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "account_key"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->setArguments(Landroid/os/Bundle;)V

    if-eqz p3, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, p3, v2}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    :cond_0
    return-object v1
.end method


# virtual methods
.method public finishOptOut(Z)V
    .locals 3

    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->findOptOutHandler(Landroid/app/Fragment;)Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->updatePredictiveCardsEnabledSwitch()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "opt_out_progress"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mOptOutTask:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    if-nez p1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mAccount:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isAccountOptedIn(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v0, "account_key"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mAccount:Landroid/accounts/Account;

    const-string v0, "turn_off_history_key"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mTurnOffHistory:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v5

    new-instance v0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mAccount:Landroid/accounts/Account;

    iget-boolean v4, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mTurnOffHistory:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;-><init>(Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;Landroid/content/Context;Landroid/accounts/Account;ZLcom/google/android/apps/sidekick/inject/NetworkClient;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mOptOutTask:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mOptOutTask:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mOptOutTask:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mOptOutTask:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mOptOutTask:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->cancel(Z)Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mOptOutTask:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method
