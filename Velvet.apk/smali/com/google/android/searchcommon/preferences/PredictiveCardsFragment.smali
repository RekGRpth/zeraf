.class public Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;
.super Landroid/preference/PreferenceFragment;
.source "PredictiveCardsFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/searchcommon/preferences/HasOptOutSwitchHandler;


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private addOptInSwitch()V
    .locals 8

    const/16 v6, 0x10

    const/4 v7, -0x2

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    if-eqz v3, :cond_0

    move-object v3, v0

    check-cast v3, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->onIsMultiPane()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0028

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    new-instance v3, Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3, v5, v5, v2, v5}, Landroid/widget/Switch;->setPadding(IIII)V

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v5, Landroid/app/ActionBar$LayoutParams;

    const/16 v6, 0x15

    invoke-direct {v5, v7, v7, v6}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v3, v4, v5}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->setSwitch(Landroid/widget/Switch;)V

    goto :goto_0
.end method

.method private removeOptInSwitch()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->setSwitch(Landroid/widget/Switch;)V

    iput-object v3, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    :cond_0
    return-void
.end method


# virtual methods
.method public getOptOutSwitchHandler()Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x1

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "sidekick"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    const v0, 0x7f070012

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v1

    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v3, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-interface {v1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->userHasSeenFirstRunScreens()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "skip_to_end"

    invoke-virtual {v3, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    new-instance v0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;-><init>(Lcom/google/android/searchcommon/MarinerOptInSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/content/Intent;Landroid/app/Activity;Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {p0, v7}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->setHasOptionsMenu(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->restoreInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0d00c3

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->shouldHideGmailCardSettings()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0d005d

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/debug/DebugFeatures;->dogfoodDebugEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0d00c0

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->remindersEnabled(Lcom/google/android/searchcommon/SearchConfig;)Z

    move-result v1

    if-nez v1, :cond_3

    const v1, 0x7f0d00be

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->removeOptInSwitch()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidateWithDelayedRefresh()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceSummaries(Landroid/preference/Preference;)V

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->addOptInSwitch()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->saveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->hasBackingConfiguration()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceSummaries(Landroid/preference/Preference;)V

    :cond_0
    return-void
.end method

.method shouldHideGmailCardSettings()Z
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0d005e

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method
