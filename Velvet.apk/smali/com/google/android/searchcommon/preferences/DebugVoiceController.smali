.class public Lcom/google/android/searchcommon/preferences/DebugVoiceController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "DebugVoiceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

.field private final mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

.field private final mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/debug/DebugFeatures;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;Lcom/google/android/speech/utils/NetworkInformation;Landroid/content/Context;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/debug/DebugFeatures;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;
    .param p3    # Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;
    .param p4    # Lcom/google/android/speech/utils/NetworkInformation;
    .param p5    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    iput-object p4, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    iput-object p5, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/preferences/DebugVoiceController;)Lcom/google/android/voicesearch/settings/Settings;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/DebugVoiceController;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-object v0
.end method

.method private getCurrentDebugServer(Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getLabel()Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_1
    const-string v3, "Unknown"

    goto :goto_0
.end method

.method private getDebugServerEntries(Ljava/util/List;)[Ljava/lang/CharSequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;",
            ">;)[",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getLabel()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private handleDebugServer(Landroid/preference/ListPreference;)V
    .locals 5
    .param p1    # Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDebug()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;->getDebugServerList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->getDebugServerEntries(Ljava/util/List;)[Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->getDebugServerEntries(Ljava/util/List;)[Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->getCurrentDebugServer(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const-string v4, "[DEBUG] Config sync error! Try checking in."

    aput-object v4, v1, v3

    invoke-virtual {p1, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    const-string v3, "[DEBUG] Config sync error! Try checking in."

    invoke-virtual {p1, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private handlePersonalization(Landroid/preference/CheckBoxPreference;)V
    .locals 1
    .param p1    # Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-interface {v0}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;->isPersonalizationAvailable()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method private handlePersonalizationChange(Landroid/preference/CheckBoxPreference;Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Landroid/preference/CheckBoxPreference;
    .param p2    # Ljava/lang/Boolean;

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v1}, Lcom/google/android/speech/utils/NetworkInformation;->getSimMcc()I

    move-result v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->updatePersonalizationMcc(I)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->handlePersonalization(Landroid/preference/CheckBoxPreference;)V

    return-void
.end method

.method private handleS3ServerChange(Landroid/preference/ListPreference;Ljava/lang/CharSequence;)V
    .locals 7
    .param p1    # Landroid/preference/ListPreference;
    .param p2    # Ljava/lang/CharSequence;

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDebug()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Debug;->getDebugServerList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getLabel()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/settings/Settings;->getOverrideConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->clearTcpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->clearPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->clearSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getTcpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setTcpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getPairHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setPairHttpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$DebugServer;->getSingleHttpServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setSingleHttpServerInfo(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v6, v4}, Lcom/google/android/voicesearch/settings/Settings;->setOverrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->handleDebugServer(Landroid/preference/ListPreference;)V

    :cond_1
    return-void
.end method

.method private sendConfiguration()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Configuration:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->getConfigurationTimestamp()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/speech/utils/ProtoBufUtils;->toString(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private showEditExperiment(Landroid/preference/Preference;)V
    .locals 5
    .param p1    # Landroid/preference/Preference;

    const/4 v4, -0x1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v3, 0x104000a

    new-instance v4, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;

    invoke-direct {v4, p0, v1, p1}, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;-><init>(Lcom/google/android/searchcommon/preferences/DebugVoiceController;Landroid/widget/EditText;Landroid/preference/Preference;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private updatePersonalizationMcc(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getOverrideConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->addMccCountryCodes(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v1

    const-string v2, "http://www.google.com"

    invoke-virtual {v1, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->setDashboardUrl(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v1

    const-string v2, "http://www.google.com"

    invoke-virtual {v1, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->setMoreInfoUrl(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setPersonalization(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/settings/Settings;->setOverrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    return-void
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->dogfoodDebugEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugS3Server"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/preference/ListPreference;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->handleDebugServer(Landroid/preference/ListPreference;)V

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugPersonalization"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    check-cast p1, Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->handlePersonalization(Landroid/preference/CheckBoxPreference;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugConfigurationDate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfigurationTimestamp()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugConfigurationExperiment"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugSendLoggedAudio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioLoggingEnabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debugS3Server"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast p1, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->handleS3ServerChange(Landroid/preference/ListPreference;Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debugPersonalization"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p1, Landroid/preference/CheckBoxPreference;

    check-cast p2, Ljava/lang/Boolean;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->handlePersonalizationChange(Landroid/preference/CheckBoxPreference;Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "audioLoggingEnabled"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/speech/debug/DebugAudioLogger;->clearAllLoggedData(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debugConfigurationDate"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->sendConfiguration()V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debugConfigurationExperiment"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->showEditExperiment(Landroid/preference/Preference;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "debugSendLoggedAudio"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/speech/debug/DebugAudioLogger;->sendLoggedAudio(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
