.class public interface abstract Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
.super Ljava/lang/Object;
.source "SharedPreferencesExt.java"

# interfaces
.implements Landroid/content/SharedPreferences;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    }
.end annotation


# virtual methods
.method public abstract allowWrites()V
.end method

.method public abstract delayWrites()V
.end method

.method public abstract edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
.end method

.method public abstract getBytes(Ljava/lang/String;[B)[B
.end method
