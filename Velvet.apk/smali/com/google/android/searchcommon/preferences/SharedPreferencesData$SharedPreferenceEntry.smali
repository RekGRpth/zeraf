.class public final Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "SharedPreferencesData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/SharedPreferencesData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SharedPreferenceEntry"
.end annotation


# instance fields
.field private boolValue_:Z

.field private bytesValue_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private cachedSize:I

.field private floatValue_:F

.field private hasBoolValue:Z

.field private hasBytesValue:Z

.field private hasFloatValue:Z

.field private hasIntValue:Z

.field private hasKey:Z

.field private hasLongValue:Z

.field private hasStringValue:Z

.field private intValue_:I

.field private key_:Ljava/lang/String;

.field private longValue_:J

.field private stringSetValue_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private stringValue_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->key_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->boolValue_:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->floatValue_:F

    iput v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->intValue_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->longValue_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringValue_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringSetValue_:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->bytesValue_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addStringSetValue(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringSetValue_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringSetValue_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringSetValue_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBoolValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->boolValue_:Z

    return v0
.end method

.method public getBytesValue()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->bytesValue_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->cachedSize:I

    return v0
.end method

.method public getFloatValue()F
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->floatValue_:F

    return v0
.end method

.method public getIntValue()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->intValue_:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->key_:Ljava/lang/String;

    return-object v0
.end method

.method public getLongValue()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->longValue_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasKey()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBoolValue()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getBoolValue()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasFloatValue()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getFloatValue()F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasIntValue()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getIntValue()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasLongValue()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getLongValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasStringValue()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringSetValueList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_6
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringSetValueList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBytesValue()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getBytesValue()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    iput v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->cachedSize:I

    return v3
.end method

.method public getStringSetValue(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringSetValue_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getStringSetValueCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringSetValue_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getStringSetValueList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringSetValue_:Ljava/util/List;

    return-object v0
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringValue_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBoolValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBoolValue:Z

    return v0
.end method

.method public hasBytesValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBytesValue:Z

    return v0
.end method

.method public hasFloatValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasFloatValue:Z

    return v0
.end method

.method public hasIntValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasIntValue:Z

    return v0
.end method

.method public hasKey()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasKey:Z

    return v0
.end method

.method public hasLongValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasLongValue:Z

    return v0
.end method

.method public hasStringValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasStringValue:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setKey(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setBoolValue(Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setFloatValue(F)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setIntValue(I)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setLongValue(J)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setStringValue(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->addStringSetValue(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setBytesValue(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    move-result-object v0

    return-object v0
.end method

.method public setBoolValue(Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBoolValue:Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->boolValue_:Z

    return-object p0
.end method

.method public setBytesValue(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBytesValue:Z

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->bytesValue_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setFloatValue(F)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasFloatValue:Z

    iput p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->floatValue_:F

    return-object p0
.end method

.method public setIntValue(I)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasIntValue:Z

    iput p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->intValue_:I

    return-object p0
.end method

.method public setKey(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasKey:Z

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->key_:Ljava/lang/String;

    return-object p0
.end method

.method public setLongValue(J)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasLongValue:Z

    iput-wide p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->longValue_:J

    return-object p0
.end method

.method public setStringValue(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasStringValue:Z

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->stringValue_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasKey()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBoolValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getBoolValue()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasFloatValue()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getFloatValue()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasIntValue()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getIntValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasLongValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getLongValue()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasStringValue()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringSetValueList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBytesValue()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getBytesValue()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_7
    return-void
.end method
