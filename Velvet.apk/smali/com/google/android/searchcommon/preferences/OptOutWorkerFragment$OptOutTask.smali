.class public Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;
.super Landroid/os/AsyncTask;
.source "OptOutWorkerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OptOutTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mDisableLocationHistory:Z

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;Landroid/content/Context;Landroid/accounts/Account;ZLcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/accounts/Account;
    .param p4    # Z
    .param p5    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->this$0:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->mAppContext:Landroid/content/Context;

    iput-boolean p4, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->mDisableLocationHistory:Z

    iput-object p5, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    :try_start_0
    new-instance v4, Lcom/google/geo/sidekick/Sidekick$Configuration;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$Configuration;-><init>()V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setCompletelyOptOutOfSidekick(Z)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v0

    iget-boolean v4, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->mDisableLocationHistory:Z

    if-eqz v4, :cond_0

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;->setLocationHistoryRecording(Z)Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;

    invoke-virtual {v0, v3}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setPlacevaultConfiguration(Lcom/google/geo/sidekick/Sidekick$PlacevaultConfiguration;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    :cond_0
    new-instance v4, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setConfigurationQuery(Lcom/google/geo/sidekick/Sidekick$Configuration;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-interface {v4, v1}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithoutLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    :catch_0
    move-exception v2

    # getter for: Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Exception while attempting to opt out"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->this$0:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->finishOptOut(Z)V

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->mAppContext:Landroid/content/Context;

    const v2, 0x7f0d01c7

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->mAppContext:Landroid/content/Context;

    const v2, 0x7f0d01c8

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->optTheUserOut()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method optTheUserOut()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "OPT_OUT"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment$OptOutTask;->this$0:Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;

    # getter for: Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->mAccount:Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->access$100(Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;)Landroid/accounts/Account;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/MarinerOptInSettings;->disableForAccount(Landroid/accounts/Account;)V

    return-void
.end method
