.class Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;
.super Ljava/lang/Object;
.source "ManageSearchHistoryHelper.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

.field final synthetic val$manageSearchHistory:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;->this$0:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;->val$manageSearchHistory:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Landroid/net/Uri;)Z
    .locals 3
    .param p1    # Landroid/net/Uri;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;->this$0:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    # getter for: Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mCancelled:Z
    invoke-static {v2}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->access$100(Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;->val$manageSearchHistory:Landroid/net/Uri;

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;->this$0:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->dismiss()V

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;->this$0:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;->this$0:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2, p1, v1}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeViewUri(Landroid/content/Context;Landroid/net/Uri;Z)Z

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;->consume(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
