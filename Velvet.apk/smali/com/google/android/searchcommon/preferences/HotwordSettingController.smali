.class public Lcom/google/android/searchcommon/preferences/HotwordSettingController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "HotwordSettingController.java"


# instance fields
.field final mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field final mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/embedded/Greco3DataManager;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/settings/Settings;
    .param p2    # Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/HotwordSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/HotwordSettingController;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    return-void
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/HotwordSettingController;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->waitForInitialization()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/HotwordSettingController;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/HotwordSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/speech/embedded/Greco3Mode;->HOTWORD:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasResources(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 0
    .param p1    # Landroid/preference/Preference;

    return-void
.end method
