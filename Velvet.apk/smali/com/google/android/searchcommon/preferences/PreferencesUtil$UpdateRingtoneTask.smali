.class Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;
.super Landroid/os/AsyncTask;
.source "PreferencesUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/PreferencesUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UpdateRingtoneTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPreference:Landroid/preference/Preference;

.field private final mRingtoneUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/preference/Preference;
    .param p3    # Landroid/net/Uri;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mPreference:Landroid/preference/Preference;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mRingtoneUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 4
    .param p1    # [Ljava/lang/Void;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mRingtoneUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mRingtoneUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mRingtoneUri:Landroid/net/Uri;

    invoke-static {v2, v3}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mContext:Landroid/content/Context;

    const v3, 0x7f0d00e8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mPreference:Landroid/preference/Preference;

    # invokes: Lcom/google/android/searchcommon/preferences/PreferencesUtil;->getRingtoneUriFromPreference(Landroid/preference/Preference;)Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->access$000(Landroid/preference/Preference;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mRingtoneUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->mPreference:Landroid/preference/Preference;

    invoke-virtual {v1, p1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
