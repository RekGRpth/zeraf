.class Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;
.super Ljava/lang/Object;
.source "GoogleAccountSettingsController.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->refreshCloudHistoryPreference(Landroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

.field final synthetic val$a:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->val$a:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Ljava/lang/Boolean;)Z
    .locals 7
    .param p1    # Ljava/lang/Boolean;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    # setter for: Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudWebHistorySetting:Ljava/lang/Boolean;
    invoke-static {v0, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->access$102(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->val$a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iget-object v1, v1, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v5}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    # getter for: Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->access$200(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d033e

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v6}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->this$0:Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d033f

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->val$a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;->consume(Ljava/lang/Boolean;)Z

    move-result v0

    return v0
.end method
