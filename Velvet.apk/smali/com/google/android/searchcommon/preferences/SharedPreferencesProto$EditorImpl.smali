.class public Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;
.super Ljava/lang/Object;
.source "SharedPreferencesProto.java"

# interfaces
.implements Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EditorImpl"
.end annotation


# instance fields
.field private mClear:Z

.field private final mEditorLock:Ljava/lang/Object;

.field private mModified:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mEditorLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    return-void
.end method

.method private doCommit(Z)Z
    .locals 24
    .param p1    # Z

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v18, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$400(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/lang/Object;

    move-result-object v20

    monitor-enter v20

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$500(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/util/Map;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$600(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    move-result-object v19

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$600(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWrittenMap:Ljava/util/Map;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    if-ne v0, v12, :cond_0

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13, v12}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object v12, v13

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mEditorLock:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mClear:Z

    move/from16 v19, v0

    if-eqz v19, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v22, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mRemoveMarker:Ljava/lang/Object;
    invoke-static/range {v22 .. v22}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$300(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/lang/Object;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/google/common/base/Predicates;->equalTo(Ljava/lang/Object;)Lcom/google/common/base/Predicate;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/common/collect/Maps;->filterValues(Ljava/util/Map;Lcom/google/common/base/Predicate;)Ljava/util/Map;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->clear()V

    invoke-interface {v12}, Ljava/util/Map;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$700(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Z

    move-result v19

    if-nez v19, :cond_2

    :cond_1
    const/4 v3, 0x1

    new-instance v11, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    # setter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z
    invoke-static {v0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$702(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Z)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mEditorLock:Ljava/lang/Object;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->notifyAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v10, v11

    :cond_2
    const/16 v19, 0x0

    :try_start_3
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mClear:Z

    :goto_0
    monitor-exit v21
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v10, :cond_3

    :try_start_4
    invoke-interface {v10}, Ljava/util/Set;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mListeners:Ljava/util/Set;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$800(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_3

    new-instance v9, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mListeners:Ljava/util/Set;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$800(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/util/Set;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v8, v9

    :cond_3
    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$600(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    move-result-object v19

    if-nez v19, :cond_12

    const/16 v16, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$600(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    move-result-object v19

    if-eqz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$600(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWrittenMap:Ljava/util/Map;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v21, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;
    invoke-static/range {v21 .. v21}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$500(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/util/Map;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    new-instance v21, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-direct/range {v21 .. v23}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;-><init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$1;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    # setter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    invoke-static {v0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$602(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    # setter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;
    invoke-static {v0, v12}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$502(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Ljava/util/Map;)Ljava/util/Map;

    :cond_6
    if-eqz p1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$600(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteDelayCounter:I
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$900(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)I

    move-result v19

    if-eqz v19, :cond_7

    const-string v19, "Search.SharedPreferencesProto"

    const-string v21, "potential deadlock: commit while delayWrites"

    new-instance v22, Ljava/lang/Throwable;

    invoke-direct/range {v22 .. v22}, Ljava/lang/Throwable;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_7
    monitor-exit v20
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v16, :cond_8

    new-instance v19, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl$1;

    const-string v20, "Search.SharedPreferencesProto"

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl$1;-><init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl$1;->start()V

    :cond_8
    if-eqz v8, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    # invokes: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->notifyListeners(Ljava/util/Collection;Ljava/util/Set;)V
    invoke-static {v0, v8, v10}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$200(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Ljava/util/Collection;Ljava/util/Set;)V

    :cond_9
    const/4 v15, 0x1

    if-eqz v18, :cond_a

    :try_start_5
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWaitLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/concurrent/CountDownLatch;->await()V

    move-object/from16 v0, v18

    iget-boolean v15, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWriteResult:Z
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_a
    :goto_2
    return v15

    :cond_b
    :try_start_6
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_c
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_10

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    invoke-interface {v12, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$700(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Z

    move-result v19

    if-eqz v19, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    move-object/from16 v19, v0

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mRemoveMarker:Ljava/lang/Object;
    invoke-static/range {v19 .. v19}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$300(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_d

    invoke-interface {v12, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    invoke-interface {v12, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v11, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v19

    move-object v10, v11

    :goto_4
    :try_start_8
    monitor-exit v21
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v19

    :catchall_1
    move-exception v19

    monitor-exit v20
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v19

    :cond_d
    :try_start_a
    invoke-interface {v12, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    if-nez v17, :cond_e

    invoke-interface {v12, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    if-nez v19, :cond_f

    :cond_e
    if-eqz v17, :cond_c

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_c

    :cond_f
    move-object/from16 v0, v17

    invoke-interface {v12, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v11, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->clear()V

    invoke-interface {v11}, Ljava/util/Set;->isEmpty()Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result v19

    if-nez v19, :cond_11

    const/4 v3, 0x1

    :goto_5
    move-object v10, v11

    goto/16 :goto_0

    :cond_11
    const/4 v3, 0x0

    goto :goto_5

    :cond_12
    const/16 v16, 0x0

    goto/16 :goto_1

    :catch_0
    move-exception v4

    const/4 v15, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_2

    :catchall_2
    move-exception v19

    goto :goto_4
.end method

.method private doPut(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mEditorLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mModified:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public apply()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doCommit(Z)Z

    return-void
.end method

.method public bridge synthetic clear()Landroid/content/SharedPreferences$Editor;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->clear()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mEditorLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->mClear:Z

    monitor-exit v1

    return-object p0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public commit()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doCommit(Z)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doPut(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method

.method public putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doPut(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0

    :cond_0
    invoke-static {p2}, Lcom/google/protobuf/micro/ByteStringMicro;->copyFrom([B)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # F

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->putFloat(Ljava/lang/String;F)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public putFloat(Ljava/lang/String;F)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # F

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doPut(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method

.method public bridge synthetic putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doPut(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method

.method public bridge synthetic putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doPut(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method

.method public bridge synthetic putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doPut(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method

.method public bridge synthetic putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Set;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public putStringSet(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;"
        }
    .end annotation

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doPut(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public bridge synthetic remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    # getter for: Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mRemoveMarker:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->access$300(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;->doPut(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method
