.class public Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "ManagePersonalizationSettingController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

.field private mPreference:Landroid/preference/Preference;

.field private final mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Landroid/app/Activity;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Landroid/app/Activity;
    .param p3    # Lcom/google/android/voicesearch/settings/Settings;
    .param p4    # Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;->mActivity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p4, p0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    return-void
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    invoke-interface {v0}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;->isPersonalizationAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;->mPreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;->mPreference:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;

    const/16 v1, 0x2f

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPersonalization()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Personalization;->getDashboardUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/ManagePersonalizationSettingController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    return v1
.end method
