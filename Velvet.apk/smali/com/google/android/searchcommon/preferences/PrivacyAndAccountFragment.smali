.class public Lcom/google/android/searchcommon/preferences/PrivacyAndAccountFragment;
.super Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;
.source "PrivacyAndAccountFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getPreferencesResourceId()I
    .locals 1

    const v0, 0x7f070016

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PrivacyAndAccountFragment;->getController()Lcom/google/android/searchcommon/preferences/PreferenceController;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PrivacyAndAccountFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->setScreen(Landroid/preference/PreferenceScreen;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/PrivacyAndAccountFragment;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const v0, 0x7f120001

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    new-instance v0, Lcom/google/android/velvet/Help;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PrivacyAndAccountFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/velvet/Help;-><init>(Landroid/content/Context;)V

    const-string v1, "privacy"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/velvet/Help;->setHelpMenuItemIntent(Landroid/view/Menu;Ljava/lang/String;)V

    return-void
.end method
