.class public Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;
.super Ljava/lang/Object;
.source "DeviceCapabilityManagerImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/DeviceCapabilityManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mInited:Z

.field private mTelephoneCapable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->mContext:Landroid/content/Context;

    return-void
.end method

.method private static isTelephoneCapable(Landroid/telephony/TelephonyManager;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    const-class v1, Landroid/telephony/TelephonyManager;

    const-string v2, "isVoiceCapable"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :try_start_1
    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :goto_1
    return v0

    :catch_0
    move-exception v1

    move-object v1, v0

    goto :goto_0

    :catch_1
    move-exception v0

    :cond_0
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2
.end method

.method private declared-synchronized maybeInit()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->mInited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->isTelephoneCapable(Landroid/telephony/TelephonyManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->mTelephoneCapable:Z

    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->mInited:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_1
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->mTelephoneCapable:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public hasRearFacingCamera()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.camera"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public isTelephoneCapable()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->maybeInit()V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;->mTelephoneCapable:Z

    return v0
.end method
