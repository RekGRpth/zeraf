.class public abstract Lcom/google/android/searchcommon/google/AbstractGoogleSource;
.super Ljava/lang/Object;
.source "AbstractGoogleSource.java"

# interfaces
.implements Lcom/google/android/searchcommon/google/GoogleSource;


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/AbstractGoogleSource;->mContext:Landroid/content/Context;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/AbstractGoogleSource;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/AbstractGoogleSource;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method


# virtual methods
.method protected abstract doQueryInternal(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation
.end method

.method public getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getClock()Lcom/google/android/searchcommon/util/Clock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/AbstractGoogleSource;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method protected getConfig()Lcom/google/android/searchcommon/SearchConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/AbstractGoogleSource;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/AbstractGoogleSource;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/Latency;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/AbstractGoogleSource;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/util/Latency;-><init>(Lcom/google/android/searchcommon/util/Clock;)V

    new-instance v1, Lcom/google/android/searchcommon/google/AbstractGoogleSource$1;

    invoke-direct {v1, p0, v0, p2}, Lcom/google/android/searchcommon/google/AbstractGoogleSource$1;-><init>(Lcom/google/android/searchcommon/google/AbstractGoogleSource;Lcom/google/android/searchcommon/util/Latency;Lcom/google/android/searchcommon/util/Consumer;)V

    invoke-virtual {p0, p1, v1}, Lcom/google/android/searchcommon/google/AbstractGoogleSource;->doQueryInternal(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method
