.class Lcom/google/android/searchcommon/google/complete/SuggestionCache$1;
.super Landroid/database/DataSetObserver;
.source "SuggestionCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/complete/SuggestionCache;-><init>(Lcom/google/android/searchcommon/CoreSearchServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/complete/SuggestionCache;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/complete/SuggestionCache;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/complete/SuggestionCache$1;->this$0:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/complete/SuggestionCache$1;->this$0:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->evictAll()V

    return-void
.end method

.method public onInvalidated()V
    .locals 1

    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/complete/SuggestionCache$1;->this$0:Lcom/google/android/searchcommon/google/complete/SuggestionCache;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/complete/SuggestionCache;->evictAll()V

    return-void
.end method
