.class Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;
.super Ljava/lang/Object;
.source "UserInteractionLogger.java"

# interfaces
.implements Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/UserInteractionLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserInteractionTimerImpl"
.end annotation


# instance fields
.field private final mStartTime:J

.field private final mTimingLabel:Ljava/lang/String;

.field private final mTimingName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/searchcommon/google/UserInteractionLogger;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/google/UserInteractionLogger;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->this$0:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->mTimingName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->mTimingLabel:Ljava/lang/String;

    # getter for: Lcom/google/android/searchcommon/google/UserInteractionLogger;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {p1}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->access$200(Lcom/google/android/searchcommon/google/UserInteractionLogger;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->mStartTime:J

    return-void
.end method


# virtual methods
.method public timingComplete()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->this$0:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    # getter for: Lcom/google/android/searchcommon/google/UserInteractionLogger;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->access$200(Lcom/google/android/searchcommon/google/UserInteractionLogger;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->mStartTime:J

    sub-long v2, v0, v4

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->this$0:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    # getter for: Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->access$300(Lcom/google/android/searchcommon/google/UserInteractionLogger;)Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    const-string v1, "TIMING"

    iget-object v4, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->mTimingName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;->mTimingLabel:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/google/analytics/tracking/android/Tracker;->trackTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method
