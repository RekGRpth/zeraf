.class public Lcom/google/android/searchcommon/google/SearchBoxLogging;
.super Ljava/lang/Object;
.source "SearchBoxLogging.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;
    }
.end annotation


# instance fields
.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private mLastShownWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private mLastShownWebSuggestionsCount:I

.field private final mStatsLock:Ljava/lang/Object;

.field private mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;

.field private mZeroQuerySuggestionsCount:I


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/HttpHelper;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p3    # Ljava/util/concurrent/Executor;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mStatsLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mBgExecutor:Ljava/util/concurrent/Executor;

    iput-object p4, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mClock:Lcom/google/android/searchcommon/util/Clock;

    new-instance v0, Lcom/google/android/searchcommon/google/SuggestUplStats;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/google/SuggestUplStats;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/google/SearchBoxLogging;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/util/Map;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logResultClickInternal(Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/google/SearchBoxLogging;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mStatsLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mLastShownWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/searchcommon/google/SearchBoxLogging;I)I
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .param p1    # I

    iput p1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mLastShownWebSuggestionsCount:I

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/google/SearchBoxLogging;)I
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iget v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mZeroQuerySuggestionsCount:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/searchcommon/google/SearchBoxLogging;I)I
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .param p1    # I

    iput p1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mZeroQuerySuggestionsCount:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/google/SearchBoxLogging;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/google/SearchBoxLogging;)Lcom/google/android/searchcommon/google/SuggestUplStats;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/google/SearchBoxLogging;)Lcom/google/android/searchcommon/util/HttpHelper;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    return-object v0
.end method

.method private getAssistedQueryStatsParam(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Ljava/lang/String;
    .locals 9
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    const/16 v8, 0x2e

    new-instance v5, Ljava/lang/StringBuffer;

    const/16 v6, 0x1e

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->getClientName(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getAbsoluteClickPosition(Lcom/google/android/searchcommon/suggest/Suggestion;)I

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-object v7, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mStatsLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v4, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mLastShownWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget v1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mLastShownWebSuggestionsCount:I

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->getExperimentStats()Ljava/lang/String;

    move-result-object v3

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, p1, p2, v4, v1}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->getAvailableSuggestionsEncoding(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Lcom/google/android/searchcommon/suggest/SuggestionList;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    :goto_0
    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    if-ne v6, v8, :cond_1

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    :cond_1
    const/4 v6, 0x0

    invoke-virtual {v5, v6, v2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private getAvailableSuggestionsEncoding(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Lcom/google/android/searchcommon/suggest/SuggestionList;I)Ljava/lang/String;
    .locals 10
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;
    .param p3    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p4    # I

    const/4 v9, 0x1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object p3

    invoke-virtual {p2, p1}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getNumShownSuggestions(Lcom/google/android/searchcommon/suggest/Suggestion;)I

    move-result v7

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result p4

    :cond_0
    if-nez p4, :cond_1

    const-string v7, ""

    :goto_0
    return-object v7

    :cond_1
    const/4 v0, -0x1

    const/4 v1, 0x0

    :try_start_0
    new-instance v5, Ljava/lang/StringBuffer;

    const/16 v7, 0x14

    invoke-direct {v5, v7}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v2, 0x0

    :goto_1
    if-ge v2, p4, :cond_4

    invoke-interface {p3, v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    if-ne v6, v0, :cond_2

    add-int/lit8 v1, v1, 0x1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    if-lez v1, :cond_3

    const/16 v7, 0x6a

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    if-le v1, v9, :cond_3

    const/16 v7, 0x6c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_3
    move v0, v6

    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    if-lez v1, :cond_5

    const/16 v7, 0x6a

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    if-le v1, v9, :cond_5

    const/16 v7, 0x6c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_5
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v7, ""

    goto :goto_0
.end method

.method private getClientName(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    const/4 v0, 0x0

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceSuggestions()Lcom/google/android/searchcommon/suggest/Suggestions;

    move-result-object v3

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebSource()Lcom/google/android/searchcommon/google/GoogleSource;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/google/android/searchcommon/google/GoogleSource;->getClientNameForLogging()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    :goto_0
    return-object v1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->getDefaultClientName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getDefaultClientName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getCompleteServerClientId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getExperimentStats()Ljava/lang/String;
    .locals 4

    new-instance v0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    const-string v1, "j"

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/searchcommon/google/SuggestUplStats;->getUplStats(Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;J)V

    iget v1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mZeroQuerySuggestionsCount:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->build()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private isRefinement(Ljava/lang/String;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p3    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logClickInternal(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 5
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    new-instance v1, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->setFollowRedirects(Z)V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    const/4 v3, 0x5

    invoke-interface {v2, v1, v3}, Lcom/google/android/searchcommon/util/HttpHelper;->get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Velvet.SearchBoxLogging"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Got redirect from click log request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException;->getRedirectLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0

    :catch_2
    move-exception v2

    goto :goto_0
.end method

.method private logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Velvet.SearchBoxLogging"

    const-string v1, "Cannot log event to GWS because URL is null/empty"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/searchcommon/google/SearchBoxLogging$4;

    invoke-direct {v1, p0, p2}, Lcom/google/android/searchcommon/google/SearchBoxLogging$4;-><init>(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private logResultClickInternal(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 9
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v6, "sa"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "sa"

    const-string v8, "T"

    invoke-virtual {v6, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logClickInternal(Landroid/net/Uri;Ljava/util/Map;)V

    return-void

    :cond_1
    const-string v6, "T"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v6, "sa"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p1, v3}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v0, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    :cond_3
    const-string v6, "sa"

    const-string v7, "T"

    invoke-virtual {v0, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    goto :goto_0
.end method

.method private sendCardAboveSrpLog(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/searchcommon/google/SearchBoxLogging$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/searchcommon/google/SearchBoxLogging$5;-><init>(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public captureShownWebSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsUi;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    new-instance v0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;-><init>(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)V

    return-object v0
.end method

.method public logEventsToGws(ILcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    if-nez p4, :cond_1

    const-string v0, "Velvet.SearchBoxLogging"

    const-string v1, "Cannot log, urlHelper==null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x8000

    and-int/2addr v0, p1

    if-eqz v0, :cond_2

    if-eqz p3, :cond_9

    invoke-direct {p0, p3, p4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->sendCardAboveSrpLog(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    const v0, -0x8001

    and-int/2addr p1, v0

    :cond_2
    :goto_1
    if-eqz p1, :cond_0

    if-eqz p2, :cond_a

    and-int/lit16 v0, p1, 0x100

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;->getAcceptUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_3
    and-int/lit16 v0, p1, 0x200

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;->getAcceptFromTimerUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_4
    and-int/lit16 v0, p1, 0x400

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;->getBailOutUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_5
    and-int/lit16 v0, p1, 0x800

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;->getRejectBySwipingCardUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_6
    and-int/lit16 v0, p1, 0x1000

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;->getRejectTimerUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_7
    and-int/lit16 v0, p1, 0x2000

    if-eqz v0, :cond_8

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;->getRejectByHittingBackUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_8
    and-int/lit16 v0, p1, 0x4000

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;->getRejectByScrollingDownUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    goto :goto_0

    :cond_9
    const-string v0, "Velvet.SearchBoxLogging"

    const-string v1, "Cannot log card above SRP because event id is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_a
    const-string v0, "Velvet.SearchBoxLogging"

    const-string v1, "Cannot log to GWS: all URLs missing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public logQueryEdit()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mStatsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/searchcommon/google/SuggestUplStats;->registerEdit(J)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public logResultClick(Lcom/google/android/searchcommon/util/UriRequest;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/searchcommon/google/SearchBoxLogging$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/google/SearchBoxLogging$1;-><init>(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/util/UriRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public logSnappyRequest(Lcom/google/android/velvet/Query;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mStatsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/google/SuggestUplStats;->registerSnappyRequest(Lcom/google/android/velvet/Query;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public logSuggestRequest()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mStatsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/searchcommon/google/SuggestUplStats;->registerSuggestRequest(J)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public logSuggestSessionStart()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mStatsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/searchcommon/google/SuggestUplStats;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/google/android/searchcommon/google/SuggestUplStats;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mLastShownWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mLastShownWebSuggestionsCount:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mZeroQuerySuggestionsCount:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendGen204(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 7
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/searchcommon/google/SearchBoxLogging$3;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/google/SearchBoxLogging$3;-><init>(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setLoggingParams(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;)V
    .locals 9
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    const/4 v8, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getClickedSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getRefinementSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getSuggestionClickLogInfo()Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isPrefetch()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getPrefetchSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-direct {p0, v5, v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->getAssistedQueryStatsParam(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p2, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setOriginalQueryString(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_0
    invoke-virtual {p2, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setAssistedQueryStats(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    return-void

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v2, v1}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->getAssistedQueryStatsParam(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7, v6, v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->isRefinement(Ljava/lang/String;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v2, v6}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-direct {p0, v6, v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->getAssistedQueryStatsParam(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v3, p1

    invoke-direct {p0, v8, v8}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->getAssistedQueryStatsParam(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
