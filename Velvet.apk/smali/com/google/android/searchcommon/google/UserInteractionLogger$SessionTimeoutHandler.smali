.class Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;
.super Landroid/os/Handler;
.source "UserInteractionLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/UserInteractionLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionTimeoutHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/UserInteractionLogger;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/google/UserInteractionLogger;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;->this$0:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/google/UserInteractionLogger;Lcom/google/android/searchcommon/google/UserInteractionLogger$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .param p2    # Lcom/google/android/searchcommon/google/UserInteractionLogger$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;-><init>(Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;->this$0:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    # invokes: Lcom/google/android/searchcommon/google/UserInteractionLogger;->endSession()V
    invoke-static {v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->access$100(Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    :cond_0
    return-void
.end method
