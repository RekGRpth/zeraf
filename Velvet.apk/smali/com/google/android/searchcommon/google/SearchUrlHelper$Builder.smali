.class public Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
.super Ljava/lang/Object;
.source "SearchUrlHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/SearchUrlHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Builder"
.end annotation


# instance fields
.field private final mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIncludeCookieHeader:Z

.field private final mParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPersistCgiParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/net/Uri;)V
    .locals 1
    .param p2    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mHeaders:Ljava/util/Map;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mIncludeCookieHeader:Z

    iput-object p2, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getAllQueryParameters(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mParams:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setDateHeader()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    return-object v0
.end method

.method private putAndCheck(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v2, "Search.SearchUrlHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "URL param or header written twice. Key: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", old value: \""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\" new value: \""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    return-void
.end method

.method private putHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mHeaders:Ljava/util/Map;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putAndCheck(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private putParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mParams:Ljava/util/Map;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putAndCheck(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setDateHeader()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # getter for: Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSimpleDateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$300(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Ljava/text/SimpleDateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Date"

    invoke-direct {p0, v1, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method


# virtual methods
.method public build()Lcom/google/android/searchcommon/util/UriRequest;
    .locals 9

    iget-object v6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mPersistCgiParameters:Ljava/util/Map;

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mPersistCgiParameters:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-object v6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mParams:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :goto_0
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mParams:Ljava/util/Map;

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const/4 v0, 0x0

    iget-boolean v6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mIncludeCookieHeader:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    const/4 v7, 0x0

    # invokes: Lcom/google/android/searchcommon/google/SearchUrlHelper;->maybeAddCookieHeader(Landroid/net/Uri;Ljava/util/Map;)Ljava/util/Map;
    invoke-static {v6, v2, v7}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$100(Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/net/Uri;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_3

    iget-object v6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mHeaders:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :goto_2
    new-instance v6, Lcom/google/android/searchcommon/util/UriRequest;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v7

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v8

    invoke-direct {v6, v2, v7, v8}, Lcom/google/android/searchcommon/util/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;Ljava/util/Map;)V

    return-object v6

    :cond_3
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mHeaders:Ljava/util/Map;

    goto :goto_2
.end method

.method public putParams(Ljava/util/Map;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public setAssistedQueryStats(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "aqs"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setBrowserDimensionsIfAvailable()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # getter for: Lcom/google/android/searchcommon/google/SearchUrlHelper;->mBrowserDimensionsSupplier:Lcom/google/common/base/Supplier;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$200(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/common/base/Supplier;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # getter for: Lcom/google/android/searchcommon/google/SearchUrlHelper;->mBrowserDimensionsSupplier:Lcom/google/common/base/Supplier;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$200(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/common/base/Supplier;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_0

    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Browser dimensions: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "biw"

    iget v2, v0, Landroid/graphics/Point;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "bih"

    iget v2, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public setCountryCode(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "gcc"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setDebugHostParam(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "host"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setDoubleRequest()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "dbl"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setDoubleRequestArchitecture()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "dbla"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setEventId(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "ei"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setHandsFree()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "hands_free"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setHostHeader(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "Host"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setIncludeCookieHeader()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mIncludeCookieHeader:Z

    return-object p0
.end method

.method public setInputMethod(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "inm"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setNativeIg()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "pbx"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setNoJesr()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "noj"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setNoSuggestions()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "sns"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setOrDisableLocation(ZLandroid/location/Location;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2
    .param p1    # Z
    .param p2    # Landroid/location/Location;

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # getter for: Lcom/google/android/searchcommon/google/SearchUrlHelper;->mApp:Lcom/google/android/velvet/VelvetApplication;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$400(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object p2

    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "action"

    const-string v1, "devloc"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "X-Geo"

    invoke-static {p2}, Lcom/google/android/searchcommon/google/XGeoEncoder;->encodeLocation(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Geo-Position"

    invoke-static {p2}, Lcom/google/android/searchcommon/google/GeoPositionEncoder;->encodeLocation(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-object p0

    :cond_2
    const-string v0, "devloc"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setOriginalQueryString(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "oq"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setPelletizedResponse()V
    .locals 2

    const-string v0, "tch"

    const-string v1, "6"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setPersistCgiParameters(Ljava/util/Map;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->mPersistCgiParameters:Ljava/util/Map;

    return-object p0
.end method

.method public setPersonalizedSearch(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "pws"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setPrefetch()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "pf"

    const-string v1, "i"

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setQueryString(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "q"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setResultStartIndex(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "start"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setRlz(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "rlz"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public setSource(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "source"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setSpeechCookie()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # getter for: Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$500(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getVoiceSearchInstallId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "X-Speech-Cookie"

    invoke-direct {p0, v1, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public setSpeechRequestId(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "X-Speech-RequestId"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public setSpokenLanguage(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "spknlang"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setStaticParams()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "redir_esc"

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "client"

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # invokes: Lcom/google/android/searchcommon/google/SearchUrlHelper;->getClientParam()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$600(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "hl"

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # invokes: Lcom/google/android/searchcommon/google/SearchUrlHelper;->getHlParameterForSearch()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$700(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "safe"

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # invokes: Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSafeSearchValue()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$800(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setSubmissionTime(J)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2
    .param p1    # J

    const-string v0, "qsubts"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setSuppressAnswers()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # getter for: Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$900(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getCardSuppressedQueryParam()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # getter for: Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$900(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getCardSuppressedQueryParamValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setTemperatureFahrenheit(Z)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2
    .param p1    # Z

    const-string v1, "fheit"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public setTimezone()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 2

    const-string v0, "ctzn"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setUserAgent(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "User-Agent"

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setWebCorpus(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->this$0:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    # getter for: Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->access$900(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getWebCorpusQueryParam()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParam(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method
