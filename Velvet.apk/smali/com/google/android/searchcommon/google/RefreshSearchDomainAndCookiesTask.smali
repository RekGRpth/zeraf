.class public Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;
.super Ljava/lang/Object;
.source "RefreshSearchDomainAndCookiesTask.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/ForceableLock$Owner;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/google/android/searchcommon/util/ForceableLock$Owner;"
    }
.end annotation


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mCookies:Lcom/google/android/velvet/Cookies;

.field private mCountryCode:Ljava/lang/String;

.field private mDomain:Ljava/lang/String;

.field private final mFactory:Lcom/google/android/velvet/VelvetFactory;

.field private final mGoogleAccountToUse:Ljava/lang/String;

.field mGsaSearchParametersRequest:Lcom/google/android/searchcommon/util/UriRequest;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private mInterrupted:Z

.field private final mIsForcedRun:Z

.field private final mLock:Ljava/lang/Object;

.field private mLoggedInUser:Ljava/lang/String;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mNeedToLogout:Z

.field private mRefreshWebViewCookiesAt:J

.field private mSearchLanguage:Ljava/lang/String;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private mUseSsl:Z

.field private mWebView:Landroid/webkit/WebView;

.field private mWebViewDestroyDone:Z

.field private mWebViewError:Z

.field private final mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

.field private mWebViewInitDone:Z

.field private mWebViewLoggedInAccount:Ljava/lang/String;

.field private mWebViewLoggedInDomain:Ljava/lang/String;

.field mWebViewLoginDomain:Landroid/net/Uri;

.field private mWebViewPageFinished:Z

.field private mWebViewTimeoutAt:J


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/velvet/Cookies;Lcom/google/android/searchcommon/util/ForceableLock;Ljava/util/concurrent/Executor;Lcom/google/android/velvet/VelvetFactory;Z)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/util/Clock;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;
    .param p4    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p5    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p6    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p7    # Lcom/google/android/velvet/Cookies;
    .param p8    # Lcom/google/android/searchcommon/util/ForceableLock;
    .param p9    # Ljava/util/concurrent/Executor;
    .param p10    # Lcom/google/android/velvet/VelvetFactory;
    .param p11    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p5, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p7, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mCookies:Lcom/google/android/velvet/Cookies;

    iput-object p8, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    iput-object p9, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUiExecutor:Ljava/util/concurrent/Executor;

    iput-object p10, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mGoogleAccountToUse:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getWebViewLoggedInAccount()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoggedInAccount:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getWebViewLoggedInDomain()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoggedInDomain:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getRefreshWebViewCookiesAt()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mRefreshWebViewCookiesAt:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mRefreshWebViewCookiesAt:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoggedInAccount:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoggedInAccount:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mGoogleAccountToUse:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mNeedToLogout:Z

    if-nez p11, :cond_1

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mRefreshWebViewCookiesAt:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mIsForcedRun:Z

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->createWebViewAndStartLoad(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)Landroid/webkit/WebView;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewDestroyDone:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;ZZ)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;
    .param p1    # Z
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->updateWebViewLoginState(ZZ)V

    return-void
.end method

.method private checkInterrupted()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mInterrupted:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mInterrupted:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private clearWebViewLoginState()V
    .locals 4

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setWebViewLoggedInAccount(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setWebViewLoggedInDomain(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0, v2, v3}, Lcom/google/android/searchcommon/SearchSettings;->setRefreshWebViewCookiesAt(J)V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoggedInAccount:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoggedInDomain:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mRefreshWebViewCookiesAt:J

    return-void
.end method

.method private createWebViewAndStartLoad(Landroid/net/Uri;)V
    .locals 6
    .param p1    # Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mInterrupted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetFactory;->createOffscreenWebView()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebView:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;-><init>(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$1;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getWebViewLoginLoadTimeoutMs()I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewTimeoutAt:J

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewInitDone:Z

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private logoutCurrentUser()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-virtual {v0}, Lcom/google/android/velvet/Cookies;->removeAllCookies()V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-virtual {v0}, Lcom/google/android/velvet/Cookies;->sync()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->clearWebViewLoginState()V

    return-void
.end method

.method private maybeDestroyWebview()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebView:Landroid/webkit/WebView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$2;

    invoke-direct {v2, p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$2;-><init>(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewDestroyDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    :try_start_2
    iput-boolean v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mInterrupted:Z

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    return-void
.end method

.method private maybeStartWebViewLogin()Z
    .locals 10

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v6}, Lcom/google/android/searchcommon/SearchSettings;->getWebViewLoggedInAccount()Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v6}, Lcom/google/android/searchcommon/SearchSettings;->getWebViewLoggedInDomain()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v6}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v6, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-wide v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mRefreshWebViewCookiesAt:J

    iget-object v8, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v6}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getLoginDomainUrl()Landroid/net/Uri;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoginDomain:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v7, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoginDomain:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v8}, Lcom/google/android/searchcommon/SearchConfig;->getPersonalizedSearchService()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->blockingGetGaiaWebLoginLink(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v5, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    const-string v6, ""

    invoke-interface {v5, v6}, Lcom/google/android/searchcommon/SearchSettings;->setWebViewLoggedInDomain(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v6, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$1;

    invoke-direct {v6, p0, v3}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$1;-><init>(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;Landroid/net/Uri;)V

    invoke-interface {v4, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    monitor-enter v6

    :goto_1
    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewInitDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_2

    :try_start_1
    iget-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v4, 0x1

    :try_start_2
    iput-boolean v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mInterrupted:Z

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :cond_2
    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v4, v5

    goto :goto_0
.end method

.method private updateSettingsFromWebViewLoginResult()V
    .locals 5

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewError:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewPageFinished:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-virtual {v0}, Lcom/google/android/velvet/Cookies;->sync()V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mGoogleAccountToUse:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setWebViewLoggedInAccount(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewLoginDomain:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setWebViewLoggedInDomain(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->getRefreshSearchParametersCookieRefreshPeriodMs()J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/SearchSettings;->setRefreshWebViewCookiesAt(J)V

    goto :goto_0
.end method

.method private updateWebViewLoginState(ZZ)V
    .locals 6
    .param p1    # Z
    .param p2    # Z

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewPageFinished:Z

    iput-boolean p2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewError:Z

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v0

    if-eqz p2, :cond_0

    iput-wide v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewTimeoutAt:J

    :goto_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getWebViewLoginRedirectTimeoutMs()I

    move-result v2

    int-to-long v4, v2

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewTimeoutAt:J

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getWebViewLoginLoadTimeoutMs()I

    move-result v2

    int-to-long v4, v2

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewTimeoutAt:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private waitForWebViewLoginToComplete()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    :goto_0
    iget-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewTimeoutAt:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    iget-object v5, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    iget-wide v8, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewTimeoutAt:J

    sub-long/2addr v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    iget-wide v6, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewTimeoutAt:J

    iget-object v8, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/Object;->wait(J)V

    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mIsForcedRun:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mNeedToLogout:Z

    if-eqz v2, :cond_3

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v2, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->forceObtain(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    iget-boolean v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mNeedToLogout:Z

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->logoutCurrentUser()V

    :cond_1
    const-string v2, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v3, "refreshing search domain"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->checkInterrupted()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->fetchSearchParameters()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->checkInterrupted()V

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->parseSearchParametersJson(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->validateSearchParams()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->checkInterrupted()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->saveToSettings()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->maybeStartWebViewLogin()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v3, "refreshing cookies"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->waitForWebViewLoginToComplete()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->checkInterrupted()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->updateSettingsFromWebViewLoginResult()V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v3, "mobilepersonalfeeds"

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->invalidateAuthToken(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->maybeDestroyWebview()V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v2, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->release(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    :goto_0
    return-object v4

    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v2, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->tryObtain(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->maybeDestroyWebview()V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v2, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->release(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v3, "refresh interrupted"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->maybeDestroyWebview()V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v2, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->release(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->maybeDestroyWebview()V

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v3, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->release(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    throw v2
.end method

.method fetchSearchParameters()Ljava/lang/String;
    .locals 6

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getGsaSearchParametersRequest()Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mGsaSearchParametersRequest:Lcom/google/android/searchcommon/util/UriRequest;

    new-instance v1, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mGsaSearchParametersRequest:Lcom/google/android/searchcommon/util/UriRequest;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mGsaSearchParametersRequest:Lcom/google/android/searchcommon/util/UriRequest;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/util/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->setUseCaches(Z)V

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    const/16 v4, 0x9

    invoke-interface {v3, v1, v4}, Lcom/google/android/searchcommon/util/HttpHelper;->get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v3, "Search.RefreshSearchDomainAndCookiesTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Search parameters fetch failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public forceReleaseLock()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mInterrupted:Z

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method parseSearchParametersJson(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v5, "Search parameters fetch failed"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v3

    :cond_0
    :try_start_0
    new-instance v2, Landroid/util/JsonReader;

    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v4}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v2}, Landroid/util/JsonReader;->beginObject()V

    :goto_1
    invoke-virtual {v2}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    if-eq v4, v5, :cond_6

    invoke-virtual {v2}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    const-string v4, "domain"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mDomain:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v4, "Search.RefreshSearchDomainAndCookiesTask"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Search parameters parsing failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v4, "countryCode"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mCountryCode:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v4, "userLang"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSearchLanguage:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string v4, "loggedInUser"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLoggedInUser:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const-string v4, "useSsl"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUseSsl:Z

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_1

    :cond_6
    invoke-virtual {v2}, Landroid/util/JsonReader;->endObject()V

    invoke-virtual {v2}, Landroid/util/JsonReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v3, 0x1

    goto/16 :goto_0
.end method

.method saveToSettings()V
    .locals 6

    iget-boolean v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUseSsl:Z

    if-eqz v2, :cond_1

    const-string v1, "https"

    :goto_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mDomain:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mCountryCode:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSearchLanguage:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v1, v3, v4, v5}, Lcom/google/android/searchcommon/SearchSettings;->setSearchDomain(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mGsaSearchParametersRequest:Lcom/google/android/searchcommon/util/UriRequest;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLoggedInUser:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/SearchSettings;->setWebViewLoggedInAccount(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v2, v0}, Lcom/google/android/searchcommon/SearchSettings;->setWebViewLoggedInDomain(Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v1, "http"

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mLoggedInUser:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/SearchSettings;->setWebViewLoggedInAccount(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method validateSearchParams()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mDomain:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v2, "Search parameters didn\'t specify domain"

    invoke-static {v1, v2}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->mCountryCode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v2, "Search parameters didn\'t specify country code"

    invoke-static {v1, v2}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
