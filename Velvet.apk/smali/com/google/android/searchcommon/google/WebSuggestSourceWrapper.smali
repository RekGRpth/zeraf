.class public Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;
.super Ljava/lang/Object;
.source "WebSuggestSourceWrapper.java"

# interfaces
.implements Lcom/google/android/searchcommon/google/WebSuggestSource;


# instance fields
.field private final mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;


# direct methods
.method protected constructor <init>(Lcom/google/android/searchcommon/google/WebSuggestSource;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    return-void
.end method


# virtual methods
.method public canQueryNow()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/WebSuggestSource;->canQueryNow()Z

    move-result v0

    return v0
.end method

.method public getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSource;->getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    return-object v0
.end method

.method public getClientNameForLogging()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/WebSuggestSource;->getClientNameForLogging()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSourceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/WebSuggestSource;->getSourceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0, p1, p2}, Lcom/google/android/searchcommon/google/WebSuggestSource;->getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public isLikelyToReturnZeroQueryResults()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/WebSuggestSource;->isLikelyToReturnZeroQueryResults()Z

    move-result v0

    return v0
.end method

.method public logClick(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/searchcommon/google/WebSuggestSource;->logClick(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public queryExternal(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSource;->queryExternal(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    return-object v0
.end method

.method public removeFromHistory(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->mWrapped:Lcom/google/android/searchcommon/google/WebSuggestSource;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSource;->removeFromHistory(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
