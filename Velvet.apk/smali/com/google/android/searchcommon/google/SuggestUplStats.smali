.class public Lcom/google/android/searchcommon/google/SuggestUplStats;
.super Ljava/lang/Object;
.source "SuggestUplStats.java"


# instance fields
.field private mCacheHitCount:I

.field private mFirstEditTime:J

.field private mLastEditTime:J

.field private mPendingRequest:Lcom/google/android/velvet/Query;

.field private mPendingRequestTime:J

.field private mServerResponseCount:I

.field private final mSessionStartTime:J

.field private mSnappySuggestCount:I

.field private mTotalRequestCount:I

.field private mTotalRoundTripTime:J


# direct methods
.method public constructor <init>(J)V
    .locals 0
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mSessionStartTime:J

    return-void
.end method


# virtual methods
.method public getUplStats(Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;J)V
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;
    .param p2    # J

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mSessionStartTime:J

    sub-long v0, p2, v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mFirstEditTime:J

    iget-wide v2, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mSessionStartTime:J

    sub-long/2addr v0, v2

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mLastEditTime:J

    iget-wide v2, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mSessionStartTime:J

    sub-long/2addr v0, v2

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    iget v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mTotalRequestCount:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    iget v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mCacheHitCount:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    iget v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mSnappySuggestCount:I

    iget v1, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mCacheHitCount:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    iget v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mServerResponseCount:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mTotalRoundTripTime:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;

    return-void
.end method

.method public registerEdit(J)V
    .locals 4
    .param p1    # J

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mFirstEditTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mFirstEditTime:J

    :cond_0
    iput-wide p1, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mLastEditTime:J

    return-void
.end method

.method public registerSnappyRequest(Lcom/google/android/velvet/Query;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mPendingRequestTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mSnappySuggestCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mSnappySuggestCount:I

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mPendingRequest:Lcom/google/android/velvet/Query;

    :cond_0
    return-void
.end method

.method public registerSuggestRequest(J)V
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mTotalRequestCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mTotalRequestCount:I

    iput-wide p1, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mPendingRequestTime:J

    return-void
.end method

.method public registerSuggestResponse(JZLcom/google/android/velvet/Query;)V
    .locals 6
    .param p1    # J
    .param p3    # Z
    .param p4    # Lcom/google/android/velvet/Query;

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mPendingRequestTime:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mPendingRequest:Lcom/google/android/velvet/Query;

    if-eq v0, p4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    iget v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mCacheHitCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mCacheHitCount:I

    :goto_1
    iput-wide v4, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mPendingRequestTime:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mPendingRequest:Lcom/google/android/velvet/Query;

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mServerResponseCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mServerResponseCount:I

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mTotalRoundTripTime:J

    iget-wide v2, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mPendingRequestTime:J

    sub-long v2, p1, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/searchcommon/google/SuggestUplStats;->mTotalRoundTripTime:J

    goto :goto_1
.end method
