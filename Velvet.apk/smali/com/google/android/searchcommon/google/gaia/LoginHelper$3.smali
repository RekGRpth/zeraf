.class Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;
.super Ljava/lang/Object;
.source "LoginHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/gaia/LoginHelper;->updateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$authTokenType:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->val$authTokenType:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->val$callback:Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    # getter for: Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->access$500(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->val$authTokenType:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->val$callback:Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    # getter for: Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v6}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->access$200(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->getHandler()Landroid/os/Handler;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method
