.class public Lcom/google/android/searchcommon/google/LocationOptIn;
.super Ljava/lang/Object;
.source "LocationOptIn.java"

# interfaces
.implements Lcom/google/android/searchcommon/google/LocationSettings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;,
        Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mContext:Landroid/content/Context;

.field private final mGetGoogleLocationSetting:Ljava/lang/Runnable;

.field private final mGetSystemLocationSetting:Ljava/lang/Runnable;

.field private final mGoogleLocationObserver:Landroid/database/ContentObserver;

.field private mGoogleLocationSetting:I

.field private mGoogleLocationSettingValid:Z

.field private mIsGoogleSettingSupported:I

.field private final mLocationManager:Landroid/location/LocationManager;

.field private final mLocationProviderReceiver:Landroid/content/BroadcastReceiver;

.field private final mObservers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/searchcommon/google/LocationSettings$Observer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettingsLock:Ljava/lang/Object;

.field private mSystemLocationEnabled:Z

.field private mSystemLocationSettingValid:Z

.field private final mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p3    # Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mIsGoogleSettingSupported:I

    new-instance v0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;-><init>(Lcom/google/android/searchcommon/google/LocationOptIn;Lcom/google/android/searchcommon/google/LocationOptIn$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGetSystemLocationSetting:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;-><init>(Lcom/google/android/searchcommon/google/LocationOptIn;Lcom/google/android/searchcommon/google/LocationOptIn$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGetGoogleLocationSetting:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mObservers:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGetGoogleLocationSetting:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mLocationManager:Landroid/location/LocationManager;

    new-instance v0, Lcom/google/android/searchcommon/google/LocationOptIn$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/google/LocationOptIn$1;-><init>(Lcom/google/android/searchcommon/google/LocationOptIn;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mLocationProviderReceiver:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mLocationProviderReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.location.PROVIDERS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lcom/google/android/searchcommon/google/LocationOptIn$2;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/google/LocationOptIn$2;-><init>(Lcom/google/android/searchcommon/google/LocationOptIn;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationObserver:Landroid/database/ContentObserver;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/searchcommon/google/LocationOptIn$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/google/LocationOptIn$3;-><init>(Lcom/google/android/searchcommon/google/LocationOptIn;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/searchcommon/google/LocationOptIn;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationSettingValid:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/searchcommon/google/LocationOptIn;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/searchcommon/google/LocationOptIn;)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationEnabled:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/searchcommon/google/LocationOptIn;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSettingValid:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGetSystemLocationSetting:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGetGoogleLocationSetting:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/google/LocationOptIn;)Landroid/database/ContentObserver;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/google/LocationOptIn;)Landroid/location/LocationManager;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/searchcommon/google/LocationOptIn;)I
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;

    iget v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSetting:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/searchcommon/google/LocationOptIn;ZI)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/LocationOptIn;
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/LocationOptIn;->updateSettingsLocked(ZI)V

    return-void
.end method

.method private getGoogleLocationSettingLocked()I
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    invoke-static {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkHoldsLock(Ljava/lang/Object;)V

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSettingValid:Z

    if-nez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "QSB.LocationOptIn"

    const-string v2, "Unexpected InterrupedException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSetting:I

    return v1
.end method

.method private isGoogleLocationEnabledLocked()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/LocationOptIn;->getGoogleLocationSettingLocked()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/searchcommon/google/LocationOptIn;->isOn(I)Z

    move-result v0

    return v0
.end method

.method private static isNotSet(I)Z
    .locals 1
    .param p0    # I

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isOn(I)Z
    .locals 1
    .param p0    # I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSystemLocationEnabledLocked()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    invoke-static {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkHoldsLock(Ljava/lang/Object;)V

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationSettingValid:Z

    if-nez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "QSB.LocationOptIn"

    const-string v2, "Unexpected InterrupedException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationEnabled:Z

    return v1
.end method

.method private notifyObserversLocked(Z)V
    .locals 3
    .param p1    # Z

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mObservers:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v2, Lcom/google/android/searchcommon/google/LocationOptIn$4;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/searchcommon/google/LocationOptIn$4;-><init>(Lcom/google/android/searchcommon/google/LocationOptIn;Ljava/util/Set;Z)V

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private updateSettingsLocked(ZI)V
    .locals 5
    .param p1    # Z
    .param p2    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    invoke-static {v4}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkHoldsLock(Ljava/lang/Object;)V

    iget-boolean v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationEnabled:Z

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSetting:I

    invoke-static {v4}, Lcom/google/android/searchcommon/google/LocationOptIn;->isOn(I)Z

    move-result v4

    if-eqz v4, :cond_1

    move v1, v2

    :goto_0
    iput-boolean p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationEnabled:Z

    iput p2, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSetting:I

    iget-boolean v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationEnabled:Z

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSetting:I

    invoke-static {v4}, Lcom/google/android/searchcommon/google/LocationOptIn;->isOn(I)Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v2

    :goto_1
    if-eq v1, v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/LocationOptIn;->notifyObserversLocked(Z)V

    :cond_0
    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1
.end method


# virtual methods
.method public addUseLocationObserver(Lcom/google/android/searchcommon/google/LocationSettings$Observer;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/google/LocationSettings$Observer;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mObservers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public canUseLocationForGoogleApps()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/LocationOptIn;->isGoogleSettingForAllApps()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/LocationOptIn;->canUseLocationForSearch()Z

    move-result v0

    goto :goto_0
.end method

.method public canUseLocationForSearch()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/LocationOptIn;->isSystemLocationEnabledLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/LocationOptIn;->isGoogleLocationEnabledLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public dispose()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mLocationProviderReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mObservers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGoogleSettingIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gsf.GOOGLE_APPS_LOCATION_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public isGoogleSettingForAllApps()Z
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mIsGoogleSettingSupported:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-gt v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.gsf.GOOGLE_APPS_LOCATION_SETTINGS"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x10000

    invoke-virtual {v1, v4, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-nez v0, :cond_1

    move v1, v2

    :goto_0
    iput v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mIsGoogleSettingSupported:I

    :cond_0
    :goto_1
    iget v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mIsGoogleSettingSupported:I

    if-ne v1, v3, :cond_3

    :goto_2
    return v3

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    iput v3, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mIsGoogleSettingSupported:I

    goto :goto_1

    :cond_3
    move v3, v2

    goto :goto_2
.end method

.method public maybeShowLegacyOptIn()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/LocationOptIn;->isGoogleSettingForAllApps()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/LocationOptIn;->isSystemLocationEnabledLocked()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/LocationOptIn;->getGoogleLocationSettingLocked()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/searchcommon/google/LocationOptIn;->isNotSet(I)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.action.SET_USE_LOCATION_FOR_SERVICES"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_2
    iget-object v2, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "QSB.LocationOptIn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t start location opt-in: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeUseLocationObserver(Lcom/google/android/searchcommon/google/LocationSettings$Observer;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/google/LocationSettings$Observer;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn;->mObservers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
