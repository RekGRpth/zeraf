.class Lcom/google/android/searchcommon/google/SearchBoxLogging$4;
.super Ljava/lang/Object;
.source "SearchBoxLogging.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventToGws(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

.field final synthetic val$urlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$4;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$4;->val$urlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$4;->val$urlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchBaseUri(ZZ)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->build()Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v2

    new-instance v1, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$4;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    # getter for: Lcom/google/android/searchcommon/google/SearchBoxLogging;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;
    invoke-static {v3}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->access$700(Lcom/google/android/searchcommon/google/SearchBoxLogging;)Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v3

    const/4 v4, 0x5

    invoke-interface {v3, v1, v4}, Lcom/google/android/searchcommon/util/HttpHelper;->get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "Velvet.SearchBoxLogging"

    const-string v4, "Could not do GWS gen_204"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
