.class public Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;
.super Ljava/lang/Object;
.source "ResizingImageLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/UriLoader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/UriLoader",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final mMaxHeight:I

.field private final mMaxWidth:I

.field private final mWrapped:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILcom/google/android/searchcommon/util/UriLoader;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mMaxWidth:I

    iput p2, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mMaxHeight:I

    iput-object p3, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->resize(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private resize(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    if-eqz p1, :cond_0

    instance-of v3, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_0

    move-object v3, p1

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mMaxWidth:I

    if-gt v3, v4, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mMaxHeight:I

    if-le v3, v4, :cond_0

    :cond_2
    iget v3, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mMaxWidth:I

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mMaxHeight:I

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v0, v3, v4, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/graphics/Bitmap;->setDensity(I)V

    new-instance p1, Landroid/graphics/drawable/BitmapDrawable;

    const/4 v3, 0x0

    invoke-direct {p1, v3, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method


# virtual methods
.method getMaxHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mMaxHeight:I

    return v0
.end method

.method getMaxWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mMaxWidth:I

    return v0
.end method

.method getWrapped()Lcom/google/android/searchcommon/util/UriLoader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    return-object v0
.end method

.method public load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader$1;-><init>(Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;Lcom/google/android/searchcommon/util/NowOrLater;)V

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
