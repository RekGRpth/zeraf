.class public Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;
.super Ljava/lang/Object;
.source "ImageLoadersManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$1;,
        Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;
    }
.end annotation


# instance fields
.field private final mCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;",
            ">;"
        }
    .end annotation
.end field

.field private final mImageLoadersFactory:Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->mImageLoadersFactory:Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->mCache:Ljava/util/Map;

    return-void
.end method

.method private createIfNull(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;Lcom/google/android/searchcommon/imageloader/PackageImageLoader;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    .param p2    # Lcom/google/android/searchcommon/imageloader/PackageImageLoader;
    .param p3    # Z

    if-eqz p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->mImageLoadersFactory:Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->createCachingImageLoader(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    move-result-object p1

    goto :goto_0
.end method

.method private purgeIfNotNull(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->purge()V

    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized clean(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->mCache:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized clearCache()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->mCache:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;

    iget-object v2, v1, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->iconSizeLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->purgeIfNotNull(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;)V

    iget-object v2, v1, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->nonIconSizeLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->purgeIfNotNull(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized getImageLoaderForPackage(Ljava/lang/String;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->mCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;-><init>(Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$1;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->mImageLoadersFactory:Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->createPackageImageLoader(Ljava/lang/String;)Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->packageLoader:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->mCache:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-eqz p2, :cond_1

    iget-object v1, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->iconSizeLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    iget-object v2, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->packageLoader:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    invoke-direct {p0, v1, v2, p2}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->createIfNull(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;Lcom/google/android/searchcommon/imageloader/PackageImageLoader;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->iconSizeLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    iget-object v1, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->iconSizeLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_1
    :try_start_1
    iget-object v1, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->nonIconSizeLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    iget-object v2, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->packageLoader:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    invoke-direct {p0, v1, v2, p2}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->createIfNull(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;Lcom/google/android/searchcommon/imageloader/PackageImageLoader;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->nonIconSizeLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    iget-object v1, v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager$Loaders;->nonIconSizeLoader:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
