.class Lcom/google/android/searchcommon/imageloader/ResizingImageLoader$1;
.super Lcom/google/android/searchcommon/util/NowOrLaterWrapper;
.source "ResizingImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/NowOrLaterWrapper",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;Lcom/google/android/searchcommon/util/NowOrLater;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader$1;->this$0:Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/util/NowOrLaterWrapper;-><init>(Lcom/google/android/searchcommon/util/NowOrLater;)V

    return-void
.end method


# virtual methods
.method public get(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader$1;->this$0:Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;

    # invokes: Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->resize(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    invoke-static {v0, p1}, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;->access$000(Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader$1;->get(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
