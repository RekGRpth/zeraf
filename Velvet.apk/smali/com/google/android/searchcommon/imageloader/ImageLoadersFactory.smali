.class public Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;
.super Ljava/lang/Object;
.source "ImageLoadersFactory.java"


# instance fields
.field private final mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

.field private final mContext:Landroid/content/Context;

.field private final mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/AsyncServices;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/AsyncServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    return-void
.end method


# virtual methods
.method public createCachingImageLoader(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/imageloader/PackageImageLoader;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v4, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    move v3, v2

    :goto_0
    move-object v0, p1

    new-instance v1, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;

    invoke-direct {v1, v3, v2, v0}, Lcom/google/android/searchcommon/imageloader/ResizingImageLoader;-><init>(IILcom/google/android/searchcommon/util/UriLoader;)V

    new-instance v0, Lcom/google/android/searchcommon/util/PostToExecutorLoader;

    iget-object v4, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lcom/google/android/searchcommon/util/PostToExecutorLoader;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/UriLoader;)V

    new-instance v4, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {v4, v0}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;-><init>(Lcom/google/android/searchcommon/util/UriLoader;)V

    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    goto :goto_0
.end method

.method public createPackageImageLoader(Ljava/lang/String;)Lcom/google/android/searchcommon/imageloader/PackageImageLoader;
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v3, p0, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/AsyncServices;->getExclusiveBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    return-object v0
.end method
