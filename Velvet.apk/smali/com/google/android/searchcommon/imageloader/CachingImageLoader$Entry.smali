.class Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;
.super Lcom/google/android/searchcommon/util/CachedLater;
.source "CachingImageLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Entry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/CachedLater",
        "<",
        "Landroid/graphics/drawable/Drawable$ConstantState;",
        ">;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDrawable:Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mDrawableId:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;Landroid/net/Uri;)V
    .locals 1
    .param p2    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->this$0:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/CachedLater;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->mDrawableId:Landroid/net/Uri;

    new-instance v0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry$1;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry$1;-><init>(Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;Lcom/google/android/searchcommon/util/NowOrLater;Lcom/google/android/searchcommon/imageloader/CachingImageLoader;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->mDrawable:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    return-void
.end method


# virtual methods
.method public consume(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->store(Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->storeNothing()V

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->consume(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method

.method protected create()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->this$0:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    # getter for: Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;
    invoke-static {v1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->access$000(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;)Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->mDrawableId:Landroid/net/Uri;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/util/NowOrLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public getDrawable()Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->prefetch()V

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->mDrawable:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    return-object v0
.end method
