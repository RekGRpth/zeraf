.class public Lcom/google/android/searchcommon/summons/ContactsSourceFilter;
.super Ljava/lang/Object;
.source "ContactsSourceFilter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionFilter;


# static fields
.field public static final INSTANCE:Lcom/google/android/searchcommon/summons/ContactsSourceFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/searchcommon/summons/ContactsSourceFilter;

    invoke-direct {v0}, Lcom/google/android/searchcommon/summons/ContactsSourceFilter;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/summons/ContactsSourceFilter;->INSTANCE:Lcom/google/android/searchcommon/summons/ContactsSourceFilter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;

    const-string v0, "android.provider.Contacts.SEARCH_SUGGESTION_CLICKED"

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
