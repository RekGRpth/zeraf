.class public interface abstract Lcom/google/android/searchcommon/summons/ContentProviderSource;
.super Ljava/lang/Object;
.source "ContentProviderSource.java"

# interfaces
.implements Lcom/google/android/searchcommon/summons/Source;


# virtual methods
.method public abstract getIntentComponent()Landroid/content/ComponentName;
.end method

.method public abstract getQueryThreshold()I
.end method

.method public abstract getSuggestUri()Ljava/lang/String;
.end method

.method public abstract getSuggestions(Lcom/google/android/velvet/Query;ILandroid/os/CancellationSignal;)Lcom/google/android/searchcommon/suggest/SuggestionList;
.end method

.method public abstract isIgnoreIcon1()Z
.end method

.method public abstract isReadable()Z
.end method

.method public abstract queryAfterZeroResults()Z
.end method
