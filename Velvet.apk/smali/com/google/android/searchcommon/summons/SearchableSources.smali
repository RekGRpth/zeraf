.class public Lcom/google/android/searchcommon/summons/SearchableSources;
.super Ljava/lang/Object;
.source "SearchableSources.java"

# interfaces
.implements Lcom/google/android/searchcommon/summons/Sources;


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private final mObservable:Landroid/database/DataSetObservable;

.field private final mSearchManager:Landroid/app/SearchManager;

.field private mSourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;"
        }
    .end annotation
.end field

.field private final mSources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;"
        }
    .end annotation
.end field

.field private final mSuggestionFilterProvider:Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mObservable:Landroid/database/DataSetObservable;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mClock:Lcom/google/android/searchcommon/util/Clock;

    const-string v0, "search"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSearchManager:Landroid/app/SearchManager;

    iput-object p4, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSuggestionFilterProvider:Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSourceList:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Landroid/database/DataSetObservable;

    invoke-direct {v2}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mObservable:Landroid/database/DataSetObservable;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    iput-object v3, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mContext:Landroid/content/Context;

    iput-object v3, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object v3, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object v3, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSearchManager:Landroid/app/SearchManager;

    iput-object v3, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSuggestionFilterProvider:Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSourceList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/summons/ContentProviderSource;

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    invoke-interface {v1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSourceList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method private addSearchableSources()V
    .locals 8

    iget-object v6, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSearchManager:Landroid/app/SearchManager;

    invoke-virtual {v6}, Landroid/app/SearchManager;->getSearchablesInGlobalSearch()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v6, "Search.SearchableSources"

    const-string v7, "getSearchablesInGlobalSearch() returned null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchableInfo;

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/summons/SearchableSources;->createSearchableSource(Landroid/app/SearchableInfo;)Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v6, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v6, v3}, Lcom/google/android/searchcommon/SearchConfig;->isSourceIgnored(Lcom/google/android/searchcommon/summons/Source;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-interface {v3}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->isReadable()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_2

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    :cond_3
    if-eqz v3, :cond_1

    goto :goto_1

    :cond_4
    iget-object v7, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    iget-object v6, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iput-object v4, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSourceList:Ljava/util/List;

    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method private createSearchableSource(Landroid/app/SearchableInfo;)Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .locals 8
    .param p1    # Landroid/app/SearchableInfo;

    const/4 v7, 0x0

    if-nez p1, :cond_0

    move-object v0, v7

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/searchcommon/summons/SearchableSource;

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v3, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v5, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSuggestionFilterProvider:Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/summons/SearchableSource;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/Clock;Landroid/app/SearchableInfo;Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v0, "Search.SearchableSources"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Source not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    goto :goto_0
.end method


# virtual methods
.method public getContentProviderSource(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/SearchableSources;->getContentProviderSourceIfExists(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/summons/ContentProviderSource;

    return-object v0
.end method

.method public getContentProviderSourceIfExists(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/summons/ContentProviderSource;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getContentProviderSources()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSources:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mSourceList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSource(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/Source;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/SearchableSources;->getContentProviderSource(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v0

    return-object v0
.end method

.method public getSourceIfExists(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/Source;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/SearchableSources;->getContentProviderSourceIfExists(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-result-object v0

    return-object v0
.end method

.method public getSources()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<+",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/SearchableSources;->getContentProviderSources()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public update()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/SearchableSources;->addSearchableSources()V

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSources;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void
.end method
