.class public Lcom/google/android/searchcommon/summons/SourceResultBuilder;
.super Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;
.source "SourceResultBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder",
        "<",
        "Lcom/google/android/searchcommon/suggest/SuggestionList;",
        ">;"
    }
.end annotation


# instance fields
.field private final mFilter:Lcom/google/android/searchcommon/suggest/SuggestionFilter;

.field private final mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/summons/ContentProviderSource;Lcom/google/android/velvet/Query;Landroid/database/Cursor;Lcom/google/android/searchcommon/suggest/SuggestionFilter;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Landroid/database/Cursor;
    .param p4    # Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    invoke-direct {p0, p2, p3}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;-><init>(Lcom/google/android/velvet/Query;Landroid/database/Cursor;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;

    iput-object p4, p0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->mFilter:Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    return-void
.end method

.method private isQsbApp(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    const-string v0, "content://applications/applications/com.google.android.googlequicksearchbox/com.google.android.googlequicksearchbox.SearchActivity"

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected createSuggestionList(Lcom/google/android/velvet/Query;Ljava/util/List;Z)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;Z)",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/searchcommon/suggest/SuggestionListFactory;->createSuggestionList(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/util/List;Z)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    return-object v0
.end method

.method public getExtras()Lcom/google/android/searchcommon/suggest/SuggestionExtras;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->mCursor:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/MapBackedSuggestionExtras;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    move-result-object v0

    return-object v0
.end method

.method protected getIcon1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->isIgnoreIcon1()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->getIcon1()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSuggestionSource()Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;

    return-object v0
.end method

.method public isHistory()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected shouldKeepSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->shouldKeepSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->isQsbApp(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->mFilter:Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->mFilter:Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/suggest/SuggestionFilter;->accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
