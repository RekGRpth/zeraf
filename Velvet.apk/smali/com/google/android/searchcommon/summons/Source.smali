.class public interface abstract Lcom/google/android/searchcommon/summons/Source;
.super Ljava/lang/Object;
.source "Source.java"


# virtual methods
.method public abstract getDefaultIntentAction()Ljava/lang/String;
.end method

.method public abstract getDefaultIntentData()Ljava/lang/String;
.end method

.method public abstract getIconPackage()Ljava/lang/String;
.end method

.method public abstract getIconUri(Ljava/lang/String;)Landroid/net/Uri;
.end method

.method public abstract getLabel()Ljava/lang/CharSequence;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPackageName()Ljava/lang/String;
.end method

.method public abstract getSettingsDescription()Ljava/lang/CharSequence;
.end method

.method public abstract getSourceIcon()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getSourceIconUri()Landroid/net/Uri;
.end method

.method public abstract hasFullSizeIcon()Z
.end method

.method public abstract isContactsSource()Z
.end method

.method public abstract isEnabledByDefault()Z
.end method

.method public abstract isShowSingleLine()Z
.end method

.method public abstract isTrusted()Z
.end method
