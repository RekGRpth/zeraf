.class public abstract Lcom/google/android/searchcommon/summons/AbstractSource;
.super Ljava/lang/Object;
.source "AbstractSource.java"

# interfaces
.implements Lcom/google/android/searchcommon/summons/Source;


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private mEnabledByDefault:Ljava/lang/Boolean;

.field private mHasFullSizeIcon:Ljava/lang/Boolean;

.field private mShowSingleLine:Ljava/lang/Boolean;

.field private mSourceIcon:Landroid/graphics/drawable/Drawable$ConstantState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mSourceIcon:Landroid/graphics/drawable/Drawable$ConstantState;

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-void
.end method

.method private loadSourceIcon()Landroid/graphics/drawable/Drawable;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getSourceIconResource()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method protected abstract getApplicationInfo()Landroid/content/pm/ApplicationInfo;
.end method

.method public getIconUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 7
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v4, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getIconPackage()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/searchcommon/util/Util;->getResourceUri(Landroid/content/Context;I)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "Search.BaseSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not get package context for source "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {p1}, Lcom/google/android/searchcommon/util/Util;->parseUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-static {p1}, Lcom/google/android/searchcommon/util/Util;->parseUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_0
.end method

.method public getSourceIcon()Landroid/graphics/drawable/Drawable;
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mSourceIcon:Landroid/graphics/drawable/Drawable$ConstantState;

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->loadSourceIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02015f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mSourceIcon:Landroid/graphics/drawable/Drawable$ConstantState;

    :goto_1
    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mSourceIcon:Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method protected abstract getSourceIconResource()I
.end method

.method public getSourceIconUri()Landroid/net/Uri;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getSourceIconResource()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mContext:Landroid/content/Context;

    const v2, 0x7f02015f

    invoke-static {v1, v2}, Lcom/google/android/searchcommon/util/Util;->getResourceUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/searchcommon/util/Util;->getResourceUri(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;I)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method protected abstract getSourceId()Ljava/lang/String;
.end method

.method protected getTextFromResource(I)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public hasFullSizeIcon()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mHasFullSizeIcon:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getSourceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->isFullSizeIconSource(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mHasFullSizeIcon:Ljava/lang/Boolean;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mHasFullSizeIcon:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isEnabledByDefault()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mEnabledByDefault:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getSourceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/SearchConfig;->isSourceEnabledByDefault(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mEnabledByDefault:Ljava/lang/Boolean;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mEnabledByDefault:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isShowSingleLine()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mShowSingleLine:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/AbstractSource;->getSourceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->isSourceShowSingleLine(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mShowSingleLine:Ljava/lang/Boolean;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/summons/AbstractSource;->mShowSingleLine:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
