.class public Lcom/google/android/searchcommon/summons/SourceNamedTask;
.super Ljava/lang/Object;
.source "SourceNamedTask.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/NamedTask;


# instance fields
.field protected final mConsumer:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;"
        }
    .end annotation
.end field

.field protected final mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field protected final mQuery:Lcom/google/android/velvet/Query;

.field protected final mQueryLimit:I

.field private final mSignal:Landroid/os/CancellationSignal;

.field protected final mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/summons/ContentProviderSource;ILcom/google/android/searchcommon/util/Consumer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .param p3    # I
    .param p5    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            "I",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mQuery:Lcom/google/android/velvet/Query;

    iput p3, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mQueryLimit:I

    iput-object p4, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mConsumer:Lcom/google/android/searchcommon/util/Consumer;

    iput-object p5, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mSignal:Landroid/os/CancellationSignal;

    return-void
.end method


# virtual methods
.method public cancelExecution()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    :cond_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuggestions()Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 4

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mSource:Lcom/google/android/searchcommon/summons/ContentProviderSource;

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mQuery:Lcom/google/android/velvet/Query;

    iget v2, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mQueryLimit:I

    iget-object v3, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mSignal:Landroid/os/CancellationSignal;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getSuggestions(Lcom/google/android/velvet/Query;ILandroid/os/CancellationSignal;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/SourceNamedTask;->getSuggestions()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SourceNamedTask;->mConsumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-static {v1, v2, v0}, Lcom/google/android/searchcommon/util/Consumers;->consumeAsync(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)V

    return-void
.end method
