.class Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$3;
.super Ljava/lang/Object;
.source "TopSourceSummonsPromoter.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->updateSourceScores()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$3;->this$0:Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$3;->consume(Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public consume(Ljava/util/Map;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$3;->this$0:Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$3;->this$0:Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    # getter for: Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mOrderedSourceStates:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->access$200(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;

    iget-object v2, v1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mScore:I

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    const/4 v2, 0x0

    :try_start_1
    iput v2, v1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mScore:I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$3;->this$0:Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    # getter for: Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mOrderedSourceStates:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->access$200(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x1

    return v2
.end method
