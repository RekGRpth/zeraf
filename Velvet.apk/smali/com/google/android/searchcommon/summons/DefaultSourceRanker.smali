.class public Lcom/google/android/searchcommon/summons/DefaultSourceRanker;
.super Ljava/lang/Object;
.source "DefaultSourceRanker.java"

# interfaces
.implements Lcom/google/android/searchcommon/summons/SourceRanker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;,
        Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;,
        Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourcesObserver;
    }
.end annotation


# instance fields
.field private final mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

.field private final mRankedSources:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;"
        }
    .end annotation
.end field

.field private final mRankedSourcesWithIcing:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache",
            "<",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mSources:Lcom/google/android/searchcommon/summons/Sources;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/summons/Sources;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/clicklog/ClickLog;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/summons/Sources;
    .param p2    # Lcom/google/android/searchcommon/SearchSettings;
    .param p3    # Lcom/google/android/searchcommon/clicklog/ClickLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    new-instance v1, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourcesObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourcesObserver;-><init>(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;Lcom/google/android/searchcommon/summons/DefaultSourceRanker$1;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/summons/Sources;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p3, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    new-instance v0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$1;-><init>(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mRankedSources:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    new-instance v0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$2;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$2;-><init>(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mRankedSourcesWithIcing:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)Lcom/google/android/searchcommon/summons/Sources;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/summons/DefaultSourceRanker;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)Lcom/google/android/searchcommon/SearchSettings;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/summons/DefaultSourceRanker;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)Lcom/google/android/searchcommon/clicklog/ClickLog;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/summons/DefaultSourceRanker;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mRankedSources:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->clear()V

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mRankedSourcesWithIcing:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->clear()V

    return-void
.end method

.method public getSourcesForQuerying(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mRankedSources:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public getSourcesForUi(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mRankedSourcesWithIcing:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method
