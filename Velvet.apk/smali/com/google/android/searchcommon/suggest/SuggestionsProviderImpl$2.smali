.class Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;
.super Lcom/google/android/searchcommon/util/NonCancellableNamedTask;
.source "SuggestionsProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->createIcingQueryTask(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/summons/IcingSources;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/NamedTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

.field final synthetic val$consumer:Lcom/google/android/searchcommon/util/Consumer;

.field final synthetic val$icingSources:Lcom/google/android/searchcommon/summons/IcingSources;

.field final synthetic val$query:Lcom/google/android/velvet/Query;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;Lcom/google/android/searchcommon/summons/IcingSources;Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;->val$icingSources:Lcom/google/android/searchcommon/summons/IcingSources;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;->val$query:Lcom/google/android/velvet/Query;

    iput-object p4, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/NonCancellableNamedTask;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "IcingSource"

    return-object v0
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;->val$icingSources:Lcom/google/android/searchcommon/summons/IcingSources;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;->val$query:Lcom/google/android/velvet/Query;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v2}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-static {v2, v3}, Lcom/google/android/searchcommon/util/Consumers;->createAsyncConsumer(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/summons/IcingSources;->getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method
