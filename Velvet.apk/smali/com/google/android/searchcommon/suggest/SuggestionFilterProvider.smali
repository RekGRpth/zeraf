.class public Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;
.super Ljava/lang/Object;
.source "SuggestionFilterProvider.java"


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-void
.end method


# virtual methods
.method public getFilter(Lcom/google/android/searchcommon/summons/ContentProviderSource;Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionFilter;
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .param p2    # Ljava/lang/String;

    if-eqz p1, :cond_1

    const-string v1, "content://browser/bookmarks/search_suggest_query"

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getSuggestUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getMaxResultsPerSource()I

    move-result v0

    new-instance v1, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;I)V

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->isContactsSource()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/searchcommon/summons/ContactsSourceFilter;->INSTANCE:Lcom/google/android/searchcommon/summons/ContactsSourceFilter;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
