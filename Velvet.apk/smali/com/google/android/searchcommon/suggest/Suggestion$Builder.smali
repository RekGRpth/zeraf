.class public Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
.super Ljava/lang/Object;
.source "Suggestion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/Suggestion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mExtras:Lcom/google/android/searchcommon/suggest/SuggestionExtras;

.field private mIcon1:Ljava/lang/String;

.field private mIcon2:Ljava/lang/String;

.field private mIconLarge:Ljava/lang/String;

.field private mIntentAction:Ljava/lang/String;

.field private mIntentComponent:Landroid/content/ComponentName;

.field private mIntentData:Ljava/lang/String;

.field private mIntentExtraData:Ljava/lang/String;

.field private mInternalIntent:Landroid/content/Intent;

.field private mIsCorrectionSuggestion:Z

.field private mIsHistory:Z

.field private mIsWordByWordSuggestion:Z

.field private mLastAccessTime:J

.field private mLogType:Ljava/lang/String;

.field private mPrefetch:Z

.field private mSource:Lcom/google/android/searchcommon/summons/Source;

.field private mSpinnerWhileRefreshing:Z

.field private mSuggestionQuery:Ljava/lang/String;

.field private mText1:Ljava/lang/CharSequence;

.field private mText2:Ljava/lang/CharSequence;

.field private mText2Url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 24

    new-instance v1, Lcom/google/android/searchcommon/suggest/Suggestion;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSource:Lcom/google/android/searchcommon/summons/Source;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText1:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText2:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText2Url:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIcon1:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIcon2:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIconLarge:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mLastAccessTime:J

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSpinnerWhileRefreshing:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentAction:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentData:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentExtraData:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentComponent:Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mInternalIntent:Landroid/content/Intent;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSuggestionQuery:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mLogType:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsHistory:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mExtras:Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mPrefetch:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsWordByWordSuggestion:Z

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsCorrectionSuggestion:Z

    move/from16 v23, v0

    invoke-direct/range {v1 .. v23}, Lcom/google/android/searchcommon/suggest/Suggestion;-><init>(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/searchcommon/suggest/SuggestionExtras;ZZZ)V

    return-object v1
.end method

.method public extras(Lcom/google/android/searchcommon/suggest/SuggestionExtras;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mExtras:Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    return-object p0
.end method

.method public fromSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionSource()Lcom/google/android/searchcommon/summons/Source;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSource:Lcom/google/android/searchcommon/summons/Source;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText1:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText2:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2Url()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText2Url:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIcon1:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIcon2:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIconLarge()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIconLarge:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isSpinnerWhileRefreshing()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSpinnerWhileRefreshing:Z

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentAction:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentData:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentExtraData()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentExtraData:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentComponent()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentComponent:Landroid/content/ComponentName;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getInternalIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mInternalIntent:Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSuggestionQuery:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mLogType:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isHistorySuggestion()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsHistory:Z

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getExtras()Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mExtras:Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->shouldPrefetch()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mPrefetch:Z

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWordByWordSuggestion()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsWordByWordSuggestion:Z

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isCorrectionSuggestion()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsCorrectionSuggestion:Z

    return-object p0
.end method

.method public icon1(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIcon1:Ljava/lang/String;

    return-object p0
.end method

.method public icon2(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIcon2:Ljava/lang/String;

    return-object p0
.end method

.method public iconLarge(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIconLarge:Ljava/lang/String;

    return-object p0
.end method

.method public intentAction(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentAction:Ljava/lang/String;

    return-object p0
.end method

.method public intentComponent(Landroid/content/ComponentName;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Landroid/content/ComponentName;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentComponent:Landroid/content/ComponentName;

    return-object p0
.end method

.method public intentData(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentData:Ljava/lang/String;

    return-object p0
.end method

.method public intentExtraData(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIntentExtraData:Ljava/lang/String;

    return-object p0
.end method

.method public isCorrectionSuggestion(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsCorrectionSuggestion:Z

    return-object p0
.end method

.method public isHistory(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsHistory:Z

    return-object p0
.end method

.method public isWordByWord(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mIsWordByWordSuggestion:Z

    return-object p0
.end method

.method public lastAccessTime(J)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mLastAccessTime:J

    return-object p0
.end method

.method public logType(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mLogType:Ljava/lang/String;

    return-object p0
.end method

.method public shouldPrefetch(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mPrefetch:Z

    return-object p0
.end method

.method public source(Lcom/google/android/searchcommon/summons/Source;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/summons/Source;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSource:Lcom/google/android/searchcommon/summons/Source;

    return-object p0
.end method

.method public spinnerWhileRefreshing(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSpinnerWhileRefreshing:Z

    return-object p0
.end method

.method public suggestionQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mSuggestionQuery:Ljava/lang/String;

    return-object p0
.end method

.method public text1(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText1:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public text2(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText2:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public text2Url(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->mText2Url:Ljava/lang/String;

    return-object p0
.end method
