.class final Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;
.super Landroid/database/DataSetObserver;
.source "CachingPromoter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/CachingPromoter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "CacheEntry"
.end annotation


# instance fields
.field mPromoted:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field final mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/CachingPromoter;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 1
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestions;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->this$0:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    return-void
.end method

.method getPromoted(Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mPromoted:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->this$0:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    # invokes: Lcom/google/android/searchcommon/suggest/CachingPromoter;->buildPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {v0, v1, p1}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->access$000(Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/Suggestions;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mPromoted:Lcom/google/android/searchcommon/suggest/SuggestionList;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mPromoted:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-object v0
.end method

.method public onChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mPromoted:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mPromoted:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->isFinal()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mPromoted:Lcom/google/android/searchcommon/suggest/SuggestionList;

    :cond_0
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mPromoted:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
