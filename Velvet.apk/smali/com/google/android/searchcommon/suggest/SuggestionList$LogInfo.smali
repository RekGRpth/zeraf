.class public Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;
.super Ljava/lang/Object;
.source "SuggestionList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/SuggestionList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LogInfo"
.end annotation


# instance fields
.field private final mNumShownSuggestions:I

.field private final mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionList;I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionList;I)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->mNumShownSuggestions:I

    return-void
.end method


# virtual methods
.method public getAbsoluteClickPosition(Lcom/google/android/searchcommon/suggest/Suggestion;)I
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->indexOf(Lcom/google/android/searchcommon/suggest/Suggestion;)I

    move-result v0

    return v0
.end method

.method public getNumShownSuggestions(Lcom/google/android/searchcommon/suggest/Suggestion;)I
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->mNumShownSuggestions:I

    return v0
.end method

.method public getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-object v0
.end method
