.class Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$5;
.super Ljava/lang/Object;
.source "SuggestionsProviderImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->getIcingResultConsumer()Lcom/google/android/searchcommon/util/Consumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/searchcommon/suggest/SuggestionList;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$5;->this$1:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$5;->consume(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public consume(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$5;->this$1:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingResults:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->access$800(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$5;->this$1:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->handleNewResultAdded(Z)V
    invoke-static {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->access$700(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;Z)V

    const/4 v0, 0x1

    return v0
.end method
