.class public Lcom/google/android/searchcommon/suggest/CorrectionPromoter;
.super Lcom/google/android/searchcommon/suggest/AbstractPromoter;
.source "CorrectionPromoter.java"


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionFilter;
    .param p2    # Lcom/google/android/searchcommon/suggest/Promoter;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/suggest/AbstractPromoter;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V

    return-void
.end method


# virtual methods
.method protected doPickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p2    # I
    .param p3    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .param p4    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebResult()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isCorrectionSuggestion()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v2, v1}, Lcom/google/android/searchcommon/suggest/CorrectionPromoter;->accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p3, v1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->areWebResultsDone()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setFinal()V

    :cond_2
    return-void
.end method
