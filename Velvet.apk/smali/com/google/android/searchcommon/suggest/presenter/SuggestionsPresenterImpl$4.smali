.class Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$4;
.super Landroid/database/DataSetObserver;
.source "SuggestionsPresenterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;-><init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$4;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$4;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mStarted:Z
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$500(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$4;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsBuffered(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$4;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->setForceSuggestionFetch()V

    goto :goto_0
.end method
