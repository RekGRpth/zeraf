.class public interface abstract Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;
.super Ljava/lang/Object;
.source "SuggestionsPresenter.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# virtual methods
.method public abstract connectToIcing()V
.end method

.method public abstract disconnectFromIcing()V
.end method

.method public abstract initialize()V
.end method

.method public abstract removeFromHistoryClicked(Lcom/google/android/searchcommon/suggest/Suggestion;)V
.end method

.method public abstract setForceSuggestionFetch()V
.end method

.method public abstract start(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;)V
.end method

.method public abstract stop(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;)V
.end method

.method public abstract updateSuggestionsBuffered(Z)V
.end method

.method public abstract updateSuggestionsNow()V
.end method
