.class Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;
.super Ljava/lang/Object;
.source "SuggestionsPresenterImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsInternal()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

.field final synthetic val$query:Lcom/google/android/velvet/Query;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;Lcom/google/android/velvet/Query;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;->val$query:Lcom/google/android/velvet/Query;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;->consume(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public consume(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;->val$query:Lcom/google/android/velvet/Query;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;
    invoke-static {v2}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$200(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getIcingSources()Lcom/google/android/searchcommon/summons/IcingSources;

    move-result-object v2

    # invokes: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsInternal(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/summons/IcingSources;)V
    invoke-static {v0, v1, p1, v2}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$700(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/summons/IcingSources;)V

    const/4 v0, 0x1

    return v0
.end method
