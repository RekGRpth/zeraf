.class public Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;
.super Ljava/lang/Object;
.source "SuggestionsPresenterImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;
    }
.end annotation


# instance fields
.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConnectToIcingTask:Lcom/google/android/searchcommon/util/CancellableRunnable;

.field private final mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field private mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

.field private volatile mForceSuggestionFetch:Z

.field private final mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

.field private final mIcingConnection:Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;

.field private mInitialized:Z

.field private mLastSuggestionFetch:J

.field private final mRemoveFromHistoryDoneTask:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;

.field private final mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

.field private mSources:Lcom/google/android/searchcommon/summons/Sources;

.field private final mSourcesObserver:Landroid/database/DataSetObserver;

.field private mStarted:Z

.field private mSuggestionFetchScheduled:Z

.field private mSuggestionRemovalFailureNotificationPending:Z

.field private volatile mSuggestionsBeingRemoved:I

.field private final mSuggestionsTimeoutTask:Ljava/lang/Runnable;

.field private mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

.field private mSummonsQueryStrategy:I

.field private final mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mUpdateSuggestionsTask:Ljava/lang/Runnable;

.field private mUserLocale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lcom/google/android/searchcommon/CoreSearchServices;
    .param p4    # Lcom/google/android/searchcommon/GlobalSearchServices;
    .param p5    # Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .param p6    # Lcom/google/android/searchcommon/util/Clock;
    .param p7    # Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$1;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUpdateSuggestionsTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$2;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$2;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsTimeoutTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$3;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$3;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mConnectToIcingTask:Lcom/google/android/searchcommon/util/CancellableRunnable;

    new-instance v0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mRemoveFromHistoryDoneTask:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSummonsQueryStrategy:I

    iput-object p4, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    iput-object p5, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iput-object p6, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p7, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mIcingConnection:Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;

    new-instance v0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$4;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$4;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSourcesObserver:Landroid/database/DataSetObserver;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$5;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$5;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V

    invoke-virtual {v0, v1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUserLocale:Ljava/util/Locale;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsInternal()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/suggest/Suggestions;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/GlobalSearchServices;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mIcingConnection:Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mStarted:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;Lcom/google/android/searchcommon/suggest/Suggestion;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->doRemoveFromHistory(Lcom/google/android/searchcommon/suggest/Suggestion;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/summons/IcingSources;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/util/List;
    .param p3    # Lcom/google/android/searchcommon/summons/IcingSources;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsInternal(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/summons/IcingSources;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;II)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->removeFromHistoryDone(II)V

    return-void
.end method

.method private cancelOngoingQuery()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->cancelSuggestionsTimeoutTask()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSuggestionsProvider()Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProvider;->cancelOngoingQuery()V

    :cond_0
    return-void
.end method

.method private cancelSuggestionsTimeoutTask()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsTimeoutTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private doRemoveFromHistory(Lcom/google/android/searchcommon/suggest/Suggestion;)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsBeingRemoved:I

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/searchcommon/google/WebSuggestSource;->removeFromHistory(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mRemoveFromHistoryDoneTask:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;

    invoke-virtual {v2, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->addSuggestionRemoved(Z)V

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mRemoveFromHistoryDoneTask:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getSourcesToQuery(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSourceRanker()Lcom/google/android/searchcommon/summons/SourceRanker;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-static {v1, p1}, Lcom/google/android/searchcommon/util/Consumers;->createAsyncConsumer(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/summons/SourceRanker;->getSourcesForQuerying(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method private initSummons()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSourcesObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/summons/Sources;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    return-void
.end method

.method private removeFromHistoryDone(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    add-int v0, p1, p2

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsBeingRemoved:I

    add-int v3, p1, p2

    if-lt v0, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsBeingRemoved:I

    add-int v2, p1, p2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsBeingRemoved:I

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsBeingRemoved:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->setWebSuggestionsEnabled(Z)V

    :cond_1
    if-lez p2, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->indicateRemoveFromHistoryFailed()V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsNow()V

    :goto_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    if-lez p2, :cond_2

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionRemovalFailureNotificationPending:Z

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->setForceSuggestionFetch()V

    goto :goto_2
.end method

.method private shouldQueryStrategyHasChanged()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->getSummonsQueryStrategy()I

    move-result v0

    iget v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSummonsQueryStrategy:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->close()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->cancelSuggestionsTimeoutTask()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->startSuggestionsTimeoutTask()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->showSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    :cond_1
    return-void
.end method

.method private startRemoveFromHistory(Lcom/google/android/searchcommon/suggest/Suggestion;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWebSearchSuggestion()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->setWebSuggestionsEnabled(Z)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->cancelOngoingQuery()V

    iget v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsBeingRemoved:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsBeingRemoved:I

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$6;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$6;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;Lcom/google/android/searchcommon/suggest/Suggestion;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "Search.SuggestionsPresenterImpl"

    const-string v1, "Attempt to remove non-web suggestion?. Just refresh"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->setForceSuggestionFetch()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsNow()V

    goto :goto_0
.end method

.method private startSuggestionsTimeoutTask()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsTimeoutTask:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getSourceTimeoutMillis()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private updateSuggestions(J)V
    .locals 3
    .param p1    # J

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUpdateSuggestionsTask:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mInitialized:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-interface {v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mForceSuggestionFetch:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/velvet/Query;->equivalentForSuggest(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->cancelOngoingQuery()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionFetchScheduled:Z

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUpdateSuggestionsTask:Ljava/lang/Runnable;

    invoke-interface {v1, v2, p1, p2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method private updateSuggestionsInternal()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mLastSuggestionFetch:J

    iput-boolean v3, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mForceSuggestionFetch:Z

    iput-boolean v3, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionFetchScheduled:Z

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-interface {v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->getSummonsQueryStrategy()I

    move-result v1

    iput v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSummonsQueryStrategy:I

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->shouldQuerySummons(Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/searchcommon/suggest/Suggestions;->NOT_QUERYING_SUMMONS:Ljava/util/List;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsInternal(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/summons/IcingSources;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;

    invoke-direct {v1, p0, v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$7;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;Lcom/google/android/velvet/Query;)V

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->getSourcesToQuery(Lcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_0
.end method

.method private updateSuggestionsInternal(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/summons/IcingSources;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .param p3    # Lcom/google/android/searchcommon/summons/IcingSources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;",
            "Lcom/google/android/searchcommon/summons/IcingSources;",
            ")V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    invoke-virtual {v2, p1}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logSnappyRequest(Lcom/google/android/velvet/Query;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSuggestionsProvider()Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

    move-result-object v2

    invoke-interface {v2, p1, p2, v1, p3}, Lcom/google/android/searchcommon/suggest/SuggestionsProvider;->getSuggestions(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/google/GoogleSource;Lcom/google/android/searchcommon/summons/IcingSources;)Lcom/google/android/searchcommon/suggest/Suggestions;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->showSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    return-void
.end method


# virtual methods
.method public connectToIcing()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mConnectToIcingTask:Lcom/google/android/searchcommon/util/CancellableRunnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public disconnectFromIcing()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mConnectToIcingTask:Lcom/google/android/searchcommon/util/CancellableRunnable;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/CancellableRunnable;->cancel()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mIcingConnection:Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;->disconnect()V

    return-void
.end method

.method public initialize()V
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mInitialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->initSummons()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mInitialized:Z

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mStarted:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsNow()V

    :cond_0
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const-string v0, "google_account"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "use_google_com"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->setForceSuggestionFetch()V

    :cond_1
    return-void
.end method

.method public removeFromHistoryClicked(Lcom/google/android/searchcommon/suggest/Suggestion;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mInitialized:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->startRemoveFromHistory(Lcom/google/android/searchcommon/suggest/Suggestion;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setForceSuggestionFetch()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mForceSuggestionFetch:Z

    return-void
.end method

.method public start(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;)V
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logSuggestSessionStart()V

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-ne v2, p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->shouldQueryStrategyHasChanged()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->setForceSuggestionFetch()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsNow()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestions;->NONE:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->showSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mStarted:Z

    if-nez v2, :cond_3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mStarted:Z

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mInitialized:Z

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->initSummons()V

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mInitialized:Z

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUserLocale:Ljava/util/Locale;

    invoke-virtual {v2, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUserLocale:Ljava/util/Locale;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/DataSetObservable;->notifyChanged()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->setForceSuggestionFetch()V

    :cond_4
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mForceSuggestionFetch:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->isDone()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/velvet/Query;->equivalentForSuggest(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->shouldQueryStrategyHasChanged()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getOpenedCopy()Lcom/google/android/searchcommon/suggest/Suggestions;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    :cond_5
    iget v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsBeingRemoved:I

    if-lez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-interface {v2, v4}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->setWebSuggestionsEnabled(Z)V

    :cond_6
    :goto_1
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->showSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    goto/16 :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionRemovalFailureNotificationPending:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->indicateRemoveFromHistoryFailed()V

    iput-boolean v4, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionRemovalFailureNotificationPending:Z

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->setForceSuggestionFetch()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestionsNow()V

    goto/16 :goto_0
.end method

.method public stop(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUpdateSuggestionsTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->cancelSuggestionsTimeoutTask()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSuggestionsProvider()Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProvider;->cancelOngoingQuery()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->close()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    sget-object v1, Lcom/google/android/searchcommon/suggest/Suggestions;->NONE:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;->showSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionsUi:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mStarted:Z

    :cond_1
    return-void
.end method

.method public updateSuggestionsBuffered(Z)V
    .locals 6
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logQueryEdit()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logSuggestRequest()V

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSuggestionFetchScheduled:Z

    if-eqz v2, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getTypingUpdateSuggestionsDelayMillis()I

    move-result v2

    int-to-long v0, v2

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mLastSuggestionFetch:J

    sub-long/2addr v2, v4

    sub-long v2, v0, v2

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestions(J)V

    goto :goto_0
.end method

.method public updateSuggestionsNow()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logSuggestRequest()V

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->updateSuggestions(J)V

    return-void
.end method
