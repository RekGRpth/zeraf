.class Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;
.super Ljava/lang/Object;
.source "GsaPreferenceUpgrader.java"

# interfaces
.implements Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->createSettingsEditorSelector(Ljava/util/Map;)Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

.field final synthetic val$searchSettings:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->this$0:Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    iput-object p2, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->val$searchSettings:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public selectEditor(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v1, 0x0

    const-string v4, "google_account"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->this$0:Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    # invokes: Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    invoke-static {v3}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->access$000(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->this$0:Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    # invokes: Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getStartupEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    invoke-static {v3}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->access$100(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    const-string v4, "web_corpora_json"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "web_corpora_json_url"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->this$0:Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    # invokes: Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    invoke-static {v4}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->access$000(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0

    :cond_3
    const-string v4, "lastloc"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "session_key"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "web_corpora_config"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "gstatic_configuration_data"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "gstatic_configuration_override_1"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "configuration_bytes_key_"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    iget-object v4, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->val$searchSettings:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    instance-of v4, v2, Ljava/lang/String;

    if-eqz v4, :cond_0

    :try_start_0
    check-cast v2, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->this$0:Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    # invokes: Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    invoke-static {v4}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->access$000(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v4

    invoke-interface {v4, p1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_0

    :cond_5
    invoke-static {p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->isStartupSetting(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "enableTestPlatformLogging"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    const/4 v1, 0x1

    :cond_7
    if-eqz v1, :cond_8

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->this$0:Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    # invokes: Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getStartupEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    invoke-static {v3}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->access$100(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    goto/16 :goto_0

    :cond_8
    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;->this$0:Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    # invokes: Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    invoke-static {v3}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->access$000(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    goto/16 :goto_0
.end method
