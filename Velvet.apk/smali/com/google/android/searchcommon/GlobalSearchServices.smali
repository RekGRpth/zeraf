.class public interface abstract Lcom/google/android/searchcommon/GlobalSearchServices;
.super Ljava/lang/Object;
.source "GlobalSearchServices.java"


# virtual methods
.method public abstract getClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;
.end method

.method public abstract getGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;
.end method

.method public abstract getIcingSources()Lcom/google/android/searchcommon/summons/IcingSources;
.end method

.method public abstract getIconLoader()Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
.end method

.method public abstract getIconLoader(Ljava/lang/String;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
.end method

.method public abstract getSearchHistoryHelper()Lcom/google/android/searchcommon/history/SearchHistoryHelper;
.end method

.method public abstract getShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;
.end method

.method public abstract getSourceRanker()Lcom/google/android/searchcommon/summons/SourceRanker;
.end method

.method public abstract getSources()Lcom/google/android/searchcommon/summons/Sources;
.end method

.method public abstract getSuggestionFormatter()Lcom/google/android/searchcommon/suggest/SuggestionFormatter;
.end method

.method public abstract getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;
.end method

.method public abstract getSuggestionsProvider()Lcom/google/android/searchcommon/suggest/SuggestionsProvider;
.end method

.method public abstract getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;
.end method

.method public abstract onTrimMemory(I)V
.end method

.method public abstract updateSearchableSources()V
.end method
