.class public interface abstract Lcom/google/android/searchcommon/AsyncServices;
.super Ljava/lang/Object;
.source "AsyncServices.java"


# virtual methods
.method public abstract getExclusiveBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;
.end method

.method public abstract getExclusiveNamedExecutor(Ljava/lang/String;)Ljava/util/concurrent/Executor;
.end method

.method public abstract getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;
.end method

.method public abstract getPooledBackgroundExecutorService()Ljava/util/concurrent/ExecutorService;
.end method

.method public abstract getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;
.end method

.method public abstract getScheduledBackgroundExecutorService()Ljava/util/concurrent/ScheduledExecutorService;
.end method

.method public abstract getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
.end method
