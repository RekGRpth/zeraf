.class public Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;
.super Lcom/google/android/speech/network/request/BaseRequestBuilderTask;
.source "MajelClientInfoBuilderTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/speech/network/request/BaseRequestBuilderTask",
        "<",
        "Lcom/google/speech/speech/s3/Majel$MajelClientInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

.field private final mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/speech/params/DeviceParams;Lcom/google/common/base/Supplier;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/params/DeviceParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/params/DeviceParams;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "MajelClientInfoBuilderTask"

    invoke-direct {p0, v0}, Lcom/google/android/speech/network/request/BaseRequestBuilderTask;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    iput-object p2, p0, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method private static getTimeZone()Ljava/lang/String;
    .locals 1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected build()Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 4

    new-instance v1, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    invoke-interface {v2}, Lcom/google/android/speech/params/DeviceParams;->getPreviewParams()Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setPreviewParams(Lcom/google/majel/proto/ClientInfoProtos$PreviewParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    invoke-interface {v2}, Lcom/google/android/speech/params/DeviceParams;->getScreenParams()Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setScreenParams(Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    invoke-interface {v2}, Lcom/google/android/speech/params/DeviceParams;->getBrowserParams()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setBrowserParams(Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    invoke-static {}, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->getTimeZone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setTimeZone(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setSystemTimeMs(J)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    invoke-interface {v2}, Lcom/google/android/speech/params/DeviceParams;->getDeviceCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->setGservicesCountryCode(Ljava/lang/String;)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;->addCapabilities(I)Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    :cond_0
    return-object v0
.end method

.method protected bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/network/request/MajelClientInfoBuilderTask;->build()Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v0

    return-object v0
.end method
