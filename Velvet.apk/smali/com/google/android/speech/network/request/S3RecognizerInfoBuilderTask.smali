.class public Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;
.super Lcom/google/android/speech/network/request/BaseRequestBuilderTask;
.source "S3RecognizerInfoBuilderTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/speech/network/request/BaseRequestBuilderTask",
        "<",
        "Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final PROFANITY_FILTER_DISABLED:I = 0x0

.field static final PROFANITY_FILTER_ENABLED:I = 0x2


# instance fields
.field private final mMaxNbest:I

.field private final mNeedsAlternates:Z

.field private final mNeedsCombinedNbest:Z

.field private final mNeedsPartialResults:Z

.field private final mRecognitionContext:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

.field private final mSpeechSettings:Lcom/google/android/speech/SpeechSettings;


# direct methods
.method public constructor <init>(Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;Lcom/google/android/speech/SpeechSettings;ZZZI)V
    .locals 1
    .param p1    # Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;
    .param p2    # Lcom/google/android/speech/SpeechSettings;
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .param p6    # I

    const-string v0, "S3RecognizerInfoBuilderTask"

    invoke-direct {p0, v0}, Lcom/google/android/speech/network/request/BaseRequestBuilderTask;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mRecognitionContext:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    iput-object p2, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    iput-boolean p3, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mNeedsPartialResults:Z

    iput-boolean p4, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mNeedsCombinedNbest:Z

    iput-boolean p5, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mNeedsAlternates:Z

    iput p6, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mMaxNbest:I

    return-void
.end method

.method static getAlternateParams(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Lcom/google/speech/common/Alternates$AlternateParams;
    .locals 2
    .param p0    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    new-instance v0, Lcom/google/speech/common/Alternates$AlternateParams;

    invoke-direct {v0}, Lcom/google/speech/common/Alternates$AlternateParams;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getMaxSpanLength()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/speech/common/Alternates$AlternateParams;->setMaxSpanLength(I)Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getMaxTotalSpanLength()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/speech/common/Alternates$AlternateParams;->setMaxTotalSpanLength(I)Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/speech/common/Alternates$AlternateParams;->setUnit(I)Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected build()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 2

    new-instance v0, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    invoke-direct {v0}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mRecognitionContext:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mRecognitionContext:Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setRecognitionContext(Lcom/google/speech/common/proto/RecognitionContextProto$RecognitionContext;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mNeedsPartialResults:Z

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnablePartialResults(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    iget-boolean v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mNeedsCombinedNbest:Z

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnableCombinedNbest(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    iget-boolean v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mNeedsCombinedNbest:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mMaxNbest:I

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setMaxNbest(I)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mNeedsAlternates:Z

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnableAlternates(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    iget-boolean v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mNeedsAlternates:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->getAlternateParams(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setAlternateParams(Lcom/google/speech/common/Alternates$AlternateParams;)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    :cond_2
    iget-object v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->isProfanityFilterEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setProfanityFilter(I)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    :goto_0
    iget-object v1, p0, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->isPersonalizationEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setEnablePersonalization(Z)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    return-object v0

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;->setProfanityFilter(I)Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    goto :goto_0
.end method

.method protected bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/network/request/S3RecognizerInfoBuilderTask;->build()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    move-result-object v0

    return-object v0
.end method
