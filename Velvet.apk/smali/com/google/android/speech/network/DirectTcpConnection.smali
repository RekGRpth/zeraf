.class public Lcom/google/android/speech/network/DirectTcpConnection;
.super Ljava/lang/Object;
.source "DirectTcpConnection.java"

# interfaces
.implements Lcom/google/android/speech/network/S3Connection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;
    }
.end annotation


# instance fields
.field protected volatile mCallback:Lcom/google/android/speech/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation
.end field

.field private final mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

.field private mInput:Lcom/google/android/speech/message/S3ResponseStream;

.field private mOutput:Lcom/google/android/speech/message/S3RequestStream;

.field private mReaderThread:Ljava/lang/Thread;

.field private final mReadingRunnable:Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;

.field private final mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

.field protected mSocket:Ljava/net/Socket;


# direct methods
.method public constructor <init>(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;Lcom/google/android/speech/network/ConnectionFactory;)V
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;
    .param p2    # Lcom/google/android/speech/network/ConnectionFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mReaderThread:Ljava/lang/Thread;

    iput-object p1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    new-instance v0, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;

    invoke-direct {v0, p0}, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;-><init>(Lcom/google/android/speech/network/DirectTcpConnection;)V

    iput-object v0, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mReadingRunnable:Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;

    iput-object p2, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/speech/network/DirectTcpConnection;)Lcom/google/android/speech/message/S3ResponseStream;
    .locals 1
    .param p0    # Lcom/google/android/speech/network/DirectTcpConnection;

    iget-object v0, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mInput:Lcom/google/android/speech/message/S3ResponseStream;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mReadingRunnable:Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;

    invoke-virtual {v0}, Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;->cancel()V

    iget-object v0, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mSocket:Ljava/net/Socket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mSocket:Ljava/net/Socket;

    invoke-static {v0}, Lcom/google/android/speech/network/IoUtils;->untagSocket(Ljava/net/Socket;)V

    iget-object v0, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mSocket:Ljava/net/Socket;

    invoke-static {v0}, Lcom/google/android/speech/network/IoUtils;->closeQuietly(Ljava/net/Socket;)V

    :cond_0
    return-void
.end method

.method public connect(Lcom/google/android/speech/callback/Callback;Lcom/google/speech/s3/S3$S3Request;)V
    .locals 5
    .param p2    # Lcom/google/speech/s3/S3$S3Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/speech/s3/S3$S3Request;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x1

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/callback/Callback;

    iput-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mCallback:Lcom/google/android/speech/callback/Callback;

    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mSocket:Ljava/net/Socket;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    invoke-interface {v1, v2}, Lcom/google/android/speech/network/ConnectionFactory;->openSocket(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;)Ljava/net/Socket;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mSocket:Ljava/net/Socket;

    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mSocket:Ljava/net/Socket;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    new-instance v1, Lcom/google/android/speech/message/S3RequestStream;

    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mServerInfo:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;

    invoke-virtual {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServerInfo;->getHeader()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/speech/message/S3RequestStream;-><init>(Ljava/io/OutputStream;Ljava/lang/String;Z)V

    iput-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mOutput:Lcom/google/android/speech/message/S3RequestStream;

    new-instance v1, Lcom/google/android/speech/message/S3ResponseStream;

    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/speech/message/S3ResponseStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mInput:Lcom/google/android/speech/message/S3ResponseStream;

    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mOutput:Lcom/google/android/speech/message/S3RequestStream;

    invoke-virtual {v1, p2}, Lcom/google/android/speech/message/S3RequestStream;->writeHeader(Lcom/google/speech/s3/S3$S3Request;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/speech/network/DirectTcpConnection;->close()V

    throw v0
.end method

.method public send(Lcom/google/speech/s3/S3$S3Request;)V
    .locals 4
    .param p1    # Lcom/google/speech/s3/S3$S3Request;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mOutput:Lcom/google/android/speech/message/S3RequestStream;

    invoke-virtual {v1, p1}, Lcom/google/android/speech/message/S3RequestStream;->write(Lcom/google/speech/s3/S3$S3Request;)V

    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mOutput:Lcom/google/android/speech/message/S3RequestStream;

    invoke-virtual {v1}, Lcom/google/android/speech/message/S3RequestStream;->flush()V

    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mReaderThread:Ljava/lang/Thread;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mReadingRunnable:Lcom/google/android/speech/network/DirectTcpConnection$ReaderRunnable;

    const-string v3, "DirectTcpReader"

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mReaderThread:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mReaderThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/speech/network/DirectTcpConnection;->mCallback:Lcom/google/android/speech/callback/Callback;

    new-instance v2, Lcom/google/android/speech/exception/NetworkRecognizeException;

    const-string v3, "Failed to send request"

    invoke-direct {v2, v3, v0}, Lcom/google/android/speech/exception/NetworkRecognizeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Lcom/google/android/speech/callback/Callback;->onError(Ljava/lang/Object;)V

    goto :goto_0
.end method
