.class Lcom/google/android/speech/audio/Tee$TeeLeaderInputStream;
.super Ljava/io/InputStream;
.source "Tee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/audio/Tee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TeeLeaderInputStream"
.end annotation


# instance fields
.field private final mSharedStream:Lcom/google/android/speech/audio/Tee;


# direct methods
.method constructor <init>(Lcom/google/android/speech/audio/Tee;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/audio/Tee;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/audio/Tee$TeeLeaderInputStream;->mSharedStream:Lcom/google/android/speech/audio/Tee;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/audio/Tee$TeeLeaderInputStream;->mSharedStream:Lcom/google/android/speech/audio/Tee;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/speech/audio/Tee;->remove(I)V

    iget-object v0, p0, Lcom/google/android/speech/audio/Tee$TeeLeaderInputStream;->mSharedStream:Lcom/google/android/speech/audio/Tee;

    invoke-virtual {v0}, Lcom/google/android/speech/audio/Tee;->close()V

    return-void
.end method

.method public read()I
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Find some other app to be inefficient in."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([BII)I
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/speech/audio/Tee$TeeLeaderInputStream;->mSharedStream:Lcom/google/android/speech/audio/Tee;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/speech/audio/Tee;->readLeader([BII)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method
