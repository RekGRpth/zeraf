.class public Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;
.super Ljava/lang/Object;
.source "MicrophoneInputStreamFactory.java"

# interfaces
.implements Lcom/google/android/speech/audio/AudioInputStreamFactory;


# instance fields
.field private final mNoiseSuppression:Z

.field private final mSampleRateHz:I

.field private final mStartEventSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Landroid/media/MediaSyncEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IZLcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Lcom/google/common/base/Supplier",
            "<",
            "Landroid/media/MediaSyncEvent;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;->mSampleRateHz:I

    iput-boolean p2, p0, Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;->mNoiseSuppression:Z

    iput-object p3, p0, Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;->mStartEventSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method


# virtual methods
.method public createInputStream()Ljava/io/InputStream;
    .locals 5

    new-instance v0, Lcom/google/android/speech/audio/MicrophoneInputStream;

    iget v1, p0, Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;->mSampleRateHz:I

    const v2, 0x1f400

    iget-boolean v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;->mNoiseSuppression:Z

    iget-object v4, p0, Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;->mStartEventSupplier:Lcom/google/common/base/Supplier;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/speech/audio/MicrophoneInputStream;-><init>(IIZLcom/google/common/base/Supplier;)V

    return-object v0
.end method
