.class public Lcom/google/android/speech/audio/AudioController;
.super Ljava/lang/Object;
.source "AudioController.java"


# instance fields
.field private final mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;

.field private final mAudioRoutingSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioSource:Lcom/google/android/speech/audio/AudioSource;

.field private final mAudioStream:I

.field private mBluetoothRecording:Z

.field private final mContext:Landroid/content/Context;

.field private mNoiseSuppressors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRawInputStreamFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;

.field private final mSettings:Lcom/google/android/speech/SpeechSettings;

.field private final mSoundManager:Lcom/google/android/speech/audio/SpeechSoundManager;

.field private final mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

.field private mStarted:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/speech/SpeechSettings;Lcom/google/android/speech/SpeechLevelSource;Lcom/google/android/speech/audio/SpeechSoundManager;ILcom/google/common/base/Supplier;Lcom/google/android/voicesearch/AudioRecorderRouter;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/speech/SpeechSettings;
    .param p3    # Lcom/google/android/speech/SpeechLevelSource;
    .param p4    # Lcom/google/android/speech/audio/SpeechSoundManager;
    .param p5    # I
    .param p7    # Lcom/google/android/voicesearch/AudioRecorderRouter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/speech/SpeechSettings;",
            "Lcom/google/android/speech/SpeechLevelSource;",
            "Lcom/google/android/speech/audio/SpeechSoundManager;",
            "I",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/voicesearch/AudioRecorderRouter;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mRawInputStreamFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;

    new-instance v0, Lcom/google/android/speech/audio/AudioController$1;

    invoke-direct {v0, p0}, Lcom/google/android/speech/audio/AudioController$1;-><init>(Lcom/google/android/speech/audio/AudioController;)V

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    iput-object p1, p0, Lcom/google/android/speech/audio/AudioController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/speech/audio/AudioController;->mSettings:Lcom/google/android/speech/SpeechSettings;

    iput-object p4, p0, Lcom/google/android/speech/audio/AudioController;->mSoundManager:Lcom/google/android/speech/audio/SpeechSoundManager;

    iput-object p3, p0, Lcom/google/android/speech/audio/AudioController;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    iput-object p6, p0, Lcom/google/android/speech/audio/AudioController;->mAudioRoutingSupplier:Lcom/google/common/base/Supplier;

    iput p5, p0, Lcom/google/android/speech/audio/AudioController;->mAudioStream:I

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioManager:Landroid/media/AudioManager;

    iput-object p7, p0, Lcom/google/android/speech/audio/AudioController;->mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/speech/audio/AudioController;)Lcom/google/android/speech/audio/SpeechSoundManager;
    .locals 1
    .param p0    # Lcom/google/android/speech/audio/AudioController;

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mSoundManager:Lcom/google/android/speech/audio/SpeechSoundManager;

    return-object v0
.end method

.method private createAudioSource(Lcom/google/android/speech/audio/AudioInputStreamFactory;)Lcom/google/android/speech/audio/AudioSource;
    .locals 6
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;

    new-instance v0, Lcom/google/android/speech/audio/AudioSource;

    const/16 v1, 0x140

    const/16 v2, 0x1f4

    const/16 v3, 0x3e8

    iget-object v5, p0, Lcom/google/android/speech/audio/AudioController;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/speech/audio/AudioSource;-><init>(IIILcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/SpeechLevelSource;)V

    return-object v0
.end method

.method private createDefaultRawInputStreamFactoryLocked(Lcom/google/android/speech/params/AudioInputParams;)Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .locals 5
    .param p1    # Lcom/google/android/speech/params/AudioInputParams;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/speech/params/AudioInputParams;->isPlayBeepEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Lcom/google/android/speech/audio/AudioController$3;

    invoke-direct {v1, p0}, Lcom/google/android/speech/audio/AudioController$3;-><init>(Lcom/google/android/speech/audio/AudioController;)V

    :cond_0
    new-instance v0, Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;

    invoke-virtual {p1}, Lcom/google/android/speech/params/AudioInputParams;->getSamplingRate()I

    move-result v2

    invoke-direct {p0, p1}, Lcom/google/android/speech/audio/AudioController;->isNoiseSuppressionEnabled(Lcom/google/android/speech/params/AudioInputParams;)Z

    move-result v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/speech/audio/MicrophoneInputStreamFactory;-><init>(IZLcom/google/common/base/Supplier;)V

    new-instance v2, Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;

    iget-object v3, p0, Lcom/google/android/speech/audio/AudioController;->mSettings:Lcom/google/android/speech/SpeechSettings;

    iget-object v4, p0, Lcom/google/android/speech/audio/AudioController;->mContext:Landroid/content/Context;

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;-><init>(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/SpeechSettings;Landroid/content/Context;)V

    return-object v2
.end method

.method private getRawInputStreamFactoryLocked(Lcom/google/android/speech/params/AudioInputParams;)Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .locals 1
    .param p1    # Lcom/google/android/speech/params/AudioInputParams;

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mRawInputStreamFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mRawInputStreamFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/speech/audio/AudioController;->createDefaultRawInputStreamFactoryLocked(Lcom/google/android/speech/params/AudioInputParams;)Lcom/google/android/speech/audio/AudioInputStreamFactory;

    move-result-object v0

    goto :goto_0
.end method

.method private isNoiseSuppressionEnabled(Lcom/google/android/speech/params/AudioInputParams;)Z
    .locals 2
    .param p1    # Lcom/google/android/speech/params/AudioInputParams;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/speech/params/AudioInputParams;->isNoiseSuppressionEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mNoiseSuppressors:Ljava/util/List;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getPlatform()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/speech/audio/AudioUtils;->getNoiseSuppressors(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Platform;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mNoiseSuppressors:Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mNoiseSuppressors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized createInputStreamFactory(Lcom/google/android/speech/params/AudioInputParams;)Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .locals 2
    .param p1    # Lcom/google/android/speech/params/AudioInputParams;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/speech/audio/AudioController;->getRawInputStreamFactoryLocked(Lcom/google/android/speech/params/AudioInputParams;)Lcom/google/android/speech/audio/AudioInputStreamFactory;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/speech/audio/AudioController;->createAudioSource(Lcom/google/android/speech/audio/AudioInputStreamFactory;)Lcom/google/android/speech/audio/AudioSource;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized rewindInputStreamFactory(J)Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .locals 2
    .param p1    # J

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/speech/audio/AudioController;->mStarted:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    new-instance v0, Lcom/google/android/speech/audio/AudioSource;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    invoke-direct {v0, v1}, Lcom/google/android/speech/audio/AudioSource;-><init>(Lcom/google/android/speech/audio/AudioSource;)V

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/speech/audio/AudioSource;->setStartTime(J)V

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setBluetoothRecordingEnabled(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/speech/audio/AudioController;->mBluetoothRecording:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCannedAudio(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/speech/audio/AudioController$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/speech/audio/AudioController$2;-><init>(Lcom/google/android/speech/audio/AudioController;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/speech/audio/AudioController;->setRawInputStreamFactory(Lcom/google/android/speech/audio/AudioInputStreamFactory;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setRawInputStreamFactory(Lcom/google/android/speech/audio/AudioInputStreamFactory;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/speech/audio/AudioController;->mRawInputStreamFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized shutdown()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/speech/audio/AudioController;->stopListening()V

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    invoke-virtual {v0}, Lcom/google/android/speech/audio/AudioSource;->shutdown()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startListening(Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/speech/audio/AudioController;->mStarted:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v0}, Lcom/google/android/speech/SpeechLevelSource;->reset()V

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    iget v2, p0, Lcom/google/android/speech/audio/AudioController;->mAudioStream:I

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    const/16 v0, 0x4c

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mAudioRoutingSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/audio/AudioSource;->start(Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/speech/audio/AudioController;->mBluetoothRecording:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;

    invoke-interface {v0}, Lcom/google/android/voicesearch/AudioRecorderRouter;->start()Z

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/speech/audio/AudioController;->mStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopListening()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/speech/audio/AudioController;->mStarted:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioSource:Lcom/google/android/speech/audio/AudioSource;

    invoke-virtual {v0}, Lcom/google/android/speech/audio/AudioSource;->stopListening()V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/speech/audio/AudioController;->mBluetoothRecording:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/audio/AudioController;->mBluetoothRecording:Z

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioRecorderRouter:Lcom/google/android/voicesearch/AudioRecorderRouter;

    invoke-interface {v0}, Lcom/google/android/voicesearch/AudioRecorderRouter;->requestStop()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioController;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioController;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    invoke-virtual {v0}, Lcom/google/android/speech/SpeechLevelSource;->reset()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/audio/AudioController;->mStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
