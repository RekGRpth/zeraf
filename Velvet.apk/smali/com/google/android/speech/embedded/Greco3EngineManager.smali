.class public Lcom/google/android/speech/embedded/Greco3EngineManager;
.super Ljava/lang/Object;
.source "Greco3EngineManager.java"

# interfaces
.implements Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;
    }
.end annotation


# instance fields
.field private mCurrentRecognition:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/android/speech/embedded/Greco3Recognizer;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentRecognizer:Lcom/google/android/speech/embedded/Greco3Recognizer;

.field private final mEndpointerModelCopier:Lcom/google/android/speech/embedded/EndpointerModelCopier;

.field private final mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field private final mGreco3Preferences:Lcom/google/android/speech/embedded/Greco3Preferences;

.field private mInitialized:Z

.field private final mRecognitionExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mResourcesByMode:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/speech/embedded/Greco3Mode;",
            "Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/embedded/Greco3Preferences;Lcom/google/android/speech/embedded/EndpointerModelCopier;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Preferences;
    .param p3    # Lcom/google/android/speech/embedded/EndpointerModelCopier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iput-object p2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mGreco3Preferences:Lcom/google/android/speech/embedded/Greco3Preferences;

    iput-object p3, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mEndpointerModelCopier:Lcom/google/android/speech/embedded/EndpointerModelCopier;

    const-string v0, "Greco3Thread"

    invoke-static {v0}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->newSingleThreadExecutor(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mRecognitionExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mResourcesByMode:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;
    .locals 1
    .param p0    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-static {p0}, Lcom/google/android/speech/embedded/Greco3EngineManager;->buildLanguagePackLog(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/speech/embedded/Greco3EngineManager;Ljava/io/File;Z)V
    .locals 0
    .param p0    # Lcom/google/android/speech/embedded/Greco3EngineManager;
    .param p1    # Ljava/io/File;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/speech/embedded/Greco3EngineManager;->doResourceDelete(Ljava/io/File;Z)V

    return-void
.end method

.method private static buildLanguagePackLog(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;
    .locals 2
    .param p0    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    new-instance v0, Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    invoke-direct {v0}, Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;->setLocale(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;->setVersion(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$LanguagePackLog;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized doResourceDelete(Ljava/io/File;Z)V
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Z

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/speech/embedded/Greco3EngineManager;->isUsedLocked(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/speech/embedded/Greco3EngineManager;->releaseAllResourcesLocked()V

    :cond_0
    invoke-static {p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->deleteSingleLevelTree(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getCompiledGrammarPath(Lcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;
    .param p2    # Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mGreco3Preferences:Lcom/google/android/speech/embedded/Greco3Preferences;

    invoke-virtual {v1, p1}, Lcom/google/android/speech/embedded/Greco3Preferences;->getCompiledGrammarRevisionId(Lcom/google/android/speech/embedded/Greco3Grammar;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getGrammarPath(Lcom/google/android/speech/embedded/Greco3Grammar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private declared-synchronized getResourcesInternal(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Grammar;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_2

    :cond_0
    move v4, v2

    :goto_0
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v4, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognition:Ljava/util/concurrent/Future;

    if-nez v4, :cond_3

    :goto_1
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mResourcesByMode:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    if-eqz v0, :cond_5

    invoke-virtual {v0, p1, p3, p2}, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;->isEquivalentTo(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3Mode;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_4

    move-object v1, v0

    :cond_1
    :goto_2
    monitor-exit p0

    return-object v1

    :cond_2
    move v4, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    :try_start_1
    iget-object v2, v0, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;->resources:Lcom/google/android/speech/embedded/Greco3ResourceManager;

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/Greco3ResourceManager;->delete()V

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mResourcesByMode:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/speech/embedded/Greco3EngineManager;->loadResourcesFor(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-virtual {p2}, Lcom/google/android/speech/embedded/Greco3Mode;->isEndpointerMode()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "en-US"

    const/4 v3, 0x0

    invoke-direct {p0, v2, p2, v3}, Lcom/google/android/speech/embedded/Greco3EngineManager;->loadResourcesFor(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    move-result-object v0

    :cond_6
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mResourcesByMode:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private isUsedLocked(Ljava/io/File;)Z
    .locals 8
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mResourcesByMode:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    iget-object v0, v6, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;->paths:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v7, 0x1

    :goto_1
    return v7

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private loadResourcesFor(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;
    .locals 15
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Grammar;

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v14

    if-nez v14, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    move-object/from16 v0, p2

    invoke-interface {v14, v0}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getConfigFile(Lcom/google/android/speech/embedded/Greco3Mode;)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v14}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getResourcePaths()Ljava/util/List;

    move-result-object v11

    if-eqz v11, :cond_2

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const-string v1, "VS.G3EngineManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Incomplete / partial data for locale: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    move-object/from16 v0, p2

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v14}, Lcom/google/android/speech/embedded/Greco3EngineManager;->getCompiledGrammarPath(Lcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;)Ljava/lang/String;

    move-result-object v12

    if-nez v12, :cond_5

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    move-object/from16 v0, p2

    if-ne v0, v1, :cond_5

    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v12, 0x0

    :cond_5
    new-instance v13, Lcom/google/android/searchcommon/util/StopWatch;

    invoke-direct {v13}, Lcom/google/android/searchcommon/util/StopWatch;-><init>()V

    invoke-virtual {v13}, Lcom/google/android/searchcommon/util/StopWatch;->start()V

    if-nez v12, :cond_7

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v9

    :goto_1
    new-array v7, v9, [Ljava/lang/String;

    invoke-interface {v11, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    if-eqz v12, :cond_6

    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    aput-object v12, v7, v1

    :cond_6
    const-string v1, "VS.G3EngineManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create_rm: m="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",l="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v10, v7}, Lcom/google/android/speech/embedded/Greco3ResourceManager;->create(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3ResourceManager;

    move-result-object v2

    if-nez v2, :cond_8

    const-string v1, "VS.G3EngineManager"

    const-string v3, "Error loading resources."

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_7
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v9, v1, 0x1

    goto :goto_1

    :cond_8
    const-string v1, "VS.G3EngineManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Brought up new g3 instance :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "in: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13}, Lcom/google/android/searchcommon/util/StopWatch;->getElapsedTime()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    move-object/from16 v0, p2

    invoke-interface {v14, v0}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getConfigFile(Lcom/google/android/speech/embedded/Greco3Mode;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v14}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getLanguageMetadata()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    move-result-object v8

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;-><init>(Lcom/google/android/speech/embedded/Greco3ResourceManager;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3Mode;[Ljava/lang/String;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    goto/16 :goto_0
.end method

.method private releaseAllResourcesLocked()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognizer:Lcom/google/android/speech/embedded/Greco3Recognizer;

    if-eqz v2, :cond_0

    const-string v2, "VS.G3EngineManager"

    const-string v3, "Terminating active recognition for shutdown."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognizer:Lcom/google/android/speech/embedded/Greco3Recognizer;

    invoke-virtual {p0, v2}, Lcom/google/android/speech/embedded/Greco3EngineManager;->release(Lcom/google/android/speech/embedded/Greco3Recognizer;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mResourcesByMode:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    iget-object v2, v1, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;->resources:Lcom/google/android/speech/embedded/Greco3ResourceManager;

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/Greco3ResourceManager;->delete()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mResourcesByMode:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    return-void
.end method


# virtual methods
.method public delete(Ljava/io/File;Z)V
    .locals 2
    .param p1    # Ljava/io/File;
    .param p2    # Z

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mInitialized:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mRecognitionExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/speech/embedded/Greco3EngineManager$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/speech/embedded/Greco3EngineManager$2;-><init>(Lcom/google/android/speech/embedded/Greco3EngineManager;Ljava/io/File;Z)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getResources(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/speech/embedded/Greco3EngineManager;->getResourcesInternal(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    move-result-object v0

    return-object v0
.end method

.method public maybeInitialize()V
    .locals 5

    const/4 v4, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mInitialized:Z

    if-eqz v1, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/speech/embedded/Greco3DataManager;->blockingUpdateResources(Z)V

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mEndpointerModelCopier:Lcom/google/android/speech/embedded/EndpointerModelCopier;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mEndpointerModelCopier:Lcom/google/android/speech/embedded/EndpointerModelCopier;

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/Greco3DataManager;->getModelsDirSupplier()Lcom/google/common/base/Supplier;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-interface {v1, v2, v3}, Lcom/google/android/speech/embedded/EndpointerModelCopier;->copyEndpointerModels(Lcom/google/common/base/Supplier;Lcom/google/android/speech/embedded/Greco3DataManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v1, v4}, Lcom/google/android/speech/embedded/Greco3DataManager;->blockingUpdateResources(Z)V

    :cond_1
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mInitialized:Z

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public release(Lcom/google/android/speech/embedded/Greco3Recognizer;)V
    .locals 6
    .param p1    # Lcom/google/android/speech/embedded/Greco3Recognizer;

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognition:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognizer:Lcom/google/android/speech/embedded/Greco3Recognizer;

    if-ne p1, v2, :cond_1

    :goto_1
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/speech/embedded/Greco3Recognizer;->cancel()I

    :try_start_0
    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognition:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognizer:Lcom/google/android/speech/embedded/Greco3Recognizer;

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/Greco3Recognizer;->delete()V

    iput-object v5, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognition:Ljava/util/concurrent/Future;

    iput-object v5, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognizer:Lcom/google/android/speech/embedded/Greco3Recognizer;

    :goto_3
    return-void

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    const-string v2, "VS.G3EngineManager"

    const-string v3, "Interrupted waiting for recognition to complete."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v2, "VS.G3EngineManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception while running recognition: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public startRecognition(Lcom/google/android/speech/embedded/Greco3Recognizer;Ljava/io/InputStream;Lcom/google/android/speech/embedded/Greco3Callback;Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;Lcom/google/android/speech/embedded/GrecoEventLogger;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 7
    .param p1    # Lcom/google/android/speech/embedded/Greco3Recognizer;
    .param p2    # Ljava/io/InputStream;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Callback;
    .param p4    # Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .param p5    # Lcom/google/android/speech/embedded/GrecoEventLogger;
    .param p6    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognition:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p1, p2}, Lcom/google/android/speech/embedded/Greco3Recognizer;->setAudioReader(Ljava/io/InputStream;)I

    invoke-virtual {p1, p3}, Lcom/google/android/speech/embedded/Greco3Recognizer;->setCallback(Lcom/google/android/speech/embedded/Greco3Callback;)V

    iget-object v6, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mRecognitionExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/speech/embedded/Greco3EngineManager$1;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/speech/embedded/Greco3EngineManager$1;-><init>(Lcom/google/android/speech/embedded/Greco3EngineManager;Lcom/google/android/speech/embedded/GrecoEventLogger;Lcom/google/android/speech/embedded/Greco3Recognizer;Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognition:Ljava/util/concurrent/Future;

    iput-object p1, p0, Lcom/google/android/speech/embedded/Greco3EngineManager;->mCurrentRecognizer:Lcom/google/android/speech/embedded/Greco3Recognizer;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
