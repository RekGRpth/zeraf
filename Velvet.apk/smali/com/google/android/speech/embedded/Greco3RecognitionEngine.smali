.class public Lcom/google/android/speech/embedded/Greco3RecognitionEngine;
.super Ljava/lang/Object;
.source "Greco3RecognitionEngine.java"

# interfaces
.implements Lcom/google/android/speech/engine/RecognitionEngine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/embedded/Greco3RecognitionEngine$NoMatchesFromEmbeddedRecognizerException;,
        Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;
    }
.end annotation


# instance fields
.field private final mBytesPerSample:I

.field private final mCallbackFactory:Lcom/google/android/speech/embedded/Greco3CallbackFactory;

.field private mCurrentRecognition:Lcom/google/android/speech/embedded/Greco3Recognizer;

.field private mCurrentResources:Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

.field private final mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

.field private final mGrecoEventLoggerFactory:Lcom/google/android/speech/embedded/GrecoEventLogger$Factory;

.field private mInput:Ljava/io/InputStream;

.field private final mModeSelector:Lcom/google/android/speech/embedded/Greco3ModeSelector;

.field private final mSamplingRate:I

.field private final mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/embedded/Greco3EngineManager;Lcom/google/android/speech/SpeechLevelSource;IILcom/google/android/speech/embedded/Greco3CallbackFactory;Lcom/google/android/speech/embedded/Greco3ModeSelector;Lcom/google/android/speech/embedded/GrecoEventLogger$Factory;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/embedded/Greco3EngineManager;
    .param p2    # Lcom/google/android/speech/SpeechLevelSource;
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/google/android/speech/embedded/Greco3CallbackFactory;
    .param p6    # Lcom/google/android/speech/embedded/Greco3ModeSelector;
    .param p7    # Lcom/google/android/speech/embedded/GrecoEventLogger$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    iput-object p2, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    iput p3, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mSamplingRate:I

    iput p4, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mBytesPerSample:I

    iput-object p5, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCallbackFactory:Lcom/google/android/speech/embedded/Greco3CallbackFactory;

    iput-object p6, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mModeSelector:Lcom/google/android/speech/embedded/Greco3ModeSelector;

    iput-object p7, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mGrecoEventLoggerFactory:Lcom/google/android/speech/embedded/GrecoEventLogger$Factory;

    return-void
.end method

.method private cleanupAndDispatchStartError(Lcom/google/android/speech/embedded/Greco3Callback;Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Callback;
    .param p2    # Lcom/google/android/speech/exception/RecognizeException;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentRecognition:Lcom/google/android/speech/embedded/Greco3Recognizer;

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentResources:Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    invoke-interface {p1, p2}, Lcom/google/android/speech/embedded/Greco3Callback;->handleError(Lcom/google/android/speech/exception/RecognizeException;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentRecognition:Lcom/google/android/speech/embedded/Greco3Recognizer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentRecognition:Lcom/google/android/speech/embedded/Greco3Recognizer;

    invoke-virtual {v0, v1}, Lcom/google/android/speech/embedded/Greco3EngineManager;->release(Lcom/google/android/speech/embedded/Greco3Recognizer;)V

    iput-object v2, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentRecognition:Lcom/google/android/speech/embedded/Greco3Recognizer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mInput:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    iput-object v2, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mInput:Ljava/io/InputStream;

    return-void
.end method

.method createRecognizer(Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;)Lcom/google/android/speech/embedded/Greco3Recognizer;
    .locals 2
    .param p1    # Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    iget v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mSamplingRate:I

    iget v1, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mBytesPerSample:I

    invoke-static {p1, v0, v1}, Lcom/google/android/speech/embedded/Greco3Recognizer;->create(Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;II)Lcom/google/android/speech/embedded/Greco3Recognizer;

    move-result-object v0

    return-object v0
.end method

.method createRecognizerFor(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3Mode;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Grammar;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    invoke-virtual {v5}, Lcom/google/android/speech/embedded/Greco3EngineManager;->maybeInitialize()V

    iget-object v5, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mModeSelector:Lcom/google/android/speech/embedded/Greco3ModeSelector;

    invoke-interface {v5, p2, p3}, Lcom/google/android/speech/embedded/Greco3ModeSelector;->getMode(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mModeSelector:Lcom/google/android/speech/embedded/Greco3ModeSelector;

    invoke-interface {v5, p2, p3}, Lcom/google/android/speech/embedded/Greco3ModeSelector;->getFallbackMode(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v0

    const/4 v3, 0x0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    iget-object v5, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    invoke-virtual {v5, p1, v1, p3}, Lcom/google/android/speech/embedded/Greco3EngineManager;->getResources(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    move-result-object v2

    if-nez v2, :cond_2

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    invoke-virtual {v5, p1, v0, v4}, Lcom/google/android/speech/embedded/Greco3EngineManager;->getResources(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v3, v0

    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->createRecognizer(Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;)Lcom/google/android/speech/embedded/Greco3Recognizer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentRecognition:Lcom/google/android/speech/embedded/Greco3Recognizer;

    iput-object v2, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentResources:Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    move-object v4, v3

    goto :goto_0

    :cond_2
    move-object v3, v1

    goto :goto_1
.end method

.method public startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V
    .locals 10
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .param p3    # Lcom/google/android/speech/params/RecognizerParams;
    .param p4    # Lcom/google/android/speech/audio/EndpointerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/audio/AudioInputStreamFactory;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/params/RecognizerParams;",
            "Lcom/google/android/speech/audio/EndpointerListener;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentRecognition:Lcom/google/android/speech/embedded/Greco3Recognizer;

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentResources:Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    invoke-static {}, Lcom/google/android/speech/embedded/Greco3Recognizer;->maybeLoadSharedLibrary()V

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getGreco3Mode()Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v8

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getSpokenLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getGreco3Grammar()Lcom/google/android/speech/embedded/Greco3Grammar;

    move-result-object v1

    invoke-virtual {p0, v0, v8, v1}, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->createRecognizerFor(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCallbackFactory:Lcom/google/android/speech/embedded/Greco3CallbackFactory;

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v1

    invoke-interface {v0, p2, p4, v9, v1}, Lcom/google/android/speech/embedded/Greco3CallbackFactory;->create(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;)Lcom/google/android/speech/embedded/Greco3Callback;

    move-result-object v3

    if-nez v9, :cond_1

    new-instance v0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;

    invoke-direct {v0}, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;-><init>()V

    invoke-direct {p0, v3, v0}, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->cleanupAndDispatchStartError(Lcom/google/android/speech/embedded/Greco3Callback;Lcom/google/android/speech/exception/RecognizeException;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/speech/audio/AudioInputStreamFactory;->createInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mInput:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    iget-object v1, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentRecognition:Lcom/google/android/speech/embedded/Greco3Recognizer;

    iget-object v2, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mInput:Ljava/io/InputStream;

    invoke-virtual {p3}, Lcom/google/android/speech/params/RecognizerParams;->getEmbeddedRecognizerParams()Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mGrecoEventLoggerFactory:Lcom/google/android/speech/embedded/GrecoEventLogger$Factory;

    invoke-interface {v5, v9}, Lcom/google/android/speech/embedded/GrecoEventLogger$Factory;->getEventLoggerForMode(Lcom/google/android/speech/embedded/Greco3Mode;)Lcom/google/android/speech/embedded/GrecoEventLogger;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->mCurrentResources:Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;

    iget-object v6, v6, Lcom/google/android/speech/embedded/Greco3EngineManager$Resources;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/speech/embedded/Greco3EngineManager;->startRecognition(Lcom/google/android/speech/embedded/Greco3Recognizer;Ljava/io/InputStream;Lcom/google/android/speech/embedded/Greco3Callback;Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;Lcom/google/android/speech/embedded/GrecoEventLogger;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    invoke-virtual {v9}, Lcom/google/android/speech/embedded/Greco3Mode;->isEndpointerMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lcom/google/android/speech/embedded/Greco3Mode;->isEndpointerMode()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;

    invoke-direct {v0}, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;-><init>()V

    invoke-interface {v3, v0}, Lcom/google/android/speech/embedded/Greco3Callback;->handleError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0

    :catch_0
    move-exception v7

    new-instance v0, Lcom/google/android/speech/exception/AudioRecognizeException;

    const-string v1, "Unable to create stream"

    invoke-direct {v0, v1, v7}, Lcom/google/android/speech/exception/AudioRecognizeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v3, v0}, Lcom/google/android/speech/embedded/Greco3RecognitionEngine;->cleanupAndDispatchStartError(Lcom/google/android/speech/embedded/Greco3Callback;Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0
.end method
