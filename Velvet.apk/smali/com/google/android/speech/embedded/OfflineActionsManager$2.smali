.class Lcom/google/android/speech/embedded/OfflineActionsManager$2;
.super Ljava/lang/Object;
.source "OfflineActionsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/embedded/OfflineActionsManager;->dispatchCallbackOnMainThread(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

.field final synthetic val$status:I


# direct methods
.method constructor <init>(Lcom/google/android/speech/embedded/OfflineActionsManager;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$2;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iput p2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$2;->val$status:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$2;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$2;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    # getter for: Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    invoke-static {v0}, Lcom/google/android/speech/embedded/OfflineActionsManager;->access$200(Lcom/google/android/speech/embedded/OfflineActionsManager;)Lcom/google/android/speech/callback/SimpleCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$2;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    # getter for: Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    invoke-static {v0}, Lcom/google/android/speech/embedded/OfflineActionsManager;->access$200(Lcom/google/android/speech/embedded/OfflineActionsManager;)Lcom/google/android/speech/callback/SimpleCallback;

    move-result-object v0

    iget v2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$2;->val$status:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$2;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    invoke-static {v0, v2}, Lcom/google/android/speech/embedded/OfflineActionsManager;->access$202(Lcom/google/android/speech/embedded/OfflineActionsManager;Lcom/google/android/speech/callback/SimpleCallback;)Lcom/google/android/speech/callback/SimpleCallback;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
