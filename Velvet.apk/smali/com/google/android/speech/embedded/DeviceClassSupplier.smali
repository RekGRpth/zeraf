.class public Lcom/google/android/speech/embedded/DeviceClassSupplier;
.super Ljava/lang/Object;
.source "DeviceClassSupplier.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityManager:Landroid/app/ActivityManager;

.field private mDeviceClass:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/google/android/speech/embedded/DeviceClassSupplier;->mActivityManager:Landroid/app/ActivityManager;

    return-void
.end method

.method private calculateDeviceClass()Ljava/lang/Integer;
    .locals 5

    invoke-static {}, Lcom/google/android/speech/embedded/Greco3Recognizer;->isEndpointerOnly()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/speech/embedded/DeviceClassSupplier;->hasNeon()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/embedded/DeviceClassSupplier;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v1, v0, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    const-wide/32 v3, 0x29b92700

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method private static hasNeon()Z
    .locals 15

    const/4 v14, 0x2

    const/4 v7, 0x0

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    new-instance v12, Ljava/io/FileInputStream;

    const-string v13, "/proc/cpuinfo"

    invoke-direct {v12, v13}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v11, v12}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v11, "[ \t]*:[ \t]*"

    const/4 v12, 0x2

    invoke-virtual {v1, v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    array-length v11, v10

    if-ne v11, v14, :cond_0

    const-string v11, "Features"

    const/4 v12, 0x0

    aget-object v12, v10, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    const/4 v11, 0x1

    aget-object v11, v10, v11

    const-string v12, "[ \t]"

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    move-object v0, v6

    array-length v9, v0

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_1

    aget-object v5, v0, v8

    const-string v11, "neon"

    invoke-virtual {v11, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v11

    if-eqz v11, :cond_2

    const/4 v7, 0x1

    :cond_1
    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    move-object v2, v3

    :goto_1
    return v7

    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :catch_0
    move-exception v4

    :goto_2
    :try_start_2
    const-string v11, "VS.DeviceClassSupplier"

    const-string v12, "Error reading /proc/cpuinfo"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v11

    :goto_3
    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v11

    :catchall_1
    move-exception v11

    move-object v2, v3

    goto :goto_3

    :catch_1
    move-exception v4

    move-object v2, v3

    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized get()Ljava/lang/Integer;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/embedded/DeviceClassSupplier;->mDeviceClass:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/speech/embedded/DeviceClassSupplier;->calculateDeviceClass()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/embedded/DeviceClassSupplier;->mDeviceClass:Ljava/lang/Integer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/embedded/DeviceClassSupplier;->mDeviceClass:Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/embedded/DeviceClassSupplier;->get()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
