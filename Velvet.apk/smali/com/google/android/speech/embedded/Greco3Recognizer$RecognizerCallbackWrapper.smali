.class Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;
.super Ljava/lang/Object;
.source "Greco3Recognizer.java"

# interfaces
.implements Lcom/google/speech/recognizer/RecognizerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/embedded/Greco3Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecognizerCallbackWrapper"
.end annotation


# instance fields
.field private mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/speech/embedded/Greco3Recognizer$1;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/embedded/Greco3Recognizer$1;

    invoke-direct {p0}, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;Lcom/google/android/speech/embedded/Greco3Callback;)Lcom/google/android/speech/embedded/Greco3Callback;
    .locals 0
    .param p0    # Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;
    .param p1    # Lcom/google/android/speech/embedded/Greco3Callback;

    iput-object p1, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    return-object p1
.end method


# virtual methods
.method public handleAudioLevelEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$AudioLevelEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$AudioLevelEvent;

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/embedded/Greco3Callback;->handleAudioLevelEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$AudioLevelEvent;)V

    :cond_0
    return-void
.end method

.method public handleEndpointerEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/embedded/Greco3Callback;->handleEndpointerEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;)V

    :cond_0
    return-void
.end method

.method public handleRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/embedded/Greco3Callback;->handleRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    :cond_0
    return-void
.end method

.method public invalidate()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    return-void
.end method

.method public notifyError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/embedded/Greco3Callback;->handleError(Lcom/google/android/speech/exception/RecognizeException;)V

    :cond_0
    return-void
.end method

.method public updateProgress(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Recognizer$RecognizerCallbackWrapper;->mDelegate:Lcom/google/android/speech/embedded/Greco3Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/speech/embedded/Greco3Callback;->handleProgressUpdate(J)V

    :cond_0
    return-void
.end method
