.class Lcom/google/android/speech/embedded/OfflineActionsManager$3;
.super Ljava/lang/Object;
.source "OfflineActionsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/embedded/OfflineActionsManager;->internalMaybeScheduleGrammarCompilation(Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/google/android/speech/embedded/Greco3Grammar;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

.field final synthetic val$greco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

.field final synthetic val$locale:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/speech/embedded/OfflineActionsManager;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iput-object p2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->val$locale:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->val$greco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    # getter for: Lcom/google/android/speech/embedded/OfflineActionsManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;
    invoke-static {v1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->access$300(Lcom/google/android/speech/embedded/OfflineActionsManager;)Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->val$locale:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->val$greco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/speech/embedded/Greco3DataManager;->getRevisionForGrammar(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    # getter for: Lcom/google/android/speech/embedded/OfflineActionsManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->access$400(Lcom/google/android/speech/embedded/OfflineActionsManager;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->val$locale:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->val$greco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    iget-object v4, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v5, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;->val$greco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    # invokes: Lcom/google/android/speech/embedded/OfflineActionsManager;->getGrammarCompilationFrequency(Lcom/google/android/speech/embedded/Greco3Grammar;)J
    invoke-static {v4, v5}, Lcom/google/android/speech/embedded/OfflineActionsManager;->access$500(Lcom/google/android/speech/embedded/OfflineActionsManager;Lcom/google/android/speech/embedded/Greco3Grammar;)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/speech/grammar/GrammarCompilationService;->maybeSchedulePeriodicCompilation(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;J)V

    return-void
.end method
