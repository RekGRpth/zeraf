.class public Lcom/google/android/speech/alternates/UtfUtils;
.super Ljava/lang/Object;
.source "UtfUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getOffsetUtf16([BI)I
    .locals 5
    .param p0    # [B
    .param p1    # I

    const/4 v1, 0x0

    const/4 v2, 0x0

    :cond_0
    :goto_0
    if-ge v2, p1, :cond_4

    aget-byte v3, p0, v2

    and-int/lit16 v0, v3, 0xff

    ushr-int/lit8 v3, v0, 0x7

    if-nez v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    ushr-int/lit8 v3, v0, 0x5

    const/4 v4, 0x6

    if-ne v3, v4, :cond_2

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    ushr-int/lit8 v3, v0, 0x4

    const/16 v4, 0xe

    if-ne v3, v4, :cond_3

    add-int/lit8 v2, v2, 0x3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    ushr-int/lit8 v3, v0, 0x3

    const/16 v4, 0x1e

    if-ne v3, v4, :cond_0

    add-int/lit8 v2, v2, 0x4

    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_4
    return v1
.end method
