.class public Lcom/google/android/speech/dispatcher/RecognitionDispatcher;
.super Ljava/lang/Object;
.source "RecognitionDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/dispatcher/RecognitionDispatcher$1;,
        Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;,
        Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;
    }
.end annotation


# instance fields
.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private mRecognitionEngineCallback:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;

.field private mRecognitionEngines:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/speech/engine/RecognitionEngine;",
            ">;"
        }
    .end annotation
.end field

.field private final mState:Lcom/google/android/searchcommon/util/StateMachine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/StateMachine",
            "<",
            "Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 4
    .param p1    # Ljava/util/concurrent/ExecutorService;

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "RecognitionDispatcher"

    sget-object v1, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->IDLE:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->newBuilder(Ljava/lang/String;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->IDLE:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    sget-object v2, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->RUNNING:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->RUNNING:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    sget-object v2, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->IDLE:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setSingleThreadOnly(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setStrictMode(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->build()Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    iput-object p1, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mExecutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/speech/dispatcher/RecognitionDispatcher;)V
    .locals 0
    .param p0    # Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    invoke-direct {p0}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->stop()V

    return-void
.end method

.method private stop()V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v3, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->RUNNING:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    iget-object v2, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v3, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->IDLE:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-object v2, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngineCallback:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;

    invoke-virtual {v2}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->invalidate()V

    iput-object v4, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngineCallback:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;

    iget-object v2, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngines:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v0}, Lcom/google/android/speech/engine/RecognitionEngine;->close()V

    goto :goto_0

    :cond_0
    iput-object v4, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngines:Ljava/util/Collection;

    return-void
.end method

.method private static final threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "TT;)TT;"
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->RUNNING:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->stop()V

    :cond_0
    return-void
.end method

.method getCallback()Lcom/google/android/speech/callback/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngineCallback:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;

    return-object v0
.end method

.method public startRecognition(Ljava/util/Collection;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V
    .locals 5
    .param p3    # Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .param p4    # Lcom/google/android/speech/params/RecognizerParams;
    .param p5    # Lcom/google/android/speech/audio/EndpointerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/speech/engine/RecognitionEngine;",
            ">;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/audio/AudioInputStreamFactory;",
            "Lcom/google/android/speech/params/RecognizerParams;",
            "Lcom/google/android/speech/audio/EndpointerListener;",
            ")V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v4, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->RUNNING:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "RecognitionDispatcher"

    const-string v4, "Multiple recognitions in progress, the first will be cancelled."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->stop()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngines:Ljava/util/Collection;

    iget-object v3, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v4, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;->RUNNING:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$State;

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    new-instance v3, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;

    const/4 v4, 0x0

    invoke-direct {v3, p0, p2, v4}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;-><init>(Lcom/google/android/speech/dispatcher/RecognitionDispatcher;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/dispatcher/RecognitionDispatcher$1;)V

    iput-object v3, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngineCallback:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;

    iget-object v3, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v4, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngineCallback:Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;

    invoke-static {v3, v4}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/speech/callback/Callback;

    iget-object v3, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->mRecognitionEngines:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/engine/RecognitionEngine;

    invoke-interface {v0, p3, v2, p4, p5}, Lcom/google/android/speech/engine/RecognitionEngine;->startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    goto :goto_0

    :cond_1
    return-void
.end method
