.class public Lcom/google/android/speech/params/NetworkRequestProducerParams;
.super Ljava/lang/Object;
.source "NetworkRequestProducerParams.java"


# instance fields
.field private final mAuthTokenHelper:Lcom/google/android/speech/helper/AuthTokenHelper;

.field private final mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

.field private final mLocationHelper:Lcom/google/android/speech/helper/SpeechLocationHelper;

.field private final mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

.field private final mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/helper/AuthTokenHelper;Landroid/view/WindowManager;Lcom/google/android/speech/utils/NetworkInformation;Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;Lcom/google/android/speech/helper/SpeechLocationHelper;Lcom/google/android/speech/SpeechSettings;Lcom/google/common/base/Supplier;Lcom/google/android/speech/params/DeviceParams;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/helper/AuthTokenHelper;
    .param p2    # Landroid/view/WindowManager;
    .param p3    # Lcom/google/android/speech/utils/NetworkInformation;
    .param p4    # Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;
    .param p5    # Lcom/google/android/speech/helper/SpeechLocationHelper;
    .param p6    # Lcom/google/android/speech/SpeechSettings;
    .param p8    # Lcom/google/android/speech/params/DeviceParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/helper/AuthTokenHelper;",
            "Landroid/view/WindowManager;",
            "Lcom/google/android/speech/utils/NetworkInformation;",
            "Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;",
            "Lcom/google/android/speech/helper/SpeechLocationHelper;",
            "Lcom/google/android/speech/SpeechSettings;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/speech/params/DeviceParams;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mAuthTokenHelper:Lcom/google/android/speech/helper/AuthTokenHelper;

    iput-object p2, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mWindowManager:Landroid/view/WindowManager;

    iput-object p3, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    iput-object p4, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    iput-object p5, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mLocationHelper:Lcom/google/android/speech/helper/SpeechLocationHelper;

    iput-object p6, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    iput-object p7, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    iput-object p8, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    return-void
.end method


# virtual methods
.method public getAuthTokenHelper()Lcom/google/android/speech/helper/AuthTokenHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mAuthTokenHelper:Lcom/google/android/speech/helper/AuthTokenHelper;

    return-object v0
.end method

.method public getDeviceParams()Lcom/google/android/speech/params/DeviceParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    return-object v0
.end method

.method public getLocationHelper()Lcom/google/android/speech/helper/SpeechLocationHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mLocationHelper:Lcom/google/android/speech/helper/SpeechLocationHelper;

    return-object v0
.end method

.method public getNetworkInformation()Lcom/google/android/speech/utils/NetworkInformation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    return-object v0
.end method

.method public getPinholeParamsBuilder()Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    return-object v0
.end method

.method public getSoundSearchEnabledSupplier()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    return-object v0
.end method

.method public getSpeechSettings()Lcom/google/android/speech/SpeechSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    return-object v0
.end method

.method public getWindowManager()Landroid/view/WindowManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/NetworkRequestProducerParams;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method
