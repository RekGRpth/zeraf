.class public Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;
.super Ljava/lang/Object;
.source "RecognitionEngineParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/params/RecognitionEngineParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MusicDetectorParams"
.end annotation


# instance fields
.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method


# virtual methods
.method public getSettings()Lcom/google/android/voicesearch/settings/Settings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-object v0
.end method
