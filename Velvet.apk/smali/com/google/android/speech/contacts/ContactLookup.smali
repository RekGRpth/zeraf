.class public Lcom/google/android/speech/contacts/ContactLookup;
.super Ljava/lang/Object;
.source "ContactLookup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/contacts/ContactLookup$Data;,
        Lcom/google/android/speech/contacts/ContactLookup$Mode;
    }
.end annotation


# static fields
.field private static final CONTACT_COLS:[Ljava/lang/String;

.field private static final EMAIL_COLS:[Ljava/lang/String;

.field private static final PHONE_COLS:[Ljava/lang/String;

.field private static final POSTAL_ADDRESS_COLS:[Ljava/lang/String;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "times_contacted"

    aput-object v1, v0, v5

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_primary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/speech/contacts/ContactLookup;->PHONE_COLS:[Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "times_contacted"

    aput-object v1, v0, v5

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_primary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/speech/contacts/ContactLookup;->EMAIL_COLS:[Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "times_contacted"

    aput-object v1, v0, v5

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_primary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/speech/contacts/ContactLookup;->POSTAL_ADDRESS_COLS:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "times_contacted"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/speech/contacts/ContactLookup;->CONTACT_COLS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1    # Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/contacts/ContactLookup;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/speech/contacts/ContactLookup;->EMAIL_COLS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/speech/contacts/ContactLookup;->PHONE_COLS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/speech/contacts/ContactLookup;->CONTACT_COLS:[Ljava/lang/String;

    return-object v0
.end method

.method private static createDatas(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/ContactLookup$Data;",
            ">;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/google/android/speech/contacts/ContactLookup$Data;

    const/4 v4, 0x0

    invoke-direct {v3, v0, p1, v4}, Lcom/google/android/speech/contacts/ContactLookup$Data;-><init>(Lcom/google/majel/proto/ActionV2Protos$ActionContact;Ljava/lang/String;Lcom/google/android/speech/contacts/ContactLookup$1;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v3, "ContactLookup"

    const-string v4, "Cannot perform contact lookups on contacts with no display name."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private static createRowHandlerForContact(Lcom/google/android/speech/contacts/ContactLookup$Data;Ljava/util/List;Z)Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;
    .locals 1
    .param p0    # Lcom/google/android/speech/contacts/ContactLookup$Data;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/contacts/ContactLookup$Data;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;Z)",
            "Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/contacts/ContactLookup$1;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/speech/contacts/ContactLookup$1;-><init>(Ljava/util/List;ZLcom/google/android/speech/contacts/ContactLookup$Data;)V

    return-object v0
.end method

.method private fetchContactDetails(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1    # J
    .param p3    # Landroid/net/Uri;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    if-eqz p6, :cond_0

    if-eqz p5, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-static/range {p6 .. p6}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->phoneTypeStringToAndroidTypeColumn(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz p5, :cond_1

    if-nez v6, :cond_4

    :cond_1
    const-string v3, "contact_id = ?"

    :goto_1
    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v5, 0x0

    move-object v1, p3

    move-object v2, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    :goto_2
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v9, v1}, Lcom/google/android/speech/contacts/ContactLookup;->createRowHandlerForContact(Lcom/google/android/speech/contacts/ContactLookup$Data;Ljava/util/List;Z)Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/google/android/speech/contacts/Cursors;->iterateCursor(Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;Landroid/database/Cursor;)V

    :cond_2
    const/4 v0, 0x1

    invoke-static {v9, v0}, Lcom/google/android/speech/contacts/ContactsHelper;->uniqueContacts(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "contact_id = ? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :catch_0
    move-exception v8

    const-string v0, "ContactLookup"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception querying contacts provider: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private final fetchContactDetailsWithPreferredType(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1    # J
    .param p3    # Landroid/net/Uri;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-direct/range {p0 .. p6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchContactDetails(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchContactDetails(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    :cond_0
    return-object v7
.end method

.method public static fetchPhotoBitmap(Landroid/content/ContentResolver;JZ)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # J
    .param p3    # Z

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v0, p3}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;Z)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v2
.end method

.method private static filterPrimaryContacts(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    new-instance v3, Landroid/util/LongSparseArray;

    invoke-direct {v3}, Landroid/util/LongSparseArray;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->isPrimary()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v4

    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3, v4, v5, v6}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v4

    if-lez v4, :cond_4

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->isPrimary()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v0}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v4

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-eq v4, v5, :cond_2

    :cond_3
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v1, p0

    :cond_5
    return-object v1
.end method

.method private findAllByDisplayNameAndSpecifiedFilters(Lcom/google/android/speech/contacts/ContactLookup$Data;Lcom/google/android/speech/contacts/ContactLookup$Mode;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1    # Lcom/google/android/speech/contacts/ContactLookup$Data;
    .param p2    # Lcom/google/android/speech/contacts/ContactLookup$Mode;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/contacts/ContactLookup$Data;",
            "Lcom/google/android/speech/contacts/ContactLookup$Mode;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Mode;->hasValueColumns()Z
    invoke-static {p2}, Lcom/google/android/speech/contacts/ContactLookup$Mode;->access$400(Lcom/google/android/speech/contacts/ContactLookup$Mode;)Z

    move-result v3

    invoke-static {p1, v2, v3}, Lcom/google/android/speech/contacts/ContactLookup;->createRowHandlerForContact(Lcom/google/android/speech/contacts/ContactLookup$Data;Ljava/util/List;Z)Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;

    move-result-object v1

    invoke-direct {p0, p1, p3, p2}, Lcom/google/android/speech/contacts/ContactLookup;->searchContactsWithRetry(Lcom/google/android/speech/contacts/ContactLookup$Data;Ljava/lang/String;Lcom/google/android/speech/contacts/ContactLookup$Mode;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v1, v0}, Lcom/google/android/speech/contacts/Cursors;->iterateCursor(Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;Landroid/database/Cursor;)V

    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    :cond_0
    return-object v2
.end method

.method private searchContacts(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/contacts/ContactLookup$Mode;Z)Landroid/database/Cursor;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/speech/contacts/ContactLookup$Mode;
    .param p4    # Z

    const/4 v5, 0x0

    invoke-virtual {p3}, Lcom/google/android/speech/contacts/ContactLookup$Mode;->getContentFilterUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    if-eqz p4, :cond_2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v3, "(times_contacted>0)"

    :goto_0
    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {p3}, Lcom/google/android/speech/contacts/ContactLookup$Mode;->getCols()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_1
    return-object v6

    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "((times_contacted>0)) AND ("

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    move-object v3, p2

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v0, "ContactLookup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception querying content provider: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private searchContactsWithRetry(Lcom/google/android/speech/contacts/ContactLookup$Data;Ljava/lang/String;Lcom/google/android/speech/contacts/ContactLookup$Mode;)Landroid/database/Cursor;
    .locals 4
    .param p1    # Lcom/google/android/speech/contacts/ContactLookup$Data;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/speech/contacts/ContactLookup$Mode;

    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Data;->getName()Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/speech/contacts/ContactLookup$Data;->access$700(Lcom/google/android/speech/contacts/ContactLookup$Data;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, p2, p3, v3}, Lcom/google/android/speech/contacts/ContactLookup;->searchContacts(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/contacts/ContactLookup$Mode;Z)Landroid/database/Cursor;

    move-result-object v0

    move-object v1, v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Data;->getName()Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/speech/contacts/ContactLookup$Data;->access$700(Lcom/google/android/speech/contacts/ContactLookup$Data;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, p2, p3, v3}, Lcom/google/android/speech/contacts/ContactLookup;->searchContacts(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/contacts/ContactLookup$Mode;Z)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    throw v2
.end method


# virtual methods
.method public fetchEmailAddresses(JLjava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/speech/contacts/ContactLookup;->EMAIL_COLS:[Ljava/lang/String;

    const-string v5, "data2"

    move-object v0, p0

    move-wide v1, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchContactDetailsWithPreferredType(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public fetchPhoneNumbers(JLjava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/speech/contacts/ContactLookup;->PHONE_COLS:[Ljava/lang/String;

    const-string v5, "data2"

    move-object v0, p0

    move-wide v1, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchContactDetailsWithPreferredType(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public fetchPostalAddresses(JLjava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/speech/contacts/ContactLookup;->POSTAL_ADDRESS_COLS:[Ljava/lang/String;

    const-string v5, "data2"

    move-object v0, p0

    move-wide v1, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchContactDetailsWithPreferredType(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public findAllByDisplayName(Lcom/google/android/speech/contacts/ContactLookup$Mode;Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1    # Lcom/google/android/speech/contacts/ContactLookup$Mode;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/contacts/ContactLookup$Mode;",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    const-string v6, "must specify a valid Mode argument"

    invoke-static {p1, v6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2, p3}, Lcom/google/android/speech/contacts/ContactLookup;->createDatas(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/contacts/ContactLookup$Data;

    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Data;->getType()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/speech/contacts/ContactLookup$Data;->access$300(Lcom/google/android/speech/contacts/ContactLookup$Data;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    move-object v4, v5

    :goto_1
    invoke-direct {p0, v1, p1, v4}, Lcom/google/android/speech/contacts/ContactLookup;->findAllByDisplayNameAndSpecifiedFilters(Lcom/google/android/speech/contacts/ContactLookup$Data;Lcom/google/android/speech/contacts/ContactLookup$Mode;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%s = %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "data2"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Data;->getType()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/speech/contacts/ContactLookup$Data;->access$300(Lcom/google/android/speech/contacts/ContactLookup$Data;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v3, 0x0

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/contacts/ContactLookup$Data;

    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Data;->getType()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/speech/contacts/ContactLookup$Data;->access$300(Lcom/google/android/speech/contacts/ContactLookup$Data;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-direct {p0, v1, p1, v5}, Lcom/google/android/speech/contacts/ContactLookup;->findAllByDisplayNameAndSpecifiedFilters(Lcom/google/android/speech/contacts/ContactLookup$Data;Lcom/google/android/speech/contacts/ContactLookup$Mode;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Mode;->hasValueColumns()Z
    invoke-static {p1}, Lcom/google/android/speech/contacts/ContactLookup$Mode;->access$400(Lcom/google/android/speech/contacts/ContactLookup$Mode;)Z

    move-result v5

    invoke-static {v0, v5}, Lcom/google/android/speech/contacts/ContactsHelper;->uniqueContacts(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/speech/contacts/ContactLookup;->filterPrimaryContacts(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
