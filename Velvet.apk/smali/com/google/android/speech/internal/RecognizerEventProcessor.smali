.class public Lcom/google/android/speech/internal/RecognizerEventProcessor;
.super Ljava/lang/Object;
.source "RecognizerEventProcessor.java"


# static fields
.field private static INVALID_PHONE_TYPE:I


# instance fields
.field private final mCallback:Lcom/google/android/speech/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation
.end field

.field private final mCombinedResultGenerator:Lcom/google/android/speech/internal/CombinedResultGenerator;

.field private final mMode:Lcom/google/android/speech/embedded/Greco3Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->INVALID_PHONE_TYPE:I

    return-void
.end method

.method constructor <init>(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/callback/Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Mode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/embedded/Greco3Mode;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mMode:Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/callback/Callback;

    iput-object v0, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCallback:Lcom/google/android/speech/callback/Callback;

    new-instance v0, Lcom/google/android/speech/internal/CombinedResultGenerator;

    invoke-direct {v0}, Lcom/google/android/speech/internal/CombinedResultGenerator;-><init>()V

    iput-object v0, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCombinedResultGenerator:Lcom/google/android/speech/internal/CombinedResultGenerator;

    return-void
.end method

.method private static addCombinedResultsTo(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 2
    .param p0    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    new-instance v0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-direct {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setCombinedResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static buildPhoneAction(Ljava/lang/String;ID)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # D

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0, p0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setParsedName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    new-instance v1, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;-><init>()V

    invoke-virtual {v1, p2, p3}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->setGrammarWeight(D)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->setEmbeddedActionContactExtension(Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    const-string v5, "VS.RecognizerEventProcessor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "n="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p0}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->elideContactName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", t="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget v5, Lcom/google/android/speech/internal/RecognizerEventProcessor;->INVALID_PHONE_TYPE:I

    if-eq p1, v5, :cond_1

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;-><init>()V

    invoke-static {p1}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->getServerTypeStringFromAndroidType(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->setType(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    :cond_0
    invoke-virtual {v0, v3}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->addPhone(Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    :cond_1
    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    return-object v2
.end method

.method private dressActionV2InMajelResponseAndThenDressThatInAnS3Response(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/speech/s3/S3$S3Response;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setPrimaryType(I)Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v1, p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addActionV2(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->setSearchResultsUnnecessary(Z)Lcom/google/majel/proto/PeanutProtos$Peanut;

    new-instance v0, Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-direct {v0}, Lcom/google/majel/proto/MajelProtos$MajelResponse;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->addPeanut(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-static {v0}, Lcom/google/android/speech/message/S3ResponseBuilder;->createWithMajel(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    return-object v2
.end method

.method private static elideContactName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static final getServerTypeStringFromAndroidType(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    const-string v0, "home"

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    const-string v0, "work"

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    const-string v0, "cell"

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleCallCommand([Ljava/lang/String;I)Lcom/google/speech/s3/S3$S3Response;
    .locals 11
    .param p1    # [Ljava/lang/String;
    .param p2    # I

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0xf

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    sget v6, Lcom/google/android/speech/internal/RecognizerEventProcessor;->INVALID_PHONE_TYPE:I

    move v3, p2

    :goto_0
    array-length v8, p1

    if-ge v3, v8, :cond_3

    aget-object v4, p1, v3

    const-string v8, "XX_"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-static {v4}, Lcom/google/android/speech/grammar/GrammarBuilder;->decodeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4}, Lcom/google/android/speech/grammar/GrammarBuilder;->decodeWeight(Ljava/lang/String;)D

    move-result-wide v1

    :cond_0
    const-string v8, "_p"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    :try_start_0
    const-string v8, "_p"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    :cond_1
    :goto_1
    const-string v8, "_d"

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "_d"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v5

    const-string v8, "VS.RecognizerEventProcessor"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid semantic tag: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    invoke-direct {p0, v0, v1, v2, v6}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->handleContactName(Ljava/lang/String;DI)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v8

    :goto_2
    return-object v8

    :cond_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_5

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->handleSpokenPhoneNumber(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v8

    goto :goto_2

    :cond_5
    const/4 v8, 0x0

    goto :goto_2
.end method

.method private handleContactName(Ljava/lang/String;DI)Lcom/google/speech/s3/S3$S3Response;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # D
    .param p4    # I

    invoke-static {p1, p4, p2, p3}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->buildPhoneAction(Ljava/lang/String;ID)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v1

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setPhoneActionExtension(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setExecute(Z)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {p0, v0}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->dressActionV2InMajelResponseAndThenDressThatInAnS3Response(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    return-object v2
.end method

.method private handleSpokenPhoneNumber(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Response;
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->setNumber(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    new-instance v2, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;-><init>()V

    new-instance v4, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-direct {v4}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;-><init>()V

    invoke-virtual {v4, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->addPhone(Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;)Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setPhoneActionExtension(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->dressActionV2InMajelResponseAndThenDressThatInAnS3Response(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    return-object v2
.end method

.method private processEventInDictationAndHotwordMode(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 3
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCombinedResultGenerator:Lcom/google/android/speech/internal/CombinedResultGenerator;

    invoke-virtual {v1, p1}, Lcom/google/android/speech/internal/CombinedResultGenerator;->update(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCombinedResultGenerator:Lcom/google/android/speech/internal/CombinedResultGenerator;

    invoke-virtual {v1}, Lcom/google/android/speech/internal/CombinedResultGenerator;->getCombinedResultEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-static {v0}, Lcom/google/android/speech/message/S3ResponseBuilder;->createInProgress(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-static {}, Lcom/google/android/speech/message/S3ResponseBuilder;->createDone()Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-static {p1}, Lcom/google/android/speech/message/S3ResponseBuilder;->createInProgress(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private processEventInGrammarMode(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 3
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->processSemanticInterpretations(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-static {p1}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->addCombinedResultsTo(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/speech/message/S3ResponseBuilder;->createInProgress(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-interface {v1, v0}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-static {}, Lcom/google/android/speech/message/S3ResponseBuilder;->createDone()Lcom/google/speech/s3/S3$S3Response;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mCallback:Lcom/google/android/speech/callback/Callback;

    new-instance v2, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$NoMatchesFromEmbeddedRecognizerException;

    invoke-direct {v2}, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$NoMatchesFromEmbeddedRecognizerException;-><init>()V

    invoke-interface {v1, v2}, Lcom/google/android/speech/callback/Callback;->onError(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private processSemanticInterpretations(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/s3/S3$S3Response;
    .locals 7
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult()Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisCount()I

    move-result v1

    const/4 v6, 0x1

    if-lt v1, v6, :cond_0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_0

    invoke-virtual {v4, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesis(I)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasSemanticResult()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getSemanticResult()Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;->getInterpretationList()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5, p1}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->processSemanticInterpretations(Ljava/util/List;Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v5

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private processSemanticInterpretations(Ljava/util/List;Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/s3/S3$S3Response;
    .locals 7
    .param p2    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lspeech/InterpretationProto$Interpretation;",
            ">;",
            "Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;",
            ")",
            "Lcom/google/speech/s3/S3$S3Response;"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lspeech/InterpretationProto$Interpretation;

    invoke-virtual {v4}, Lspeech/InterpretationProto$Interpretation;->getSlotCount()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v4, v5

    :goto_0
    return-object v4

    :cond_1
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lspeech/InterpretationProto$Interpretation;

    invoke-virtual {v4, v6}, Lspeech/InterpretationProto$Interpretation;->getSlot(I)Lspeech/InterpretationProto$Slot;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/InterpretationProto$Slot;->hasValue()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lspeech/InterpretationProto$Slot;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    move-object v4, v5

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Lspeech/InterpretationProto$Slot;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_7

    aget-object v1, v2, v0

    if-eqz v1, :cond_6

    const-string v4, "_call"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    add-int/lit8 v4, v0, 0x1

    invoke-direct {p0, v2, v4}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->handleCallCommand([Ljava/lang/String;I)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v4

    goto :goto_0

    :cond_4
    const-string v4, "_cancel"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v6, "_okay"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v4, v6

    const-string v6, "_call_back"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v4, v6

    const-string v6, "_respond"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v4, v6

    const-string v6, "_hotword"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v4, v6

    const-string v6, "_next"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v4, v6

    const-string v6, "_select"

    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    or-int/2addr v4, v6

    if-eqz v4, :cond_5

    invoke-static {p2}, Lcom/google/android/speech/message/S3ResponseBuilder;->createWithRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/s3/S3$S3Response;

    move-result-object v4

    goto :goto_0

    :cond_5
    const-string v4, "_other"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object v4, v5

    goto/16 :goto_0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    move-object v4, v5

    goto/16 :goto_0
.end method


# virtual methods
.method process(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 2
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-object v0, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mMode:Lcom/google/android/speech/embedded/Greco3Mode;

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mMode:Lcom/google/android/speech/embedded/Greco3Mode;

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->HOTWORD:Lcom/google/android/speech/embedded/Greco3Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mMode:Lcom/google/android/speech/embedded/Greco3Mode;

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasEventType()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "VS.RecognizerEventProcessor"

    const-string v1, "Received recognition event without type."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getStatus()I

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "VS.RecognizerEventProcessor"

    const-string v1, "Error from embedded recognizer."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/speech/internal/RecognizerEventProcessor;->mMode:Lcom/google/android/speech/embedded/Greco3Mode;

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne v0, v1, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->processEventInGrammarMode(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->processEventInDictationAndHotwordMode(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    goto :goto_1
.end method
