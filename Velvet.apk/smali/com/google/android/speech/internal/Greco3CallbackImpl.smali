.class public Lcom/google/android/speech/internal/Greco3CallbackImpl;
.super Ljava/lang/Object;
.source "Greco3CallbackImpl.java"

# interfaces
.implements Lcom/google/android/speech/embedded/Greco3Callback;


# instance fields
.field private final mCallback:Lcom/google/android/speech/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation
.end field

.field private final mEndpointerProcessor:Lcom/google/android/speech/internal/EndpointerEventProcessor;

.field private final mRecognitionEventProcessor:Lcom/google/android/speech/internal/RecognizerEventProcessor;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/internal/EndpointerEventProcessor;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p3    # Lcom/google/android/speech/internal/EndpointerEventProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/embedded/Greco3Mode;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/internal/EndpointerEventProcessor;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/speech/internal/Greco3CallbackImpl;->mEndpointerProcessor:Lcom/google/android/speech/internal/EndpointerEventProcessor;

    new-instance v0, Lcom/google/android/speech/internal/RecognizerEventProcessor;

    invoke-direct {v0, p1, p2}, Lcom/google/android/speech/internal/RecognizerEventProcessor;-><init>(Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/android/speech/callback/Callback;)V

    iput-object v0, p0, Lcom/google/android/speech/internal/Greco3CallbackImpl;->mRecognitionEventProcessor:Lcom/google/android/speech/internal/RecognizerEventProcessor;

    iput-object p2, p0, Lcom/google/android/speech/internal/Greco3CallbackImpl;->mCallback:Lcom/google/android/speech/callback/Callback;

    return-void
.end method


# virtual methods
.method public handleAudioLevelEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$AudioLevelEvent;)V
    .locals 0
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$AudioLevelEvent;

    return-void
.end method

.method public handleEndpointerEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    iget-object v0, p0, Lcom/google/android/speech/internal/Greco3CallbackImpl;->mEndpointerProcessor:Lcom/google/android/speech/internal/EndpointerEventProcessor;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->process(Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;)V

    return-void
.end method

.method public handleError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/speech/internal/Greco3CallbackImpl;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/callback/Callback;->onError(Ljava/lang/Object;)V

    return-void
.end method

.method public handleProgressUpdate(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/speech/internal/Greco3CallbackImpl;->mEndpointerProcessor:Lcom/google/android/speech/internal/EndpointerEventProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->updateProgress(J)V

    return-void
.end method

.method public handleRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-object v0, p0, Lcom/google/android/speech/internal/Greco3CallbackImpl;->mRecognitionEventProcessor:Lcom/google/android/speech/internal/RecognizerEventProcessor;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/internal/RecognizerEventProcessor;->process(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    return-void
.end method
