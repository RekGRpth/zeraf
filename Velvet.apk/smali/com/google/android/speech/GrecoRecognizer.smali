.class public Lcom/google/android/speech/GrecoRecognizer;
.super Ljava/lang/Object;
.source "GrecoRecognizer.java"

# interfaces
.implements Lcom/google/android/speech/Recognizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/GrecoRecognizer$3;,
        Lcom/google/android/speech/GrecoRecognizer$State;
    }
.end annotation


# instance fields
.field private mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

.field private final mAudioController:Lcom/google/android/speech/audio/AudioController;

.field private final mAudioStore:Lcom/google/android/speech/audio/AudioStore;

.field private final mEngineStore:Lcom/google/android/speech/RecognitionEngineStore;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mListeningState:Lcom/google/android/searchcommon/util/StateMachine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/StateMachine",
            "<",
            "Lcom/google/android/speech/GrecoRecognizer$State;",
            ">;"
        }
    .end annotation
.end field

.field private final mRecognitionDispatcher:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

.field private mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/audio/AudioController;Lcom/google/android/speech/audio/AudioStore;Lcom/google/android/speech/dispatcher/RecognitionDispatcher;Lcom/google/android/speech/RecognitionEngineStore;Ljava/util/concurrent/ExecutorService;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/audio/AudioController;
    .param p2    # Lcom/google/android/speech/audio/AudioStore;
    .param p3    # Lcom/google/android/speech/dispatcher/RecognitionDispatcher;
    .param p4    # Lcom/google/android/speech/RecognitionEngineStore;
    .param p5    # Ljava/util/concurrent/ExecutorService;

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "GrecoRecognizer"

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->newBuilder(Ljava/lang/String;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    sget-object v2, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    sget-object v2, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    sget-object v2, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    sget-object v2, Lcom/google/android/speech/GrecoRecognizer$State;->STOPPED:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->STOPPED:Lcom/google/android/speech/GrecoRecognizer$State;

    sget-object v2, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setSingleThreadOnly(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setStrictMode(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->build()Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    new-instance v0, Lcom/google/android/speech/GrecoRecognizer$1;

    invoke-direct {v0, p0}, Lcom/google/android/speech/GrecoRecognizer$1;-><init>(Lcom/google/android/speech/GrecoRecognizer;)V

    iput-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    iput-object p1, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    iput-object p2, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    iput-object p3, p0, Lcom/google/android/speech/GrecoRecognizer;->mRecognitionDispatcher:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    iput-object p4, p0, Lcom/google/android/speech/GrecoRecognizer;->mEngineStore:Lcom/google/android/speech/RecognitionEngineStore;

    iput-object p5, p0, Lcom/google/android/speech/GrecoRecognizer;->mExecutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/speech/GrecoRecognizer;)Lcom/google/android/speech/audio/AudioStore;
    .locals 1
    .param p0    # Lcom/google/android/speech/GrecoRecognizer;

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/speech/GrecoRecognizer;)Lcom/google/android/searchcommon/util/StateMachine;
    .locals 1
    .param p0    # Lcom/google/android/speech/GrecoRecognizer;

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/speech/GrecoRecognizer;)V
    .locals 0
    .param p0    # Lcom/google/android/speech/GrecoRecognizer;

    invoke-direct {p0}, Lcom/google/android/speech/GrecoRecognizer;->internalStopAudio()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/speech/GrecoRecognizer;)V
    .locals 0
    .param p0    # Lcom/google/android/speech/GrecoRecognizer;

    invoke-direct {p0}, Lcom/google/android/speech/GrecoRecognizer;->internalShutdownAudio()V

    return-void
.end method

.method public static create(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/params/RecognitionEngineParams;Lcom/google/android/speech/audio/AudioController;Lcom/google/android/speech/audio/AudioStore;)Lcom/google/android/speech/Recognizer;
    .locals 6
    .param p0    # Ljava/util/concurrent/ExecutorService;
    .param p1    # Lcom/google/android/speech/params/RecognitionEngineParams;
    .param p2    # Lcom/google/android/speech/audio/AudioController;
    .param p3    # Lcom/google/android/speech/audio/AudioStore;

    new-instance v0, Lcom/google/android/speech/GrecoRecognizer;

    new-instance v3, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    invoke-direct {v3, p0}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;-><init>(Ljava/util/concurrent/ExecutorService;)V

    new-instance v4, Lcom/google/android/speech/RecognitionEngineStore;

    invoke-direct {v4, p1}, Lcom/google/android/speech/RecognitionEngineStore;-><init>(Lcom/google/android/speech/params/RecognitionEngineParams;)V

    move-object v1, p2

    move-object v2, p3

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/speech/GrecoRecognizer;-><init>(Lcom/google/android/speech/audio/AudioController;Lcom/google/android/speech/audio/AudioStore;Lcom/google/android/speech/dispatcher/RecognitionDispatcher;Lcom/google/android/speech/RecognitionEngineStore;Ljava/util/concurrent/ExecutorService;)V

    invoke-static {p0, v0}, Lcom/google/android/speech/GrecoRecognizer;->threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/Recognizer;

    return-object v0
.end method

.method private internalShutdownAudio()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    invoke-virtual {v0}, Lcom/google/android/speech/ResponseProcessor;->invalidate()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    invoke-virtual {v0}, Lcom/google/android/speech/audio/AudioController;->shutdown()V

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    invoke-virtual {v0}, Lcom/google/android/speech/audio/AudioStore;->waitForRecording()V

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mRecognitionDispatcher:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    invoke-virtual {v0}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->cancel()V

    return-void
.end method

.method private internalStopAudio()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->STOPPED:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    invoke-virtual {v0}, Lcom/google/android/speech/audio/AudioController;->stopListening()V

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    invoke-virtual {v0}, Lcom/google/android/speech/audio/AudioStore;->waitForRecording()V

    :cond_0
    return-void
.end method

.method private static recordStartRecognitionEvent(Lcom/google/android/speech/params/RecognizerParams;)V
    .locals 2
    .param p0    # Lcom/google/android/speech/params/RecognizerParams;

    sget-object v0, Lcom/google/android/speech/GrecoRecognizer$3;->$SwitchMap$com$google$android$speech$params$RecognizerParams$Mode:[I

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerParams$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    :goto_0
    return-void

    :pswitch_0
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static final threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static final threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "TT;)TT;"
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->notIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    invoke-virtual {v0}, Lcom/google/android/speech/ResponseProcessor;->onRecognitionCancelled()V

    invoke-direct {p0}, Lcom/google/android/speech/GrecoRecognizer;->internalShutdownAudio()V

    :cond_0
    return-void
.end method

.method getAudioCallback()Lcom/google/android/speech/ResponseProcessor$AudioCallback;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    return-object v0
.end method

.method setResponseProcessor(Lcom/google/android/speech/ResponseProcessor;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/ResponseProcessor;

    iput-object p1, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    return-void
.end method

.method public startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getAudioInputParams()Lcom/google/android/speech/params/AudioInputParams;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v3, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v1, v3}, Lcom/google/android/searchcommon/util/StateMachine;->notIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/google/android/speech/params/AudioInputParams;->hasStreamRewindTime()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mRecognitionDispatcher:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    invoke-virtual {v1}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->cancel()V

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    invoke-virtual {v1}, Lcom/google/android/speech/ResponseProcessor;->invalidate()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    move v1, v0

    :goto_0
    const-class v0, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-static {p3, v0, p2}, Lcom/google/android/speech/GrecoRecognizer;->threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-static {p1}, Lcom/google/android/speech/GrecoRecognizer;->recordStartRecognitionEvent(Lcom/google/android/speech/params/RecognizerParams;)V

    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getRequestId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    invoke-virtual {v2}, Lcom/google/android/speech/params/AudioInputParams;->getStreamRewindTime()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/google/android/speech/audio/AudioController;->rewindInputStreamFactory(J)Lcom/google/android/speech/audio/AudioInputStreamFactory;

    move-result-object v3

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/speech/params/AudioInputParams;->shouldStoreCompleteAudio()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    invoke-interface {v3}, Lcom/google/android/speech/audio/AudioInputStreamFactory;->createInputStream()Ljava/io/InputStream;

    move-result-object v2

    const/16 v4, 0x140

    invoke-virtual {v1, v2, v4}, Lcom/google/android/speech/audio/AudioStore;->startRecording(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->isBluetoothRecordingEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/speech/audio/AudioController;->setBluetoothRecordingEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    invoke-virtual {v1, v0}, Lcom/google/android/speech/audio/AudioController;->startListening(Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    new-instance v1, Lcom/google/android/speech/ResponseProcessor;

    iget-object v2, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    invoke-direct {v1, v2, v0, p1}, Lcom/google/android/speech/ResponseProcessor;-><init>(Lcom/google/android/speech/ResponseProcessor$AudioCallback;Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams;)V

    iput-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mRecognitionDispatcher:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mEngineStore:Lcom/google/android/speech/RecognitionEngineStore;

    invoke-virtual {v1, p1}, Lcom/google/android/speech/RecognitionEngineStore;->getEnginesForParams(Lcom/google/android/speech/params/RecognizerParams;)Ljava/util/Collection;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    iget-object v4, p0, Lcom/google/android/speech/GrecoRecognizer;->mExecutor:Ljava/util/concurrent/ExecutorService;

    const-class v5, Lcom/google/android/speech/audio/EndpointerListener;

    iget-object v6, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    invoke-static {v4, v5, v6}, Lcom/google/android/speech/GrecoRecognizer;->threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/speech/audio/EndpointerListener;

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->startRecognition(Ljava/util/Collection;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    :goto_2
    return-void

    :cond_1
    const-string v1, "GrecoRecognizer"

    const-string v3, "Multiple recognitions in progress, the first will be cancelled."

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/speech/GrecoRecognizer;->internalShutdownAudio()V

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v3, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v1, v3}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    invoke-virtual {v1, v2}, Lcom/google/android/speech/audio/AudioController;->createInputStreamFactory(Lcom/google/android/speech/params/AudioInputParams;)Lcom/google/android/speech/audio/AudioInputStreamFactory;

    move-result-object v3

    goto :goto_1

    :catch_0
    move-exception v1

    new-instance v2, Lcom/google/android/speech/exception/AudioRecognizeException;

    const-string v3, "Unable to start the audio recording"

    invoke-direct {v2, v3, v1}, Lcom/google/android/speech/exception/AudioRecognizeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v2}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_2
.end method

.method public startRecordedAudioRecognition(Lcom/google/android/speech/params/RecognizerParams;[BLcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->notIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GrecoRecognizer"

    const-string v1, "Multiple recognitions in progress, the first will be cancelled."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/speech/GrecoRecognizer;->internalShutdownAudio()V

    :cond_0
    const-class v0, Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-static {p4, v0, p3}, Lcom/google/android/speech/GrecoRecognizer;->threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/listeners/RecognitionEventListener;

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/speech/GrecoRecognizer$State;->IDLE:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    invoke-static {p1}, Lcom/google/android/speech/GrecoRecognizer;->recordStartRecognitionEvent(Lcom/google/android/speech/params/RecognizerParams;)V

    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getRequestId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    new-instance v3, Lcom/google/android/speech/GrecoRecognizer$2;

    invoke-direct {v3, p0, p2}, Lcom/google/android/speech/GrecoRecognizer$2;-><init>(Lcom/google/android/speech/GrecoRecognizer;[B)V

    new-instance v1, Lcom/google/android/speech/ResponseProcessor;

    iget-object v2, p0, Lcom/google/android/speech/GrecoRecognizer;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    invoke-direct {v1, v2, v0, p1}, Lcom/google/android/speech/ResponseProcessor;-><init>(Lcom/google/android/speech/ResponseProcessor$AudioCallback;Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams;)V

    iput-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mRecognitionDispatcher:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    iget-object v1, p0, Lcom/google/android/speech/GrecoRecognizer;->mEngineStore:Lcom/google/android/speech/RecognitionEngineStore;

    invoke-virtual {v1, p1}, Lcom/google/android/speech/RecognitionEngineStore;->getEnginesForParams(Lcom/google/android/speech/params/RecognizerParams;)Ljava/util/Collection;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    iget-object v4, p0, Lcom/google/android/speech/GrecoRecognizer;->mExecutor:Ljava/util/concurrent/ExecutorService;

    const-class v5, Lcom/google/android/speech/audio/EndpointerListener;

    iget-object v6, p0, Lcom/google/android/speech/GrecoRecognizer;->mResponseProcessor:Lcom/google/android/speech/ResponseProcessor;

    invoke-static {v4, v5, v6}, Lcom/google/android/speech/GrecoRecognizer;->threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/speech/audio/EndpointerListener;

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->startRecognition(Ljava/util/Collection;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V

    return-void
.end method

.method public stopListening()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/GrecoRecognizer;->mListeningState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/GrecoRecognizer$State;->LISTENING:Lcom/google/android/speech/GrecoRecognizer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/speech/GrecoRecognizer;->internalStopAudio()V

    :cond_0
    return-void
.end method
