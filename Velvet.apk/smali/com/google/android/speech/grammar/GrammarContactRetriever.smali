.class public Lcom/google/android/speech/grammar/GrammarContactRetriever;
.super Ljava/lang/Object;
.source "GrammarContactRetriever.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/grammar/GrammarContactRetriever$1;,
        Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;,
        Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;
    }
.end annotation


# static fields
.field private static final COLS:[Ljava/lang/String;


# instance fields
.field protected final mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "times_contacted"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "last_time_contacted"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/speech/grammar/GrammarContactRetriever;->COLS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1
    .param p1    # Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/speech/contacts/ContactRetriever;

    invoke-direct {v0, p1}, Lcom/google/android/speech/contacts/ContactRetriever;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever;->mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;

    return-void
.end method


# virtual methods
.method public getContacts()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/grammar/GrammarContact;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;

    invoke-direct {v0}, Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever;->mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/16 v3, 0x64

    sget-object v4, Lcom/google/android/speech/grammar/GrammarContactRetriever;->COLS:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/speech/contacts/ContactRetriever;->getContacts(Landroid/net/Uri;I[Ljava/lang/String;Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;)V

    # getter for: Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;->mResults:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;->access$000(Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getLowerCaseContactNames()[Ljava/lang/String;
    .locals 5

    new-instance v0, Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;-><init>(Lcom/google/android/speech/grammar/GrammarContactRetriever$1;)V

    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever;->mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/16 v3, 0x1f4

    sget-object v4, Lcom/google/android/speech/grammar/GrammarContactRetriever;->COLS:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/speech/contacts/ContactRetriever;->getContacts(Landroid/net/Uri;I[Ljava/lang/String;Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;)V

    # getter for: Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;->contactNames:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;->access$200(Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;)Ljava/util/List;

    move-result-object v1

    # getter for: Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;->contactNames:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;->access$200(Lcom/google/android/speech/grammar/GrammarContactRetriever$LowerCaseContactNamesRowHandler;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    return-object v1
.end method
