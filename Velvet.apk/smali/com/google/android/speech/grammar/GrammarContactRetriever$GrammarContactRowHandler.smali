.class Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;
.super Ljava/lang/Object;
.source "GrammarContactRetriever.java"

# interfaces
.implements Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/grammar/GrammarContactRetriever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GrammarContactRowHandler"
.end annotation


# instance fields
.field private final mNow:J

.field private mResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/grammar/GrammarContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;->mResults:Ljava/util/List;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;->mNow:J

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;

    iget-object v0, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;->mResults:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public handleCurrentRow(Landroid/database/Cursor;)V
    .locals 8
    .param p1    # Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;->mResults:Ljava/util/List;

    new-instance v2, Lcom/google/android/speech/grammar/GrammarContact;

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget-wide v4, p0, Lcom/google/android/speech/grammar/GrammarContactRetriever$GrammarContactRowHandler;->mNow:J

    const/4 v6, 0x2

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/speech/grammar/GrammarContact;-><init>(Ljava/lang/String;IJ)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    const-string v1, "GrammarContactRetriever"

    const-string v2, "Provider returned null display name."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
