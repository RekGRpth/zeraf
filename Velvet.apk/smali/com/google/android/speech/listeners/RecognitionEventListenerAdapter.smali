.class public Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.super Ljava/lang/Object;
.source "RecognitionEventListenerAdapter.java"

# interfaces
.implements Lcom/google/android/speech/listeners/RecognitionEventListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBeginningOfSpeech()V
    .locals 0

    return-void
.end method

.method public onDone()V
    .locals 0

    return-void
.end method

.method public onEndOfSpeech()V
    .locals 0

    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    return-void
.end method

.method public onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    return-void
.end method

.method public onMediaDataResult([B)V
    .locals 0
    .param p1    # [B

    return-void
.end method

.method public onMusicDetected()V
    .locals 0

    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 0

    return-void
.end method

.method public onPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V
    .locals 0
    .param p1    # Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    return-void
.end method

.method public onRecognitionCancelled()V
    .locals 0

    return-void
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 0
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    return-void
.end method

.method public onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V
    .locals 0
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    return-void
.end method
