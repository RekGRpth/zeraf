.class public Lcom/google/android/speech/engine/NetworkRecognitionEngine;
.super Ljava/lang/Object;
.source "NetworkRecognitionEngine.java"

# interfaces
.implements Lcom/google/android/speech/engine/RecognitionEngine;
.implements Lcom/google/android/speech/engine/RetryCallback$Retrier;


# instance fields
.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mFallback:Lcom/google/android/speech/network/S3ConnectionFactory;

.field private mInputFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;

.field private mOriginalCallback:Lcom/google/android/speech/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation
.end field

.field private final mPrimary:Lcom/google/android/speech/network/S3ConnectionFactory;

.field private final mRequestProducerFactory:Lcom/google/android/speech/network/producers/RequestProducerFactory;

.field private mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

.field private mRetryFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private final mRetryLock:Ljava/lang/Object;

.field private final mRetryPolicy:Lcom/google/android/speech/engine/RetryPolicy;

.field private mRunner:Lcom/google/android/speech/network/NetworkRecognitionRunner;

.field private final mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/engine/RetryPolicy;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/network/producers/RequestProducerFactory;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/network/S3ConnectionFactory;
    .param p2    # Lcom/google/android/speech/network/S3ConnectionFactory;
    .param p3    # Lcom/google/android/speech/engine/RetryPolicy;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Lcom/google/android/speech/network/producers/RequestProducerFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mPrimary:Lcom/google/android/speech/network/S3ConnectionFactory;

    iput-object p2, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mFallback:Lcom/google/android/speech/network/S3ConnectionFactory;

    iput-object p3, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryPolicy:Lcom/google/android/speech/engine/RetryPolicy;

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iput-object p5, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRequestProducerFactory:Lcom/google/android/speech/network/producers/RequestProducerFactory;

    return-void
.end method

.method private maybeRefreshRecognizerParams(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    instance-of v0, p1, Lcom/google/android/speech/exception/AuthFailureException;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRequestProducerFactory:Lcom/google/android/speech/network/producers/RequestProducerFactory;

    invoke-interface {v1}, Lcom/google/android/speech/network/producers/RequestProducerFactory;->refresh()V

    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iput-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

    iput-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mOriginalCallback:Lcom/google/android/speech/callback/Callback;

    iput-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mInputFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRunner:Lcom/google/android/speech/network/NetworkRecognitionRunner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRunner:Lcom/google/android/speech/network/NetworkRecognitionRunner;

    invoke-virtual {v0}, Lcom/google/android/speech/network/NetworkRecognitionRunner;->close()V

    iput-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRunner:Lcom/google/android/speech/network/NetworkRecognitionRunner;

    :cond_0
    iget-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryFuture:Ljava/util/concurrent/Future;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryFuture:Ljava/util/concurrent/Future;

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method createAndStartRecognitionRunner(Lcom/google/android/speech/network/S3ConnectionFactory;)V
    .locals 6
    .param p1    # Lcom/google/android/speech/network/S3ConnectionFactory;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mInputFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;

    invoke-interface {v2}, Lcom/google/android/speech/audio/AudioInputStreamFactory;->createInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/speech/network/NetworkRecognitionRunner;

    iget-object v3, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

    sget-object v4, Lcom/google/android/speech/engine/LoggingEventListener;->INSTANCE:Lcom/google/android/speech/engine/LoggingEventListener;

    iget-object v5, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRequestProducerFactory:Lcom/google/android/speech/network/producers/RequestProducerFactory;

    invoke-interface {v5, v1}, Lcom/google/android/speech/network/producers/RequestProducerFactory;->newRequestProducer(Ljava/io/InputStream;)Lcom/google/android/speech/network/producers/S3RequestProducer;

    move-result-object v5

    invoke-direct {v2, v3, v4, p1, v5}, Lcom/google/android/speech/network/NetworkRecognitionRunner;-><init>(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/network/NetworkEventListener;Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/network/producers/S3RequestProducer;)V

    iput-object v2, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRunner:Lcom/google/android/speech/network/NetworkRecognitionRunner;

    iget-object v2, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRunner:Lcom/google/android/speech/network/NetworkRecognitionRunner;

    invoke-virtual {v2}, Lcom/google/android/speech/network/NetworkRecognitionRunner;->start()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

    new-instance v3, Lcom/google/android/speech/exception/AudioRecognizeException;

    const-string v4, "Unable to create stream"

    invoke-direct {v3, v4, v0}, Lcom/google/android/speech/exception/AudioRecognizeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2, v3}, Lcom/google/android/speech/engine/RetryCallback;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0
.end method

.method retry(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/speech/engine/RetryCallback;

    iget-object v2, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mOriginalCallback:Lcom/google/android/speech/callback/Callback;

    iget-object v3, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryPolicy:Lcom/google/android/speech/engine/RetryPolicy;

    invoke-direct {v1, v2, v3, p0}, Lcom/google/android/speech/engine/RetryCallback;-><init>(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/engine/RetryPolicy;Lcom/google/android/speech/engine/RetryCallback$Retrier;)V

    iput-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

    invoke-direct {p0, p1}, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->maybeRefreshRecognizerParams(Lcom/google/android/speech/exception/RecognizeException;)V

    iget-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRunner:Lcom/google/android/speech/network/NetworkRecognitionRunner;

    invoke-virtual {v1}, Lcom/google/android/speech/network/NetworkRecognitionRunner;->close()V

    iget-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mFallback:Lcom/google/android/speech/network/S3ConnectionFactory;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mFallback:Lcom/google/android/speech/network/S3ConnectionFactory;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->createAndStartRecognitionRunner(Lcom/google/android/speech/network/S3ConnectionFactory;)V

    iget-object v2, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mPrimary:Lcom/google/android/speech/network/S3ConnectionFactory;

    goto :goto_1
.end method

.method public scheduleRetry(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryFuture:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/speech/engine/NetworkRecognitionEngine$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/speech/engine/NetworkRecognitionEngine$1;-><init>(Lcom/google/android/speech/engine/NetworkRecognitionEngine;Lcom/google/android/speech/exception/RecognizeException;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryFuture:Ljava/util/concurrent/Future;

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    const-string v0, "NetworkRecognitionEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Discarding retry request (already active) for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/speech/exception/RecognizeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startRecognition(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/audio/EndpointerListener;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .param p3    # Lcom/google/android/speech/params/RecognizerParams;
    .param p4    # Lcom/google/android/speech/audio/EndpointerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/audio/AudioInputStreamFactory;",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/params/RecognizerParams;",
            "Lcom/google/android/speech/audio/EndpointerListener;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mSameThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryPolicy:Lcom/google/android/speech/engine/RetryPolicy;

    invoke-interface {v0}, Lcom/google/android/speech/engine/RetryPolicy;->reset()V

    iput-object p2, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mOriginalCallback:Lcom/google/android/speech/callback/Callback;

    new-instance v0, Lcom/google/android/speech/engine/RetryCallback;

    iget-object v1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryPolicy:Lcom/google/android/speech/engine/RetryPolicy;

    invoke-direct {v0, p2, v1, p0}, Lcom/google/android/speech/engine/RetryCallback;-><init>(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/engine/RetryPolicy;Lcom/google/android/speech/engine/RetryCallback$Retrier;)V

    iput-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRetryCallback:Lcom/google/android/speech/engine/RetryCallback;

    iput-object p1, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mInputFactory:Lcom/google/android/speech/audio/AudioInputStreamFactory;

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mRequestProducerFactory:Lcom/google/android/speech/network/producers/RequestProducerFactory;

    invoke-interface {v0, p3}, Lcom/google/android/speech/network/producers/RequestProducerFactory;->init(Lcom/google/android/speech/params/RecognizerParams;)V

    iget-object v0, p0, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->mPrimary:Lcom/google/android/speech/network/S3ConnectionFactory;

    invoke-virtual {p0, v0}, Lcom/google/android/speech/engine/NetworkRecognitionEngine;->createAndStartRecognitionRunner(Lcom/google/android/speech/network/S3ConnectionFactory;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
