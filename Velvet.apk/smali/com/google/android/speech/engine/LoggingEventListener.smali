.class final Lcom/google/android/speech/engine/LoggingEventListener;
.super Ljava/lang/Object;
.source "LoggingEventListener.java"

# interfaces
.implements Lcom/google/android/speech/network/NetworkEventListener;


# static fields
.field static final INSTANCE:Lcom/google/android/speech/engine/LoggingEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/speech/engine/LoggingEventListener;

    invoke-direct {v0}, Lcom/google/android/speech/engine/LoggingEventListener;-><init>()V

    sput-object v0, Lcom/google/android/speech/engine/LoggingEventListener;->INSTANCE:Lcom/google/android/speech/engine/LoggingEventListener;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionFinished()V
    .locals 1

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method public onConnectionStarted()V
    .locals 1

    const/4 v0, 0x7

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method public onDataComplete()V
    .locals 1

    const/16 v0, 0x17

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method public onDataReceived()V
    .locals 0

    return-void
.end method

.method public onDataSent()V
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(I)V

    return-void
.end method

.method public onError()V
    .locals 0

    return-void
.end method
