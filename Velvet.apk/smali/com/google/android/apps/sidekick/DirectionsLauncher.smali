.class public interface abstract Lcom/google/android/apps/sidekick/DirectionsLauncher;
.super Ljava/lang/Object;
.source "DirectionsLauncher.java"


# virtual methods
.method public abstract checkNavigationAvailability(Lcom/google/android/apps/sidekick/TravelReport;)Z
.end method

.method public abstract checkNavigationAvailability(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Z
.end method

.method public abstract getDrivingLauncherIntent(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/util/List;I)Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            ">;I)",
            "Landroid/content/Intent;"
        }
    .end annotation
.end method

.method public abstract start(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/util/List;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            ">;I)V"
        }
    .end annotation
.end method
