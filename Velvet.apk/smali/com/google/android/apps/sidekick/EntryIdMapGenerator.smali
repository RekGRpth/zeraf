.class public Lcom/google/android/apps/sidekick/EntryIdMapGenerator;
.super Lcom/google/android/apps/sidekick/EntryTreeVisitor;
.source "EntryIdMapGenerator.java"


# instance fields
.field private final mEntryKeys:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;-><init>()V

    invoke-static {}, Lcom/google/common/collect/HashBiMap;->create()Lcom/google/common/collect/HashBiMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->mEntryKeys:Lcom/google/common/collect/BiMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/common/collect/BiMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;-><init>()V

    if-nez p1, :cond_0

    invoke-static {}, Lcom/google/common/collect/HashBiMap;->create()Lcom/google/common/collect/HashBiMap;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->mEntryKeys:Lcom/google/common/collect/BiMap;

    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/HashBiMap;->create(Ljava/util/Map;)Lcom/google/common/collect/HashBiMap;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getEntryKeyMap()Lcom/google/common/collect/BiMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->isVisited()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->mEntryKeys:Lcom/google/common/collect/BiMap;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableBiMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableBiMap;

    move-result-object v0

    return-object v0
.end method

.method protected process(Lcom/google/android/apps/sidekick/ProtoKey;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 2
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->mEntryKeys:Lcom/google/common/collect/BiMap;

    invoke-interface {v0}, Lcom/google/common/collect/BiMap;->inverse()Lcom/google/common/collect/BiMap;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/common/collect/BiMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->mEntryKeys:Lcom/google/common/collect/BiMap;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method protected process(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->hasGroupEntry()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v1, v0}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/sidekick/EntryIdMapGenerator;->process(Lcom/google/android/apps/sidekick/ProtoKey;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    :cond_0
    return-void
.end method
