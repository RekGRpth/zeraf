.class public Lcom/google/android/apps/sidekick/StockListEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "StockListEntryAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getStockQuoteListEntry()Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/StockListEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/StockListEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/View;
    .param p4    # I
    .param p5    # I

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->addStocks(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/StockListEntryAdapter;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->launchDetails(Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/StockListEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->updateStockInSettings(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;Z)V

    return-void
.end method

.method private addBottomDivider(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const v1, 0x7f100253

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->setShowDividers(I)V

    return-void
.end method

.method private addStocks(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/View;
    .param p4    # I
    .param p5    # I

    const v4, 0x7f100253

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TableLayout;

    move v0, p4

    :goto_0
    if-ge v0, p5, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v4, v0}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntry(I)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    move-result-object v2

    const v4, 0x7f0400bf

    const/4 v5, 0x0

    invoke-virtual {p2, v4, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v4, 0x7f10024c

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1, v2, v1}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->setStockValues(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/view/View;)V

    invoke-virtual {v3, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static getColorForPriceVariation(Landroid/content/Context;D)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # D

    const-wide/16 v1, 0x0

    cmpl-double v1, p1, v1

    if-ltz v1, :cond_0

    const v0, 0x7f09003c

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    :cond_0
    const v0, 0x7f09003b

    goto :goto_0
.end method

.method private getStockChartUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Ljava/lang/String;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$StockQuote;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0078

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v3, v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0077

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v2, v4

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v4, 0xf0

    if-eq v1, v4, :cond_0

    const/16 v4, 0x140

    if-eq v1, v4, :cond_0

    const/16 v4, 0x1e0

    if-ne v1, v4, :cond_1

    :cond_0
    const/4 v0, 0x2

    div-int/lit8 v3, v3, 0x2

    div-int/lit8 v2, v2, 0x2

    :cond_1
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getChartUrl()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private getSubtitle(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$StockQuote;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastUpdateSeconds()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    const v2, 0x7f0d0227

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getExchange()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/16 v5, 0x11

    invoke-static {p1, v0, v1, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private launchDetails(Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0d0228

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p2, v0, v2}, Lcom/google/android/apps/sidekick/TgPresenterAccessor;->startWebSearch(Landroid/content/Context;Ljava/lang/String;Landroid/location/Location;)Z

    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->logDetailsInteractionWithLabel(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private multiStockView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v2, 0x7f0400c1

    const/4 v3, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getLastUpdateTimeMillis(Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;)J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v12, v13, v2, v3}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v14

    const v2, 0x7f100031

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v14, :cond_1

    const v3, 0x7f0d0224

    :goto_0
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const v2, 0x7f10024f

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    const v2, 0x7f0d0226

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v6, 0x11

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntryCount()I

    move-result v2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v7

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->addStocks(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntryCount()I

    move-result v2

    const/4 v3, 0x5

    if-le v2, v3, :cond_0

    const v2, 0x7f10015c

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    new-instance v6, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;

    move-object/from16 v7, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Landroid/widget/Button;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;)V

    invoke-virtual {v8, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/widget/Button;->setVisibility(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->addBottomDivider(Landroid/view/View;)V

    :cond_0
    return-object v5

    :cond_1
    const v3, 0x7f0d0229

    goto :goto_0
.end method

.method private setStockValues(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .param p3    # Landroid/view/View;

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasLastPrice()Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f100249

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const-string v3, "%.2f"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastPrice()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariation()Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f10024d

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v3, "%.2f"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariation()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariation()F

    move-result v3

    float-to-double v3, v3

    invoke-static {p1, v3, v4}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getColorForPriceVariation(Landroid/content/Context;D)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPriceVariationPercent()Z

    move-result v3

    if-eqz v3, :cond_2

    const v3, 0x7f10024e

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "%.2f%%"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariationPercent()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPriceVariationPercent()F

    move-result v3

    float-to-double v3, v3

    invoke-static {p1, v3, v4}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getColorForPriceVariation(Landroid/content/Context;D)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    new-instance v3, Lcom/google/android/apps/sidekick/StockListEntryAdapter$4;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/apps/sidekick/StockListEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/content/Context;)V

    invoke-virtual {p3, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private singleStockView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Landroid/view/View;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$StockQuote;

    const/4 v7, 0x0

    const v5, 0x7f0400c0

    invoke-virtual {p2, v5, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v5, 0x7f100031

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {p0, p1, p4}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getTitle(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f10024f

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getSubtitle(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f100250

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p4, v1}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->setStockValues(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/view/View;)V

    invoke-virtual {p4}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasChartUrl()Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f100251

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/WebImageView;

    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getStockChartUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    new-instance v5, Lcom/google/android/apps/sidekick/StockListEntryAdapter$3;

    invoke-direct {v5, p0, p4, p1}, Lcom/google/android/apps/sidekick/StockListEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v7}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    :cond_0
    return-object v0
.end method

.method private updateStockInSettings(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$StockQuote;
    .param p3    # Z

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->hasPrimaryKey()Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Stock to update does not have primary key: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->newNowConfigurationPreferences(Landroid/content/SharedPreferences;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v3

    const v5, 0x7f0d006c

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getPrimaryKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->setDeleted(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->editConfiguration()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v5

    invoke-virtual {v5, v1, v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->updateMessage(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;->apply()V

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Stock to update was not found in settings: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 4

    new-instance v1, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;-><init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v2

    const v3, 0x7f0d02f9

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/inject/ActivityHelper;I)V

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;)V

    return-object v2
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d022e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0229

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d022d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntryCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7f0d02d4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d02d3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLastUpdateTimeMillis(Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;)J
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    const-wide/16 v1, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/geo/sidekick/Sidekick$StockQuote;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastUpdateSeconds()J

    move-result-wide v4

    cmp-long v4, v4, v1

    if-lez v4, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastUpdateSeconds()J

    move-result-wide v1

    goto :goto_0

    :cond_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    return-wide v4
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getTitle(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$StockQuote;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getLastUpdateSeconds()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0d0225

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$StockQuote;->getSymbol()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntryCount()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v2, v4}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntry(I)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    move-result-object v2

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->singleStockView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$StockQuote;)Landroid/view/View;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->hasDisclaimerUrl()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f100252

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v2, Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->multiStockView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntryCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7f10005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntryCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntry(I)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    move-result-object v0

    const-string v1, "STOCK_TITLE"

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->launchDetails(Lcom/google/geo/sidekick/Sidekick$StockQuote;Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
