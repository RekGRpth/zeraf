.class public Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;
.super Landroid/app/DialogFragment;
.source "WorkerFragmentSpinnerDialog.java"


# instance fields
.field private mWorkerFragmentTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static hide(Landroid/app/FragmentManager;)V
    .locals 2
    .param p0    # Landroid/app/FragmentManager;

    const-string v1, "spinner_dialog"

    invoke-virtual {p0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->dismissAllowingStateLoss()V

    :cond_0
    return-void
.end method

.method static show(Landroid/app/FragmentManager;Landroid/app/Fragment;)V
    .locals 4
    .param p0    # Landroid/app/FragmentManager;
    .param p1    # Landroid/app/Fragment;

    const-string v2, "spinner_dialog"

    invoke-virtual {p0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "worker_tag_key"

    invoke-virtual {p1}, Landroid/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->setArguments(Landroid/os/Bundle;)V

    const-string v2, "spinner_dialog"

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->dismiss()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->mWorkerFragmentTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "worker_tag_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->mWorkerFragmentTag:Ljava/lang/String;

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0d036f

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    return-object v0
.end method
