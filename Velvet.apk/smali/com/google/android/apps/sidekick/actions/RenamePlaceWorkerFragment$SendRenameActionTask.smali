.class Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;
.super Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;
.source "RenamePlaceWorkerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SendRenameActionTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 7
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .param p6    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p6

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V

    return-void
.end method


# virtual methods
.method protected onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d00e6

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->handleResponse(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->access$000(Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;

    # invokes: Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->handleResponse(Z)V
    invoke-static {v0, v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->access$000(Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;Z)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    return-void
.end method
