.class Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;
.super Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;
.source "EditHomeWorkWorkerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendEditActionTask"
.end annotation


# instance fields
.field private final mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 6
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    move-object v0, p0

    move-object v1, p5

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    iput-object p4, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    return-void
.end method


# virtual methods
.method protected buildExecutedAction(Lcom/google/geo/sidekick/Sidekick$Action;J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->buildExecutedAction(Lcom/google/geo/sidekick/Sidekick$Action;J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setEditedPlaceLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->buildExecutedAction(Lcom/google/geo/sidekick/Sidekick$Action;J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v0

    goto :goto_0
.end method

.method protected onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    const v0, 0x7f0d00cb

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasActionsResponse()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getActionsResponse()Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$ActionsResponse;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-super {p0, v1}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getActionsResponse()Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ActionsResponse;->getError()I

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_0

    const v0, 0x7f0d01ed

    :cond_0
    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    # invokes: Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->handleResponse(Z)V
    invoke-static {v0, v5}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->access$000(Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;Z)V

    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d01ee

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    # invokes: Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->handleResponse(Z)V
    invoke-static {v0, v4}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->access$000(Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;Z)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;

    # invokes: Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->handleResponse(Z)V
    invoke-static {v0, v5}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;->access$000(Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment;Z)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkWorkerFragment$SendEditActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    return-void
.end method
