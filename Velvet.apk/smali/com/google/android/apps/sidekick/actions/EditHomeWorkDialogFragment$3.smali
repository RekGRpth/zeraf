.class Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$3;
.super Ljava/lang/Object;
.source "EditHomeWorkDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->createEditDialog(ILandroid/view/View;Landroid/widget/EditText;Landroid/widget/EditText;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;

.field final synthetic val$editAddressText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$3;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$3;->val$editAddressText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$3;->val$editAddressText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$3;->val$editAddressText:Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$3;->val$editAddressText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    return-void
.end method
