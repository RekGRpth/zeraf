.class public Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;
.super Lcom/google/android/apps/sidekick/actions/EntryActionBase;
.source "DismissNotificationAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;
    }
.end annotation


# static fields
.field private static mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, p2, v0}, Lcom/google/android/apps/sidekick/actions/EntryActionBase;-><init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;)V

    sput-object p4, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;-><init>(Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
