.class Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;
.super Ljava/lang/Object;
.source "EditHomeWorkDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->createEditDialog(ILandroid/view/View;Landroid/widget/EditText;Landroid/widget/EditText;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;

.field final synthetic val$editAddressText:Landroid/widget/EditText;

.field final synthetic val$editLabelText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->val$editAddressText:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->val$editLabelText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->val$editAddressText:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->hideSoftKeyboard(Landroid/view/View;)V

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->val$editAddressText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$Location;->setAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->val$editLabelText:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->val$editLabelText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->val$editLabelText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$Location;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment$2;->this$0:Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;

    # invokes: Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->startServerAction(Lcom/google/geo/sidekick/Sidekick$Location;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;->access$000(Lcom/google/android/apps/sidekick/actions/EditHomeWorkDialogFragment;Lcom/google/geo/sidekick/Sidekick$Location;)V

    return-void
.end method
