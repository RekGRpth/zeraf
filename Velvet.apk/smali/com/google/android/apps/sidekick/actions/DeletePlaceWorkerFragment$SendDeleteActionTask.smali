.class Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;
.super Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;
.source "DeletePlaceWorkerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SendDeleteActionTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 6
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    move-object v0, p0

    move-object v1, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    return-void
.end method


# virtual methods
.method protected onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    # invokes: Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->handleResponse(Z)V
    invoke-static {v1, v0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->access$000(Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    return-void
.end method
