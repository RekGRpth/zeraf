.class Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;
.super Lcom/google/android/apps/sidekick/actions/RecordActionTask;
.source "DismissNotificationAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SendDismissActionTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;

    # getter for: Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;
    invoke-static {}, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->access$000()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->mContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p1, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    return-void
.end method


# virtual methods
.method protected onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->onPostExecute(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->success(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;->this$0:Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->failure()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction$SendDismissActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    return-void
.end method
