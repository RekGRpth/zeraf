.class Lcom/google/android/apps/sidekick/CalendarEntryAdapter$3;
.super Ljava/lang/Object;
.source "CalendarEntryAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->access$300(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->markEventAsDismissed(J)Z

    return-void
.end method
