.class Lcom/google/android/apps/sidekick/ReminderEntryAdapter$1;
.super Ljava/lang/Object;
.source "ReminderEntryAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

.field final synthetic val$appContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/ReminderEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter$1;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter$1;->val$appContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter$1;->val$appContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getDeleteNotificationIntent(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
