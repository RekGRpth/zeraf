.class public Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "MovieTicketEntryAdapter.java"


# instance fields
.field private final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

.field private final mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p3    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p4    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMovieTicketEntry()Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;)Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    return-object v0
.end method

.method private addGmailButton(Landroid/content/Context;Landroid/view/View;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasGmailReference()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getGmailReference()Lcom/google/geo/sidekick/Sidekick$GmailReference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->hasEmailUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getGmailReference()Lcom/google/geo/sidekick/Sidekick$GmailReference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->getEmailUrl()Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f10002f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    invoke-virtual {v11, v4}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getMovie()Lcom/google/geo/sidekick/Sidekick$Movie;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    const-string v6, "MOVIE_TICKET_EMAIL"

    const-string v7, "mail"

    sget-object v8, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->GMAIL_URL_PREFIXES:[Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v10

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    invoke-virtual {v11, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method private getShowtimeText(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasShowtimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getShowtimeSeconds()J

    move-result-wide v3

    const-wide/16 v7, 0x3e8

    mul-long v1, v3, v7

    const/4 v5, 0x1

    move-object v0, p1

    move-wide v3, v1

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v6

    :cond_0
    return-object v6
.end method

.method private maybeAddNavigateButton(Landroid/content/Context;Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasTheater()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getTheater()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->checkNavigationAvailability(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Z

    move-result v5

    if-eqz v5, :cond_2

    const v5, 0x7f10002c

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getTheater()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v3

    const/4 v0, 0x0

    const/4 v2, 0x0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelTimeWithoutDelayInMinutes()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p1, v3, v9}, Lcom/google/android/apps/sidekick/TimeUtilities;->getEtaString(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Z)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary()Z

    move-result v5

    if-eqz v5, :cond_3

    const v5, 0x7f0d010a

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v8

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    :cond_0
    :goto_0
    if-nez v2, :cond_1

    const v5, 0x7f0d00db

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    :cond_1
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;

    invoke-direct {v5, p0, v3, v4}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Lcom/google/geo/sidekick/Sidekick$Location;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-void

    :cond_3
    const v5, 0x7f0d0109

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v0, v6, v8

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    goto :goto_0
.end method

.method private populateMovieInfo(Landroid/content/Context;Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v6, 0x0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getMovie()Lcom/google/geo/sidekick/Sidekick$Movie;

    move-result-object v1

    const v5, 0x7f100031

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImage()Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f10016f

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Movie;->getImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    const v5, 0x7f100170

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTrailerUrl()Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f100171

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    new-instance v5, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;

    invoke-direct {v5, p0, p1, v1}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Movie;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method private populateShowtimeInfo(Landroid/content/Context;Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasTheater()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getTheater()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getTheater()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v3

    const v4, 0x7f10017c

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->getShowtimeText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const v4, 0x7f0d021e

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f100179

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private showBarCodeCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v9, 0x0

    const v6, 0x7f040075

    invoke-virtual {p2, v6, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v6, 0x7f100031

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0d021f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getMovie()Lcom/google/geo/sidekick/Sidekick$Movie;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f100178

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getConfirmationNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->getShowtimeText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const v6, 0x7f100179

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasNumberOfTickets()Z

    move-result v6

    if-eqz v6, :cond_1

    const v6, 0x7f10017b

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getNumberOfTickets()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasTheater()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getTheater()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v6

    if-eqz v6, :cond_2

    const v6, 0x7f10017a

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getTheater()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasBarcode()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasUrl()Z

    move-result v6

    if-eqz v6, :cond_3

    const v6, 0x7f100042

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/CrossfadingWebImageView;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setImageUri(Landroid/net/Uri;)V

    invoke-virtual {v2, v9}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setVisibility(I)V

    :cond_3
    return-object v0
.end method

.method private showMovieCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f040076

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->populateMovieInfo(Landroid/content/Context;Landroid/view/View;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->populateShowtimeInfo(Landroid/content/Context;Landroid/view/View;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02be

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/GmailEventsCardSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->mMovieEntry:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasConfirmationNumber()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->showBarCodeCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->addGmailButton(Landroid/content/Context;Landroid/view/View;)V

    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->showMovieCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->maybeAddNavigateButton(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
