.class public Lcom/google/android/apps/sidekick/VelvetNetworkClient;
.super Ljava/lang/Object;
.source "VelvetNetworkClient.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/NetworkClient;


# instance fields
.field private final mAppVersionName:Ljava/lang/String;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field private mDebugBadConnection:Z

.field private final mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

.field private mDebugResponse:Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

.field private final mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

.field private final mSessionManager:Lcom/google/android/apps/sidekick/inject/SessionManager;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/debug/DebugFeatures;Ljava/lang/String;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/apps/sidekick/SensorSignalsOracle;Landroid/net/ConnectivityManager;Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;Lcom/google/android/apps/sidekick/inject/SessionManager;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/CoreSearchServices;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/debug/DebugFeatures;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p6    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p7    # Lcom/google/android/apps/sidekick/SensorSignalsOracle;
    .param p8    # Landroid/net/ConnectivityManager;
    .param p9    # Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;
    .param p10    # Lcom/google/android/apps/sidekick/inject/SessionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugResponse:Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugBadConnection:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mAppVersionName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p9, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    iput-object p8, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iput-object p10, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mSessionManager:Lcom/google/android/apps/sidekick/inject/SessionManager;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v1, "oauth2:https://www.googleapis.com/auth/googlenow"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->requireAuthTokenType(Ljava/lang/String;)V

    return-void
.end method

.method private addSensorSignalsToPayload(Lcom/google/geo/sidekick/Sidekick$RequestPayload;Z)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasSensorSignals()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getSensorSignals()Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->buildCurrentSensorSignals(Lcom/google/geo/sidekick/Sidekick$SensorSignals;Z)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setSensorSignals(Lcom/google/geo/sidekick/Sidekick$SensorSignals;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    return-object p1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addStoredExecutedUserActionsToPayload(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mExecutedUserActionStore:Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;->flush()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasActionsQuery()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getActionsQuery()Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    move-result-object v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    invoke-virtual {v0, v2}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;->addExecutedUserAction(Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;)Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    goto :goto_1

    :cond_0
    new-instance v0, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;-><init>()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setActionsQuery(Lcom/google/geo/sidekick/Sidekick$ActionsQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    :cond_2
    return-object p1
.end method

.method private buildRequest(Lcom/google/geo/sidekick/Sidekick$RequestPayload;J)[B
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .param p2    # J

    iget-object v3, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mSessionManager:Lcom/google/android/apps/sidekick/inject/SessionManager;

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SessionManager;->getSessionKey()Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    move-result-object v2

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;-><init>()V

    iget-object v4, v2, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->key:Ljava/util/UUID;

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setSessionId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    move-result-object v3

    iget-wide v4, v2, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->expirationSeconds:J

    invoke-virtual {v3, v4, v5}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setSessionIdExpirationSeconds(J)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setOsType(I)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    move-result-object v3

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setOsVersion(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mAppVersionName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/geo/sidekick/Sidekick$ClientDescription;->setSidekickAppVersion(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$ClientDescription;

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->setClient(Lcom/google/geo/sidekick/Sidekick$ClientDescription;)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->setPayload(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    div-long v4, p2, v4

    invoke-virtual {v3, v4, v5}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpRequest;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private sendRequestHelper(Lcom/google/geo/sidekick/Sidekick$RequestPayload;ZLandroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 24
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .param p2    # Z
    .param p3    # Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/searchcommon/SearchConfig;->isTheGoogleDeployed()Z

    move-result v21

    if-nez v21, :cond_0

    const-string v21, "Velvet.VelvetNetworkClient"

    const-string v22, "Swallowed network request because tg is not deployed"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x0

    :goto_0
    return-object v21

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v21

    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugBadConnection:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    const-wide/16 v21, 0x7d0

    :try_start_0
    invoke-static/range {v21 .. v22}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    const/16 v21, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugResponse:Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-object/from16 v21, v0

    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugResponse:Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-object/from16 v21, v0

    goto :goto_0

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->isNetworkAvailable()Z

    move-result v21

    if-nez v21, :cond_3

    const-string v21, "Velvet.VelvetNetworkClient"

    const-string v22, "Network connection not availble"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x0

    goto :goto_0

    :cond_3
    if-nez p3, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object p3

    :cond_4
    if-nez p3, :cond_5

    const-string v21, "Velvet.VelvetNetworkClient"

    const-string v22, "Cannot connect to server without account"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x0

    goto :goto_0

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    invoke-direct/range {p0 .. p2}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->addSensorSignalsToPayload(Lcom/google/geo/sidekick/Sidekick$RequestPayload;Z)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object p1

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->addStoredExecutedUserActionsToPayload(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object p1

    new-instance v17, Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;

    const-string v21, "https://android.googleapis.com/tg/fe/request"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->buildRequest(Lcom/google/geo/sidekick/Sidekick$RequestPayload;J)[B

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;->setContent([B)V

    const/4 v8, 0x0

    :goto_2
    const/16 v21, 0x2

    move/from16 v0, v21

    if-ge v8, v0, :cond_b

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-object/from16 v21, v0

    const-string v22, "oauth2:https://www.googleapis.com/auth/googlenow"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->blockingGetAuthTokenForAccount(Ljava/lang/String;Landroid/accounts/Account;)Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    move-result-object v4

    if-nez v4, :cond_6

    const-string v21, "Velvet.VelvetNetworkClient"

    const-string v22, "Failed to get auth token"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_6
    const-string v21, "Authorization"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "OAuth "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v4}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->getToken()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v15

    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    move/from16 v2, v22

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/util/HttpHelper;->rawPost(Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;I)[B

    move-result-object v18

    if-nez v18, :cond_7

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    sub-long v13, v21, v15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-static/range {v18 .. v18}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpResponse;->parseFrom([B)Lcom/google/geo/sidekick/Sidekick$SidekickHttpResponse;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    sub-long v9, v21, v11

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpResponse;->getStatus()I

    move-result v21

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    const-string v6, ""

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpResponse;->hasErrorCode()Z

    move-result v21

    if-eqz v21, :cond_8

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, ": "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpResponse;->getErrorCode()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_8
    const-string v21, "Velvet.VelvetNetworkClient"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Received ERROR from server"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/google/android/velvet/VelvetBackgroundTasks;->maybeStartTasks()V

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SidekickHttpResponse;->getPayload()Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    :try_end_2
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v21

    goto/16 :goto_0

    :catch_0
    move-exception v5

    :try_start_3
    invoke-virtual {v5}, Lcom/google/android/searchcommon/util/HttpHelper$HttpException;->getStatusCode()I

    move-result v21

    const/16 v22, 0x191

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    const-string v21, "Velvet.VelvetNetworkClient"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Authorization exception: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-object/from16 v21, v0

    const-string v22, "oauth2:https://www.googleapis.com/auth/googlenow"

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->invalidateAuthToken(Ljava/lang/String;)V

    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    :cond_a
    throw v5
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v5

    const-string v21, "Velvet.VelvetNetworkClient"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Network error: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_b
    :try_start_4
    const-string v21, "Velvet.VelvetNetworkClient"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Request retries failed: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;->getUrl()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    const/16 v21, 0x0

    goto/16 :goto_0

    :catch_2
    move-exception v21

    goto/16 :goto_1
.end method


# virtual methods
.method public isNetworkAvailable()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    goto :goto_0
.end method

.method public sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->sendRequestHelper(Lcom/google/geo/sidekick/Sidekick$RequestPayload;ZLandroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public sendRequestWithoutLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->sendRequestHelper(Lcom/google/geo/sidekick/Sidekick$RequestPayload;ZLandroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public sendRequestWithoutLocationWithAccount(Lcom/google/geo/sidekick/Sidekick$RequestPayload;Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .param p2    # Landroid/accounts/Account;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->sendRequestHelper(Lcom/google/geo/sidekick/Sidekick$RequestPayload;ZLandroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public setDebugBadConnection(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugBadConnection:Z

    :cond_0
    return-void
.end method

.method public setDebugResponse(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->mDebugResponse:Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    :cond_0
    return-void
.end method
