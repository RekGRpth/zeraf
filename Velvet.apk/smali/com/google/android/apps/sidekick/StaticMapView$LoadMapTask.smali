.class Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;
.super Landroid/os/AsyncTask;
.source "StaticMapView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/StaticMapView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

.field private final mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

.field private final mShowRoute:Z

.field private final mSource:Lcom/google/geo/sidekick/Sidekick$Location;

.field final synthetic this$0:Lcom/google/android/apps/sidekick/StaticMapView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/StaticMapView;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;ZLcom/google/android/apps/sidekick/inject/StaticMapCache;)V
    .locals 0
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .param p4    # Z
    .param p5    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iput-boolean p4, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mShowRoute:Z

    iput-object p5, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # [Ljava/lang/Void;

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;-><init>()V

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->clearPlaceData()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->clearAlternatePlaceData()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    :cond_0
    iget-boolean v4, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mShowRoute:Z

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->clearRoute()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    :cond_1
    new-instance v4, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    # getter for: Lcom/google/android/apps/sidekick/StaticMapView;->mMapHeight:I
    invoke-static {v5}, Lcom/google/android/apps/sidekick/StaticMapView;->access$200(Lcom/google/android/apps/sidekick/StaticMapView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setHeight(I)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    # getter for: Lcom/google/android/apps/sidekick/StaticMapView;->mMapWidth:I
    invoke-static {v5}, Lcom/google/android/apps/sidekick/StaticMapView;->access$100(Lcom/google/android/apps/sidekick/StaticMapView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setWidth(I)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setPlaceEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setStartLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    # getter for: Lcom/google/android/apps/sidekick/StaticMapView;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;
    invoke-static {v4}, Lcom/google/android/apps/sidekick/StaticMapView;->access$300(Lcom/google/android/apps/sidekick/StaticMapView;)Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v4

    new-instance v5, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v5}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v5, v3}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setStaticMapQuery(Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasStaticMapResponse()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getStaticMapResponse()Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;->getMapPng()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/google/protobuf/micro/ByteStringMicro;->size()I

    move-result v6

    invoke-static {v4, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v4

    :goto_1
    return-object v4

    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iget-boolean v3, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->mShowRoute:Z

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/google/android/apps/sidekick/inject/StaticMapCache;->put(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;ZLandroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    # invokes: Lcom/google/android/apps/sidekick/StaticMapView;->setImageBitmapAndFadeIn(Landroid/graphics/Bitmap;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/sidekick/StaticMapView;->access$400(Lcom/google/android/apps/sidekick/StaticMapView;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/StaticMapView$LoadMapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
