.class public Lcom/google/android/apps/sidekick/ChildEntryRemover;
.super Lcom/google/android/apps/sidekick/EntryTreeVisitor;
.source "ChildEntryRemover.java"


# instance fields
.field private final mGroupNodeKey:Lcom/google/android/apps/sidekick/ProtoKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeysToRemove:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/ProtoKey;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ChildEntryRemover;->mGroupNodeKey:Lcom/google/android/apps/sidekick/ProtoKey;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ChildEntryRemover;->mKeysToRemove:Ljava/util/Collection;

    return-void
.end method


# virtual methods
.method protected process(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->hasGroupEntry()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v2, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ChildEntryRemover;->mGroupNodeKey:Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ChildEntryRemover;->mKeysToRemove:Ljava/util/Collection;

    new-instance v4, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v4, v0}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-interface {v3, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method
