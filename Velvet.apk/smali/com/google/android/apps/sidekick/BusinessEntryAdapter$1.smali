.class Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;
.super Ljava/lang/Object;
.source "BusinessEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/BusinessEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/BusinessEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BusinessEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BusinessEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->launchDetails(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BusinessEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BusinessEntryAdapter;

    const-string v3, "DETAILS"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
