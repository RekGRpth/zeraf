.class public Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;
.super Ljava/lang/Object;
.source "SportsEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/SportsEntryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LogoUriWrapper"
.end annotation


# instance fields
.field private final mClipRect:Landroid/graphics/Rect;

.field private final mUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/net/Uri;Landroid/graphics/Rect;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->mUri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->mClipRect:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;)Landroid/graphics/Rect;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->mClipRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public static fromSportEntry(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;)Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;
    .locals 7
    .param p0    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasLogoUrl()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getLogoUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :cond_0
    :goto_0
    new-instance v5, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;

    invoke-direct {v5, v4, v0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;-><init>(Landroid/net/Uri;Landroid/graphics/Rect;)V

    return-object v5

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSpriteUrl()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSpriteUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "//"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "https:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromLeft()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromTop()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getSpriteOffsetFromLeft()I

    move-result v5

    rsub-int/lit8 v1, v5, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getSpriteOffsetFromTop()I

    move-result v5

    rsub-int/lit8 v3, v5, 0x0

    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v5, v1, 0x1e

    add-int/lit8 v6, v3, 0x10

    invoke-direct {v0, v1, v3, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public getClipRect()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->mClipRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->mUri:Landroid/net/Uri;

    return-object v0
.end method
