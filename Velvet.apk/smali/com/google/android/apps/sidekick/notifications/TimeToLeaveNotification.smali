.class public Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;
.source "TimeToLeaveNotification.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    return-void
.end method


# virtual methods
.method public getLoggingName()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->getLoggingName()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_0
    const-string v1, "GmailRestaurantReservation"

    goto :goto_0

    :pswitch_1
    const-string v1, "GmailEventReservation"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 10
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasEventTimeSeconds()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/TravelReport;->getTotalEtaMinutes()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    mul-int/lit8 v5, v6, 0x3c

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventTimeSeconds()J

    move-result-wide v6

    int-to-long v8, v5

    sub-long v3, v6, v8

    new-instance v2, Ljava/util/Date;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v3

    invoke-direct {v2, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    const v6, 0x7f0d00fb

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d00fa

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-static {p1, v3}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported source type for time to leave notification: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->EVENT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    :goto_0
    return-object v1

    :pswitch_0
    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->RESTAURANT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->EVENT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    const v0, 0x7f02011c

    return v0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/notifications/TimeToLeaveNotification;->getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
