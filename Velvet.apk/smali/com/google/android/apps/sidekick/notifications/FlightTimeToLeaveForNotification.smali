.class public Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "FlightTimeToLeaveForNotification.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

.field private final mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

.field private mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->getNotifiedFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-direct {p0, v2}, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->getCommuteSumary(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/TravelReport$Builder;

    const/4 v2, 0x0

    invoke-direct {v0, p2, v2}, Lcom/google/android/apps/sidekick/TravelReport$Builder;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/TravelReport$Builder;->setRegularCommute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/android/apps/sidekick/TravelReport$Builder;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/TravelReport$Builder;->build()Lcom/google/android/apps/sidekick/TravelReport;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->addNaviateAction()V

    return-void
.end method

.method private addNaviateAction()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/TravelReport;->getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureAirport()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/google/android/apps/sidekick/notifications/NavigateAction;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {v2, v3, v4, v5, v1}, Lcom/google/android/apps/sidekick/notifications/NavigateAction;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->addAction(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)V

    goto :goto_0
.end method

.method private getCommuteSumary(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureAirport()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->hasRoute()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getNotifiedFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFlightStatusEntry()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlightList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasNotificationDetails()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v7, 0x1

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasNotificationDetails()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getNotificationDetails()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;->getLeaveByTimeSecondsSinceEpoch()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    invoke-static {p1, v3, v4, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;->getArriveMinutesBefore()I

    move-result v3

    invoke-static {p1, v3}, Lcom/google/android/apps/sidekick/TimeUtilities;->getDurationString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0d00fc

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    aput-object v0, v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d00fa

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "%s %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineCode()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;->mNotifiedFlight:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->FLIGHT_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    return-object v0
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    const v0, 0x7f02011c

    return v0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNotificationType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
