.class final Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;
.super Lcom/google/android/apps/sidekick/EntryTreeVisitor;
.source "NotificationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/notifications/NotificationStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NotificationFinder"
.end annotation


# instance fields
.field private final entriesWithNotifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;->entriesWithNotifications:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/notifications/NotificationStore$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/notifications/NotificationStore$1;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;->entriesWithNotifications:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected process(Lcom/google/android/apps/sidekick/ProtoKey;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 4
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasTriggerCondition()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getConditionCount()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->hasTimeSeconds()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_0

    # invokes: Lcom/google/android/apps/sidekick/notifications/NotificationStore;->hasLocationTriggerConditions(Lcom/google/geo/sidekick/Sidekick$Notification;)Z
    invoke-static {v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->access$300(Lcom/google/geo/sidekick/Sidekick$Notification;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationStore$NotificationFinder;->entriesWithNotifications:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
