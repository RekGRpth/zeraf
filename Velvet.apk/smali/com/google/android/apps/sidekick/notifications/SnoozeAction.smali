.class public Lcom/google/android/apps/sidekick/notifications/SnoozeAction;
.super Ljava/lang/Object;
.source "SnoozeAction.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/notifications/NotificationAction;


# instance fields
.field private final mEntries:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/SnoozeAction;->mEntries:Ljava/util/Collection;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getActionIcon()I
    .locals 1

    const v0, 0x7f020119

    return v0
.end method

.method public getActionString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02f8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCallbackIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/SnoozeAction;->mEntries:Ljava/util/Collection;

    invoke-static {p1, v0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getNotificationSnoozeIntent(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getCallbackType()Ljava/lang/String;
    .locals 1

    const-string v0, "service"

    return-object v0
.end method

.method public getLogString()Ljava/lang/String;
    .locals 1

    const-string v0, "SNOOZE"

    return-object v0
.end method

.method public isActive()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
