.class public Lcom/google/android/apps/sidekick/notifications/GenericLowPriorityNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "GenericLowPriorityNotification.java"


# instance fields
.field private final mIcon:I


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;I)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    iput p2, p0, Lcom/google/android/apps/sidekick/notifications/GenericLowPriorityNotification;->mIcon:I

    return-void
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/GenericLowPriorityNotification;->getDefaultNotificationText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    return-object v0
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/notifications/GenericLowPriorityNotification;->mIcon:I

    return v0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method
