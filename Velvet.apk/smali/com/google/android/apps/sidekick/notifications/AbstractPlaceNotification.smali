.class public abstract Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "AbstractPlaceNotification.java"


# instance fields
.field protected final mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field protected final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

.field protected final mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

.field protected final mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteCount()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/sidekick/TravelReport$Builder;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/sidekick/TravelReport$Builder;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRoute(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/sidekick/TravelReport$Builder;->setRegularCommute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/android/apps/sidekick/TravelReport$Builder;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/TravelReport$Builder;->build()Lcom/google/android/apps/sidekick/TravelReport;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    :goto_1
    iput-object p3, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->addNavigateAction()V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    goto :goto_1
.end method

.method private addNavigateAction()V
    .locals 5

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/TravelReport;->getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/notifications/NavigateAction;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {v1, v3, v0, v4, v2}, Lcom/google/android/apps/sidekick/notifications/NavigateAction;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/notifications/AbstractPlaceNotification;->addAction(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)V

    return-void
.end method
