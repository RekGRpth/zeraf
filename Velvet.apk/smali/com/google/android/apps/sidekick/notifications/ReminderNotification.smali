.class public Lcom/google/android/apps/sidekick/notifications/ReminderNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "ReminderNotification.java"


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->mClock:Lcom/google/android/searchcommon/util/Clock;

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/SnoozeAction;

    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/notifications/SnoozeAction;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->addAction(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)V

    return-void
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->getTriggerMessage(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$ReminderEntry;J)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getReminderMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->isLowPriorityNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->REMINDER_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    goto :goto_0
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->hasTriggerTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f020177

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020176

    goto :goto_0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/ReminderNotification;->mReminder:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getReminderMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
