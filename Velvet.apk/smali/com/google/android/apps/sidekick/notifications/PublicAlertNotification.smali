.class public Lcom/google/android/apps/sidekick/notifications/PublicAlertNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "PublicAlertNotification.java"


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    return-void
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/PublicAlertNotification;->getDefaultNotificationText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/PublicAlertNotification;->isLowPriorityNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->PUBLIC_ALERT_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    goto :goto_0
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    const v0, 0x7f020173

    return v0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/PublicAlertNotification;->getDefaultNotificationText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
