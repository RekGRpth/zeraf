.class public Lcom/google/android/apps/sidekick/notifications/SportsNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "SportsNotification.java"


# instance fields
.field private final mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getSportScoreEntry()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    return-void
.end method


# virtual methods
.method getFormattedStartTime(Landroid/content/Context;)Ljava/lang/String;
    .locals 12
    .param p1    # Landroid/content/Context;

    const/4 v11, 0x0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeSeconds()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long v2, v7, v9

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "z"

    invoke-direct {v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    const v7, 0x7f0d0141

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v11

    const/4 v9, 0x1

    aput-object v5, v8, v9

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    :goto_0
    return-object v7

    :cond_0
    invoke-static {p1, v2, v3, v11}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatusCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :pswitch_0
    const v2, 0x7f0d0142

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    const v2, 0x7f0d0158

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeSeconds()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v0, v2, v4

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0d0156

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->getFormattedStartTime(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const v2, 0x7f0d0157

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->getFormattedStartTime(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x4

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3, v6}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3, v7}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasScore()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasScore()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSport()I

    move-result v3

    if-ne v3, v4, :cond_0

    const v0, 0x7f0d0155

    :goto_0
    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x3

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    const v0, 0x7f0d0154

    goto :goto_0

    :cond_1
    const v3, 0x7f0d0153

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    return-object v0
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/SportsNotification;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSport()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f020179

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f02016d

    goto :goto_0

    :pswitch_1
    const v0, 0x7f02016e

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020171

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020172

    goto :goto_0

    :pswitch_4
    const v0, 0x7f020178

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method
