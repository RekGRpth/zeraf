.class public Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;
.super Ljava/lang/Object;
.source "NowNotificationManagerImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mNotificationManager:Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;

.field private final mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private final mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/google/UserInteractionLogger;Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p4    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p5    # Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .param p6    # Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;
    .param p7    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p8    # Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mNotificationManager:Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p8, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    return-void
.end method

.method private addAction(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;Lcom/google/android/apps/sidekick/notifications/NotificationAction;I)V
    .locals 6

    invoke-interface {p3}, Lcom/google/android/apps/sidekick/notifications/NotificationAction;->getActionIcon()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-interface {p3, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationAction;->getActionString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/apps/sidekick/NotificationReceiver;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.google.android.apps.sidekick.NOTIFICATION_CALLBACK_ACTION"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notification_action://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v3, "notificationEntriesKey"

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getEntries()Ljava/util/Collection;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/sidekick/ProtoUtils;->putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v3, "notificationIdKey"

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v3, "notificationLogActionKey"

    invoke-interface {p3}, Lcom/google/android/apps/sidekick/notifications/NotificationAction;->getLogString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "notificationLoggingNameKey"

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getLoggingName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "notification_callback"

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-interface {p3, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationAction;->getCallbackIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "callback_type"

    invoke-interface {p3}, Lcom/google/android/apps/sidekick/notifications/NotificationAction;->getCallbackType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-interface {v3, v4, v2, v5}, Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;->getBroadcast(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    return-void
.end method

.method private addDeletePendingIntent(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)V
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.NOTIFICATION_DISMISS_ACTION"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/apps/sidekick/NotificationReceiver;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notification_id://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "notificationEntriesKey"

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getEntries()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v1, "notificationIdKey"

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    if-eqz p3, :cond_0

    const-string v1, "notificationDismissCallback"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    const/4 v2, 0x0

    const/high16 v3, 0x48000000

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;->getBroadcast(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    return-void
.end method


# virtual methods
.method public cancelAll()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mNotificationManager:Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;->cancelAll()V

    return-void
.end method

.method public cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mNotificationManager:Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->getNotificationId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;->cancel(I)V

    return-void
.end method

.method public createNotification(Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 5
    .param p1    # Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .param p2    # Landroid/app/PendingIntent;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationType()I

    move-result v1

    const/4 v4, -0x1

    if-ne v1, v4, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    const/4 v4, 0x4

    if-ne v1, v4, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v2, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-interface {p1, v4}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_0

    :cond_2
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->setupNotification(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)V

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    goto :goto_0
.end method

.method public dismissNotification(Ljava/util/Collection;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V
    .locals 7
    .param p2    # Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    new-instance v1, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-direct {v1, v5, v2, v0, v6}, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;-><init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/actions/DismissNotificationAction;->run()V

    :cond_2
    return-void
.end method

.method public getLastNotificationTime()J
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "last_notification_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public sendDeliverActiveNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    const/16 v4, 0xc

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v3

    if-ne v3, v4, :cond_1

    move-object v1, v0

    :cond_2
    if-nez v1, :cond_3

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$Action;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$Action;->setType(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v1

    :cond_3
    new-instance v3, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-direct {v3, v4, v5, p1, v1}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public setLastNotificationTime()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last_notification_time"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method setupLowPriorityNotification(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)V
    .locals 7
    .param p1    # Landroid/app/Notification$Builder;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getTotalEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11000a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    :cond_1
    const/4 v2, -0x2

    invoke-virtual {p1, v2}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    return-void
.end method

.method setupNormalNotification(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)V
    .locals 6
    .param p1    # Landroid/app/Notification$Builder;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    const/4 v0, 0x4

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->isActiveNotification()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    const v5, 0x7f0d0097

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    const v5, 0x7f0d0096

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    invoke-virtual {p1, v0}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-interface {p2, v4}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v5, "NOTIFY"

    invoke-virtual {v4, v5, p2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryNotification(Ljava/lang/String;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)V

    return-void

    :cond_1
    or-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method setupNotification(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)V
    .locals 8
    .param p1    # Landroid/app/Notification$Builder;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .param p3    # Landroid/app/PendingIntent;

    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    invoke-virtual {p1, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-interface {p2, v6}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationContentIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationSmallIcon()I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-interface {p2, v6}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-interface {p2, v6}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p1, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationStyle()Landroid/app/Notification$Style;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p1, v4}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->addDeletePendingIntent(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)V

    const/4 v1, 0x0

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getActiveActions()Ljava/lang/Iterable;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/notifications/NotificationAction;

    add-int/lit8 v2, v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->addAction(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;Lcom/google/android/apps/sidekick/notifications/NotificationAction;I)V

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationType()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_3

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->setupLowPriorityNotification(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)V

    :goto_1
    return-void

    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->setupNormalNotification(Landroid/app/Notification$Builder;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)V

    goto :goto_1
.end method

.method public showNotification(Landroid/app/Notification;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V
    .locals 2
    .param p1    # Landroid/app/Notification;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManagerImpl;->mNotificationManager:Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->getNotificationId()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/sidekick/inject/NotificationManagerInjectable;->notify(ILandroid/app/Notification;)V

    return-void
.end method
