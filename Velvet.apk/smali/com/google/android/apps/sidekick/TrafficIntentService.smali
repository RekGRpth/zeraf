.class public Lcom/google/android/apps/sidekick/TrafficIntentService;
.super Landroid/app/IntentService;
.source "TrafficIntentService.java"


# static fields
.field static final DEFAULT_TIME_BETWEEN_QUERIES_IN_MILLIS:J = 0xdbba0L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

.field private mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

.field private mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

.field private mPowerManager:Landroid/os/PowerManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/TrafficIntentService;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/TrafficIntentService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/TrafficIntentService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private broadcastEntry(Landroid/location/Location;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 3
    .param p1    # Landroid/location/Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.NOTIFICATION_ENTRY_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "notification_entry"

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v1, "location_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private static buildRequestPayload()Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 5

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$Interest;->setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$Interest;->addEntryTypeRestrict(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v1

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;-><init>()V

    invoke-virtual {v3, v1}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;->addInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    move-result-object v0

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setEntryQuery(Lcom/google/geo/sidekick/Sidekick$EntryQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v2

    return-object v2
.end method

.method private checkTrafficReport()J
    .locals 13

    const-wide/16 v3, 0x0

    const/4 v12, 0x0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->hasLocation()Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    sget-object v9, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->TRAFFIC_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-interface {v8, v9}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    :cond_0
    :goto_0
    return-wide v3

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-static {}, Lcom/google/android/apps/sidekick/TrafficIntentService;->buildRequestPayload()Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse()Z

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    iget-object v8, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    sget-object v9, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->TRAFFIC_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-interface {v8, v9}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    goto :goto_0

    :cond_3
    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v8

    if-lez v8, :cond_6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v8

    invoke-virtual {v8, v12}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasError()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getError()I

    move-result v2

    :cond_4
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasExpirationTimestampSeconds()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getExpirationTimestampSeconds()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long v3, v8, v10

    :cond_5
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v8, -0x1

    if-ne v2, v8, :cond_6

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v8

    if-lez v8, :cond_6

    invoke-virtual {v7, v12}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$Notification;->getType()I

    move-result v8

    const/4 v9, 0x3

    if-eq v8, v9, :cond_6

    iget-object v8, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v8

    invoke-direct {p0, v8, v0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->broadcastEntry(Landroid/location/Location;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    const/4 v5, 0x1

    :cond_6
    if-nez v5, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    sget-object v9, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->TRAFFIC_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-interface {v8, v9}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    goto/16 :goto_0
.end method

.method public static ensureScheduled(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V
    .locals 2

    invoke-static {p0, p1}, Lcom/google/android/apps/sidekick/TrafficIntentService;->trafficIntentExists(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/sidekick/TrafficIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method private static getTrafficIntent(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;Z)Landroid/app/PendingIntent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/apps/sidekick/TrafficIntentService;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x40000000

    if-nez p2, :cond_0

    const/high16 v0, 0x60000000

    :cond_0
    const/4 v2, 0x0

    invoke-interface {p1, v2, v1, v0}, Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;->getService(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private resetAlarmForScheduledUpdates(IJ)V
    .locals 4
    .param p1    # I
    .param p2    # J

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/sidekick/TrafficIntentService;->getTrafficIntent(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;Z)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private static trafficIntentExists(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)Z
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->getTrafficIntent(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;Z)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method injectDependencies(Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Landroid/os/PowerManager;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p2    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p3    # Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
    .param p4    # Landroid/os/PowerManager;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .param p6    # Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;
    .param p7    # Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mPowerManager:Landroid/os/PowerManager;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    return-void
.end method

.method public onCreate()V
    .locals 10

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v1

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v2

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNowNotificationManager()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-result-object v3

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getAlarmManager()Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    move-result-object v6

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getPendingIntentFactory()Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    move-result-object v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/sidekick/TrafficIntentService;->injectDependencies(Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Landroid/os/PowerManager;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->maybeRegisterSidekickAlarms()V

    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Intent;

    const-wide/32 v10, 0x493e0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mPowerManager:Landroid/os/PowerManager;

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/google/android/apps/sidekick/TrafficIntentService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_onHandleIntent"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    :try_start_0
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    if-eqz p1, :cond_1

    const-string v6, "com.google.android.apps.sidekick.TrafficIntentService.SHUTDOWN_ACTION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/sidekick/TrafficIntentService;->getTrafficIntent(Landroid/content/Context;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;Z)Landroid/app/PendingIntent;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v6, v4}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/TrafficIntentService;->checkTrafficReport()J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-lez v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    sub-long v6, v0, v2

    cmp-long v6, v6, v10

    if-gez v6, :cond_2

    add-long v0, v2, v10

    :cond_2
    const/4 v6, 0x0

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/apps/sidekick/TrafficIntentService;->resetAlarmForScheduledUpdates(IJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    :goto_1
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_5

    :try_start_2
    const-string v6, "force_refresh"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_5
    const/4 v6, 0x2

    iget-object v7, p0, Lcom/google/android/apps/sidekick/TrafficIntentService;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v7}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v7

    const-wide/32 v9, 0xdbba0

    add-long/2addr v7, v9

    invoke-direct {p0, v6, v7, v8}, Lcom/google/android/apps/sidekick/TrafficIntentService;->resetAlarmForScheduledUpdates(IJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v6

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v6
.end method
