.class Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;
.super Ljava/lang/Object;
.source "ClockEntryAdapter.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Clock$TimeTickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/ClockEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/ClockEntryAdapter;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$clock:Lcom/google/android/searchcommon/util/Clock;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/ClockEntryAdapter;Landroid/view/View;Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/ClockEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->val$view:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->val$appContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->val$clock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeTick()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/ClockEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/ClockEntryAdapter;->mClockEntry:Lcom/google/geo/sidekick/Sidekick$ClockEntry;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/ClockEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$ClockEntry;->getTimeZoneCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->val$view:Landroid/view/View;

    const v1, 0x7f100130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/ClockEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->val$appContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;->val$clock:Lcom/google/android/searchcommon/util/Clock;

    # invokes: Lcom/google/android/apps/sidekick/ClockEntryAdapter;->getFormattedText(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)Ljava/lang/String;
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/ClockEntryAdapter;Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
