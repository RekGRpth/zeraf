.class Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;
.super Ljava/lang/Object;
.source "CalendarDataProviderImpl.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MaybeNotifiable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;-><init>()V

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z
    .locals 4
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasServerData()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasServerData()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsDismissed()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotified()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasWhereField()Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    :goto_1
    return v2

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;->apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    move-result v0

    return v0
.end method
