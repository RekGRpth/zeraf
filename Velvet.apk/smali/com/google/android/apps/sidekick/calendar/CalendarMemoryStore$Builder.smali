.class Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;
.super Ljava/lang/Object;
.source "CalendarMemoryStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Builder"
.end annotation


# instance fields
.field private final mDataByIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->mDataByIdMap:Ljava/util/Map;

    return-void
.end method

.method private buildDataByHashMap()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->mDataByIdMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    # getter for: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$300()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Duplicate calendar entry by hash: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    return-object v1
.end method


# virtual methods
.method add(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z
    .locals 4
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    # invokes: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z
    invoke-static {p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$200(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->mDataByIdMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    # getter for: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicate calendar entry by provider ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
    .locals 4

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->mDataByIdMap:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->buildDataByHashMap()Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;-><init>(Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;)V

    return-object v0
.end method
