.class public final Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatLng"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasLat:Z

.field private hasLng:Z

.field private lat_:D

.field private lng_:D


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->lat_:D

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->lng_:D

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->cachedSize:I

    return v0
.end method

.method public getLat()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->lat_:D

    return-wide v0
.end method

.method public getLng()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->lng_:D

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLat()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLat()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLng()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLng()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->cachedSize:I

    return v0
.end method

.method public hasLat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLat:Z

    return v0
.end method

.method public hasLng()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLng:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->setLat(D)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->setLng(D)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v0

    return-object v0
.end method

.method public setLat(D)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLat:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->lat_:D

    return-object p0
.end method

.method public setLng(D)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLng:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->lng_:D

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLat()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLat()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLng()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLng()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_1
    return-void
.end method
