.class Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;
.super Landroid/database/ContentObserver;
.source "CalendarControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CalendarObserver"
.end annotation


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private mChangesSwallowedLastBatch:I

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mLastBatchStartTimeMillis:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mLastBatchStartTimeMillis:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mChangesSwallowedLastBatch:I

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onChange(Z)V
    .locals 6
    .param p1    # Z

    iget-object v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mLastBatchStartTimeMillis:J

    const-wide/16 v4, 0x1770

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mLastBatchStartTimeMillis:J

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mChangesSwallowedLastBatch:I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mAppContext:Landroid/content/Context;

    sget-object v3, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->UPDATE_CALENDAR_ACTION:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->sendIntentWithAction(Landroid/content/Context;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mChangesSwallowedLastBatch:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;->mChangesSwallowedLastBatch:I

    goto :goto_0
.end method
