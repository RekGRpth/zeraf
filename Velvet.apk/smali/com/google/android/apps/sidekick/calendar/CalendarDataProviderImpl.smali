.class Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;
.super Ljava/lang/Object;
.source "CalendarDataProviderImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;,
        Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotifyingAt;,
        Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotificationPending;,
        Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;,
        Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

.field private volatile mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->EMPTY:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->readFromDisk()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private convertEntryTreeToServerData(Lcom/google/geo/sidekick/Sidekick$EntryTree;)Ljava/util/Collection;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTree;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$EntryTree;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->recursiveEntryNodeConversion(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private flushToDisk()V
    .locals 10

    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;->addCalendarData(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :try_start_0
    new-instance v5, Ljava/io/BufferedOutputStream;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mAppContext:Landroid/content/Context;

    const-string v8, "calendar_store"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;->toByteArray()[B

    move-result-object v6

    array-length v7, v6

    const/high16 v8, 0x20000

    if-ge v7, v8, :cond_1

    invoke-virtual {v5, v6}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_1
    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    move-object v4, v5

    :goto_2
    return-void

    :cond_1
    :try_start_2
    sget-object v7, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Disk store too big to write ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v6

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bytes)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v2

    move-object v4, v5

    :goto_3
    :try_start_3
    sget-object v7, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    const-string v8, "Failed flushing to disk store"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v7

    :goto_4
    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v7

    :catchall_1
    move-exception v7

    move-object v4, v5

    goto :goto_4

    :catch_1
    move-exception v2

    goto :goto_3
.end method

.method private getNotificationTimeSecs(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/Long;
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasTriggerCondition()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getConditionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->hasTimeSeconds()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getTimeSeconds()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private readFromDisk()V
    .locals 15

    new-instance v10, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-direct {v10}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;-><init>()V

    new-instance v12, Ljava/io/File;

    iget-object v13, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mAppContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v13

    const-string v14, "calendar_store"

    invoke-direct {v12, v13, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/32 v12, 0x20000

    cmp-long v12, v5, v12

    if-lez v12, :cond_0

    sget-object v12, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Disk store is too large ("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " bytes)"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->clearData()V

    :goto_0
    return-void

    :cond_0
    const-wide/16 v12, 0x1

    cmp-long v12, v5, v12

    if-gez v12, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->clearData()V

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    :try_start_0
    iget-object v12, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mAppContext:Landroid/content/Context;

    const-string v13, "calendar_store"

    invoke-virtual {v12, v13}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v8

    long-to-int v12, v5

    new-array v9, v12, [B

    const/4 v11, 0x0

    long-to-int v1, v5

    :cond_2
    invoke-virtual {v8, v9, v11, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v0

    const/4 v12, 0x1

    if-ge v0, v12, :cond_3

    :goto_1
    const/4 v12, 0x1

    if-ge v11, v12, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->clearData()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v8}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_3
    sub-int/2addr v1, v0

    add-int/2addr v11, v0

    if-gtz v1, :cond_2

    goto :goto_1

    :cond_4
    :try_start_1
    new-instance v3, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;

    invoke-direct {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;-><init>()V

    const/4 v12, 0x0

    invoke-static {v9, v12, v11}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance([BII)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarDataSet;->getCalendarDataList()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v10, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->add(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    move-result v12

    if-nez v12, :cond_5

    sget-object v12, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "Read invalid data from disk store"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v12

    invoke-static {v8}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_3
    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    goto :goto_0

    :cond_6
    invoke-static {v8}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_3

    :catch_1
    move-exception v4

    :try_start_2
    sget-object v12, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    const-string v13, "Failed reading from disk store"

    invoke-static {v12, v13, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->clearData()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v8}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v12

    invoke-static {v8}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v12
.end method

.method private recursiveEntryNodeConversion(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Ljava/util/Collection;)V
    .locals 10
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildCount()I

    move-result v6

    if-lez v6, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->recursiveEntryNodeConversion(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Ljava/util/Collection;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v6

    const/16 v7, 0xe

    if-ne v6, v7, :cond_1

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCalendarEntry()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCalendarEntry()Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getHash()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v6, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    const-string v7, "Received CalendarEntry from server without hash"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-direct {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;-><init>()V

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getHash()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTravelTimeSeconds()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getTravelTimeSeconds()I

    move-result v6

    div-int/lit8 v6, v6, 0x3c

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setTravelTimeMinutes(I)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    :cond_3
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasLocation()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Location;->hasLat()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Location;->hasLng()Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setIsGeocodable(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    invoke-direct {v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;-><init>()V

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->setLat(D)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->setLng(D)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setGeocodedLatLng(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    :cond_4
    :goto_2
    invoke-direct {p0, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->getNotificationTimeSecs(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setNotifyTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    :cond_5
    invoke-interface {p2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setIsGeocodable(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_2

    :cond_7
    return-void
.end method

.method private updateWithNewServerData(Ljava/util/Collection;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;",
            ">;)Z"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->waitForInitialization()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mergeFromServerBuilder()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->update(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    const-string v5, "ServerData from server contains invalid data"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->wasChanged()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->flushToDisk()V

    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private waitForInitialization()Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    const-string v2, "Initialization latch wait interrupted"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized clearAllEventNotifiedMarkers()V
    .locals 8

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    new-instance v5, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-direct {v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;-><init>()V

    iget-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotified()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsDismissed()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotificationDismissed()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v4, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-direct {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->setIsNotified(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :goto_1
    :try_start_2
    invoke-virtual {v5, v4}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->add(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :catch_0
    move-exception v2

    :try_start_3
    sget-object v6, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    const-string v7, "Failed to clear calendar notification."

    invoke-static {v6, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v1

    goto :goto_1

    :cond_0
    move-object v4, v1

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->flushToDisk()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    monitor-exit p0

    return-void
.end method

.method public clearData()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mAppContext:Landroid/content/Context;

    const-string v1, "calendar_store"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->EMPTY:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    return-void
.end method

.method public getCalendarDataByServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->getByServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsDismissed()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public getCalendarDataForNotify()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->values()Ljava/util/Collection;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$MaybeNotifiable;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/lang/Iterable;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterables;->transform(Ljava/lang/Iterable;Lcom/google/common/base/Function;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getCalendarDataForRefresh()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->values()Ljava/util/Collection;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/lang/Iterable;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterables;->transform(Ljava/lang/Iterable;Lcom/google/common/base/Function;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getEarliestNotificationTimeSecs()Ljava/lang/Long;
    .locals 6

    const-wide v1, 0x7fffffffffffffffL

    iget-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->values()Ljava/util/Collection;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotificationPending;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotificationPending;

    invoke-static {v4, v5}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getNotifyTimeSeconds()J

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    goto :goto_0

    :cond_0
    const-wide v4, 0x7fffffffffffffffL

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    const/4 v4, 0x0

    :goto_1
    return-object v4

    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_1
.end method

.method public getNotifyingCalendarData()Ljava/lang/Iterable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->values()Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotifyingAt;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotifyingAt;-><init>(J)V

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public initialize()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$1;-><init>(Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public declared-synchronized markEventAsDismissed(J)Z
    .locals 8
    .param p1    # J

    const/4 v7, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    const/4 v3, 0x0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->setClientAction(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v6

    if-eqz v6, :cond_0

    iput-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->flushToDisk()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v6, :cond_1

    move v0, v7

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized markEventAsNotified(J)Z
    .locals 8
    .param p1    # J

    const/4 v7, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->setClientAction(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v6

    if-eqz v6, :cond_0

    iput-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->flushToDisk()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v6, :cond_1

    move v0, v7

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized markEventNotificationAsDismissed(J)Z
    .locals 8
    .param p1    # J

    const/4 v7, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->setClientAction(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v6

    if-eqz v6, :cond_0

    iput-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->flushToDisk()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v6, :cond_1

    move v0, v7

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public updateWithNewEntryTreeFromServer(Lcom/google/geo/sidekick/Sidekick$EntryTree;)Z
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->convertEntryTreeToServerData(Lcom/google/geo/sidekick/Sidekick$EntryTree;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->updateWithNewServerData(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public updateWithNewEventData(Ljava/util/Collection;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;",
            ">;)Z"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->waitForInitialization()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mergeFromEventBuilder()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->update(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->TAG:Ljava/lang/String;

    const-string v5, "EventData from calendar contains invalid data"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_2
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->wasChanged()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->flushToDisk()V

    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
