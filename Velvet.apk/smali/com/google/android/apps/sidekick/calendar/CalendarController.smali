.class public interface abstract Lcom/google/android/apps/sidekick/calendar/CalendarController;
.super Ljava/lang/Object;
.source "CalendarController.java"


# virtual methods
.method public abstract newCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
.end method

.method public abstract startCalendar(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V
.end method

.method public abstract stopCalendar(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V
.end method
