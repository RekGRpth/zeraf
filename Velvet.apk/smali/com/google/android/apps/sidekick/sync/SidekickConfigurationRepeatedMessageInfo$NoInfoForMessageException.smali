.class Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$NoInfoForMessageException;
.super Ljava/lang/RuntimeException;
.source "SidekickConfigurationRepeatedMessageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NoInfoForMessageException"
.end annotation


# direct methods
.method constructor <init>(Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    const-string v0, "An information provider was not provided for repeated message: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    return-void
.end method
