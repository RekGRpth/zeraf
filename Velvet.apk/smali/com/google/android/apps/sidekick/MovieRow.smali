.class public Lcom/google/android/apps/sidekick/MovieRow;
.super Ljava/lang/Object;
.source "MovieRow.java"


# instance fields
.field private final mEntryItemAdapter:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

.field private final mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

.field private final mShowImage:Z

.field private final mShowTitleWithMenu:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/BaseEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Movie;ZZ)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/BaseEntryAdapter;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Movie;
    .param p3    # Z
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/MovieRow;->mEntryItemAdapter:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    iput-boolean p3, p0, Lcom/google/android/apps/sidekick/MovieRow;->mShowImage:Z

    iput-boolean p4, p0, Lcom/google/android/apps/sidekick/MovieRow;->mShowTitleWithMenu:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/MovieRow;)Lcom/google/android/apps/sidekick/BaseEntryAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/MovieRow;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieRow;->mEntryItemAdapter:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/MovieRow;)Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/MovieRow;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    return-object v0
.end method


# virtual methods
.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f040074

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/MovieRow;->populateView(Landroid/content/Context;Landroid/view/View;)V

    return-object v0
.end method

.method public populateView(Landroid/content/Context;Landroid/view/View;)V
    .locals 28
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mShowTitleWithMenu:Z

    move/from16 v22, v0

    if-eqz v22, :cond_a

    const v22, 0x7f100173

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/view/ViewStub;

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    const v22, 0x7f100031

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTitle()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mShowImage:Z

    move/from16 v22, v0

    if-eqz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasImage()Z

    move-result v22

    if-eqz v22, :cond_0

    const v22, 0x7f10016f

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v11

    const v22, 0x7f100170

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasTrailerUrl()Z

    move-result v22

    if-eqz v22, :cond_0

    const v22, 0x7f100171

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageButton;

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    new-instance v22, Lcom/google/android/apps/sidekick/MovieRow$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/MovieRow$1;-><init>(Lcom/google/android/apps/sidekick/MovieRow;Landroid/content/Context;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getActorCount()I

    move-result v22

    if-lez v22, :cond_1

    const-string v22, ", "

    invoke-static/range {v22 .. v22}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/geo/sidekick/Sidekick$Movie;->getActorList()Ljava/util/List;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    const v22, 0x7f100176

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasMpaaRating()Z

    move-result v22

    if-nez v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasLength()Z

    move-result v22

    if-nez v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getRatingCount()I

    move-result v22

    if-lez v22, :cond_5

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getRatingCount()I

    move-result v22

    if-lez v22, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Lcom/google/geo/sidekick/Sidekick$Movie;->getRating(I)Lcom/google/geo/sidekick/Sidekick$Rating;

    move-result-object v12

    :goto_1
    if-eqz v12, :cond_c

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$Rating;->getRating()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_c

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$Rating;->getRating()Ljava/lang/String;

    move-result-object v13

    :goto_2
    if-eqz v12, :cond_d

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$Rating;->getUrl()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_d

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$Rating;->getUrl()Ljava/lang/String;

    move-result-object v21

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getMpaaRating()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getMpaaRating()Ljava/lang/String;

    move-result-object v10

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getLength()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getLength()Ljava/lang/String;

    move-result-object v8

    :goto_5
    const v22, 0x7f100177

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    if-eqz v13, :cond_4

    if-eqz v21, :cond_4

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$Rating;->hasFreshness()Z

    move-result v22

    if-eqz v22, :cond_3

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$Rating;->getFreshness()I

    move-result v6

    const/16 v22, 0x2

    move/from16 v0, v22

    if-ne v6, v0, :cond_10

    const v22, 0x7f0200d0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :cond_3
    :goto_6
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    new-instance v22, Lcom/google/android/apps/sidekick/MovieRow$2;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/MovieRow$2;-><init>(Lcom/google/android/apps/sidekick/MovieRow;Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    const-string v22, "&#8901;"

    invoke-static/range {v22 .. v22}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/common/base/Joiner;->skipNulls()Lcom/google/common/base/Joiner;

    move-result-object v22

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v8, v23, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v13, v10, v1}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_5

    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getShowtimeCount()I

    move-result v22

    if-lez v22, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f09003a

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    sget-object v22, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v23, "<font color=\"#%1$h\">%2$s</font>"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const v26, 0xffffff

    and-int v26, v26, v19

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Lcom/google/geo/sidekick/Sidekick$Movie;->getShowtime(I)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v22 .. v24}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getShowtimeCount()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_6

    const-string v22, ", "

    invoke-static/range {v22 .. v22}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v22

    const-string v23, ", "

    invoke-static/range {v23 .. v23}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$Movie;->getShowtimeList()Ljava/util/List;

    move-result-object v24

    const/16 v25, 0x1

    invoke-interface/range {v24 .. v25}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/google/common/base/Joiner;->join(Ljava/util/Iterator;)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    :cond_6
    const v22, 0x7f100175

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    invoke-static/range {v16 .. v16}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->hasWebUrl()Z

    move-result v22

    if-eqz v22, :cond_8

    new-instance v22, Lcom/google/android/apps/sidekick/MovieRow$3;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/MovieRow$3;-><init>(Lcom/google/android/apps/sidekick/MovieRow;Landroid/content/Context;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mEntryItemAdapter:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReason()Z

    move-result v22

    if-eqz v22, :cond_9

    const v22, 0x7f1000af

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mEntryItemAdapter:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReason()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_9
    return-void

    :cond_a
    const v22, 0x7f100174

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/MovieRow;->mMovie:Lcom/google/geo/sidekick/Sidekick$Movie;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTitle()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_b
    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_2

    :cond_d
    const/16 v21, 0x0

    goto/16 :goto_3

    :cond_e
    const/4 v10, 0x0

    goto/16 :goto_4

    :cond_f
    const/4 v8, 0x0

    goto/16 :goto_5

    :cond_10
    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v6, v0, :cond_3

    const v22, 0x7f0200cf

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_6
.end method
