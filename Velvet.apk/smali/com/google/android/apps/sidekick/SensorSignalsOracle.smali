.class public Lcom/google/android/apps/sidekick/SensorSignalsOracle;
.super Ljava/lang/Object;
.source "SensorSignalsOracle.java"


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mContext:Landroid/content/Context;

.field private final mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

.field private mDebugUserTimeMillis:Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private final mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private final mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/DeviceCapabilityManager;Lcom/google/android/apps/sidekick/inject/WidgetManager;Lcom/google/android/searchcommon/debug/DebugFeatures;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p4    # Lcom/google/android/searchcommon/DeviceCapabilityManager;
    .param p5    # Lcom/google/android/apps/sidekick/inject/WidgetManager;
    .param p6    # Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mDebugUserTimeMillis:Lcom/google/common/base/Optional;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    return-void
.end method

.method private getUserTimeMillis()J
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mDebugUserTimeMillis:Lcom/google/common/base/Optional;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/base/Optional;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method private isWifiEnabled()Z
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mContext:Landroid/content/Context;

    const-string v3, "wifi"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public buildCurrentSensorSignals(Lcom/google/geo/sidekick/Sidekick$SensorSignals;Z)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 21
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .param p2    # Z

    if-nez p1, :cond_0

    new-instance v14, Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    invoke-direct {v14}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;-><init>()V

    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setLocale(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->getUserTimeMillis()J

    move-result-wide v17

    const-wide/16 v19, 0x3e8

    div-long v19, v17, v19

    move-wide/from16 v0, v19

    invoke-virtual {v14, v0, v1}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v19

    move/from16 v0, v19

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setTimezoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocations()Ljava/util/List;

    move-result-object v3

    if-eqz p2, :cond_1

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_1

    invoke-static {v3}, Lcom/google/android/apps/sidekick/LocationUtilities;->locationsToTimestampedLocations(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->addTimestampedLocation(Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_1

    :cond_0
    move-object/from16 v14, p1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->phoneIsStationary()Z

    move-result v19

    if-eqz v19, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->stationaryTimeSeconds()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setStationaryTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->isWifiEnabled()Z

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setWifiEnabled(Z)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->hasRearFacingCamera()Z

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setHasRearFacingCamera(Z)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->getWidgetInstallCount()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setNumberWidgetsInstalled(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v19, v0

    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v20, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v8

    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v19, v0

    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v20, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v20, v0

    invoke-static/range {v19 .. v20}, Lcom/google/android/velvet/util/LayoutUtils;->getContentPadding(Landroid/content/Context;I)I

    move-result v12

    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v19, v0

    mul-int/lit8 v20, v12, 0x2

    sub-int v5, v19, v20

    const v19, 0x7f0b0048

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v11

    const/16 v19, 0x1

    move/from16 v0, v19

    if-le v11, v0, :cond_3

    const v19, 0x7f0c0022

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/lit8 v19, v11, -0x1

    mul-int v9, v4, v19

    sub-int v19, v5, v9

    div-int v5, v19, v11

    :cond_3
    new-instance v19, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    invoke-direct/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setScreenPixelsLongestEdge(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setScreenPixelsShortestEdge(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    move-result-object v19

    iget v0, v10, Landroid/util/DisplayMetrics;->density:F

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setScreenDensity(F)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setCardWidthPixelsLandscape(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;->setCardWidthPixelsPortrait(I)Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setLayoutInfo(Lcom/google/geo/sidekick/Sidekick$LayoutInfo;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    return-object v14
.end method

.method clearUserTimeMillis()V
    .locals 1

    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mDebugUserTimeMillis:Lcom/google/common/base/Optional;

    return-void
.end method

.method setUserTimeMillis(J)V
    .locals 1
    .param p1    # J

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Optional;->of(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->mDebugUserTimeMillis:Lcom/google/common/base/Optional;

    return-void
.end method
