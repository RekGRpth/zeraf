.class public interface abstract Lcom/google/android/apps/sidekick/TgPresenterAccessor;
.super Ljava/lang/Object;
.source "TgPresenterAccessor.java"


# virtual methods
.method public abstract dismissEntry(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
.end method

.method public abstract startVisualSearch(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;I)Z
.end method

.method public abstract startWebSearch(Landroid/content/Context;Ljava/lang/String;Landroid/location/Location;)Z
.end method

.method public abstract toggleBackOfCard(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
.end method
