.class public Lcom/google/android/apps/sidekick/EntryRemover;
.super Lcom/google/android/apps/sidekick/EntryTreeVisitor;
.source "EntryRemover.java"


# instance fields
.field private final mKeysToRemove:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryRemover;->mKeysToRemove:Ljava/util/Collection;

    return-void
.end method


# virtual methods
.method protected shouldRemove(Lcom/google/android/apps/sidekick/ProtoKey;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryRemover;->mKeysToRemove:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
