.class Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;
.super Ljava/lang/Object;
.source "ReservationEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->updateActionButtons(Landroid/app/Activity;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

.field final synthetic val$cid:J

.field final synthetic val$context:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/ReservationEntryAdapter;Landroid/app/Activity;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->val$context:Landroid/app/Activity;

    iput-wide p3, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->val$cid:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->val$context:Landroid/app/Activity;

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->val$cid:J

    iget-object v4, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;
    invoke-static {v4}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/ReservationEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;-><init>(Landroid/content/Context;JLcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->run()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    const-string v3, "PLACE_PAGE"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
