.class Lcom/google/android/apps/sidekick/EntryProviderImpl$3;
.super Ljava/lang/Object;
.source "EntryProviderImpl.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/EntryProviderImpl;->updateStoredEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<[B[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

.field final synthetic val$visitor:Lcom/google/android/apps/sidekick/EntryTreeVisitor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$3;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$3;->val$visitor:Lcom/google/android/apps/sidekick/EntryTreeVisitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/EntryProviderImpl$3;->apply([B)[B

    move-result-object v0

    return-object v0
.end method

.method public apply([B)[B
    .locals 9
    .param p1    # [B

    const/4 v6, 0x0

    if-eqz p1, :cond_0

    array-length v7, p1

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    :try_start_0
    new-instance v3, Lcom/google/android/apps/sidekick/EntryProviderData;

    invoke-direct {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;-><init>()V

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/EntryProviderData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLastRefreshMillis()J

    move-result-wide v4

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$3;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mMainEntriesLastRefreshTimeMillis:J
    invoke-static {v7}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$600(Lcom/google/android/apps/sidekick/EntryProviderImpl;)J

    move-result-wide v7

    cmp-long v7, v4, v7

    if-nez v7, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasEntryResponse()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$3;->val$visitor:Lcom/google/android/apps/sidekick/EntryTreeVisitor;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->visit(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->toByteArray()[B
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    :catch_0
    move-exception v0

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$500()Ljava/lang/String;

    move-result-object v7

    const-string v8, "File storage contained invalid data"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
