.class public Lcom/google/android/apps/sidekick/TravelReport$Builder;
.super Ljava/lang/Object;
.source "TravelReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/TravelReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAlternateCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

.field private final mDestination:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

.field private mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

.field private final mSource:Lcom/google/geo/sidekick/Sidekick$Location;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/TravelReport$Builder;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/TravelReport$Builder;->mDestination:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/sidekick/TravelReport;
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport$Builder;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-nez v0, :cond_0

    const-string v6, "Cannot build TrafficReport: need regular commute or commute"

    # getter for: Lcom/google/android/apps/sidekick/TravelReport;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/TravelReport;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/sidekick/TravelReport;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/TravelReport$Builder;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/TravelReport$Builder;->mDestination:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/TravelReport$Builder;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/TravelReport$Builder;->mAlternateCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/TravelReport;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Lcom/google/android/apps/sidekick/TravelReport$1;)V

    return-object v0
.end method

.method public setRegularCommute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/android/apps/sidekick/TravelReport$Builder;
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/TravelReport$Builder;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object p0
.end method
