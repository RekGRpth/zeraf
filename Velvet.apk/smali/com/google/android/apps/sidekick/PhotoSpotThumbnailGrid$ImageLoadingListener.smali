.class Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;
.super Ljava/lang/Object;
.source "PhotoSpotThumbnailGrid.java"

# interfaces
.implements Lcom/google/android/velvet/ui/WebImageView$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageLoadingListener"
.end annotation


# instance fields
.field private final mIndex:I

.field final synthetic this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;->mIndex:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;ILcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;-><init>(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;I)V

    return-void
.end method


# virtual methods
.method public onImageDownloaded(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    # getter for: Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mImageListeners:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->access$300(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    # getter for: Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mImageListeners:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->access$300(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    # getter for: Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mInvalidIndices:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->access$400(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;->mIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->maybeClearInvalidImages()V

    :cond_1
    return-void
.end method
