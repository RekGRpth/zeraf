.class Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;
.super Ljava/lang/Object;
.source "NearbyPlacesListEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;->val$uri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$1;->val$uri:Landroid/net/Uri;

    const-string v4, "NEARBY_PLACES_SEARCH_FOR_MORE"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method
