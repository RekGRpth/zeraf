.class Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;
.super Ljava/lang/Object;
.source "SignedCipherHelperImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeyPair"
.end annotation


# instance fields
.field private final mHmacKey:Ljavax/crypto/spec/SecretKeySpec;

.field private final mSecretKey:Ljavax/crypto/spec/SecretKeySpec;


# direct methods
.method constructor <init>([B[B)V
    .locals 2
    .param p1    # [B
    .param p2    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, p1, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->mSecretKey:Ljavax/crypto/spec/SecretKeySpec;

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "HmacSHA1"

    invoke-direct {v0, p2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->mHmacKey:Ljavax/crypto/spec/SecretKeySpec;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;)Ljavax/crypto/spec/SecretKeySpec;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->mSecretKey:Ljavax/crypto/spec/SecretKeySpec;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;)Ljavax/crypto/spec/SecretKeySpec;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->mHmacKey:Ljavax/crypto/spec/SecretKeySpec;

    return-object v0
.end method
