.class public Lcom/google/android/apps/sidekick/ProtoUtils;
.super Ljava/lang/Object;
.source "ProtoUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p0    # [B

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Action;-><init>()V

    invoke-static {v0, p0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    return-object v0
.end method

.method public static getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;
    .locals 10
    .param p0    # Landroid/content/Intent;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    invoke-virtual {p0, p1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    if-nez v4, :cond_0

    :goto_0
    return-object v6

    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    instance-of v7, v3, Lcom/google/android/apps/sidekick/ProtoParcelable;

    if-nez v7, :cond_1

    const-string v7, "ProtoUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid parcelable in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    move-object v5, v3

    check-cast v5, Lcom/google/android/apps/sidekick/ProtoParcelable;

    :try_start_0
    new-instance v7, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v7}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    invoke-virtual {v5, v7}, Lcom/google/android/apps/sidekick/ProtoParcelable;->extract(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v7, "ProtoUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid entry in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_3

    :goto_2
    move-object v6, v0

    goto :goto_0

    :cond_3
    move-object v0, v6

    goto :goto_2
.end method

.method public static getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p0    # [B

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    invoke-static {v0, p0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method

.method public static getEntryFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 2
    .param p0    # Landroid/content/Intent;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    return-object v1
.end method

.method public static getEntryTreeNodeFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .locals 2
    .param p0    # Landroid/content/Intent;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;-><init>()V

    invoke-static {v1, v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    return-object v1
.end method

.method public static getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;
    .locals 3
    .param p1    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(TT;[B)TT;"
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/protobuf/micro/MessageMicro;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    const-string v1, "ProtoUtils"

    const-string v2, "Error in parsing proto buffer"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static getGenericEntryType(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGenericCardEntry()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getGenericCardEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasCardType()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getGenericCardEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getCardType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInterestFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 4
    .param p0    # Landroid/content/Intent;
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/geo/sidekick/Sidekick$Interest;->parseFrom([B)Lcom/google/geo/sidekick/Sidekick$Interest;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    const-string v2, "ProtoUtils"

    const-string v3, "Error in parsing proto buffer"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getViewActionFromIntent(Landroid/content/Intent;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
    .locals 8
    .param p0    # Landroid/content/Intent;

    new-instance v5, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    invoke-direct {v5}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;-><init>()V

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->setAction(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->setUri(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->setKey(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    if-eqz v4, :cond_2

    instance-of v6, v4, Ljava/lang/Boolean;

    if-eqz v6, :cond_3

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v0, v6}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->setBoolValue(Z)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    :cond_2
    :goto_1
    invoke-virtual {v5, v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->addExtra(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    goto :goto_0

    :cond_3
    instance-of v6, v4, Ljava/lang/Long;

    if-eqz v6, :cond_4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->setLongValue(J)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->setStringValue(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    goto :goto_1

    :cond_5
    return-object v5
.end method

.method public static protosFromStringSet(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v5, 0x3

    invoke-static {v1, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v5, v0}, Lcom/google/protobuf/micro/MessageMicro;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v4
.end method

.method public static putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 4
    .param p0    # Landroid/content/Intent;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ProtoParcelable;->create(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/apps/sidekick/ProtoParcelable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-void
.end method

.method public static removeAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)V
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method
