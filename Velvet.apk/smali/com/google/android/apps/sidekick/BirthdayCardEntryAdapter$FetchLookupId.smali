.class Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;
.super Lcom/google/android/searchcommon/util/ExecutorAsyncTask;
.source "BirthdayCardEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FetchLookupId"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/ExecutorAsyncTask",
        "<",
        "Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

.field private final viewContactButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;Landroid/widget/Button;Landroid/content/Context;Lcom/google/android/searchcommon/AsyncServices;)V
    .locals 3
    .param p2    # Landroid/widget/Button;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/google/android/searchcommon/AsyncServices;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    invoke-interface {p4}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    const-string v1, "FetchLookupId"

    invoke-interface {p4}, Lcom/google/android/searchcommon/AsyncServices;->getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/searchcommon/util/ExecutorAsyncTask;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    iput-object p3, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->viewContactButton:Landroid/widget/Button;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->context:Landroid/content/Context;

    return-object v0
.end method

.method private fetchLookupId(Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;)Ljava/lang/String;
    .locals 10
    .param p1    # Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    const/4 v9, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getEmail()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getPhone()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v7}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    const/4 v6, 0x0

    :try_start_0
    # getter for: Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->CONTACT_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->access$200()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "lookup"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getPhone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v2, v9

    goto :goto_1

    :cond_3
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v2, v9

    goto :goto_1

    :catchall_0
    move-exception v2

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->doInBackground([Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;)Ljava/lang/String;
    .locals 1
    .param p1    # [Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->fetchLookupId(Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->viewContactButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->viewContactButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->viewContactButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f1000f0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    const v1, 0x7f1000ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;->viewContactButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId$1;-><init>(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$FetchLookupId;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    const v1, 0x7f1000ec

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
