.class public Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "BarcodeEntryAdapter.java"


# instance fields
.field private final mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getBarcodeEntry()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    return-void
.end method

.method private addBoardingPassToCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 24
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasFlightBoardingPass()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getFlightBoardingPass()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    move-result-object v19

    const v4, 0x7f100031

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->getTitle(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;)Ljava/lang/CharSequence;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f100043

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TableLayout;

    const v4, 0x7f040009

    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->setBoardingPassPassenger(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;Landroid/view/View;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    const v4, 0x7f04000a

    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->setBoardingPassData(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;Landroid/view/View;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/TableLayout;->setVisibility(I)V

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAdditionalTicketText()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAdditionalTicketText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    const v4, 0x7f100041

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAdditionalTicketText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    const v4, 0x7f10002f

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/Button;

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGmailReferenceList()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v15, v4}, Lcom/google/android/apps/sidekick/MoonshineUtilities;->getEffectiveGmailReferenceAndSetText(Landroid/content/Context;Landroid/widget/Button;Ljava/util/List;)Lcom/google/geo/sidekick/Sidekick$GmailReference;

    move-result-object v16

    if-eqz v16, :cond_3

    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v4, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->getEmailUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const-string v10, "BOARDING_PASS_EMAIL"

    const-string v11, "mail"

    sget-object v12, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->GMAIL_URL_PREFIXES:[Ljava/lang/String;

    const/4 v13, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v14

    move-object/from16 v5, p1

    move-object/from16 v9, p0

    invoke-direct/range {v4 .. v14}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    invoke-virtual {v15, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasManageFlightUrl()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getManageFlightUrl()Ljava/lang/String;

    move-result-object v18

    const v4, 0x7f100044

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/Button;

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v4, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method private basicBarcodeView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    const v2, 0x7f04000b

    invoke-virtual {p2, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasBarcode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f100042

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/CrossfadingWebImageView;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setImageUri(Landroid/net/Uri;)V

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setVisibility(I)V

    :cond_0
    return-object v0
.end method

.method private getTitle(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    const v0, 0x7f0d0132

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAirlineCode()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getFlightNumber()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method private setBoardingPassData(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .param p3    # Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasTerminal()Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f10003a

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getTerminal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGate()Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x7f10003c

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasSeat()Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f10003e

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getSeat()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGroup()Z

    move-result v4

    if-eqz v4, :cond_3

    const v4, 0x7f100040

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGroup()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method private setBoardingPassPassenger(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .param p3    # Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasPassengerName()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f100038

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getPassengerName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 7

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getFlightBoardingPass()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const v3, 0x7f0d02eb

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAirlineCode()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getFlightNumber()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;I[Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v2
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d020f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0288

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0289

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasFlightBoardingPass()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d02bd

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/GmailBarcodesCardSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->basicBarcodeView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->mBarcodeEntry:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasFlightBoardingPass()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;->addBoardingPassToCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V

    :cond_0
    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
