.class Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;
.super Ljava/lang/Object;
.source "LocalAttractionsListEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->addAttractions(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

.field final synthetic val$businessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BusinessData;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;->val$businessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;->val$businessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCid()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;
    invoke-static {v4}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->access$300(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;-><init>(Landroid/content/Context;JLcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->run()V

    return-void
.end method
