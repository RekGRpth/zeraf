.class Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;
.super Ljava/lang/Object;
.source "SportsEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/SportsEntryAdapter;->addActionButton(Landroid/content/Context;Lcom/google/android/velvet/cards/SportsMatchCard;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$logLabel:Ljava/lang/String;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/SportsEntryAdapter;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->val$url:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->val$logLabel:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/16 v0, 0xd

    const/16 v1, 0x1b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->val$url:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$1;->val$logLabel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
