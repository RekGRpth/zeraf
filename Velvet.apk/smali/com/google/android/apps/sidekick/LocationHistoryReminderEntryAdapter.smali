.class public Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "LocationHistoryReminderEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;
    }
.end annotation


# static fields
.field private static final SUMMARY_COLORS:[I


# instance fields
.field private final mAerobicStats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocationHistory:Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->SUMMARY_COLORS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f09003e
        0x7f09003f
    .end array-data
.end method

.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 10
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getLocationHistoryReminderEntry()Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mLocationHistory:Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    const/4 v2, 0x1

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mLocationHistory:Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;->getAerobicStatsList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivity()Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "LocationHistoryReminderEntryAdapter"

    const-string v7, "Activity name was missing, dropping activity data"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getStatsCount()I

    move-result v6

    if-ge v3, v6, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getStats(I)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-gt v6, v3, :cond_6

    new-instance v1, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;

    const/4 v6, 0x0

    invoke-direct {v1, v6}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;-><init>(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;)V

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;->hasMonth()Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "LocationHistoryReminderEntryAdapter"

    const-string v7, "Month was missing, dropping activity data"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_3
    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;->getMonth()I

    move-result v6

    iput v6, v1, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->month:I

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;->hasDistanceInMeters()Z

    move-result v6

    if-eqz v6, :cond_5

    iget v6, v1, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->distance:I

    int-to-long v6, v6

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;->getDistanceInMeters()I

    move-result v8

    invoke-static {v8}, Lcom/google/android/apps/sidekick/LocationUtilities;->toLocalDistanceUnits(I)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    add-long/2addr v6, v8

    long-to-int v6, v6

    iput v6, v1, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->distance:I

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;

    iget v6, v1, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->month:I

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;->getMonth()I

    move-result v7

    if-eq v6, v7, :cond_4

    const-string v6, "LocationHistoryReminderEntryAdapter"

    const-string v7, "Month didn\'t match between activities, dropping activity data"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_7
    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    :goto_2
    if-ltz v3, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;

    iget v6, v6, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->distance:I

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v3, v3, -0x1

    goto :goto_2
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;Landroid/view/View;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->isPromptBubbleHidden(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->hidePromptBubble(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/View;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->showActivitySummary(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;)V

    return-void
.end method

.method private addSummaryRow(Landroid/content/Context;Landroid/widget/TableLayout;Landroid/view/LayoutInflater;Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;FI)V
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/TableLayout;
    .param p3    # Landroid/view/LayoutInflater;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;
    .param p5    # F
    .param p6    # I

    const v11, 0x7f04006c

    const/4 v12, 0x0

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v0, v11, v1, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TableRow;

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->hasActivityIcon()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getActivityIcon()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasUrl()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getActivityIcon()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrlType()I

    move-result v11

    if-nez v11, :cond_0

    const v11, 0x7f100162

    invoke-virtual {v10, v11}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getActivityIcon()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    :cond_0
    const v11, 0x7f100163

    invoke-virtual {v10, v11}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/ui/widget/Histogram;

    const v11, 0x7f100164

    invoke-virtual {v10, v11}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    add-int/lit8 v6, p6, -0x1

    :goto_0
    if-ltz v6, :cond_2

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getStatsCount()I

    move-result v11

    if-ge v6, v11, :cond_1

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getStats(I)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;->getDistanceInMeters()I

    move-result v11

    invoke-static {v11}, Lcom/google/android/apps/sidekick/LocationUtilities;->toLocalDistanceUnits(I)D

    move-result-wide v11

    :goto_1
    invoke-static {v11, v12}, Ljava/lang/Math;->round(D)J

    move-result-wide v11

    long-to-int v3, v11

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->SUMMARY_COLORS:[I

    aget v12, v12, v6

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    int-to-float v11, v3

    mul-float v11, v11, p5

    invoke-virtual {v4, v11, v2}, Lcom/google/android/velvet/ui/widget/Histogram;->addBar(FI)V

    const v11, 0x7f0d025c

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->formatDistance(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getActivity()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    sub-int/2addr v11, v6

    invoke-virtual {v9, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    :cond_1
    const-wide/16 v11, 0x0

    goto :goto_1

    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private static formatDistance(Landroid/content/Context;I)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-static {}, Lcom/google/android/apps/sidekick/LocationUtilities;->getLocalDistanceUnits()Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;->KILOMETERS:Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;

    if-ne v1, v2, :cond_0

    const v0, 0x7f11000c

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const v0, 0x7f11000b

    goto :goto_0
.end method

.method private getAerobicDistanceMax()I
    .locals 4

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;

    iget v3, v2, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->distance:I

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_0
    return v1
.end method

.method private getDiffTitle(Landroid/content/Context;II)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    if-eqz p3, :cond_1

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-static {p1, v3}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->formatDistance(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    if-ltz p3, :cond_0

    const v3, 0x7f0d0264

    :goto_0
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f002e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    if-gt p2, v3, :cond_1

    if-lez p2, :cond_1

    add-int/lit8 v3, p2, -0x1

    aget-object v3, v2, v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    const v3, 0x7f0d0265

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getMonthName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-gt p1, v1, :cond_0

    if-lez p1, :cond_0

    add-int/lit8 v1, p1, -0x1

    aget-object v1, v0, v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getSummaryText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getMonthName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09003e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const v2, 0x7f0d025b

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    const v5, 0xffffff

    and-int/2addr v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getSummaryText(Landroid/content/Context;II)Ljava/lang/String;
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    const v9, 0xffffff

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p1, p2}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getMonthName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, p3}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getMonthName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    const v5, 0x7f09003e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v5, 0x7f09003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const v5, 0x7f0d025a

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    and-int v8, v0, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    aput-object v3, v6, v7

    const/4 v7, 0x3

    and-int v8, v2, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private getTitle(Landroid/content/Context;II)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f002c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->formatDistance(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    array-length v3, v1

    if-gt p3, v3, :cond_0

    if-lez p3, :cond_0

    add-int/lit8 v3, p3, -0x1

    aget-object v3, v1, v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private hasRecordedDistance(Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;I)Z
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;
    .param p2    # I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getStatsCount()I

    move-result v1

    if-le v1, v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;->getStats(I)Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$StatsPerMonth;->getDistanceInMeters()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationUtilities;->toLocalDistanceUnits(I)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private hidePromptBubble(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const v1, 0x7f100161

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private isPromptBubbleHidden(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;

    const v1, 0x7f100161

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private showActivitySummary(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 24
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/View;

    const v4, 0x7f100031

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    new-instance v4, Lcom/google/android/velvet/Help;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Lcom/google/android/velvet/Help;-><init>(Landroid/content/Context;)V

    const-string v5, "locationhistory"

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/Help;->getHelpUrl(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    const v4, 0x7f10015e

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    const v4, 0x7f10001e

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    const v4, 0x7f10001f

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    const v4, 0x7f100021

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    new-instance v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSelectAllOnFocus(Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    const v4, 0x7f0d0258

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v4, 0x8

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v4, 0x8

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const v4, 0x7f0d025d

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v5, v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;

    iget v0, v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->month:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;

    iget v0, v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->distance:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getTitle(Landroid/content/Context;II)Ljava/lang/CharSequence;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f09003e

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    const/16 v4, 0x8

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getSummaryText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const v4, 0x7f100020

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TableLayout;

    const/high16 v4, 0x3f800000

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getAerobicDistanceMax()I

    move-result v5

    int-to-float v5, v5

    div-float v9, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mLocationHistory:Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;->getAerobicStatsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_1
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v10

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v10}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->hasRecordedDistance(Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;I)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    invoke-direct/range {v4 .. v10}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->addSummaryRow(Landroid/content/Context;Landroid/widget/TableLayout;Landroid/view/LayoutInflater;Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry$ActivityStats;FI)V

    goto :goto_2

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;

    iget v11, v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->month:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->mAerobicStats:Ljava/util/List;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;

    iget v4, v4, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$CumulativeStats;->distance:I

    sub-int v12, v17, v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11, v12}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getDiffTitle(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_3

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v11}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->getSummaryText(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_3
    const/16 v4, 0x8

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_4
    const v4, 0x7f0d025e

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v5, v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d029a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0298

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0299

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02b8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v3, 0x7f04006b

    const/4 v4, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    invoke-static/range {p1 .. p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->newNowConfigurationPreferences(Landroid/content/SharedPreferences;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v6

    const v3, 0x7f0d00bd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v3, 0x7f0d00bc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v3, 0x0

    invoke-virtual {v6, v7, v3}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    if-nez v11, :cond_0

    const v3, 0x7f100160

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v12

    new-instance v3, Lcom/google/android/velvet/Help;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/velvet/Help;-><init>(Landroid/content/Context;)V

    const-string v4, "locationhistory"

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/Help;->getHelpUrl(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    const v3, 0x7f100056

    invoke-virtual {v12, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v3, 0x7f0d025f

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v13, v4, v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f100031

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    const v3, 0x7f0d0258

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f10015d

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f10005e

    invoke-virtual {v12, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    const v3, 0x7f0d0261

    invoke-virtual {v14, v3}, Landroid/widget/Button;->setText(I)V

    new-instance v3, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;Landroid/view/View;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f100060

    invoke-virtual {v12, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/Button;

    const v3, 0x7f0d0260

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    new-instance v3, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$2;

    move-object/from16 v4, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;Landroid/view/View;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/view/LayoutInflater;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-object v5

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;->showActivitySummary(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;)V

    goto :goto_0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
