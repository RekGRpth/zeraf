.class Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;
.super Ljava/lang/Object;
.source "EntriesRefreshIntentService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->refreshEntries(Lcom/google/geo/sidekick/Sidekick$Interest;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$marinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;Lcom/google/android/searchcommon/MarinerOptInSettings;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;->this$0:Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;->val$marinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;->val$marinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;->val$account:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->disableForAccount(Landroid/accounts/Account;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;->this$0:Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService$1;->this$0:Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
