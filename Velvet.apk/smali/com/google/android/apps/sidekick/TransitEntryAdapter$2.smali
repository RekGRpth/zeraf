.class Lcom/google/android/apps/sidekick/TransitEntryAdapter$2;
.super Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;
.source "TransitEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/TransitEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/TransitEntryAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/TransitEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 0
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/TransitEntryAdapter;

    invoke-direct {p0, p2}, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    return-void
.end method


# virtual methods
.method public getQuestionCount(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x2

    return v0
.end method

.method public getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    if-nez p2, :cond_0

    const v0, 0x7f0d02f4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d02f5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v0, 0x0

    return-object v0
.end method
