.class public Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "NearbyEventsEntryAdapter.java"


# instance fields
.field private final mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p4    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p5    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->expandCard(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Landroid/view/View;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->isCardExpanded(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private addEvent(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;Z)Landroid/view/View;
    .locals 18
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p5    # Z

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;

    move-result-object v6

    const v15, 0x7f04007d

    const/16 v16, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, v16

    invoke-virtual {v0, v15, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    const v15, 0x7f10018d

    invoke-virtual {v13, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getTitle()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v15, 0x7f10018e

    invoke-virtual {v13, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->hasLocation()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v15, 0x7f10018f

    invoke-virtual {v13, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->hasStartTime()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->hasEndTime()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getStartTime()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getEndTime()Ljava/lang/String;

    move-result-object v16

    const v17, 0x80008

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-static {v0, v15, v1, v2}, Lcom/google/android/apps/sidekick/TimeUtilities;->formatDateTimeRangeFromRFC3339(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v4

    :goto_1
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    const v15, 0x7f1000ac

    invoke-virtual {v13, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/velvet/ui/WebImageView;

    if-eqz p5, :cond_8

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->hasImage()Z

    move-result v15

    if-eqz v15, :cond_7

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasUrl()Z

    move-result v15

    if-eqz v15, :cond_7

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrlType()I

    move-result v15

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v15, 0x7f0c0073

    invoke-virtual {v12, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    const v16, 0x7f0c0074

    move/from16 v0, v16

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/google/android/apps/sidekick/FifeImageUrlUtil;->setImageUrlSmartCrop(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    :cond_0
    invoke-virtual {v8, v7}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasInfoUrl()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$Photo;->getInfoUrl()Ljava/lang/String;

    move-result-object v9

    new-instance v15, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v15, v0, v1, v2, v9}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;)V

    invoke-virtual {v8, v15}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_3
    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getCategoryCount()I

    move-result v15

    if-lez v15, :cond_2

    const v15, 0x7f100190

    invoke-virtual {v13, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const-string v15, ", "

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getCategoryList()Ljava/util/List;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->hasViewAction()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getViewAction()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasUri()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getViewAction()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getUri()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$4;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v15, v0, v1, v2, v14}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;)V

    invoke-virtual {v13, v15}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v13

    :cond_4
    const/16 v15, 0x8

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getStartTime()Ljava/lang/String;

    move-result-object v15

    const v16, 0x80008

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v15, v1}, Lcom/google/android/apps/sidekick/TimeUtilities;->formatDateTimeFromRFC3339(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v4

    goto/16 :goto_1

    :cond_6
    const/16 v15, 0x8

    invoke-virtual {v5, v15}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_7
    const/4 v15, 0x4

    invoke-virtual {v8, v15}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    goto :goto_3

    :cond_8
    const/16 v15, 0x8

    invoke-virtual {v8, v15}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    goto :goto_3
.end method

.method private expandCard(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 21
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    const v2, 0x7f100031

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    const v2, 0x7f0d026a

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    const v2, 0x7f10005a

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    new-instance v2, Landroid/animation/LayoutTransition;

    invoke-direct {v2}, Landroid/animation/LayoutTransition;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    const/16 v18, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->shouldShowImages()Z

    move-result v7

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_0
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventEntry()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "EventEntryAdapter"

    const-string v3, "Unexpected Entry without event data."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p4

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->addEvent(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;Z)Landroid/view/View;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v11, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v18, v18, 0x1

    const/16 v2, 0xa

    move/from16 v0, v18

    if-ne v0, v2, :cond_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->addListCardTags(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/List;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v16

    check-cast p2, Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    new-instance v8, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-object/from16 v10, p0

    move-object/from16 v12, p1

    invoke-direct/range {v8 .. v16}, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/util/Map;Landroid/content/Context;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->setOnDismissListener(Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;)V

    :cond_3
    return-void
.end method

.method private getSummaryText()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventEntry()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->hasTitle()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v3, ", "

    invoke-static {v3, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private isCardExpanded(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    const v1, 0x7f10018c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/CardTableLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/CardTableLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/CardTableLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private shouldShowImages()Z
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventEntry()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$5;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$5;-><init>(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v1
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d026c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d026a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d026b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02bb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v7, 0x7f10018c

    const v5, 0x7f04007c

    const/4 v6, 0x0

    invoke-virtual {p2, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v5, 0x7f100031

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0d0269

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f10005a

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->getSummaryText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/CardTableLayout;

    new-instance v5, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$1;

    invoke-direct {v5, p0, p1, v0}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Landroid/content/Context;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v5}, Lcom/google/android/apps/sidekick/CardTableLayout;->setAdapter(Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;)V

    new-instance v2, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$2;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Lcom/google/android/apps/sidekick/CardTableLayout;)V

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v5, v0

    check-cast v5, Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->setDismissableContainer(Landroid/view/ViewGroup;)V

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
