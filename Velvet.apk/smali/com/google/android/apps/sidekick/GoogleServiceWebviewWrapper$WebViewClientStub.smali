.class Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;
.super Landroid/webkit/WebViewClient;
.source "GoogleServiceWebviewWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WebViewClientStub"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received error while loading page("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$400(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->sendViewIntent(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->finish()V

    return-void
.end method

.method public onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/HttpAuthHandler;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Auth error while loading page"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$400(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->sendViewIntent(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->finish()V

    return-void
.end method

.method public onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Login Request while loading page"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$400(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->sendViewIntent(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->finish()V

    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/SslErrorHandler;
    .param p3    # Landroid/net/http/SslError;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ssl Error while loading page"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$400(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->sendViewIntent(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->finish()V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 12
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v8

    :cond_1
    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->SCHEMES_TO_OPEN_IN_AN_APP:Ljava/util/Set;
    invoke-static {}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$000()Ljava/util/Set;

    move-result-object v10

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    # invokes: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->sendViewIntent(Landroid/net/Uri;)V
    invoke-static {v8, v6}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;Landroid/net/Uri;)V

    move v8, v9

    goto :goto_0

    :cond_2
    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    const-string v5, ""

    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v10, "accounts.google."

    invoke-virtual {v2, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUrlPrefixesStayInWebView:[Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$200(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)[Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->mUrlPrefixesStayInWebView:[Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$200(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;)[Ljava/lang/String;

    move-result-object v0

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v7, v0, v3

    const-string v10, "*"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    iget-object v8, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper$WebViewClientStub;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    # invokes: Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->sendViewIntent(Landroid/net/Uri;)V
    invoke-static {v8, v6}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;Landroid/net/Uri;)V

    move v8, v9

    goto :goto_0
.end method
