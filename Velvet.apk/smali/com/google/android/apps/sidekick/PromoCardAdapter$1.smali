.class Lcom/google/android/apps/sidekick/PromoCardAdapter$1;
.super Ljava/lang/Object;
.source "PromoCardAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/PromoCardAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/PromoCardAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/PromoCardAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PromoCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PromoCardAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PromoCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PromoCardAdapter;

    # getter for: Lcom/google/android/apps/sidekick/PromoCardAdapter;->mTgPresenter:Lcom/google/android/velvet/presenter/TgPresenter;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/PromoCardAdapter;->access$000(Lcom/google/android/apps/sidekick/PromoCardAdapter;)Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->showTheGoogleCardList()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PromoCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PromoCardAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/PromoCardAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PromoCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PromoCardAdapter;

    const-string v3, "CARD_LIST"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
