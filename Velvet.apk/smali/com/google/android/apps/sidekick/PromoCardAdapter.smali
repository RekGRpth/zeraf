.class public Lcom/google/android/apps/sidekick/PromoCardAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "PromoCardAdapter.java"


# instance fields
.field private final mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

.field private final mTgPresenter:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/velvet/presenter/CardDismissalHandler;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p2    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p3    # Lcom/google/android/velvet/presenter/CardDismissalHandler;
    .param p4    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    new-instance v1, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    invoke-direct {v1, p2}, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;-><init>(Lcom/google/android/apps/sidekick/inject/EntryProvider;)V

    invoke-direct {p0, v0, v1, p4}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PromoCardAdapter;->mTgPresenter:Lcom/google/android/velvet/presenter/TgPresenter;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/PromoCardAdapter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/PromoCardAdapter;)Lcom/google/android/velvet/presenter/TgPresenter;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PromoCardAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PromoCardAdapter;->mTgPresenter:Lcom/google/android/velvet/presenter/TgPresenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/PromoCardAdapter;)Lcom/google/android/velvet/presenter/CardDismissalHandler;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PromoCardAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PromoCardAdapter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v2, 0x7f04009a

    const/4 v3, 0x0

    invoke-virtual {p2, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f1001e4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/sidekick/PromoCardAdapter$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/sidekick/PromoCardAdapter$1;-><init>(Lcom/google/android/apps/sidekick/PromoCardAdapter;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    new-instance v0, Lcom/google/android/apps/sidekick/PromoCardAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/PromoCardAdapter$2;-><init>(Lcom/google/android/apps/sidekick/PromoCardAdapter;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->addClientRunnable(Ljava/lang/Runnable;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
