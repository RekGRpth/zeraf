.class public Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;
.super Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardActionAdapter;
.source "SettingsActionAdapter.java"


# instance fields
.field private final mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

.field private final mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

.field private final mLabelResId:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/inject/ActivityHelper;I)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p2    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .param p3    # I

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardActionAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    iput p3, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mLabelResId:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;)Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    return-object v0
.end method


# virtual methods
.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->createActionList(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mLabelResId:I

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p2, v1, v2}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->addButton(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/widget/Button;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;-><init>(Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method
