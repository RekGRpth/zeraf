.class public Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;
.super Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardActionAdapter;
.source "PlaceActionAdapter.java"


# instance fields
.field private final mEntryAdapter:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardActionAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;)Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    return-object v0
.end method


# virtual methods
.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 5
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;->createActionList(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteCount()I

    move-result v4

    if-lez v4, :cond_0

    const v4, 0x7f0d0197

    invoke-virtual {p1, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p2, v3, v4}, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;->addButton(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/widget/Button;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRoute(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelModeSetting()I

    move-result v2

    new-instance v4, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter$1;

    invoke-direct {v4, p0, v2, p1}, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter$1;-><init>(Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;ILandroid/app/Activity;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object v3
.end method
