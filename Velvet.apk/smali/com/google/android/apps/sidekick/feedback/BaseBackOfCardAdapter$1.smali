.class Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;
.super Ljava/lang/Object;
.source "BaseBackOfCardAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->yesNoCheckedListener(Landroid/content/Context;ILcom/google/android/apps/sidekick/FeedbackInterestButton;Z)Landroid/widget/CompoundButton$OnCheckedChangeListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

.field final synthetic val$isYes:Z

.field final synthetic val$otherButton:Lcom/google/android/apps/sidekick/FeedbackInterestButton;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;Lcom/google/android/apps/sidekick/FeedbackInterestButton;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->val$otherButton:Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    iput-boolean p3, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->val$isYes:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->val$otherButton:Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->val$otherButton:Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->access$000(Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "CARD_BACK_BUTTON_PRESS"

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->access$000(Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->val$isYes:Z

    if-eqz v0, :cond_1

    const-string v0, "FEEDBACK_YES"

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    # invokes: Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->updateCardEnablement()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->access$100(Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;)V

    return-void

    :cond_1
    const-string v0, "FEEDBACK_NO"

    goto :goto_0
.end method
