.class public Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;
.super Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardActionAdapter;
.source "WeatherActionAdapter.java"


# instance fields
.field private final mEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/WeatherEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardActionAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;)Lcom/google/android/apps/sidekick/WeatherEntryAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    return-object v0
.end method


# virtual methods
.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->createActionList(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v0

    const v2, 0x7f0d01b4

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p2, v0, v2}, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;->addButton(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter$1;-><init>(Lcom/google/android/apps/sidekick/feedback/WeatherActionAdapter;Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method
