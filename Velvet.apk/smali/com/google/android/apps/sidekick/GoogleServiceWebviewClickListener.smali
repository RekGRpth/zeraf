.class public Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;
.super Ljava/lang/Object;
.source "GoogleServiceWebviewClickListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mEnableJavascript:Z

.field private final mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

.field private final mInteractionLabel:Ljava/lang/String;

.field private final mRecordActionTaskFactory:Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;

.field private final mService:Ljava/lang/String;

.field private final mTargetUrl:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field private final mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

.field private final mWebviewUrlPrefixes:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # [Ljava/lang/String;
    .param p9    # Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;
    .param p10    # Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mTargetUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mTitle:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mEnableJavascript:Z

    iput-object p5, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mInteractionLabel:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mService:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mWebviewUrlPrefixes:[Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mRecordActionTaskFactory:Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;

    iput-object p10, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;II)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->makeToast(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mWebviewUrlPrefixes:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mEnableJavascript:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method private makeToast(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$2;-><init>(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;II)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mTargetUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mService:Ljava/lang/String;

    new-instance v3, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;-><init>(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getGaiaWebLoginLink(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/searchcommon/util/Consumer;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v2, "CARD_BUTTON_PRESS"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mInteractionLabel:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mRecordActionTaskFactory:Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mRecordActionTaskFactory:Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;->create()Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v2, "CARD_BUTTON_PRESS"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mInteractionLabel:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
