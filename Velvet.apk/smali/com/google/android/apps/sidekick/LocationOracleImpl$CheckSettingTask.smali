.class Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckSettingTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p2    # Lcom/google/android/apps/sidekick/LocationOracleImpl$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STARTED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->startListening()V
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2000(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->stopListening()V
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    goto :goto_0
.end method
