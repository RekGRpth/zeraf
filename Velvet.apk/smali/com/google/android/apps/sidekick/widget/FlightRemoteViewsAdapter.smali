.class public Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "FlightRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter$1;
    }
.end annotation


# static fields
.field private static final DELAYED_DRAWABLES:[I

.field private static final ON_TIME_DRAWABLES:[I

.field private static final UNKNOWN_DRAWABLES:[I


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x3

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->ON_TIME_DRAWABLES:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->DELAYED_DRAWABLES:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->UNKNOWN_DRAWABLES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f020151
        0x7f020150
        0x7f02014f
    .end array-data

    :array_1
    .array-data 4
        0x7f02014d
        0x7f02014c
        0x7f02014f
    .end array-data

    :array_2
    .array-data 4
        0x7f02014b
        0x7f02014a
        0x7f02014f
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    return-void
.end method

.method private createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    iget-object v9, p0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    invoke-virtual {v9}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlightCount()I

    move-result v9

    if-nez v9, :cond_1

    const/4 v8, 0x0

    :cond_0
    :goto_0
    return-object v8

    :cond_1
    new-instance v8, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const v10, 0x7f04003d

    invoke-direct {v8, v9, v10}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v9, p0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    invoke-virtual {v9}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getRelevantFlight()Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v2

    :goto_1
    const v9, 0x7f1000d3

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v9, 0x7f1000d7

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c0091

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const v9, 0x7f1000d3

    const/4 v10, 0x0

    int-to-float v11, v6

    invoke-virtual {v8, v9, v10, v11}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    const v9, 0x7f1000d7

    const/4 v10, 0x0

    int-to-float v11, v6

    invoke-virtual {v8, v9, v10, v11}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    :cond_2
    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->getProgressResourceIds(I)[I

    move-result-object v4

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getStatus()Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    move-result-object v7

    :goto_2
    sget-object v9, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter$1;->$SwitchMap$com$google$android$apps$sidekick$FlightStatusEntryAdapter$RelevantFlight$Status:[I

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    :cond_3
    :goto_3
    new-instance v3, Lcom/google/android/velvet/cards/FlightStatusFormatter;

    invoke-direct {v3, p1}, Lcom/google/android/velvet/cards/FlightStatusFormatter;-><init>(Landroid/content/Context;)V

    const v9, 0x7f1000d8

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v10

    invoke-virtual {v3, v10}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->getStatusSummary(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v9, 0x7f1000d8

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v10

    invoke-virtual {v3, v10}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->getColorForStatus(I)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/RemoteViews;->setTextColor(II)V

    if-nez p2, :cond_0

    if-eqz v5, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    invoke-virtual {v9, p1, v5}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getFormattedTime(Landroid/content/Context;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    const v9, 0x7f1000d9

    invoke-virtual {v8, v9, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    iget-object v9, p0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    invoke-virtual {v9}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlight(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v2

    goto/16 :goto_1

    :cond_5
    sget-object v7, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->NOT_DEPARTED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    goto :goto_2

    :pswitch_0
    const v9, 0x7f1000d5

    const/4 v10, 0x1

    aget v10, v4, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->setDrawable(Landroid/widget/RemoteViews;II)V

    if-nez p2, :cond_3

    const v9, 0x7f1000d6

    const/4 v10, 0x2

    aget v10, v4, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->setDrawable(Landroid/widget/RemoteViews;II)V

    goto :goto_3

    :pswitch_1
    const v9, 0x7f1000d5

    const/4 v10, 0x1

    aget v10, v4, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->setDrawable(Landroid/widget/RemoteViews;II)V

    if-nez p2, :cond_3

    const v9, 0x7f1000d4

    const/4 v10, 0x0

    aget v10, v4, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->setDrawable(Landroid/widget/RemoteViews;II)V

    const v9, 0x7f1000d6

    const/4 v10, 0x2

    aget v10, v4, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->setDrawable(Landroid/widget/RemoteViews;II)V

    goto :goto_3

    :pswitch_2
    const v9, 0x7f1000d5

    const/4 v10, 0x1

    aget v10, v4, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->setDrawable(Landroid/widget/RemoteViews;II)V

    if-nez p2, :cond_3

    const v9, 0x7f1000d4

    const/4 v10, 0x0

    aget v10, v4, v10

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->setDrawable(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getProgressResourceIds(I)[I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->UNKNOWN_DRAWABLES:[I

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->ON_TIME_DRAWABLES:[I

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->DELAYED_DRAWABLES:[I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private setDrawable(Landroid/widget/RemoteViews;II)V
    .locals 1
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const-string v0, "setBackgroundResource"

    invoke-virtual {p1, p2, v0, p3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method
