.class public Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "BirthdayRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

.field private final mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;
    .param p2    # Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 11
    .param p1    # Landroid/content/Context;

    const v3, 0x7f1002a0

    const v10, 0x7f10029e

    const v5, 0x7f10029d

    const/4 v9, 0x0

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0400df

    invoke-direct {v2, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getBirthdayCardEntry()Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthday()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getOwnBirthday()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0d0275

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthdaySeconds()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0276

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v3

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getOwnBirthdaySeconds()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v9

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v10, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0d0279

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v10, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->getPhotoUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->loadImageUri(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Landroid/graphics/Rect;)V

    invoke-virtual {v2, v3, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method
