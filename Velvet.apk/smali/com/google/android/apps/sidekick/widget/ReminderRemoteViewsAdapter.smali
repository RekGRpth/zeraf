.class public Lcom/google/android/apps/sidekick/widget/ReminderRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "ReminderRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/ReminderEntryAdapter;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/ReminderEntryAdapter;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/ReminderRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/widget/ReminderRemoteViewsAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 6
    .param p1    # Landroid/content/Context;

    const v5, 0x7f10029e

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0400df

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/ReminderRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v0

    const v3, 0x7f10029d

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;->getReminderMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/ReminderRemoteViewsAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {p1, v0, v3, v4}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->getTriggerMessage(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$ReminderEntry;J)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, v5, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_0
    return-object v2

    :cond_0
    const/16 v3, 0x8

    invoke-virtual {v2, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method
