.class public Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "PackageTrackingRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    return-void
.end method

.method private createRemoteViewInternal(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const v9, 0x7f10029d

    new-instance v6, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0400df

    invoke-direct {v6, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;->getShortTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    :goto_0
    invoke-virtual {v6, v9, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    invoke-virtual {v7, p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getTitleColor(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Landroid/widget/RemoteViews;->setTextColor(II)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPackageTrackingEntry()Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getItemsCount()I

    move-result v7

    if-lez v7, :cond_3

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getItemsCount()I

    move-result v7

    invoke-static {v7}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getItemsList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$PackageItem;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$PackageItem;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    invoke-virtual {v7, p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_0

    :cond_2
    const v7, 0x7f10029e

    const-string v8, ", "

    invoke-static {v8}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_3
    return-object v6
.end method

.method private getShortTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPackageTrackingEntry()Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatus()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const v1, 0x7f0d0238

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;->createRemoteViewInternal(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;->createRemoteViewInternal(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method
