.class public Lcom/google/android/apps/sidekick/widget/NewsRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "NewsRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/NewsEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/NewsEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/NewsEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/NewsRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/NewsEntryAdapter;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/widget/NewsRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 7
    .param p1    # Landroid/content/Context;

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f040081

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/widget/NewsRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/NewsEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/NewsEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNewsEntry()Lcom/google/geo/sidekick/Sidekick$NewsEntry;

    move-result-object v0

    const v2, 0x7f100194

    const v3, 0x7f0d02a1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$NewsEntry;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-object v1
.end method
