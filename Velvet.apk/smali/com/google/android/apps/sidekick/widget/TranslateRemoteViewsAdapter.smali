.class public Lcom/google/android/apps/sidekick/widget/TranslateRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "TranslateRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/TranslateEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/TranslateRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/widget/TranslateRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 7
    .param p1    # Landroid/content/Context;

    const v6, 0x7f10029d

    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0400df

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/widget/TranslateRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Entry;->getTranslateEntry()Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    move-result-object v1

    move-object v2, v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->hasTargetText()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v2, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;-><init>()V

    :try_start_0
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    const-string v4, "Bienvenido"

    invoke-virtual {v2, v4}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->setTargetText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    const-string v4, "Welcome"

    invoke-virtual {v2, v4}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->setSourceText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TranslateEntry;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->getTargetText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090065

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v6, v4}, Landroid/widget/RemoteViews;->setTextColor(II)V

    const v4, 0x7f10029e

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->getSourceText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-object v3

    :catch_0
    move-exception v0

    move-object v2, v1

    goto :goto_0
.end method
