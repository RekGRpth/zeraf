.class public Lcom/google/android/apps/sidekick/widget/WidgetPopulator;
.super Ljava/lang/Object;
.source "WidgetPopulator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/MarinerOptInSettings;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p2    # Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;
    .param p3    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p4    # Lcom/google/android/searchcommon/MarinerOptInSettings;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method

.method private addCard(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/apps/sidekick/EntryItemAdapter;Z)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/RemoteViews;
    .param p3    # I
    .param p4    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p5    # Z

    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->createView(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    const/4 v5, 0x0

    invoke-virtual {p2, p3, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    new-instance v1, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-interface {p4}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    const/4 v5, 0x1

    invoke-static {p1, v5}, Lcom/google/android/velvet/util/IntentUtils;->createAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "target_entry"

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/ProtoKey;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-interface {p4}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v5, "target_group_entry_tree"

    invoke-interface {p4}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/ProtoKey;->hashCode()I

    move-result v5

    const/high16 v6, 0x8000000

    invoke-static {p1, v5, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {p2, p3, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void
.end method

.method private addLaunchIntent(Landroid/content/Context;ILandroid/widget/RemoteViews;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/widget/RemoteViews;

    const/4 v2, 0x1

    invoke-static {p1, v2}, Lcom/google/android/velvet/util/IntentUtils;->createAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {p1, p2, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v2, 0x7f1001dc

    invoke-virtual {p3, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void
.end method

.method private createView(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;Z)Landroid/widget/RemoteViews;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p3    # Z

    const/4 v0, 0x0

    instance-of v1, p2, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/sidekick/widget/WeatherRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/WeatherEntryAdapter;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)V

    :cond_0
    :goto_0
    if-eqz v0, :cond_f

    if-eqz p3, :cond_e

    invoke-interface {v0, p1}, Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;->createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v1

    :goto_1
    return-object v1

    :cond_1
    instance-of v1, p2, Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    if-eqz v1, :cond_2

    new-instance v0, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/SportsEntryAdapter;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)V

    goto :goto_0

    :cond_2
    instance-of v1, p2, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    if-eqz v1, :cond_3

    new-instance v0, Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;)V

    goto :goto_0

    :cond_3
    instance-of v1, p2, Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    if-eqz v1, :cond_4

    new-instance v0, Lcom/google/android/apps/sidekick/widget/StockQuoteRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/StockQuoteRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;)V

    goto :goto_0

    :cond_4
    instance-of v1, p2, Lcom/google/android/apps/sidekick/PublicAlertEntryAdapter;

    if-eqz v1, :cond_5

    new-instance v0, Lcom/google/android/apps/sidekick/widget/PublicAlertRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/PublicAlertEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/PublicAlertRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/PublicAlertEntryAdapter;)V

    goto :goto_0

    :cond_5
    instance-of v1, p2, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    if-eqz v1, :cond_6

    new-instance v0, Lcom/google/android/apps/sidekick/widget/CalendarRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/CalendarRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)V

    goto :goto_0

    :cond_6
    instance-of v1, p2, Lcom/google/android/apps/sidekick/NewsEntryAdapter;

    if-eqz v1, :cond_7

    new-instance v0, Lcom/google/android/apps/sidekick/widget/NewsRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/NewsEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/NewsRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/NewsEntryAdapter;)V

    goto :goto_0

    :cond_7
    instance-of v1, p2, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    if-eqz v1, :cond_8

    new-instance v0, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/PackageTrackingRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)V

    goto :goto_0

    :cond_8
    instance-of v1, p2, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    if-eqz v1, :cond_9

    new-instance v0, Lcom/google/android/apps/sidekick/widget/TranslateRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/TranslateRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/TranslateEntryAdapter;)V

    goto :goto_0

    :cond_9
    instance-of v1, p2, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    if-eqz v1, :cond_a

    new-instance v0, Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/sidekick/widget/BirthdayRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)V

    goto :goto_0

    :cond_a
    instance-of v1, p2, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    if-eqz v1, :cond_b

    new-instance v0, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/FlightRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)V

    goto/16 :goto_0

    :cond_b
    instance-of v1, p2, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    if-eqz v1, :cond_c

    new-instance v0, Lcom/google/android/apps/sidekick/widget/CurrencyExchangeRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/CurrencyExchangeRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;)V

    goto/16 :goto_0

    :cond_c
    instance-of v1, p2, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    if-eqz v1, :cond_d

    new-instance v0, Lcom/google/android/apps/sidekick/widget/WebsiteUpdateRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/widget/WebsiteUpdateRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;)V

    goto/16 :goto_0

    :cond_d
    instance-of v1, p2, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/widget/ReminderRemoteViewsAdapter;

    check-cast p2, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/sidekick/widget/ReminderRemoteViewsAdapter;-><init>(Lcom/google/android/apps/sidekick/ReminderEntryAdapter;Lcom/google/android/searchcommon/util/Clock;)V

    goto/16 :goto_0

    :cond_e
    invoke-interface {v0, p1}, Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v1

    goto/16 :goto_1

    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method private static isSupported(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Z
    .locals 3
    .param p0    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v0, 0x1

    const/4 v1, 0x0

    instance-of v2, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    if-eqz v2, :cond_1

    check-cast p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->placeConfirmationRequested()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    instance-of v2, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/NewsEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/PublicAlertEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    if-nez v2, :cond_2

    instance-of v2, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    if-eqz v2, :cond_3

    :cond_2
    move v1, v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private populateViewInternal(Landroid/content/Context;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;Ljava/util/List;I)Landroid/widget/RemoteViews;
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            ">;I)",
            "Landroid/widget/RemoteViews;"
        }
    .end annotation

    iget v0, p2, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mRows:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p2, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mIncludePadding:Z

    if-nez v0, :cond_0

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f040097

    invoke-direct {v2, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v3, 0x7f1001df

    const/4 v0, 0x0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->addCard(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/apps/sidekick/EntryItemAdapter;Z)V

    :goto_0
    return-object v2

    :cond_0
    new-instance v10, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f040096

    invoke-direct {v10, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v0, 0x7f1001de

    const/16 v1, 0x8

    invoke-virtual {v10, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001dd

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001dd

    invoke-virtual {v10, v0}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    iget v0, p2, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mRows:I

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v7, v0, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_4

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_4

    iget v0, p2, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mRows:I

    if-ge v8, v0, :cond_4

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f040097

    invoke-direct {v2, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    if-nez v9, :cond_2

    const v3, 0x7f1001df

    invoke-interface {p3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->addCard(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/apps/sidekick/EntryItemAdapter;Z)V

    :cond_1
    :goto_2
    const v0, 0x7f1001dd

    invoke-virtual {v10, v0, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    add-int/lit8 v8, v8, 0x1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v6

    iget v1, p2, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mRows:I

    sub-int/2addr v1, v8

    mul-int/lit8 v1, v1, 0x2

    if-lt v0, v1, :cond_3

    const/4 v9, 0x1

    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    const v3, 0x7f1001e0

    invoke-interface {p3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iget-boolean v5, p2, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mShowHalfWidthCards:Z

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->addCard(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/apps/sidekick/EntryItemAdapter;Z)V

    add-int/lit8 v6, v6, 0x1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    const v3, 0x7f1001e1

    invoke-interface {p3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iget-boolean v5, p2, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;->mShowHalfWidthCards:Z

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->addCard(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/apps/sidekick/EntryItemAdapter;Z)V

    goto :goto_2

    :cond_3
    const/4 v9, 0x0

    goto :goto_3

    :cond_4
    invoke-direct {p0, p1, p4, v10}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->addLaunchIntent(Landroid/content/Context;ILandroid/widget/RemoteViews;)V

    move-object v2, v10

    goto/16 :goto_0
.end method

.method private showNoCards(Landroid/content/Context;I)Landroid/widget/RemoteViews;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const v8, 0x7f1001de

    const/4 v1, 0x0

    new-instance v5, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f040096

    invoke-direct {v5, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const/4 v6, 0x0

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v6, 0x7f1001dd

    const/16 v7, 0x8

    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v6, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v6}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    const v4, 0x7f0d029e

    iget-object v6, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v6, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->canAccountRunTheGoogle(Landroid/accounts/Account;)I

    move-result v2

    const/4 v6, 0x1

    if-eq v2, v6, :cond_5

    if-nez v2, :cond_2

    const v4, 0x7f0d029d

    :cond_0
    :goto_0
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v8, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, p2, v5}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->addLaunchIntent(Landroid/content/Context;ILandroid/widget/RemoteViews;)V

    :cond_1
    return-object v5

    :cond_2
    if-eqz v0, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v6, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->getSavedConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v6, v3}, Lcom/google/android/searchcommon/MarinerOptInSettings;->localeIsBlockedFromNow(Lcom/google/geo/sidekick/Sidekick$Configuration;)Z

    move-result v6

    if-eqz v6, :cond_3

    const v4, 0x7f0d029f

    goto :goto_0

    :cond_3
    const v4, 0x7f0d0297

    goto :goto_0

    :cond_4
    const v4, 0x7f0d029d

    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    if-eqz v0, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v6, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isAccountOptedIn(Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v6, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->getSavedConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v6

    if-nez v6, :cond_0

    :cond_6
    const v4, 0x7f0d029d

    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method populateView(Landroid/content/Context;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;I)Landroid/widget/RemoteViews;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;
    .param p3    # Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;
    .param p4    # I

    iget-object v7, p0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v7}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getEntries()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->showNoCards(Landroid/content/Context;I)Landroid/widget/RemoteViews;

    move-result-object v7

    :goto_0
    return-object v7

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntriesToShow()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntriesToShow()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->isSupported(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->showNoCards(Landroid/content/Context;I)Landroid/widget/RemoteViews;

    move-result-object v7

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2, v2, p4}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->populateViewInternal(Landroid/content/Context;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;Ljava/util/List;I)Landroid/widget/RemoteViews;

    move-result-object v4

    invoke-direct {p0, p1, p3, v2, p4}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->populateViewInternal(Landroid/content/Context;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;Ljava/util/List;I)Landroid/widget/RemoteViews;

    move-result-object v5

    new-instance v7, Landroid/widget/RemoteViews;

    invoke-direct {v7, v4, v5}, Landroid/widget/RemoteViews;-><init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    goto :goto_0
.end method
