.class Lcom/google/android/apps/sidekick/LocationOracleImpl$7;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;->postAndroidLocation(Landroid/location/Location;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

.field final synthetic val$location:Landroid/location/Location;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$7;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$7;->val$location:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$7;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$7;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$7;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$7;->val$location:Landroid/location/Location;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2800(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V

    goto :goto_0
.end method
