.class public Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;
.super Ljava/lang/Object;
.source "EntryItemFactoryImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/EntryItemFactory;


# instance fields
.field private final mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

.field private final mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

.field private final mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

.field private final mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

.field private final mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/searchcommon/MarinerOptInSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Lcom/google/android/apps/sidekick/DirectionsLauncher;Landroid/net/wifi/WifiManager;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/history/SearchHistoryHelper;Lcom/google/android/apps/sidekick/inject/ActivityHelper;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Lcom/google/android/searchcommon/MarinerOptInSettings;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;
    .param p5    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p6    # Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    .param p7    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p8    # Landroid/net/wifi/WifiManager;
    .param p9    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p10    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .param p11    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .param p12    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/util/IntentUtilsImpl;

    invoke-direct {v0}, Lcom/google/android/searchcommon/util/IntentUtilsImpl;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    new-instance v0, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;-><init>(Lcom/google/android/apps/sidekick/inject/EntryProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iput-object p8, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mWifiManager:Landroid/net/wifi/WifiManager;

    iput-object p9, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p10, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iput-object p11, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    iput-object p12, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-void
.end method

.method private createPlaceEntryAdapter(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .locals 14
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasSourceType()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v12

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v9

    const/4 v0, 0x7

    if-eq v9, v0, :cond_1

    const/4 v0, 0x6

    if-eq v12, v0, :cond_0

    const/16 v0, 0x8

    if-eq v12, v0, :cond_0

    const/4 v0, 0x7

    if-ne v12, v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v8, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    :goto_0
    return-object v0

    :cond_1
    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceDataFromEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v10

    if-eqz v10, :cond_6

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasContactData()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/sidekick/ContactEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/sidekick/ContactEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v11, 0x0

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v13

    const/4 v0, 0x3

    if-ne v13, v0, :cond_4

    const/4 v11, 0x1

    :cond_3
    :goto_1
    if-eqz v11, :cond_6

    new-instance v0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v8, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x6

    if-ne v13, v0, :cond_5

    invoke-virtual/range {p4 .. p4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getEventType()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    const/4 v11, 0x1

    goto :goto_1

    :cond_5
    const/16 v0, 0x9

    if-ne v13, v0, :cond_3

    const/4 v11, 0x1

    goto :goto_1

    :cond_6
    new-instance v0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0
.end method


# virtual methods
.method public create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .locals 9
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->createPlaceEntryAdapter(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlaceEntry()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->createPlaceEntryAdapter(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCalendarEntry()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v8, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWeatherEntry()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/WeatherEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTransitStationEntry()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/apps/sidekick/TransitEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/TransitEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGenericCardEntry()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/MarinerOptInSettings;Landroid/net/wifi/WifiManager;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/history/SearchHistoryHelper;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFlightStatusEntry()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/util/Clock;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasSportScoreEntry()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTranslateEntry()Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCurrencyExchangeEntry()Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasClockEntry()Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lcom/google/android/apps/sidekick/ClockEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/ClockEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPublicAlertEntry()Z

    move-result v0

    if-eqz v0, :cond_b

    new-instance v0, Lcom/google/android/apps/sidekick/PublicAlertEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/PublicAlertEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieListEntry()Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Lcom/google/android/apps/sidekick/MovieListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/MovieListEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasStockQuoteListEntry()Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasAttractionListEntry()Z

    move-result v0

    if-eqz v0, :cond_e

    new-instance v0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPackageTrackingEntry()Z

    move-result v0

    if-eqz v0, :cond_f

    new-instance v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNewsEntry()Z

    move-result v0

    if-eqz v0, :cond_10

    new-instance v0, Lcom/google/android/apps/sidekick/NewsEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/NewsEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPhotoSpotEntry()Z

    move-result v0

    if-eqz v0, :cond_11

    new-instance v0, Lcom/google/android/apps/sidekick/PhotoSpotEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/google/android/apps/sidekick/PhotoSpotEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_11
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasLocationHistoryReminderEntry()Z

    move-result v0

    if-eqz v0, :cond_12

    new-instance v0, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/LocationHistoryReminderEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBirthdayCardEntry()Z

    move-result v0

    if-eqz v0, :cond_13

    new-instance v0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventEntry()Z

    move-result v0

    if-eqz v0, :cond_14

    new-instance v0, Lcom/google/android/apps/sidekick/EventEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/EventEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_14
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieEntry()Z

    move-result v0

    if-eqz v0, :cond_15

    new-instance v0, Lcom/google/android/apps/sidekick/MovieEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/MovieEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_15
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBarcodeEntry()Z

    move-result v0

    if-eqz v0, :cond_16

    new-instance v0, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/sidekick/BarcodeEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_16
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieTicketEntry()Z

    move-result v0

    if-eqz v0, :cond_17

    new-instance v0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_17
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->remindersEnabled(Lcom/google/android/searchcommon/SearchConfig;)Z

    move-result v0

    if-eqz v0, :cond_18

    new-instance v0, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;Lcom/google/android/searchcommon/util/Clock;)V

    goto/16 :goto_0

    :cond_18
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    const/16 v1, 0x29

    if-ne v0, v1, :cond_19

    new-instance v0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto/16 :goto_0

    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public createForGroup(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .locals 8
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->hasGroupEntry()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    const/16 v1, 0x1f

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    const/16 v1, 0x26

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    const/16 v1, 0x28

    if-ne v0, v1, :cond_4

    new-instance v0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_5

    new-instance v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryItemFactoryImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
