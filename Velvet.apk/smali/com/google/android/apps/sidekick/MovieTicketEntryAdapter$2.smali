.class Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;
.super Ljava/lang/Object;
.source "MovieTicketEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->populateMovieInfo(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$movie:Lcom/google/geo/sidekick/Sidekick$Movie;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Movie;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;->val$movie:Lcom/google/geo/sidekick/Sidekick$Movie;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter$2;->val$movie:Lcom/google/geo/sidekick/Sidekick$Movie;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Movie;->getTrailerUrl()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MOVIE_TRAILER"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/MovieTicketEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
