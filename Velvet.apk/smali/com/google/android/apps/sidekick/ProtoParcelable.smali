.class public Lcom/google/android/apps/sidekick/ProtoParcelable;
.super Ljava/lang/Object;
.source "ProtoParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/sidekick/ProtoParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/sidekick/ProtoParcelable$1;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/ProtoParcelable$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/sidekick/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/google/android/apps/sidekick/ProtoParcelable;->mData:[B

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ProtoParcelable;->mData:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/sidekick/ProtoParcelable$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/apps/sidekick/ProtoParcelable$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/ProtoParcelable;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>([B)V
    .locals 0
    .param p1    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ProtoParcelable;->mData:[B

    return-void
.end method

.method public static create(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/apps/sidekick/ProtoParcelable;
    .locals 2
    .param p0    # Lcom/google/protobuf/micro/MessageMicro;

    new-instance v0, Lcom/google/android/apps/sidekick/ProtoParcelable;

    invoke-virtual {p0}, Lcom/google/protobuf/micro/MessageMicro;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/ProtoParcelable;-><init>([B)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public extract(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(TT;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoParcelable;->mData:[B

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/MessageMicro;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    return-object p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoParcelable;->mData:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoParcelable;->mData:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
