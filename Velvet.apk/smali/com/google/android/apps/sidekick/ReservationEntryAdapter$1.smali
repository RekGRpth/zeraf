.class Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;
.super Ljava/lang/Object;
.source "ReservationEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->populateEventView(Landroid/content/Context;Landroid/view/View;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

.field final synthetic val$attribution:Lcom/google/geo/sidekick/Sidekick$Attribution;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/ReservationEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Attribution;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;->val$attribution:Lcom/google/geo/sidekick/Sidekick$Attribution;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/ReservationEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ReservationEntryAdapter$1;->val$attribution:Lcom/google/geo/sidekick/Sidekick$Attribution;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Attribution;->getUrl()Ljava/lang/String;

    move-result-object v3

    const-string v4, "EVENT_PHOTO_ATTRIBUTION"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/ReservationEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
