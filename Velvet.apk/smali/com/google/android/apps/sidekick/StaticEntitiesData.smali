.class public final Lcom/google/android/apps/sidekick/StaticEntitiesData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "StaticEntitiesData.java"


# instance fields
.field private cachedSize:I

.field private hasLastRefreshMillis:Z

.field private hasStaticEntities:Z

.field private lastRefreshMillis_:J

.field private staticEntities_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->lastRefreshMillis_:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->staticEntities_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->cachedSize:I

    return v0
.end method

.method public getLastRefreshMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->lastRefreshMillis_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasLastRefreshMillis()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->getLastRefreshMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasStaticEntities()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->getStaticEntities()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->cachedSize:I

    return v0
.end method

.method public getStaticEntities()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->staticEntities_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    return-object v0
.end method

.method public hasLastRefreshMillis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasLastRefreshMillis:Z

    return v0
.end method

.method public hasStaticEntities()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasStaticEntities:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/StaticEntitiesData;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->setLastRefreshMillis(J)Lcom/google/android/apps/sidekick/StaticEntitiesData;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->setStaticEntities(Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;)Lcom/google/android/apps/sidekick/StaticEntitiesData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/StaticEntitiesData;

    move-result-object v0

    return-object v0
.end method

.method public setLastRefreshMillis(J)Lcom/google/android/apps/sidekick/StaticEntitiesData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasLastRefreshMillis:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->lastRefreshMillis_:J

    return-object p0
.end method

.method public setStaticEntities(Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;)Lcom/google/android/apps/sidekick/StaticEntitiesData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasStaticEntities:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/StaticEntitiesData;->staticEntities_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasLastRefreshMillis()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->getLastRefreshMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->hasStaticEntities()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->getStaticEntities()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    return-void
.end method
