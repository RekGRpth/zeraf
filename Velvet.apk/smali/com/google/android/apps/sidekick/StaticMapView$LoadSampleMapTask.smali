.class Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;
.super Landroid/os/AsyncTask;
.source "StaticMapView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/StaticMapView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSampleMapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/StaticMapView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/StaticMapView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/StaticMapView;Lcom/google/android/apps/sidekick/StaticMapView$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/StaticMapView;
    .param p2    # Lcom/google/android/apps/sidekick/StaticMapView$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;-><init>(Lcom/google/android/apps/sidekick/StaticMapView;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1    # [Ljava/lang/Void;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;-><init>()V

    :try_start_0
    iget-object v10, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/StaticMapView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v10

    const-string v11, "sample_route.txt.bin"

    invoke-virtual {v10, v11}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v10

    invoke-virtual {v2, v10}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_0
    new-instance v10, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    invoke-direct {v10}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;-><init>()V

    iget-object v11, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    # getter for: Lcom/google/android/apps/sidekick/StaticMapView;->mMapHeight:I
    invoke-static {v11}, Lcom/google/android/apps/sidekick/StaticMapView;->access$200(Lcom/google/android/apps/sidekick/StaticMapView;)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setHeight(I)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    # getter for: Lcom/google/android/apps/sidekick/StaticMapView;->mMapWidth:I
    invoke-static {v11}, Lcom/google/android/apps/sidekick/StaticMapView;->access$100(Lcom/google/android/apps/sidekick/StaticMapView;)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setWidth(I)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v10

    invoke-virtual {v10, v2}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;->setPlaceEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v9

    new-instance v10, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v10}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    const-wide v11, 0x4042b6ecbfb15b57L

    invoke-virtual {v10, v11, v12}, Lcom/google/geo/sidekick/Sidekick$Location;->setLat(D)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v10

    const-wide v11, -0x3fa1752bd3c36113L

    invoke-virtual {v10, v11, v12}, Lcom/google/geo/sidekick/Sidekick$Location;->setLng(D)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v3

    new-instance v10, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    invoke-direct {v10}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;-><init>()V

    invoke-virtual {v10, v3}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    move-result-object v8

    new-instance v10, Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    invoke-direct {v10}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;-><init>()V

    invoke-virtual {v10, v8}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->addTimestampedLocation(Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-result-object v7

    new-instance v10, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v10}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v10, v9}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setStaticMapQuery(Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v10

    invoke-virtual {v10, v7}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setSensorSignals(Lcom/google/geo/sidekick/Sidekick$SensorSignals;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v5

    iget-object v10, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    # getter for: Lcom/google/android/apps/sidekick/StaticMapView;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;
    invoke-static {v10}, Lcom/google/android/apps/sidekick/StaticMapView;->access$300(Lcom/google/android/apps/sidekick/StaticMapView;)Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v10

    invoke-interface {v10, v5}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithoutLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasStaticMapResponse()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getStaticMapResponse()Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;->getMapPng()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v4}, Lcom/google/protobuf/micro/ByteStringMicro;->size()I

    move-result v12

    invoke-static {v10, v11, v12}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v10

    :goto_1
    return-object v10

    :catch_0
    move-exception v0

    :try_start_1
    const-string v10, "StaticMapView"

    const-string v11, "File not found: "

    invoke-static {v10, v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v10, "StaticMapView"

    const-string v11, "IO Exception: "

    invoke-static {v10, v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_3
    const-string v10, "StaticMapView"

    const-string v11, "IO Exception: "

    invoke-static {v10, v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v10

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v10

    :cond_0
    const/4 v10, 0x0

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->this$0:Lcom/google/android/apps/sidekick/StaticMapView;

    # invokes: Lcom/google/android/apps/sidekick/StaticMapView;->setImageBitmapAndFadeIn(Landroid/graphics/Bitmap;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/sidekick/StaticMapView;->access$400(Lcom/google/android/apps/sidekick/StaticMapView;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/StaticMapView$LoadSampleMapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
