.class Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;
.super Ljava/lang/Object;
.source "RealEstateEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->addListing(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field final synthetic val$uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;->val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;->val$uri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;->val$entry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$5;->val$uri:Ljava/lang/String;

    const-string v4, "REAL_ESTATE_LEARN_MORE"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
