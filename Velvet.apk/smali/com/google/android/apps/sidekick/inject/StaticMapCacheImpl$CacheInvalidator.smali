.class Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$CacheInvalidator;
.super Ljava/lang/Object;
.source "StaticMapCacheImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/EntryProviderObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CacheInvalidator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$CacheInvalidator;->this$0:Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;
    .param p2    # Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$CacheInvalidator;-><init>(Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;)V

    return-void
.end method


# virtual methods
.method public onCardListEntriesRefreshed()V
    .locals 0

    return-void
.end method

.method public onEntriesAdded(Lcom/google/geo/sidekick/Sidekick$Interest;Ljava/util/List;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Interest;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onInvalidated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$CacheInvalidator;->this$0:Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;

    # getter for: Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->mCache:Landroid/util/LruCache;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->access$100(Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;)Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    return-void
.end method

.method public onRefreshed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$CacheInvalidator;->this$0:Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;

    # getter for: Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->mCache:Landroid/util/LruCache;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->access$100(Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;)Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    return-void
.end method
