.class public interface abstract Lcom/google/android/apps/sidekick/inject/NetworkClient;
.super Ljava/lang/Object;
.source "NetworkClient.java"


# virtual methods
.method public abstract isNetworkAvailable()Z
.end method

.method public abstract sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
.end method

.method public abstract sendRequestWithoutLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
.end method

.method public abstract sendRequestWithoutLocationWithAccount(Lcom/google/geo/sidekick/Sidekick$RequestPayload;Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
.end method
