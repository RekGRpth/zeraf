.class Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;
.super Ljava/lang/Object;
.source "StaticMapCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StaticMapCacheKey"
.end annotation


# instance fields
.field private final mFrequentPlaceEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mShowRoute:Z

.field private final mStartLocationKey:Lcom/google/android/apps/sidekick/ProtoKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;Z)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v0, p1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mStartLocationKey:Lcom/google/android/apps/sidekick/ProtoKey;

    new-instance v0, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v0, p2}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mFrequentPlaceEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;

    iput-boolean p3, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mShowRoute:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;

    iget-boolean v3, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mShowRoute:Z

    iget-boolean v4, v0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mShowRoute:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mStartLocationKey:Lcom/google/android/apps/sidekick/ProtoKey;

    iget-object v4, v0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mStartLocationKey:Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mFrequentPlaceEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;

    iget-object v4, v0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mFrequentPlaceEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mFrequentPlaceEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;->mShowRoute:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
