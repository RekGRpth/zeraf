.class public Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;
.super Ljava/lang/Object;
.source "StaticMapCacheImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/StaticMapCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$1;,
        Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;,
        Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$CacheInvalidator;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/apps/sidekick/inject/EntryProvider;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, p1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->mCache:Landroid/util/LruCache;

    new-instance v0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$CacheInvalidator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$CacheInvalidator;-><init>(Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$1;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->registerEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;)Landroid/util/LruCache;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->mCache:Landroid/util/LruCache;

    return-object v0
.end method


# virtual methods
.method public get(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;Z)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .param p3    # Z

    new-instance v0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;Z)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->mCache:Landroid/util/LruCache;

    invoke-virtual {v1, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    return-object v1
.end method

.method public put(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;ZLandroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .param p3    # Z
    .param p4    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl;->mCache:Landroid/util/LruCache;

    new-instance v1, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/sidekick/inject/StaticMapCacheImpl$StaticMapCacheKey;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;Z)V

    invoke-virtual {v0, v1, p4}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
