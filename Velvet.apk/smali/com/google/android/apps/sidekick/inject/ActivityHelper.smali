.class public interface abstract Lcom/google/android/apps/sidekick/inject/ActivityHelper;
.super Ljava/lang/Object;
.source "ActivityHelper.java"


# virtual methods
.method public abstract getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;
.end method

.method public abstract safeStartActivity(Landroid/content/Context;Landroid/content/Intent;)Z
.end method

.method public abstract safeStartActivityWithMessage(Landroid/content/Context;Landroid/content/Intent;I)Z
.end method

.method public abstract safeViewUri(Landroid/content/Context;Landroid/net/Uri;Z)Z
.end method

.method public abstract safeViewUriWithMessage(Landroid/content/Context;Landroid/net/Uri;ZI)Z
.end method

.method public abstract safeViewUrl(Landroid/content/Context;Ljava/lang/String;Z)Z
.end method
