.class public interface abstract Lcom/google/android/apps/sidekick/inject/EntryProvider;
.super Ljava/lang/Object;
.source "EntryProvider.java"


# virtual methods
.method public abstract appendMoreCardEntries(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Landroid/location/Location;)V
.end method

.method public abstract cancelDelayedRefresh()V
.end method

.method public abstract entriesIncludeMore()Z
.end method

.method public abstract findAdapterForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
.end method

.method public abstract getBackgroundImagePhotos()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/inject/BackgroundImage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCardListEntries()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEntries()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getIdForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;
.end method

.method public abstract getLastRefreshTimeMillis()J
.end method

.method public abstract getStalenessTimeoutMs()J
.end method

.method public abstract getTotalEntryCount()I
.end method

.method public abstract handleDismissedEntries(Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract hasLocationChangedSignificantlySinceRefresh()Z
.end method

.method public abstract initializeFromStorage()V
.end method

.method public abstract invalidate()V
.end method

.method public abstract invalidateWithDelayedRefresh()V
.end method

.method public abstract isDataForLocale(Ljava/util/Locale;)Z
.end method

.method public abstract isInitializedFromStorage()Z
.end method

.method public abstract mutateEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V
.end method

.method public abstract registerEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V
.end method

.method public abstract removeGroupChildEntries(Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract unregisterEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V
.end method

.method public abstract updateFromEntryResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Interest;Landroid/location/Location;ZLjava/util/Locale;)V
.end method
