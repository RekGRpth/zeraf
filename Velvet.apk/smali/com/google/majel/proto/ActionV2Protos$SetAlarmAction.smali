.class public final Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SetAlarmAction"
.end annotation


# instance fields
.field private alarmLabelSpan_:Lcom/google/majel/proto/SpanProtos$Span;

.field private alarmLabel_:Ljava/lang/String;

.field private cachedSize:I

.field private date_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

.field private hasAlarmLabel:Z

.field private hasAlarmLabelSpan:Z

.field private hasDate:Z

.field private hasSecondsFromNow:Z

.field private hasTime:Z

.field private secondsFromNow_:I

.field private time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->alarmLabel_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->alarmLabelSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->date_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->secondsFromNow_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAlarmLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->alarmLabel_:Ljava/lang/String;

    return-object v0
.end method

.method public getAlarmLabelSpan()Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->alarmLabelSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->cachedSize:I

    return v0
.end method

.method public getDate()Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->date_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    return-object v0
.end method

.method public getSecondsFromNow()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->secondsFromNow_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasAlarmLabel()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getAlarmLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasAlarmLabelSpan()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getAlarmLabelSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasTime()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasDate()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getDate()Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasSecondsFromNow()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getSecondsFromNow()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->cachedSize:I

    return v0
.end method

.method public getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    return-object v0
.end method

.method public hasAlarmLabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasAlarmLabel:Z

    return v0
.end method

.method public hasAlarmLabelSpan()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasAlarmLabelSpan:Z

    return v0
.end method

.method public hasDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasDate:Z

    return v0
.end method

.method public hasSecondsFromNow()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasSecondsFromNow:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasTime:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->setAlarmLabel(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/SpanProtos$Span;

    invoke-direct {v1}, Lcom/google/majel/proto/SpanProtos$Span;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->setAlarmLabelSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->setDate(Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->setSecondsFromNow(I)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    move-result-object v0

    return-object v0
.end method

.method public setAlarmLabel(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasAlarmLabel:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->alarmLabel_:Ljava/lang/String;

    return-object p0
.end method

.method public setAlarmLabelSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/SpanProtos$Span;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasAlarmLabelSpan:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->alarmLabelSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object p0
.end method

.method public setDate(Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasDate:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->date_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    return-object p0
.end method

.method public setSecondsFromNow(I)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasSecondsFromNow:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->secondsFromNow_:I

    return-object p0
.end method

.method public setTime(Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;)Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasTime:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->time_:Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasAlarmLabel()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getAlarmLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasAlarmLabelSpan()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getAlarmLabelSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getDate()Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasSecondsFromNow()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getSecondsFromNow()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    return-void
.end method
