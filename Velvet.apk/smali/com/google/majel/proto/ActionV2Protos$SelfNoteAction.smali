.class public final Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelfNoteAction"
.end annotation


# instance fields
.field private bodySpan_:Lcom/google/majel/proto/SpanProtos$Span;

.field private body_:Ljava/lang/String;

.field private cachedSize:I

.field private dEPRECATEDSubjectSpan_:Lcom/google/majel/proto/SpanProtos$Span;

.field private dEPRECATEDSubject_:Ljava/lang/String;

.field private hasBody:Z

.field private hasBodySpan:Z

.field private hasDEPRECATEDSubject:Z

.field private hasDEPRECATEDSubjectSpan:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->dEPRECATEDSubject_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->dEPRECATEDSubjectSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->body_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->bodySpan_:Lcom/google/majel/proto/SpanProtos$Span;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->body_:Ljava/lang/String;

    return-object v0
.end method

.method public getBodySpan()Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->bodySpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->cachedSize:I

    return v0
.end method

.method public getDEPRECATEDSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->dEPRECATEDSubject_:Ljava/lang/String;

    return-object v0
.end method

.method public getDEPRECATEDSubjectSpan()Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->dEPRECATEDSubjectSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasDEPRECATEDSubject()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getDEPRECATEDSubject()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasBody()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasDEPRECATEDSubjectSpan()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getDEPRECATEDSubjectSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasBodySpan()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getBodySpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->cachedSize:I

    return v0
.end method

.method public hasBody()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasBody:Z

    return v0
.end method

.method public hasBodySpan()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasBodySpan:Z

    return v0
.end method

.method public hasDEPRECATEDSubject()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasDEPRECATEDSubject:Z

    return v0
.end method

.method public hasDEPRECATEDSubjectSpan()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasDEPRECATEDSubjectSpan:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->setDEPRECATEDSubject(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->setBody(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/SpanProtos$Span;

    invoke-direct {v1}, Lcom/google/majel/proto/SpanProtos$Span;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->setDEPRECATEDSubjectSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/SpanProtos$Span;

    invoke-direct {v1}, Lcom/google/majel/proto/SpanProtos$Span;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->setBodySpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    move-result-object v0

    return-object v0
.end method

.method public setBody(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasBody:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->body_:Ljava/lang/String;

    return-object p0
.end method

.method public setBodySpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/SpanProtos$Span;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasBodySpan:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->bodySpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object p0
.end method

.method public setDEPRECATEDSubject(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasDEPRECATEDSubject:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->dEPRECATEDSubject_:Ljava/lang/String;

    return-object p0
.end method

.method public setDEPRECATEDSubjectSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/SpanProtos$Span;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasDEPRECATEDSubjectSpan:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->dEPRECATEDSubjectSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasDEPRECATEDSubject()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getDEPRECATEDSubject()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasBody()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasDEPRECATEDSubjectSpan()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getDEPRECATEDSubjectSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->hasBodySpan()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getBodySpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    return-void
.end method
