.class public final Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientInfoProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ClientInfoProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenParams"
.end annotation


# instance fields
.field private cachedSize:I

.field private densityDpi_:I

.field private dpiBucket_:I

.field private hasDensityDpi:Z

.field private hasDpiBucket:Z

.field private hasHeightPixels:Z

.field private hasWidthPixels:Z

.field private heightPixels_:I

.field private widthPixels_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->widthPixels_:I

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->heightPixels_:I

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->densityDpi_:I

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->dpiBucket_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->cachedSize:I

    return v0
.end method

.method public getDensityDpi()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->densityDpi_:I

    return v0
.end method

.method public getDpiBucket()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->dpiBucket_:I

    return v0
.end method

.method public getHeightPixels()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->heightPixels_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasWidthPixels()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getWidthPixels()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasHeightPixels()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getHeightPixels()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasDensityDpi()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getDensityDpi()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasDpiBucket()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getDpiBucket()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->cachedSize:I

    return v0
.end method

.method public getWidthPixels()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->widthPixels_:I

    return v0
.end method

.method public hasDensityDpi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasDensityDpi:Z

    return v0
.end method

.method public hasDpiBucket()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasDpiBucket:Z

    return v0
.end method

.method public hasHeightPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasHeightPixels:Z

    return v0
.end method

.method public hasWidthPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasWidthPixels:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->setWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->setHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->setDensityDpi(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->setDpiBucket(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v0

    return-object v0
.end method

.method public setDensityDpi(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasDensityDpi:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->densityDpi_:I

    return-object p0
.end method

.method public setDpiBucket(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasDpiBucket:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->dpiBucket_:I

    return-object p0
.end method

.method public setHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasHeightPixels:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->heightPixels_:I

    return-object p0
.end method

.method public setWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasWidthPixels:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->widthPixels_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasWidthPixels()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getWidthPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasHeightPixels()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getHeightPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasDensityDpi()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getDensityDpi()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->hasDpiBucket()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->getDpiBucket()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    return-void
.end method
