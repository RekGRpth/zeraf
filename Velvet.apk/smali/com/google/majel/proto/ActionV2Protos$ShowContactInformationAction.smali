.class public final Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowContactInformationAction"
.end annotation


# instance fields
.field private cachedSize:I

.field private contact_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;"
        }
    .end annotation
.end field

.field private hasShowEmail:Z

.field private hasShowPhone:Z

.field private hasShowPostalAddress:Z

.field private showEmail_:Z

.field private showPhone_:Z

.field private showPostalAddress_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->contact_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showPhone_:Z

    iput-boolean v1, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showEmail_:Z

    iput-boolean v1, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showPostalAddress_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->contact_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->cachedSize:I

    return v0
.end method

.method public getContactList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->contact_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getContactList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowPhone()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowPhone()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowEmail()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowEmail()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowPostalAddress()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowPostalAddress()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    iput v2, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->cachedSize:I

    return v2
.end method

.method public getShowEmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showEmail_:Z

    return v0
.end method

.method public getShowPhone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showPhone_:Z

    return v0
.end method

.method public getShowPostalAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showPostalAddress_:Z

    return v0
.end method

.method public hasShowEmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowEmail:Z

    return v0
.end method

.method public hasShowPhone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowPhone:Z

    return v0
.end method

.method public hasShowPostalAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowPostalAddress:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->addContact(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->setShowPhone(Z)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->setShowEmail(Z)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->setShowPostalAddress(Z)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    move-result-object v0

    return-object v0
.end method

.method public setShowEmail(Z)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowEmail:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showEmail_:Z

    return-object p0
.end method

.method public setShowPhone(Z)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowPhone:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showPhone_:Z

    return-object p0
.end method

.method public setShowPostalAddress(Z)Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowPostalAddress:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->showPostalAddress_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getContactList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowPhone()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowPhone()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowEmail()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowEmail()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->hasShowPostalAddress()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowPostalAddress()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    return-void
.end method
