.class public final Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientInfoProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ClientInfoProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BrowserParams"
.end annotation


# instance fields
.field private cachedSize:I

.field private googleDomain_:Ljava/lang/String;

.field private hasGoogleDomain:Z

.field private hasHeightPixels:Z

.field private hasSearchLanguage:Z

.field private hasUseMetricUnits:Z

.field private hasUsePreciseGeolocation:Z

.field private hasUserAgent:Z

.field private hasWidthPixels:Z

.field private heightPixels_:I

.field private searchLanguage_:Ljava/lang/String;

.field private useMetricUnits_:Z

.field private usePreciseGeolocation_:Z

.field private userAgent_:Ljava/lang/String;

.field private widthPixels_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->widthPixels_:I

    iput v1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->heightPixels_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->userAgent_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->googleDomain_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->searchLanguage_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->useMetricUnits_:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->usePreciseGeolocation_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->cachedSize:I

    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->clearWidthPixels()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->clearHeightPixels()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->clearUserAgent()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->clearGoogleDomain()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->clearSearchLanguage()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->clearUseMetricUnits()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->clearUsePreciseGeolocation()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->cachedSize:I

    return-object p0
.end method

.method public clearGoogleDomain()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasGoogleDomain:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->googleDomain_:Ljava/lang/String;

    return-object p0
.end method

.method public clearHeightPixels()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasHeightPixels:Z

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->heightPixels_:I

    return-object p0
.end method

.method public clearSearchLanguage()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasSearchLanguage:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->searchLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public clearUseMetricUnits()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUseMetricUnits:Z

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->useMetricUnits_:Z

    return-object p0
.end method

.method public clearUsePreciseGeolocation()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUsePreciseGeolocation:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->usePreciseGeolocation_:Z

    return-object p0
.end method

.method public clearUserAgent()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUserAgent:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->userAgent_:Ljava/lang/String;

    return-object p0
.end method

.method public clearWidthPixels()Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasWidthPixels:Z

    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->widthPixels_:I

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->cachedSize:I

    return v0
.end method

.method public getGoogleDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->googleDomain_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeightPixels()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->heightPixels_:I

    return v0
.end method

.method public getSearchLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->searchLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasWidthPixels()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getWidthPixels()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasHeightPixels()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getHeightPixels()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUserAgent()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getUserAgent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasGoogleDomain()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getGoogleDomain()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasSearchLanguage()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getSearchLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUseMetricUnits()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getUseMetricUnits()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUsePreciseGeolocation()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getUsePreciseGeolocation()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->cachedSize:I

    return v0
.end method

.method public getUseMetricUnits()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->useMetricUnits_:Z

    return v0
.end method

.method public getUsePreciseGeolocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->usePreciseGeolocation_:Z

    return v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->userAgent_:Ljava/lang/String;

    return-object v0
.end method

.method public getWidthPixels()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->widthPixels_:I

    return v0
.end method

.method public hasGoogleDomain()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasGoogleDomain:Z

    return v0
.end method

.method public hasHeightPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasHeightPixels:Z

    return v0
.end method

.method public hasSearchLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasSearchLanguage:Z

    return v0
.end method

.method public hasUseMetricUnits()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUseMetricUnits:Z

    return v0
.end method

.method public hasUsePreciseGeolocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUsePreciseGeolocation:Z

    return v0
.end method

.method public hasUserAgent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUserAgent:Z

    return v0
.end method

.method public hasWidthPixels()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasWidthPixels:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setUserAgent(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setGoogleDomain(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setSearchLanguage(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setUseMetricUnits(Z)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->setUsePreciseGeolocation(Z)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;

    move-result-object v0

    return-object v0
.end method

.method public setGoogleDomain(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasGoogleDomain:Z

    iput-object p1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->googleDomain_:Ljava/lang/String;

    return-object p0
.end method

.method public setHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasHeightPixels:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->heightPixels_:I

    return-object p0
.end method

.method public setSearchLanguage(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasSearchLanguage:Z

    iput-object p1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->searchLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setUseMetricUnits(Z)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUseMetricUnits:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->useMetricUnits_:Z

    return-object p0
.end method

.method public setUsePreciseGeolocation(Z)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUsePreciseGeolocation:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->usePreciseGeolocation_:Z

    return-object p0
.end method

.method public setUserAgent(Ljava/lang/String;)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUserAgent:Z

    iput-object p1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->userAgent_:Ljava/lang/String;

    return-object p0
.end method

.method public setWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasWidthPixels:Z

    iput p1, p0, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->widthPixels_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasWidthPixels()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getWidthPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasHeightPixels()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getHeightPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUserAgent()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasGoogleDomain()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getGoogleDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasSearchLanguage()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getSearchLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUseMetricUnits()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getUseMetricUnits()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->hasUsePreciseGeolocation()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ClientInfoProtos$BrowserParams;->getUsePreciseGeolocation()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    return-void
.end method
