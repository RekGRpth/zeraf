.class public final Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CalendarProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Reminder"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasMethod:Z

.field private hasMinutes:Z

.field private method_:I

.field private minutes_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->minutes_:I

    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->method_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->cachedSize:I

    return v0
.end method

.method public getMethod()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->method_:I

    return v0
.end method

.method public getMinutes()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->minutes_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMinutes()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->getMinutes()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMethod()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->getMethod()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->cachedSize:I

    return v0
.end method

.method public hasMethod()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMethod:Z

    return v0
.end method

.method public hasMinutes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMinutes:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->setMinutes(I)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->setMethod(I)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;

    move-result-object v0

    return-object v0
.end method

.method public setMethod(I)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMethod:Z

    iput p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->method_:I

    return-object p0
.end method

.method public setMinutes(I)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMinutes:Z

    iput p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->minutes_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMinutes()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->getMinutes()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMethod()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->getMethod()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    return-void
.end method
